package com.enation.app.javashop.buyer.api.promotion;

import com.enation.app.javashop.core.promotion.luck.model.vo.QueryLuckRecordVO;
import com.enation.app.javashop.core.promotion.luck.service.LuckManager;
import com.enation.app.javashop.core.promotion.luck.service.LuckRecordManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Buyer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;


/**
 * 抽奖活动
 * @author 孙建
 */
@RestController
@RequestMapping("/promotions/luck")
@Api(description = "抽奖活动API")
public class LuckBuyerController {

    @Autowired
    private LuckManager luckManager;
    @Autowired
    private LuckRecordManager luckRecordManager;


    @GetMapping("query_luck_index")
    @ApiOperation(value = "查询抽奖活动首页")
    public Map queryLuckIndex() {
        Buyer buyer = UserContext.getBuyer();
        return luckManager.queryLuckIndex(buyer.getUid());
    }


    @GetMapping("query_luck_record_list")
    @ApiOperation(value = "查询抽奖记录列表，如果查询我的抽奖记录，传当前登录人的member_id")
    public Page queryLuckRecordList(QueryLuckRecordVO queryLuckRecordVO) {
        return luckRecordManager.queryLuckRecordList(queryLuckRecordVO);
    }


    @GetMapping("start_luck")
    @ApiOperation(value = "点击开始抽奖")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "luck_id", value = "抽奖活动Id", required = true, dataType = "int", paramType = "query")
    })
    public Map startLuck(@ApiIgnore Integer luckId) {
        Buyer buyer = UserContext.getBuyer();
        return luckManager.startLuck(luckId, buyer.getUid());
    }


    @GetMapping("confirm_luck")
    @ApiOperation(value = "确认抽奖结果")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "luck_id", value = "抽奖活动Id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "exchange_id", value = "兑换标识", required = true, dataType = "String", paramType = "query")
    })
    public Map confirmLuck(@ApiIgnore Integer luckId, @ApiIgnore String exchangeId) {
        Buyer buyer = UserContext.getBuyer();
        return luckManager.confirmLuck(luckId, exchangeId, buyer.getUid());
    }

    @ApiOperation(value = "订单付款后查询是否显示抽奖按钮")
    @PostMapping("checkButtonStatus/{order_sn}")
    public boolean checkButtonStatus(@PathVariable("order_sn") String orderSn){
        return luckManager.checkButtonStatus(orderSn);
    }


}
