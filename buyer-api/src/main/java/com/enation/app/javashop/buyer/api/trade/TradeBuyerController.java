package com.enation.app.javashop.buyer.api.trade;

import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanCartManager;
import com.enation.app.javashop.core.trade.cart.model.enums.CartSourceType;
import com.enation.app.javashop.core.trade.cart.model.enums.CheckedWay;
import com.enation.app.javashop.core.trade.order.model.vo.TradeVO;
import com.enation.app.javashop.core.trade.order.service.TradeManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.DictUtils;
import com.enation.app.javashop.framework.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 交易控制器
 *
 * @author Snow create in 2018/5/8
 * @version v2.0
 * @since v7.0.0
 */
@Api(description = "交易接口模块")
@RestController
@RequestMapping("/trade")
@Validated
public class TradeBuyerController {

    @Autowired
    @Qualifier("tradeManagerImpl")
    private TradeManager tradeManager;

    @Autowired
    @Qualifier("shetuanCartManagerImpl")
    private ShetuanCartManager shetuanCartManager;


    @ApiOperation(value = "创建交易---【多个店铺】")
    @PostMapping(value = "/create")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client", value = "客户端类型", required = false, dataType = "String", paramType = "query",allowableValues = "PC,WAP,NATIVE,REACT,MINI"),
            @ApiImplicitParam(name = "way", value = "检查获取方式，购物车还是立即购买", required = true, dataType = "String", paramType = "query",allowableValues = "BUY_NOW,CART"),
    })
    @Deprecated
    public TradeVO create(String client, String way) {
        return this.tradeManager.createTrade(client, CheckedWay.valueOf(way));
    }

    /**
     * 提交订单 by jfeng 2020-06
     * @param client
     * @param way
     * @param sellerId
     * @param cartSourceType
     * @return
     */
    @ApiOperation(value = "创建交易---【单个店铺】")
    @PostMapping(value = "/createSingle")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client", value = "客户端类型", required = false, dataType = "String", paramType = "query",allowableValues = "PC,WAP,NATIVE,REACT,MINI"),
            @ApiImplicitParam(name = "way", value = "检查获取方式，购物车还是立即购买", required = true, dataType = "String", paramType = "query",allowableValues = "BUY_NOW,CART"),
            @ApiImplicitParam(name = "sellerId", value = "卖家ID", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "receive_time_seq", value = "定时达收货时间配置",dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "cartSourceType", value = "SHETUAN_CART：社区团购购物车，COMMON_CART：普通购物车", allowableValues = "SHETUAN_CART,COMMON_CART", paramType = "query",required = true, dataType = "String"),
            @ApiImplicitParam(name = "walletPayPrice", value = "钱包支付金额", required = true, dataType = "double", paramType = "query")
    })
    public TradeVO createSingle(@RequestParam(value = "client") String client, @RequestParam(value = "way") String way,
                                @RequestParam(value = "sellerId") int sellerId, @RequestParam(value = "cartSourceType") String cartSourceType,
                                @RequestParam(value = "receive_time_seq",required = false) Integer receiveTimeSeq,
                                @RequestParam(value = "walletPayPrice") double walletPayPrice) {
        TradeVO trade =null;
        CartSourceType cartCheckTypeEnums = CartSourceType.valueOf(cartSourceType);
        switch (cartCheckTypeEnums) {
            case SHETUAN_CART:
                trade=  this.shetuanCartManager.createTrade(client, CheckedWay.valueOf(way),sellerId, UserContext.getBuyer().getUid(), walletPayPrice,receiveTimeSeq);
                break;
            case COMMON_CART:
                trade = this.tradeManager.createTrade(client, CheckedWay.valueOf(way), sellerId, UserContext.getBuyer().getUid(), walletPayPrice);
                break;
            default:
                throw new ServiceException("500","未知购物车结算类型-普通订单-社区团购订单!");
        }

        // 红包分享开关
        getCouponShare(sellerId, trade);

        return trade;
    }

    private void getCouponShare( int sellerId, TradeVO trade) {
        String seller_share_coupon = DictUtils.getDictValue(null, "SELLER_SHARE_COUPON_" + sellerId, "SELLER_SHARE_COUPON");
        seller_share_coupon = StringUtil.isEmpty(seller_share_coupon) ? "CLOSE" : seller_share_coupon;
        trade.setCouponShare(seller_share_coupon);
    }


}
