package com.enation.app.javashop.buyer.api.promotion;

import com.enation.app.javashop.core.member.model.dos.MemberCoupon;
import com.enation.app.javashop.core.member.service.MemberCouponManager;
import com.enation.app.javashop.core.promotion.coupon.model.dos.CouponDO;
import com.enation.app.javashop.core.promotion.coupon.model.dos.CouponTypeEnum;
import com.enation.app.javashop.core.promotion.coupon.service.CouponManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 优惠券相关API
 *
 * @author Snow create in 2018/7/13
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/promotions/coupons")
@Api(description = "优惠券相关API")
@Validated
public class CouponBuyerController {

    @Autowired
    private CouponManager couponManager;

    @Autowired
    private MemberCouponManager memberCouponManager;

    @ApiOperation(value = "查询商家优惠券列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "seller_id", value = "商家ID", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "coupon_type", value = "红包类型", dataType = "String", paramType = "query")
    })
    @GetMapping()
    public List<CouponDO> getList(@ApiIgnore Integer sellerId,@ApiIgnore String  couponType) {
        // 小程序默认只展示商品券
        couponType=StringUtils.isEmpty(couponType)?CouponTypeEnum.GOODS.value():couponType;
        // 查询优惠券
        List<CouponDO> couponDOList = this.couponManager.getList(sellerId, couponType);
        // 查询用户已领取优惠
        Map<Integer, MemberCoupon> memberCouponMap = Maps.newHashMap();
        if(UserContext.getBuyer()!=null){
            List<MemberCoupon> memberCoupons = memberCouponManager.listByCheckout(sellerId, UserContext.getBuyer().getUid());
            if(!CollectionUtils.isEmpty(memberCoupons)){
               memberCouponMap = memberCoupons.stream().collect(Collectors.toMap(MemberCoupon::getCouponId, Function.identity()));
            }
        }
        for (CouponDO couponDO : couponDOList) {
            MemberCoupon memberCoupon = memberCouponMap.get(couponDO.getCouponId());
            if(couponDO.getLimitNum()!=null && memberCoupon!=null && couponDO.getLimitNum()<=memberCoupon.getCouponNum()){
                couponDO.setDisabled(1);
                couponDO.setErrorMsg("已领取");
            }
            if(couponDO.getReceivedNum()>=couponDO.getCreateNum()){
                couponDO.setDisabled(2);
                couponDO.setErrorMsg("已领完");
            }
        }
        return couponDOList;
    }


    @ApiOperation(value = "查询所有优惠券")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "条数", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "seller_id", value = "商家ID", dataType = "int", paramType = "query")
    })
    @GetMapping(value = "/all")
    public Page<CouponDO> getPage(@ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize, @ApiIgnore Integer sellerId) {
        Page<CouponDO> page = this.couponManager.all(pageNo, pageSize, sellerId, CouponTypeEnum.GOODS.value());
        return page;
    }

    @ApiOperation(value = "根据ids查询商家优惠券")
    @PostMapping("/queryByIds")
    public List<CouponDO> queryByIds(@RequestBody List<Integer> couponIds) {
        List<CouponDO> couponDOList = this.couponManager.getByIds(couponIds);
        return couponDOList;
    }

    @ApiOperation(value = "根据ids查询商家优惠券")
    @GetMapping("/queryCouponByIds")
    public List<CouponDO> queryCouponByIds(@RequestParam String couponIds) {
        if (StringUtils.isEmpty(couponIds)) {
            return new ArrayList<>();
        }
        String[] couponIdsArray = couponIds.split(",");
        List<Integer> couponIdsList = new ArrayList<>();
        for (String id : couponIdsArray) {
            couponIdsList.add(Integer.valueOf(id));
        }
        List<CouponDO> couponDOList = this.couponManager.getByIds(couponIdsList);
        return couponDOList;
    }


}
