package com.enation.app.javashop.buyer.api.account;

import com.dag.eagleshop.core.account.model.dto.account.AccountDTO;
import com.dag.eagleshop.core.account.model.dto.account.QueryTradeRecordDTO;
import com.dag.eagleshop.core.account.model.dto.account.TradeRecordDTO;
import com.dag.eagleshop.core.account.model.dto.bankcard.BindBankCardDTO;
import com.dag.eagleshop.core.account.model.dto.base.IdDTO;
import com.dag.eagleshop.core.account.model.dto.base.PageDTO;
import com.dag.eagleshop.core.account.model.dto.base.SortField;
import com.dag.eagleshop.core.account.model.dto.base.ViewPage;
import com.dag.eagleshop.core.account.model.dto.withdraw.AddWithdrawAccountDTO;
import com.dag.eagleshop.core.account.model.dto.withdraw.UpdateWithdrawAccountDTO;
import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;
import com.dag.eagleshop.core.account.model.enums.WithdrawAccountTypeEnum;
import com.dag.eagleshop.core.account.model.vo.AddWithdrawAccountVO;
import com.dag.eagleshop.core.account.model.vo.BindBankCardVO;
import com.dag.eagleshop.core.account.model.vo.UnBindBankCardVO;
import com.dag.eagleshop.core.account.model.vo.UpdateWithdrawAccountVO;
import com.dag.eagleshop.core.account.service.AccountManager;
import com.dag.eagleshop.core.account.service.BankCardManager;
import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.security.model.Buyer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 账户控制器
 * @author 孙建
 */
@Api(description = "账户接口模块")
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private MemberManager memberManager;
    @Autowired
    private AccountManager accountManager;
    @Autowired
    private BankCardManager bankCardManager;


    @GetMapping("/walletIndex")
    @ApiOperation(value = "我的钱包首页")
    public JsonBean walletIndex() {
        Integer uid = UserContext.getBuyer().getUid();
        String accountMemberId = this.getAccountMemberId(uid);

        // 查询账户
        AccountDTO accountDTO = accountManager.queryByMemberIdAndAccountType(accountMemberId, AccountTypeEnum.MEMBER_MASTER.getIndex());

        // 查询交易记录
        QueryTradeRecordDTO queryTradeRecordDTO = new QueryTradeRecordDTO();
        queryTradeRecordDTO.setMemberId(accountMemberId);
        queryTradeRecordDTO.setAccountId(accountDTO.getId());
        PageDTO<QueryTradeRecordDTO> pageDTO = new PageDTO<>(1, 10, queryTradeRecordDTO);
        pageDTO.setOrderBys(new SortField[]{ new SortField(false, "create_time" )});
        ViewPage<TradeRecordDTO> tradeRecordList = accountManager.queryTradeRecordList(pageDTO);

        // 封装返回值
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("account", accountDTO);
        resultMap.put("tradeRecordList", tradeRecordList.getRecords());
        return new JsonBean("查询成功", resultMap);
    }


    @ApiOperation(value = "查询交易记录列表")
    @PostMapping(value = "/queryTradeRecordList"   )
    public JsonBean queryTradeRecordList(@RequestBody PageDTO<QueryTradeRecordDTO> pageDTO){
        Integer uid = UserContext.getBuyer().getUid();
        String accountMemberId = this.getAccountMemberId(uid);

        // 查询交易记录列表
        QueryTradeRecordDTO queryWithdrawBillDTO = pageDTO.getBody();
        queryWithdrawBillDTO.setMemberId(accountMemberId);
        pageDTO.setOrderBys(new SortField[]{ new SortField(false, "create_time") });
        ViewPage viewPage = accountManager.queryTradeRecordList(pageDTO);
        return new JsonBean("查询成功", viewPage);
    }


    @ApiOperation(value = "提交提现账号，例如：支付宝、微信", response = JsonBean.class)
    @PostMapping(value = "/submitWithdrawAccount")
    public JsonBean submitWithdrawAccount(@RequestBody AddWithdrawAccountVO addWithdrawAccountVO){
        Integer uid = UserContext.getBuyer().getUid();
        String accountMemberId = this.getAccountMemberId(uid);

        AddWithdrawAccountDTO addWithdrawAccountDTO = new AddWithdrawAccountDTO();
        BeanUtils.copyProperties(addWithdrawAccountVO, addWithdrawAccountDTO);
        addWithdrawAccountDTO.setMemberId(accountMemberId);
        addWithdrawAccountDTO.setTypeName(WithdrawAccountTypeEnum.getTextByIndex(addWithdrawAccountDTO.getType()));
        accountManager.addWithdrawAccount(addWithdrawAccountDTO);
        return new JsonBean("提交成功");
    }


    @ApiOperation(value = "修改提现账号，例如：支付宝、微信", response = JsonBean.class)
    @PostMapping(value = "/updateWithdrawAccount")
    public JsonBean updateWithdrawAccount(@RequestBody UpdateWithdrawAccountVO updateWithdrawAccountVO){
        Integer uid = UserContext.getBuyer().getUid();
        String accountMemberId = this.getAccountMemberId(uid);

        UpdateWithdrawAccountDTO updateWithdrawAccountDTO = new UpdateWithdrawAccountDTO();
        BeanUtils.copyProperties(updateWithdrawAccountVO, updateWithdrawAccountDTO);
        updateWithdrawAccountDTO.setMemberId(accountMemberId);
        updateWithdrawAccountDTO.setTypeName(WithdrawAccountTypeEnum.getTextByIndex(updateWithdrawAccountDTO.getType()));
        accountManager.updateWithdrawAccountById(updateWithdrawAccountDTO);
        return new JsonBean("更新成功");
    }


    @ApiOperation(value = "绑定银行卡", response = JsonBean.class)
    @PostMapping(value = "/bindBankCard")
    public JsonBean bindBankCard(@RequestBody BindBankCardVO bindBankCardVO){
        Buyer buyer = UserContext.getBuyer();
        Integer uid = buyer.getUid();
        String accountMemberId = this.getAccountMemberId(uid);

        Member member = memberManager.getModel(uid);

        // 绑定银行卡
        BindBankCardDTO bindBankCardDTO = new BindBankCardDTO();
        BeanUtils.copyProperties(bindBankCardVO, bindBankCardDTO);
        bindBankCardDTO.setMemberId(accountMemberId);
        bindBankCardDTO.setMobile(member.getMobile());
        bankCardManager.bind(bindBankCardDTO);
        return new JsonBean("绑定成功");
    }


    @ApiOperation(value = "解绑银行卡，需短信验证", response = JsonBean.class)
    @PostMapping(value = "/unBindBankCard")
    public JsonBean unBindBankCard(@RequestBody UnBindBankCardVO unBindBankCardVO){
        Buyer buyer = UserContext.getBuyer();
        Integer uid = buyer.getUid();

        // 解绑银行卡
        bankCardManager.unbind(IdDTO.by(unBindBankCardVO.getBankCardId()));
        return new JsonBean("解绑成功");
    }

    /**
     * 获取账户会员id
     */
    private String getAccountMemberId(Integer uid){
        Member member = memberManager.getModel(uid);
        return member.getAccountMemberId();
    }

    @GetMapping("/queryAccount")
    @ApiOperation(value = "查询会员帐户信息")
    public JsonBean queryAccount() {
        Integer uid = UserContext.getBuyer().getUid();
        String accountMemberId = this.getAccountMemberId(uid);

        // 查询账户
        AccountDTO accountDTO = accountManager.queryByMemberIdAndAccountType(accountMemberId, AccountTypeEnum.MEMBER_MASTER.getIndex());
        return new JsonBean("查询成功", accountDTO);
    }
}
