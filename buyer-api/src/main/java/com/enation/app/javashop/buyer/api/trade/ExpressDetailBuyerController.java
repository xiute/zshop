package com.enation.app.javashop.buyer.api.trade;

import com.enation.app.javashop.core.system.model.vo.ExpressDetailVO;
import com.enation.app.javashop.core.system.service.ExpressPlatformManager;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 物流查询接口
 *
 * @author zh
 * @version v7.0
 * @date 18/7/12 上午10:30
 * @since v7.0
 */
@Api(description = "物流查询接口")
@RestController
@RequestMapping("/express")
@Validated
public class ExpressDetailBuyerController {

    @Autowired
    private ExpressPlatformManager expressPlatformManager;

    @Autowired
    private OrderQueryManager orderQueryManager;

    @ApiOperation(value = "根据物流单号查询物流详细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "物流公司id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "num", value = "快递单号", dataType = "String", paramType = "query"),
    })
    @GetMapping
    public ExpressDetailVO list(@ApiIgnore Integer id, @ApiIgnore String num) {
        return this.expressPlatformManager.getExpressDetail(id, num);
    }

    @ApiOperation(value = "根据订单号,查询物流详细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_id", value = "物流公司id", dataType = "int", paramType = "query"),
    })
    @GetMapping("/{order_id}")
    public void list(@ApiIgnore @PathVariable("order_id") Integer  orderId) {
        // 2020/7/23
        //OrderDO orderDO = orderQueryManager.getModel(orderId);
        //Map<Integer, List<OrderItemsDO>> collect = orderItems.stream().filter(orderItemsDO -> orderItemsDO.getShipNo() != null && orderItemsDO.getLogiId() != null)
        //          .collect(Collectors.groupingBy(OrderItemsDO::getLogiId, Collectors.toList()));
        //List<OrderPackageVO>  = new ArrayList<>();
        //for (OrderItemsDO orderItem : orderItems) {
        //
        //}
        //List<OrderSkuDTO> orderSkuList = orderDetailDTO.getOrderSkuList();
        //this.expressPlatformManager.getExpressDetail(id, num);
    }
}
