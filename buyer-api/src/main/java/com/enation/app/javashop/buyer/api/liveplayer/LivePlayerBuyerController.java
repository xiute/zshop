package com.enation.app.javashop.buyer.api.liveplayer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.enation.app.javashop.core.client.system.SmsClient;
import com.enation.app.javashop.core.member.plugin.wechat.WechatConnectLoginPlugin;
import com.enation.app.javashop.core.member.service.ConnectManager;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.util.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 小程序直播api
 */
@RestController
@RequestMapping("/liveplayer/minipro")
@Api(description = "小程序直播api")
@Validated
public class LivePlayerBuyerController {

    @Autowired
    public WechatConnectLoginPlugin wechatConnectLoginPlugin;
    @Autowired
    public ConnectManager connectManager;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private SmsClient smsClient;
    @Autowired
    private Cache cache;


    @GetMapping("/getliveinfo")
    @ApiOperation(value = "获取精选直播列表")
    public JSONArray getliveinfo(Integer start, Integer limit) {
        String url = "https://api.weixin.qq.com/wxa/business/getliveinfo?access_token=" + wechatConnectLoginPlugin.getWXAccessToken();
        Map<String, Object> params = new HashMap<>();
        params.put("start", start);
        params.put("limit", limit);
        String jsonStringResult = HttpUtils.doPostWithJson(url, params);
        JSONObject jsonResult = JSON.parseObject(jsonStringResult);
        JSONArray roomInfoArray = jsonResult.getJSONArray("room_info");
        int size = roomInfoArray.size();
        JSONArray resultArray = new JSONArray();
        // 过滤直播间状态 101：直播中，102：未开始，103：已结束，104：禁播，105：暂停，106：异常，107：已过期
        for (int i = 0; i < size; i++) {
            JSONObject jsonRoom = roomInfoArray.getJSONObject(i);
            Integer liveStatus = jsonRoom.getInteger("live_status");
            if(liveStatus == 101 || liveStatus == 102 || liveStatus == 103 || liveStatus == 105 || liveStatus == 106){
                resultArray.add(jsonRoom);
            }
        }
        return resultArray;
    }

}
