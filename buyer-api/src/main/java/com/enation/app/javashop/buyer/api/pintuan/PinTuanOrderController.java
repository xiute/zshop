package com.enation.app.javashop.buyer.api.pintuan;

import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.goods.service.GoodsSkuManager;
import com.enation.app.javashop.core.promotion.pintuan.model.PinTuanGoodsVO;
import com.enation.app.javashop.core.promotion.pintuan.model.PintuanOrderDetailVo;
import com.enation.app.javashop.core.promotion.pintuan.model.PtGoodsDoc;
import com.enation.app.javashop.core.promotion.pintuan.service.PinTuanSearchManager;
import com.enation.app.javashop.core.promotion.pintuan.service.PintuanGoodsManager;
import com.enation.app.javashop.core.promotion.pintuan.service.PintuanOrderManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * Created by kingapex on 2019-01-30.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-01-30
 */
@Api(description = "拼团订单API")
@RestController
@RequestMapping("/pintuan")
public class PinTuanOrderController {

    @Autowired
    private PintuanOrderManager pintuanOrderManager;

    @Autowired
    private PinTuanSearchManager pinTuanSearchManager;

    @Autowired
    private PintuanGoodsManager pintuanGoodsManager;


    @Autowired
    private GoodsSkuManager goodsSkuManager;

    @GetMapping("/orders/{order_sn}")
    @ApiOperation(value = "获取某个拼团的详细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "order_sn", required = true, dataType = "String", paramType = "path")

    })
    public PintuanOrderDetailVo detail(@ApiIgnore @PathVariable(name = "order_sn") String orderSn) {

        PintuanOrderDetailVo pintuanOrder = pintuanOrderManager.getMainOrderBySn(orderSn);
        PinTuanGoodsVO pinTuanGoodsVO = pintuanGoodsManager.getDetail(pintuanOrder.getSkuId(), pintuanOrder.getPintuanId());
        pintuanOrder.setPinTuanGoods(pinTuanGoodsVO);
        return pintuanOrder;

    }
    @GetMapping("/orders/{order_sn}/guest")
    @ApiOperation(value = "猜你喜欢")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "order_sn", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "num", value = "显示数量", required = true, dataType = "String", paramType = "path")

    })
    public List<PtGoodsDoc> guest(@ApiIgnore @PathVariable(name = "order_sn") String orderSn, Integer num) {

        try {
            PintuanOrderDetailVo pintuanOrder = pintuanOrderManager.getMainOrderBySn(orderSn);
            GoodsSkuVO skuVO = goodsSkuManager.getSkuFromCache(pintuanOrder.getSkuId());

            // 准备默认值
            if(num==null){
                num=10;
            }
            return pinTuanSearchManager.search(skuVO.getCategoryId(), 1, num);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping("/test/{order_id}")
    @ApiOperation(value = "获取某个拼团的详细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_id", value = "order_id", required = true, dataType = "int", paramType = "path")

    })
    public void test(@ApiIgnore @PathVariable(name = "order_id") Integer pintuanOrderId) {

        pintuanOrderManager.calDistributionCommission(pintuanOrderId);

    }



}
