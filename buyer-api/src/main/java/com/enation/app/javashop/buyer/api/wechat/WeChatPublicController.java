package com.enation.app.javashop.buyer.api.wechat;

import com.enation.app.javashop.core.statistics.util.WeChatUtil;
import com.enation.app.javashop.core.system.service.WechatPublicManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 微信公众号控制器
 * @author 王志杨
 * @since 2020年11月10日 09:37:00
 */
@Api(description = "微信公众号")
@RestController
@RequestMapping("/wechat/public")
@Validated
public class WeChatPublicController {

    @Autowired
    private WechatPublicManager wechatPublicManager;

    /**
     * 处理微信服务器发来的get请求，进行签名的验证
     *
     * signature 微信端发来的签名
     * timestamp 微信端发来的时间戳
     * nonce     微信端发来的随机字符串
     * echostr   微信端发来的验证字符串
     */
    @GetMapping
    public String validate(@RequestParam(value = "signature") String signature,
                           @RequestParam(value = "timestamp") String timestamp,
                           @RequestParam(value = "nonce") String nonce,
                           @RequestParam(value = "echostr") String echostr) {
        return WeChatUtil.checkSignature(signature, timestamp, nonce) ? echostr : null;
    }

    /**
     * 处理微信服务器的消息转发
     */
    @PostMapping
    public String processMsg(HttpServletRequest request) {
        // 调用核心服务类接收处理请求
        return wechatPublicManager.processRequest(request);
    }

    @GetMapping("/get-access-token")
    @ApiOperation(value = "获取公众号accessToken")
    public String getAccessToken() {
        return wechatPublicManager.getTokenFromCache();
    }


//    @ApiOperation(value = "创建微信公众号底部菜单")
//    @PostMapping(value = "/menu/create")
//    public String createMenu() {
//        // 调用核心服务类接收处理请求
//        return wechatPublicManager.createMenu();
//    }
}
