package com.enation.app.javashop.buyer.api.trade;

import com.enation.app.javashop.core.promotion.shetuan.model.vo.OrderProfitStatisticVO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.OrderProfitVO;
import com.enation.app.javashop.core.trade.order.model.dto.OrderProfitQueryParam;
import com.enation.app.javashop.core.trade.order.service.OrderProfitManager;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.util.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(description = "订单收益API")
@RestController
@RequestMapping("/trade/profit")
@Validated
public class OrderProfitController {

    @Autowired
    private OrderProfitManager orderProfitManager;


    @ApiOperation(value = "统计收益列表", response = OrderProfitStatisticVO.class)
    @PostMapping("count_profit_list")

    public Map<String,Object> countProfitList(@RequestBody OrderProfitQueryParam queryParam){

        // 修复时间戳问题
        Long endTime = queryParam.getEndTime();
        if(endTime != null){
            endTime = endTime + 86400;
            queryParam.setEndTime(endTime);
        }
        // 查询收益列表
        Page page = orderProfitManager.loadProfitList(queryParam);
        List<OrderProfitStatisticVO> dataList = page.getData();
        if(dataList != null && !dataList.isEmpty()){
            for (OrderProfitStatisticVO orderProfitStatisticVO : dataList) {
                orderProfitStatisticVO.show();
            }
        }
        // 查询页面下面的合计
        OrderProfitStatisticVO orderProfitStatisticVO = orderProfitManager.statisticAll(queryParam);
        orderProfitStatisticVO.show();

        //结果
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("statisticPageList", page);
        resultMap.put("statisticCount", orderProfitStatisticVO);
        return resultMap;
    }

    @ApiOperation(value = "查询收益明细", response = OrderProfitVO.class)
    @PostMapping("query_profit_list")

    public Map<String,Object> queryProfitList(@RequestBody OrderProfitQueryParam queryParam){
        // 订单明细详情 时间转换
       String dateTime = queryParam.getDateTime();
        Integer type = queryParam.getType();
        Long startTime;
        Long endTime;
        if(type == 0){
            Date date = DateUtil.toDate(dateTime, "yyyyMMdd");
            startTime = DateUtil.loadDayBegin(date);
            endTime = startTime + 86400;
        }else{
            Date date = DateUtil.toDate(dateTime, "yyyyMM");
            startTime = DateUtil.loadMonthBegin(date);
            //一个月零一天时间戳
            endTime = startTime + 2678400;
        }


        queryParam.setStartTime(startTime);
        queryParam.setEndTime(endTime);
        queryParam.setIsZero(0);

        // 收益明细详情数据
        Page page = orderProfitManager.queryProfitList(queryParam);
        List<OrderProfitVO> dataList = page.getData();
        if(dataList != null && !dataList.isEmpty()){
            for (OrderProfitVO orderProfitVO : dataList) {
                orderProfitVO.show();
            }
        }
        // 收益明细详情页面下面统计统计数据
        OrderProfitQueryParam orderProfitQueryParam = new OrderProfitQueryParam();
        orderProfitQueryParam.setMemberId(queryParam.getMemberId());
        orderProfitQueryParam.setStartTime(startTime);
        orderProfitQueryParam.setEndTime(endTime);
        OrderProfitStatisticVO orderProfitStatisticVO = orderProfitManager.statisticAll(orderProfitQueryParam);
        orderProfitStatisticVO.show();

        // 结果
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("statisticPageList", page);
        resultMap.put("statisticCount", orderProfitStatisticVO);

        return resultMap;
    }
}
