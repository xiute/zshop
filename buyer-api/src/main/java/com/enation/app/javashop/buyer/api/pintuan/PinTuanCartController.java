package com.enation.app.javashop.buyer.api.pintuan;

import com.enation.app.javashop.core.promotion.pintuan.service.PintuanCartManager;
import com.enation.app.javashop.core.promotion.pintuan.service.impl.PintuanTradeManagerImpl;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuOriginVo;
import com.enation.app.javashop.core.trade.cart.model.vo.CartView;
import com.enation.app.javashop.core.trade.order.model.vo.TradeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;

/**
 * Created by kingapex on 2019-01-23.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2019-01-23
 */

@Api(description = "拼团购物API")
@RestController
@RequestMapping("/pintuan")
public class PinTuanCartController {

    @Autowired
    private PintuanCartManager pintuanCartManager;

    @Autowired
    private PintuanTradeManagerImpl pintuanTradeManagerImpl;

    /**
     * 查询拼团购物车详情
     * @return 购物车视图
     */
    @ApiOperation(value = "获取购物车页面购物车详情")
    @GetMapping("/cart")
    public CartView cart() {

        CartView cartView = pintuanCartManager.getCart();

        return cartView;
    }


    /**
     * 参与拼团和发起拼团 首页都要向拼团购物车添加商品
     * @param skuId 商品skuID
     * @param num 数量
     */
    @ApiOperation(value = "向拼团购物车中加一个sku")
    @PostMapping("/cart/sku")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sku_id", value = "sku ID", dataType = "int", paramType = "query", required = true),
            @ApiImplicitParam(name = "num", value = "购买数量", dataType = "int", paramType = "query")
    })
    public CartSkuOriginVo addSku(@ApiIgnore @NotNull(message = "sku id不能为空") Integer skuId, Integer num) {
        CartSkuOriginVo cartSkuOriginVo = pintuanCartManager.addSku(skuId, num);
        return cartSkuOriginVo;
    }


    @ApiOperation(value = "创建交易")
    @PostMapping(value = "/trade")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client", value = "客户端类型", dataType = "String", paramType = "query", allowableValues = "PC,WAP,NATIVE,REACT"),
            @ApiImplicitParam(name = "pintuan_order_id", value = "拼团订单id，如果为空创建拼团，如果不为空参团", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "receive_time_seq", value = "定时达收货时间配置",dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "wallet_pay_price", value = "钱包支付金额", required = true, dataType = "double", paramType = "query")
    })
    public TradeVO create(@RequestParam(value = "client") String client,
                          @RequestParam(value = "pintuan_order_id") Integer pintuanOrderId,
                          @RequestParam(value = "receive_time_seq", required = false) Integer receiveTimeSeq,
                          @RequestParam(value = "wallet_pay_price") double walletPayPrice) {
        TradeVO tradeVO = this.pintuanTradeManagerImpl.createTrade(client, pintuanOrderId,  receiveTimeSeq, walletPayPrice);
        return tradeVO;
    }
}
