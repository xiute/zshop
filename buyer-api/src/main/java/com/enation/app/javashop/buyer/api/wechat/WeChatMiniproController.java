package com.enation.app.javashop.buyer.api.wechat;

import com.enation.app.javashop.core.system.model.dos.WechatMsgTemplate;
import com.enation.app.javashop.core.system.service.WechatMiniproTemplateManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.util.SqlUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 微信小程序
 * @author 孙建
 */
@Api(description = "微信小程序")
@RestController
@RequestMapping("/wechat/minipro")
@Validated
public class WeChatMiniproController {

    @Autowired
    @Qualifier("systemDaoSupport")
    private DaoSupport daoSupport;
    @Autowired
    private WechatMiniproTemplateManager wechatMiniproTemplateManager;


    @ApiOperation(value = "获取模板数据")
    @GetMapping
    public List<String> getTemplate(String types) {
        if(types == null || types.isEmpty()){
            throw new RuntimeException("参数不能为空");
        }

        String[] typeArray = types.split(",");
        if(typeArray.length > 0){
            for (int i = 0; i < typeArray.length; i++) {
                typeArray[i] = typeArray[i].trim();
            }
        }

        List<Object> term = new ArrayList<>();
        String str = SqlUtil.getInSql(typeArray, term);
        String sql = "select * from es_wechat_msg_template where tmp_type in (" + str + ")";
        List<WechatMsgTemplate> wechatMsgTemplateList = this.daoSupport.queryForList(sql, WechatMsgTemplate.class, term.toArray());
        if(wechatMsgTemplateList != null && !wechatMsgTemplateList.isEmpty()){
            return wechatMsgTemplateList.stream().map(WechatMsgTemplate::getTemplateId).collect(Collectors.toList());
        }
        return null;
    }

}
