package com.enation.app.javashop.buyer.api.goods;

import com.enation.app.javashop.core.goods.model.vo.PlacardGenVO;
import com.enation.app.javashop.core.goods.service.GoodsShareManager;
import com.enation.app.javashop.core.system.service.WechatMiniproTemplateManager;
import com.enation.app.javashop.framework.util.Validator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/goods/share")
@Api(description = "商品分享api")
public class GoodsShareController {


    @Autowired
    WechatMiniproTemplateManager wechatMiniproTemplateManager;

    @Autowired
    GoodsShareManager goodsShareManager;

    /**
     * 海报生成接口
     */
    @ApiOperation(value = "海报生成接口")
    @PostMapping(value = "/")
    public Map<String,Object> placardGen(@RequestBody PlacardGenVO placardVO ){

        return  this.goodsShareManager.gen(placardVO);


    }


    /**
     * 海报分享前，将场景参数存入缓存
     */
    @ApiImplicitParam(name = "kvs" , value = "参数Map,kv式{\"k1\":\"v1\",\"k2\":\"v2\"……}")
    @PostMapping("/save/scene")
    @ApiOperation(value = "缓存海报scene详细参数",notes = "返回具体的scene唯一值，跳转到具体页面后，拿scene去后台获取具体的参数值，进行页面处理")
    public String saveSceneDetail(@RequestBody Map  kvs ){


        return this.goodsShareManager.saveSceneDetail(kvs);

    }

    /**
     * 海报分享前，将场景参数存入缓存
     */
    @GetMapping("/get/scene/{scene}")
    @ApiImplicitParam(name = "scene" , required = true,value = "scene的编码值")
    @ApiOperation(value = "缓存海报scene详细参数",notes = "返回具体的scene唯一值，跳转到具体页面后，拿scene去后台获取具体的参数值，进行页面处理")
    public Map getScene(@PathVariable("scene") String  scene ){

        return this.goodsShareManager.getSceneDetail(scene);

    }





}
