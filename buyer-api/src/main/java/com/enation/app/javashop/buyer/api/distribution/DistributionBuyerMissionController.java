package com.enation.app.javashop.buyer.api.distribution;

import com.enation.app.javashop.core.distribution.service.DistributionMissionService;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.security.model.Buyer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * @author 孙建
 * 团长任务API
 */
@RestController
@RequestMapping("/distribution/mission")
@Api(description = "团长任务API")
public class DistributionBuyerMissionController {

    @Autowired
    private DistributionMissionService distributionMissionService;


    @GetMapping("query_mission_index")
    @ApiOperation(value = "查询团长任务首页")
    public Map queryMissionIndex() {
        Buyer buyer = UserContext.getBuyer();
        return distributionMissionService.queryMissionIndex(buyer.getUid());
    }


}
