package com.enation.app.javashop.framework.util;


import java.util.*;
import java.util.stream.Collectors;


/**
 *  数据集辅助类
 **/
public final class IDataUtils {

	private IDataUtils(){}

	public static boolean isEmpty(Map map){
		return map == null || map.isEmpty();
	}
	
	public static boolean isEmpty(List list){
		return list == null || list.isEmpty();
	}

	public static boolean isEmpty(Set set){
		return set == null || set.isEmpty();
	}

	public static boolean isEmpty(String[] array){
		return array == null || array.length == 0;
	}

	public static boolean isNotEmpty(Map map){
		return map != null && !map.isEmpty();
	}
	
	public static boolean isNotEmpty(List list){
		return list != null && !list.isEmpty();
	}

	public static boolean isNotEmpty(Set set){
		return set != null && !set.isEmpty();
	}

	public static boolean isNotEmpty(String[] strArry){
		return strArry != null && strArry.length != 0;
	}

	/**
	 * List集合去重复
	 */
	public static void removeDuplicate(List list) {
		Set set = new LinkedHashSet(list.size());
		set.addAll(list);
		list.clear();
		list.addAll(set);
	}

	/**
	 * List集合去重复
	 */
	public static List distinct(List list) {
		return (List) list.stream().distinct().collect(Collectors.toList());
	}


	/**
	 * 将字符串数组转换成Integer的list
	 */
	public static List<Integer> convertIntegerList(String [] strArray){
		if(IDataUtils.isEmpty(strArray)){
			return new ArrayList<Integer>();
		}

		List<Integer> list = new ArrayList<>(strArray.length);
		Arrays.asList(strArray).forEach(str ->
			list.add(Integer.valueOf(str))
		);
		return list;
	}


}
