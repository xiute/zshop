package com.enation.app.javashop.framework.redis.listener;

import com.enation.app.javashop.framework.util.DictUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


/**
 * Redis字典监听器
 * @author ChienSun
 * @date 2019/1/3
 */
@Service
public class RedisDictMonitor implements RedisMonitor{

    private static final Logger log = LoggerFactory.getLogger(RedisDictMonitor. class);

    @Override
    public void receiveMessage(String message){
        log.debug("receiveMessage - {}", message);
        // 初始化字典
        DictUtils.initDictLocalCache();
    }

}
