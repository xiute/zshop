package com.enation.app.javashop.framework.util;

import org.elasticsearch.common.geo.GeoPoint;
import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GeodeticCurve;
import org.gavaghan.geodesy.GlobalCoordinates;

/**
 * @author JFeng
 * @date 2019/6/10 23:24
 */
public class GeoUtils {
    //https://restapi.amap.com/v4/direction/bicycling?origin=116.434307,39.90909&destination=116.434307,39.90909&key=<用户的key>
    public static final String GAODE_KEY="716a1b9c5ab75fc7fcd2ce1c58d3dbc3";
    public static final String DISTANCE_URL="https://restapi.amap.com/v3/distance";


    public static String getBaseLocation(GeoPoint originPoint,GeoPoint destinationPoint,Integer type) {
        StringBuilder sb = new StringBuilder(DISTANCE_URL);
        sb.append("?key=" + GAODE_KEY)
                .append("&origins=" + originPoint.getLon()+","+originPoint.getLat())
                .append("&destination=" + destinationPoint.getLon()+","+destinationPoint.getLat())
                .append("&type=" + type)
                .append("&output=JSON");
        String resultString = HttpUtils.doGet(sb.toString());
        return resultString;
    }

    public static void main(String[] args) {
        // GeoPoint origin = new GeoPoint(39.989643,116.481028 );
        // GeoPoint destination = new GeoPoint(39.999538,116.465063);
        // String baseLocation = getBaseLocation(origin, destination, 0);
        // System.out.println(baseLocation);

        GlobalCoordinates source = new GlobalCoordinates(39.989643,116.481028);
        GlobalCoordinates target = new GlobalCoordinates(39.999538,116.465063);
        // 高德使用 WGS－84, Ellipsoid.WGS84
        double meter2 = getDistanceMeter(source, target);
        System.out.println("WGS84坐标系计算结果："+meter2 + "米");
    }


    public static double getDistanceMeter(GlobalCoordinates gpsFrom, GlobalCoordinates gpsTo)
    {
        //创建GeodeticCalculator，调用计算方法，传入坐标系、经纬度用于计算距离
        GeodeticCurve geoCurve = new GeodeticCalculator().calculateGeodeticCurve( Ellipsoid.WGS84, gpsFrom, gpsTo);

        return geoCurve.getEllipsoidalDistance();
    }
}
