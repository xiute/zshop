package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.shop.model.vo.ShipTemplateChangeMsg;
import com.enation.app.javashop.core.shop.model.vo.ShopChangeMsg;

/**
 * 店铺变更
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:26:32
 */
public interface ShipTemplateChangeEvent {

    /**
     * 同城配送模板修改消息
     *
     * @param shipTemplateChangeMsg 同城配送模板修改消息
     */
    void shipTemplateChange(ShipTemplateChangeMsg shipTemplateChangeMsg);
}
