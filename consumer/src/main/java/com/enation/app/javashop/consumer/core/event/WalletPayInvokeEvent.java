package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.payment.model.vo.PayBill;

/**
 * @author 王志杨
 * @since 2020年10月17日 10:37:25
 * 调用钱包支付事件
 */
public interface WalletPayInvokeEvent {

    /**
     * 调用钱包支付需要执行的方法
     * @param paymentbill
     */
    void walletPayInvoke(PaymentBillDO paymentbill);
}
