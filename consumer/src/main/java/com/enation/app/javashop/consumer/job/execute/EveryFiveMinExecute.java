package com.enation.app.javashop.consumer.job.execute;

/**
 * 每5分钟执行
 */
public interface EveryFiveMinExecute {
    /**
     * 每5分钟执行
     */
    void everyFiveMin();
}
