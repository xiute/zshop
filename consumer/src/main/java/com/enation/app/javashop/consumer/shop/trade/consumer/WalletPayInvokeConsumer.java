package com.enation.app.javashop.consumer.shop.trade.consumer;

import com.enation.app.javashop.consumer.core.event.WalletPayInvokeEvent;
import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.payment.model.enums.ClientType;
import com.enation.app.javashop.core.payment.model.enums.TradeType;
import com.enation.app.javashop.core.payment.model.vo.PayBill;
import com.enation.app.javashop.core.payment.plugin.walletpay.WalletPayPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author 王志杨
 * @since 2020年10月17日 10:37:25
 * 调用钱包支付事件实现
 */
@Component
public class WalletPayInvokeConsumer implements WalletPayInvokeEvent {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private WalletPayPlugin walletPayPlugin;

    @Override
    public void walletPayInvoke(PaymentBillDO paymentBill) {
        try {
            PayBill payBill = new PayBill();
            payBill.setSn(paymentBill.getSn());
            payBill.setBillSn(paymentBill.getOutTradeNo());
            payBill.setTradeType(TradeType.valueOf(paymentBill.getTradeType()));
            payBill.setClientType(ClientType.MINI);
            payBill.setOrderPrice(paymentBill.getTradePrice());
            payBill.setPluginId(paymentBill.getPaymentPluginId());
            walletPayPlugin.pay(payBill);
        } catch (Exception e) {
            this.logger.error("钱包支付调用失败", e);
        }
    }
}
