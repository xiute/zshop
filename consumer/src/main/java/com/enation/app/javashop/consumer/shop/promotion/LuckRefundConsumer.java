package com.enation.app.javashop.consumer.shop.promotion;

import com.enation.app.javashop.consumer.core.event.RefundStatusChangeEvent;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.base.message.RefundChangeMsg;
import com.enation.app.javashop.core.promotion.luck.service.LuckRecordManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

/**
 * @Author: zhou
 * @Date: 2021/3/2
 * @Description: 订单退款成功之后退回抽奖奖品
 */
public class LuckRefundConsumer implements RefundStatusChangeEvent {

    @Autowired
    private LuckRecordManager luckRecordManager;

    @Override
    public void refund(RefundChangeMsg refundChangeMsg) {
        if (RefundStatusEnum.PASS.equals(refundChangeMsg.getRefundStatusEnum()) && refundChangeMsg.getRefund() != null) {
            // 下单之后抽过奖，才需要调用方法返回奖品
            luckRecordManager.LuckRefund(Arrays.asList(refundChangeMsg.getRefund()));

        }

    }

}
