package com.enation.app.javashop.consumer.shop.distribution;

import com.enation.app.javashop.consumer.core.event.OrderStatusChangeEvent;
import com.enation.app.javashop.consumer.core.event.RefundStatusChangeEvent;
import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.base.message.RefundChangeMsg;
import com.enation.app.javashop.core.distribution.model.dos.DistributionDO;
import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionDetailDO;
import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionScheduleDO;
import com.enation.app.javashop.core.distribution.model.vo.DistributionMissionVO;
import com.enation.app.javashop.core.distribution.service.DistributionManager;
import com.enation.app.javashop.core.distribution.service.DistributionMissionDetailService;
import com.enation.app.javashop.core.distribution.service.DistributionMissionScheduleService;
import com.enation.app.javashop.core.distribution.service.DistributionMissionService;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.service.LeaderManager;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.core.statistics.util.DateUtil;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 团长任务进度消费者
 * @author: 孙建
 */
@Service
public class DistributionMissionConsumer implements OrderStatusChangeEvent, RefundStatusChangeEvent {

    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private OrderQueryManager orderQueryManager;
    @Autowired
    private DistributionManager distributionManager;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private LeaderManager leaderManager;
    @Autowired
    private DistributionMissionService distributionMissionService;
    @Autowired
    private DistributionMissionDetailService distributionMissionDetailService;
    @Autowired
    private DistributionMissionScheduleService distributionMissionScheduleService;


    /**
     * 订单支付
     */
    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        OrderDO orderDO = orderMessage.getOrderDO();
        if(orderMessage.getNewStatus().equals(OrderStatusEnum.PAID_OFF)){
            Integer memberId = orderDO.getMemberId();
            DistributionDO currMemberDistributionDO = distributionManager.getDistributorByMemberId(memberId);
            if(currMemberDistributionDO != null){
                Integer lv1MemberId = currMemberDistributionDO.getMemberIdLv1();
                DistributionDO lv1DistributionDO = distributionManager.getDistributorByMemberId(lv1MemberId);
                if(lv1DistributionDO == null || lv1DistributionDO.getAuditStatus() != 2 || lv1DistributionDO.getStatus() != 1){
                    return;
                }
                DistributionMissionVO distributionMission = distributionMissionService.queryLatestMission();
                if(distributionMission != null){
                    Integer missionId = distributionMission.getId();

                    // 查询团长会员信息
                    Member lv1Member = memberManager.getMemberById(lv1MemberId);

                    // 查询任务明细
                    List<DistributionMissionDetailDO> distributionMissionDetailList = distributionMissionDetailService.queryDistributionMissionDetailByMissionId(missionId);
                    for (DistributionMissionDetailDO distributionMissionDetailDO : distributionMissionDetailList) {
                        Integer missionType = distributionMissionDetailDO.getMissionType();
                        Integer missionDetailId = distributionMissionDetailDO.getId();
                        if(missionType == 0){
                            // 获取今天日期
                            String todayDate = DateUtil.toString(new Date(), "MM-dd");
                            // 查询每日任务 如果没有就新建
                            DistributionMissionScheduleDO dayMissionScheduleDO = distributionMissionScheduleService.queryDistributionMissionSchedule(lv1MemberId,missionId, missionDetailId, missionType, todayDate);
                            if(dayMissionScheduleDO == null){
                                dayMissionScheduleDO = distributionMissionScheduleService.createMissionSchedule(lv1Member, distributionMission, distributionMissionDetailDO, todayDate);
                            }

                            // 累计人数或者累计金额
                            distributionMissionScheduleService.increase(distributionMission, distributionMissionDetailDO, dayMissionScheduleDO, orderDO);
                        }else{
                            // 获取本月
                            String thisMonth = DateUtil.toString(new Date(), "yy-MM");
                            // 查询每月任务 如果没有就新建
                            DistributionMissionScheduleDO monthMissionScheduleDO = distributionMissionScheduleService.queryDistributionMissionSchedule(lv1MemberId,missionId, missionDetailId, missionType, thisMonth);
                            if(monthMissionScheduleDO == null){
                                monthMissionScheduleDO = distributionMissionScheduleService.createMissionSchedule(lv1Member, distributionMission, distributionMissionDetailDO, thisMonth);
                            }

                            // 累计人数或者累计金额
                            distributionMissionScheduleService.increase(distributionMission, distributionMissionDetailDO, monthMissionScheduleDO, orderDO);
                        }
                    }
                }
            }
        }
    }

    /**
     * 退款
     */
    @Override
    public void refund(RefundChangeMsg refundChangeMsg) {
        RefundDO refund = refundChangeMsg.getRefund();
        RefundStatusEnum refundStatusEnum = refundChangeMsg.getRefundStatusEnum();

        OrderDetailDTO orderDetailDTO = orderQueryManager.getModel(refund.getOrderSn());
        OrderDO orderDO = orderQueryManager.getModel(orderDetailDTO.getOrderId());

        // 申请退款的订单就需要减一
        if (refundStatusEnum.equals(RefundStatusEnum.APPLY)) {
            Integer memberId = orderDO.getMemberId();
            DistributionDO currMemberDistributionDO = distributionManager.getDistributorByMemberId(memberId);
            if(currMemberDistributionDO != null){
                Integer lv1MemberId = currMemberDistributionDO.getMemberIdLv1();
                DistributionDO lv1DistributionDO = distributionManager.getDistributorByMemberId(lv1MemberId);
                if(lv1DistributionDO == null || lv1DistributionDO.getAuditStatus() != 2 || lv1DistributionDO.getStatus() != 1){
                    return;
                }
                DistributionMissionVO distributionMission = distributionMissionService.queryLatestMission();
                if(distributionMission != null){
                    Integer missionId = distributionMission.getId();

                    // 查询任务明细
                    List<DistributionMissionDetailDO> distributionMissionDetailList = distributionMissionDetailService.queryDistributionMissionDetailByMissionId(missionId);
                    for (DistributionMissionDetailDO distributionMissionDetailDO : distributionMissionDetailList) {
                        Integer missionType = distributionMissionDetailDO.getMissionType();
                        Integer missionDetailId = distributionMissionDetailDO.getId();
                        if(missionType == 0){
                            // 获取今天日期
                            String todayDate = DateUtil.toString(new Date(), "MM-dd");
                            // 查询每日任务 如果没有就新建
                            DistributionMissionScheduleDO dayMissionScheduleDO = distributionMissionScheduleService.queryDistributionMissionSchedule(lv1MemberId,missionId, missionDetailId, missionType, todayDate);
                            if(dayMissionScheduleDO == null){
                                continue;
                            }
                            // 减少人数或者减少金额
                            distributionMissionScheduleService.decrease(distributionMission, distributionMissionDetailDO, dayMissionScheduleDO, orderDO);
                        }else{
                            // 获取本月
                            String thisMonth = DateUtil.toString(new Date(), "yy-MM");
                            // 查询每月任务 如果没有就新建
                            DistributionMissionScheduleDO monthMissionScheduleDO = distributionMissionScheduleService.queryDistributionMissionSchedule(lv1MemberId,missionId, missionDetailId, missionType, thisMonth);
                            if(monthMissionScheduleDO == null){
                                continue;
                            }
                            // 减少人数或者减少金额
                            distributionMissionScheduleService.decrease(distributionMission, distributionMissionDetailDO, monthMissionScheduleDO, orderDO);
                        }
                    }
                }
            }
        }
    }


}
