package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.core.aftersale.model.vo.ClaimsChangeMsg;

/**
 * @author JFeng
 * @date 2020/9/24 22:56
 */
public interface ClaimsChangeEvent {

    public void claimsChange(ClaimsChangeMsg claimsChangeMsg);
}
