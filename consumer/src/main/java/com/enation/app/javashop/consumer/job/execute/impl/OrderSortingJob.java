package com.enation.app.javashop.consumer.job.execute.impl;

import com.enation.app.javashop.consumer.job.execute.EveryDayExecute;
import com.enation.app.javashop.consumer.job.execute.EveryFiveMinExecute;
import com.enation.app.javashop.core.trade.order.service.OrderTaskManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * 订单分拣定时任务
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-05 下午2:11
 */
@Component
public class OrderSortingJob implements EveryDayExecute {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private DaoSupport daoSupport;

    /**
     * 每晚00:01执行
     */
    @Override
    public void everyDay() {
        try {
            String lastDay = LocalDate.now().plusDays(-1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            this.daoSupport.execute("call pro_sorting_no_gen(?);" ,lastDay);
        } catch (Exception e) {
            logger.error("订单分拣序列号生成出错", e);
        }

    }
}
