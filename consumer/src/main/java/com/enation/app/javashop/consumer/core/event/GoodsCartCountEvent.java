package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.base.message.GoodsCartCountMsg;

/**
 * @Author: zhou
 * @Date: 2020/9/11
 * @Description: 购物车商品加购统计
 */
public interface GoodsCartCountEvent {
    void goodsCartCount(GoodsCartCountMsg goodsCartCountMsg);
}
