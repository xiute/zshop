package com.enation.app.javashop.consumer.core.event;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/12/10
 * @Description: 缓存店铺中关联所有商品的标签
 */
public interface ShopTagChangeEvent {
    /**
     * 缓存店铺中关联所有商品的标签
     * @param sellerIds 店铺Id
     */
    void shopTagChangeCache(List<Integer> sellerIds);
}
