package com.enation.app.javashop.consumer.core.receiver;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.enation.app.javashop.consumer.core.event.GoodsChangeEvent;
import com.enation.app.javashop.consumer.core.event.GoodsSkuChangeEvent;
import com.enation.app.javashop.core.base.message.GoodsChangeMsg;
import com.enation.app.javashop.core.base.message.GoodsSkuChangeMsg;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 商品sku 变化消费者
 * 
 * @author JFENG
 */
@Component
public class GoodsSkuChangeReceiver {

	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired(required = false)
	private List<GoodsSkuChangeEvent> events;

	/**
	 * 商品变化
	 * 
	 * @param goodsSkuChangeMsg
	 */
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = AmqpExchange.GOODS_SKU_CHANGE + "_QUEUE"),
			exchange = @Exchange(value = AmqpExchange.GOODS_SKU_CHANGE, type = ExchangeTypes.FANOUT)
	))
	public void goodsChange(GoodsSkuChangeMsg goodsSkuChangeMsg) {
		if (events != null) {
			for (GoodsSkuChangeEvent event : events) {
				try {
					event.goodsSkuChange(goodsSkuChangeMsg);
				} catch (Exception e) {
					logger.error("处理商品变化消息出错", e);
				}
			}
		}

	}
}
