package com.enation.app.javashop.consumer.shop.promotion;

import com.enation.app.javashop.consumer.core.event.RefundStatusChangeEvent;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.base.message.RefundChangeMsg;
import com.enation.app.javashop.core.client.distribution.DistributionOrderClient;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanOrderManager;
import com.enation.app.javashop.framework.redis.transactional.RedisTransactional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 社区团购订单退款
 *
 */

@Component
public class ShetuanRefundConsumer implements RefundStatusChangeEvent{
    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private ShetuanOrderManager shetuanOrderManager;


    @Override
    @RedisTransactional(acquireTimeout = 1,lockTimeout = 30,lockName = "orderStatistic_",value = "#refundChangeMsg.refund.id")
    public void refund(RefundChangeMsg refundChangeMsg) {
        try {
            if (refundChangeMsg.getRefundStatusEnum().equals(RefundStatusEnum.PASS)) {
                // 退货时算好各个级别需要退的返利金额 放入数据库
                this.shetuanOrderManager.calReturnCommission(refundChangeMsg.getRefund().getOrderSn(), refundChangeMsg.getRefund().getRefundPrice());
           }
        } catch (Exception e) {
            logger.error("订单退款计算返利异常：",e);
        }
    }

}
