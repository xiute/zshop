package com.enation.app.javashop.consumer.shop.message;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dag.eagleshop.core.delivery.model.dto.AddOrderDTO;
import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import com.dag.eagleshop.core.delivery.service.DeliveryManager;
import com.enation.app.javashop.consumer.core.event.ClaimsChangeEvent;
import com.enation.app.javashop.consumer.core.event.OrderStatusChangeEvent;
import com.enation.app.javashop.consumer.core.event.RefundStatusChangeEvent;
import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.aftersale.model.dto.ClaimSkusDTO;
import com.enation.app.javashop.core.aftersale.model.enums.ClaimsStatusEnum;
import com.enation.app.javashop.core.aftersale.model.enums.ClaimsTypeEnum;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.aftersale.model.enums.RefuseTypeEnum;
import com.enation.app.javashop.core.aftersale.model.vo.ClaimsChangeMsg;
import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.base.message.RefundChangeMsg;
import com.enation.app.javashop.core.geo.service.TencentManager;
import com.enation.app.javashop.core.goodssearch.service.impl.GoodsShipCalculator;
import com.enation.app.javashop.core.member.model.dos.LeaderDO;
import com.enation.app.javashop.core.member.model.dos.MemberAddress;
import com.enation.app.javashop.core.member.service.LeaderManager;
import com.enation.app.javashop.core.member.service.MemberAddressManager;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.core.pagedata.model.PageData;
import com.enation.app.javashop.core.pagedata.service.PageDataManager;
import com.enation.app.javashop.core.shop.model.dos.ShopDetailDO;
import com.enation.app.javashop.core.shop.model.vo.ShopListVO;
import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.core.shop.service.ShopManager;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.OrderTypeEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 下配送订单
 */
@Component
public class DeliveryOrderConsumer implements OrderStatusChangeEvent, ClaimsChangeEvent, RefundStatusChangeEvent {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private ShopManager shopManager;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private DeliveryManager deliveryManager;
    @Autowired
    private TencentManager tencentManager;
    @Autowired
    private MemberAddressManager memberAddressManager;
    @Autowired
    private LeaderManager leaderManager;
    @Autowired
    private OrderQueryManager orderQueryManager;
    @Autowired
    private PageDataManager pageDataManager;
    @Autowired
    private GoodsShipCalculator goodsShipCalculator;


    private static final int SHOP_ID = 102; // 102  15

    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {

        // 配送系统已关闭 这套代码取消 防止IO占用
        if(1 == 1){
            return;
        }

       // 测试环境不开放
//       if(!EnvironmentUtils.isProd()){
//           return;
//       }

        OrderDO orderDO = orderMessage.getOrderDO();
        List<OrderSkuVO> skuVOList = JsonUtil.jsonToList(orderDO.getItemsJson(), OrderSkuVO.class);

        // 支付成功 根据情况下单给配送系统
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.PAID_OFF.name())&& !OrderTypeEnum.fuwu.name().equals(orderDO.getOrderType())) {
            String shippingType = orderDO.getShippingType();
            Integer leaderId = orderDO.getLeaderId();
            Integer sellerId = orderDO.getSellerId();
            Integer addressId = orderDO.getAddressId();
            MemberAddress memberAddress  = memberAddressManager.getModel(addressId);

            // 收货人信息
            String consigneeCustomerName = orderDO.getShipName();
            String consigneeCustomerMobile = orderDO.getShipMobile();
            String siteManName = null;
            // 判断订单是否是自提点订单
            if(shippingType.equals(ShipTypeEnum.SELF.name())){

                // 将订单信息改为自提点信息
                LeaderDO leaderDO = leaderManager.getById(leaderId);
                consigneeCustomerName = leaderDO.getSiteName();
                consigneeCustomerMobile = leaderDO.getLeaderMobile();
                siteManName = leaderDO.getLeaderName();
                orderDO.setShipProvince(leaderDO.getProvince());
                orderDO.setShipCity(leaderDO.getCity());
                orderDO.setShipCounty(leaderDO.getCounty());
                orderDO.setShipTown(leaderDO.getTown());
                orderDO.setShipAddr(leaderDO.getAddress());

                memberAddress = new MemberAddress();
                // 经度
                memberAddress.setShipLng(leaderDO.getLng());
                // 维度
                memberAddress.setShipLat(leaderDO.getLat());
            }
            // 查询店铺信息
            ShopVO shop = shopManager.getShop(sellerId);
            ShopDetailDO shopDetail = shopManager.getShopDetail(sellerId);

            AddOrderDTO addOrderDTO = new AddOrderDTO();
            addOrderDTO.setUpsBillId(String.valueOf(orderDO.getOrderId()));
            addOrderDTO.setUpsBillNo(orderDO.getSn());
            addOrderDTO.setOrderType(1);
            // 发货地址 (从仓库发货)
            getDefaultConsignerInfo(addOrderDTO);

            // 衢山镇 本地生活标识
            addOrderDTO.setLocalLifeShopFlag(0);
            // 下单的地址是衢山镇的 判断是不是从本地生活店铺中下的单子
            String shipTown = orderDO.getShipTown();
            String shipAddr = orderDO.getShipAddr();
            if(shipTown.contains("衢山") || shipAddr.contains("衢山")){
                // 查询本地店铺
                PageData pageData = pageDataManager.getLocalLifeShop("APP", 2, "4", "衢山镇");
                if(pageData != null){
                    String pageDataStr = pageData.getPageData();
                    JSONArray pageDataArray = JSON.parseArray(pageDataStr);
                    for (Object object : pageDataArray) {
                        JSONObject jsonObject = JSON.parseObject(object.toString());
                        // 查询页面是不是发现页
                        Integer tplId = jsonObject.getInteger("tpl_id");
                        // 发现页组件
                        if(tplId == 75){
                            boolean dataSource = jsonObject.containsKey("dataSource");
                            // 取出配置的本地生活店铺id
                            if(dataSource){
                                JSONObject data = jsonObject.getJSONObject("dataSource").getJSONObject("data");
                                if(!CollectionUtils.isEmpty(data)){
                                    logger.info("取出配置的本地生活店铺id"+data);
                                    JSONArray shopIds = data.getJSONArray("shopIds");
                                    if(!CollectionUtils.isEmpty(shopIds)){
                                        // 下单店铺是本地生活店铺  发货地址是店铺地址
                                        boolean contains = shopIds.contains(sellerId);
                                        if(contains){
                                            addOrderDTO.setConsignerCustomerName(shop.getShopName());
                                            addOrderDTO.setConsignerCustomerMobile(shop.getLinkPhone());
                                            addOrderDTO.setConsignerName(shop.getLinkName());
                                            addOrderDTO.setConsignerPhone(shop.getLinkPhone());
                                            addOrderDTO.setConsignerProvince(shop.getShopProvince());
                                            addOrderDTO.setConsignerCity(shop.getShopCity());
                                            addOrderDTO.setConsignerDistrict(shop.getShopCounty());
                                            addOrderDTO.setConsignerStreet(shop.getShopTown());
                                            addOrderDTO.setConsignerAddress(shop.getShopAdd());
                                            addOrderDTO.setConsignerLng(shop.getShopLng());
                                            addOrderDTO.setConsignerLat(shop.getShopLat());
                                            addOrderDTO.setLocalLifeShopFlag(1);

                                            ShopListVO shopListVO = new ShopListVO();
                                            BeanUtil.copyProperties(shop,shopListVO);
                                            // 计算收货地址和店铺地址距离
                                            double distanceMeter =( memberAddress.getShipLng()==null || shop.getShopLng()==null )? 0:
                                                    GeoUtils.getDistanceMeter(new GlobalCoordinates(memberAddress.getShipLat(), memberAddress.getShipLng()),
                                                            new GlobalCoordinates(shop.getShopLat(), shop.getShopLng()));
                                            // 距离单位换算
                                            BigDecimal distanceKiloMeter = BigDecimal.valueOf(distanceMeter / 1000).setScale(2, RoundingMode.HALF_UP);
                                            shopListVO.setDistance(distanceKiloMeter.doubleValue());

                                            goodsShipCalculator.calculateShop(shopListVO);
                                            String shipTimeShow = shopListVO.getShipTimeShow();
                                            addOrderDTO.setLocalDeliveryTime(shipTimeShow.substring(0, shipTimeShow.indexOf("分")));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            addOrderDTO.setConsigneeCustomerName(consigneeCustomerName);
            addOrderDTO.setConsigneeCustomerMobile(consigneeCustomerMobile);
            addOrderDTO.setConsigneeName(orderDO.getShipName());
            addOrderDTO.setConsigneePhone(orderDO.getShipMobile());
            addOrderDTO.setConsigneeProvince(orderDO.getShipProvince());
            addOrderDTO.setConsigneeCity(orderDO.getShipCity());
            addOrderDTO.setConsigneeDistrict(orderDO.getShipCounty());
            addOrderDTO.setConsigneeStreet(orderDO.getShipTown());
            addOrderDTO.setConsigneeAddress(orderDO.getShipAddr());
            addOrderDTO.setConsigneeLng(memberAddress.getShipLng());
            addOrderDTO.setConsigneeLat(memberAddress.getShipLat());

            if(skuVOList != null && skuVOList.size() > 0){
                addOrderDTO.setGoodsName(getGoodsName(skuVOList));
            }
            JSONArray itemsJsonArr = JSON.parseArray(orderDO.getItemsJson());
            addOrderDTO.setGoodsPiece(itemsJsonArr.size());
            addOrderDTO.setGoodsType("");
            addOrderDTO.setGoodsVolume(BigDecimal.ZERO);
            addOrderDTO.setGoodsWeight(BigDecimal.ZERO);
            addOrderDTO.setTotalCost(BigDecimal.ZERO);
            addOrderDTO.setCreaterName(shop.getShopName());
            addOrderDTO.setDistance(this.countDistance(shopDetail, memberAddress));
            addOrderDTO.setIsAppointment(1);
            addOrderDTO.setPlanDeliveryTime(this.getPlanDeliveryTime());
            addOrderDTO.setIsSelfTake(shippingType.equals(ShipTypeEnum.SELF.name()) ? 1 : 0);
            addOrderDTO.setSiteManName(siteManName);
            addOrderDTO.setLabels(JSON.toJSON(getLabels()).toString());

            logger.info("【配送订单请求报文】:"+JSON.toJSON(addOrderDTO).toString());

            if(!EnvironmentUtils.isProd()){
                addOrderDTO.setUpsBillNo("T" + addOrderDTO.getUpsBillNo());
            }
            JsonBean jsonBean = deliveryManager.addOrder(addOrderDTO);
            if(jsonBean.isSuccess()){
                logger.debug("配送订单下单成功");
            }else{
                logger.debug("配送订单下单失败" + JSON.toJSON(jsonBean).toString());
            }
            // 订单取消 同步到骑手app
        }else if(orderMessage.getNewStatus().name().equals(OrderStatusEnum.CANCELLED.name())){
            /*CancelOrderDTO cancelOrderDTO = new CancelOrderDTO();
            // 取消的订单号
            List<String> list = new ArrayList<>();
            list.add(orderDO.getSn());
            cancelOrderDTO.setIds(list);
            //取消原因
            cancelOrderDTO.setReason(orderDO.getCancelReason());
            cancelOrderDTO.setUpdaterId(orderDO.getSellerId().toString());
            cancelOrderDTO.setUpdaterName(orderDO.getSellerName());
            deliveryManager.cancelOrder(cancelOrderDTO);*/
        }
    }

    public static void main(String[] args) {

        String a = "衢山镇幸福中路17";
        String b = "";
        System.out.println();
        System.out.println(b.contains("衢山") || a.contains("衢山"));
    }

    /**
     * 理赔单审核
     */
    @Override
    public void claimsChange(ClaimsChangeMsg claimsChangeMsg) {
        // 测试环境不开放
//        if(!EnvironmentUtils.isProd()){
//            return;
//        }

        if(claimsChangeMsg.getAuditEnum().equals(ClaimsStatusEnum.AUDIT_SUCCESS.value())){
            logger.info("理赔审核通过-准备下单到配送系统：" + JSON.toJSON(claimsChangeMsg).toString());
            List<ClaimsDO> claimsDOList = claimsChangeMsg.getClaimsDOList();
            // 查询订单
            String orderSn = claimsDOList.get(0).getOrderSn();
            OrderDetailDTO orderDetailDTO = orderQueryManager.getModel(orderSn);
            OrderDO orderDO = orderQueryManager.getModel(orderDetailDTO.getOrderId());
            List<OrderSkuVO> skuVOList = JsonUtil.jsonToList(orderDO.getItemsJson(), OrderSkuVO.class);

            for (ClaimsDO claimsDO : claimsDOList) {
                if(claimsDO.getClaimType().equals(ClaimsTypeEnum.COMPENSATION_GOODS.value())
                        || claimsDO.getClaimType().equals(ClaimsTypeEnum.FREE_GIFT.value())  ){
                    // 下单
                    String shippingType = orderDO.getShippingType();
                    Integer leaderId = orderDO.getLeaderId();
                    Integer sellerId = orderDO.getSellerId();
                    Integer addressId = orderDO.getAddressId();
                    MemberAddress memberAddress = memberAddressManager.getModel(addressId);

                    // 收货人信息
                    String consigneeCustomerName = orderDO.getShipName();
                    String consigneeCustomerMobile = orderDO.getShipMobile();
                    String siteManName = null;
                    // 判断订单是否是自提点订单
                    if (shippingType.equals(ShipTypeEnum.SELF.name())) {

                        // 将订单信息改为自提点信息
                        LeaderDO leaderDO = leaderManager.getById(leaderId);
                        consigneeCustomerName = leaderDO.getSiteName();
                        consigneeCustomerMobile = leaderDO.getLeaderMobile();
                        siteManName = leaderDO.getLeaderName();
                        orderDO.setShipProvince(leaderDO.getProvince());
                        orderDO.setShipCity(leaderDO.getCity());
                        orderDO.setShipCounty(leaderDO.getCounty());
                        orderDO.setShipTown(leaderDO.getTown());
                        orderDO.setShipAddr(leaderDO.getAddress());

                        memberAddress = new MemberAddress();
                        // 经度
                        memberAddress.setShipLng(leaderDO.getLng());
                        // 维度
                        memberAddress.setShipLat(leaderDO.getLat());
                    }
                    // 查询店铺信息
                    ShopVO shop = shopManager.getShop(sellerId);
                    ShopDetailDO shopDetail = shopManager.getShopDetail(sellerId);

                    AddOrderDTO addOrderDTO = new AddOrderDTO();
                    addOrderDTO.setUpsBillId(String.valueOf(claimsDO.getClaimId()));
                    addOrderDTO.setUpsBillNo(claimsDO.getClaimSn());
                    addOrderDTO.setOrderType(2);
                    // 初始化默认信息
                    getDefaultConsignerInfo(addOrderDTO);

                    addOrderDTO.setConsigneeCustomerName(consigneeCustomerName);
                    addOrderDTO.setConsigneeCustomerMobile(consigneeCustomerMobile);
                    addOrderDTO.setConsigneeName(orderDO.getShipName());
                    addOrderDTO.setConsigneePhone(orderDO.getShipMobile());
                    addOrderDTO.setConsigneeProvince(orderDO.getShipProvince());
                    addOrderDTO.setConsigneeCity(orderDO.getShipCity());
                    addOrderDTO.setConsigneeDistrict(orderDO.getShipCounty());
                    addOrderDTO.setConsigneeStreet(orderDO.getShipTown());
                    addOrderDTO.setConsigneeAddress(orderDO.getShipAddr());
                    addOrderDTO.setConsigneeLng(memberAddress.getShipLng());
                    addOrderDTO.setConsigneeLat(memberAddress.getShipLat());

                    if (skuVOList != null && skuVOList.size() > 0) {
                        addOrderDTO.setGoodsName(getGoodsName(claimsDO.getClaimSkus()));
                    }
                    JSONArray itemsJsonArr = JSON.parseArray(orderDO.getItemsJson());
                    addOrderDTO.setGoodsPiece(itemsJsonArr.size());
                    addOrderDTO.setGoodsType("");
                    addOrderDTO.setGoodsVolume(BigDecimal.ZERO);
                    addOrderDTO.setGoodsWeight(BigDecimal.ZERO);
                    addOrderDTO.setTotalCost(BigDecimal.ZERO);
                    addOrderDTO.setCreaterName(shop.getShopName());
                    addOrderDTO.setDistance(this.countDistance(shopDetail, memberAddress));
                    addOrderDTO.setIsAppointment(1);
                    addOrderDTO.setPlanDeliveryTime(this.getPlanDeliveryTime());
                    addOrderDTO.setIsSelfTake(shippingType.equals(ShipTypeEnum.SELF.name()) ? 1 : 0);
                    addOrderDTO.setSiteManName(siteManName);
                    addOrderDTO.setLabels(JSON.toJSON(getLabels()).toString());
                    if(!EnvironmentUtils.isProd()){
                        addOrderDTO.setUpsBillNo("T" + addOrderDTO.getUpsBillNo());
                    }
                    JsonBean jsonBean = deliveryManager.addOrder(addOrderDTO);
                    if (jsonBean.isSuccess()) {
                        logger.debug("配送订单下单成功");
                    } else {
                        logger.debug("配送订单下单失败" + JSON.toJSON(jsonBean).toString());
                    }
                }
            }
        }
    }


    /**
     * 退货审核通过
     */
    @Override
    public void refund(RefundChangeMsg refundChangeMsg) {
        // 测试环境不开放
//        if(!EnvironmentUtils.isProd()){
//            return;
//        }

        if(refundChangeMsg.getRefund().getRefundStatus().equals(RefundStatusEnum.PASS.value())){
            logger.info("退货审核通过-准备下单到配送系统：" + JSON.toJSON(refundChangeMsg).toString());
            RefundDO refundDO = refundChangeMsg.getRefund();

            if(refundDO.getRefuseType().equals(RefuseTypeEnum.RETURN_GOODS.value())){
                // 查询订单
                String orderSn = refundDO.getOrderSn();
                OrderDetailDTO orderDetailDTO = orderQueryManager.getModel(orderSn);
                OrderDO orderDO = orderQueryManager.getModel(orderDetailDTO.getOrderId());
                List<OrderSkuVO> skuVOList = JsonUtil.jsonToList(orderDO.getItemsJson(), OrderSkuVO.class);

                // 下单
                String shippingType = orderDO.getShippingType();
                Integer leaderId = orderDO.getLeaderId();
                Integer sellerId = orderDO.getSellerId();
                Integer addressId = orderDO.getAddressId();
                MemberAddress memberAddress = memberAddressManager.getModel(addressId);

                //  发货人信息
                String consignerCustomerName = orderDO.getShipName();
                String consignerCustomerMobile = orderDO.getShipMobile();
                String siteManName = null;
                // 判断订单是否是自提点订单
                if (shippingType.equals(ShipTypeEnum.SELF.name())) {
                    // 将订单信息改为自提点信息
                    LeaderDO leaderDO = leaderManager.getById(leaderId);
                    consignerCustomerName = leaderDO.getSiteName();
                    consignerCustomerMobile = leaderDO.getLeaderMobile();
                    siteManName = leaderDO.getLeaderName();
                    orderDO.setShipProvince(leaderDO.getProvince());
                    orderDO.setShipCity(leaderDO.getCity());
                    orderDO.setShipCounty(leaderDO.getCounty());
                    orderDO.setShipTown(leaderDO.getTown());
                    orderDO.setShipAddr(leaderDO.getAddress());
                    memberAddress = new MemberAddress();
                    // 经度
                    memberAddress.setShipLng(leaderDO.getLng());
                    // 维度
                    memberAddress.setShipLat(leaderDO.getLat());
                }
                // 查询店铺信息
                ShopDetailDO shopDetail = shopManager.getShopDetail(sellerId);

                AddOrderDTO addOrderDTO = new AddOrderDTO();
                addOrderDTO.setUpsBillId(String.valueOf(refundDO.getId()));
                addOrderDTO.setUpsBillNo(refundDO.getSn());
                addOrderDTO.setOrderType(3);

                // 发货人
                addOrderDTO.setConsignerCustomerName(consignerCustomerName);
                addOrderDTO.setConsignerCustomerMobile(consignerCustomerMobile);
                addOrderDTO.setConsignerName(orderDO.getShipName());
                addOrderDTO.setConsignerPhone(orderDO.getShipMobile());
                addOrderDTO.setConsignerProvince(orderDO.getShipProvince());
                addOrderDTO.setConsignerCity(orderDO.getShipCity());
                addOrderDTO.setConsignerDistrict(orderDO.getShipCounty());
                addOrderDTO.setConsignerStreet(orderDO.getShipTown());
                addOrderDTO.setConsignerAddress(orderDO.getShipAddr());
                addOrderDTO.setConsignerLng(memberAddress.getShipLng());
                addOrderDTO.setConsignerLat(memberAddress.getShipLat());

                // 收货人
                getDefaultConsignerInfo(addOrderDTO);

                if (skuVOList != null && skuVOList.size() > 0) {
                    addOrderDTO.setGoodsName(getGoodsName(skuVOList));
                }
                JSONArray itemsJsonArr = JSON.parseArray(orderDO.getItemsJson());
                addOrderDTO.setGoodsPiece(itemsJsonArr.size());
                addOrderDTO.setGoodsType("");
                addOrderDTO.setGoodsVolume(BigDecimal.ZERO);
                addOrderDTO.setGoodsWeight(BigDecimal.ZERO);
                addOrderDTO.setTotalCost(BigDecimal.ZERO);
                addOrderDTO.setCreaterName(orderDO.getShipName());
                addOrderDTO.setDistance(this.countDistance(shopDetail, memberAddress));
                addOrderDTO.setIsAppointment(1);
                addOrderDTO.setIsSelfTake(shippingType.equals(ShipTypeEnum.SELF.name()) ? 1 : 0);
                addOrderDTO.setSiteManName(siteManName);
                addOrderDTO.setLabels(JSON.toJSON(getLabels()).toString());
                if(!EnvironmentUtils.isProd()){
                    addOrderDTO.setUpsBillNo("T" + addOrderDTO.getUpsBillNo());
                }
                JsonBean jsonBean = deliveryManager.addOrder(addOrderDTO);
                if (jsonBean.isSuccess()) {
                    logger.debug("退货审核通过到配送订单下单成功");
                } else {
                    logger.debug("退货审核通过到配送订单下单失败" + JSON.toJSON(jsonBean).toString());
                }

            }
        }
    }

    // 获取默认发货人/收货人信息
    private void getDefaultConsignerInfo(AddOrderDTO addOrderDTO){
        // 从字典中获取发货信息
        String consignerCustomerName = DictUtils.getDictValue("", "发货客户名称", "SHOP_INFORMATION");
        String consignerCustomerMobile = DictUtils.getDictValue("", "发货客户手机号", "SHOP_INFORMATION");
        String consignerName = DictUtils.getDictValue("", "发货人姓名", "SHOP_INFORMATION");
        String consignerPhone = DictUtils.getDictValue("", "发货人电话", "SHOP_INFORMATION");
        String consignerProvince = DictUtils.getDictValue("", "发货地址省", "SHOP_INFORMATION");
        String consignerCity = DictUtils.getDictValue("", "发货地址市", "SHOP_INFORMATION");
        String consignerDistrict = DictUtils.getDictValue("", "发货地址区", "SHOP_INFORMATION");
        String consignerStreet = DictUtils.getDictValue("", "发货地址街道", "SHOP_INFORMATION");
        String consignerAddress = DictUtils.getDictValue("", "发货地址详细地址", "SHOP_INFORMATION");
        String consignerLng = DictUtils.getDictValue("", "发货地址经度", "SHOP_INFORMATION");
        String consignerLat = DictUtils.getDictValue("", "发货地址纬度", "SHOP_INFORMATION");

        if(addOrderDTO.getOrderType() == 1 || addOrderDTO.getOrderType() == 2){
            // 发货人信息
            addOrderDTO.setConsignerCustomerName(consignerCustomerName);
            addOrderDTO.setConsignerCustomerMobile(consignerCustomerMobile);
            addOrderDTO.setConsignerName(consignerName);
            addOrderDTO.setConsignerPhone(consignerPhone);
            addOrderDTO.setConsignerProvince(consignerProvince);
            addOrderDTO.setConsignerCity(consignerCity);
            addOrderDTO.setConsignerDistrict(consignerDistrict);
            addOrderDTO.setConsignerStreet(consignerStreet);
            addOrderDTO.setConsignerAddress(consignerAddress);
            addOrderDTO.setConsignerLng(Double.valueOf(consignerLng));
            addOrderDTO.setConsignerLat(Double.valueOf(consignerLat));
        }else if(addOrderDTO.getOrderType() == 3){
            // 收货人信息
            addOrderDTO.setConsigneeCustomerName(consignerCustomerName);
            addOrderDTO.setConsigneeCustomerMobile(consignerCustomerMobile);
            addOrderDTO.setConsigneeName(consignerName);
            addOrderDTO.setConsigneePhone(consignerPhone);
            addOrderDTO.setConsigneeProvince(consignerProvince);
            addOrderDTO.setConsigneeCity(consignerCity);
            addOrderDTO.setConsigneeDistrict(consignerDistrict);
            addOrderDTO.setConsigneeStreet(consignerStreet);
            addOrderDTO.setConsigneeAddress(consignerAddress);
            addOrderDTO.setConsigneeLng(Double.valueOf(consignerLat));
            addOrderDTO.setConsigneeLat(Double.valueOf(consignerLng));
        }
    }


    // 获取
    private List<Map<String, String>> getLabels(){
        List<Map<String, String>> labelList = new ArrayList<>();
        Map<String, String> map1 = new HashMap<>();
        map1.put("label", "预约");
        map1.put("color", "#EB6100");

        Map<String, String> map2 = new HashMap<>();
        map2.put("label", "同城");
        map2.put("color", "#00A0E9");

        Map<String, String> map3 = new HashMap<>();
        map3.put("label", "平台订单");
        map3.put("color", "#757575");

        Map<String, String> map4 = new HashMap<>();
        map4.put("label", "积分");
        map4.put("color", "#EB6100");

        // 默认的颜色
        labelList.add(map1);
        labelList.add(map2);
        labelList.add(map3);
        labelList.add(map4);
        return labelList;
    }

    /**
     * 计划配送时间
     */
    private Date getPlanDeliveryTime(){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 10);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return c.getTime();
    }

    /**
     * 计算距离 (千米)
     */
    private Double countDistance(ShopDetailDO shopDetail, MemberAddress memberAddress) {
        Double shopLng = shopDetail.getShopLng();
        Double shopLat = shopDetail.getShopLat();

        Double shipLng = memberAddress.getShipLng();
        Double shipLat = memberAddress.getShipLat();
        double distance = tencentManager.countDrivingDistance((shopLat + "," + shopLng), shipLat + "," + shipLng);
        return new BigDecimal(distance / 1000).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }

    // 获取订单商品名称
    private String getGoodsName(List<OrderSkuVO> skuList){
//        OrderSkuVO orderSkuVO = skuList.get(0);
//        String goodsName = orderSkuVO.getName();
//        if(goodsName.length() >= 20){
//            goodsName = goodsName.substring(0, 17) + "...";
//        }
        StringBuffer goodsName = new StringBuffer();
        for (OrderSkuVO orderSkuVO : skuList) {
            goodsName.append(orderSkuVO.getName());
            goodsName.append("x");
            goodsName.append(orderSkuVO.getNum());
            goodsName.append("，");
        }
        return goodsName.toString();
    }


    // 获取赔付商品名称
    private String getGoodsName(String claimSkus){
        List<ClaimSkusDTO> claimSkusList = JSON.parseArray(claimSkus, ClaimSkusDTO.class);
        if(CollectionUtils.isEmpty(claimSkusList)){
            return "Sku见订单";
        }
        ClaimSkusDTO claimSkusDTO = claimSkusList.get(0);
        String goodsName = claimSkusDTO.getGoodsName();
        if(goodsName.length() >= 20){
            goodsName = goodsName.substring(0, 17) + "...";
        }
        return goodsName;
    }


}
