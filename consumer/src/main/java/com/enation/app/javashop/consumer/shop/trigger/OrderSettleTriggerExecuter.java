package com.enation.app.javashop.consumer.shop.trigger;

import com.enation.app.javashop.core.orderbill.model.dto.OrderSettleMsgDTO;
import com.enation.app.javashop.core.orderbill.service.SettleAccountsManager;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.framework.trigger.Interface.TimeTriggerExecuter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单结算延迟加载执行器
 */
@Component("orderSettleTriggerExecuter")
public class OrderSettleTriggerExecuter implements TimeTriggerExecuter {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private OrderQueryManager orderQueryManager;
    @Autowired
    private SettleAccountsManager settleAccountsManager;

    /**
     * 订单结算延迟加载执行器
     */
    @Override
    public void execute(Object object) {
        try {
            OrderSettleMsgDTO orderSettleMsgDTO = (OrderSettleMsgDTO) object;
            Integer orderId = orderSettleMsgDTO.getOrderId();
            OrderDO orderDO = orderQueryManager.getModel(orderId);
            settleAccountsManager.orderSettle(orderDO);
        } catch (Exception e) {
            logger.error("订单结算出现错误", e);
        }
    }
}
