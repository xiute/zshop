package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.DistributionBindingMessageEvent;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.distribution.model.vo.DistributionRelationshipVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xlg
 * @since 2020-12-03 10:42
 * 记录会员团长绑定记录
 */
@Component
public class DistributionBindingReceiver {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired(required = false)
    private List<DistributionBindingMessageEvent> events;


    /**
     * 记录会员团长绑定记录
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.DISTIRBUTION_BINDING_MESSAGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.DISTIRBUTION_BINDING_MESSAGE, type = ExchangeTypes.FANOUT)
    ))
    public void spreadRefundComplete(List<DistributionRelationshipVO> distributionRelationshipVO) {
        logger.info("【消息内容】:"+distributionRelationshipVO.toString());

        if (events != null) {
            for (DistributionBindingMessageEvent event : events) {
                try {
                    event.DistributionBindingMessageInvoke(distributionRelationshipVO);
                } catch (Exception e) {
                    logger.error("保存会员团长绑定消息失败:", e);
                }
            }
        }

    }
}
