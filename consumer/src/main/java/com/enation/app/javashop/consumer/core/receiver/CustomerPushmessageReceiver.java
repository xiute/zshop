package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.CustomerPushMessageEvent;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.promotion.activitymessage.model.dos.ActivityMessageDO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xlg
 * @since 2020-11-24 19:52
 * 给用户推送活动消息
 */
@Component
public class CustomerPushmessageReceiver {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired(required = false)
    private List<CustomerPushMessageEvent> events;


    /**
     * 给会员推送活动消息  CustomePpushMessageConsumer
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.CUSTOMER_PUSH_MESSAGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.CUSTOMER_PUSH_MESSAGE, type = ExchangeTypes.FANOUT)
    ))
    public void spreadRefundComplete(ActivityMessageDO activityMessageDO) {
        logger.info("【消息内容】:"+activityMessageDO.toString());

        if (events != null) {
            for (CustomerPushMessageEvent event : events) {
                try {
                    event.customerPushMessageInvoke(activityMessageDO);
                } catch (Exception e) {
                    logger.error("活动推送消息失败:", e);
                }
            }
        }

    }
}
