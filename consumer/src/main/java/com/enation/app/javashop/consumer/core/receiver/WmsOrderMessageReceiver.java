package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.SmsSendMessageEvent;
import com.enation.app.javashop.consumer.core.event.WmsOrderMessageEvent;
import com.enation.app.javashop.core.base.model.vo.SmsSendVO;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.wms.model.vo.WmsOrderChangeMsg;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 出库消息通知
 */
@Component
public class WmsOrderMessageReceiver {

    @Autowired(required = false)
    private List<WmsOrderMessageEvent> events;

    protected final Log logger = LogFactory.getLog(this.getClass());

    /**
     *
     * @param wmsOrderChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.WMS_ORDER_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.WMS_ORDER_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void wmsOrderMessage(WmsOrderChangeMsg wmsOrderChangeMsg) {
        if (events != null) {
            for (WmsOrderMessageEvent event : events) {
                try {
                    event.change(wmsOrderChangeMsg);
                } catch (Exception e) {
                    logger.error("出库单状态变更", e);
                }
            }
        }
    }
}
