package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.ClaimsStatusChangeEvent;
import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/9/24
 * @Description: 理赔状态改变
 */
public class ClaimsStatusChangeReceiver {

    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private List<ClaimsStatusChangeEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.CLAIMS_STATUS_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.CLAIMS_STATUS_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void claimsChange(List<ClaimsDO> claimsDOList) {
        logger.info("【理赔单状态改变消息内容】:" + claimsDOList.toString());

        if (events != null) {
            for (ClaimsStatusChangeEvent event : events) {
                try {
                    event.claimsChange(claimsDOList);
                } catch (Exception e) {
                    logger.error("理赔单状态改变消息出错", e);
                }
            }
        }
    }
}
