package com.enation.app.javashop.consumer.shop.message;

import com.enation.app.javashop.consumer.core.event.*;
import com.enation.app.javashop.consumer.core.receiver.OrderIntoDbReceiver;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.aftersale.model.enums.RefuseTypeEnum;
import com.enation.app.javashop.core.base.message.*;
import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.member.MemberClient;
import com.enation.app.javashop.core.client.member.MemberNoticeLogClient;
import com.enation.app.javashop.core.client.member.ShopNoticeLogClient;
import com.enation.app.javashop.core.client.system.MessageTemplateClient;
import com.enation.app.javashop.core.client.trade.OrderClient;
import com.enation.app.javashop.core.goods.model.vo.CacheGoods;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.vo.MemberLoginMsg;
import com.enation.app.javashop.core.shop.model.dos.ShopNoticeLogDO;
import com.enation.app.javashop.core.shop.model.enums.ShopNoticeTypeEnum;
import com.enation.app.javashop.core.system.enums.MessageCodeEnum;
import com.enation.app.javashop.core.system.enums.MessageOpenStatusEnum;
import com.enation.app.javashop.core.system.model.dos.MessageTemplateDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.dto.OrderDTO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.vo.TradeVO;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import org.apache.commons.lang.text.StrSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zjp
 * @version v7.0
 * @Description 消息模版发送站内信
 * @ClassName NoticeSendMessageConsumer
 * @since v7.0 上午11:43 2018/7/9
 */
@Component
public class NoticeSendMessageConsumer implements OrderStatusChangeEvent, RefundStatusChangeEvent, GoodsChangeEvent, MemberLoginEvent, MemberRegisterEvent, TradeIntoDbEvent, GoodsCommentEvent {

    @Autowired
    private ShopNoticeLogClient shopNoticeLogClient;

    @Autowired
    private MemberNoticeLogClient memberNoticeLogClient;

    @Autowired
    private MessageTemplateClient messageTemplateClient;

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private MemberClient memberClient;

    private void sendShopNotice(ShopNoticeLogDO shopNoticeLogDO) {
        shopNoticeLogDO.setIsDelete(0);
        shopNoticeLogDO.setIsRead(0);
        shopNoticeLogDO.setSendTime(DateUtil.getDateline());
        shopNoticeLogClient.add(shopNoticeLogDO);
    }

    private void sendMemberNotice(String content, long sendTime, Integer memberId) {
        memberNoticeLogClient.add(content, sendTime, memberId, "");
    }

    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {

        OrderDO orderDO = orderMessage.getOrderDO();

        ShopNoticeLogDO shopNoticeLogDO = new ShopNoticeLogDO();

        MessageTemplateDO messageTemplate = null;

        //订单支付提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.PAID_OFF.name())) {
            Map<String, Object> valuesMap = new HashMap<String, Object>(4);
            valuesMap.put("ordersSn", orderDO.getSn());
            valuesMap.put("paymentTime", DateUtil.toString(orderDO.getPaymentTime(), "yyyy-MM-dd"));

            // 店铺订单支付提醒
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSPAY);
            // 判断是否开启
            if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                shopNoticeLogDO.setShopId(orderDO.getSellerId());
                shopNoticeLogDO.setType(ShopNoticeTypeEnum.ORDER.value());
                shopNoticeLogDO.setNoticeContent(this.replaceContent(messageTemplate.getContent(), valuesMap));
                this.sendShopNotice(shopNoticeLogDO);
            }

            // 会员订单支付提醒
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERORDERSPAY);
            if (messageTemplate != null) {
                // 判断是否开启
                if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    sendMemberNotice(this.replaceContent(messageTemplate.getContent(), valuesMap), DateUtil.getDateline(), orderDO.getMemberId());
                }
            }
        }

        //订单收货提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.ROG.name())) {

            Map<String, Object> valuesMap = new HashMap<String, Object>(4);
            valuesMap.put("ordersSn", orderDO.getSn());
            valuesMap.put("finishTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));

            // 店铺订单收货提醒
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSRECEIVE);
            if (messageTemplate != null) {
                if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    shopNoticeLogDO.setNoticeContent(this.replaceContent(messageTemplate.getContent(), valuesMap));
                    shopNoticeLogDO.setShopId(orderDO.getSellerId());
                    shopNoticeLogDO.setType(ShopNoticeTypeEnum.ORDER.value());
                    this.sendShopNotice(shopNoticeLogDO);
                }
            }
            //会员订单收货提醒
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERORDERSRECEIVE);
            if (messageTemplate != null) {
                if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    sendMemberNotice(this.replaceContent(messageTemplate.getContent(), valuesMap), DateUtil.getDateline(), orderDO.getMemberId());
                }
            }
        }

        //订单取消提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.CANCELLED.name())) {

            Map<String, Object> valuesMap = new HashMap<String, Object>(4);
            valuesMap.put("ordersSn", orderDO.getSn());
            valuesMap.put("cancelTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));

            // 发送会员消息
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERORDERSCANCEL);
            if (messageTemplate != null) {

                // 判断是否开启
                if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    sendMemberNotice(this.replaceContent(messageTemplate.getContent(), valuesMap), DateUtil.getDateline(), orderDO.getMemberId());
                }
            }

            // 发送店铺消息
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSCANCEL);
            if (messageTemplate != null) {
                // 判断是否开启
                if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    shopNoticeLogDO.setShopId(orderDO.getSellerId());
                    shopNoticeLogDO.setNoticeContent(this.replaceContent(messageTemplate.getContent(), valuesMap));
                    shopNoticeLogDO.setType(ShopNoticeTypeEnum.ORDER.value());
                    this.sendShopNotice(shopNoticeLogDO);
                }
            }
        }

        //订单发货提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.SHIPPED.name())) {
            // 会员消息发送
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERORDERSSEND);
            if (messageTemplate != null) {
                // 判断是否开启
                if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    Map<String, Object> valuesMap = new HashMap<String, Object>(4);
                    valuesMap.put("ordersSn", orderDO.getSn());
                    valuesMap.put("shipSn", orderDO.getShipNo());
                    valuesMap.put("sendTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));
                    sendMemberNotice(this.replaceContent(messageTemplate.getContent(), valuesMap), DateUtil.getDateline(), orderDO.getMemberId());
                }
            }
        }
    }


    @Override
    public void refund(RefundChangeMsg refundChangeMsg) {
        ShopNoticeLogDO shopNoticeLogDO = new ShopNoticeLogDO();
        OrderDetailDTO orderDetailDTO = orderClient.getModel(refundChangeMsg.getRefund().getOrderSn());

        //退货/款提醒
        if (refundChangeMsg.getRefundStatusEnum().equals(RefundStatusEnum.APPLY)) {
            if (orderDetailDTO != null) {

                MessageTemplateDO messageTemplate = null;

                // 会员信息发送
                // 记录会员订单取消信息（会员中心查看）
                if (refundChangeMsg.getRefund().getRefuseType().equals(RefuseTypeEnum.RETURN_MONEY.value())) {
                    messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERREFUNDUPDATE);
                }
                if (refundChangeMsg.getRefund().getRefuseType().equals(RefuseTypeEnum.RETURN_GOODS.value())) {
                    messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERRETURNUPDATE);
                }

                if (messageTemplate != null) {
                    // 判断是否开启
                    if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                        Map<String, Object> valuesMap = new HashMap<String, Object>(2);
                        valuesMap.put("refundSn", refundChangeMsg.getRefund().getSn());
                        sendMemberNotice(this.replaceContent(messageTemplate.getContent(), valuesMap), DateUtil.getDateline(), orderDetailDTO.getMemberId());
                    }
                }

                // 店铺信息发送
                messageTemplate = null;
                if (refundChangeMsg.getRefund().getRefuseType().equals(RefuseTypeEnum.RETURN_GOODS.value())) {
                    messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPRETURN);
                }

                if (refundChangeMsg.getRefund().getRefuseType().equals(RefuseTypeEnum.RETURN_MONEY.value())) {
                    messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPREFUND);
                }

                // 记录店铺订单取消信息（商家中心查看）
                if (messageTemplate != null) {
                    // 判断是否开启
                    if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                        Map<String, Object> valuesMap = new HashMap<String, Object>(2);
                        valuesMap.put("refundSn", refundChangeMsg.getRefund().getSn());
                        shopNoticeLogDO.setShopId(orderDetailDTO.getSellerId());
                        shopNoticeLogDO.setNoticeContent(this.replaceContent(messageTemplate.getContent(), valuesMap));
                        shopNoticeLogDO.setType(ShopNoticeTypeEnum.AFTERSALE.value());
                        this.sendShopNotice(shopNoticeLogDO);
                    }
                }
            }
        }

    }

    @Override
    public void goodsChange(GoodsChangeMsg goodsChangeMsg) {
        ShopNoticeLogDO shopNoticeLogDO = new ShopNoticeLogDO();
        //商品审核失败提醒
        if (GoodsChangeMsg.GOODS_VERIFY_FAIL == goodsChangeMsg.getOperationType()) {
            //发送店铺消息
            for (Integer goodsId : goodsChangeMsg.getGoodsIds()) {

                CacheGoods goods = goodsClient.getFromCache(goodsId);

                // 记录店铺订单取消信息（商家中心查看）
                MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPGOODSVERIFY);
                if (messageTemplate != null) {

                    // 判断是否开启
                    if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {

                        Map<String, Object> valuesMap = new HashMap<String, Object>(2);
                        valuesMap.put("name", goods.getGoodsName());
                        valuesMap.put("message", goodsChangeMsg.getMessage());
                        shopNoticeLogDO.setShopId(goods.getSellerId());
                        shopNoticeLogDO.setNoticeContent(this.replaceContent(messageTemplate.getContent(), valuesMap));
                        shopNoticeLogDO.setType(ShopNoticeTypeEnum.GOODS.value());
                        this.sendShopNotice(shopNoticeLogDO);
                    }
                }
            }
        }
        //商品下架消息提醒
        if (GoodsChangeMsg.UNDER_OPERATION == goodsChangeMsg.getOperationType() && !StringUtil.isEmpty(goodsChangeMsg.getMessage())) {
            //发送店铺消息
            for (Integer goodsId : goodsChangeMsg.getGoodsIds()) {

                CacheGoods goods = goodsClient.getFromCache(goodsId);
                // 记录店铺订单取消信息（商家中心查看）
                MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPGOODSMARKETENABLE);
                if (messageTemplate != null) {

                    // 判断是否开启
                    if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {

                        Map<String, Object> valuesMap = new HashMap<String, Object>(2);
                        valuesMap.put("name", goods.getGoodsName());
                        valuesMap.put("reason", goodsChangeMsg.getMessage());
                        shopNoticeLogDO.setShopId(goods.getSellerId());
                        shopNoticeLogDO.setNoticeContent(this.replaceContent(messageTemplate.getContent(), valuesMap));
                        shopNoticeLogDO.setType(ShopNoticeTypeEnum.GOODS.value());
                        this.sendShopNotice(shopNoticeLogDO);
                    }
                }
            }
        }
    }

    @Override
    public void memberLogin(MemberLoginMsg memberLoginMsg) {
        Member member = memberClient.getModel(memberLoginMsg.getMemberId());
        MessageTemplateDO messageTemplate = null;
        // 记录会员登录成功信息（会员中心查看）
        messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERLOGINSUCCESS);

        // 判断站内信是否开启
        if (messageTemplate != null) {
            // 判断短信是否开启
            if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                Map<String, Object> valuesMap = new HashMap<String, Object>(2);
                valuesMap.put("name", member.getUname());
                valuesMap.put("loginTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));

                //判断是会员登录还是商家登录 1 会员，2 商家
                if (memberLoginMsg.getMemberOrSeller().equals(1)) {
                    sendMemberNotice(this.replaceContent(messageTemplate.getContent(), valuesMap), DateUtil.getDateline(), member.getMemberId());
                } else {
                    ShopNoticeLogDO shopNoticeLogDO = new ShopNoticeLogDO();
                    shopNoticeLogDO.setShopId(member.getShopId());
                    shopNoticeLogDO.setNoticeContent(this.replaceContent(messageTemplate.getContent(), valuesMap));
                    shopNoticeLogDO.setType(ShopNoticeTypeEnum.OTHER.value());
                    this.sendShopNotice(shopNoticeLogDO);
                }

            }
        }
    }


    @Override
    public void memberRegister(MemberRegisterMsg memberRegisterMsg) {
        Member member = memberRegisterMsg.getMember();
        //会员注册成功提醒
        MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERREGISTESUCCESS);
        if (messageTemplate != null) {
            // 判断是否开启
            if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                Map<String, Object> valuesMap = new HashMap<String, Object>(2);
                valuesMap.put("loginTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));
                valuesMap.put("name", member.getUname());
                sendMemberNotice(this.replaceContent(messageTemplate.getContent(), valuesMap), DateUtil.getDateline(), member.getMemberId());
            }
        }
    }

    @Override
    public void onTradeIntoDb(TradeVO tradeVO) {
        OrderIntoDbReceiver.THREAD_POOL.execute(() -> {
            //店铺新订单创建提醒
            ShopNoticeLogDO shopNoticeLogDO = new ShopNoticeLogDO();
            List<OrderDTO> orderList = tradeVO.getOrderList();
            MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSNEW);
            if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {
                for (OrderDTO orderDTO : orderList) {
                    // 判断是否开启
                    Map<String, Object> valuesMap = new HashMap<String, Object>(4);
                    valuesMap.put("ordersSn", orderDTO.getSn());
                    valuesMap.put("createTime", DateUtil.toString(orderDTO.getCreateTime(), "yyyy-MM-dd"));
                    shopNoticeLogDO.setShopId(orderDTO.getSellerId());
                    shopNoticeLogDO.setType(ShopNoticeTypeEnum.ORDER.value());
                    shopNoticeLogDO.setNoticeContent(this.replaceContent(messageTemplate.getContent(), valuesMap));
                    this.sendShopNotice(shopNoticeLogDO);
                }
            }
        });
    }

    @Override
    public void goodsComment(GoodsCommentMsg goodsCommentMsg) {
        if(goodsCommentMsg.getComment() == null || goodsCommentMsg.getComment().isEmpty()){
            return;
        }
        //商品评价提醒
        ShopNoticeLogDO shopNoticeLogDO = new ShopNoticeLogDO();
        MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSEVALUATE);
        // 判断是否开启
        if (messageTemplate.getNoticeState().equals(MessageOpenStatusEnum.OPEN.value())) {

            goodsCommentMsg.getComment().forEach(comment -> {
                Map<String, Object> valuesMap = new HashMap<String, Object>(4);
                valuesMap.put("sn", comment.getOrderSn());
                valuesMap.put("member_name", comment.getMemberName());
                shopNoticeLogDO.setShopId(comment.getSellerId());
                shopNoticeLogDO.setType(ShopNoticeTypeEnum.ORDER.value());
                shopNoticeLogDO.setNoticeContent(this.replaceContent(messageTemplate.getContent(), valuesMap));
                this.sendShopNotice(shopNoticeLogDO);
            });


        }
    }

    /**
     * 替换短信中的内容
     *
     * @param content 短信
     * @param map     替换的文本内容
     * @return
     */
    private String replaceContent(String content, Map map) {
        StrSubstitutor strSubstitutor = new StrSubstitutor(map);
        return strSubstitutor.replace(content);
    }
}
