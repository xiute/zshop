package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.wms.model.vo.WmsOrderChangeMsg;

/**
 * @author JFeng
 * @date 2020/10/13 14:25
 */
public interface WmsOrderMessageEvent {
    void change(WmsOrderChangeMsg wmsOrderChangeMsg);
}
