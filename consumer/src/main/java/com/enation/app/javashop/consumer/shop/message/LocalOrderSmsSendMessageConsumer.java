package com.enation.app.javashop.consumer.shop.message;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.enation.app.javashop.consumer.core.event.OrderStatusChangeEvent;
import com.enation.app.javashop.consumer.core.event.RefundStatusChangeEvent;
import com.enation.app.javashop.core.base.SettingGroup;
import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.base.message.RefundChangeMsg;
import com.enation.app.javashop.core.base.model.vo.SmsSendVO;
import com.enation.app.javashop.core.client.member.MemberClient;
import com.enation.app.javashop.core.client.member.ShopClient;
import com.enation.app.javashop.core.client.system.SmsClient;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.pagedata.model.PageData;
import com.enation.app.javashop.core.pagedata.service.PageDataManager;
import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.core.system.enums.MessageCodeEnum;
import com.enation.app.javashop.core.system.enums.MessageOpenStatusEnum;
import com.enation.app.javashop.core.system.model.dos.MessageTemplateDO;
import com.enation.app.javashop.core.system.model.vo.SiteSetting;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.DictUtils;
import com.enation.app.javashop.framework.util.JsonUtil;
import jodd.util.StringPool;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  同城配送订单  发短信通知商户,运营人员
 */
@Component
public class LocalOrderSmsSendMessageConsumer  implements OrderStatusChangeEvent{
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private ShopClient shopClient;

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private SmsClient smsClient;

    @Autowired
    private PageDataManager pageDataManager;

    /**
     * 发短信
     */
    private void sendSms(SmsSendVO smsSendVO) {
        smsClient.send(smsSendVO);
    }

    /**
     * 本地生活 下单成功 发短信通知
     * @param orderMessage
     */
    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        OrderDO orderDO = orderMessage.getOrderDO();

        //获取当前店铺所有者的联系方式
        ShopVO shopVO = shopClient.getShop(orderDO.getSellerId());
        //订单支付提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.PAID_OFF.name())) {

            String shipTown = orderDO.getShipTown();
            String shipAddr = orderDO.getShipAddr();
            // 订单的收货 镇 是衢山镇   判断是不是 本地生活的单子
            if(shipTown.contains("衢山") || shipAddr.contains("衢山")){
                // 查询本地店铺
                PageData pageData = pageDataManager.getLocalLifeShop("APP", 2, "4", "衢山镇");
                if(pageData != null){
                    String pageDataStr = pageData.getPageData();
                    JSONArray pageDataArray = JSON.parseArray(pageDataStr);
                    for (Object object : pageDataArray) {
                        JSONObject jsonObject = JSON.parseObject(object.toString());
                        // 查询页面是不是发现页
                        Integer tplId = jsonObject.getInteger("tpl_id");
                        // 附近店铺组件
                        if(tplId == 75){
                            boolean dataSource = jsonObject.containsKey("dataSource");
                            // 是否是附近店铺
                            if(dataSource){
                                JSONObject data = jsonObject.getJSONObject("dataSource").getJSONObject("data");
                                if(!CollectionUtils.isEmpty(data)){
                                    logger.info("发短信:取出配置的本地生活店铺id"+data);
                                    JSONArray shopIds = data.getJSONArray("shopIds");
                                    if(!CollectionUtils.isEmpty(shopIds)){
                                        // 下单店铺是本地生活店铺  发货地址是店铺地址
                                        boolean contains = shopIds.contains(orderDO.getSellerId());
                                        if(contains){
                                            // 订单支付完成  发短信提醒商家
                                            this.sendSms(this.getSmsMessage(shopVO.getLinkPhone(),"【智溢商城】有用户在您的店里下单了，快去智溢商城商家APP里面查看订单备货吧~"));

                                            //订单支付完成  发短信提醒运营人员
                                            // 获取数据字典中 运营人员手机号
                                            String operateMobiles = DictUtils.getDictValue(null, "operate_mobiles", "operate_mobiles");
                                            List<String> mobilesList = Arrays.asList(operateMobiles.replace("，", ",").split(StringPool.COMMA));

                                            for (String mobile : mobilesList) {
                                                this.sendSms(this.getSmsMessage(mobile ,"【智溢商城】本地生活<"+shopVO.getShopName()+">店铺有用户下单了，快去运营中心里面查看订单吧~"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * 组织短信发送的相关信息
     *
     * @param mobile  手机号
     * @param content 内容
     * @return 短信业务传递参数vo
     */
    private SmsSendVO getSmsMessage(String mobile, String content) {
        SmsSendVO smsSendVO = new SmsSendVO();
        smsSendVO.setMobile(mobile);
        smsSendVO.setContent(content);
        return smsSendVO;
    }

}
