package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;

/**
 * @Author 王校长
 * @Date 2020/9/10
 * @Descroption 多退少补完成事件
 */
public interface SpreadRefundCompleteEvent {

    /**
     * 多退少补完成后需要执行的方法
     * @param refund
     */
    void spreadRefundComplete(RefundDO refund);
}
