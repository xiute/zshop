package com.enation.app.javashop.consumer.shop.goodssearch;

import com.enation.app.javashop.consumer.core.event.GoodsIndexInitEvent;
import com.enation.app.javashop.consumer.core.event.ShetuanChangeEvent;
import com.enation.app.javashop.core.base.message.ShetuanChangeMsg;
import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.goods.GoodsIndexClient;
import com.enation.app.javashop.core.client.member.ShopClient;
import com.enation.app.javashop.core.client.trade.PintuanClient;
import com.enation.app.javashop.core.client.trade.PintuanGoodsClient;
import com.enation.app.javashop.core.client.trade.ShetuanClient;
import com.enation.app.javashop.core.promotion.pintuan.model.Pintuan;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.tool.model.enums.PromotionStatusEnum;
import com.enation.app.javashop.core.shop.model.dos.ShopDetailDO;
import com.enation.app.javashop.core.system.progress.model.TaskProgressConstant;
import com.enation.app.javashop.core.system.progress.service.ProgressManager;
import com.enation.app.javashop.framework.logs.Debugger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author fk
 * @version v1.0
 * @Description: 商品索引初始化
 * @date 2018/6/25 11:38
 * @since v7.0.0
 */
@Service
public class GoodsIndexInitConsumer implements GoodsIndexInitEvent {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private ProgressManager progressManager;
    @Autowired
    private GoodsIndexClient goodsIndexClient;
    @Autowired
    private GoodsClient goodsClient;
    @Autowired
    private PintuanClient pintuanClient;
    @Autowired
    private ShetuanClient shetuanClient;
    @Autowired
    private PintuanGoodsClient pintuanGoodsClient;
    @Autowired
    private ShopClient shopClient;

    @Autowired
    private Debugger debugger;

    @Override
    public void createGoodsIndex() {

        debugger.log("开始生成索引");

        String key = TaskProgressConstant.GOODS_INDEX;
        try {
            List<Pintuan> list = pintuanClient.get(PromotionStatusEnum.UNDERWAY.name());
            //List<ShetuanDO> shetuanList = shetuanClient.get(PromotionStatusEnum.UNDERWAY.name());

            /** 获取商品数 */
            int goodsCount = this.goodsClient.queryGoodsCount();

            /** 生成任务进度 */
            progressManager.taskBegin(key, goodsCount + list.size());

            //生成普通商品商品索引
            boolean goodsResult = createOrdinaryGoods(goodsCount);

            //生成拼团活动商品索引
            boolean ptResult  = createPintuanGoods(list, key);

            //生成社区团购商品索引
            //boolean stResult  = createShetuanGoods(shetuanList, key);

            //任务结束
            progressManager.taskEnd(key, "索引生成完成");

            debugger.log("索引生成完成");

            if (goodsResult | ptResult) {
                debugger.log("索引生成出现错误");
            }

        } catch (Exception e) {
            debugger.log("索引生成异常");
            progressManager.taskError(key, "生成索引异常，请联系运维人员");
            this.logger.error("生成索引异常：", e);

        }

    }

    /**
     * 生成普通商品的索引
     *
     * @param goodsCount 商品数
     */
    private boolean createOrdinaryGoods(Integer goodsCount) {

        //用来标记是否有错误
        boolean hasError = false;

        int pageSize = 100;
        int pageCount;
        pageCount = goodsCount / pageSize;
        pageCount = goodsCount % pageSize > 0 ? pageCount + 1 : pageCount;
        for (int i = 1; i <= pageCount; i++) {
            List<Map> goodsList = this.goodsClient.queryGoodsByRange(i, pageSize);
            Integer[] goodsIds = new Integer[goodsList.size()];
            List<Integer> sellerIds = new ArrayList<>();
            int j = 0;
            for (Map map : goodsList) {
                goodsIds[j] = Integer.valueOf(map.get("goods_id").toString());
                sellerIds.add(Integer.valueOf(map.get("seller_id").toString()));
                j++;
            }
            List<Map<String, Object>> list = goodsClient.getGoodsAndParams(goodsIds);
            // 店铺属性信息
            sellerIds = sellerIds.stream().distinct().collect(Collectors.toList());
            List<ShopDetailDO> shopDetailDOList= shopClient.getShopDetailByIds(sellerIds);
            Map<Integer, ShopDetailDO> shopMap = shopDetailDOList.stream().collect(Collectors.toMap(ShopDetailDO::getShopId, Function.identity()));
            boolean result = goodsIndexClient.addAll(list,shopMap, i);

            //有过错误就是有错误
            hasError = result && hasError;
        }

        return hasError;

    }


    /**
     * 生成拼团活动商品索引
     *
     * @param list 拼团活动集合
     */
    private boolean createPintuanGoods(List<Pintuan> list, String key) {
        boolean hasError = false;
        for (Pintuan pintuan : list) {
            boolean result  = pintuanGoodsClient.createGoodsIndex(pintuan.getPromotionId());
            progressManager.taskUpdate(key, "为拼团名称为[" + pintuan.getPromotionName() + "]的活动生成索引");
            hasError = result && hasError;
        }

        return hasError;
    }

}
