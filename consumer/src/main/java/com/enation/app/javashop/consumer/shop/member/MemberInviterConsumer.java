package com.enation.app.javashop.consumer.shop.member;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dag.eagleshop.core.account.utils.HttpUtils;
import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import com.enation.app.javashop.consumer.core.event.MemberRegisterEvent;
import com.enation.app.javashop.core.base.message.MemberRegisterMsg;
import com.enation.app.javashop.core.distribution.model.dos.DistributionDO;
import com.enation.app.javashop.core.distribution.service.DistributionManager;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.dos.MemberInviter;
import com.enation.app.javashop.core.member.service.MemberInviterManager;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.core.promotion.coupon.service.CouponManager;
import com.enation.app.javashop.core.system.enums.WechatMiniproTemplateTypeEnum;
import com.enation.app.javashop.core.system.sendMessage.WechatSendMessage;
import com.enation.app.javashop.core.system.service.WechatPublicManager;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.DictUtils;
import com.enation.app.javashop.framework.util.StringUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 注册后绑定邀请人
 */
@Component
public class MemberInviterConsumer implements MemberRegisterEvent{

    @Autowired
    private MemberInviterManager memberInviterManager;

    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private Cache cache;



    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void memberRegister(MemberRegisterMsg memberRegisterMsg) {
        // 注册完毕后，给注册会员添加他的上级分销商id
        Object upMemberId = cache.get(MemberInviterManager.PREFIX + memberRegisterMsg.getUuid());
        logger.debug("MemberInviterConsumer_cache_upMemberId:" + upMemberId + "," + memberRegisterMsg.getUuid());
        if(upMemberId != null){
            Member member = memberRegisterMsg.getMember();
            Integer memberId = member.getMemberId();

            // 查询会员是否已经有邀请人 之前注册过小程序 后来删掉了
            MemberInviter memberInviter = memberInviterManager.getByMemberId(memberId);
            if(memberInviter == null){
                logger.debug("会员" + memberId + "绑定邀请人：" + upMemberId);
                memberInviter = new MemberInviter();
                memberInviter.setMemberId(memberId);
                memberInviter.setInviterMemberId(Integer.parseInt(upMemberId.toString()));
                memberInviterManager.addMemberInviter(memberInviter);

            }
            cache.remove(MemberInviterManager.PREFIX + memberRegisterMsg.getUuid());
        }
    }
}
