package com.enation.app.javashop.consumer.job.execute.impl;

import com.enation.app.javashop.consumer.job.execute.EveryDayExecute;
import com.enation.app.javashop.consumer.job.execute.EveryHourExecute;
import com.enation.app.javashop.core.member.service.MemberVisitDataManager;
import com.enation.app.javashop.framework.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: zhou
 * @Date: 2020/9/15
 * @Description: 获取小程序昨日日活及留存
 */
@Component
public class MemberVisitDataJob implements EveryHourExecute {

    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private MemberVisitDataManager memberVisitDataManager;

    @Override
    public void everyHour() {
        try {
            long yesterdayLong = DateUtil.startOfYesterday();
            this.memberVisitDataManager.add(yesterdayLong);
        } catch (Exception e) {
            logger.error("获取小程序昨日日活及留存出错", e);
        }
    }
}
