package com.enation.app.javashop.consumer.job.execute.impl;

import com.dag.eagleshop.core.account.service.AccountManager;
import com.enation.app.javashop.consumer.job.execute.EveryFiveMinExecute;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: zhou
 * @Date: 2020/11/21
 * @Description: 每五分钟查询支付中的提现单，然后调用微信的查询接口，更新提现单状态
 */
@Component
public class WechatTransferinfoJob implements EveryFiveMinExecute {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private AccountManager accountManager;

    @Override
    public void everyFiveMin() {
        try {
            accountManager.updateTransferStatus();
        }catch (Exception e){
            logger.error("更新微信转账状态失败：", e);
        }
    }
}
