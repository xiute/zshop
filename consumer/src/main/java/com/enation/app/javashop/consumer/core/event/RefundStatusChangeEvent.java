package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.base.message.RefundChangeMsg;

/**
 * 退款/退货申请
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:23:11
 */
public interface RefundStatusChangeEvent {
	
	/**
	 * 售后消息
	 * @param refundChangeMsg
	 */
    void refund(RefundChangeMsg refundChangeMsg);
}
