package com.enation.app.javashop.base.api;

import com.enation.app.javashop.core.member.model.vo.RegionVO;
import com.enation.app.javashop.core.system.model.dos.Regions;
import com.enation.app.javashop.core.system.service.RegionsManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 地区api
 *
 * @author zh
 * @version v7.0
 * @date 18/5/28 下午7:49
 * @since v7.0
 */
@RestController
@RequestMapping("/regions")
@Api(description = "地区API")
public class RegionsBaseController {

    @Autowired
    private RegionsManager regionsManager;


    @GetMapping(value = "/{id}/children")
    @ApiOperation(value = "获取某地区的子地区")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "地区id", required = true, dataType = "int", paramType = "path")
    })
    public List<Regions> getChildrenById(@PathVariable Integer id) {

        return regionsManager.getRegionsChildren(id);
    }


    @GetMapping(value = "/depth/{depth}")
    @ApiOperation(value = "根据地区深度查询组织好地区数据结构的地区")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "depth", value = "深度", required = true, dataType = "int", paramType = "path")
    })
    public List<RegionVO> getRegionByDepth(@PathVariable Integer depth) {
        return regionsManager.getRegionByDepth(depth);
    }


    /**
     * 根据区名称合和市名称得到编号
     * @return
     */
    @GetMapping(value = "/area")
    @ApiOperation(value = "根据省市区名称查区编号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "province_name", value = "省名称", required = true, dataType = "String",paramType = "path"),
            @ApiImplicitParam(name = "city_name", value = "市名称", required = true, dataType = "String",paramType = "path"),
            @ApiImplicitParam(name = "area_name", value = "区名称", required = true, dataType = "String",paramType = "path")
    })
    public Map<String, Object> getRegionByDepth( String provinceName,  String cityName, String areaName) {

        return regionsManager.getRegionByLocalName(provinceName,cityName,areaName);
    }



}
