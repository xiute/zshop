import java.math.BigDecimal;

public class Count {
    public static void main(String[] args) {
        boolean isLv1 = true;
        boolean isLv2 = true;
        boolean isInvite = false;
        boolean isLeader = true;

        BigDecimal orderPrice = new BigDecimal("25.6");
        BigDecimal seller = orderPrice;
        BigDecimal sum = BigDecimal.ZERO;

        BigDecimal platform = orderPrice.multiply(BigDecimal.valueOf(0.05)).setScale(2, BigDecimal.ROUND_HALF_UP);
        seller = seller.subtract(platform).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        System.out.println("platform: " + platform);

        if(isLv1){
            BigDecimal lv1 = orderPrice.multiply(BigDecimal.valueOf(0.08)).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            seller = seller.subtract(lv1).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            System.out.println("lv1: " + lv1);
            sum = sum.add(lv1).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        }

        if(isLv2){
            BigDecimal lv2 = orderPrice.multiply(BigDecimal.valueOf(0.01)).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            seller = seller.subtract(lv2).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            System.out.println("lv2: " + lv2);
            sum = sum.add(lv2).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        }

        if(isInvite){
            BigDecimal invite = orderPrice.multiply(BigDecimal.valueOf(0.01)).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            seller = seller.subtract(invite).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            System.out.println("invite: " + invite);
        }

        if(isLeader){
            BigDecimal leader = orderPrice.multiply(BigDecimal.valueOf(0.02)).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            seller = seller.subtract(leader).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            System.out.println("leader: " + leader);
        }

        System.out.println("seller: " + seller);
        System.out.println("sum: " + sum);
    }
}
