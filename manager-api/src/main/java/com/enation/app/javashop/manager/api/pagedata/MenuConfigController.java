package com.enation.app.javashop.manager.api.pagedata;

import com.enation.app.javashop.core.pagedata.model.MenuDate;
import com.enation.app.javashop.core.pagedata.model.vo.PageListQueryVo;
import com.enation.app.javashop.core.pagedata.service.PageDataManager;
import com.enation.app.javashop.framework.database.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/admin/pages/menu-config")
@Api(description = "菜单配置API")
public class MenuConfigController {

    @Autowired
    private PageDataManager pageManager;


    /**
     * 菜单配置-- 数据页面
     * @param pageVO
     */
    @PostMapping(value = "/sel-menu")
    @ApiOperation(value = "数据页面接口")
    public Page selMenu(@RequestBody PageListQueryVo pageVO) {
        return this.pageManager.menuConfigList(pageVO);
    }

    @DeleteMapping(value = "/del-menu/{ids}")
    @ApiOperation(value = "菜单配置-批量删除接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要删除已存在的配置数据", required = true, dataType = "int", paramType = "path",allowMultiple=true) })
    public void delMenu(@PathVariable Integer[] ids) {
        this.pageManager.delMenu(ids);
    }
    @PutMapping(value = "/add-menu")
    @ApiOperation(value = "菜单配置-新增页面",response = MenuDate.class)
    public MenuDate addMenu(@Valid  MenuDate menuDate) {
        this.pageManager.addMenu(menuDate);
        return menuDate;
    }

    @PutMapping(value = "/update-menu/{id}")
    @ApiOperation(value = "菜单配置-修改页面", response = MenuDate.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "int", paramType = "path")})
    public MenuDate udpMenu( @Valid  MenuDate menuDate,@PathVariable(value = "id") Integer id) {
        this.pageManager.udpMenu(menuDate,id);
        return menuDate;
    }

}
