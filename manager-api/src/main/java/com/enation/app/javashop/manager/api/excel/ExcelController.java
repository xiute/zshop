package com.enation.app.javashop.manager.api.excel;

import com.enation.app.javashop.core.excel.ExcelManager;
import com.enation.app.javashop.core.goods.model.dto.GoodsQueryParam;
import com.enation.app.javashop.core.trade.order.model.dto.OrderQueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author JFeng
 * @date 2020/7/11 10:45
 */
@RestController
@RequestMapping("/admin/excel")
@Api(description = "excel导入导出")
public class ExcelController {
    @Autowired
    private ExcelManager excelManager;

    @ApiOperation(value = "导出订单sku列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "ship_name", value = "收货人", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "goods_name", value = "商品名称", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "buyer_name", value = "买家名字", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "ship_mobile", value = "收货人手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "nick_name", value = "会员名称", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "lv1_member_nick_name", value = "团长名称", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "lv1_member_mobile", value = "团长手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "start_time", value = "开始时间", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "结束时间", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "seller_id", value = "店铺ID", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "order_status", value = "订单状态", dataType = "String", paramType = "query",
                    allowableValues = "ALL,WAIT_PAY,WAIT_SHIP,WAIT_ROG,CANCELLED,COMPLETE,WAIT_COMMENT,REFUND",
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中"),
            @ApiImplicitParam(name = "member_id", value = "会员ID", dataType = "int", paramType = "query"),

            @ApiImplicitParam(name = "payment_type", value = "支付方式", dataType = "String", paramType = "query",
                    allowableValues = "ONLINE,COD", example = "ONLINE:在线支付,COD:货到付款"),
            @ApiImplicitParam(name = "order_type", value = "订单类型", dataType = "String", paramType = "query",
                    allowableValues = "normal,pintuan,shetuan", example = "normal:普通订单,pintuan:拼团订单,shetuan:社团订单"),
            @ApiImplicitParam(name = "client_type", value = "订单来源", dataType = "String", paramType = "query",
                    allowableValues = "PC,WAP,NATIVE,REACT,MINI", example = "PC:pc客户端,WAP:WAP客户端,NATIVE:原生APP,REACT:RNAPP,MINI:小程序")
    })
    @GetMapping("/exportOrdersItem")
    public void exportOrdersItem(@ApiIgnore String orderSn, @ApiIgnore String shipName, @ApiIgnore String goodsName, @ApiIgnore String buyerName,
                                 @ApiIgnore String shipMobile,@ApiIgnore String nickName,@ApiIgnore String lv1MemberNickName,@ApiIgnore String lv1MemberMobile,
                                 @ApiIgnore Long startTime, @ApiIgnore Long endTime, @ApiIgnore String orderStatus,
                                  @ApiIgnore Integer memberId, @ApiIgnore Integer sellerId,
                                 @ApiIgnore String paymentType,@ApiIgnore String orderType, @ApiIgnore String clientType) {
        OrderQueryParam param = new OrderQueryParam();
        param.setOrderSn(orderSn);
        param.setShipName(shipName);
        param.setGoodsName(goodsName);
        param.setBuyerName(buyerName);
        param.setOrderStatus(orderStatus);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        param.setPageNo(1);
        param.setPageSize(20000);
        param.setMemberId(memberId);
        param.setSellerId(sellerId);
        param.setPaymentType(paymentType);
        param.setClientType(clientType);
        param.setOrderType(orderType);
        param.setShipMobile(shipMobile);
        param.setNickName(nickName);
        param.setLv1MemberNickName(lv1MemberNickName);
        param.setLv1MemberMobile(lv1MemberMobile);

        excelManager.exportOrderItems(param);
    }


    @ApiOperation(value = "导出商品sku列表")
    @GetMapping("/exportGoodsSku")
    public void exportGoodsSku(GoodsQueryParam param) {
        param.setPageNo(1);
        param.setPageSize(2000);
        this.excelManager.exportGoodsSkuList(param);
    }


    @ApiOperation(value = "导入商品列表")
    @PostMapping(value = "/importGoods")
    public void importGoods(@RequestParam MultipartFile file) {
        excelManager.importGoods(file);
    }

    @ApiOperation(value = "导入商品sku列表")
    @PostMapping(value = "/importGoodsSku")
    public void importGoodsSku(@RequestParam MultipartFile file) {
        excelManager.importGoodsSku(file);
    }

}
