package com.enation.app.javashop.manager.api.base;

import com.enation.app.javashop.core.base.SettingGroup;
import com.enation.app.javashop.core.base.service.SettingManager;
import com.enation.app.javashop.core.shop.ShopErrorCode;
import com.enation.app.javashop.core.shop.model.vo.ShipTemplateSellerVO;
import com.enation.app.javashop.core.shop.service.ShipTemplateManager;
import com.enation.app.javashop.core.system.model.vo.SiteSetting;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.JsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 站点设置api
 *
 * @author zh
 * @version v7.0
 * @date 18/5/18 下午6:55
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/settings")
@Api(description = "站点设置")
@Validated
public class TemplateSettingManagerController {
    @Autowired
    private ShipTemplateManager shipTemplateManager;


    @PutMapping(value = "/temp")
    @ApiOperation(value = "修改默认配送模板")
    public List<ShipTemplateSellerVO> setDefaultTemp(@RequestParam("temp_ids") String tempIds) {
        String[] ids = tempIds.split(",");
        if (ids.length == 0) {
            throw new ServiceException(ShopErrorCode.E202.code(), "模板不能为空");
        }
        return this.shipTemplateManager.setDefaultTemp(ids);
    }
}
