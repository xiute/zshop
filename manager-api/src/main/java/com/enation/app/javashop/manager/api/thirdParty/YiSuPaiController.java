package com.enation.app.javashop.manager.api.thirdParty;

import com.enation.app.javashop.core.thirdParty.model.dto.yisupai.YiSuPaiCategoryDTO;
import com.enation.app.javashop.core.thirdParty.model.dto.yisupai.YiSuPaiGoodsDTO;
import com.enation.app.javashop.core.thirdParty.service.YiSuPaiManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2021/5/8
 * @Description:
 */
@Api(description = "获取浙里淘商品API")
@RestController
@RequestMapping("/admin/third-party/yiSuPai")
public class YiSuPaiController {
    @Autowired
    private YiSuPaiManager yiSuPaiManager;

    @ApiOperation(value = "保存商品")
    @GetMapping(value = "/addYiSuPaiGoods")
    public void addYiSuPaiGoods() {
        yiSuPaiManager.addYiSuPaiGoods();
    }

    @ApiOperation(value = "分页获取商品列表测试接口")
    @GetMapping(value = "/goodsList")
    public List<YiSuPaiGoodsDTO> getGoodsList(int page ,int pageSize) {
        return yiSuPaiManager.getGoodsList(page,pageSize);
    }

    @ApiOperation(value = "获取商品详情")
    @GetMapping(value = "/getGoodsDetail")
    public YiSuPaiGoodsDTO getGoodsDetail(String goodsId) {
        return yiSuPaiManager.getGoodsDetail(goodsId);
    }

    @ApiOperation(value = "获取分类数据测试接口")
    @GetMapping(value = "/categoryList")
    public Map<String, YiSuPaiCategoryDTO> getCategoryList() {
        return yiSuPaiManager.getCategoryList();
    }

    @ApiOperation(value = "注册匿名账户")
    @GetMapping(value="/createAnonyAccount")
    public String createAnonyAccount(Integer memberId) {
        return yiSuPaiManager.createAnonyAccount(memberId);
    }
}
