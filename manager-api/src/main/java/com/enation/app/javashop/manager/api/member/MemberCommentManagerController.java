package com.enation.app.javashop.manager.api.member;

import com.enation.app.javashop.core.member.model.dto.CommentQueryParam;
import com.enation.app.javashop.core.member.model.vo.CommentVO;
import com.enation.app.javashop.core.member.service.MemberCommentManager;
import com.enation.app.javashop.framework.database.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 评论控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-03 10:19:14
 */
@RestController
@RequestMapping("/admin/members/comments")
@Api(description = "评论相关API")
public class MemberCommentManagerController {

    @Autowired
    private MemberCommentManager memberCommentManager;

    @ApiOperation(value = "查询评论列表", response = CommentVO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping
    public Page list(@ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize, CommentQueryParam param) {

        param.setPageNo(pageNo);
        param.setPageSize(pageSize);

        return this.memberCommentManager.list(param);
    }

    @ApiOperation(value = "删除评论")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "comment_id", value = "评论id", required = true, dataType = "int", paramType = "path"),
    })
    @DeleteMapping(value = "/{comment_id}")
    public String deleteComment(@PathVariable(name = "comment_id") Integer commentId) {

        this.memberCommentManager.delete(commentId);

        return "";
    }


    @ApiOperation(value = "审核评论")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "comment_id", value = "评论id", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "audit_status", value = "审核状态,PASS_AUDIT：审核通过，REFUSE_AUDIT：审核拒绝", required = true, dataType = "string",
                    allowableValues = "PASS_AUDIT,REFUSE_AUDIT",
                    paramType = "query"),
    })
    @PostMapping(value = "/{comment_id}")
    public String auditComment(@PathVariable(name = "comment_id") Integer commentId,@ApiIgnore String auditStatus) {

        this.memberCommentManager.auditComment(commentId.toString(),auditStatus,null);

        return "";
    }


}
