package com.enation.app.javashop.manager.api.goods;

import com.enation.app.javashop.core.goods.model.dto.GoodsQueryParam;
import com.enation.app.javashop.core.goods.model.enums.Permission;
import com.enation.app.javashop.core.goods.model.vo.GoodsSelectLine;
import com.enation.app.javashop.core.goods.model.vo.CopyGoodsResult;
import com.enation.app.javashop.core.goods.service.GoodsManager;
import com.enation.app.javashop.core.goods.service.GoodsQueryManager;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 商品控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-21 11:23:10
 */
@RestController
@RequestMapping("/admin/goods")
@Api(description = "商品相关API")
@Validated
public class GoodsManagerController {

	@Autowired
	private GoodsManager goodsManager;
	@Autowired
	private GoodsQueryManager goodsQueryManager;

	@ApiOperation(value = "查询商品或者审核列表")
	@GetMapping
	public Page list(GoodsQueryParam param,@ApiIgnore Integer pageSize,@ApiIgnore Integer pageNo) {

		param.setPageNo(pageNo);
		param.setPageSize(pageSize);
		return this.goodsQueryManager.list(param);
	}


	@ApiOperation(value = "管理员下架商品",notes = "管理员下架商品时使用")
	@ApiImplicitParams({
		@ApiImplicitParam(name="goods_id",value="商品ID",required=true,paramType="path",dataType="int"),
		@ApiImplicitParam(name="reason",value="下架理由",required=true,paramType="query",dataType="string")
	})
	@PutMapping(value = "/{goods_id}/under")
	public String underGoods(@PathVariable("goods_id") Integer goodsId,@NotEmpty(message = "下架原因不能为空") String reason){

		this.goodsManager.under(new Integer[]{goodsId},reason,Permission.ADMIN);

		return null;
	}

	@ApiOperation(value = "管理员批量下架商品",notes = "管理员批量下架商品时使用")
	@ApiImplicitParams({
			@ApiImplicitParam(name="goods_ids",value="商品ID",required=true,paramType="path",dataType="int"),
			@ApiImplicitParam(name="reason",value="下架理由",required=true,paramType="query",dataType="string")
	})
	@PutMapping(value = "/batch/under/{goods_ids}")
	public String batchUnderGoods(@PathVariable("goods_ids") Integer[] goodsIds,String reason){
		if(StringUtil.isEmpty(reason)){
			reason = "管理平台批量自行下架，无原因";
		}
		this.goodsManager.under(goodsIds,reason,Permission.ADMIN);
		return "操作成功";
	}

	@ApiOperation(value = "管理员上架商品",notes = "管理员上架商品时使用")
	@ApiImplicitParams({
		@ApiImplicitParam(name="goods_id",value="商品ID",required=true,paramType="path",dataType="int"),
	})
	@PutMapping(value = "/{goods_id}/up")
	public String unpGoods(@PathVariable("goods_id") Integer goodsId){

		this.goodsManager.up(goodsId);

		return null;
	}

	@ApiOperation(value = "管理员批量上架商品",notes = "管理员批量上架商品时使用")
	@ApiImplicitParams({
			@ApiImplicitParam(name="goods_ids",value="商品ID",required=true,paramType="path",dataType="int"),
	})
	@PutMapping(value = "/batch/up/{goods_ids}")
	public String batchUnpGoods(@PathVariable("goods_ids") Integer[] goodsIds){

		if(goodsIds!=null&&goodsIds.length>0){
			for (Integer goodsId:goodsIds){
				this.goodsManager.up(goodsId);
			}
		}

		return "操作成功";
	}


	@ApiOperation(value = "管理员审核商品",notes = "审核商品时使用")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "goods_id", value = "商品id", required = true, dataType = "int", paramType = "query"),
		@ApiImplicitParam(name = "pass", value = "是否通过审核 1通过 0 未通过", required = true, dataType = "int", paramType = "query"),
		@ApiImplicitParam(name = "message", value = "审核备注", required = false, dataType = "string", paramType = "query") })
	@PutMapping(value = "/{goods_id}/auth")
	public String auth(@ApiIgnore@PathVariable("goods_id") Integer goodsId,
					   @Min(value = 0,message = "审核状态值不正确")
					   @Max(value = 1,message = "审核状态值不正确")
					   @NotNull(message = "审核状态不能为空") Integer pass, String message) {

		this.goodsManager.authGoods(goodsId, pass, message);

		return null;
	}

	@GetMapping(value = "/{goods_ids}/details")
	@ApiOperation(value = "查询多个商品的基本信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "goods_ids", value = "要查询的商品主键", required = true, dataType = "int", paramType = "path",allowMultiple = true) })
	public List<GoodsSelectLine> getGoodsDetail(@PathVariable("goods_ids") Integer[] goodsIds) {

		return this.goodsQueryManager.query(goodsIds,null);
	}

	@PostMapping(value = "/copyGoods")
	@ApiOperation(value = "复制商品")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "goodsIds", value = "要复制的商品主键", required = true, dataType = "string", allowMultiple = true),
			@ApiImplicitParam(name = "sellerId", value = "目标店铺ID", required = true, dataType = "int")})
	public CopyGoodsResult copyGoods(@RequestParam(value = "goodsIds")String goodsIds, @RequestParam(value = "sellerId")Integer sellerId){
		return this.goodsManager.copyGoods(goodsIds,sellerId);
	}
}
