package com.enation.app.javashop.manager.api.member;

import com.enation.app.javashop.core.distribution.exception.DistributionErrorCode;
import com.enation.app.javashop.core.distribution.exception.DistributionException;
import com.enation.app.javashop.core.distribution.model.vo.WithdrawApplyVO;
import com.enation.app.javashop.core.member.model.dto.WithdrawRecordQueryParam;
import com.enation.app.javashop.core.member.service.WithdrawRecordManager;
import com.enation.app.javashop.framework.database.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 商家提现控制器
 * @author 孙建
 */
@Api(description = "商家提现控制器")
@RestController
@RequestMapping("/admin/members/withdraw")
public class WithdrawSellerManagerController {

    @Autowired
    private WithdrawRecordManager withdrawRecordManager;

    protected final Log logger = LogFactory.getLog(this.getClass());

    private static final String PATTERN = "yyyy-MM-dd HH:mm:ss";


    @ApiOperation("提现申请审核列表")
    @GetMapping(value = "/apply/list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_size", value = "页码大小", required = false, paramType = "query", dataType = "int", allowMultiple = false),
            @ApiImplicitParam(name = "page_no", value = "页码", required = false, paramType = "query", dataType = "int", allowMultiple = false),
            @ApiImplicitParam(name = "keyword", value = "关键字", required = false, paramType = "query", dataType = "String", allowMultiple = false),
            @ApiImplicitParam(name = "start_time", value = "开始时间", required = false, paramType = "query", dataType = "long", allowMultiple = false),
            @ApiImplicitParam(name = "end_time", value = "结束时间", required = false, paramType = "query", dataType = "long", allowMultiple = false),
            @ApiImplicitParam(name = "status", value = "状态 全部的话，不要传递参数即可 APPLY:申请中/VIA_AUDITING:审核成功/FAIL_AUDITING:审核失败/RANSFER_ACCOUNTS:已转账 ", required = false, paramType = "query", dataType = "String", allowMultiple = false)
    })
    public Page<WithdrawApplyVO> pageList(@ApiIgnore Long startTime, @ApiIgnore Long endTime,
                                          @ApiIgnore String keyword, @ApiIgnore Integer pageNo,
                                          @ApiIgnore Integer pageSize, String status) {
        // 查询列表
        WithdrawRecordQueryParam withdrawRecordQueryParam = new WithdrawRecordQueryParam();
        withdrawRecordQueryParam.setPageNo(pageNo);
        withdrawRecordQueryParam.setPageSize(pageSize);
        withdrawRecordQueryParam.setKeyword(keyword);
        withdrawRecordQueryParam.setStatus(status);
        withdrawRecordQueryParam.setStartTime(startTime);
        withdrawRecordQueryParam.setEndTime(endTime);
        return withdrawRecordManager.list(withdrawRecordQueryParam);
    }


    @PostMapping(value = "/auditing")
    @ApiOperation("审核提现申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "withdraw_record_id", value = "提现记录id", required = true, paramType = "query", dataType = "int", allowMultiple = false),
            @ApiImplicitParam(name = "remark", value = "备注", required = false, paramType = "query", dataType = "String", allowMultiple = false),
            @ApiImplicitParam(name = "audit_result", value = "申请结果 VIA_AUDITING：审核通过/FAIL_AUDITING：审核拒绝", required = true, paramType = "query", dataType = "String", allowMultiple = false),
    })
    public void auditing(@ApiIgnore int withdrawRecordId, String remark, @ApiIgnore String auditResult) {
        try {
            this.withdrawRecordManager.auditing(withdrawRecordId, remark, auditResult);
        } catch (DistributionException e) {
            logger.error("提现审核异常：", e);
            throw e;
        } catch (Exception e) {
            logger.error("提现审核异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }


    @ApiOperation("标记为已转账")
    @PostMapping(value = "/markTransfer")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "withdraw_record_id", value = "提现记录id", required = true, paramType = "query", dataType = "int", allowMultiple = false),
            @ApiImplicitParam(name = "remark", value = "备注", required = false, paramType = "query", dataType = "int", allowMultiple = false),
    })
    public void markTransfer(@ApiIgnore int withdrawRecordId, String remark) {
        try {
            this.withdrawRecordManager.transfer(withdrawRecordId, remark);
        } catch (DistributionException e) {
            logger.error("标记为已转账异常：", e);
            throw e;
        } catch (Exception e) {
            logger.error("标记为已转账异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }

    }
}
