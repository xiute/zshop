package com.enation.app.javashop.manager.api.account;

import com.dag.eagleshop.core.account.model.dto.account.QueryTradeRecordDTO;
import com.dag.eagleshop.core.account.model.dto.base.PageDTO;
import com.dag.eagleshop.core.account.model.dto.base.SortField;
import com.dag.eagleshop.core.account.model.dto.base.ViewPage;
import com.dag.eagleshop.core.account.service.AccountManager;
import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 账户控制器
 * @author 孙建
 */
@Api(description = "账户接口模块")
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountManager accountManager;

    @ApiOperation(value = "查询交易记录列表")
    @PostMapping(value = "/queryTradeRecordList" )
    public JsonBean queryTradeRecordList(@RequestBody PageDTO<QueryTradeRecordDTO> pageDTO){
        // 查询交易记录列表
        QueryTradeRecordDTO queryWithdrawBillDTO = pageDTO.getBody();
        pageDTO.setOrderBys(new SortField[]{ new SortField(false, "create_time") });
        ViewPage viewPage = accountManager.queryTradeRecordList(pageDTO);
        return new JsonBean("查询成功", viewPage);
    }

}
