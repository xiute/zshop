package com.enation.app.javashop.core.wms.model.enums;

import lombok.Data;

/**
 * @author JFeng
 * @date 2020/9/28 11:19
 */
public enum WmsExportType {

    /**
     * 分拣清单
     */
    SKU_ORDER,

    /**
     * 采购汇总单
     */
    SKU_SUPPLIER,

    /**
     * 库位站点对照表
     */
    ORDER_SORTING,

    /**
     * 配送汇总单
     */
    ORDER_SITE;




}
