package com.enation.app.javashop.core.trade.order.model.vo;

import com.enation.app.javashop.core.goods.model.vo.SpecValueVO;
import com.enation.app.javashop.core.promotion.coupon.model.dos.CouponDO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartPromotionVo;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.List;

/**
 * 订单商品项
 * @author Snow create in 2018/5/15
 * @version v2.0
 * @since v7.0.0
 */
@ApiModel( description = "订单商品项")
@Data
public class ShareOrderVO {

    @ApiModelProperty(value = "订单详情")
    private OrderDetailVO orderDetail;

    @ApiModelProperty(value = "是否可以领券")
    private Boolean canReceive;

    @ApiModelProperty(value = "可领取优惠券")
    private CouponDO coupon;

    @ApiModelProperty(value = "优惠金额")
    private Double discountPrice;


}