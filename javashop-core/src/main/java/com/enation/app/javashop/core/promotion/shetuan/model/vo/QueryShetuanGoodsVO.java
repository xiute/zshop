package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class QueryShetuanGoodsVO {
    @ApiModelProperty(name = "shetuan_id", value = "社团活动id")
    @NotNull(message = "活动id不能为空")
    private Integer shetuanId;

    @ApiModelProperty(name = "page_size", value = "每页记录条数")
    private Integer pageSize;
    @ApiModelProperty(name = "page_no", value = "页码")
    private Integer pageNo;

    @ApiModelProperty(value = "关键字")
    private String keyword;

    @ApiModelProperty(value = "店铺分组")
    private Integer shopCatId;

    @ApiModelProperty(value = "社团商品状态")
    private String status;

    @ApiModelProperty(value = "排序字段")
    private String sortFiled;

    @ApiModelProperty(value = "参团数量")
    private Integer goodsNum;

    @ApiModelProperty(name = "goods_ids", value = "商品ID")
    private String goodsIds;

}
