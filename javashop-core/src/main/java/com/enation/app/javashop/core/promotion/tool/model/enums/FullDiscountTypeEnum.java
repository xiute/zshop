package com.enation.app.javashop.core.promotion.tool.model.enums;

/**
 * 满减活动优惠门槛类型
 */
public enum FullDiscountTypeEnum {

	/**
	 * 满xxx元
	 */
	MONEY,

	/**
	 * 满xxx件
	 */
	NUM;

}
