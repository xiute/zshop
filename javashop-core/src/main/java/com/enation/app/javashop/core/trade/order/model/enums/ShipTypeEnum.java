package com.enation.app.javashop.core.trade.order.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 发货状态
 * @author Snow
 * @version 1.0
 * @since v7.0.0
 * 2017年3月31日下午2:44:54
 */
public enum ShipTypeEnum {

	/**
	 * 本地同城
	 */
	LOCAL("LOCAL",1,"同城配送"),

	/**
	 * 快递配送
	 */
	GLOBAL("GLOBAL",2,"快递配送"),

	/**
	 * 客户自提
	 */
	SELF("SELF",3,"客户自提");

	private String index;
	private Integer text;
	private String desc;

	private static Map<String, Integer> enumMap = new HashMap<>();

	static {
		for (ShipTypeEnum item : values()) {
			enumMap.put(item.getIndex(), item.getText());
		}
	}

	ShipTypeEnum(String index, int text,String desc){
		this.index = index;
		this.text = text;
		this.desc = desc;
	}


	public String getIndex() {
		return index;
	}

	public int getText() {
		return text;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static int getTextByIndex(String index){
		return enumMap.get(index);
	}



}
