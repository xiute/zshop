package com.enation.app.javashop.core.member.model.vo;

import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author JFENG
 * @version 1.0
 * @Description:
 * @date 2020/4/2 18:28
 */
@Data
@ApiModel
public class MemberShopVO  extends MemberVO{
    @ApiModelProperty(name="shop",value="店铺")
    private ShopVO shop;
}
