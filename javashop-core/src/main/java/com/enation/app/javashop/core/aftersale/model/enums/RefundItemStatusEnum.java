package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * @author 王志杨
 * @since 2020/10/29 15:38
 * 退款单明细状态
 */
public enum RefundItemStatusEnum {
    //申请中
    APPLY("申请中"),
    //已失效
    DISABLED("已失效"),
    //退款成功
    SUCCESS("退款成功"),
    //退款失败
    FAIL("退款失败");

    private String description;

    RefundItemStatusEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String value() {
        return this.name();
    }
}
