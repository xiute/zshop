package com.enation.app.javashop.core.shop.model.dos;

import com.enation.app.javashop.core.shop.model.dto.PeekTime;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class TemplateFeeSetting implements Serializable {

    private static final long serialVersionUID = 6348162955890093L;


    @ApiParam("费用配置类型 1.基础距离配送费计算 2.距离区间配送费计算")
    private Integer  feeType;
    @ApiParam("基础距离")
    private BigDecimal baseDistance;
    @ApiParam("基础价格")
    private BigDecimal  basePrice;
    @ApiParam("单位距离")
    private BigDecimal  unitDistance;
    @ApiParam("单位费用")
    private BigDecimal  unitPrice;
    @ApiParam("距离段配送费")
    private List<DistanceSection> distanceSections;

    @ApiParam("加收时段")
    private List<PeekTime> peekTimes;


}
