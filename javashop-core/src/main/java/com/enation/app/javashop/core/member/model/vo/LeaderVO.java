package com.enation.app.javashop.core.member.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class LeaderVO {

    @ApiModelProperty(name ="member_id",value = "会员ID")
    private Integer memberId;

    @ApiModelProperty(name = "facade_pic_url",value = "门面图片")
    private String facadePicUrl;

    @ApiModelProperty(name = "leader_id",value = "团长ID")
    private Integer leaderId;

    @ApiModelProperty(name = "leader_name",value = "团长名")
    private String leaderName;

    @ApiModelProperty(name = "midentity", value = "身份证号", required = false)
    private String midentity;

    @ApiModelProperty(name = "leader_mobile",value = "团长手机号")
    private String leaderMobile;

    @ApiModelProperty(name = "leader_type",value = "团长类型 1自提点 2配送点")
    private Integer leaderType;

    @ApiModelProperty(name = "street",value = "街道")
    private String street;

    @ApiModelProperty(name = "county",value = "县名称")
    private String county;
    @ApiModelProperty(name = "address",value = "地址")
    private String address;

    @ApiModelProperty(name = "cell_name",value = "小区名称")
    private String cellName;

    @ApiModelProperty(name = "site_name",value = "站点名称")
    private String siteName;

    @ApiModelProperty(name = "site_type",value = "站点类型")
    private Integer siteType;

    @ApiModelProperty(name="join_reason",value = "加入原因")
    private String joinReason;

    @ApiModelProperty(name = "lat",value = "纬度")
    private Double lat;

    @ApiModelProperty(name = "lng",value = "经度")
    private Double lng;

    @ApiModelProperty(name = "work_time", value = "营业时间")
    private String workTime;

    @ApiModelProperty(name = "audit_status",value = "审核状态")
    private Integer auditStatus;

    @ApiModelProperty(name = "face",value = "会员头像")
    private String face;

    @ApiModelProperty(name = "operate_areas",value = "运营区域")
    private String operateAreas;

    @ApiModelProperty(name = "ship_priority",value = "配送优先级")
    private Integer shipPriority;

    @ApiModelProperty(name = "dispatch_type",value = "分拣配送方式")
    private String dispatchType;

    // 描述一
    private String describe1;
    // 描述二
    private String describe2;


    @ApiModelProperty(name = "remarks", value = "备注", required = false)
    private String remarks;


}
