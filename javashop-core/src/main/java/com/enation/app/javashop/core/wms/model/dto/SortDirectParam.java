package com.enation.app.javashop.core.wms.model.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author JFeng
 * @date 2020/9/23 11:43
 */
@Data
public class SortDirectParam {
    private Date startTime;
    private Date endTime;
    // 批次数量
    private Integer batchSize;

    private Integer batchNum;

    private Integer sellerId;

    private Integer receiveTimeSeq;
}
