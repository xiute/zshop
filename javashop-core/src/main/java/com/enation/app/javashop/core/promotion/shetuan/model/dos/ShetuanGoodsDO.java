package com.enation.app.javashop.core.promotion.shetuan.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 拼团商品实体
 *
 * @author admin
 * @version vv1.0.0
 * @since vv7.1.0
 * 2019-01-22 11:20:56
 */
@Table(name = "es_shetuan_goods")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShetuanGoodsDO  implements Serializable {

    private static final long serialVersionUID = 4796645552318671313L;


    @Id(name= "id")
    @ApiModelProperty(name = "id",value = "主键")
    private Integer id;

    /**
     * shetuan_id
     */
    @Column(name = "shetuan_id")
    @NotEmpty(message = "活动id为空")
    @ApiModelProperty(name = "shetuan_id", value = "shetuan_id")
    private Integer shetuanId;
    /**
     * sku_id
     */
    @Column(name = "sku_id")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "sku_id", value = "sku_id")
    private Integer skuId;

    /**
     * goods_id
     */
    @Column(name = "goods_id")
    @ApiModelProperty(name = "goods_id", value = "goods_id")
    private Integer goodsId;


    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "卖家id")
    private Integer sellerId;

    /**
     * 卖家名称
     */
    @Column(name = "seller_name")
    @ApiModelProperty(name = "seller_name", value = "卖家名称", hidden = true)
    private String sellerName;

    /**
     * 商品名称
     */
    @Column(name = "goods_name")
    @ApiModelProperty(name = "goods_name", value = "商品名称", required = true)
    private String goodsName;
    /**
     * 结算价格
     */
    @Column(name = "settle_price")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "settle_price", value = "结算价格")
    private Double settlePrice;
    /**
     * 原价
     */
    @Column(name = "origin_price")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "origin_price", value = "原价")
    private Double originPrice;
    /**
     * 店铺售价
     */
    @Column(name = "sales_price")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "sales_price", value = "活动价")
    private Double salesPrice;
    /**
     * 社区团购价
     */
    @Column(name = "shetuan_price")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "shetuan_price", value = "社区团购价")
    private Double shetuanPrice;
    /**
     * sn 商品编号
     */
    @Column(name = "sn")
    @ApiModelProperty(name = "sn", value = "sn", required = true)
    private String sn;

    @Column(name = "specs")
    @ApiModelProperty(name = "specs", value = "规格信息json")
    @JsonRawValue
    private String specs;

    @Column(name = "thumbnail")
    @ApiModelProperty(name = "thumbnail", value = "商品图片")
    private String thumbnail;

    @Column(name = "video_url")
    @ApiModelProperty(name = "video_url", value = "商品视频")
    private String videoUrl;

    @Column(name = "status")
    @ApiModelProperty(name = "status", value = "状态")
    private Integer status;

    @Column(name="first_rate")
    @ApiModelProperty(name="first_rate",value = "团长一级分佣金")
    private Double firstRate;

    @Column(name="second_rate")
    @ApiModelProperty(name="second_rate",value = "团长二级分佣金")
    private Double secondRate;

    @Column(name="third_rate")
    @ApiModelProperty(name="third_rate",value = "团长三级分佣金")
    private Double thirdRate;

    @Column(name="invite_rate")
    @ApiModelProperty(name="invite_rate",value = "邀请佣金")
    private Double inviteRate;

    @Column(name="self_raising_rate")
    @ApiModelProperty(name="self_raising_rate",value = "自提点佣金")
    private Double selfRaisingRate;

    @Column(name = "subsidy_rate")
    @ApiModelProperty(name = "subsidy_rate", value = "补贴比例", required = true)
    private Double subsidyRate;

    @Column(name = "shop_cat_id")
    @ApiModelProperty(name = "shop_cat_id", value = "店铺分组")
    private Integer shopCatId;


    /**商品总数*/
    @Column(name = "goods_num")
    @NotNull(message = "请输入活动商品总数")
    @ApiModelProperty(name="goods_num",value="商品总数",required=false)
    private Integer goodsNum;

    /**虚拟数量*/
    @Column(name = "visual_num")
    @NotNull(message = "请输入虚拟数量")
    @ApiModelProperty(name="visual_num",value="虚拟数量",required=false)
    private Integer visualNum;
    /**虚拟数量*/
    @Column(name = "mock_num")
    @NotNull(message = "请输入虚拟数量")
    @ApiModelProperty(name="mock_num",value="虚拟数量",required=false)
    private Integer mockNum;

    /**限购数量*/
    @Column(name = "limit_num")
    @NotNull(message = "请输入限购数量")
    @ApiModelProperty(name="limit_num",value="限购数量",required=false)
    private Integer limitNum;

    /**已团购数量*/
    @Column(name = "buy_num")
    @ApiModelProperty(name="buy_num",value="已团购数量",required=false)
    private Integer buyNum;

    /**店铺分组名称*/
    @Column(name = "shop_cat_name")
    private String  shopCatName;

    @Column(name = "create_time")
    @ApiModelProperty(name="create_time",value="创建时间",required=false)
    private Long createTime;

    @Column(name = "priority")
    @ApiModelProperty(name="priority",value="排序",required=false)
    private Integer priority;

    @Column(name = "shop_cat_items")
    @ApiModelProperty(name="shop_cat_items",value="店铺多分组",required=false)
    private String shopCatItems;

    @Column(name = "cost")
    @ApiModelProperty(name = "cost", value = "成本价格", required = true)
    @Max(value = 99999999, message = "成本价格不能超过99999999")
    private Double cost;

}
