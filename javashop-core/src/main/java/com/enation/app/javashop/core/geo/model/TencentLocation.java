package com.enation.app.javashop.core.geo.model;

import lombok.Data;

import java.util.List;

@Data
public class TencentLocation {
    private String address;
    private  LatLng location;
    private FormattedAddresses formattedAddresses;
    private  AddressComponent addressComponent;
    private  AddressReference addressReference;

    private List<TencentPois> pois;
}

@Data
class LatLng {
    private Double lat;
    private Double lng;
}

@Data
class FormattedAddresses {
    private String  recommend;
    private String  rough;
}
@Data
class TencentPois {
    private String  title;
    private String address;
    private Double _distance;
    private AddressComponent adInfo;
    private LatLng location;
}
