package com.enation.app.javashop.core.promotion.pintuan.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by kingapex on 2019-02-12.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-02-12
 */
@Data
public class PintuanOrderDetailVo extends PintuanOrder {

    @ApiModelProperty(name="origin_price",value="原价")
    private Double originPrice;

    @ApiModelProperty(name="sales_price",value="拼团价")
    private Double salesPrice;

    @ApiModelProperty(name="left_time",value="拼团活动剩余秒数")
    private Long leftTime;

    @ApiModelProperty(name="lack_people_num",value="成团缺少人数")
    private Integer lackPeopleNum;

    @ApiModelProperty(name="pin_tuan_goods",value="拼团商品")
    private PinTuanGoodsVO pinTuanGoods;

}
