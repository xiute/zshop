package com.enation.app.javashop.core.goods.model.enums;

/**
 * @Author: zhou
 * @Date: 2020/11/18
 * @Description:
 */
public enum ResultCodeEnum {
    S200(200, "操作成功"),
    E601(601, "未选中商品"),
    E602(602,"店铺未启用"),
    E603(603,"店铺经营类目为空"),
    E604(604,"目标店铺没有分类");
    private int code;
    private String description;

    ResultCodeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String value() {
        return this.name();
    }
}
