package com.enation.app.javashop.core.system.model.dos;

import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2020/11/12 12:02
 * 微信公众号发送模板消息Data封装实体
 */
public class WxPublicTemplateData implements Serializable {

    //值
    private String value;
    //字体颜色
    private String color;

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    public WxPublicTemplateData(String value, String color) {
        this.value = value;
        this.color = color;
    }

    public WxPublicTemplateData() {
    }
}
