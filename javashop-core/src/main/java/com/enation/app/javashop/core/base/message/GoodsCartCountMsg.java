package com.enation.app.javashop.core.base.message;

import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2020/9/11
 * @Description:商品添加购物车统计消息
 */
@Data
public class GoodsCartCountMsg implements Serializable {

    private static final long serialVersionUID = 7593092179624790423L;
    //
    private Integer memberId;
    //商品sku
    private GoodsSkuVO skuVO;
    //添加购物车数量
    private Integer num;

    public GoodsCartCountMsg() {
    }
}
