package com.enation.app.javashop.core.client.trade;

/**
 * 促销活动客户端
 *
 * @author zh
 * @version v7.0
 * @date 19/3/28 上午11:10
 * @since v7.0
 */
public interface PromotionGoodsClient {
    /**
     * 删除促销活动商品
     *
     * @param goodsId    商品id
     * @param type       活动类型
     * @param activityId 活动id
     */
    void delPromotionGoods(Integer goodsId, String type, Integer activityId);


    /**
     * @Author liuyulei
     * @Description 删除促销活动商品
     * @Date 上午10:50 19-8-1
     * @Param [goodsId]
     * @return void
     **/
    void delPromotionGoods(Integer goodsId);
}
