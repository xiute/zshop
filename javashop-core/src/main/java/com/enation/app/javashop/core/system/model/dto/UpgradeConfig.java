package com.enation.app.javashop.core.system.model.dto;

import lombok.Data;

@Data
public class UpgradeConfig {

    /**
     * APP名称
     */
    private String appName;
    /**
     * app类型
     */
    private Integer appType;
    /**
     * 最新版本
     */
    private String lastVersion;
    /**
     * 不需升级版本
     */
    private String withoutVersion;
    /**
     * 版本升级内容
     */
    private String upgradeContent;
    /**
     * 下载路径
     */
    private String downloadUrl;


}
