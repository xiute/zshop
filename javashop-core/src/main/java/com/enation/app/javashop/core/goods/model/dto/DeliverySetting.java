package com.enation.app.javashop.core.goods.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;

/**
 * @author JFeng
 * @date 2020/8/8 14:12
 */
@Data
public class DeliverySetting {

    @ApiModelProperty(value = "是否本地商品")
    private Boolean isLocal;

    @ApiModelProperty(value = "是否商城商品")
    private Boolean isGlobal;

    @ApiModelProperty(name = "is_self_take", value = "是否自提", required = false)
    private Boolean isSelfTake;

    @ApiModelProperty(name = "is_self_take", value = "运费模板id,不需要运费模板时值是0", required = false)
    @Min(value = 0, message = "运费模板值不正确")
    private Integer templateId;

    @ApiModelProperty(name = "local_template_id", value = "运费模板id,不需要运费模板时值是0", required = false)
    private Integer localTemplateId;

    @ApiModelProperty(name = "freight_pricing_way", value = "运费定价方式 0.统一运费 1.模板运费", required = false)
    private Integer freightPricingWay;

    /** 快递运费一口价 */
    @ApiModelProperty(name = "freight_unified_price", value = "运费统一价", required = false)
    private Double freightUnifiedPrice;

    @ApiModelProperty(name = "local_freight_pricing_way", value = "同城运费定价方式 0.统一运费 1.模板运费", required = false)
    private Integer localFreightPricingWay;

    /** 运费一口价 */
    @ApiModelProperty(name = "local_freight_unified_price", value = "同城运费统一价", required = false)
    private Double localFreightUnifiedPrice;


}
