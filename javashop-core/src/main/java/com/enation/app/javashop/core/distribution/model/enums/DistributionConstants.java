package com.enation.app.javashop.core.distribution.model.enums;

/**
 * 分销常量类
 */
public class DistributionConstants {

    /** 分销关系过期时间 生产2592000 30天 测试 */
    public static final int EXPIRE_TIME = 2592000;

}
