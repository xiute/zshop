package com.enation.app.javashop.core.passport.dto;

import lombok.Data;

/**
 * @author ChienSun
 * @date 2019/2/19
 */
@Data
public class DataDetailDTO {

    public DataDetailDTO(){}

    public DataDetailDTO(String value, String color) {
        this.value = value;
        this.color = color;
    }

    // 内容
    private String value;

    // 模板内容字体颜色，不填默认为黑色
    private String color;

}
