package com.enation.app.javashop.core.wms.model.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author JFeng
 * @date 2020/7/17 14:31
 */

@Data
public class ExportOrderSortingVO {

    @ExcelProperty("订单号")
    private String orderSn;

    @ExcelProperty("单号(配送系统用)")
    private String dispatchNo;

    @ExcelProperty("批次号")
    private Integer sortingBatch;

    @ExcelProperty("格子号")
    private Integer sortingSeq;

    @ExcelProperty("自提点")
    private String siteName;

    @ExcelProperty("订单类型")
    private String orderType;

    @ExcelProperty("分拣备注")
    private String remarks;

}
