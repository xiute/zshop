package com.enation.app.javashop.core.thirdParty.model.dto;

public class ZheretaoGoodsStatus {
    //上架
    public static final String ENABLED="ENABLED";
    //下架
    public static final String UNENABLED="UNENABLED";
}
