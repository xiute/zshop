package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 申请开店VO
 * @author 孙建
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class SubmitApplyVO {

	@ApiModelProperty(name="shop_name",value="店铺名称",required=true)
	@NotEmpty(message="店铺名称必填")
	private String shopName;

	@ApiModelProperty(name="goods_management_category",value="店铺经营类目",required=false)
	private String goodsManagementCategory;

	@ApiModelProperty(name="shop_tag",value="店铺类型",required=false)
	private String shopTag;

	@ApiModelProperty(name = "shop_logo", value = "店铺logo", required = true)
	@NotEmpty(message="店铺logo必填")
	private String shopLogo;

	@ApiModelProperty(name="shop_province",value="店铺所在省",required=false)
	private String shopProvince;

	@ApiModelProperty(name="shop_city",value="店铺所在市",required=false)
	private String shopCity;

	@ApiModelProperty(name="shop_county",value="店铺所在县",required=false)
	private String shopCounty;

	@ApiModelProperty(name="shop_town",value="店铺所在镇",required=false)
	private String shopTown;

	@ApiModelProperty(name = "shop_add", value = "店铺详细地址", required = false)
	private String shopAdd;

	@ApiModelProperty(name="link_name",value="联系人姓名",required=true)
	@NotEmpty(message="联系人姓名必填")
	private String linkName;

	@ApiModelProperty(name="link_phone",value="联系人电话",required=true)
	@NotEmpty(message="联系人电话必填")
	private String linkPhone;

	@ApiModelProperty(name = "licence_img", value = "营业执照电子版", required = true)
	@NotEmpty(message = "营业执照电子版必填")
	private String licenceImg;


	@ApiModelProperty(name = "legal_img", value = "身份证正面照片", required = true)
	@NotEmpty(message = "身份证正面照片必填")
	private String legalImg;


	@ApiModelProperty(name = "legal_back_img", value = "身份证反面照片", required = true)
	@NotEmpty(message = "身份证反面照片必填")
	private String legalBackImg;


	@ApiModelProperty(name = "hold_img", value = "手持身份证照片", required = true)
	@NotEmpty(message = "手持身份证照片必填")
	private String holdImg;

	@ApiModelProperty(name = "shop_type", value = "店铺类型", required = false)
	private String shopType;

	@ApiModelProperty(name = "shop_lat", value = "店铺纬度", required = false)
	private Double shopLat;

	@ApiModelProperty(name = "shop_lng", value = "店铺经度", required = false)
	private Double shopLng;



}
