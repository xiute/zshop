package com.enation.app.javashop.core.goodssearch.service.impl;

import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.goods.GoodsWordsClient;
import com.enation.app.javashop.core.client.member.ShopCatClient;
import com.enation.app.javashop.core.goods.model.dos.CategoryDO;
import com.enation.app.javashop.core.goodssearch.enums.GoodsWordsType;
import com.enation.app.javashop.core.goodssearch.model.GoodsIndex;
import com.enation.app.javashop.core.goodssearch.model.Param;
import com.enation.app.javashop.core.goodssearch.service.GoodsIndexManager;
import com.enation.app.javashop.core.goodssearch.util.HexUtil;
import com.enation.app.javashop.core.shop.model.dos.ShipLocalTemplateVO;
import com.enation.app.javashop.core.shop.model.dos.ShopCatDO;
import com.enation.app.javashop.core.shop.model.dos.ShopDetailDO;
import com.enation.app.javashop.core.shop.service.ShipTemplateLocalManager;
import com.enation.app.javashop.core.shop.service.ShopManager;
import com.enation.app.javashop.core.system.progress.model.TaskProgress;
import com.enation.app.javashop.core.system.progress.model.TaskProgressConstant;
import com.enation.app.javashop.core.system.progress.service.ProgressManager;
import com.enation.app.javashop.framework.elasticsearch.EsConfig;
import com.enation.app.javashop.framework.elasticsearch.EsSettings;
import com.enation.app.javashop.framework.logs.Debugger;
import com.enation.app.javashop.framework.logs.Logger;
import com.enation.app.javashop.framework.logs.LoggerFactory;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeAction;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeRequestBuilder;
import org.elasticsearch.action.admin.indices.analyze.AnalyzeResponse.AnalyzeToken;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * es的商品索引实现
 *
 * @author fk
 * @version v6.4
 * @since v6.4
 * 2017年9月18日 上午11:41:44
 */
@Service
public class GoodsIndexManagerImpl implements GoodsIndexManager {


    @Autowired
    protected GoodsClient goodsClient;
    @Autowired
    protected ShopCatClient shopCatClient;
    @Autowired
    protected ProgressManager progressManager;
    @Autowired
    protected ElasticsearchTemplate elasticsearchOperations;
    @Autowired
    private ShopManager shopManager;
    @Autowired
    private ShipTemplateLocalManager shipTemplateLocalManager;

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected GoodsWordsClient goodsWordsClient;

    @Autowired
    protected EsConfig esConfig;

    @Autowired
    protected Debugger debugger;

    @Override
    public void addIndex(Map goods) {
        // 店铺信息
        ShopDetailDO shopDetail = null;
        if (goods.get("shop_lat") == null) {
            shopDetail = shopManager.getShopDetail(Integer.valueOf(goods.get("seller_id").toString()));
            this.composeShopInfo(goods, shopDetail);
        }
        // 同城配送信息
        if (goods.get("local_template_id") != null && goods.get("ship_fee_setting") == null) {
            int localTemplateId = Integer.valueOf(goods.get("local_template_id").toString());
            ShipLocalTemplateVO shipTemplate = shipTemplateLocalManager.getFromCache(localTemplateId);
            if(shipTemplate != null){
                goods.put("minTongChengPrice", shipTemplate.getMinShipPrice() == null ? 0 : shipTemplate.getMinShipPrice());
                this.composeShipInfo(goods, shipTemplate);
            }
        }
        String goodsName = goods.get("goods_name").toString();
        try {

            //配置文件中定义的索引名字
            String indexName = esConfig.getIndexName() + "_" + EsSettings.GOODS_INDEX_NAME;

            GoodsIndex goodsIndex = this.getSource(goods);

            IndexQuery indexQuery = new IndexQuery();
            indexQuery.setIndexName(indexName);
            indexQuery.setType(EsSettings.GOODS_TYPE_NAME);
            indexQuery.setId(goodsIndex.getGoodsId().toString());
            indexQuery.setObject(goodsIndex);

            //审核通过且没有下架且没有删除  ---->>> 添加分词统计
            boolean flag = goodsIndex.getDisabled() == 1 && goodsIndex.getMarketEnable() == 1 && goodsIndex.getIsAuth() == 1;
            if (flag) {
                List<String> wordsList = toWordsList(goodsName);

                // 分词入库
                this.wordsToDb(wordsList);
            }

            elasticsearchOperations.index(indexQuery);
            logger.info("=====================更新索引:" + goods.get("goods_Id").toString() + "【" + goods.get("goods_Name").toString() + "】" + "========================");
        } catch (Exception e) {
            logger.error("为商品[" + goodsName + "]生成索引异常", e);
            debugger.log("为商品[" + goodsName + "]生成索引异常", StringUtil.getStackTrace(e));
            throw new RuntimeException("为商品[" + goodsName + "]生成索引异常", e);
        }

    }

    @Override
    public void updateIndex(Map goods) {
        logger.info("更新商品[" + goods + "]生成索引异常");
        //删除
        this.deleteIndex(goods);
        //添加
        this.addIndex(goods);

    }

    @Override
    public void deleteIndex(Map goods) {

        //配置文件中定义的索引名字
        String indexName = esConfig.getIndexName() + "_" + EsSettings.GOODS_INDEX_NAME;
        elasticsearchOperations.delete(indexName, EsSettings.GOODS_TYPE_NAME, goods.get("goods_id").toString());

        String goodsName = goods.get("goods_name").toString();
        List<String> wordsList = toWordsList(goodsName);
        this.deleteWords(wordsList);

    }

    /**
     * 将list中的分词减一
     *
     * @param wordsList
     */
    protected void deleteWords(List<String> wordsList) {
        wordsList = removeDuplicate(wordsList);
        for (String words : wordsList) {
            this.goodsWordsClient.delete(words);
        }
    }

    /**
     * 封装成内存需要格式数据
     *
     * @param goods
     * @return
     */
    protected GoodsIndex getSource(Map goods) {
        long timestamp = DateUtil.getDateline();
        GoodsIndex goodsIndex = new GoodsIndex();
        goodsIndex.setTimestamp(timestamp);
        goodsIndex.setGoodsId(StringUtil.toInt(goods.get("goods_id").toString(), 0));
        goodsIndex.setGoodsName(goods.get("goods_name").toString());
        goodsIndex.setThumbnail(goods.get("thumbnail") == null ? "" : goods.get("thumbnail").toString());
        goodsIndex.setVideoUrl(goods.get("video_url")==null ? "" : goods.get("video_url").toString() );
        goodsIndex.setSelling(goods.get("selling")==null ? "" : goods.get("selling").toString() );
        goodsIndex.setSmall(goods.get("small") == null ? "" : goods.get("small").toString());
        Double p = goods.get("price") == null ? 0d : StringUtil.toDouble(goods.get("price").toString(), 0d);
        goodsIndex.setPrice(p);
        Double mktprice = goods.get("mktprice") == null ? 0d : StringUtil.toDouble(goods.get("mktprice").toString(), 0d);
        goodsIndex.setMktprice(mktprice);
        Double discountPrice = goods.get("discount_price") == null ? 0d : StringUtil.toDouble(goods.get("discount_price").toString(), 0d);
        goodsIndex.setDiscountPrice(discountPrice);
        goodsIndex.setBuyCount(goods.get("buy_count") == null ? 0 : StringUtil.toInt(goods.get("buy_count").toString(), 0));
        goodsIndex.setMetaDescription(goods.get("meta_description") == null ? "" : goods.get("meta_description").toString());
        goodsIndex.setSellerId(StringUtil.toInt(goods.get("seller_id").toString(), 0));
        //店铺分组
        goodsIndex.setShopCatId(goods.get("shop_cat_id") == null ? 0 : StringUtil.toInt(goods.get("shop_cat_id").toString(), 0));
        goodsIndex.setShopCatPath("");
        if (goodsIndex.getShopCatId() != 0) {
            ShopCatDO shopCat = shopCatClient.getModel(goodsIndex.getShopCatId());
            if (shopCat != null) {
                goodsIndex.setShopCatPath(HexUtil.encode(shopCat.getCatPath()));
            }
        }

        goodsIndex.setSellerName(goods.get("seller_name").toString());
        goodsIndex.setCommentNum(goods.get("comment_num") == null ? 0 : StringUtil.toInt(goods.get("comment_num").toString(), 0));
        goodsIndex.setGrade(goods.get("grade") == null ? 100 : StringUtil.toDouble(goods.get("grade").toString(), 100d));

        goodsIndex.setBrand(goods.get("brand_id") == null ? 0 : StringUtil.toInt(goods.get("brand_id"). toString(), 0));
        goodsIndex.setCategoryId(goods.get("category_id") == null ? 0 : StringUtil.toInt(goods.get("category_id").toString(), 0));
        CategoryDO cat = goodsClient.getCategory(Integer.parseInt(goods.get("category_id").toString()));
        if(cat!=null){
            goodsIndex.setCategoryPath(HexUtil.encode(cat.getCategoryPath()));
        }
        goodsIndex.setDisabled(StringUtil.toInt(goods.get("disabled").toString(), 0));
        goodsIndex.setMarketEnable(StringUtil.toInt(goods.get("market_enable").toString(), 0));
        goodsIndex.setIsAuth(StringUtil.toInt(goods.get("is_auth").toString(), 0));
        goodsIndex.setIntro(goods.get("intro") == null ? "" : goods.get("intro").toString());
        goodsIndex.setSelfOperated(goods.get("self_operated") == null ? 0 : StringUtil.toInt(goods.get("self_operated").toString(), 0));
        goodsIndex.setIsLocal(StringUtil.toInt(goods.get("is_local").toString(), 0));
        goodsIndex.setIsGlobal(StringUtil.toInt(goods.get("is_global").toString(), 0));
        goodsIndex.setIsSelfTake(StringUtil.toInt(goods.get("is_self_take").toString(), 0));

        //添加商品优先级维度
        goodsIndex.setPriority(goods.get("priority") == null ? 1 : StringUtil.toInt(goods.get("priority").toString(), 1));
        //参数维度,已填写参数
        List<Map> params = (List<Map>) goods.get("params");
        List<Param> paramsList = this.convertParam(params);
        goodsIndex.setParams(paramsList);

        goodsIndex.setCommunityShop(goods.get("community_shop")==null?0:StringUtil.toInt(goods.get("community_shop").toString()));

        // 店铺信息
        if (goods.get("shop_lat") != null && goods.get("shop_lng") != null) {
            goodsIndex.setLocation(new GeoPoint(Double.valueOf(goods.get("shop_lat").toString()), Double.valueOf(goods.get("shop_lng").toString())));
            goodsIndex.setShopLogo(goods.get("shop_logo").toString());
        }
        // 同城配送信息
        if (goodsIndex.getIsLocal()==1) {
            goodsIndex.setLocalTemplateId(goods.get("local_template_id").toString());
            goodsIndex.setShipFeeSetting(goods.get("ship_fee_setting").toString());
            goodsIndex.setShipTimeSetting(goods.get("ship_time_setting").toString());
            goodsIndex.setBaseShipPrice(Double.valueOf(goods.get("base_ship_price").toString()));
            goodsIndex.setShipRange(Double.valueOf(goods.get("ship_range").toString()));
        }
        // 自提点信息
        if (goodsIndex.getIsSelfTake() == 1) {
            // TODO 自提点信息
        }

        return goodsIndex;
    }

    /**
     * 获取分词结果
     *
     * @param txt
     * @return 分词list
     */
    protected List<String> toWordsList(String txt) {

        //配置文件中定义的索引名字
        String indexName = esConfig.getIndexName() + "_" + EsSettings.GOODS_INDEX_NAME;

        List<String> list = new ArrayList<String>();

        IndicesAdminClient indicesAdminClient = elasticsearchOperations.getClient().admin().indices();
        AnalyzeRequestBuilder request = new AnalyzeRequestBuilder(indicesAdminClient, AnalyzeAction.INSTANCE, indexName, txt);
        //	分词
        request.setAnalyzer("ik_max_word");
        request.setTokenizer("ik_max_word");
        List<AnalyzeToken> listAnalysis = request.execute().actionGet().getTokens();
        for (AnalyzeToken token : listAnalysis) {
            list.add(token.getTerm());
        }
        return list;
    }

    /**
     * 转换参数
     *
     * @param params
     * @return
     */
    protected List<Param> convertParam(List<Map> params) {
        List<Param> paramIndices = new ArrayList<>();
        if (params != null && params.size() > 0) {

            for (Map param : params) {
                Param index = new Param();
                index.setName(param.get("param_name") == null ? "" : param.get("param_name").toString());
                index.setValue(param.get("param_value") == null ? "" : param.get("param_value").toString());
                paramIndices.add(index);
            }

        }
        return paramIndices;
    }


    /**
     * 将分词结果写入数据库
     *
     * @param wordsList
     */
    protected void wordsToDb(List<String> wordsList) {
        wordsList = removeDuplicate(wordsList);
        for (String words : wordsList) {
            goodsWordsClient.addWords(words);
        }
    }

    @Override
    public boolean addAll(List<Map<String, Object>> list, Map<Integer, ShopDetailDO> shopMap, int index) {
        //配置文件中定义的索引名字
        String indexName = esConfig.getIndexName() + "_" + EsSettings.GOODS_INDEX_NAME;
        //删除所有的索引
        if (index == 1) {
            if (elasticsearchOperations.indexExists(indexName)) {
                //删除goods的所有索引
                DeleteQuery deleteQuery = new DeleteQuery();
                deleteQuery.setIndex(indexName);
                deleteQuery.setType(EsSettings.GOODS_TYPE_NAME);
                //删除索引
                elasticsearchOperations.delete(deleteQuery);
                //删除分词
                goodsWordsClient.delete(GoodsWordsType.SYSTEM, 0);
            }
        }

        boolean hasError = false;

        //循环生成索引
        for (Map goods : list) {

            //如果任务停止则停止生成索引
            TaskProgress tk = progressManager.getProgress(TaskProgressConstant.GOODS_INDEX);

            if (tk != null) {

                try {
                    /** 生成索引消息 */
                    progressManager.taskUpdate(TaskProgressConstant.GOODS_INDEX, "正在生成[" + StringUtil.toString(goods.get("goods_name")) + "]");
                    /** 生成优惠价格索引 */
                    goods.put("discount_price", 0L);
                    ShopDetailDO shopDetail = shopMap.get(goods.get("seller_id"));
                    this.composeShopInfo(goods, shopDetail);
                    this.addIndex(goods);
                } catch (Exception e) {
                    e.printStackTrace();
                    hasError = true;
                    logger.error(StringUtil.toString(goods.get("goods_name")) + "索引生成异常", e);
                }


            } else {
                return true;
            }
        }

        return hasError;
    }

    private void composeShopInfo(Map goods, ShopDetailDO shopDetail) {
        if(shopDetail == null){
            return;
        }
        goods.put("shop_logo", shopDetail.getShopLogo());
        goods.put("shop_lat", shopDetail.getShopLat());
        goods.put("shop_lng", shopDetail.getShopLng());
        goods.put("community_shop", shopDetail.getCommunityShop());
    }


    private void composeShipInfo(Map goods, ShipLocalTemplateVO shipTemplate ) {
        goods.put("ship_fee_setting", shipTemplate.getFeeSetting());
        goods.put("ship_time_setting", shipTemplate.getTimeSetting());
        goods.put("base_ship_price", shipTemplate.getBaseShipPrice());
        goods.put("ship_range", shipTemplate.getShipRange());
    }


    /**
     * list去重
     *
     * @param list
     * @return
     */
    protected List<String> removeDuplicate(List<String> list) {
        List<String> listTemp = new ArrayList();
        for (String words : list) {
            if (!listTemp.contains(words)) {
                listTemp.add(words);
            }
        }
        return listTemp;
    }


}
