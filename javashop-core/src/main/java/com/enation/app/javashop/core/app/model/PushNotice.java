package com.enation.app.javashop.core.app.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.UUID;

@Data
public class PushNotice implements Serializable{
    private static final long serialVersionUID = 6542301331043988701L;
    /**
     * 推送唯一标识符
     * 必填
     */
    // @NotBlank(message = "cid不能为空")
    // private String cid;

    /**
     * "all"-全部  ["android", "ios"]-多选  "android"-单个
     * 必填
     */
    private String[] platform;

    /**
     * 推送设备指定 shop__id
     * 必填 现在用alias模式
     * "all"-全部设备[有限制要求]
     */
    private String[] audience;

    /**
     * 推送的标题
     * 必填
     */
    private String title;
    /**
     * 推送的内容
     * 必填！
     */
    private String Content;
    /**
     * 推送参数
     * true表示推送到正式服务器，为false表示推送到测试服务器
     * 可选
     */
    private boolean options;
    /**
     * 推送的类型
     * 跟安卓约定的参数名
     * 值代表的含义：1申请开店审核成功;  2买家订单支付成功;  3:其它
     */
    private int type;

    /**
     * 通知内容体。
     * 可选 暂时没用
     * 是被推送到客户端的内容。与 message 一起二者必须有其一，可以二者并存
     */
    private Notification notification;

    /**
     * 消息内容体。
     * 可选 暂时没用
     * 是被推送到客户端的内容。与 notification 一起二者必须有其一，可以二者并存
     */
    private Message message;


    /**
     * 回调参数
     * 可选
     * 暂时没用
     */
    private CallBack callback;

    /**
     * 根据tag构建对象
     */
    // public static PushNotice byTag(String title, String type, String tag, String messageJson){
    //     PushNotice pushNotice = new PushNotice();
    //     pushNotice.setCid(UUID.randomUUID().toString());
    //     pushNotice.setPlatform(new String[]{"android", "ios"});
    //
    //     Audience audience = new Audience();
    //     audience.setTag(new String[]{ tag });
    //     pushNotice.setAudience(audience);
    //
    //     Message messageBody = new Message();
    //     messageBody.setTitle(title);
    //     messageBody.setContent_type(type);
    //     messageBody.setMsg_content(messageJson);
    //     pushNotice.setMessage(messageBody);
    //
    //     return pushNotice;
    // }

}
