package com.enation.app.javashop.core.trade.order.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;


/**
 * 收款单实体
 *
 * @author xlp
 * @version v2.0
 * @since v7.0.0
 * 2018-07-18 10:39:51
 */
@Table(name = "es_member_order")
@ApiModel
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderMemberDO implements Serializable {

    private static final long serialVersionUID = 7244927488352294L;

    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;

    @Column(name = "member_id")
    @ApiModelProperty(name = "member_id", value = "会员id", required = false)
    private int  memberId;

    @Column(name = "order_new")
    @ApiModelProperty(name = "order_new", value = "新订单", required = false)
    private int orderNew;

    @Column(name = "order_confirm")
    @ApiModelProperty(name = "order_confirm", value = "待付款", required = false)
    private int orderConfirm;

    @Column(name = "order_paid_off")
    @ApiModelProperty(name = "order_paid_off", value = "待发货", required = false)
    private int orderPaidOff;

    @Column(name = "order_shipped")
    @ApiModelProperty(name = "order_shipped", value = "已发货", required = false)
    private int orderShipped;

    @Column(name = "order_rog")
    @ApiModelProperty(name = "order_rog", value = "已收货", required = false)
    private int orderRog;

    @Column(name = "order_completed")
    @ApiModelProperty(name = "order_completed", value = "已完成", required = false)
    private int ordeCompleted;

    @Column(name = "order_comment")
    @ApiModelProperty(name = "order_comment", value = "待评论", required = false)
    private int orderComment;

    @Column(name = "order_cancelled")
    @ApiModelProperty(name = "order_cancelled", value = "已取消", required = false)
    private int orderCancelled;

    @Column(name = "order_after_service")
    @ApiModelProperty(name = "order_after_service", value = "售后中", required = false)
    private int orderAfterService;

    @Column(name = "cart_goods_count")
    @ApiModelProperty(name = "cart_goods_count", value = "购物车商品数量", required = false)
    private int cartGoodsCount;

    @Column(name = "create_time")
    @ApiModelProperty(name = "create_time", value = "创建时间", required = false)
    private Long createTime;

    @Column(name = "update_time")
    @ApiModelProperty(name = "update_time", value = "更新时间", required = false)
    private Long updateTime;



}
