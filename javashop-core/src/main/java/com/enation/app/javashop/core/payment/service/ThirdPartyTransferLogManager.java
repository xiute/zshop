package com.enation.app.javashop.core.payment.service;

import com.enation.app.javashop.core.payment.model.dos.ThirdPartyTransferLogDO;
import org.springframework.stereotype.Service;

/**
 * @Author: zhou
 * @Date: 2020/10/12
 * @Description:
 */
public interface ThirdPartyTransferLogManager {

    //添加第三方支付记录
    void addThirdPartyTransferLog(ThirdPartyTransferLogDO thirdPartyTransferLogDO);

    //根据交易单号更新状态
    void updateStatusByNo(String transferNo, String transferStatus);
}
