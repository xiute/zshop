package com.enation.app.javashop.core.distribution.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 王志杨
 * @since 2021/1/22 17:20
 * 团长任务明细DO
 */

@Table(name="es_distribution_mission_detail")
@Data
public class DistributionMissionDetailDO implements Serializable {

    /**
     * 团长任务明细ID
     */
    @Id(name="id")
    private Integer id;
    /**
     * 团长任务ID
     */
    @Column(name = "mission_id")
    private Integer missionId;
    /**
     * 团长任务名称
     */
    @Column(name = "mission_name")
    private String missionName;
    /**
     * 任务明细类型（0：每日任务，1：每月任务）
     */
    @Column(name = "mission_type")
    private Integer missionType;
    /**
     * 任务详情状态（1:待上线,2：已上线，3：已下线）
     */
    @Column(name = "mission_detail_status")
    private Integer missionDetailStatus;
    /**
     * 任务明细指标（0：下单人数，1：成交金额）
     */
    @Column(name = "mission_target")
    private Integer missionTarget;
    /**
     * 任务阶段1指标（人数或者金额）
     */
    @Column(name = "level1_target")
    private Integer level1Target;
    /**
     * 任务阶段1奖励现金
     */
    @Column(name = "level1_cash")
    private BigDecimal level1Cash;
    /**
     * 任务阶段1奖励现金券
     */
    @Column(name = "level1_coupon_id")
    private Integer level1CouponId;
    /**
     * 任务阶段2指标（人数或者金额）
     */
    @Column(name = "level2_target")
    private Integer level2Target;
    /**
     * 任务阶段2奖励现金
     */
    @Column(name = "level2_cash")
    private BigDecimal level2Cash;
    /**
     * 任务阶段2奖励现金券
     */
    @Column(name = "level2_coupon_id")
    private Integer level2CouponId;
    /**
     * 任务阶段3指标（人数或者金额）
     */
    @Column(name = "level3_target")
    private Integer level3Target;
    /**
     * 任务阶段3奖励现金
     */
    @Column(name = "level3_cash")
    private BigDecimal level3Cash;
    /**
     * 任务阶段3奖励现金券
     */
    @Column(name = "level3_coupon_id")
    private Integer level3CouponId;
    /**
     * 任务阶段4指标（人数或者金额）
     */
    @Column(name = "level4_target")
    private Integer level4Target;
    /**
     * 任务阶段4奖励现金
     */
    @Column(name = "level4_cash")
    private BigDecimal level4Cash;
    /**
     * 任务阶段4奖励现金券
     */
    @Column(name = "level4_coupon_id")
    private Integer level4CouponId;
    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

}