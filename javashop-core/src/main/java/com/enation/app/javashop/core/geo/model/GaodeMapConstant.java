package com.enation.app.javashop.core.geo.model;

/**
 * @author JFeng
 * @date 2019/1/11 9:44
 */
public class GaodeMapConstant {

    // 浏览器端AK
    public static final String KEY = "c4aae69b4bf8e0f312f4f92e44f88786";

    //详细地址-坐标
    public static final String GEO_URL = "https://restapi.amap.com/v3/geocode/geo";
    //坐标—详细地址
    public static final String REGEO_URL = "https://restapi.amap.com/v3/geocode/regeo";
    //地址关键字解析
    public static final String INPUT_TIPS = "https://restapi.amap.com/v3/assistant/inputtips";

}
