package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2021/1/21 10:18
 * 团长粉丝的订单数据
 */
@Data
public class DistributionFansOrderVO implements Serializable {

    private String dataType;

    private Integer num;

}
