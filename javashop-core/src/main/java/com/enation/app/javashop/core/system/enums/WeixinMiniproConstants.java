package com.enation.app.javashop.core.system.enums;

/**
 * 微信小程序常量
 */
public class WeixinMiniproConstants {

    /** 小程序accessToken */
    public static final String MINIPRO_ACCESS_TOKEN = "MINIPRO_ACCESS_TOKEN";

}
