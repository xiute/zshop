package com.enation.app.javashop.core.promotion.newcomer.model.vos;

import com.enation.app.javashop.core.promotion.newcomer.model.dos.NewcomerDO;
import com.enation.app.javashop.core.promotion.newcomer.model.dos.NewcomerGoodsDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class MiniNewcomerDataVO extends NewcomerGoodsDO implements Serializable{

    private static final long serialVersionUID = 5573194350655656413L;
    @ApiModelProperty(value = "剩余数量", name = "surplus_num")
    private Integer surplusNum;

    @ApiModelProperty(value = "活动结束开始时间", name = "start_time")
    private Long startTime;

    @ApiModelProperty(value = "活动结束时间", name = "end_time")
    private Long endTime;

    @ApiModelProperty(value = "距离活动结束时间", name = "count_down")
    private Long countDown;

    @ApiModelProperty(value = "商品描述", name = "selling")
    private String selling;

    @ApiModelProperty(value = "商品图片", name = "thumbnail")
    private String thumbnail ;

    @ApiModelProperty(value = "送货时间", name = "pick_time_show")
    private Long pickTimeShow;

    @ApiModelProperty(value = "是否可销售")
    private Boolean salesEnable;

    @ApiModelProperty(value = "购买人头像", name = "member_imgs")
    private List<String> memberImgs;

    @ApiModelProperty(value = "购物车存在商品数量", name = "cart_num")
    private Integer cartNum;

    @ApiModelProperty(value = "每人限购商品数量", name = "newcomer_limit_num")
    private Integer  newcomerLimitNum;



}
