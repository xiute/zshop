
package com.enation.app.javashop.core.member.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 会员邀请人
 */
@Data
@Table(name = "es_member_inviter")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberInviter implements Serializable {

    private static final long serialVersionUID = 3227466962489949L;

    /**
     * ID
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;

    /**
     * 会员ID
     */
    @Column(name = "member_id")
    @ApiModelProperty(name = "member_id", value = "会员id", required = true)
    private Integer memberId;

    /**
     * 邀请人会员ID
     */
    @Column(name = "inviter_member_id")
    @ApiModelProperty(name = "inviter_member_id", value = "邀请人会员ID", required = true)
    private Integer inviterMemberId;

    /**
     * 邀请时间
     */
    @Column(name = "invite_time")
    @ApiModelProperty(name = "invite_time", value = "邀请时间", required = true)
    private Date inviteTime;

}