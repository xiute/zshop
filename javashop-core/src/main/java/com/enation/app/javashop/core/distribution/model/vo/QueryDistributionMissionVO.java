package com.enation.app.javashop.core.distribution.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2021/1/22 18:55
 * 团长任务查询VO
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class QueryDistributionMissionVO implements Serializable {

    @ApiModelProperty(value = "页码", name = "page_no", required = true)
    @NotNull(message = "请输入当前页码")
    private Integer pageNo;

    @ApiModelProperty(value = "分页大小", name = "page_size", required = true)
    @NotNull(message = "请输入分页大小")
    private Integer pageSize;

    @ApiModelProperty(value = "店铺ID", name = "seller_id", hidden = true)
    private Integer sellerId;

    /**
     * 团长任务名称
     */
    @ApiModelProperty(name = "mission_name", value = "团长任务名称")
    @NotBlank(message = "请输入团长任务名称")
    private String missionName;
    /**
     * 团长任务状态
     */
    @ApiModelProperty(name = "mission_status", value = "团长任务状态")
    private Integer missionStatus;
    /**
     * 开始时间
     */
    @ApiModelProperty(name = "start_time", value = "开始时间")
    @NotNull(message = "请输入开始时间")
    private Long startTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(name = "end_time", value = "结束时间")
    @NotNull(message = "请输入结束时间")
    private Long endTime;
    /**
     * 奖励类型
     */
    @ApiModelProperty(name = "rewards_type", value = "奖励类型")
    @NotNull(message = "请选择奖励类型")
    private Integer rewardsType;

}
