package com.enation.app.javashop.core.promotion.shetuan.service;


import com.enation.app.javashop.core.goods.model.dos.GoodsDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.QueryShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.tool.model.dto.PromotionDTO;
import com.enation.app.javashop.core.shop.model.vo.ShopCatItem;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;

import java.util.List;
import java.util.Map;

public interface ShetuanGoodsManager {

    ShetuanGoodsDO getById(Integer id);

    Page<ShetuanGoodsDO>  getPageForMap(Map map,int pageNo,int pageSize);

    ShetuanGoodsVO getBySkuId(Integer shetuanId,Integer skuId);

    List<ShetuanGoodsDO> queryShetuanGoodsBySkuIds(Integer shetuanId,List<Integer> skuIds);

    ShetuanGoodsDO checkIsShetuan(Integer skuId, Integer activityId);

    List<ShetuanGoodsDO>  getByShetuanAndStatus(Integer shetuanId,Integer[] statusArray);

    Page<ShetuanGoodsVO> queryByPage(QueryShetuanGoodsVO queryShetuanGoodsVO);

    List<ShetuanGoodsDO>  getOverlapGoods(List<ShetuanGoodsDO> goodsList, List<ShetuanDO> shetuanDOS);

    List<ShetuanGoodsDO> querySheuanGoodsByGoodsIds(Integer[] goodsIds);

    List<ShetuanGoodsDO> getBySkuIds(List<Integer> skuIds, int status);

    ShetuanGoodsVO getShetuanGoodsDetail(Integer shetuanGoodsId);

    List<ShetuanGoodsDO> queryShetuanGoodsByIds(List<Integer> ids);

    List<ShetuanGoodsDO> getByShetuanId(Integer shetuanId);
}
