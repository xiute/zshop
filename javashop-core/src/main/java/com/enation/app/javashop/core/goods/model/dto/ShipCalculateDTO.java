package com.enation.app.javashop.core.goods.model.dto;

import com.enation.app.javashop.core.shop.model.dos.TemplateFeeSetting;
import com.enation.app.javashop.core.shop.model.dos.TemplateTimeSetting;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author JFeng
 * @date 2020/2/17 14:29
 */

@Data
public class ShipCalculateDTO {

    // 是否支持同城配送
    private Integer allowLocalShip;
    // 是否支持快递
    private Integer allowGlobalShip;
    // 同城配送 派送时间段
    private String localStartTime;
    // 同城配送 送达时间段
    private String localEndTime;
    // 快递到达日期
    private String globalEndTime;
    // 同城配送价格
    private BigDecimal localShipPrice;
    // 快递价格
    private BigDecimal globalShipPrice;

    private Integer isLocal;
    private Integer isGlobal;
    private Integer isSelfTake;

    private String errorMsg;

    private BigDecimal distance;
    private BigDecimal shipRange;
    private String shipTimeView;
    private TemplateTimeSetting templateTimeSetting;
    private TemplateFeeSetting templateFeeSetting;

    private Boolean shopIsOPen;
}
