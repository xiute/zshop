package com.enation.app.javashop.core.trade.cart.model.vo;

import com.enation.app.javashop.core.member.model.dos.LeaderDO;
import com.enation.app.javashop.core.promotion.fulldiscount.model.dos.FullDiscountGiftDO;
import com.enation.app.javashop.core.shop.model.dos.ShipLocalTemplateVO;
import com.enation.app.javashop.core.shop.model.dto.ShipTemplateChildDTO;
import com.enation.app.javashop.core.trade.cart.model.dos.CartDO;
import com.enation.app.javashop.core.trade.cart.model.enums.CartType;
import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 购物车展示Vo
 * @author Snow
 * @since v6.4
 * @version v1.0
 * 2017年08月23日14:22:48
 */

@ApiModel(description = "购物车展示Vo")
@Data
public class CartVO extends CartDO implements Serializable{


	private static final long serialVersionUID = 6382186311779188645L;
	/**
	 * 把Cart.SkuList 数据 根据促销活动压入到此集合中。
	 */
	@ApiModelProperty(value = "促销活动集合（包含商品")
	private List<GroupPromotionVO>  promotionList;

	@ApiModelProperty(value = "已参与的的促销活动提示，直接展示给客户")
	private String promotionNotice;


	/**
	 * 购物车与运费dto的map映射
	 * key为skuid value 为模版
	 */
	private Map<Integer, ShipTemplateChildDTO> shipTemplateChildMap;

	@ApiModelProperty(value = "购物车页展示时，店铺内的商品是否全选状态.1为店铺商品全选状态,0位非全选")
	private Integer checked;

	private List<PromotionRule> ruleList;

	/**
	 * 购物车类型：购物车页面和或结算页
	 */
	private CartType cartType;

	private Integer allowLocal;

	private Integer allowGlobal;

	private Integer allowSelf;

	private BigDecimal distance;

	private Double shopLat;

	private Double shopLng;

	private ShipLocalTemplateVO shipLocalTemplateVO;

	private String errorMsg;

	private String activeShipWay;

	private Boolean shopIsOpen;

	private Integer communityShop;

	private Integer selfOperated;

	private Double totalGoodsPrice;

	private Double totalWeight;

	private Double totalNum;

	private int goodsNum;

	private LeaderDO leader;

	private  String cartSourceType;

	private Boolean allowCreateOrder;

	private String city;

	private String dispatchMsg;

	public CartVO(){

	}

	/**
	 * 父类的构造器
	 * @param sellerId
	 * @param sellerName
	 */
	public CartVO(int sellerId, String sellerName,String sellerLogo,CartType cartType) {
		this.cartType = cartType;
		super.setWeight(0D);
		super.setSellerId(sellerId);
		super.setSellerName(sellerName);
		super.setSellerLogo(sellerLogo);
		super.setPrice(new PriceDetailVO());
		super.setSkuList(new ArrayList<CartSkuVO>());
		super.setCouponList(new ArrayList<CouponVO>());
		super.setGiftList(new ArrayList<FullDiscountGiftDO>());
		super.setGiftCouponList(new ArrayList<CouponVO>());
		this.setChecked(1);
		this.setInvalid(0);
		ruleList = new ArrayList<>();
	}

	/**
	 * 父类的构造器
	 * @param sellerId
	 * @param sellerName
	 */
	public CartVO(int sellerId, String sellerName,CartType cartType) {
		this.cartType = cartType;
		super.setWeight(0D);
		super.setSellerId(sellerId);
		super.setSellerName(sellerName);
		super.setPrice(new PriceDetailVO());
		super.setSkuList(new ArrayList<CartSkuVO>());
		super.setCouponList(new ArrayList<CouponVO>());
		super.setGiftList(new ArrayList<FullDiscountGiftDO>());
		super.setGiftCouponList(new ArrayList<CouponVO>());
		this.setChecked(1);
		this.setInvalid(0);
		ruleList = new ArrayList<>();
	}

}
