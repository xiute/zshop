package com.enation.app.javashop.core.goods.service;


import com.enation.app.javashop.core.goods.model.vo.PlacardGenVO;

import java.util.Map;

/**
 * john
 * 商品分享
 */
public interface GoodsShareManager {

    /**
     * 生成海报
     */

     Map<String ,Object>   gen(PlacardGenVO vo);

    /**
     * 保存海报scene场景具体参数
     * @param kvs
     * @return
     */
     String saveSceneDetail(Map kvs);

    /**
     *获取海报scene参数
     */
    Map getSceneDetail(String scene);

}
