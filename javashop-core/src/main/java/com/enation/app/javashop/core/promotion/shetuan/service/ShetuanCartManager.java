package com.enation.app.javashop.core.promotion.shetuan.service;


import com.enation.app.javashop.core.trade.cart.model.enums.CheckedWay;
import com.enation.app.javashop.core.trade.cart.model.vo.CartView;
import com.enation.app.javashop.core.trade.order.model.vo.TradeVO;

/**
 *
 * 购物车只读操作业务接口<br>
 * 包含对购物车读取操作
 * @author Snow
 * @since v7.0.0
 * @version v2.0
 * 2018年03月19日21:55:53
 */
public interface ShetuanCartManager {


    CartView getCheckedItems(CheckedWay way,int sellerId);

    TradeVO createTrade(String client, CheckedWay valueOf, int sellerId, Integer buyerId, double walletPayPrice,Integer receiveTimeSeq);

    CartView getCartListAndCountPrice(CheckedWay way);

}
