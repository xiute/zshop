package com.enation.app.javashop.core.distribution.service;

import com.enation.app.javashop.core.distribution.model.dos.DistributionDO;
import com.enation.app.javashop.core.distribution.model.dos.DistributionOrderDO;
import com.enation.app.javashop.core.distribution.model.dos.DistributionRelationshipDO;
import com.enation.app.javashop.core.distribution.model.dos.ShareDO;
import com.enation.app.javashop.core.distribution.model.dto.QueryShareInfoDTO;
import com.enation.app.javashop.core.distribution.model.dto.ShareDTO;
import com.enation.app.javashop.core.distribution.model.vo.DistributionRelationshipVO;
import com.enation.app.javashop.core.distribution.model.vo.DistributionVO;
import com.enation.app.javashop.core.distribution.model.vo.FansDataVO;
import com.enation.app.javashop.core.distribution.service.pattern.DistributionStrategy;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.vo.MemberInviterVO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.DistributionFansOrderVO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;
import java.util.Map;

/**
 * 分销商Manager接口
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/21 下午3:19
 */
public interface DistributionManager {

    /**
     * 新增分销商
     */
    DistributionDO add(DistributionDO distributor);

    /**
     * 所有下线
     */
    List<DistributionVO> allDown(Integer memberId);


    /**
     * 分页分销商
     * @param pageNo 页码
     * @param pageSize 分页大小
     * @return PAGE
     */
    Page page(Integer pageNo, Integer pageSize, DistributionVO distributionVO);

    /**
     * 根据会员id获得分销商的信息
     *
     * @param memberId 会员id
     * @return 分销商对象 Distributor,没有就返回null
     */
    DistributionDO getDistributorByMemberId(Integer memberId);

    /**
     * 根据会员id获得分销商的信息
     *
     * @param id 分销商id
     * @return 分销商对象 Distributor,没有就返回null
     */
    DistributionDO getDistributor(Integer id);


    /**
     * 更新Distributor信息
     */
    DistributionDO edit(DistributionDO distributor);


    /**
     * 根据会员id设置其上级分销商（两级）
     *
     * @param memberId 会员id
     * @param parentId 上级会员的id
     * @return 设置结果， trun=成功 false=失败
     */
    boolean setParentDistributorId(Integer memberId, Integer parentId);

    /**
     * 获取可提现金额
     */
    Double getCanRebate(Integer memberId);

    /**
     * 增加冻结返利金额
     *
     * @param price     返利金额金额
     * @param memberId 会员id
     */
    void addFrozenCommission(Double price, Integer memberId);


    /**
     * 增加总销售额、总的返利金额金额
     *
     * @param orderPrice 订单金额
     * @param rebate     返利金额
     * @param memberId  会员id
     */
    void addTotalPrice(Double orderPrice, Double rebate, Integer memberId);

    /**
     * 减去总销售额、总的返利金额金额
     *
     * @param orderPrice 订单金额
     * @param rebate     返利金额
     * @param memberId  会员id
     */
    void subTotalPrice(Double orderPrice, Double rebate, Integer memberId);

    /**
     * 获取当前会员 的上级
     *
     * @return 返回的字符串
     */
    String getUpMember();

    /**
     * 获取下级 分销商集合
     */
    List<DistributionVO> getLowerDistributorTree(Integer memberId);

    /**
     * 修改模版
     */
    void changeTpl(Integer memberId, Integer tplId);

    /**
     * 统计下线人数
     */
    void countDown(Integer memberId);


    ShareDTO getShareInfo(QueryShareInfoDTO queryShareInfoDTO);


    void createShare(String uuid, Integer memberId, String shareKey, String shortUrl);

    ShareDO getByShareKey(String shareKey);

    ShareDO getByMemberIdAndUrl(Integer memberId, String shareUrl);


    DistributionStrategy getDistributionStrategy(OrderDO orderDO, DistributionDO buyerDistributor);

    /**
     * 计算每个人的收益情况
     */
    void calAllProfit(OrderDO orderDO, DistributionOrderDO distributionOrderDO, DistributionDO buyerDistributor);

    /**
     * 更新退款后剩余收益
     */
    void updateRefundSurplusProfit(OrderDO orderDO, double oldOrderPrice, double newOrderPrice);


    String randomInviteCode();

    DistributionDO getDistributorByInviteCode(String inviteCode);

    /**
     *查询我的邀请人
     */
    Page getInviterList(Integer memberId, long startTime, long endTime, Integer pageNo, Integer pageSize);

    /**
     * 邀请人排行榜
     */
    Page getRecommendedCouponsRanking(long startTime, long endTime, Integer pageNo, Integer pageSize);

    /**
     *查询我的邀请人
     */
    Page loadInviterList(Integer memberId, Integer pageNo, Integer pageSize);

    /**
     * 查询我的粉丝
     */
    Page getMyFansList(Integer memberId, Integer pageNo, Integer pageSize);

    /**
     * 查询下级团长
     */
    Page getNextLeaderList(Integer memberId, Integer pageNo, Integer pageSize);

    /**
     * 我的下级首页
     */
    Map<String,Object> fansIndex(Integer memberId);

    /**
     * 查询所有粉丝
     */
    List<DistributionDO> getAllFans();

    /**
     * 清除lv1关系
     */
    void removeLv1Relationship(Integer memberId);

    /**
     *  我的上级首页
     */
    Map<String,Object> superiorIndex(Integer memberId);

    /**
     *  我的上级团长信息
     */
    Member getSuperiorLeader(Integer memberId);

    /**
     * 查询团长信息
     */
    Page getSuperiorDistribution(String keyWords);

    /**
     * 修改上级团长
     */
    void updateLv1MemberId(String memberId,Integer memberIdLv1);

    /**
     * 修改普通用户的上级团长
     */
    void updateNormalMemberLv1MemberId(String memberIds,Integer memberIdLv1,Integer source);

    // 查询团长集合
    List<DistributionVO> getdistributionVO(String memberIdss);

    /**
     * 邀请我的人(我的邀请人)
     */
    Member getMemberInviter(Integer memberId);

    /**
     *启用 禁用 团长
     */
    void updateDistributionStatus(String distributionIds, Integer status);

    /**
     *  切换团长关系
     */
    void handleShareRelationship(Integer currentMemberId, Integer sharerMemberId);


    /**
     *  查询团长别名最大值,除我们自己团长之外
     */
    Integer getTeamName(Integer provinceId);

    /**
     *  查询团号是否在该地区已存在
     */
    Boolean queryTeamNameNum(DistributionDO distributionDO);

    /**
     * 修改上级团长信息
     */
    void updateSuperiorDistribution(Integer memberId,String mobileOrInvitationCode);

    /**
     * 查询自己是不是团长, 是团长返回会员信息
     */
    Member getDistributionMemberByMemberId(Integer memberId);

    void changeDistributionRelationship(Integer oldMemberId, Integer newMemberId);

    /**
     * 保存会员团长绑定记录
     */
    void addDistributionRelationshipVO(List<DistributionRelationshipVO> distributionRelationshipVO);

    /**
     * 查询会员团长绑定记录
     */
    List<DistributionRelationshipVO> getDistributionRelationshipVOByMemberId(Integer memberId);

    /**
     * 生成 DistributionRelationshipVO 工厂
     */
    DistributionRelationshipVO buildDistributionRelationshipVO(Integer currentMemberId,Integer memberLv1,String operationType,Integer source,
                                                               String operationDescribe);

    /**
     * 查询团长粉丝的订单数据集
     * @param memberId 会员ID
     * @return 团长粉丝的订单数据集合
     */
    List<DistributionFansOrderVO> getDistributionFansOrderList(Integer memberId);

    Page queryFansDataByType(String fans_data_key, Integer memberId, Integer pageNo, Integer pageSize);
}
