package com.enation.app.javashop.core.member.service;

import com.enation.app.javashop.core.member.model.dos.WithdrawRecordDO;
import com.enation.app.javashop.core.member.model.dto.WithdrawRecordQueryParam;
import com.enation.app.javashop.framework.database.Page;

/**
 * 提现记录
 * @author 孙建
 */
public interface WithdrawRecordManager {

    WithdrawRecordDO getModel(Integer id);

    Page list(WithdrawRecordQueryParam withdrawRecordQueryParam);

    void applyWithdraw(WithdrawRecordDO withdrawRecordDO);

    void auditing(Integer withdrawRecordId, String remark, String auditResult);

    void transfer(Integer withdrawRecordId, String remark);

}