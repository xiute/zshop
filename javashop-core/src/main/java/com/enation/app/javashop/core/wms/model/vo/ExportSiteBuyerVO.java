package com.enation.app.javashop.core.wms.model.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author JFeng
 * @date 2020/7/17 14:31
 */

@Data
public class ExportSiteBuyerVO {

    @ExcelProperty("站点名称")
    private String siteName;

    @ExcelProperty("收货人")
    private String shipName;

    @ExcelProperty("收货电话")
    private String shipMobile;

    @ExcelProperty("订单号")
    private String orderSn;

    @ExcelProperty("单号(配送系统用)")
    private String dispatchNo;

    @ExcelProperty("出库单号")
    private String deliverySn;

    @ExcelProperty("单品数量")
    private Integer skuTotal;

    @ExcelProperty("订单类型")
    private String orderType;

    private String siteAddr;

}
