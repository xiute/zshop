package com.enation.app.javashop.core.wms.model.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author JFeng
 * @date 2020/10/22 17:35
 */
@Data
public class ExportSiteVO {

    // 站点ID，导出时忽略
    @ExcelIgnore
    private Integer leaderId;

    // 站点所在区
    @ExcelIgnore
    private String county;

    // 站点所在镇
    @ExcelIgnore
    private String town;

    @ExcelProperty(value = "站点名称")
    private String siteName;

    @ExcelProperty(value = "联系人")
    private String leaderName;

    @ExcelProperty(value = "联系电话")
    private String leaderMobile;

    @ExcelProperty(value = "站点地址")
    private String address;}
