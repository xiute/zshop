package com.enation.app.javashop.core.payment.model.enums;

/**
 * @Author: zhou
 * @Date: 2020/10/12
 * @Description:
 */
public enum WechatTransferStatusEnum {
    SUCCESS("成功"),
    FAIL("失败"),
    PROCESSING("处理中");

    private String describe;

    WechatTransferStatusEnum(String describe) {
        this.describe = describe;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String value() {
        return this.name();
    }
}
