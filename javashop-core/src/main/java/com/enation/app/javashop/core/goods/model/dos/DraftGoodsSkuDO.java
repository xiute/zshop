package com.enation.app.javashop.core.goods.model.dos;

import java.io.Serializable;

import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 草稿商品sku实体
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-26 11:38:06
 */
@Table(name="es_draft_goods_sku")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DraftGoodsSkuDO implements Serializable {
			
    private static final long serialVersionUID = 5684194304207265L;
    
    /**主键ID*/
    @Id(name = "draft_sku_id")
    @ApiModelProperty(hidden=true)
    private Integer draftSkuId;
    /**草稿id*/
    @Column(name = "draft_goods_id")
    @ApiModelProperty(name="draft_goods_id",value="草稿id",required=false)
    private Integer draftGoodsId;
    /**货号*/
    @Column(name = "sn")
    @ApiModelProperty(name="sn",value="货号",required=false)
    private String sn;
    /**总库存*/
    @Column(name = "quantity")
    @ApiModelProperty(name="quantity",value="总库存",required=false)
    private Integer quantity;
    /**价格*/
    @Column(name = "price")
    @ApiModelProperty(name="price",value="价格",required=false)
    private Double price;
    /**规格*/
    @Column(name = "specs")
    @ApiModelProperty(name="specs",value="规格",required=false)
    private String specs;
    /**成本*/
    @Column(name = "cost")
    @ApiModelProperty(name="cost",value="成本",required=false)
    private Double cost;
    /**重量*/
    @Column(name = "weight")
    @ApiModelProperty(name="weight",value="重量",required=false)
    private Double weight;



    @Column(name = "is_local")
    @ApiModelProperty(value = "是否本地商品")
    private Integer isLocal;

    @Column(name = "is_global")
    @ApiModelProperty(value = "是否商城商品")
    private Integer isGlobal;


    @Column(name = "local_template_id")
    @ApiModelProperty(value = "同城配送模板id")
    private Integer localTemplateId;

    @Column(name = "is_self_take")
    @ApiModelProperty(name = "is_self_take", value = "是否自提", required = false)
    private Integer isSelfTake;

    @Column(name = "freight_pricing_way")
    @ApiModelProperty(name = "freight_pricing_way", value = "运费定价方式 1统一价 2模板", required = false)
    private Integer freightPricingWay;

    /** 运费一口价 */
    @Column(name = "freight_unified_price")
    @ApiModelProperty(name = "freight_unified_price", value = "运费统一价", required = false)
    private Double freightUnifiedPrice;

    /** 商品卖点 */
    @Column(name = "selling")
    @ApiModelProperty(name = "selling", value = "商品卖点", required = false)
    private String selling;

    @Column(name = "video_url")
    @ApiModelProperty(name = "video_url", value = "主图视频", required = false)
    private String videoUrl;

    @Column(name = "supplier_name")
    @ApiModelProperty(name = "supplier_name", value = "供应商姓名")
    private String  supplierName;

    @Column(name = "up_goods_id")
    @ApiModelProperty(name = "up_goods_id", value = "上游商品id")
    private String upGoodsId;

    public DraftGoodsSkuDO() {}
    
    public DraftGoodsSkuDO(GoodsSkuVO skuVO) {
    	this.setDraftGoodsId(skuVO.getGoodsId());
		this.setSn(skuVO.getSn());
		this.setPrice(skuVO.getPrice());
		this.setCost(skuVO.getCost());
		this.setWeight(skuVO.getWeight());
		this.setQuantity(skuVO.getQuantity());
		this.setSpecs(skuVO.getSpecs());
    }
    
    @PrimaryKeyField
    public Integer getDraftSkuId() {
        return draftSkuId;
    }
    public void setDraftSkuId(Integer draftSkuId) {
        this.draftSkuId = draftSkuId;
    }

    public Integer getDraftGoodsId() {
        return draftGoodsId;
    }
    public void setDraftGoodsId(Integer draftGoodsId) {
        this.draftGoodsId = draftGoodsId;
    }

    public String getSn() {
        return sn;
    }
    public void setSn(String sn) {
        this.sn = sn;
    }

    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSpecs() {
        return specs;
    }
    public void setSpecs(String specs) {
        this.specs = specs;
    }

    public Double getCost() {
        return cost;
    }
    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getWeight() {
        return weight;
    }
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(Integer isLocal) {
        this.isLocal = isLocal;
    }

    public Integer getIsGlobal() {
        return isGlobal;
    }

    public void setIsGlobal(Integer isGlobal) {
        this.isGlobal = isGlobal;
    }

    public Integer getLocalTemplateId() {
        return localTemplateId;
    }

    public void setLocalTemplateId(Integer localTemplateId) {
        this.localTemplateId = localTemplateId;
    }

    public Integer getIsSelfTake() {
        return isSelfTake;
    }

    public void setIsSelfTake(Integer isSelfTake) {
        this.isSelfTake = isSelfTake;
    }

    public Integer getFreightPricingWay() {
        return freightPricingWay;
    }

    public void setFreightPricingWay(Integer freightPricingWay) {
        this.freightPricingWay = freightPricingWay;
    }

    public Double getFreightUnifiedPrice() {
        return freightUnifiedPrice;
    }

    public void setFreightUnifiedPrice(Double freightUnifiedPrice) {
        this.freightUnifiedPrice = freightUnifiedPrice;
    }

    public String getSelling() {
        return selling;
    }

    public void setSelling(String selling) {
        this.selling = selling;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getUpGoodsId() {
        return upGoodsId;
    }

    public void setUpGoodsId(String upGoodsId) {
        this.upGoodsId = upGoodsId;
    }

    @Override
	public String toString() {
		return "DraftGoodsSkuDO [draftSkuId=" + draftSkuId + ", draftGoodsId=" + draftGoodsId + ", sn=" + sn
				+ ", quantity=" + quantity + ", price=" + price + ", specs=" + specs + ", cost=" + cost + ", weight="
				+ weight + "]";
	}


	
}