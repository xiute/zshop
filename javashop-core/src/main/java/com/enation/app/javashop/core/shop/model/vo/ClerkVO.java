package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.framework.validation.annotation.Mobile;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 店员实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-08-04 18:48:39
 */

@Data
public class ClerkVO implements Serializable {
    /**
     * 店员名称
     */
    @ApiModelProperty(value = "账号名称", required = true)
    @NotNull(message = "账号名称不能为空")
    private String uname;
    /**
     * 账号密码
     */
    @ApiModelProperty(value = "账号密码")
    private String password;
    /**
     * 手机号码
     */
    @Mobile(message = "手机格式不正确")
    @ApiModelProperty(value = "手机号码", required = true)
    private String mobile;
    /**
     * 权限id
     */
    @NotNull(message = "权限不能为空")
    @ApiModelProperty(name = "role_id", value = "权限id,如果是店主则传0", required = true)
    private Integer roleId;

    /**
     * 邮箱
     */
    @Email(message = "邮箱格式不正确")
    @ApiModelProperty(value = "邮箱", required = true)
    private String email;

    private String face;
}