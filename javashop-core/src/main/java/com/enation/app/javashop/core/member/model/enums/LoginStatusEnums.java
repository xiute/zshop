package com.enation.app.javashop.core.member.model.enums;

import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * 登录人状态
 * @author xlg
 * 2020/11/03 18:00
 */
public enum LoginStatusEnums {
    //未注册的新用户
    NEW_USERS(0,"未注册的新用户"),
    //已注册未下单的并且没有新人卷
    FIRST_ORDER_USERS(1,"首单用户"),
    //老用户
    OLD_USERS(2,"老用户");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (AccountTypeEnum item : AccountTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    LoginStatusEnums(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }
}
