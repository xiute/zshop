package com.enation.app.javashop.core.geo.model;

import lombok.Data;

/**
 * @author JFeng
 * @date 2019/10/22 10:43
 */
@Data
public class GeoCode {
    private String formatted_address;
    private String country;
    private String province;
    private String city;
    private String district;
    private String street;
    private String adcode;
    private String location;
}
