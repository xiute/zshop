package com.enation.app.javashop.core.member.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AuditLeaderDTO {

    @ApiModelProperty(name = "id", value = "id")
    private Integer id;

    @ApiModelProperty(name = "audit_status", value = "审核状态 2通过 3不通过", required = true)
    private Integer auditStatus;

    @ApiModelProperty(name = "audit_remark", value = "审核备注", required = true)
    private String auditRemark;

    @ApiModelProperty(name ="operateAreas",value = "运营区域")
    private String operateAreas;

    @ApiModelProperty(name = "leader_type",value = "团长类型 1自提点 2配送点")
    private Integer leaderType;

    @ApiModelProperty(name = "dispatch_type",value = "分拣配送方式 SELF_GLOBAL 仓库分拣 SELF_BATCH  站点分拣")
    private String dispatchType;

    @ApiModelProperty(name ="ship_priority",value = "配送优先级")
    private Integer shipPriority;

}
