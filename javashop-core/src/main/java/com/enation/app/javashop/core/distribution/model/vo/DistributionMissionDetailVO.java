package com.enation.app.javashop.core.distribution.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 王志杨
 * @since 2021/1/22 17:20
 * 团长任务明细DO
 */

@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class DistributionMissionDetailVO implements Serializable {

    /**
     * 团长任务明细ID
     */
    @ApiModelProperty(name = "id", value = "团长任务明细ID")
    private Integer id;
    /**
     * 团长任务ID
     */
    @ApiModelProperty(name = "mission_id", value = "团长任务ID")
    private Integer missionId;
    /**
     * 团长任务名称
     */
    @ApiModelProperty(name = "mission_name", value = "团长任务名称")
    private String missionName;
    /**
     * 任务明细类型（0：每日任务，1：每月任务）
     */
    @ApiModelProperty(name = "mission_type", value = "任务明细类型（0：每日任务，1：每月任务）")
    private Integer missionType;
    /**
     * 任务明细类型（0：每日任务，1：每月任务）文本
     */
    @ApiModelProperty(name = "mission_type_text", value = "任务明细类型（0：每日任务，1：每月任务）文本")
    private Integer missionTypeText;
    /**
     * 任务详情状态（1:待上线,2：已上线，3：已下线）
     */
    @ApiModelProperty(name = "mission_detail_status", value = "任务详情状态（1:待上线,2：已上线，3：已下线）")
    private Integer missionDetailStatus;
    /**
     * 任务详情状态（1:待上线,2：已上线，3：已下线）文本
     */
    @ApiModelProperty(name = "mission_detail_status_text", value = "任务详情状态（1:待上线,2：已上线，3：已下线）文本")
    private Integer missionDetailStatusText;
    /**
     * 任务明细指标（0：下单人数，1：成交金额）
     */
    @ApiModelProperty(name = "mission_target", value = "任务明细指标（0：下单人数，1：成交金额）")
    private Integer missionTarget;
    /**
     * 任务明细指标（0：下单人数，1：成交金额）文本
     */
    @ApiModelProperty(name = "mission_target_text", value = "任务明细指标（0：下单人数，1：成交金额）文本")
    private Integer missionTargetText;
    /**
     * 任务阶段1指标（人数或者金额）
     */
    @ApiModelProperty(name = "level1_target", value = "任务阶段1指标（人数或者金额）")
    private Integer level1Target;
    /**
     * 任务阶段1奖励现金
     */
    @ApiModelProperty(name = "level1_cash", value = "任务阶段1奖励现金")
    private BigDecimal level1Cash;
    /**
     * 任务阶段1奖励现金券
     */
    @ApiModelProperty(name = "level1_coupon_id", value = "任务阶段1奖励现金券")
    private Integer level1CouponId;
    /**
     * 任务阶段2指标（人数或者金额）
     */
    @ApiModelProperty(name = "level2_target", value = "任务阶段2指标（人数或者金额）")
    private Integer level2Target;
    /**
     * 任务阶段2奖励现金
     */
    @ApiModelProperty(name = "level2_cash", value = "任务阶段2奖励现金")
    private BigDecimal level2Cash;
    /**
     * 任务阶段2奖励现金券
     */
    @ApiModelProperty(name = "level2_coupon_id", value = "任务阶段2奖励现金券")
    private Integer level2CouponId;
    /**
     * 任务阶段3指标（人数或者金额）
     */
    @ApiModelProperty(name = "level3_target", value = "任务阶段3指标（人数或者金额）")
    private Integer level3Target;
    /**
     * 任务阶段3奖励现金
     */
    @ApiModelProperty(name = "level3_cash", value = "任务阶段3奖励现金")
    private BigDecimal level3Cash;
    /**
     * 任务阶段3奖励现金券
     */
    @ApiModelProperty(name = "level3_coupon_id", value = "任务阶段3奖励现金券")
    private Integer level3CouponId;
    /**
     * 任务阶段4指标（人数或者金额）
     */
    @ApiModelProperty(name = "level4_target", value = "任务阶段4指标（人数或者金额）")
    private Integer level4Target;
    /**
     * 任务阶段4奖励现金
     */
    @ApiModelProperty(name = "level4_cash", value = "任务阶段4奖励现金")
    private BigDecimal level4Cash;
    /**
     * 任务阶段4奖励现金券
     */
    @ApiModelProperty(name = "level4_coupon_id", value = "任务阶段4奖励现金券")
    private Integer level4CouponId;

}