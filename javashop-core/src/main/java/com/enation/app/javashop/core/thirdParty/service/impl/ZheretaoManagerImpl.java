package com.enation.app.javashop.core.thirdParty.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.enation.app.javashop.core.base.SettingGroup;
import com.enation.app.javashop.core.base.service.SettingManager;
import com.enation.app.javashop.core.distribution.model.dos.DistributionGoods;
import com.enation.app.javashop.core.distribution.service.DistributionGoodsManager;
import com.enation.app.javashop.core.goods.model.dos.*;
import com.enation.app.javashop.core.goods.model.enums.GoodsType;
import com.enation.app.javashop.core.goods.model.enums.QuantityType;
import com.enation.app.javashop.core.goods.model.vo.GoodsQuantityVO;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.goods.model.vo.SpecValueVO;
import com.enation.app.javashop.core.goods.service.*;
import com.enation.app.javashop.core.goodssearch.util.PinYinUtil;
import com.enation.app.javashop.core.thirdParty.model.dto.*;
import com.enation.app.javashop.core.thirdParty.model.enums.ResultCodeEnum;
import com.enation.app.javashop.core.thirdParty.model.enums.UpdateTypeEnum;
import com.enation.app.javashop.core.thirdParty.model.vo.ZheretaoRequestVo;
import com.enation.app.javashop.core.thirdParty.service.ZheretaoManager;
import com.enation.app.javashop.framework.JavashopConfig;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author: zhou
 * @Date: 2020/10/12
 * @Description:
 */
@Service
public class ZheretaoManagerImpl implements ZheretaoManager {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    @Qualifier("goodsDaoSupport")
    private DaoSupport daoSupport;
    @Autowired
    private GoodsManager goodsManager;
    @Autowired
    private GoodsSkuManager goodsSkuManager;
    @Autowired
    private SpecificationManager specificationManager;
    @Autowired
    private SpecValuesManager specValuesManager;
    @Autowired
    private SettingManager settingManager;
    @Autowired
    private DistributionGoodsManager distributionGoodsManager;
    @Autowired
    private GoodsQuantityManager goodsQuantityManager;
    @Autowired
    private JavashopConfig javashopConfig;

    //正式
    // private static final String BASE_URL = "http://napi.zheretao.com/v1/remote";
    //测试
    private static final String BASE_URL = "https://testnapi.zheretao.com/v1/remote";
    // private static final String BASE_URL = "https://test2napi.zheretao.com/v1/remote";
    private static final String API_KEY = "ax7PQmWnFPql8HQTmkl5INoXzA7KZ1x3";
    //拉取这个时间之后创建的商品
    private static final String TIME = "2020-09-20 00:00:00";
    //分页拉取数据时，一页多少个商品
    private static final Integer PAGE_SIZE = 10;


    //获取到的商品放在平台自营商店中，商店ID测试为1，正式为50
    private static final Integer SELLER_ID = 18;
    private static final String SELLER_NAME = "数码家电111 ";
    //运费定价方式 2表示用运费模板
    private static final Integer FREIGHT_PRICING_WAY = 2;
    //运费模板id
    private static final Integer TEMPLATE_ID = 4;

    /**
     * 获取时间戳之后创建的商品
     */
    @Override
    public void getZheretaoAllGoods() {
        getGoodsCategoryByZheretao(TIME);
    }

    /**
     * 获取昨天的增量商品
     */
    @Override
    public void getZheretaoAddGoods() {
        DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00:00");
        String yesterday = LocalDateTime.now().minusDays(1).format(ftf);
        getGoodsCategoryByZheretao(yesterday);
    }

    /**
     * 更新商品信息或库存
     */
    @Override
    public Result updateGoods(ZheretaoRequestVo zheretaoRequestVo) {
        //浙里淘商品列表
        List<ZheretaoGood> zheretaoGoodList = zheretaoRequestVo.getZheretaoGoodList();
        //更新类型
        String updateType = zheretaoRequestVo.getUpdateType();
        if (!CollectionUtils.isEmpty(zheretaoGoodList) || "".equals(updateType)) {
            try {
                //1、获取浙里淘和商城分类ID的映射
                String setStr = settingManager.get(SettingGroup.ZHERETAO_CATEGORY);
                Map<Integer, Integer> mapping = JSON.parseObject(setStr, new TypeReference<Map<Integer, Integer>>() {
                });
                //2、获取系统商品详情
                List<String> upGoodsIds = zheretaoGoodList.stream().map(ZheretaoGood::getGoodId).collect(Collectors.toList());
                List<GoodsDO> goodsDOList = goodsManager.getListByUpGoodsIds(upGoodsIds.toArray(new String[0]));
                Map<String, GoodsDO> goodsDOMap = goodsDOList.stream().collect(Collectors.toMap(GoodsDO::getUpGoodsId, Function.identity()));
                //3、更新商品
                if (UpdateTypeEnum.GOODS.value().equals(updateType)) {
                    for (ZheretaoGood zheretaoGood : zheretaoGoodList) {
                        GoodsDO goodsDO = goodsDOMap.get(zheretaoGood.getGoodId());
                        editGoods(zheretaoGood, mapping.get(zheretaoGood.getCatId()), goodsDO.getGoodsId());
                    }
                }

                //更新库存
                if (UpdateTypeEnum.QUANTITY.value().equals(updateType)) {
                    for (ZheretaoGood zheretaoGood : zheretaoGoodList) {
                        //浙里淘的sku列表
                        List<SkuAmt> skuAmtList = zheretaoGood.getSkuAmtList();
                        // 循环浙里淘sku列表
                        if (!CollectionUtils.isEmpty(skuAmtList)) {
                            //浙里淘的规格名及规格值列表
                            List<GoodsSpecs> goodsSpecsZhe = zheretaoGood.getGoodsSpecsList();
                            //map中的字段 <规格ID，[规格名，规格值]>
                            Map<Integer, String[]> goodsSpecZheMap = new HashMap<>();
                            if (!CollectionUtils.isEmpty(goodsSpecsZhe)) {
                                for (GoodsSpecs spec : goodsSpecsZhe) {
                                    String[] specValue = {spec.getSpecsTitle(), spec.getSpecsName()};
                                    goodsSpecZheMap.put(spec.getSpecsId(), specValue);
                                }
                            }
                            //组装sku
                            List<GoodsSkuVO> skuVOList = this.getGoodsSkuList(goodsDOMap.get(zheretaoGood.getGoodId()), skuAmtList, goodsSpecZheMap);
                            if (!CollectionUtils.isEmpty(skuVOList)) {
                                List<GoodsQuantityVO> quantityList = new ArrayList<>();
                                for (GoodsSkuVO sku : skuVOList) {
                                    //实际库存vo
                                    GoodsQuantityVO actualQuantityVO = new GoodsQuantityVO();
                                    actualQuantityVO.setQuantity(sku.getQuantity());
                                    actualQuantityVO.setGoodsId(sku.getGoodsId());
                                    actualQuantityVO.setSkuId(sku.getSkuId());
                                    actualQuantityVO.setQuantityType(QuantityType.actual);

                                    //可用库存vo
                                    GoodsQuantityVO enableQuantityVO = new GoodsQuantityVO();
                                    enableQuantityVO.setQuantity(sku.getQuantity());
                                    enableQuantityVO.setGoodsId(sku.getGoodsId());
                                    enableQuantityVO.setSkuId(sku.getSkuId());
                                    enableQuantityVO.setQuantityType(QuantityType.enable);

                                    quantityList.add(actualQuantityVO);
                                    quantityList.add(enableQuantityVO);
                                }
                                //更新库存
                                goodsQuantityManager.updateSkuQuantity(quantityList);
                                //如果商品库存缓冲池开启了，那么需要立即同步数据库的商品库存，以保证商品库存显示正常
                                if (javashopConfig.isStock()) {
                                    goodsQuantityManager.syncDataBase();
                                }
                            }
                        }
                    }

                }
                return Result.success();
            } catch (Exception e) {
                logger.error("浙里淘更新商品信息异常：", e);
                return new Result(ResultCodeEnum.E615.getCode(), ResultCodeEnum.E615.getDescription());
            }

        } else {
            return new Result(ResultCodeEnum.E614.getCode(), ResultCodeEnum.E614.getDescription());
        }
    }

    /**
     * 保存浙里淘分类和系统分类的映射
     *
     * @param uploadFile 映射文件 第一列为浙里淘二级分类的Id，第二列为商城三级分类的名字
     * @return 保存失败的行
     */
    @Override
    public List<Integer> categoryMapping(MultipartFile uploadFile) {
        //保存失败的行
        List<Integer> failCell = new ArrayList<>();
        try {
            // 获取文件
            InputStream file = uploadFile.getInputStream();
            XSSFWorkbook excel = new XSSFWorkbook(file);
            // 加载第一张表格（XSSFSheet是Excel2017）
            XSSFSheet sheet = excel.getSheetAt(0);
            //key为浙里淘的分类ID，value为商城的分类ID
            Map<Integer, Integer> finalMap = new HashMap<>();

            String sql = "select  * from es_category where parent_id>0";
            List<CategoryDO> categoryDOList = this.daoSupport.queryForList(sql, CategoryDO.class);
            //保存三级目录
            Map<String, Integer> map = new HashMap<>();
            for (CategoryDO category : categoryDOList) {
                String[] str = category.getCategoryPath().replace("|", ",").split(",");
                if (str.length >= 4) {
                    map.put(category.getName(), category.getCategoryId());
                }
            }
            // 循环每一行
            for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                // 获取第i行
                XSSFRow row = sheet.getRow(i);
                //获取第一列的数据（excel中数据格式为number)
                double zheDou = row.getCell(0).getNumericCellValue();
                // 商城的三级分类名
                String categoryName = row.getCell(1).toString();
                if (!"".equals(categoryName)) {
                    //浙里淘的分类ID
                    Integer zheCategoryId = new Double(zheDou).intValue();
                    //商城的分类ID
                    Integer shopCategoryId = map.get(categoryName);

                    if (shopCategoryId != null) {
                        finalMap.put(zheCategoryId, map.get(categoryName));
                    } else {
                        failCell.add(i + 1);
                    }
                }
            }
            //map转json类型的字符串，保存到es_settings表中
            settingManager.save(SettingGroup.ZHERETAO_CATEGORY, finalMap);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return failCell;
    }

    // 获取商品分类
    private void getGoodsCategoryByZheretao(String timeStr) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        String url = BASE_URL + "/listRemoteGoodsCat?apiKey={apiKey}";
        Map<String, String> map = new HashMap();
        map.put("apiKey", API_KEY);
        String result = restTemplate.getForObject(url, String.class, map);
        ZheretaoResponse<List<Category>> zheretaoResponse = JSON.parseObject(result, new TypeReference<ZheretaoResponse<List<Category>>>() {
        });
        if (zheretaoResponse.getCode() == 200) {
            logger.debug("获取商品分类数据成功！");
            //一级分类
            List<Category> primaryCategoryList = zheretaoResponse.getData().result;
            for (Category primary : primaryCategoryList) {
                //二级分类
                List<Category> secondaryCategoryList = primary.getChildren();
                //分类的ID
                List<Integer> secondaryIds = new ArrayList<>();
                if (!CollectionUtils.isEmpty(secondaryCategoryList)) {
                    for (Category secondary : secondaryCategoryList) {
                        secondaryIds.add(secondary.getId());
                    }
                    getGoodsListByZheretao(secondaryIds);
                }
            }
        } else {
            logger.debug("数据获取失败！");
        }
    }

    /**
     * 分页获取TIME之后创建的商品
     *
     * @param classificationIds 二级分类ID
     */
    private void getGoodsListByTimeZheretao(List<Integer> classificationIds, String timeStr) {
        //获取浙里淘和商城分类ID的映射
        String setStr = settingManager.get(SettingGroup.ZHERETAO_CATEGORY);
        //解析json key为浙里淘的分类ID，value为商城的分类ID
        Map<Integer, Integer> mapping = JSON.parseObject(setStr, new TypeReference<Map<Integer, Integer>>() {
        });

        if (mapping != null && classificationIds != null && mapping.size() > 0 && classificationIds.size() > 0) {
            String goodsZheSql = "select up_goods_id as up_goods_id, goods_id as goods_id from es_goods  where supplier_name='浙里淘' and seller_id=?";
            List<GoodsDO> goodsList = this.daoSupport.queryForList(goodsZheSql, GoodsDO.class, SELLER_ID);
            //已经保存过的商品 map<浙里淘的商品ID，系统的商品ID>
            Map<String, Integer> goodsIdMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(goodsList)) {
                goodsIdMap = goodsList.stream().collect(Collectors.toMap(GoodsDO::getUpGoodsId, GoodsDO::getGoodsId));
            }

            //分页拉取TIME之后创建的商品
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            String url = BASE_URL + "/listRemoteAllGoodsByCat";
            Map<String, Object> mapReques = new HashMap();
            mapReques.put("apiKey", API_KEY);
            mapReques.put("catIdList", classificationIds);
            //拉取这个时间之后创建的商品
            long gmtCreate = DateUtil.getDateline(timeStr);
            mapReques.put("gmtCreate", gmtCreate);
            mapReques.put("pageSize", PAGE_SIZE);

            //是否有下一页
            Boolean nextPageState = true;
            Integer page = 1;
            do {
                mapReques.put("curPage", page);
                String result = restTemplate.postForObject(url, mapReques, String.class);
                ZheretaoResponse<List<ZheretaoGood>> goodsListResponse = JSON.parseObject(result, new TypeReference<ZheretaoResponse<List<ZheretaoGood>>>() {
                });
                if (goodsListResponse.getCode() == 200) {
                    logger.debug("获取商品列表数据成功！");
                    //商品列表
                    List<ZheretaoGood> goodsListZhe = goodsListResponse.getData().result;
                    if (!CollectionUtils.isEmpty(goodsListZhe)) {
                        Integer addInt = 0;
                        Integer editInt = 0;
                        for (ZheretaoGood goodCategory : goodsListZhe) {
                            //获取商品详情
                            HttpHeaders headersGood = new HttpHeaders();
                            headersGood.add("Accept", "application/json");
                            Map<String, Object> mapGood = new HashMap();
                            mapGood.put("apiKey", API_KEY);
                            mapGood.put("goodsId", goodCategory.getGoodsId());
                            String urlGood = BASE_URL + "/getGoodsDetail?apiKey={apiKey}&goodsId={goodsId}";
                            //获取商品详情
                            String goodResult = restTemplate.getForObject(urlGood, String.class, mapGood);
                            ZheretaoResponse<ZheretaoGood> goodDetailResponse = JSON.parseObject(goodResult, new TypeReference<ZheretaoResponse<ZheretaoGood>>() {
                            });
                            if (goodDetailResponse.getCode() == 200) {
                                //商品详情
                                ZheretaoGood goodDetail = goodDetailResponse.getData().result;
                                logger.debug("获取商品详情数据成功！ goodId===" + goodDetail.getGoodId());
                                //根据浙里淘商品的分类ID获取商城的分类ID
                                Integer catId = mapping.get(Integer.parseInt(goodCategory.getCatId()));
                                if (catId != null) {
                                    //系统商品ID
                                    Integer goodsId = goodsIdMap.get(goodDetail.getGoodId());
                                    if (goodsId != null && goodsId > 0) {
                                        editGoods(goodDetail, catId, goodsId);
                                        editInt += 1;
                                    } else {
                                        addGoods(goodDetail, catId);
                                        addInt += 1;
                                    }
                                }

                            }
                        }
                        page += 1;
                        logger.debug("---------------------------成功添加商品     " + addInt.toString() + "     成功修改商品    " + editInt.toString());
                    } else {
                        nextPageState = false;
                        logger.debug("浙里淘分类ID为：" + classificationIds.toString() + " 没有查询到商品");
                    }
                }
            } while (nextPageState);
        }
    }

    /**
     * 获取分类商品列表（只能拿到最新的50条数据）
     *
     * @param classificationIds 浙里淘二级分类的ID
     */
    private void getGoodsListByZheretao(List<Integer> classificationIds) {
        //获取浙里淘和商城分类ID的映射
        String setStr = settingManager.get(SettingGroup.ZHERETAO_CATEGORY);
        //解析json key为浙里淘的分类ID，value为商城的分类ID
        Map<Integer, Integer> mapping = JSON.parseObject(setStr, new TypeReference<Map<Integer, Integer>>() {
        });

        if (mapping != null && mapping.size() > 0 && CollectionUtils.isEmpty(classificationIds)) {
            String goodsZheSql = "select up_goods_id as up_goods_id, goods_id as goods_id from es_goods  where supplier_name='浙里淘' and seller_id=?";
            List<GoodsDO> goodsList = this.daoSupport.queryForList(goodsZheSql, GoodsDO.class, SELLER_ID);
            //已经保存过的商品 map<浙里淘的商品ID，系统的商品ID>
            Map<String, Integer> goodsIdMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(goodsList)) {
                goodsIdMap = goodsList.stream().collect(Collectors.toMap(GoodsDO::getUpGoodsId, GoodsDO::getGoodsId));
            }
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            String url = BASE_URL + "/listRemoteGoodsByCat";
            Map<String, Object> mapReques = new HashMap();
            mapReques.put("apiKey", API_KEY);
            mapReques.put("catIdList", classificationIds);
            String result = restTemplate.postForObject(url, mapReques, String.class);
            ZheretaoResponse<List<ZheretaoGood>> goodsListResponse = JSON.parseObject(result, new TypeReference<ZheretaoResponse<List<ZheretaoGood>>>() {
            });
            if (goodsListResponse.getCode() == 200) {
                logger.debug("获取商品列表数据成功！");
                //商品列表
                List<ZheretaoGood> goodsListZhe = goodsListResponse.getData().result;
                if (!CollectionUtils.isEmpty(goodsListZhe)) {
                    Integer addInt = 0;
                    Integer editInt = 0;
                    for (ZheretaoGood goodCategory : goodsListZhe) {
                        //获取商品详情
                        HttpHeaders headersGood = new HttpHeaders();
                        headersGood.add("Accept", "application/json");
                        Map<String, Object> mapGood = new HashMap();
                        mapGood.put("apiKey", API_KEY);
                        mapGood.put("goodsId", goodCategory.getGoodsId());
                        String urlGood = BASE_URL + "/getGoodsDetail?apiKey={apiKey}&goodsId={goodsId}";
                        //获取商品详情
                        String goodResult = restTemplate.getForObject(urlGood, String.class, mapGood);
                        ZheretaoResponse<ZheretaoGood> goodDetailResponse = JSON.parseObject(goodResult, new TypeReference<ZheretaoResponse<ZheretaoGood>>() {
                        });
                        if (goodDetailResponse.getCode() == 200) {
                            //商品详情
                            ZheretaoGood goodDetail = goodDetailResponse.getData().result;
                            logger.debug("获取商品详情数据成功！ goodId===" + goodDetail.getGoodId());
                            //根据浙里淘商品的分类ID获取商城的分类ID
                            Integer catId = mapping.get(Integer.parseInt(goodCategory.getCatId()));
                            if (catId != null) {
                                //系统商品ID
                                Integer goodsId = goodsIdMap.get(goodDetail.getGoodId());
                                if (goodsId != null && goodsId > 0) {
                                    editGoods(goodDetail, catId, goodsId);
                                    editInt += 1;
                                } else {
                                    addGoods(goodDetail, catId);
                                    addInt += 1;
                                }
                            }
                        }
                    }
                    logger.debug("---------------------------成功添加商品     " + addInt.toString() + "     成功修改商品    " + editInt.toString());
                } else {
                    logger.debug("浙里淘分类ID为：" + classificationIds.toString() + " 没有查询到商品");
                }
            }
        }
    }

    /**
     * 新增商品
     *
     * @param goodDetail 浙里淘商品详情
     * @param categoryId 系统商城的分类ID
     */
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void addGoods(ZheretaoGood goodDetail, Integer categoryId) {
        //如果没有映射分类ID则不能存储
        if (categoryId != null) {
            long start = System.currentTimeMillis();
            GoodsDO goodsDO = formatGood(goodDetail);
            //分类ID
            goodsDO.setCategoryId(categoryId);
            //创建时间
            goodsDO.setCreateTime(DateUtil.getDateline());
            // 商品最后更新时间
            goodsDO.setLastModify(DateUtil.getDateline());
            // 添加商品
            this.daoSupport.insert(goodsDO);
            // 获取添加商品的商品ID
            Integer goodsId = this.daoSupport.getLastId("es_goods");
            goodsDO.setGoodsId(goodsId);

            //设置分销返利
            addDistributionGoods(goodsId);

            //浙里淘的sku列表
            List<SkuAmt> skuAmtList = goodDetail.getSkuAmtList();
            //商城的sku数组
            List<GoodsSkuVO> goodsSkuVOList = new ArrayList<>();

            // 循环浙里淘sku列表
            if (!CollectionUtils.isEmpty(skuAmtList)) {
                //浙里淘的规格名及规格值列表
                List<GoodsSpecs> goodsSpecsZhe = goodDetail.getGoodsSpecsList();
                //map中的字段 <规格ID，[规格名，规格值]>
                Map<Integer, String[]> goodsSpecZheMap = new HashMap<>();
                if (!CollectionUtils.isEmpty(goodsSpecsZhe)) {
                    for (GoodsSpecs spec : goodsSpecsZhe) {
                        String[] specValue = {spec.getSpecsTitle(), spec.getSpecsName()};
                        goodsSpecZheMap.put(spec.getSpecsId(), specValue);
                    }
                    goodsSkuVOList = getGoodsSkuList(goodsDO, skuAmtList, goodsSpecZheMap);
                }
            }
            // 有没有规格都需要添加商品sku信息
            this.goodsSkuManager.add(goodsSkuVOList, goodsDO);
            long end = System.currentTimeMillis();
            logger.debug("新增商品" + goodDetail.getGoodId().toString() + "耗时=============================================================================" + (end - start));
        }
    }

    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void editGoods(ZheretaoGood goodDetail, Integer categoryId, Integer goodsId) {
        long start = System.currentTimeMillis();
        GoodsDO goodsDO = formatGood(goodDetail);
        goodsDO.setGoodsId(goodsId);
        //分类ID
        goodsDO.setCategoryId(categoryId);
        // 商品最后更新时间
        goodsDO.setLastModify(DateUtil.getDateline());
        // 修改商品
        this.daoSupport.update(goodsDO, goodsId);
        //设置分销返利
        addDistributionGoods(goodsId);
        //浙里淘的sku列表
        List<SkuAmt> skuAmtList = goodDetail.getSkuAmtList();
        //商城的sku列表
        String skuListSql = "select * from es_goods_sku where goods_id=? and seller_id=?";
        List<GoodsSkuDO> shopGoodsList = this.daoSupport.queryForList(skuListSql, GoodsSkuDO.class, goodsId, SELLER_ID);
        Map<String, Integer> shopGoodsMap = shopGoodsList.stream().collect(Collectors.toMap(GoodsSkuDO::getUpSkuId, GoodsSkuDO::getSkuId));
        //要保存的sku列表
        List<GoodsSkuVO> goodsSkuVOList = new ArrayList<>();
        // 循环浙里淘sku列表
        if (!CollectionUtils.isEmpty(skuAmtList)) {
            //浙里淘的规格名及规格值列表
            List<GoodsSpecs> goodsSpecsZhe = goodDetail.getGoodsSpecsList();
            //map中的字段 <规格ID，[规格名，规格值]>
            Map<Integer, String[]> goodsSpecZheMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(goodsSpecsZhe)) {
                for (GoodsSpecs spec : goodsSpecsZhe) {
                    String[] specValue = {spec.getSpecsTitle(), spec.getSpecsName()};
                    goodsSpecZheMap.put(spec.getSpecsId(), specValue);
                }
            }
            goodsSkuVOList = getGoodsSkuList(goodsDO, skuAmtList, goodsSpecZheMap);
            if (!CollectionUtils.isEmpty(goodsSkuVOList)) {
                for (GoodsSkuVO g : goodsSkuVOList) {
                    //如果是已经存在的sku，则把系统的skuId放进去
                    if (shopGoodsMap.get(g.getUpSkuId()) != null) {
                        g.setSkuId(shopGoodsMap.get(g.getUpSkuId()));
                    }
                }
            }
        }
        //修改sku，没有就新增，有就修改，不存在的sku就删除
        this.goodsSkuManager.edit(goodsSkuVOList, goodsDO);
        long end = System.currentTimeMillis();
        logger.debug("更新商品" + goodDetail.getGoodId().toString() + "耗时==================================" + (end - start));
    }

    /**
     * 组装商品的sku列表
     *
     * @param goodsDO         sku所属的商品
     * @param skuAmtList      浙里淘sku列表
     * @param goodsSpecZheMap 浙里淘规格列表
     * @return
     */
    private List<GoodsSkuVO> getGoodsSkuList(GoodsDO goodsDO, List<SkuAmt> skuAmtList, Map<Integer, String[]> goodsSpecZheMap) {
        //商城的sku数组
        List<GoodsSkuVO> goodsSkuVOList = new ArrayList<>();
        Integer categoryId = goodsDO.getCategoryId();
        //批量保存的规格值
        List<SpecValuesDO> specValueDOList = new ArrayList<>();

        // 保存已经匹配过的规格名  Map<浙里淘的规格名,系统的规格名ID>
        Map<String, Integer> zheretaoSpecMap = new HashMap<>();
        for (SkuAmt skuAmt : skuAmtList) {
            //sku的规格不为空
            if (!skuAmt.getSpecsIdStr().equals("")) {
                GoodsSkuVO skuVO = new GoodsSkuVO();
                //当前sku中的规格值列表
                List<SpecValueVO> specValueVOList = new ArrayList<>();
                //保存已经匹配过的规格值，Map<浙里淘的规格值,规格值的状态> value值大于0表示已经匹配过
                Map<String, Integer> zheretaoSpecValMap = new HashMap<>();
                String[] specsIdsArrayZhe = skuAmt.getSpecsIdStr().split(",");
                //sku的规格值
                for (int i = 0; i < specsIdsArrayZhe.length; i++) {
                    //sku关联的规格ID
                    Integer skuSpecId = Integer.parseInt(specsIdsArrayZhe[i]);
                    //goodsSpecZheMap.get(skuSpecId)：sku中的规格id，对应的规格
                    if (skuSpecId > 0 && goodsSpecZheMap.size() > 0 && goodsSpecZheMap.get(skuSpecId) != null) {
                        //浙里淘的规格名
                        String specName = goodsSpecZheMap.get(skuSpecId)[0];
                        //浙里淘的规格值
                        String specValue = goodsSpecZheMap.get(skuSpecId)[1];
                        //匹配规格名
                        if (zheretaoSpecMap == null || zheretaoSpecMap.size() == 0) {
                            //查询系统中符合条件的规格名并保存到zheretaoSpecMap里 分类，sellerId,规格名
                            String sql = "select s.* from es_specification s inner join es_category_spec cs on s.spec_id = cs.spec_id  where disabled = 1 and category_id = ? and seller_id = ? ";
                            List<SpecificationDO> specificationDOList = this.daoSupport.queryForList(sql, SpecificationDO.class, categoryId, SELLER_ID);
                            // 保存已经系统中有的规格名  Map<系统的规格名,系统的规格名ID>
                            Map<String, Integer> specMap = specificationDOList.stream().collect(Collectors.toMap(SpecificationDO::getSpecName, SpecificationDO::getSpecId));
                            if (specMap != null && specMap.size() > 0)
                                zheretaoSpecMap.putAll(specMap);
                        }
                        if (zheretaoSpecMap.get(specName) == null) {
                            SpecificationDO newSpec = new SpecificationDO();
                            //存储规格名
                            newSpec.setSellerId(SELLER_ID);
                            newSpec.setSpecName(specName);
                            newSpec.setSpecMemo("浙里淘商家自定义");
                            newSpec = specificationManager.add(newSpec);
                            Integer specId = newSpec.getSpecId();
                            //保存分类规格的关系
                            CategorySpecDO categorySpec = new CategorySpecDO(categoryId, specId);
                            this.daoSupport.insert(categorySpec);
                            //匹配过的规格名存在map中
                            zheretaoSpecMap.put(specName, specId);
                        }

                        //系统的规格名ID
                        Integer specId = zheretaoSpecMap.get(specName);
                        SpecValuesDO specValuesDO = new SpecValuesDO();
                        specValuesDO.setSpecId(specId);
                        specValuesDO.setSpecName(specName);
                        specValuesDO.setSpecValue(specValue);
                        specValuesDO.setSellerId(SELLER_ID);
                        //没有保存过规格值 或者数据库中没有
                        if (zheretaoSpecValMap == null || zheretaoSpecValMap.size() == 0) {
                            String sql = "select * from es_spec_values where spec_id = ? and seller_id = ? group by spec_value";
                            //系统中该规格名下的所有规格值
                            List<SpecValuesDO> specValuesDOList = this.daoSupport.queryForList(sql, SpecValuesDO.class, specId, SELLER_ID);
                            Map<String, Integer> specValueMap = specValuesDOList.stream().collect(Collectors.toMap(SpecValuesDO::getSpecValue, SpecValuesDO::getSpecValueId));
                            if (specValueMap != null && specValueMap.size() > 0)
                                zheretaoSpecValMap.putAll(specValueMap);
                        }
                        if (zheretaoSpecValMap.get(specValue) == null) {
                            //当前规格名下没有规格值，或规格值没有重复，则保存规格值
                            // specValuesManager.add(specValuesDO);
                            specValueDOList.add(specValuesDO);
                        }
                        SpecValueVO specValueVO = new SpecValueVO();
                        BeanUtils.copyProperties(specValuesDO, specValueVO);
                        specValueVOList.add(specValueVO);
                        zheretaoSpecValMap.put(specValue, 0);
                    }
                }
                if (!CollectionUtils.isEmpty(specValueVOList)) {
                    skuVO.setSpecList(specValueVOList);
                    BeanUtils.copyProperties(goodsDO, skuVO);
                    //上游skuId
                    skuVO.setUpSkuId(skuAmt.getAmtId());
                    goodsSkuVOList.add(skuVO);
                }
            }
        }

        //批量保存规格值
        if (!CollectionUtils.isEmpty(specValueDOList)) {
            specValuesManager.addSpecValuesList(specValueDOList);
        }
        return goodsSkuVOList;
    }

    /**
     * 格式化商品，填默认值
     *
     * @param goodDetail 浙里淘获取的商品信息
     * @return
     */
    private GoodsDO formatGood(ZheretaoGood goodDetail) {
        //判断商品是新增还是修改
        GoodsDO goodsDO = new GoodsDO();
        //商品名
        goodsDO.setGoodsName(goodDetail.getTitle());

        //对方商品ID
        goodsDO.setUpGoodsId(goodDetail.getGoodId());
        //库存
        goodsDO.setQuantity(goodDetail.getInitStock());
        //可用用库存
        goodsDO.setEnableQuantity(goodDetail.getInitStock());
        //商品头图
        String picUrl = goodDetail.getPics().get(0);
        goodsDO.setThumbnail(picUrl);
        goodsDO.setBig(picUrl);
        goodsDO.setSmall(picUrl);
        goodsDO.setOriginal(picUrl);
        //商品详情
        String[] pic = goodDetail.getDescription().split("https://");
        StringBuffer pre = new StringBuffer("<p>");
        for (int i = 1; i < pic.length; i++) {
            String name = pic[i].substring(pic[i].lastIndexOf("/") + 1);
            pre.append("<img src=\"https://" + pic[i] + "\" title=\"" + name + "\" alt=\"" + name + "\"/>");
        }
        pre.append("</p>");
        goodsDO.setIntro(pre.toString());

        //售价
        goodsDO.setPrice(goodDetail.getBackAmount());
        //原价 划线价格
        goodsDO.setMktprice(goodDetail.getOrderAmount());
        //接口新增参数，目前只有浙里淘测试环境有
        //成本价 takeAmount
        goodsDO.setCost(goodDetail.getTakeAmount());
        // 上架状态 1 上架   status:上下架状态，ENABLED上架 UNENABLED下架
        goodsDO.setMarketEnable(goodDetail.getStatus().equals(ZheretaoGoodsStatus.ENABLED) ? 1 : 0);
        // goodsDO.setMarketEnable(1);
        // 成本价 takeAmount
        // goodsDO.setCost(goodDetail.getBackAmount());

        //商品编码
        String sn = PinYinUtil.getPinYinHeadChar("zheretao_" + DateUtil.toString(new Date(), "yyyyMMdd") + "_" + ((int) (Math.random() * 900) + 100));
        goodsDO.setSn(sn);
        //商品类型
        goodsDO.setGoodsType(GoodsType.NORMAL.name());
        //卖家ID
        goodsDO.setSellerId(SELLER_ID);
        goodsDO.setSellerName(SELLER_NAME);
        //供应商名
        goodsDO.setSupplierName("浙里淘");
        //商品店铺分组
        // goodsDO.setShopCatId(SHOP_CAT_ID);
        //是否是自营商品 0 不是 1是
        goodsDO.setSelfOperated(0);
        //不需要审核
        goodsDO.setIsAuth(1);

        //运费定价方式 2表示用运费模板
        goodsDO.setFreightPricingWay(FREIGHT_PRICING_WAY);
        //运费模板id
        goodsDO.setTemplateId(TEMPLATE_ID);
        //重量
        goodsDO.setWeight(1.00);
        // 谁承担运费0：买家承担，1：卖家承担
        goodsDO.setGoodsTransfeeCharge(1);
        //不是本地商品
        goodsDO.setIsLocal(0);
        goodsDO.setIsGlobal(1);
        //不支持自提
        goodsDO.setIsSelfTake(0);
        // 商品浏览次数
        goodsDO.setViewCount(0);
        // 商品购买数量
        goodsDO.setBuyCount(0);
        //好评率
        goodsDO.setGrade(100.0);
        //评论数
        goodsDO.setCommentNum(0);
        //未删除
        goodsDO.setDisabled(1);

        //sku列表
        List<SkuAmt> skuAmtList = goodDetail.getSkuAmtList();
        if (!CollectionUtils.isEmpty(skuAmtList)) {
            goodsDO.setHaveSpec(1);
        } else {
            //没有规格
            goodsDO.setHaveSpec(0);
        }
        return goodsDO;
    }

    //设置分销返利
    private void addDistributionGoods(Integer goodsId) {
        DistributionGoods distributionGoods = new DistributionGoods();
        distributionGoods.setGoodsId(goodsId);
        distributionGoods.setGrade1Rebate(8.00);
        distributionGoods.setGrade2Rebate(1.00);
        distributionGoods.setInviterRate(1.00);
        distributionGoodsManager.edit(distributionGoods);
    }
}
