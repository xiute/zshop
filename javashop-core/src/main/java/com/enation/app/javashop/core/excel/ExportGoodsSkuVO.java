package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.enation.app.javashop.core.goods.model.dos.GoodsSkuDO;
import com.enation.app.javashop.core.goods.model.vo.SpecValueVO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.PayStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ShipStatusEnum;
import com.enation.app.javashop.core.trade.order.support.OrderSpecialStatus;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.JsonUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * @author JFeng
 * @date 2020/7/17 14:31
 */

@Data
public class ExportGoodsSkuVO {

    @ExcelProperty("所属店铺")
    private String sellerName;

    @ExcelProperty("商品ID")
    private Integer goodsId;

    @ExcelProperty("商品名称")
    private String goodsName;

    @ExcelProperty("商品类目")
    private String categoryName;

    //@ExcelProperty("商品价格")
    //private Double price;

    //@ExcelProperty("市场价格")
    //private Double mktprice;

    @ExcelProperty("供货商")
    private String supplierName;

    @ExcelProperty("外部商品ID")
    private String upGoodsId;
    //==========================================================================

    @ExcelProperty("sku_id")
    private Integer skuId;

    @ExcelProperty("sku规格")
    private String specs;

    @ExcelProperty("sku货号")
    private String sn;

    @ExcelProperty("sku重量")
    private Double weight;

    @ExcelProperty("sku总库存")
    private Integer quantity;

    @ExcelProperty("sku可用库存")
    private Integer enableQuantity;

    @ExcelProperty("sku售价")
    private Double price;

    @ExcelProperty("sku成本价")
    private Double cost;

    @ExcelProperty("外部SKU_ID")
    private String upSkuId;

    public ExportGoodsSkuVO(Map goods, GoodsSkuDO goodsSkuDO) {
        this.goodsId=Integer.valueOf(goods.get("goods_id").toString());
        this.goodsName=goods.get("goods_name").toString();
        this.sellerName=goods.get("seller_name").toString();
        this.supplierName=goods.get("supplier_name")==null?"":goods.get("supplier_name").toString();
        this.upGoodsId=goods.get("up_goods_id")==null?"":goods.get("up_goods_id").toString();
        this.categoryName=goods.get("category_name").toString();

        this.skuId=goodsSkuDO.getSkuId();
        this.sn=goodsSkuDO.getSn();
        this.weight=goodsSkuDO.getWeight();
        this.quantity=goodsSkuDO.getQuantity();
        this.enableQuantity=goodsSkuDO.getEnableQuantity();
        this.price=goodsSkuDO.getPrice();
        this.cost=goodsSkuDO.getCost();
        this.upSkuId=goodsSkuDO.getUpSkuId();

        String specValues="";
        if (goodsSkuDO.getSpecs() != null) {
            List<SpecValueVO> specValueVOS = JsonUtil.jsonToList(goodsSkuDO.getSpecs(), SpecValueVO.class);
            for (SpecValueVO specValueVO : specValueVOS) {
                specValues+= specValueVO.getSpecName()+":"+specValueVO.getSpecValue()+";";
            }
        }
        this.specs=specValues;

    }
}
