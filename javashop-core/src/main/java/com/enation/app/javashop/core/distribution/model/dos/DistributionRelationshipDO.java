package com.enation.app.javashop.core.distribution.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *团长关系表
 */
@Data
@Table(name = "es_distribution_relationship")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DistributionRelationshipDO {
    /**
     * 主键id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;

    /**
     * 会员id
     */
    @Column(name = "member_id")
    @ApiModelProperty(value = "member_id", required = true)
    private Integer memberId;

    /**
     * 上级团长id
     */
    @Column(name = "member_lv1_id")
    @ApiModelProperty(value = "上级团长id", required = true)
    private Integer memberLv1Id;

    /**
     * 上级团长手机号
     */
    @Column(name = "member_lv1_mobile")
    @ApiModelProperty(value = "上级团长手机号", required = true)
    private String memberLv1Mobile;

    /**
     *
     * 操作类型
     */
    @Column(name = "operation_type")
    @ApiModelProperty(value = "操作类型", required = true)
    private String operationType;

    /**
     * 操作方式
     */
    @Column(name = "operation_mode")
    @ApiModelProperty(value = "操作方式", required = true)
    private String operationMode;

    /**
     * 操作描述
     */
    @Column(name = "operation_describe")
    @ApiModelProperty(value = "操作描述", required = true)
    private String operationDescribe;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(value = "创建时间", required = true)
    private Long createTime;
}
