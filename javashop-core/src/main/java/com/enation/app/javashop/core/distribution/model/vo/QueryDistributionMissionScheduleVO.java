package com.enation.app.javashop.core.distribution.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author 孙建
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class QueryDistributionMissionScheduleVO implements Serializable {

    @ApiModelProperty(value = "页码", name = "page_no", required = true)
    @NotNull(message = "请输入当前页码")
    private Integer pageNo;

    @ApiModelProperty(value = "分页大小", name = "page_size", required = true)
    @NotNull(message = "请输入分页大小")
    private Integer pageSize;

    @ApiModelProperty(value = "店铺ID", name = "seller_id", hidden = true)
    private Integer sellerId;
    /**
     * 团长任务状态
     */
    @ApiModelProperty(name = "mission_status", value = "团长任务状态")
    private Integer missionStatus;

    /**
     * 任务明细类型（0：每日任务，1：每月任务）
     */
    @ApiModelProperty(name = "mission_type", value = "任务明细类型（0：每日任务，1：每月任务）")
    private Integer missionType;
    /**
     * 任务明细指标（0：下单人数，1：成交金额）
     */
    @ApiModelProperty(name = "mission_target", value = "任务明细指标（0：下单人数，1：成交金额）")
    private Integer missionTarget;
    /**
     * 任务期间
     */
    @ApiModelProperty(name = "mission_time", value = "任务期间")
    private String missionTime;
    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    private String sellerName;
    /**
     * 团长名称
     */
    @Column(name = "leader_name")
    private String leaderName;

}
