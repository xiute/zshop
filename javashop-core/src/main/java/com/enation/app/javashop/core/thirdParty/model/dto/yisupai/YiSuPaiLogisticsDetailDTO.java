package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/5/27
 * @Description: 物流信息详情
 */
@Data
public class YiSuPaiLogisticsDetailDTO {
    // 发生时间
    private String ocurrTimeStr;
    // 物流信息
    private String standerdDesc;
}
