package com.enation.app.javashop.core.shop.model.dto;

import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

/**
 * @author JFeng
 * @date 2020/5/22 11:17
 */

@Data
public class PeekTime  implements Serializable {
    private static final long serialVersionUID = 6348162955890093L;


    @ApiParam("时段费用")
    private Double peekTimePrice;

    @ApiParam("高峰时段开始")
    private String peekTimeStart;

    @ApiParam("高峰时段结束")
    private String peekTimeEnd;

    private int startTimeCount;

    private int endTimeCount;
}
