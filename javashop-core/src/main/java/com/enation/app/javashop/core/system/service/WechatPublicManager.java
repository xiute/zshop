package com.enation.app.javashop.core.system.service;

import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.system.enums.WechatMiniproTemplateTypeEnum;
import com.enation.app.javashop.core.system.model.dos.WxPublicTemplate;
import com.enation.app.javashop.core.system.model.dto.WeChatPublicUserInfo;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * 微信公众号业务层
 */
public interface WechatPublicManager {

    /**
     * 处理微信公众号的消息和事件
     * @param request HttpServletRequest
     * @return 回复的消息
     */
    String processRequest(HttpServletRequest request);

    /**
     * 从微信获取公众号的access_token
     */
    String getTokenFromWeiXin();

    /**
     *
     * @param openId 订阅人的openID
     * @return WeChatPublicUserInfo 订阅人的个人信息
     */
    WeChatPublicUserInfo getUserInfo(String openId);

    /**
     * 从缓存获取公众号的access_token
     */
    String getTokenFromCache();

    /**
     * 微信公众号给粉丝发模板消息（复用小程序发消息的DTO）
     */
    void publicSendMessage(WxPublicTemplate wxPublicTemplate);

    void sendExtensionMessage(OrderDO orderDO, BigDecimal commissionMoney, Integer memberIdLv1, WechatMiniproTemplateTypeEnum templateTypeEnum, Long startTime,
                              Integer spreadWay, String orderTypeName, String commissionTypeName );

    void sendProfitMessageFromPublic(OrderDO orderDO, BigDecimal commissionMoney, Integer memberIdLv1,
                                            WechatMiniproTemplateTypeEnum templateTypeEnum, Long startTime,
                                            Integer spreadWay, String commissionTypeName );

    void sendRecommendedCouponsMessageFromPublic(Member member,WechatMiniproTemplateTypeEnum templateTypeEnum,Integer myFriendsRegisterReward);

}