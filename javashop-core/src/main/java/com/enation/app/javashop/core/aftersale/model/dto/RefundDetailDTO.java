package com.enation.app.javashop.core.aftersale.model.dto;

import com.enation.app.javashop.core.aftersale.model.dos.RefundGoodsDO;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @Description 退货(款)单详细DTO
 * @ClassName RefundDetailDTO
 * @author zjp
 * @version v7.0
 * @since v7.0 上午11:32 2018/5/8
 */
public class RefundDetailDTO implements Serializable{
    @ApiModelProperty(value = "退货（款）单")
    private RefundDTO refund;

    @ApiModelProperty(value = "退货商品")
    private List<RefundGoodsDO> refundGoods;

    @ApiModelProperty(value = "退货商品")
    private List<RefundGoodsDTO> refundGoodsDTO;

    public RefundDTO getRefund() {
        return refund;
    }

    public void setRefund(RefundDTO refund) {
        this.refund = refund;
    }

    public List<RefundGoodsDO> getRefundGoods() {
        return refundGoods;
    }

    public void setRefundGoods(List<RefundGoodsDO> refundGoods) {
        this.refundGoods = refundGoods;
    }

    public List<RefundGoodsDTO> getRefundGoodsDTO() {
        return refundGoodsDTO;
    }

    public void setRefundGoodsDTO(List<RefundGoodsDTO> refundGoodsDTO) {
        this.refundGoodsDTO = refundGoodsDTO;
    }

    @Override
    public String toString() {
        return "RefundDetailDTO{" +
                "refund=" + refund +
                ", refundGoods=" + refundGoods +
                '}';
    }
}
