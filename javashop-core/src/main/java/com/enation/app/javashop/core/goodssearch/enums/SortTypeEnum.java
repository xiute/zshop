package com.enation.app.javashop.core.goodssearch.enums;


/**
 * @author JFENG
 */

public enum SortTypeEnum {


    GENERAL("综合排序"),
    GENERAL_SHIP_PRICE("配送价格"),
    GENERAL_BASE_SHIP_PRICE("起送价格"),
    GENERAL_SALE_COUNT("销量"),
    GENERAL_GRADE("商家星级"),
    DISTANCE("距离"),
    CATEGORY("类型");

    private String description;

    SortTypeEnum(String description) {
        this.description = description;

    }
}
