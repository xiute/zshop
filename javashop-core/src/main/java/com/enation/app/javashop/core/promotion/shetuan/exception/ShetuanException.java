package com.enation.app.javashop.core.promotion.shetuan.exception;

import org.springframework.http.HttpStatus;

public class ShetuanException extends Exception {
    public ShetuanException(String msg) {

        super(msg);

    }
}
