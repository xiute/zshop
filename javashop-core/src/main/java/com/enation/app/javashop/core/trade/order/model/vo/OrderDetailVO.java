package com.enation.app.javashop.core.trade.order.model.vo;

import com.enation.app.javashop.core.member.model.dos.LeaderDO;
import com.enation.app.javashop.core.member.model.dos.ReceiptHistory;
import com.enation.app.javashop.core.promotion.fulldiscount.model.dos.FullDiscountGiftDO;
import com.enation.app.javashop.core.promotion.pintuan.model.PintuanOrderDetailVo;
import com.enation.app.javashop.core.trade.cart.model.vo.CouponVO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.*;
import com.enation.app.javashop.core.trade.order.support.OrderSpecialStatus;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.List;

/**
 * 订单明细
 *
 * @author Snow create in 2018/5/15
 * @version v2.0
 * @since v7.0.0
 */
@ApiModel(description = "订单明细")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderDetailVO extends OrderDO {

    private static final long serialVersionUID = 1781685707569607250L;
    @ApiModelProperty(value = "订单操作允许情况")
    private OrderOperateAllowable orderOperateAllowableVO;

    @ApiModelProperty(value = "订单状态文字")
    private String orderStatusText;

    @ApiModelProperty(value = "付款状态文字")
    private String payStatusText;

    @ApiModelProperty(value = "发货状态文字")
    private String shipStatusText;

    @ApiModelProperty(value = "售后状态文字")
    private String serviceStatusText;

    @ApiModelProperty(value = "配送方式文字")
    private String shippingTypeText;

    @ApiModelProperty(value = "支付方式类型文字")
    private String paymentTypeText;

    @ApiModelProperty(value = "支付方式")
    private String paymentName;

    @ApiModelProperty(value = "sku列表")
    private List<OrderSkuVO> orderSkuList;

    @ApiModelProperty(value = "发票信息")
    private ReceiptHistory receiptHistory;

    @ApiModelProperty(value = "订单赠品列表")
    private List<FullDiscountGiftDO> giftList;


    @ApiModelProperty(value = "返现金额")
    private Double cashBack;

    @ApiModelProperty(value = "优惠券抵扣金额")
    private Double couponPrice;

    @ApiModelProperty(value = "商品售价，拼团/秒杀/团购时设置的售价")
    private Double goodsSellingPrice;

    @ApiModelProperty(value = "赠送的积分")
    private Integer giftPoint;

    @ApiModelProperty(value = "赠送的优惠券")
    private CouponVO giftCoupon;


    @ApiModelProperty(value = "此订单使用的积分")
    private Integer usePoint;


    @ApiModelProperty(value = "满减金额")
    private Double fullMinus;

    /**
     * 拼团订单状态
     */
    @ApiModelProperty(value = "拼团订单状态")
    private String pingTuanStatus;

    private LeaderDO leader;

    @ApiModelProperty(value = "自动取消剩余秒数，如果已经超时会为0")
    private Long cancelLeftTime;

    @ApiModelProperty(value = "自动收货剩余时间，如果已经超时会为0")
    private Long rogLeftTime;

    private String orderSeq;

    private Integer printTimes;

    @ApiModelProperty(value = "在线支付金额")
    private Double onlinePayPrice;

    @ApiModelProperty(value = "订单类型")
    private String orderType;

    @ApiModelProperty(value = "团长姓名")
    private String distributionName;

    @ApiModelProperty(value = "团长手机号")
    private String distributionMobile;

    @ApiModelProperty(value = "下单用户昵称")
    private String memberNickName;

    @ApiModelProperty(value = "下单用户的手机号")
    private String memberMobile;


    @ApiModelProperty(name = "expiry_day",value = "虚拟商品的使用截止日期")
    private Integer expiryDay;

    @ApiModelProperty(name = "available_date",value = "虚拟商品的可用日期")
    private String availableDate;

    @ApiModelProperty(name = "verification_code",value = "核销码")
    private String verificationCode;

    @ApiModelProperty(name = "virtual_status", value = "核销码按钮显示状态")
    private Integer virtualStatus;

    @ApiModelProperty(name = "is_virtual_order",value = "是否是服务订单")
    private Integer isVirtualOrder;

    @ApiModelProperty(name = "seller_town", value = "商家-镇")
    private String sellerTown;

    @ApiModelProperty(name = "seller_address", value = "商家地址")
    private String sellerAddress;

    @ApiModelProperty(name = "seller_phone", value = "商家电话")
    private String sellerPhone;

    /**店铺纬度*/
    @ApiModelProperty(name="shop_lat",value="店铺纬度",required=false)
    private Double shopLat;

    /**店铺经度*/
    @ApiModelProperty(name="shop_lng",value="店铺经度",required=false)
    private Double shopLng;

    @ApiModelProperty(name = "expiry_day_timestamp",value = "虚拟订单过期时间戳")
    private Long expiryDayTimestamp;

    @ApiModelProperty(name = "expiry_day_timestamp",value = "虚拟订单过期时间戳")
    private PintuanOrderDetailVo pintuanOrderDetailVo;

    @ApiModelProperty(name = "rider_name",value = "配送员名字")
    private String riderName;

    @ApiModelProperty(name = "rider_mobile",value = "配送员手机号")
    private String riderMobile;

    public ReceiptHistory getReceiptHistory() {
        return receiptHistory;
    }

    public void setReceiptHistory(ReceiptHistory receiptHistory) {
        this.receiptHistory = receiptHistory;
    }

    public OrderOperateAllowable getOrderOperateAllowableVO() {
        return orderOperateAllowableVO;
    }

    public void setOrderOperateAllowableVO(OrderOperateAllowable orderOperateAllowableVO) {
        this.orderOperateAllowableVO = orderOperateAllowableVO;
    }

    public String getPingTuanStatus() {
//        pingTuanStatus = "";
//        //已经付款的拼团订单的状态为待成团
//        if (OrderTypeEnum.pintuan.name().equals(this.getOrderType())) {
//            if (this.getPayStatus().equals(PayStatusEnum.PAY_NO.value())) {
//                if(OrderStatusEnum.CANCELLED.value().equals(this.getOrderStatus())){
//                    pingTuanStatus = "未成团";
//                }else{
//                    pingTuanStatus = "待成团";
//                }
//            } else if (OrderStatusEnum.PAID_OFF.value().equals(this.getOrderStatus())) {
//                pingTuanStatus = "待成团";
//            } else {
//                pingTuanStatus = "已成团";
//            }
//
//        }

        return pingTuanStatus;
    }

    public void setPingTuanStatus(String pingTuanStatus) {
        this.pingTuanStatus = pingTuanStatus;
    }

    public Long getCancelLeftTime() {
        return cancelLeftTime;
    }

    public void setCancelLeftTime(Long cancelLeftTime) {
        this.cancelLeftTime = cancelLeftTime;
    }

    public Long getRogLeftTime() {
        return rogLeftTime;
    }

    public void setRogLeftTime(Long rogLeftTime) {
        this.rogLeftTime = rogLeftTime;
    }

    public String getOrderStatusText() {

        //先从特殊的流程-状态显示 定义中读取，如果为空说明不是特殊的状态，直接显示为 状态对应的提示词
        orderStatusText = OrderSpecialStatus.getStatusText(getOrderType(), getPaymentType(), getOrderStatus());
        if (StringUtil.isEmpty(orderStatusText)) {
            orderStatusText = OrderStatusEnum.valueOf(getOrderStatus()).description();
        }

        return orderStatusText;
    }

    public void setOrderStatusText(String orderStatusText) {
        this.orderStatusText = orderStatusText;
    }

    public String getPayStatusText() {
        if (this.getPayStatus() != null) {
            PayStatusEnum payStatusEnum = PayStatusEnum.valueOf(this.getPayStatus());
            payStatusText = payStatusEnum.description();
        }
        return payStatusText;
    }

    public void setPayStatusText(String payStatusText) {
        this.payStatusText = payStatusText;
    }

    public String getShipStatusText() {
        if (this.getShipStatus() != null) {
            ShipStatusEnum shipStatusEnum = ShipStatusEnum.valueOf(this.getShipStatus());
            shipStatusText = shipStatusEnum.description();
        }
        return shipStatusText;
    }

    public void setShipStatusText(String shipStatusText) {
        this.shipStatusText = shipStatusText;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }


    public List<OrderSkuVO> getOrderSkuList() {
        return orderSkuList;
    }

    public void setOrderSkuList(List<OrderSkuVO> orderSkuList) {
        this.orderSkuList = orderSkuList;
    }

    public String getServiceStatusText() {
        if (this.getServiceStatus() != null) {
            ServiceStatusEnum serviceStatusEnum = ServiceStatusEnum.valueOf(this.getServiceStatus());
            serviceStatusText = serviceStatusEnum.description();
        }
        return serviceStatusText;
    }

    public void setServiceStatusText(String serviceStatusText) {
        this.serviceStatusText = serviceStatusText;
    }

    public String getShippingTypeText() {
        if (this.getShippingType() != null) {
            ShipTypeEnum shipTypeEnum = ShipTypeEnum.valueOf(this.getShippingType());
            shippingTypeText = shipTypeEnum.getDesc();
        }
        return shippingTypeText;
    }

    public void setShippingTypeText(String shippingTypeText) {
        this.shippingTypeText = shippingTypeText;
    }

    public String getPaymentTypeText() {
        if (this.getPaymentType() != null) {
            PaymentTypeEnum paymentTypeEnum = PaymentTypeEnum.valueOf(this.getPaymentType());
            paymentTypeText = paymentTypeEnum.description();
        }
        return paymentTypeText;
    }

    public void setPaymentTypeText(String paymentTypeText) {
        this.paymentTypeText = paymentTypeText;
    }

    public List<FullDiscountGiftDO> getGiftList() {
        return giftList;
    }

    public void setGiftList(List<FullDiscountGiftDO> giftList) {
        this.giftList = giftList;
    }

    public Double getCashBack() {
        return cashBack;
    }

    public void setCashBack(Double cashBack) {
        this.cashBack = cashBack;
    }

    public Double getCouponPrice() {
        if (couponPrice == null) {
            return 0D;
        }
        return couponPrice;
    }

    public void setCouponPrice(Double couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Integer getGiftPoint() {
        return giftPoint;
    }

    public void setGiftPoint(Integer giftPoint) {
        this.giftPoint = giftPoint;
    }

    public Integer getUsePoint() {
        return usePoint;
    }

    public void setUsePoint(Integer usePoint) {
        this.usePoint = usePoint;
    }

    public CouponVO getGiftCoupon() {
        return giftCoupon;
    }

    public void setGiftCoupon(CouponVO giftCoupon) {
        this.giftCoupon = giftCoupon;
    }

    public Double getFullMinus() {
        if (fullMinus == null) {
            return 0D;
        }
        return fullMinus;
    }

    public void setFullMinus(Double fullMinus) {
        this.fullMinus = fullMinus;
    }

    public LeaderDO getLeader() {
        return leader;
    }

    public void setLeader(LeaderDO leader) {
        this.leader = leader;
    }

    public String getOrderSeq() {
        return orderSeq;
    }

    public void setOrderSeq(String orderSeq) {
        this.orderSeq = orderSeq;
    }

    public Double getOnlinePayPrice() {
        return onlinePayPrice;
    }

    public void setOnlinePayPrice(Double onlinePayPrice) {
        this.onlinePayPrice = onlinePayPrice;
    }

    @Override
    public Integer getExpiryDay() {
        return expiryDay;
    }

    @Override
    public void setExpiryDay(Integer expiryDay) {
        //默认不显示核销码按钮，如果在可用时间范围内则显示
        this.virtualStatus = 0;
        if (PayStatusEnum.PAY_YES.value().equals(this.getPayStatus())
                && !OrderStatusEnum.AFTER_SERVICE.value().equals(this.getOrderStatus())) {
            this.virtualStatus = 1;
        }

        this.expiryDay = expiryDay;
    }

    @Override
    public String getAvailableDate() {
        return availableDate;
    }

    @Override
    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    @Override
    public String getVerificationCode() {
        return verificationCode;
    }

    @Override
    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Integer getVirtualStatus() {
        return virtualStatus;
    }

    public void setVirtualStatus(Integer virtualStatus) {
        //默认不显示核销码按钮，如果在可用时间范围内则显示
        // this.virtualStatus = 0;
        // if (this.getPayStatusText().equals(PayStatusEnum.PAY_YES.description())
        //         && !this.getOrderStatusText().equals(OrderStatusEnum.AFTER_SERVICE.description())) {
        //     this.virtualStatus = 1;
        // }
        this.virtualStatus=virtualStatus;
    }

    public Integer getIsVirtualOrder() {
        return isVirtualOrder;
    }

    public void setIsVirtualOrder(Integer isVirtualOrder) {
        this.isVirtualOrder = isVirtualOrder;
    }

    public String getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(String sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public String getSellerPhone() {
        return sellerPhone;
    }

    public void setSellerPhone(String sellerPhone) {
        this.sellerPhone = sellerPhone;
    }

    public Double getShopLat() {
        return shopLat;
    }

    public void setShopLat(Double shopLat) {
        this.shopLat = shopLat;
    }

    public Double getShopLng() {
        return shopLng;
    }

    public void setShopLng(Double shopLng) {
        this.shopLng = shopLng;
    }

    public Long getExpiryDayTimestamp() {
        return expiryDayTimestamp;
    }

    public void setExpiryDayTimestamp(Long expiryDayTimestamp) {
        this.expiryDayTimestamp = expiryDayTimestamp;
    }

    public PintuanOrderDetailVo getPintuanOrderDetailVo() {
        return pintuanOrderDetailVo;
    }

    public void setPintuanOrderDetailVo(PintuanOrderDetailVo pintuanOrderDetailVo) {
        this.pintuanOrderDetailVo = pintuanOrderDetailVo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderDetailVO that = (OrderDetailVO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(orderOperateAllowableVO, that.orderOperateAllowableVO)
                .append(orderStatusText, that.orderStatusText)
                .append(payStatusText, that.payStatusText)
                .append(shipStatusText, that.shipStatusText)
                .append(serviceStatusText, that.serviceStatusText)
                .append(paymentName, that.paymentName)
                .append(orderSkuList, that.orderSkuList)
                .isEquals();
    }


    @Override
    public Integer getPrintTimes() {
        return printTimes;
    }

    @Override
    public void setPrintTimes(Integer printTimes) {
        this.printTimes = printTimes;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(orderOperateAllowableVO)
                .append(orderStatusText)
                .append(payStatusText)
                .append(shipStatusText)
                .append(serviceStatusText)
                .append(paymentName)
                .append(orderSkuList)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "OrderDetailVO{" +
                "orderOperateAllowableVO=" + orderOperateAllowableVO +
                ", orderStatusText='" + orderStatusText + '\'' +
                ", payStatusText='" + payStatusText + '\'' +
                ", shipStatusText='" + shipStatusText + '\'' +
                ", serviceStatusText='" + serviceStatusText + '\'' +
                ", paymentName='" + paymentName + '\'' +
                ", orderSkuList=" + orderSkuList +
                '}';
    }

    @Override
    public String getOrderType() {
        return orderType;
    }

    @Override
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getDistributionName() {
        return distributionName;
    }

    public void setDistributionName(String distributionName) {
        this.distributionName = distributionName;
    }

    public String getDistributionMobile() {
        return distributionMobile;
    }

    public void setDistributionMobile(String distributionMobile) {
        this.distributionMobile = distributionMobile;
    }

    public String getMemberNickName() {
        return memberNickName;
    }

    public void setMemberNickName(String memberNickName) {
        this.memberNickName = memberNickName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public Double getGoodsSellingPrice() {
        return goodsSellingPrice;
    }

    public void setGoodsSellingPrice(Double goodsSellingPrice) {
        this.goodsSellingPrice = goodsSellingPrice;
    }

    public String getRiderName() {
        return riderName;
    }

    public void setRiderName(String riderName) {
        this.riderName = riderName;
    }

    public String getRiderMobile() {
        return riderMobile;
    }

    public void setRiderMobile(String riderMobile) {
        this.riderMobile = riderMobile;
    }

    public String getSellerTown() {
        return sellerTown;
    }

    public void setSellerTown(String sellerTown) {
        this.sellerTown = sellerTown;
    }
}
