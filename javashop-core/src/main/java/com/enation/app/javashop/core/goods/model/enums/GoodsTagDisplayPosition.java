package com.enation.app.javashop.core.goods.model.enums;

import com.enation.app.javashop.framework.util.StringUtil;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2020/11/27
 * @Description:
 */
public enum GoodsTagDisplayPosition {

    SHOP_HOME_PAGE(0,"店铺主页"),
    GOODS_LIST_PAGE(0,"商品列表"),
    GOODS_DETAILS_PAGE(0,"商品详情");

    private int index;
    private String describe;

    GoodsTagDisplayPosition(){

    }

    private static Map<String,Integer> enumMap = new HashMap<>();

    static {
        for (GoodsTagDisplayPosition item : values()) {
            enumMap.put(item.getDescribe(),item.getIndex());
        }
    }

    GoodsTagDisplayPosition(int index,String describe){
        this.index=index;
        this.describe=describe;
    }

    public int getIndex() {
        return index;
    }

    public String getDescribe() {
        return describe;
    }

    public static Integer getIndexByText(String text){
        return StringUtil.isEmpty(text) ? 0 : enumMap.get(text);
    }

}
