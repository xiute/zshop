package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * 店铺分组项
 */
@Data
public class ShopCatItem {
    @ApiModelProperty(name="shop_cat_id",value="店铺分组ID",required=false)
    private Integer shopCatId;
    @ApiModelProperty(name="shop_cat_pid",value="店铺分组父ID",required=false)
    private Integer shopCatPid;
    @ApiModelProperty(name="shop_id",value="店铺id",required=false)
    private Integer shopId;
    @ApiModelProperty(name="shop_cat_name",value="店铺分组名称",required=true)
    private String shopCatName;
    @ApiModelProperty(name="shop_cat_desc",value="店铺分组描述",required=false)
    private String shopCatDesc;
    @ApiModelProperty(name="disable",value="店铺分组显示状态:1显示 0不显示",required=true)
    private Integer disable;
    @ApiModelProperty(name="cat_path",value="分组路径",required=false)
    private String catPath;
    @ApiModelProperty(name="cat_path_name",value="分组路径名称",required=false)
    private String catPathName;

}
