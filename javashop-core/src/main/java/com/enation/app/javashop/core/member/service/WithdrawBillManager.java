package com.enation.app.javashop.core.member.service;

import com.enation.app.javashop.core.member.model.dos.WithdrawBillDO;

import java.util.List;

/**
 * 提现结算单关系
 * @author 孙建
 */
public interface WithdrawBillManager {

    void batchSave(List<WithdrawBillDO> withdrawBillList);

    List<WithdrawBillDO> listByWithdrawRecordId(Integer withdrawRecordId);

}