package com.enation.app.javashop.core.client.system;

import com.enation.app.javashop.core.base.model.vo.EmailVO;

/**
 * @author fk
 * @version v2.0
 * @Description: 邮件
 * @date 2018/8/13 16:25
 * @since v7.0.0
 */
public interface EmailClient {

    /**
     * 邮件发送实现，供消费者调用
     *
     * @param emailVO
     */
    void sendEmail(EmailVO emailVO);
}
