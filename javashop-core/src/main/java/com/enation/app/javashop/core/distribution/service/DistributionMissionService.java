package com.enation.app.javashop.core.distribution.service;

import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionDO;
import com.enation.app.javashop.core.distribution.model.vo.DistributionMissionVO;
import com.enation.app.javashop.core.distribution.model.vo.QueryDistributionMissionVO;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;

import java.util.List;
import java.util.Map;

/**
 * @author 王志杨
 * @since 2021/1/22 17:20
 * 团长任务Service
 */
public interface DistributionMissionService {

    void insertDistributionMission(DistributionMissionDO distributionMissionDO, Seller seller);

    void updateDistributionMission(DistributionMissionDO distributionMissionDO, Seller seller);

    Page<DistributionMissionVO> queryDistributionMissionList(QueryDistributionMissionVO queryDistributionMissionVO);

    DistributionMissionVO queryLatestMission();

    void deleteDistributionMission(Integer missionId, Seller seller);

    DistributionMissionDO getDistributionMissionByMissionId(Integer missionId);

    void onOffLine(Integer missionId, Integer missionStatus);

    Map queryMissionIndex(Integer memberId);

    // 查询店铺中的团长任务，店铺同一时间只能有一个上线的任务
    List<DistributionMissionDO> getDistributionMissionBySellerId(Integer sellerId);

}
