package com.enation.app.javashop.core.shop.service;

import com.enation.app.javashop.core.shop.model.dos.ShopGisDO;
import com.enation.app.javashop.core.shop.model.dto.ShopGisDTO;
import com.enation.app.javashop.core.shop.model.vo.ShopGisVO;

import java.util.List;

/**
 * @Author 王志杨
 * @Date 2020/8/26 14:41
 * @Descroption 店铺服务范围业务接口
 * @Since
 * @Version
 */
public interface ShopGisManager {

    /**
     * 添加店铺服务范围
     * @param shopGisDTO 店铺服务范围DTO
     * @return ShopGisDTO 返回添加成功的店铺服务范围DTO
     */
    void insertShopGis(ShopGisDTO shopGisDTO);

    /**
     * 通过shop_id获取店铺服务范围节点坐标集合
     * @param shopId
     * @return List<ShopGisDTO>
     */
    ShopGisVO getShopGisByShopId(Integer shopId);

    /**
     * 检测指定收货点的坐标是否在shop_id店铺的配送范围
     * @param coord
     * @param shopId
     * @return 200 在范围内，444不在配送范围内
     */
    Integer checkPointWithinMultiPolygon(Double[] coord, Integer shopId);

    /**
     * 判断买家要求配送位置的经纬坐标属于哪些自提点的配送范围内
     * @param coord 买家要求配送位置的经纬坐标
     * @return 属于配送范围的集合
     */
    List<ShopGisVO> checkPointBelongMultiPolygon(Double[] coord);

    /**
     * 根据市级地区ID查询范围内所有自提点的服务范围的集合
     * @param shop_city_id 市级地区ID
     * @return 属所有自提点的服务范围的集合
     */
    List<ShopGisVO> getMultiPolygonByAreaId(Integer shop_city_id);


}
