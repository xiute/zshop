package com.enation.app.javashop.core.goodssearch.service.impl;

import com.enation.app.javashop.core.goods.model.dto.ShipCalculateDTO;
import com.enation.app.javashop.core.goodssearch.service.ShipCalculator;
import com.enation.app.javashop.core.shop.model.dos.TemplateTimeSetting;
import com.enation.app.javashop.core.shop.model.dos.TimeSection;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


/**
 * @author JFENG
 */
@Component
public class ShipTimeCalculator implements ShipCalculator {


    @Override
    public void calculate(ShipCalculateDTO shipCalculateDTO) {
        String shipTimeShow;// 配送时间================================================ 1 立即配送 2 时段配送
        TemplateTimeSetting templateTimeSetting = shipCalculateDTO.getTemplateTimeSetting();
        if (templateTimeSetting.getTimeType() == 1) {
            int shipTime = calculateTimeByDistance(templateTimeSetting, shipCalculateDTO.getDistance());
            shipTimeShow = shipTime + "分钟";
            LocalTime now = LocalTime.now();
            shipCalculateDTO.setLocalStartTime("现在下单");
            shipCalculateDTO.setLocalEndTime(now.plusMinutes(shipTime).format(DateTimeFormatter.ofPattern("HH:mm")));
        } else {
            TimeSection currentTimeSection = calculateTimeBySeciton(templateTimeSetting);
            if (currentTimeSection == null) {
                shipTimeShow="当日无配送";
                shipCalculateDTO.setLocalStartTime("");
                shipCalculateDTO.setLocalEndTime("当日无配送");
            } else {
                String timeType = currentTimeSection.getShipTimeType() == 1 ? "当日" : "次日";
                shipTimeShow = timeType + " " + currentTimeSection.getArriveTime();
                shipCalculateDTO.setLocalStartTime("现在下单");
                shipCalculateDTO.setLocalEndTime(timeType + "-" + currentTimeSection.getArriveTime());
            }
        }
        shipCalculateDTO.setShipTimeView(shipTimeShow);
    }

    /**
     * 根据时段---配送时间
     * @param templateTimeSetting
     * @return
     */
    private TimeSection calculateTimeBySeciton(TemplateTimeSetting templateTimeSetting) {
        LocalDateTime currentTime = LocalDateTime.now();
        List<TimeSection> timeSections = templateTimeSetting.getTimeSections();
        TimeSection currentTimeSection = null;
        for (TimeSection timeSection : timeSections) {
            String[] times = timeSection.getShipTime().split(":");
            Integer hour = Integer.valueOf(times[0]);
            LocalTime localTime = hour==24?LocalTime.MAX:LocalTime.of(hour, Integer.valueOf(times[1]));
            LocalDateTime sectionTime = LocalDateTime.of(LocalDate.now(), localTime);
            if (currentTime.compareTo(sectionTime) < 0) {
                currentTimeSection = timeSection;
                break;
            }
        }
        return currentTimeSection;
    }

    /**
     * 根据距离---配送时间
     * @param templateTimeSetting
     * @param distanceTarget
     * @return
     */
    private int calculateTimeByDistance(TemplateTimeSetting templateTimeSetting, BigDecimal distanceTarget) {
        Integer shipTime = 0;
        if (templateTimeSetting.getBaseDistance().compareTo(distanceTarget) < 0) {
            BigDecimal extraTime = distanceTarget.subtract(templateTimeSetting.getBaseDistance())
                    .divide(templateTimeSetting.getUnitDistance(),3,BigDecimal.ROUND_HALF_DOWN)
                    .multiply(templateTimeSetting.getUnitTime());
            shipTime = templateTimeSetting.getBaseTime().add(extraTime).intValue();
        } else {
            shipTime = templateTimeSetting.getBaseTime().intValue();
        }
        return shipTime;
    }
}
