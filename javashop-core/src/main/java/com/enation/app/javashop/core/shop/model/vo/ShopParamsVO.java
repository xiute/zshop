package com.enation.app.javashop.core.shop.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 店铺搜索条件VO
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 下午8:43:57
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShopParamsVO {
	/** 页码 */
	@ApiModelProperty(name="page_no",value = "页码")
	private Integer pageNo;
	/** 分页数 */
	@ApiModelProperty(name="page_size",value = "分页数")
	private Integer pageSize;
	 /**店铺名称*/
    @ApiModelProperty(name="shop_name",value="店铺名称",required=false)
    private String shopName;	
    /**会员名称*/
    @ApiModelProperty(name="member_name",value="会员名称",required=false)
    private String memberName;
    /**开始时间*/
    @ApiModelProperty(name="start_time",value="店铺开始时间",required=false)
    private String startTime;
    /**结束时间*/
    @ApiModelProperty(name="end_time",value="店铺关闭时间",required=false)
    private String endTime;
    /**关键字*/
    @ApiModelProperty(name="keyword",value="关键字",required=false)
    private String keyword;
    /**店铺状态*/
    @ApiModelProperty(name="shop_disable",value="店铺状态",required=false)
    private String shopDisable;
	/**排序方式*/
	@ApiModelProperty(name="order",value="排序方式",required=false)
    private String order;
	/**店铺纬度*/
	@ApiModelProperty(name="shop_lat",value="店铺纬度",required=false)
	private Double shopLat;
	/**店铺经度*/
	@ApiModelProperty(name="shop_lng",value="店铺经度",required=false)
	private Double shopLng;
	/** 距离范围 */
	@ApiModelProperty(name="scope",value="距离范围",required=false)
	private Integer scope;
	/** 商铺标签 */
	@ApiModelProperty(name="shop_tag",value="商铺标签",required=false)
	private String shopTag;

	/** 本地生活微页面标识*/
	@ApiModelProperty(name="local_micro_page",value="本地生活微页面标识 value = 3 ",required=false)
	private Integer localMicroPage;

	/** 商铺标签 */
	@ApiModelProperty(name="shop_type",value="店铺类型-1:商店 2:团购 3本地生活",required=false)
	private Integer shopType;

}
