package com.enation.app.javashop.core.geo.model;

import lombok.Data;

import java.util.List;

@Data
public class RegeoCode {
    //结构化地址信息
    private String formatted_address;
    //省名
    private AddressComponent addressComponent;
    //城市名
    private List<Pois> pois;
}
