package com.enation.app.javashop.core.member.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;


@ApiModel
@Data
public class MemberWapDTO {
    /**
     * 会员手机号码
     */
    @NotEmpty(message = "手机号不能为空")
    @ApiModelProperty(name = "mobile", value = "会员手机号码")
    private String mobile;
    /**
     * 密码
     */
    @Pattern(regexp = "[a-fA-F0-9]{32}", message = "密码格式不正确")
    @ApiModelProperty(name = "password", value = "密码")
    private String password;

    // private String position;

}