package com.enation.app.javashop.core.statistics.service.impl;

import com.enation.app.javashop.core.aftersale.service.AfterSaleManager;
import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.member.MemberAskClient;
import com.enation.app.javashop.core.client.trade.OrderClient;
import com.enation.app.javashop.core.statistics.model.vo.ShopDashboardVO;
import com.enation.app.javashop.core.statistics.service.DashboardStatisticManager;
import com.enation.app.javashop.core.statistics.service.PageViewStatisticManager;
import com.enation.app.javashop.core.trade.order.model.vo.OrderLineVO;
import com.enation.app.javashop.core.trade.order.model.vo.OrderStatusNumVO;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.framework.context.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 仪表盘业务实现类
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018/6/25 10:41
 */
@Service
public class DashboardStatisticManagerImpl implements DashboardStatisticManager {

    @Autowired
    private MemberAskClient memberAskClient;

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private GoodsClient goodsClient;
    @Autowired
    private OrderQueryManager orderQueryManager;
    @Autowired
    private PageViewStatisticManager pageViewStatisticManager;
    @Autowired
    private AfterSaleManager afterSaleManager;

    /**
     * 获取仪表盘数据
     *
     * @return 商家中心数据
     */
    @Override
    public ShopDashboardVO getShopData() {

        // 获取商家id
        int sellerId = UserContext.getSeller().getSellerId();

        // 返回值
        ShopDashboardVO shopDashboardVO = new ShopDashboardVO();

        Integer num = goodsClient.queryGoodsCountByParam(1, sellerId,1);

        // 出售中的商品数量
        String marketGoods = null == num ? "0" : num.toString();
        shopDashboardVO.setMarketGoods(null == marketGoods ? "0" : marketGoods);

        // 仓库待上架的商品数量
        num = goodsClient.queryGoodsCountByParam(0, sellerId,1);
        String pendingGoods = null == num ? "0" : num.toString();
        shopDashboardVO.setPendingGoods(null == pendingGoods ? "0" : pendingGoods);

        // 待审核的商品数量
        num = goodsClient.queryGoodsCountByParam(0, sellerId,0);
        String authGoods = null == num ? "0" : num.toString();
        shopDashboardVO.setDisabledGoods(null == authGoods ? "0" : authGoods);

        // 待处理买家咨询数量
        String pendingMemberAsk = this.memberAskClient.getNoReplyCount(sellerId).toString();
        shopDashboardVO.setPendingMemberAsk(null == pendingMemberAsk ? "0" : pendingMemberAsk);

        OrderStatusNumVO orderStatusNumVO = this.orderClient.getOrderStatusNum(null, sellerId);
        // 所有订单数量
        shopDashboardVO.setAllOrdersNum(null == orderStatusNumVO.getAllNum() ? "0" : orderStatusNumVO.getAllNum().toString());
        // 待付款订单数量
        shopDashboardVO.setWaitPayOrderNum(null == orderStatusNumVO.getWaitPayNum() ? "0" : orderStatusNumVO.getWaitPayNum().toString());
        // 待发货订单数量
        shopDashboardVO.setWaitShipOrderNum(null == orderStatusNumVO.getWaitShipNum() ? "0" : orderStatusNumVO.getWaitShipNum().toString());
        // 待收货订单数量
        shopDashboardVO.setWaitDeliveryOrderNum(null == orderStatusNumVO.getWaitRogNum() ? "0" : orderStatusNumVO.getWaitRogNum().toString());
        // 待处理申请售后订单数量
        shopDashboardVO.setAfterSaleOrderNum(null == orderStatusNumVO.getRefundNum() ? "0" : orderStatusNumVO.getRefundNum().toString());
        return shopDashboardVO;
    }

    /**
     *封装 查询今日营业额 payMoney
     *  访客数
     */
    @Override
    public Map<String,Object> getAppIndex(){
        Map<String,Object> map = new HashMap<>();
        Double payMoney = orderQueryManager.getPayMoney();
        map.put("payMoney",payMoney);
        Integer visitors = pageViewStatisticManager.getVisitors();
        map.put("visitors",visitors);
        Integer afterSaleCount = afterSaleManager.getAfterSaleCount();
        map.put("afterSaleCount",afterSaleCount);
        return map;
    }

}
