package com.enation.app.javashop.core.base.message;

import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;

import java.io.Serializable;

/**
 * 退货退款消息
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018/6/19 上午10:36
 * @Description:
 *
 */
public class RefundChangeMsg implements Serializable {

	private static final long serialVersionUID = -5608209655474949712L;

	private RefundDO refund;

	/**
	 * 售后状态
 	 */
	private RefundStatusEnum refundStatusEnum;

	public RefundChangeMsg(RefundDO refundDO, RefundStatusEnum refundStatusEnum){
		this.refund=refundDO;
		this.refundStatusEnum=refundStatusEnum;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public RefundDO getRefund() {
		return refund;
	}

	public void setRefund(RefundDO refund) {
		this.refund = refund;
	}

	public RefundStatusEnum getRefundStatusEnum() {
		return refundStatusEnum;
	}

	public void setRefundStatusEnum(RefundStatusEnum refundStatusEnum) {
		this.refundStatusEnum = refundStatusEnum;
	}
}
