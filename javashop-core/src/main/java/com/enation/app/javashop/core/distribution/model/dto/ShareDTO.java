package com.enation.app.javashop.core.distribution.model.dto;

import lombok.Data;

@Data
public class ShareDTO {

    // 主键
    private Integer id;

    // 分享类型 1商品 2店铺
    private Integer shareType;

    // 类型 1小程序
    private Integer channelType;

    // 标题
    private String title;

    // 描述
    private String content;

    // 图片url
    private String imageUrl;

    // 访问路径
    private String path;

}
