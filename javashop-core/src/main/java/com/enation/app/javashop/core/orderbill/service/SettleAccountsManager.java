package com.enation.app.javashop.core.orderbill.service;

import com.enation.app.javashop.core.orderbill.model.dos.OrderSettle;
import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;

import java.math.BigDecimal;

/**
 * 结算管理
 */
public interface SettleAccountsManager {

	void orderSettle(OrderDO orderDO);

	OrderSettle getOrderSettleByOrderId(Integer orderId);

	OrderSettle getOrderSettleByOrderSn(String orderSn);

	void addOrderSettle(OrderSettle orderSettle);

	void updateOrderSettle(OrderSettle orderSettle, Integer id);


	// 计算平台收益
	BigDecimal countPlatformCommissionMoney(ShopVO shop, Double totalOrderMoney);

}