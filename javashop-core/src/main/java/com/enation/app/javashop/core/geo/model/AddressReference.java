package com.enation.app.javashop.core.geo.model;

import lombok.Data;

@Data
public class AddressReference {

    private String street_number;

    private String town;

    private String street;

    private String landmark_l2;

}
