package com.enation.app.javashop.core.promotion.shetuan.service;


import com.enation.app.javashop.core.goods.model.dos.GoodsDO;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.QueryShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.tool.model.dto.PromotionDTO;
import com.enation.app.javashop.core.shop.model.vo.ShopCatItem;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;

import java.util.List;
import java.util.Map;

public interface ShetuanGoodsOperateManager {

    ShetuanGoodsDO composeAddStGoods(GoodsSkuVO skuVO ,ShetuanGoodsDO shetuanGoodsDO, GoodsDO goodsDO, Seller seller);

    void updateStGoods(ShetuanGoodsVO shetuanGoodsVO);

    void operateStGoodsStatus(Integer id, int status);

    boolean batchUpdateStGoods(List<ShetuanGoodsDO> goodsList);

    /**
     * 批量添加团购商品
     * @param shetuanGoodsDOList
     */
    void batchAddShetuanGoods(List<ShetuanGoodsDO> shetuanGoodsDOList);


    void updateGoodsCat(Integer goodsId,  List<ShopCatItem> shopCatList);

    void syncGoodsNumToGoodsQuantity(List<ShetuanGoodsDO> shetuanGoodsDOS);

    void syncGoodsQuantityToGoodsNum(Integer shetuanId);
}
