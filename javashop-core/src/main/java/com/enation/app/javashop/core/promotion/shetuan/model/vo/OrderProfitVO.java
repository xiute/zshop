package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.util.DateUtil;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderProfitVO {


    @Id(name = "id")
    private Integer id;
    /**
     * 收益编号
     */
    @Column(name = "profit_no")
    private String profitNo;
    /**
     * 收益名称
     */
    @Column(name = "profit_name")
    private String profitName;
    /**
     * 会员id
     */
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 会员名称
     */
    @Column(name = "member_name")
    private String memberName;
    /**
     * 会员类型名称
     */
    @Column(name = "member_type_name")
    private String memberTypeName;
    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;
    /**
     * 订单sn
     */
    @Column(name = "order_sn")
    private String orderSn;
    /**
     * 订单类型
     */
    @Column(name = "order_type")
    private String orderType;
    /**
     * 订单类型名称
     */
    @Column(name = "order_type_name")
    private String orderTypeName;
    /**
     * 订单状态
     */
    @Column(name = "order_status")
    private String orderStatus;
    /**
     * 订单状态名称
     */
    @Column(name = "order_status_name")
    private String orderStatusName;
    /**
     * 店铺id
     */
    @Column(name = "seller_id")
    private Integer sellerId;
    /**
     * 店铺名称
     */
    @Column(name = "seller_Name")
    private String sellerName;
    /**
     * 买家id
     */
    @Column(name = "buyer_id")
    private Integer buyerId;
    /**
     * 买家姓名
     */
    @Column(name = "buyer_name")
    private String buyerName;
    /**
     * 付款时间
     */
    @Column(name = "pay_time")
    private Long payTime;
    /**
     * 取消时间
     */
    @Column(name = "cancel_time")
    private Long cancelTime;
    /**
     * 订单总金额
     */
    @Column(name = "order_price")
    private BigDecimal orderPrice;
    /**
     * 佣金类型
     */
    @Column(name = "commission_type")
    private Integer commissionType;
    /**
     * 佣金类型名称
     */
    @Column(name = "commission_type_name")
    private String commissionTypeName;
    /**
     * 佣金比例
     */
    @Column(name = "commission_rate")
    private BigDecimal commissionRate;
    /**
     * 佣金金额
     */
    @Column(name = "commission_money")
    private BigDecimal commissionMoney;
    /**
     * 订单图片
     */
    @Column(name = "order_imgs")
    private String orderImgs;
    /**
     * 推广方式 1直推 2间推
     */
    @Column(name = "spread_way")
    private Integer spreadWay;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 订单备注
     */
    @Column(name = "remark")
    private String remark;

    /**
     * 会员昵称
     */
    @Column(name = "nickname")
    private String nickName;

    /**
     * 收益人姓名
     */
    @Column(name = "beneficiary_name")
    private String beneficiaryName;

    /**
     * 收益人电话
     */
    @Column(name = "beneficiary_mobile")
    private String beneficiaryMobile;

    // 付款金额
    private String showOrderPrice;
    // 预估佣金
    private String showCommissionMoney;
    // 付款时间处理
    private String createTimeString;

    public void show() {
        orderPrice = orderPrice == null ? BigDecimal.ZERO : orderPrice;
        commissionMoney = commissionMoney == null ? BigDecimal.ZERO : commissionMoney;

        if(orderPrice.doubleValue() >= 10000){
            showOrderPrice = "￥" + (BigDecimal.valueOf(orderPrice.doubleValue()).divide(BigDecimal.valueOf(10000)).setScale(2, BigDecimal.ROUND_HALF_DOWN)) + "W";
        }else{
            showOrderPrice = "￥" + orderPrice;
        }

        if(commissionMoney.doubleValue() >= 10000){
            showCommissionMoney = "￥" + (BigDecimal.valueOf(commissionMoney.doubleValue()).divide(BigDecimal.valueOf(10000)).setScale(2, BigDecimal.ROUND_HALF_DOWN)) + "W";
        }else{
            showCommissionMoney = "￥" + commissionMoney;
        }
        // 处理付款时间
        createTimeString = DateUtil.toString(createTime,"yyyy-MM-dd HH:mm:ss");

    }
}
