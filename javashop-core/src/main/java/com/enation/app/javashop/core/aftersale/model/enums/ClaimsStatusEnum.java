package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * @Author: zhou
 * @Date: 2020/9/21
 * @Description: 理赔状态枚举
 */
public enum ClaimsStatusEnum {

    SUBMIT("已提交"),

    CANCELLED("已撤销"),

    AUDIT_SUCCESS("审核通过"),

    AUDIT_FAIL("审核失败"),

    CLAIM_SUCCESS("理赔成功"),

    CLAIM_FAIL("理赔失败");

    private String description;

    ClaimsStatusEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
