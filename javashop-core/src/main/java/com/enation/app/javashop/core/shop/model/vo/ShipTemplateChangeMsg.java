package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.core.shop.model.dos.ShipTemplateDO;
import lombok.Data;

import java.io.Serializable;

/**
 * @author JFENG
 * @version 1.0
 * @Description:
 * @date 2020/2/22 15:38
 */
@Data
public class ShipTemplateChangeMsg  implements Serializable {


    private static final long serialVersionUID = -109352767337144162L;
    /**
     * 原同城配送模板
     */
    private ShipTemplateDO originalTemplate;

    /**
     * 现在配送模板
     */
    private ShipTemplateDO newTemplate;

    public ShipTemplateChangeMsg(ShipTemplateDO originalTemplate, ShipTemplateDO newTemplate) {
        this.originalTemplate = originalTemplate;
        this.newTemplate = newTemplate;
    }

    public ShipTemplateChangeMsg() {
    }
}
