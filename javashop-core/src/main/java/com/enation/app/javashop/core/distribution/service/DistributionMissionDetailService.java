package com.enation.app.javashop.core.distribution.service;

import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionDetailDO;
import com.enation.app.javashop.core.distribution.model.dto.CreateMissionDetailsDTO;

import java.util.List;

/**
 * @author 王志杨
 * @since 2021/1/22 17:20
 * 团长任务明细Service
 */
public interface DistributionMissionDetailService {

    void createMissionDetails(List<DistributionMissionDetailDO> distributionMissionDetailDOList);

    void updateMissionDetails(List<DistributionMissionDetailDO> distributionMissionDetailDOList);

    List<DistributionMissionDetailDO> queryDistributionMissionDetailByMissionId(Integer missionId);

    void deleteDistributionMissionDetailByMissionId(Integer missionId);

    void batchInsertMissionDetails(List<DistributionMissionDetailDO> distributionMissionDetailDOList);

    CreateMissionDetailsDTO getMissionDetailByMisId(Integer missionId);

}
