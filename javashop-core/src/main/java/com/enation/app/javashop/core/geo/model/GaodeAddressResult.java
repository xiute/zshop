package com.enation.app.javashop.core.geo.model;

import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2019/10/22 9:56
 */
@Data
public class GaodeAddressResult {
    private String status;
    private String info;
    private String count;
    private List<GeoCode> geocodes;

}
