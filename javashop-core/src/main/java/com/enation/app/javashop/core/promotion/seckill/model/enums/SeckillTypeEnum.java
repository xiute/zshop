package com.enation.app.javashop.core.promotion.seckill.model.enums;

/**
 * 秒杀活动类型
 */
public enum SeckillTypeEnum {

    /**
     * 店铺秒杀
     */
    SELLLER("店铺秒杀"),

    /**
     * 平台
     */
    PLATFORM("平台秒杀");

    private String description;

    SeckillTypeEnum(String description) {
        this.description = description;

    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
