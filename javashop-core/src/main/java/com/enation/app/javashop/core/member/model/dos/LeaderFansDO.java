package com.enation.app.javashop.core.member.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Table(name = "es_leader_fans")
public class LeaderFansDO {

    @ApiModelProperty(name = "id",value = "id")
    @Id(name = "id")
    private Integer  id;

    @ApiModelProperty(name = "leader_id",value = "团长id")
    @Column(name="leader_id")
    private Integer leaderId;

    @ApiModelProperty(name = "buyer_id",value = "客户id")
    @Column(name = "buyer_id")
    private Integer buyerId;

    @ApiModelProperty(name = "create_time",value="创建时间")
    @Column(name="create_time")
    private long createTime;



}
