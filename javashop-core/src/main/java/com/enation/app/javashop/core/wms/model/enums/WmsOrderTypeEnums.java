package com.enation.app.javashop.core.wms.model.enums;

/**
 * 出库单状态
 * @author JFeng
 * @date 2020/9/22 14:39
 */
public enum WmsOrderTypeEnums {

    ORDER("常规订单"),

    CLAIMS("理赔订单");

    private String description;

    WmsOrderTypeEnums(String description) {
        this.description = description;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
