package com.enation.app.javashop.core.thirdParty.model.dto;

import lombok.Data;

@Data
public class SkuAmt {
    //库存 以商品库存为准
    private Integer amt;
    //skuID
    private String amtId;
    //规格ID
    private String specsIdStr;
}
