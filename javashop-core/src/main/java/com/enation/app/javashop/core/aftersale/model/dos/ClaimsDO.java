package com.enation.app.javashop.core.aftersale.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Date: Created inMon Sep 21 11:46:08 CST 2020
 * @Author: zhou
 * @Description:
 */
@Data
@ApiModel
@Table(name = "es_claims")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ClaimsDO implements Serializable {

    private static final long serialVersionUID = 8172328547919236851L;

    @Id(name = "claim_id")
    private Integer claimId;

    @ApiModelProperty(value = "理赔单号")
    @Column(name = "claim_sn")
    private String claimSn;

    @ApiModelProperty(value = "购买订单的用户ID")
    @Column(name = "member_id")
    private Integer memberId;

    @ApiModelProperty(value = "异常单ID")
    @Column(name = "exception_id")
    private Integer exceptionId;

    @ApiModelProperty(value = "异常单编号")
    @Column(name = "exception_sn")
    private String exceptionSn;

    @ApiModelProperty(value = "订单编号")
    @Column(name = "order_sn")
    private String orderSn;

    @ApiModelProperty(value = "理赔状态")
    @Column(name = "claim_status")
    private String claimStatus;

    @ApiModelProperty(value = "理赔类型")
    @Column(name = "claim_type")
    private String claimType;

    @ApiModelProperty(value = "多退少补类型")
    @Column(name = "refund_reason")
    private String refundReason;

    @ApiModelProperty(value = "理赔商品信息json")
    @Column(name = "claim_skus")
    private String claimSkus;

    @ApiModelProperty(value = "理赔金额")
    @Column(name = "claim_price")
    private Double claimPrice;

    @ApiModelProperty(value = "理赔成本")
    @Column(name = "claim_cost")
    private Double claimCost;

    @ApiModelProperty(value = "理赔说明")
    @Column(name = "claim_describe")
    private String claimDescribe;

    @ApiModelProperty(value = "理赔时间")
    @Column(name = "claim_time")
    private Long claimTime;

    @ApiModelProperty(value = "理赔人")
    @Column(name = "claim_member_id")
    private Integer claimMemberId;

    @ApiModelProperty(value = "申请人")
    @Column(name = "apply_member_id")
    private Integer applyMemberId;

    @ApiModelProperty(value = "申请时间")
    @Column(name = "apply_time")
    private Long applyTime;

    @ApiModelProperty(value = "审核人")
    @Column(name = "audit_member_id")
    private Integer auditMemberId;

    @ApiModelProperty(value = "审核时间")
    @Column(name = "audit_time")
    private Long auditTime;

    @ApiModelProperty(value = "审核备注")
    @Column(name = "audit_describe")
    private String auditDescribe;

    @ApiModelProperty(value = "理赔人姓名")
    @Column(name = "claim_member_name")
    private String claimMemberName;

    @ApiModelProperty(value = "申请人姓名")
    @Column(name = "apply_member_name")
    private String applyMemberName;

    @ApiModelProperty(value = "审核人姓名")
    @Column(name = "audit_member_name")
    private String auditMemberName;

    @ApiModelProperty(value = "出库日期")
    @Column(name = "delivery_time")
    private Long deliveryTime;

}
