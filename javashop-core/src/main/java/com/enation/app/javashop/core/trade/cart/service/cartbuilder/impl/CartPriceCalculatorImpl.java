package com.enation.app.javashop.core.trade.cart.service.cartbuilder.impl;

import com.enation.app.javashop.core.trade.TradeErrorCode;
import com.enation.app.javashop.core.trade.cart.model.enums.CartType;
import com.enation.app.javashop.core.trade.cart.model.enums.PromotionTarget;
import com.enation.app.javashop.core.trade.cart.model.vo.*;
import com.enation.app.javashop.core.trade.cart.service.cartbuilder.CartPriceCalculator;
import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.enation.app.javashop.core.trade.order.service.CheckoutParamManager;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.CurrencyUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import com.google.common.collect.Maps;
import org.apache.bcel.generic.IF_ACMPEQ;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by kingapex on 2018/12/10.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/12/10
 */
@Service(value = "cartPriceCalculator")
public class CartPriceCalculatorImpl implements CartPriceCalculator {

    protected final Log logger = LogFactory.getLog(this.getClass());


    @Override
    public PriceDetailVO countPrice(List<CartVO> cartList) {

        //根据规则计算价格
        PriceDetailVO priceDetailVO = this.countPriceWithRule(cartList);


        return priceDetailVO;
    }


    /**
     * 购物车费用计算
     * 促销规则计算
     * @param cartList
     * @return
     */
    private PriceDetailVO countPriceWithRule(List<CartVO> cartList) {
        // 主单总费用
        PriceDetailVO price = new PriceDetailVO();
        // 商品售价，拼团/秒杀/团购时设置的售价
        Double goodsSellingPrice=0D;
        for (CartVO cart : cartList) {
            // 购物车使用的配送方式(自提作为兜底配送方式)
            String activeShipWay = cart.getActiveShipWay()==null?ShipTypeEnum.SELF.name():cart.getActiveShipWay();

            boolean freeShipping = false;

            //子单费用
            PriceDetailVO cartPrice = new PriceDetailVO();
            cartPrice.setAllowCreateOrder(true);
            cartPrice.setLocalFreightPrice(cart.getPrice().getLocalFreightPrice());
            cartPrice.setGlobalFreightPrice(cart.getPrice().getGlobalFreightPrice());
            cartPrice.setLocalFreightTime(cart.getPrice().getLocalFreightTime());
            //最终计算使用的配送费用
            if (activeShipWay.equals(ShipTypeEnum.GLOBAL.name())) {
                cartPrice.setFreightPrice(cartPrice.getGlobalFreightPrice());
            } else if (activeShipWay.equals(ShipTypeEnum.LOCAL.name())) {
                cartPrice.setFreightPrice(cartPrice.getLocalFreightPrice());
            } else if (activeShipWay.equals(ShipTypeEnum.SELF.name())) {
                cartPrice.setFreightPrice(0.0);
            }

            if(cart.getAllowCreateOrder()!=null && !cart.getAllowCreateOrder()){
                cartPrice.setAllowCreateOrder(cart.getAllowCreateOrder());
            }

            for (CartSkuVO cartSku : cart.getSkuList()) {
                //如果是结算页，则忽略未选中的   update by liuyulei 2019-05-07
                if (CartType.CHECKOUT.equals(cart.getCartType()) && cartSku.getChecked() == 0) {
                    continue;
                }

                PromotionRule skuRule = cartSku.getRule();

                if (skuRule != null) {
                    // 单品促销
                    this.applyGoodsRule(cartSku, skuRule, cart);

                    //有一个商品免运费则全部免运费
                    if (!freeShipping) {
                        freeShipping = skuRule.getFreeShipping();
                    }

                } else {
                    skuRule = new PromotionRule(PromotionTarget.SKU);
                }
                //未选中的不计入合计中   update by liuyulei 2019-05-07
                if (cartSku.getChecked() == 0) {
                    continue;
                }
                //购物车全部商品的原价合
                cartPrice.setOriginalPrice(CurrencyUtil.add(cartPrice.getOriginalPrice(), CurrencyUtil.mul(cartSku.getOriginalPrice(), cartSku.getNum())));

                //购物车所有小计合
                cartPrice.setGoodsPrice(CurrencyUtil.add(cartPrice.getGoodsPrice(), cartSku.getSubtotal()));

                //购物车返现合
                cartPrice.setCashBack(CurrencyUtil.add(cartPrice.getCashBack(), skuRule.getReducedTotalPrice()));

                //购物车使用积分
                cartPrice.setExchangePoint(cartPrice.getExchangePoint() + skuRule.getUsePoint());

                goodsSellingPrice=goodsSellingPrice+cartSku.getSubtotal();

                //累计商品重量
                double weight = CurrencyUtil.mul(cartSku.getGoodsWeight(), cartSku.getNum());
                double cartWeight = CurrencyUtil.add(cart.getWeight(), weight);
                cart.setWeight(cartWeight);
            }


            //应用购物车级别的促销规则
            List<PromotionRule> cartRuleList = cart.getRuleList();

            boolean cartFreeShipping = false;
            for (PromotionRule rule : cartRuleList) {
                //应用购物车促销规则
                this.applyCartRule(rule, cartPrice, cart);
                if (rule != null) {
                    cartFreeShipping = rule.getFreeShipping();
                }
            }

            //单品规则中有免运费或满减里有免运费
            if (freeShipping || cartFreeShipping) {
                cartPrice.setIsFreeFreight(1);
                cartPrice.setFreightPrice(0D);
            }

            //计算店铺商品总优惠金额
            double totalDiscount = CurrencyUtil.add(cartPrice.getCashBack(), cartPrice.getCouponPrice());
            cartPrice.setDiscountPrice(totalDiscount);

            //总价为商品价加运费
            double totalPrice = CurrencyUtil.add(cartPrice.getGoodsPrice(), cartPrice.getFreightPrice());
            cartPrice.setTotalPrice(totalPrice);
            cart.setPrice(cartPrice);

            price = price.plus(cartPrice);
            price.setActiveShipWay(activeShipWay);
        }

        price.setGoodsSellingPrice(goodsSellingPrice);

        // 小计=商品金额+运费-满减
        if(price.getFreightPrice()!=null){
            goodsSellingPrice=CurrencyUtil.add(goodsSellingPrice,price.getFreightPrice());
        }
        if(price.getLocalFreightPrice()!=null){
            goodsSellingPrice=CurrencyUtil.add(goodsSellingPrice,price.getLocalFreightPrice());
        }
        if(price.getGlobalFreightPrice()!=null){
            goodsSellingPrice=CurrencyUtil.add(goodsSellingPrice,price.getGlobalFreightPrice());
        }
        goodsSellingPrice=CurrencyUtil.sub(goodsSellingPrice,price.getFullMinus());
        price.setSubtotalPrice(goodsSellingPrice);

        if (logger.isDebugEnabled()) {
            logger.debug("计算完优惠后购物车数据为：");
            logger.debug(cartList);

            logger.debug("价格为：");
            logger.debug(price);
        }

        return price;
    }

    /**
     * 为购物车应用一个购物车促销规则
     *
     * @param cartRule  购物车规则
     * @param cartPrice 购物车价格
     * @param cart      购物车
     */
    private void applyCartRule(PromotionRule cartRule, PriceDetailVO cartPrice, CartVO cart) {

        if (cartRule == null) {
            return;
        }

        //减掉要优惠的总金额
        if (cartRule.getReducedTotalPrice() != null) {

            double subtotal = cartPrice.getGoodsPrice();
            //减
            subtotal = CurrencyUtil.sub(subtotal, cartRule.getReducedTotalPrice());

            //总之不能为负数
            if (subtotal < 0) {
                subtotal = 0;
            }
            //设置
            cartPrice.setFullMinus(cartRule.getReducedTotalPrice());
            cartPrice.setGoodsPrice(subtotal);
            //购物车返现合
            cartPrice.setCashBack(CurrencyUtil.add(cartPrice.getCashBack(), cartRule.getReducedTotalPrice()));
        }

        //优惠券不计入,合计的时候会算
        if (cartRule.getUseCoupon() != null) {

            double subtotal = cartPrice.getGoodsPrice();
            //减
            subtotal = CurrencyUtil.sub(subtotal, cartRule.getUseCoupon().getAmount());

            //总之不能为负数
            if (subtotal < 0) {
                subtotal = 0;
            }
            cartPrice.setGoodsPrice(subtotal);
            cartPrice.setCouponPrice(cartRule.getUseCoupon().getAmount());

        }
        //赠送的优惠券
        if (cartRule.getCouponGiftList() != null) {
            cart.getGiftCouponList().addAll(cartRule.getCouponGiftList());
        }

        //赠送的赠品
        if (cartRule.getGoodsGift() != null) {
            cart.getGiftList().add(cartRule.getGoodsGift());
        }

        //赠送的积分
        if (cartRule.getPointGift() != null) {
            cart.setGiftPoint(cartRule.getPointGift());
        }


    }


    /**
     * 为购物车应用一个单品促销规则
     *
     * @param cartSku 购物车商品sku
     * @param skuRule 促销规则
     * @param cart    购物车
     */
    private void applyGoodsRule(CartSkuVO cartSku, PromotionRule skuRule, CartVO cart) {

        if (!StringUtil.isEmpty(skuRule.getInvalidReason())) {
            cartSku.setChecked(0);
            cartSku.setErrorMessage(skuRule.getInvalidReason());
        }

        //如果已经失效，标记为不选中和失效原因，且不参与计算价格
        if (skuRule.isInvalid()) {
            cartSku.setChecked(0);
            cartSku.setInvalid(1);
            return;
        }

        //设置促销tag
        if (!StringUtil.isEmpty(skuRule.getTag())) {
            cartSku.getPromotionTags().add(skuRule.getTag());
        }

        //减掉要优惠的总金额
        if (skuRule.getReducedTotalPrice() != null) {
            double subtotal = cartSku.getSubtotal();
            subtotal = CurrencyUtil.sub(subtotal, skuRule.getReducedTotalPrice());

            //总之不能为负数
            if (subtotal < 0) {
                subtotal = 0;
            }

            cartSku.setSubtotal(subtotal);
        }


        //单价
        if (skuRule.getReducedPrice() != null) {
            double originalPrice = cartSku.getOriginalPrice();
            double purchasePrice = CurrencyUtil.sub(originalPrice, skuRule.getReducedPrice());

            //总之不能为负数
            if (purchasePrice < 0) {
                purchasePrice = 0;
            }

            cartSku.setPurchasePrice(purchasePrice);
        }


        if (skuRule.getUsePoint() != null) {
            //要使用的积分
            cartSku.setPoint(skuRule.getUsePoint());
        }


        //赠送的优惠券
        if (skuRule.getCouponGiftList() != null) {
            cart.getGiftCouponList().addAll(skuRule.getCouponGiftList());
        }

        //赠送的赠品
        if (skuRule.getGoodsGift() != null) {
            cart.getGiftList().add(skuRule.getGoodsGift());
        }

        //赠送的积分
        if (skuRule.getPointGift() != null) {
            cart.setGiftPoint(skuRule.getPointGift());
        }


    }


}
