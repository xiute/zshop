package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: zhou
 * @Date: 2021/1/12
 * @Description: 导出提现单
 */
@Data
public class ExcelWithdrawBill {
    /**
     * 提现申请号
     */
    @ExcelProperty(value = "流水号", index = 0)
    private String withdrawNo;
    /**
     * 申请时间
     */
    @ExcelProperty(value = "提现时间", index = 1)
    private String applyTimeString;
    /**
     * 申请提现金额
     */
    @ExcelProperty(value = "提现金额", index = 2)
    private BigDecimal amount;

    @ExcelProperty(value = "用户类型", index = 3)
    private String memberType;
    /**
     * 会员名称
     */
    @ExcelProperty(value = "用户名", index = 4)
    private String memberName;
    /**
     * 提现渠道
     */
    @ExcelProperty(value = "收款方式", index = 5)
    private String withdrawChannelName;
    /**
     * 银行名称
     */
    @ExcelProperty(value = "开户银行", index = 6)
    private String bankName;


    @ExcelProperty(value = "收款账户", index = 7)
    private String accountName;
    @ExcelProperty(value = "收款账号", index = 8)
    private String accountNo;
    /**
     * 提现状态
     */
    @ExcelProperty(value = "提现状态", index = 9)
    private String statusText;

}
