package com.enation.app.javashop.core.goods.model.enums;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2020/11/28
 * @Description:
 */
public enum GoodsTagPageStatus {
    SHOW(0, "显示"),
    HIDE(1, "不显示");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (GoodsTagPageStatus item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    GoodsTagPageStatus(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return index == null ? StringUtils.EMPTY : enumMap.get(index);
    }
}
