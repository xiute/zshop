package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class GoodsNumVO {
    @ApiModelProperty(name = "id",value = "id")
    @NotEmpty(message = "id不能为空")
    private Integer id;
    @ApiModelProperty(name = "num_name",value = "修改的数量字段")
    @NotEmpty(message = "字段名不能为空")
    private String numName;
    @ApiModelProperty(name = "num",value = "数量")
    @Min(0)
    private Integer num;
}
