package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.core.goods.model.dos.TagsDO;
import com.enation.app.javashop.core.shop.model.dto.NearbyShopDTO;
import com.enation.app.javashop.framework.util.StringUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: zhou
 * @Date: 2021/3/10
 * @Description: 附近店铺列表返回实体类
 */
@Data
public class NearbyShopVO {
    @ApiModelProperty(value = "店铺名")
    private String sellerName;

    @ApiModelProperty(value = "店铺ID")
    private Integer sellerId;

    @ApiModelProperty(value = "店铺地址")
    private String sellerAddress;

    @ApiModelProperty(value = "店铺logo")
    private String shopLogo;

    @ApiModelProperty(value = "起送价格")
    private Double baseShipPrice;

    @ApiModelProperty(value = "最低配送价格")
    private Double minShipPrice;

    @ApiModelProperty(value = "店铺服务态度分数")
    private Double shopServiceCredit;

    @ApiModelProperty(value = "当前定位与店铺的距离")
    private Double shopDistance;

    @ApiModelProperty(value = "用户是否收藏了店铺")
    private Boolean isCollection;

    @ApiModelProperty(value = "店铺到当前定位的配送时间")
    private Integer shipTime;

    @ApiModelProperty(value = "店铺商品评价")
    private String content;

    @ApiModelProperty(value = "商品二级分类名")
    private List<String> categoryNames;

    @ApiModelProperty(value = "商品头图列表")
    private List<String> thumbnails;

    @ApiModelProperty(value = "活动标签")
    private List<TagsDO> promotionTags;

    @ApiModelProperty(value = "商品标签")
    private List<TagsDO> goodsTags;

    @ApiModelProperty(value = "商品列表")
    private List<NearbyShopDTO> goodsList;

    public NearbyShopVO() {

    }

    public NearbyShopVO(List<NearbyShopDTO> nearbyShopDTOList) {
        NearbyShopDTO nearbyShopDTO = nearbyShopDTOList.get(0);
        this.sellerId = nearbyShopDTO.getSellerId();
        this.sellerName = nearbyShopDTO.getSellerName();
        String url = "https://egale-shop.oss-cn-hangzhou.aliyuncs.com/miniapp/menkoutao.jpg";
        this.shopLogo = StringUtil.isEmpty(nearbyShopDTO.getShopLogo()) ? url : nearbyShopDTO.getShopLogo();
        this.sellerAddress = nearbyShopDTO.getShopAddress();
        this.baseShipPrice = nearbyShopDTO.getBaseShipPrice() == null ? 0 : nearbyShopDTO.getBaseShipPrice();
        this.minShipPrice = nearbyShopDTO.getMinShipPrice() == null ? 0 : nearbyShopDTO.getMinShipPrice();
        this.shopServiceCredit = nearbyShopDTO.getShopServiceCredit();

        this.categoryNames = nearbyShopDTOList.stream().map(NearbyShopDTO::getTwoCategoryName).distinct().collect(Collectors.toList());
        this.thumbnails = nearbyShopDTOList.stream().map(NearbyShopDTO::getThumbnail).distinct().collect(Collectors.toList());
        this.goodsList = nearbyShopDTOList;

        this.promotionTags = new ArrayList<>();
        List<String> promotionNames = nearbyShopDTOList.stream().map(NearbyShopDTO::getTitle).distinct().collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(promotionNames)) {
            for (String name : promotionNames) {
                TagsDO tagsDO = new TagsDO(name);
                this.promotionTags.add(tagsDO);
            }
        }
    }

}
