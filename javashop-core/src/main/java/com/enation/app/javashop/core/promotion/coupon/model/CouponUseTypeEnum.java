package com.enation.app.javashop.core.promotion.coupon.model;

/**
 * @author JFeng
 * @date 2020/10/30 15:09
 */
public enum CouponUseTypeEnum {

    FIX,

    PERIOD;
}
