package com.enation.app.javashop.core.goods.service.impl;

import com.enation.app.javashop.core.base.CachePrefix;
import com.enation.app.javashop.core.goods.model.dto.GoodsQuantityRefreshDTO;
import com.enation.app.javashop.core.goods.model.enums.QuantityType;
import com.enation.app.javashop.core.goods.model.vo.GoodsQuantityVO;
import com.enation.app.javashop.core.goods.service.GoodsQuantityManager;
import com.enation.app.javashop.core.goods.service.impl.util.StockCacheKeyUtil;
import com.enation.app.javashop.core.goods.service.impl.util.UpdatePool;
import com.enation.app.javashop.framework.JavashopConfig;
import com.enation.app.javashop.framework.database.DaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scripting.ScriptSource;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 商品库存接口
 *
 * @author fk
 * @author kingapex
 * @version v1.0 2017年4月1日 下午12:03:08
 * @version v2.0 written by kingapex  2019年2月27日
 * 采用lua脚本执行redis中的库存扣减<br/>
 * 数据库的更新采用非时时同步<br/>
 * 而是建立了一个缓冲池，当达到一定条件时再同步数据库<br/>
 * 这样条件有：缓冲区大小，缓冲次数，缓冲时间<br/>
 * 上述条件在配置中心可以配置，如果没有配置采用 ${@link UpdatePool} 默认值<br/>
 * 在配置项说明：<br/>
 * <li>缓冲区大小：javashop.pool.stock.max-pool-size</li>
 * <li>缓冲次数：javashop.pool.stock.max-update-time</li>
 * <li>缓冲时间（秒数）：javashop.pool.stock.max-lazy-second</li>
 *
 * @see JavashopConfig
 */
@Service
public class GoodsQuantityManagerImpl implements GoodsQuantityManager {


    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DaoSupport daoSupport;

    @Autowired
    private JavashopConfig javashopConfig;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * sku库存更新缓冲池
     */
    private static UpdatePool skuUpdatePool ;
    /**
     * goods库存更新缓冲池
     */
    private static UpdatePool goodsUpdatePool ;


    /**
     * 单例获取sku pool ，初始化时设置参数
     * @return
     */
    private UpdatePool getSkuPool() {
        if (skuUpdatePool == null) {
            skuUpdatePool = new UpdatePool(javashopConfig.getMaxUpdateTime(),javashopConfig.getMaxPoolSize(),javashopConfig.getMaxLazySecond());

            if (logger.isDebugEnabled()) {
                logger.debug("初始化sku pool:");
                logger.debug(skuUpdatePool.toString());

            }
        }

        return skuUpdatePool;
    }



    /**
     * 单例获取goods pool ，初始化时设置参数
     * @return
     */
    private UpdatePool getGoodsPool() {
        if (goodsUpdatePool == null) {
            goodsUpdatePool = new UpdatePool(javashopConfig.getMaxUpdateTime(),javashopConfig.getMaxPoolSize(),javashopConfig.getMaxLazySecond());


        }

        return goodsUpdatePool;
    }

    @Autowired
    public StringRedisTemplate stringRedisTemplate;

    private static RedisScript<Boolean> script = null;

    private static RedisScript<Boolean> getRedisScript() {

        if (script != null) {
            return script;
        }

        ScriptSource scriptSource = new ResourceScriptSource(new ClassPathResource("sku_quantity.lua"));
        String str = null;
        try {
            str = scriptSource.getScriptAsString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        script = RedisScript.of(str, Boolean.class);
        return script;
    }

    @Override
    public Boolean updateSkuQuantity(List<GoodsQuantityVO> goodsQuantityList) {

        List<Integer> skuIdList = new ArrayList();
        List<Integer> goodsIdList = new ArrayList();

        List keys = new ArrayList<>();
        List values = new ArrayList<>();

        for (GoodsQuantityVO quantity : goodsQuantityList) {

            Assert.notNull(quantity.getGoodsId(), "goods id must not be null");
            Assert.notNull(quantity.getSkuId(), "sku id must not be null");
            Assert.notNull(quantity.getQuantity(), "quantity id must not be null");
            Assert.notNull(quantity.getQuantityType(), "Type must not be null");


            //sku库存
            if (QuantityType.enable.equals(quantity.getQuantityType())){
                keys.add(StockCacheKeyUtil.skuEnableKey(quantity.getSkuId()) );
            }else if (QuantityType.actual.equals(quantity.getQuantityType())){
                keys.add(StockCacheKeyUtil.skuActualKey(quantity.getSkuId()) );
            }
            values.add("" + quantity.getQuantity());

            //goods库存key
            if (QuantityType.enable.equals(quantity.getQuantityType())){
                keys.add(StockCacheKeyUtil.goodsEnableKey(quantity.getGoodsId()) );
            }else if (QuantityType.actual.equals(quantity.getQuantityType())){
                keys.add(StockCacheKeyUtil.goodsActualKey(quantity.getGoodsId()) );
            }
            values.add("" + quantity.getQuantity());


            skuIdList.add(quantity.getSkuId());
            goodsIdList.add(quantity.getGoodsId());
        }

        RedisScript<Boolean> redisScript = getRedisScript();
        Boolean result = stringRedisTemplate.execute(redisScript, keys, values.toArray());

        if (logger.isInfoEnabled()) {
            logger.info("更新库存：");
            logger.info(goodsQuantityList.toString());
            logger.info("更新结果：" + result);
        }

        //如果lua脚本执行成功则记录缓冲区
        if (result) {

            //判断配置文件中设置的商品库存缓冲池是否开启
            if (javashopConfig.isStock()) {

                //是否需要同步数据库
                boolean needSync = getSkuPool().oneTime(skuIdList);
                getGoodsPool().oneTime(goodsIdList);

                if (logger.isDebugEnabled()) {
                    logger.debug("是否需要同步数据库:" + needSync);
                    logger.debug(getSkuPool().toString());
                }

                //如果开启了缓冲池，并且缓冲区已经饱和，则同步数据库
                if (needSync) {
                    syncDataBase();
                }
            } else {
                //如果未开启缓冲池，则实时同步商品数据库中的库存数据
                syncDataBase(skuIdList, goodsIdList);
            }

        }


        return result;
    }

    @Override
    public void syncDataBase() {

        //获取同步的skuid 和goodsid
        List<Integer>  skuIdList = getSkuPool().getTargetList();
        List<Integer>  goodsIdList = getGoodsPool().getTargetList();

        if (logger.isInfoEnabled()) {
            logger.info("goodsIdList is:");
            logger.info(goodsIdList.toString());
        }

        //判断要同步的goods和sku集合是否有值
        if (skuIdList.size() != 0 && goodsIdList.size() != 0) {
            //同步数据库
            syncDataBase(skuIdList, goodsIdList);
        }

        //重置缓冲池
        getSkuPool().reset();
        getGoodsPool().reset();
    }

    @Override
    public Map<String,Integer>  fillCacheFromDB(int skuId) {
        Map<String,Integer>  map= daoSupport.queryForMap("select enable_quantity,quantity from es_goods_sku where sku_id=?", skuId);
        Integer enableNum = map.get("enable_quantity");
        Integer actualNum = map.get("quantity");

        stringRedisTemplate.opsForValue().set(StockCacheKeyUtil.skuActualKey(skuId), ""+actualNum);
        stringRedisTemplate.opsForValue().set(StockCacheKeyUtil.skuEnableKey(skuId), ""+enableNum);
        return  map;
     }

    @Override
    public void batchUpdateQuantity(List<GoodsQuantityRefreshDTO> skuQuantityList) {
        List<Object[]> skuBatchArgs = new ArrayList<>();
        List<Object[]> goodsBatchArgs = new ArrayList<>();

        List<String> keys=new ArrayList<>();
        for (GoodsQuantityRefreshDTO goodsQuantityRefreshDTO : skuQuantityList) {
            logger.info("【更新库存】"+goodsQuantityRefreshDTO.toString());
            keys.add(StockCacheKeyUtil.skuEnableKey(goodsQuantityRefreshDTO.getSkuId()));
            keys.add(StockCacheKeyUtil.skuActualKey(goodsQuantityRefreshDTO.getSkuId()));
            //将商品的可用库存和实际库存一起读
            keys.add(StockCacheKeyUtil.goodsEnableKey(goodsQuantityRefreshDTO.getGoodsId()));
            keys.add(StockCacheKeyUtil.goodsActualKey(goodsQuantityRefreshDTO.getGoodsId()));

            keys.add(CachePrefix.GOODS.getPrefix() + goodsQuantityRefreshDTO.getGoodsId());
            // 更新数据
            skuBatchArgs.add(new Object[]{
                    goodsQuantityRefreshDTO.getQuantity(),
                    goodsQuantityRefreshDTO.getEnableQuantity(),
                    goodsQuantityRefreshDTO.getSkuId()});
            goodsBatchArgs.add(new Object[]{
                    goodsQuantityRefreshDTO.getQuantity(),
                    goodsQuantityRefreshDTO.getEnableQuantity(),
                    goodsQuantityRefreshDTO.getGoodsId()});
        }
        // 更新es_goods_sku quantity  enable_quantity
        this.jdbcTemplate.batchUpdate("UPDATE es_goods_sku SET quantity=? , enable_quantity=?  WHERE sku_id = ? ", skuBatchArgs);
        this.jdbcTemplate.batchUpdate("UPDATE es_goods SET quantity=? , enable_quantity=?  WHERE goods_id = ? ", goodsBatchArgs);
        // 删除cache_goods
        this.redisTemplate.delete(keys);

    }


    /**
     * 同步数据库中的库存
     *
     * @param skuIdList   需要同步的skuid
     * @param goodsIdList 需要同步的goodsid
     */
    private void syncDataBase(List<Integer> skuIdList, List<Integer> goodsIdList) {

        //要形成的指更新sql
        List<String> sqlList = new ArrayList<String>();


        //批量获取sku的库存
        List skuKeys = StockCacheKeyUtil.skuKeys(skuIdList);
        List<String> skuQuantityList = stringRedisTemplate.opsForValue().multiGet(skuKeys);


        int i = 0;

        //形成批量更新sku的list
        for (Integer skuId : skuIdList) {
            String sql = "update es_goods_sku set enable_quantity=" + skuQuantityList.get(i) + ", quantity=" + skuQuantityList.get(i + 1) + " where sku_id=" + skuId;
            sqlList.add(sql);
            i=i+2;
        }

        //批量获取商品的库存
        List goodsKeys = createGoodsKeys(goodsIdList);
        List<String> goodsQuantityList =  stringRedisTemplate.opsForValue().multiGet(goodsKeys);

        i = 0;

        //形成批量更新goods的list
        for (Integer goodsId : goodsIdList) {
            String sql = "update es_goods set enable_quantity=" + goodsQuantityList.get(i) + ", quantity=" + goodsQuantityList.get(i + 1) + " where goods_id=" + goodsId;
            sqlList.add(sql);
            i=i+2;
        }

        logger.info("批量更新的sql为："+sqlList.toString());

        daoSupport.batchUpdate(sqlList.toArray(new String[]{}));

    }




    /**
     * 生成批量获取goods库存的keys
     * @param goodsIdList
     * @return
     */
    private List createGoodsKeys(List<Integer> goodsIdList) {
        List keys = new ArrayList();
        for (Integer goodsId : goodsIdList) {
            keys.add( StockCacheKeyUtil.goodsEnableKey(goodsId) );
            keys.add(StockCacheKeyUtil.goodsActualKey(goodsId)  );
        }
        return keys;
    }




}
