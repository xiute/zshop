package com.enation.app.javashop.core.wms.model.enums;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 出库单状态
 * @author JFeng
 * @date 2020/9/22 14:39
 */
public enum WmsOrderStatusEnums {

    CREATED("待出库"),

    READY_OUT("出库中"),

    DELAY_OUT("延迟出库"),

    OUT_SUCC("出库成功"),

    OUT_FAIL("出库失败"),

    CANCELLED("取消出库");



    private static Map<String, String> enumMap = new HashMap<>();

    static {
        for (WmsOrderStatusEnums item : values()) {
            enumMap.put(item.value(), item.getDescription());
        }
    }

    private String description;

    WmsOrderStatusEnums(String description) {
        this.description = description;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }

    public static String getTextByIndex(String value){
        return value == null ? StringUtils.EMPTY : enumMap.get(value);
    }
}
