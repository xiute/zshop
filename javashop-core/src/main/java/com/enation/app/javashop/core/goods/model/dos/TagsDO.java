package com.enation.app.javashop.core.goods.model.dos;

import java.io.Serializable;
import java.util.Date;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 商品标签实体
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-28 14:49:36
 */
@Table(name = "es_tags")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TagsDO implements Serializable {

    private static final long serialVersionUID = 1899720595535600L;

    /**
     * 主键
     */
    @Id(name = "tag_id")
    @ApiModelProperty(hidden = true)
    private Integer tagId;
    /**
     * 标签名字
     */
    @Column(name = "tag_name")
    @ApiModelProperty(name = "tag_name", value = "标签名字", required = false)
    private String tagName;
    /**
     * 所属卖家
     */
    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "所属卖家", required = false)
    private Integer sellerId;
    /**
     * 关键字
     */
    @Column(name = "mark")
    @ApiModelProperty(name = "mark", value = "关键字", required = false)
    private String mark;

    @Column(name = "shop_home_page")
    @ApiModelProperty(name = "shop_home_page", value = "店铺主页显示标签（0显示，1不显示）")
    private Integer shopHomePage;
    @Column(name = "goods_list_page")
    @ApiModelProperty(name = "goods_list_page", value = "商品列表页显示标签（0显示，1不显示）")
    private Integer goodsListPage;
    @Column(name = "goods_details_page")
    @ApiModelProperty(name = "goods_details_page", value = "商品详情页显示标签（0显示，1不显示）")
    private Integer goodsDetailsPage;

    @Column(name = "permanent_enable")
    @ApiModelProperty(name = "permanent_enable", value = "是否永久有效（0是，1否）")
    private Integer permanentEnable;
    @Column(name = "all_goods")
    @ApiModelProperty(name = "all_goods", value = "是否给全部商品添加标签（0是，1否）")
    private Integer allGoods;
    @Column(name = "priority")
    @ApiModelProperty(name = "priority", value = "标签的优先级（数字越大排序越靠前）")
    private Integer priority;
    @Column(name = "type")
    @ApiModelProperty(name = "type", value = "标签类型，0系统默认，1自定义")
    private Integer type;
    @Column(name = "status")
    @ApiModelProperty(name = "status", value = "标签状态")
    private Integer status;
    @Column(name = "start_time")
    @ApiModelProperty(name = "start_time", value = "标签有效开始时间", required = false)
    private Long startTime;
    @Column(name = "end_time")
    @ApiModelProperty(name = "end_time", value = "标签有效截止时间", required = false)
    private Long endTime;
    @Column(name = "color")
    @ApiModelProperty(name = "color", value = "颜色")
    private String color;
    @Column(name = "style")
    @ApiModelProperty(name = "style", value = "样式")
    private String style;
    @Column(name = "tag_describe")
    @ApiModelProperty(name = "tag_describe", value = "标签描述")
    private String tagDescribe;

    @Column(name = "create_time")
    @ApiModelProperty(name = "create_time", value = "创建时间", required = false)
    private Long createTime;
    @Column(name = "update_time")
    @ApiModelProperty(name = "update_time", value = "更新时间")
    private Long updateTime;

    public TagsDO() {
    }

    public TagsDO(String tagName, Integer sellerId, String mark) {
        super();
        this.tagName = tagName;
        this.sellerId = sellerId;
        this.mark = mark;
    }

    public TagsDO(String tagName) {
        this.tagName = tagName;
        this.color = "#F43E14";
        this.style = "2";
    }

    public TagsDO(String tagName, Integer sellerId, String mark, int status, int shopHomePageStauts, int goodsListPageStauts, int goodsDetailsPageStauts, String color, String style, int priority) {
        super();
        this.tagName = tagName;
        this.sellerId = sellerId;
        this.mark = mark;
        this.tagName = tagName;
        this.status = status;
        this.shopHomePage = shopHomePageStauts;
        this.goodsListPage = goodsListPageStauts;
        this.goodsDetailsPage = goodsDetailsPageStauts;
        this.color = color;
        this.style = style;
        this.priority = priority;
    }


    @PrimaryKeyField
    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public Integer getShopHomePage() {
        return shopHomePage;
    }

    public void setShopHomePage(Integer shopHomePage) {
        this.shopHomePage = shopHomePage;
    }

    public Integer getGoodsListPage() {
        return goodsListPage;
    }

    public void setGoodsListPage(Integer goodsListPage) {
        this.goodsListPage = goodsListPage;
    }

    public Integer getGoodsDetailsPage() {
        return goodsDetailsPage;
    }

    public void setGoodsDetailsPage(Integer goodsDetailsPage) {
        this.goodsDetailsPage = goodsDetailsPage;
    }

    public Integer getPermanentEnable() {
        return permanentEnable;
    }

    public void setPermanentEnable(Integer permanentEnable) {
        this.permanentEnable = permanentEnable;
    }

    public Integer getAllGoods() {
        return allGoods;
    }

    public void setAllGoods(Integer allGoods) {
        this.allGoods = allGoods;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getTagDescribe() {
        return tagDescribe;
    }

    public void setTagDescribe(String tagDescribe) {
        this.tagDescribe = tagDescribe;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TagsDO{" +
                "tagId=" + tagId +
                ", tagName='" + tagName + '\'' +
                ", sellerId=" + sellerId +
                ", mark='" + mark + '\'' +
                '}';
    }
}