package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ShetuanAddVO {

    /**
     * id
     */
    @ApiModelProperty(hidden = true)
    private Integer shetuanId;

    /**
     * 活动名称
     */
    @NotEmpty(message = "社区团购名称不能为空")
    @ApiModelProperty(name = "shetuan_name", value = "社区团购名称", required = true)
    private String shetuanName;

    /**
     * 开始时间
     */
    @NotNull(message = "开始时间")
    @ApiModelProperty(name = "start_time", value = "活动开始时间", required = true)
    private Long startTime;

    /**
     * 结束时间
     */
    @NotNull(message = "结束时间不能为空")
    @ApiModelProperty(name = "end_time", value = "活动开始时间", required = true)
    private Long endTime;

    @ApiModelProperty(name = "shetuan_no", value = "社团编码", required = true)
    private String shetuanNo;

    @ApiModelProperty(name = "pre", value = "是否预售活动", required = true)
    @NotNull(message = "是否预售活动不能为空")
    private Integer pre;

    /**
     * 提货时间
     */
    @ApiModelProperty(name = "pick_time", value = "提货时间", required = true)
    @NotNull(message = "提货时间不能为空")
    private Long pickTime;

    /**
     * 报名截止时间
     */
    @ApiModelProperty(name = "join_stop_time", value = "报名截止时间")
    @NotNull(message = "报名截止时间时间不能为空")
    private Long joinStopTime;

    @ApiModelProperty(name = "province_id", value = "省份id")
    private Integer provinceId;

    @ApiModelProperty(name = "city_id", value = "城市id")
    private Integer cityId;

    @ApiModelProperty(name = "county_id", value = "区县id")
    private Integer countyId;

    @ApiModelProperty(name = "province", value = "省份名")
    private String province;

    @ApiModelProperty(name = "city", value = "城市名")
    private String city;

    @ApiModelProperty(name = "county", value = "区县名")
    private String county;

    @ApiModelProperty(name = "address", value = "详细地址")
    private String address;

    @ApiModelProperty(name = "description", value = "活动描述")
    private String description;
    /**
     * 首页是否弹框
     * 1 弹框
     * 0 不弹
     */
    @ApiModelProperty(name = "page_popup", value = "首页是否弹框")
    private Integer pagePopup;




}
