package com.enation.app.javashop.core.base.message;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 商品sku 变化消息
 */
@Data
public class GoodsSkuChangeMsg  implements Serializable {

    private static final long serialVersionUID = 8344213824120196957L;
    /**
     * 变更资源，商品id集合
     */
    private List<Integer> skuIds;

    /**
     * 操作类型
     */
    private Integer operationType;

    /**
     * 删除
     */
    public final static int DELETE_OPERATION = 1;

	public GoodsSkuChangeMsg() {
	}


	public GoodsSkuChangeMsg(List<Integer> skuIds, Integer operationType) {
		this.skuIds = skuIds;
		this.operationType = operationType;
	}
}
