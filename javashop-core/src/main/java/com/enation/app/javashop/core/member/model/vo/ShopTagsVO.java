package com.enation.app.javashop.core.member.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 商铺标签VO
 */
@Data
public class ShopTagsVO {

    @ApiModelProperty(name = "tag_id", value = "标签id")
    private Integer tagId;

    @ApiModelProperty(name = "tag_name", value = "标签名称")
    private String tagName;

    @ApiModelProperty(name = "mark", value = "关键字")
    private String mark;

    private ShopTagsVO(){}

    public ShopTagsVO(String tagName, String mark) {
        this.tagName = tagName;
        this.mark = mark;
    }
}
