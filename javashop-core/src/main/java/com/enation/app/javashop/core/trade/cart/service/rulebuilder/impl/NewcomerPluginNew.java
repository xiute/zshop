package com.enation.app.javashop.core.trade.cart.service.rulebuilder.impl;


import com.enation.app.javashop.core.promotion.newcomer.model.vos.MiniNewcomerDataVO;
import com.enation.app.javashop.core.promotion.tool.model.enums.PromotionTypeEnum;
import com.enation.app.javashop.core.promotion.tool.model.vo.PromotionVO;
import com.enation.app.javashop.core.trade.cart.model.enums.PromotionTarget;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuVO;
import com.enation.app.javashop.core.trade.cart.model.vo.PromotionRule;
import com.enation.app.javashop.core.trade.cart.service.rulebuilder.SkuPromotionRuleBuilder;
import com.enation.app.javashop.framework.util.CurrencyUtil;
import com.enation.app.javashop.framework.util.DateUtil;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 新人购插件
 * @date 20210303 14:50
 */
@Component
public class NewcomerPluginNew implements SkuPromotionRuleBuilder {

    @Order(3)
    @Override
    public PromotionRule build(CartSkuVO skuVO, PromotionVO promotionVO) {

        PromotionRule rule = new PromotionRule(PromotionTarget.SKU);

        MiniNewcomerDataVO miniNewcomerDataVO = promotionVO.getMiniNewcomerDataVO();
        if (miniNewcomerDataVO == null) {
            return rule;
        }

        /**
         * 过期判定
         */
        //开始时间和结束时间
        long startTime = promotionVO.getStartTime();
        long endTime = promotionVO.getEndTime();

        //是否过期了
        boolean expired = !DateUtil.inRangeOf(startTime, endTime);
        if (expired) {
            rule.setInvalid(true);
            rule.setInvalidReason("新人购活动已过期,有效期为:[" + DateUtil.toString(startTime, "yyyy-MM-dd HH:mm:ss") + "至" + DateUtil.toString(endTime, "yyyy-MM-dd HH:mm:ss") + "]");
            return rule;
        }


        //默认商品标签
        String tag = "新人购";

        //剩余可售数量 万一发生超卖，这里处理一下
        int num = miniNewcomerDataVO.getSoldQuantity() < 0 ? 0 : miniNewcomerDataVO.getSoldQuantity();


        //如果0件享受促销
        if (num == 0) {
            return rule;
        }


        //如果 剩余优惠数量不足
        if (skuVO.getNum() > num) {
            tag = "仅[" + num + "]件享新人购活动";
            skuVO.setPurchaseNum(num);
        } else {
            skuVO.setPurchaseNum(skuVO.getNum());
        }


        // 销售的总价格  售价 * sku的数量
        double totalActivityPrice = CurrencyUtil.mul(miniNewcomerDataVO.getSalesPrice(), skuVO.getPurchaseNum());

        //非活动部分价格
        double otherTotal = 0;
        if (!skuVO.getNum().equals(skuVO.getPurchaseNum())) {
            otherTotal = CurrencyUtil.mul((skuVO.getNum() - skuVO.getPurchaseNum()), skuVO.getOriginalPrice());
        }
        totalActivityPrice = CurrencyUtil.add(totalActivityPrice, otherTotal);
        double reducedTotalPrice = CurrencyUtil.sub(skuVO.getSubtotal(), totalActivityPrice);
        double reducedPrice = CurrencyUtil.sub(skuVO.getOriginalPrice(), miniNewcomerDataVO.getSalesPrice());

        //如果商品金额 小于 优惠金额    则减免金额 = 需要支付的金额

        if (skuVO.getSubtotal() < reducedTotalPrice) {
            reducedTotalPrice = totalActivityPrice;
        }

        rule.setReducedTotalPrice(reducedTotalPrice);


        rule.setReducedPrice(reducedPrice);
        rule.setTips("新人购价[" + miniNewcomerDataVO.getSalesPrice() + "]元");
        rule.setTag(tag);
        return rule;
    }

    @Override
    public PromotionTypeEnum getPromotionType() {
        return PromotionTypeEnum.NEWCOMER;
    }
}
