package com.enation.app.javashop.core.promotion.shetuan.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author JFeng
 * @date 2020/6/3 14:59
 */

@Table(name = "es_shetuan_order")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShetuanOrderDO {
    /**
     * id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;
    /**
     * 团长id
     */
    @Column(name = "leader_id")
    private Integer leaderId;
    /**
     * 订单id
     */
    @Column(name = "order_id")
    private Integer orderId;

    @Column(name = "order_sn")
    private String orderSn;

    @Column(name = "order_price")
    private Double orderPrice;

    @Column(name = "shetuan_id")
    private Integer shetuanId;

    @Column(name = "leader_commission")
    private Double leaderCommission;

    @Column(name = "leader_return_commission")
    private Double leaderReturnCommission;

    @Column(name = "commission_rate")
    private Double commissionRate;

    @Column(name = "pick_address")
    private String pickAddress;

    @Column(name = "buy_num")
    private Integer buyNum;

    @Column(name = "buyer_name")
    private String buyerName;

    @Column(name = "buyer_mobile")
    private String buyerMobile;

    @Column(name = "pick_time")
    private Long pickTime;

    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "update_time")
    private Long updateTime;

    @Column(name = "order_status")
    private String  orderStatus;

    @Column(name = "st_order_status")
    private String  stOrderStatus;

    @Column(name = "leader_member_id")
    private Integer leaderMemberId;

    @Column(name = "commission_type")
    private String  commissionType;

    @Column(name = "buyer_member_id")
    private Integer buyerMemberId;

    @Column(name = "buyer_member_name")
    private String  buyerMemberName;

    @Column(name = "leader_name")
    private String  leaderName;

    @Column(name = "leader_mobile")
    private String  leaderMobile;

    @Column(name = "site_name")
    private String siteName;

    @Column(name = "site_type")
    private Integer siteType;

    @Column(name = "seller_id")
    private Integer  sellerId;

    @Column(name = "remark")
    private String remark;
}
