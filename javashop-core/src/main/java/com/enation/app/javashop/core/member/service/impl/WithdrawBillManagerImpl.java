package com.enation.app.javashop.core.member.service.impl;

import com.enation.app.javashop.core.member.model.dos.WithdrawBillDO;
import com.enation.app.javashop.core.member.service.WithdrawBillManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 提现结算单关系
 * @author 孙建
 */
@Service
public class WithdrawBillManagerImpl implements WithdrawBillManager {

    @Autowired
    @Qualifier("memberDaoSupport")
    private DaoSupport memberDaoSupport;

    /**
     * 批量保存
     */
    @Override
    public void batchSave(List<WithdrawBillDO> withdrawBillList) {
        List<String> sqlList = new ArrayList<>();
        for (WithdrawBillDO withdrawBillDO : withdrawBillList) {
            String sql = "insert into es_withdraw_bill values ( null,"
                    + withdrawBillDO.getWithdrawRecordId() + ","
                    + withdrawBillDO.getBillId()
                    + " )";
            sqlList.add(sql);
        }
        memberDaoSupport.batchUpdate(sqlList.toArray(new String[sqlList.size()]));
    }

    @Override
    public List<WithdrawBillDO> listByWithdrawRecordId(Integer withdrawRecordId) {
        String sql = "select * from es_withdraw_bill where withdraw_record_id = ?";
        return memberDaoSupport.queryForList(sql, WithdrawBillDO.class, withdrawRecordId);
    }
}
