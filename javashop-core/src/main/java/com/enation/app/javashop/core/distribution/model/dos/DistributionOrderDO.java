package com.enation.app.javashop.core.distribution.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分销订单
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/21 上午11:13
 */
@Data
@Table(name = "es_distribution_order")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DistributionOrderDO {

    /**
     * 主键id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;

    /**
     * 关联的普通订单的id
     */
    @Column(name = "order_id")
    @ApiModelProperty(value = "关联的普通订单的id", required = true)
    private Integer orderId;

    /**
     * 订单编号
     */
    @Column(name = "order_sn")
    @ApiModelProperty(value = "订单编号", required = true)
    private String orderSn;

    /**
     * 购买会员的id
     */
    @Column(name = "buyer_member_id")
    @ApiModelProperty(value = "购买会员的id", required = true)
    private Integer buyerMemberId;

    /**
     * 购买会员的名称
     */
    @Column(name = "buyer_member_name")
    @ApiModelProperty(value = "购买会员的名称", required = true)
    private String buyerMemberName;

    /**
     * 1级分销商id
     */
    @Column(name = "member_id_lv1")
    @ApiModelProperty(value = "1级分销商id", required = true)
    private Integer memberIdLv1;

    /**
     * 2级分销商id
     */
    @Column(name = "member_id_lv2")
    @ApiModelProperty(value = "2级分销商id", required = true)
    private Integer memberIdLv2;


    @Column(name = "lv1_point")
    @ApiModelProperty(value = "一级模版返现比例", required = true)
    private Double lv1Point;


    @Column(name = "lv2_point")
    @ApiModelProperty(value = "二级模版返现比例", required = true)
    private Double lv2Point;

    @Column(name = "inviter_member_id")
    @ApiModelProperty(value = "邀请人会员id", required = true)
    private Integer inviterMemberId;

    @Column(name = "invite_point")
    @ApiModelProperty(value = "邀请佣金返现比例", required = true)
    private Double invitePoint;

    @Column(name = "invite_rebate")
    @ApiModelProperty(value = "邀请佣金金额", required = true)
    private Double inviteRebate;

    @Column(name = "subsidy_member_id")
    @ApiModelProperty(value = "补贴人会员id", required = true)
    private Integer subsidyMemberId;

    @Column(name = "subsidy_point")
    @ApiModelProperty(value = "补贴比例", required = true)
    private Double subsidyPoint;

    @Column(name = "subsidy_rebate")
    @ApiModelProperty(value = "补贴金额", required = true)
    private Double subsidyRebate;

    @Column(name = "goods_rebate")
    @ApiModelProperty(value = "商品返现详情", required = true)
    /**
     * see DistributionGoods.java
     * detail as list<DistributionGoods>
     */
    private String goodsRebate;


    /**
     * 结算单id
     */
    @Column(name = "bill_id")
    @ApiModelProperty(value = "结算单id", required = true)
    private Integer billId;

    /**
     * 解冻日期
     */
    @Column(name = "settle_cycle")
    @ApiModelProperty(value = "解冻日期", required = true)
    private Integer settleCycle;

    /**
     * 订单总价
     */
    @Column(name = "order_price")
    @ApiModelProperty(value = "订单总价", required = true)
    private Double orderPrice;

    /**
     * 订单创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(value = "订单创建时间", required = true)
    private Long createTime;

    /**
     * 1级提成金额
     */
    @Column(name = "grade1_rebate")
    @ApiModelProperty(value = "1级提成金额", required = true)
    private Double grade1Rebate;

    /**
     * 2级提成金额
     */
    @Column(name = "grade2_rebate")
    @ApiModelProperty(value = "2级提成金额", required = true)
    private Double grade2Rebate;


    /**
     * 1级退款金额
     */
    @Column(name = "grade1_sellback_price")
    @ApiModelProperty(value = "1级退款金额", required = true)
    private Double grade1SellbackPrice;

    /**
     * 2级退款金额
     */
    @Column(name = "grade2_sellback_price")
    @ApiModelProperty(value = "2级退款金额", required = true)
    private Double grade2SellbackPrice;


    /**
     * 是否退货  0=未退货 1=已退货
     */
    @Column(name = "is_return")
    @ApiModelProperty(value = "是否退货  0=未退货 1=已退货", required = true)
    private Integer isReturn;

    /**
     * 退款金额
     */
    @Column(name = "return_money")
    @ApiModelProperty(value = "退款金额", required = true)
    private Double returnMoney;

    @Column(name = "is_withdraw")
    @ApiModelProperty(value = "是否结算  0=未结算  1=已结算", required = true)
    private Integer isWithdraw;

    /**
     * 店铺id
     */
    @Column(name = "seller_id")
    @ApiModelProperty(value = "店铺id", required = true)
    private Integer sellerId;

    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    @ApiModelProperty(value = "店铺名称", required = true)
    private String sellerName;

}