package com.enation.app.javashop.core.payment.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fk
 * @version v1.0
 * @Description: 支付请求返回的form
 * @date 2018/7/17 10:39
 * @since v7.0.0
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Form {

    public static final String RESULT = "success";
    public static final String MESSAGE = "message";
    public static final String SUCCESS = "200";
    public static final String FAIL = "500";

    @ApiModelProperty(value = "表单请求地址")
    private String gatewayUrl;

    @ApiModelProperty(value = "表单请求内容")
    private List<FormItem> formItems;

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public List<FormItem> getFormItems() {
        return formItems;
    }

    public void setFormItems(List<FormItem> formItems) {
        this.formItems = formItems;
    }

    public static Form failForm(String message){
        List<FormItem> formItems = new ArrayList<>();
        FormItem formItem = new FormItem();
        formItem.setItemName(RESULT);
        formItem.setItemValue(FAIL);
        formItems.add(formItem);
        FormItem formItemMessage = new FormItem();
        formItemMessage.setItemName(MESSAGE);
        formItemMessage.setItemValue(message);
        formItems.add(formItemMessage);
        Form form = new Form();
        form.setFormItems(formItems);
        return form;
    }
}
