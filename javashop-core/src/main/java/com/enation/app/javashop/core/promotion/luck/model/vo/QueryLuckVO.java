package com.enation.app.javashop.core.promotion.luck.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author 孙建
 * 抽奖活动VO
 */
@Data
public class QueryLuckVO implements Serializable {

    @ApiModelProperty(value = "页码", name = "page_no", required = true)
    @NotNull(message = "请输入当前页码")
    private Integer pageNo;

    @ApiModelProperty(value = "分页大小", name = "page_size", required = true)
    @NotNull(message = "请输入分页大小")
    private Integer pageSize;
    /**
     * 活动名称
     */
    private String luckName;
    /**
     * 活动标题
     */
    private String luckTitle;
    /**
     * 开始时间
     */
    private Long startTime;
    /**
     * 结束时间
     */
    private Long endTime;
    /**
     * 活动状态
     */
    private Integer luckStatus;
    /**
     * 店铺ID
     */
    private Integer sellerId;
    /**
     * 店铺名称
     */
    private String sellerName;

}