package com.enation.app.javashop.core.aftersale.model.dto;

import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.aftersale.model.enums.*;
import com.enation.app.javashop.core.aftersale.service.AfterSaleOperateAllowable;
import com.enation.app.javashop.core.trade.order.model.enums.PaymentTypeEnum;
import com.enation.app.javashop.framework.util.StringUtil;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description 退款单列表DTO
 * @ClassName RefundDTO
 * @author zjp
 * @version v7.0
 * @since v7.0 上午11:32 2018/5/8
 */
public class RefundDTO extends RefundDO{

    private static final long serialVersionUID = -6549013574643894322L;
    @ApiModelProperty(value = "退货(款)单状态文字描述",name = "refund_status_text")
    private String refundStatusText;

    @ApiModelProperty(value = "退款账户类型文字描述",name = "account_type_text")
    private String accountTypeText;

    @ApiModelProperty(value = "退(货)款类型文字描述:退款，退货",name = "refuse_type_text")
    private String refuseTypeText ;

    @ApiModelProperty(value = "售后类型文字描述:取消订单，申请售后",name = "refund_type_text")
    private String refundTypeText ;

    @ApiModelProperty(value = "操作是否允许",name = "after_sale_operate_allowable")
    private AfterSaleOperateAllowable afterSaleOperateAllowable;

    private String  orderStatus;

    private Integer isShipped;

    public String getRefundStatusText() {
        return RefundStatusEnum.valueOf(this.getRefundStatus()).description();
    }

    @ApiModelProperty(value = "售后操作允许情况")
    public AfterSaleOperateAllowable getAfterSaleOperateAllowable() {
        AfterSaleOperateAllowable allowable = new AfterSaleOperateAllowable(RefuseTypeEnum.valueOf(this.getRefuseType()),
                    RefundStatusEnum.valueOf(this.getRefundStatus()), PaymentTypeEnum.valueOf(this.getPaymentType()));
        return allowable;
    }

    @ApiModelProperty(value = "退款方式文字")
    public String getAccountTypeText() {

        try {

            // 退款方式  園路退回或者线下支付
            String refundWay = RefundWayEnum.valueOf(this.getRefundWay()).description();

            if(!StringUtil.isEmpty(this.getAccountType())){
                String text = AccountTypeEnum.valueOf(this.getAccountType()).description();

                return  refundWay+"-"+text;
            }

            return refundWay;
        }catch (Exception e){
            e.printStackTrace();
        }

        return "未知";
    }

    public String getRefuseTypeText() {
        return RefuseTypeEnum.valueOf(this.getRefuseType()).description();
    }

    public String getRefundTypeText() { return RefundTypeEnum.valueOf(this.getRefundType()).description(); }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getIsShipped() {
        return isShipped;
    }

    public void setIsShipped(Integer isShipped) {
        this.isShipped = isShipped;
    }

    @Override
    public String toString() {
        return "RefundDTO{}";
    }
}
