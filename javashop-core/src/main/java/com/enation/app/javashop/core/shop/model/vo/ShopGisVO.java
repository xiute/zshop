package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author 王志杨
 * @Date 2020/8/26 14:25
 * @Descroption 店铺服务范围VO
 */
@ApiModel(value = "店铺服务范围VO")
@Data
public class ShopGisVO implements Serializable {

    /**店铺Id*/
    @ApiModelProperty(name="shop_id",value="店铺id",required=false)
    private Integer shopId;

    /**店铺服务范围多多边形各节点坐标组成的嵌套集合*/
    @ApiModelProperty(name="polygons",value="店铺服务范围各节点坐标组成的嵌套集合List<List<Double>>",required=true)
//    private List<String> polygons;
    private List<List<Double>> polygons;

    /**多边形绘制人*/
    @ApiModelProperty(name="create_name",value="多边形绘制人",required=false)
    private String createName;
//
//    /**最后一次修改人*/
//    @Column(name = "update_name")
//    @ApiModelProperty(name="update_name",value="最后一次修改人",required=false)
//    private String updateName;

}
