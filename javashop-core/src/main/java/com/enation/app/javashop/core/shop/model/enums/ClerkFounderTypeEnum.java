package com.enation.app.javashop.core.shop.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 店员类型
 */
public enum ClerkFounderTypeEnum {
    SUPER_CLERK(1,"主店长" ),

    COMMON_CLERK(0,"店员");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (ClerkFounderTypeEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    ClerkFounderTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }


}
