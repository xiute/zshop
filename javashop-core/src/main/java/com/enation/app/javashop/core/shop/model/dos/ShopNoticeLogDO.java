package com.enation.app.javashop.core.shop.model.dos;

import java.io.Serializable;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 店铺站内消息实体
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-10 10:21:45
 */
@Table(name="es_shop_notice_log")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShopNoticeLogDO implements Serializable {
			
    private static final long serialVersionUID = 7865920545802873L;
    
    /**id*/
    @Id(name = "id")
    @ApiModelProperty(hidden=true)
    private Integer id;
    /**店铺ID*/
    @Column(name = "shop_id")
    @ApiModelProperty(name="shop_id",value="店铺ID",required=false)
    private Integer shopId;
    /**站内信内容*/
    @Column(name = "notice_content")
    @ApiModelProperty(name="notice_content",value="站内信内容",required=false)
    private String noticeContent;
    /**发送时间*/
    @Column(name = "send_time")
    @ApiModelProperty(name="send_time",value="发送时间",required=false)
    private Long sendTime;
    /**是否删除 ：1 删除   0  未删除*/
    @Column(name = "is_delete")
    @ApiModelProperty(name="is_delete",value="是否删除 ：1 删除   0  未删除",required=false)
    private Integer isDelete;
    /**是否已读 ：1已读   0 未读*/
    @Column(name = "is_read")
    @ApiModelProperty(name="is_read",value="是否已读 ：1已读   0 未读",required=false)
    private Integer isRead;
    /**消息类型*/
    @Column(name = "type")
    @ApiModelProperty(name="type",value="消息类型",required=false)
    private String type;

    private String typeText;

    @PrimaryKeyField
    public Integer getId() {
        return id;
    }

}