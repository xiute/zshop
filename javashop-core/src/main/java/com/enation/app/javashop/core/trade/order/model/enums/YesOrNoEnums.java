package com.enation.app.javashop.core.trade.order.model.enums;

/**
 * @Author: zhou
 * @Date: 2020/12/27
 * @Description:
 */
public enum YesOrNoEnums {
    YES(1, "是"),
    NO(0, "否");

    private int index;
    private String describe;

    YesOrNoEnums(int index, String describe) {
        this.index = index;
        this.describe = describe;
    }

    public int getIndex() {
        return index;
    }

    public String getDescribe() {
        return describe;
    }
}
