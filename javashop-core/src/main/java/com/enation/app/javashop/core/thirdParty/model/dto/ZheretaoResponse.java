package com.enation.app.javashop.core.thirdParty.model.dto;

import lombok.Data;

@Data
public class ZheretaoResponse<T> {
    private String authorization;
    private Integer code;
    private boolean mock;
    private String msg;
    private ResponseData<T> data;

    public ZheretaoResponse(String authorization, Integer code, boolean mock, String msg, ResponseData<T> data) {
        this.authorization = authorization;
        this.code = code;
        this.mock = mock;
        this.msg = msg;
        this.data = data;
    }
}
