package com.enation.app.javashop.core.base.model.dto;

import lombok.Data;

@Data
public class JituTrack {
    private String customerTracking;
    private String scanTime;
}