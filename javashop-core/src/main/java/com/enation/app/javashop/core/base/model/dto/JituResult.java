package com.enation.app.javashop.core.base.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/11/30 10:25
 */
@Data
public class JituResult {
    private  Boolean  succ;
    private  List<JituData>  data;

}
