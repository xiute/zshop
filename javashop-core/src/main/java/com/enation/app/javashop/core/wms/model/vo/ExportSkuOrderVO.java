package com.enation.app.javashop.core.wms.model.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.enation.app.javashop.core.goods.model.dos.GoodsSkuDO;
import com.enation.app.javashop.core.goods.model.vo.SpecValueVO;
import com.enation.app.javashop.framework.util.JsonUtil;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author JFeng
 * @date 2020/7/17 14:31
 */

@ExcelIgnoreUnannotated
public class ExportSkuOrderVO {

    @ExcelProperty("商品名称")
    private String name;

    @ExcelProperty("购买数量")
    private Integer shipNum;
    //
    //@ExcelProperty("今日总数")
    //private Integer numTotal;

    @ExcelProperty("批次号")
    private Integer sortingBatch;

    @ExcelProperty("格子号")
    private Integer sortingSeq;

    @ExcelProperty("订单号")
    private String orderSn;

    @ExcelProperty("自提站点")
    private String siteName;

    @ExcelProperty("订单类型")
    private String orderType;

    @ExcelProperty("商品类目")
    private String categoryName;

    private Integer skuId;


    private String categoryPath;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getShipNum() {
        return shipNum;
    }

    public void setShipNum(Integer shipNum) {
        this.shipNum = shipNum;
    }

    public Integer getSortingBatch() {
        return sortingBatch;
    }

    public void setSortingBatch(Integer sortingBatch) {
        this.sortingBatch = sortingBatch;
    }

    public Integer getSortingSeq() {
        return sortingSeq;
    }

    public void setSortingSeq(Integer sortingSeq) {
        this.sortingSeq = sortingSeq;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
    }
}
