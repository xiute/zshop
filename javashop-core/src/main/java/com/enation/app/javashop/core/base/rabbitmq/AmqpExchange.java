package com.enation.app.javashop.core.base.rabbitmq;

/**
 * AMQP消息定义-交换机
 *
 * @author kingapex
 * @version 1.0
 * @since 6.4
 * 2017-08-17 18：00
 */
public class AmqpExchange {


    /**
     * TEST
     */
    public final static String TEST_EXCHANGE = "TEST_EXCHANGE_FANOUT";


    /**
     * PC首页变化消息
     */
    public final static String PC_INDEX_CHANGE = "PC_INDEX_CHANGE";

    /**
     * 移动端首页变化消息
     */
    public final static String MOBILE_INDEX_CHANGE = "MOBILE_INDEX_CHANGE";

    /**
     * 商品变化消息
     */
    public final static String GOODS_CHANGE = "GOODS_CHANGE";

    /**
     * 商品SKU 变化消息
     */
    public final static String GOODS_SKU_CHANGE = "GOODS_SKU_CHANGE";
    /**
     * 商品优先级变化消息
     */
    public final static String GOODS_PRIORITY_CHANGE = "GOODS_PRIORITY_CHANGE";

    /**
     * 商品变化消息附带原因
     */
    public final static String GOODS_CHANGE_REASON = "GOODS_CHANGE_REASON";

    /**
     * 帮助变化消息
     */
    public final static String HELP_CHANGE = "HELP_CHANGE";


    /**
     * 页面生成消息
     */
    public final static String PAGE_CREATE = "PAGE_CREATE";

    /**
     * 索引生成消息
     */
    public final static String INDEX_CREATE = "INDEX_CREATE";

    /**
     * 订单创建消息
     * 没有入库
     */
    public final static String ORDER_CREATE = "ORDER_CREATE";

    /**
     * 入库失败消息
     * 入库失败
     */
    public final static String ORDER_INTODB_ERROR = "ORDER_INTODB_ERROR";

    /**
     * 订单状态变化消息
     * 带入库的
     */
    public final static String ORDER_STATUS_CHANGE = "ORDER_STATUS_CHANGE";

    /**
     * 会员登录消息
     */
    public final static String MEMEBER_LOGIN = "MEMEBER_LOGIN";

    /**
     * 会员注册消息
     */
    public final static String MEMEBER_REGISTER = "MEMEBER_REGISTER";

    /**
     * 店铺变更消息
     */
    public final static String SHOP_CHANGE_REGISTER = "SHOP_CHANGE_REGISTER";
    /**
     * 分类变更消息
     */
    public final static String GOODS_CATEGORY_CHANGE = "GOODS_CATEGORY_CHANGE";

    /**
     * 售后状态改变消息
     */
    public final static String REFUND_STATUS_CHANGE = "REFUND_STATUS_CHANGE";

    /**
     * 发送站内信息
     */
    public final static String MEMBER_MESSAGE = "MEMBER_MESSAGE";

    /**
     * 发送手机短信消息
     */
    public final static String SEND_MESSAGE = "SEND_MESSAGE";

    /**
     * 邮件发送消息
     */
    public final static String EMAIL_SEND_MESSAGE = "EMAIL_SEND_MESSAGE";

    /**
     * 商品评论消息
     */
    public final static String GOODS_COMMENT_COMPLETE = "GOODS_COMMENT_COMPLETE";
    /**
     * 网上支付
     */
    public final static String ONLINE_PAY = "ONLINE_PAY";

    /**
     * 完善个人资料
     */
    public final static String MEMBER_INFO_COMPLETE = "MEMBER_INFO_COMPLETE";

    /**
     * 站点导航栏变化消息
     */
    public final static String SITE_NAVIGATION_CHANGE = "SITE_NAVIGATION_CHANGE";


    /**
     * 商品收藏
     */
    public final static String GOODS_COLLECTION_CHANGE = "GOODS_COLLECTION_CHANGE";


    /**
     * 店铺收藏
     */
    public final static String SELLER_COLLECTION_CHANGE = "SELLER_COLLECTION_CHANGE";


    /**
     * 店铺关闭
     */
    public final static String CLOSE_STORE = "CLOSE_STORE";

    /**
     * 店铺信息发生改变
     */
    public final static String SHOP_CHANGE = "SHOP_CHANGE";

    /**
     * 店铺信息发生改变
     */
    public final static String SHIP_TEMPLATE_CHANGE = "SHIP_TEMPLATE_CHANGE";


    /**
     * 店铺浏览统计
     */
    public final static String SHOP_VIEW_COUNT = "SHOP_VIEW_COUNT";

    /**
     * 商品浏览统计
     */
    public final static String GOODS_VIEW_COUNT = "GOODS_VIEW_COUNT";

    /**
     * 会员资料改变
     */
    public final static String MEMBER_INFO_CHANGE = "MEMBER_INFO_CHANGE";

    /**
     * 会员历史足迹
     */
    public final static String MEMBER_HISTORY = "MEMBER_HISTORY";

    /**
     * 搜索关键字消息
     */
    public final static String SEARCH_KEYWORDS = "SEARCH_KEYWORDS";

    /**
     * 提示词变更
     */
    public final static String GOODS_WORDS_CHANGE = "GOODS_WORDS_CHANGE";


    /**
     * 拼团成功消息
     */
    public final static String PINTUAN_SUCCESS = "PINTUAN_SUCCESS";


    /**
     * 拼团活动变更消息
     */
    public final static String SHETUAN_CHANGE = "SHETUAN_CHANGE";


    /**
     * 多退少补退款后消息
     */
    public final static String SPREAD_REFUND_COMPLETE = "SPREAD_REFUND_COMPLETE";

    /**
     * 购物车加购商品消息
     */
    public final static String GOODS_CART_COUNT = "CART_GOODS_COUNT";

    /**
     * 理赔单状态改变通知
     */
    public final static String CLAIMS_STATUS_CHANGE = "CLAIMS_STATUS_CHANGE";

    /**
     * 出库单事件通知
     */
    public final static String WMS_ORDER_CHANGE = "WMS_ORDER_CHANGE";

    /**
     * 调用钱包支付消息
     */
    public final static String WALLET_PAY_INVOKE = "WALLET_PAY_INVOKE";

    public final static String USER_VISIT = "USER_VISIT";
    /**
     * 给客户推活动订阅消息
     */
    public final static String CUSTOMER_PUSH_MESSAGE  = "CUSTOMER_PUSH_MESSAGE";
    /**
     * 保存会员团长绑定记录消息
     */
    public final static String DISTIRBUTION_BINDING_MESSAGE  = "DISTIRBUTION_BINDING_MESSAGE";

    /**
     * 查询店铺中关联所有商品的标签，并放入redis
     */
    public final static String SHOP_TAG_CHANGE = "SHOP_TAG_CHANGE";

}
