package com.enation.app.javashop.core.goods.model.vo;

import com.enation.app.javashop.core.goods.model.dos.GoodsGalleryDO;
import com.enation.app.javashop.core.goods.model.dos.TagsDO;
import com.enation.app.javashop.core.goods.model.dto.TagsDTO;
import com.enation.app.javashop.core.goodssearch.model.GoodsTagIndex;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 商品详情页使用
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月29日 上午9:54:05
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class GoodsShowDetail extends CacheGoods implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3531885212488390703L;
    @ApiModelProperty(name = "分类名称")
    private String categoryName;
    @ApiModelProperty(name = "是否下架或删除，1正常  0 下架或删除")
    private Integer goodsOff;
    @ApiModelProperty(name = "商品参数")
    private List<GoodsParamsGroupVO> paramList;
    @ApiModelProperty(name = "商品相册")
    private List<GoodsGalleryDO> galleryList;
    @ApiModelProperty(name = "商品好平率")
    private Double grade;

    @ApiModelProperty(name = "video_url", value = "主图视频", required = false)
    private String videoUrl;

    @ApiModelProperty(name = "分享(一级)佣金显示")
    private Double firstCommissionDisplay;

    @ApiModelProperty(name = "平台补贴显示")
    private Double subsidyDisplay;
    /**
     * seo标题
     */
    @ApiModelProperty(name = "page_title", value = "seo标题", required = false)
    private String pageTitle;
    /**
     * seo关键字
     */
    @ApiModelProperty(name = "meta_keywords", value = "seo关键字", required = false)
    private String metaKeywords;
    /**
     * seo描述
     */
    @ApiModelProperty(name = "meta_description", value = "seo描述", required = false)
    private String metaDescription;

    /** 谁承担运费0：买家承担，1：卖家承担 */
    @ApiModelProperty(name = "goods_transfee_charge", value = "谁承担运费0：买家承担，1：卖家承担")
    private Integer goodsTransfeeCharge;


    @ApiModelProperty(value = "是否本地商品")
    private Integer isLocal;

    @ApiModelProperty(value = "是否商城商品")
    private Integer isGlobal;

    private Double mktprice;

    @ApiModelProperty(name = "goods_type", value = "商品类型NORMAL普通,POINT积分,VIRTUAL虚拟商品")
    private String goodsType;

    @ApiModelProperty(name = "expiry_day_text",value = "虚拟商品的使用截止日期")
    private String expiryDayText;

    @ApiModelProperty(name = "available_date",value = "虚拟商品的可用日期")
    private String availableDate;

    @ApiModelProperty(name = "seller_logo", value = "店铺logo", required = false)
    private String sellerLogo;

    @ApiModelProperty(name = "seller_address", value = "商家地址")
    private String sellerAddress;

    @ApiModelProperty(name = "seller_phone", value = "商家电话")
    private String sellerPhone;

    /**店铺纬度*/
    @ApiModelProperty(name="shop_lat",value="店铺纬度",required=false)
    private Double shopLat;

    /**店铺经度*/
    @ApiModelProperty(name="shop_lng",value="店铺经度",required=false)
    private Double shopLng;

    private List<TagsDTO> shopHomeTags;

    private List<TagsDTO> goodsListTags;

    private List<TagsDTO> goodsDetailsTags;

}
