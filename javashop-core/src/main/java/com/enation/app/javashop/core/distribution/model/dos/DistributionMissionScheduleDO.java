package com.enation.app.javashop.core.distribution.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 孙建
 * 团长任务进度DO
 */
@Table(name="es_distribution_mission_schedule")
@Data
public class DistributionMissionScheduleDO implements Serializable {

    /**
     * 团长进度ID
     */
    @Id(name="id")
    private Integer id;
    /**
     * 团长任务名称
     */
    @Column(name = "mission_name")
    private String missionName;
    /**
     * 任务明细类型（0：每日任务，1：每月任务）
     */
    @Column(name = "mission_type")
    private Integer missionType;
    /**
     * 任务明细指标（0：下单人数，1：成交金额）
     */
    @Column(name = "mission_target")
    private Integer missionTarget;
    /**
     * 任务期间
     */
    @Column(name = "mission_time")
    private String missionTime;
    /**
     * 团长任务活动id
     */
    @Column(name = "mission_id")
    private Integer missionId;
    /**
     * 任务详情id
     */
    @Column(name = "mission_detail_id")
    private Integer missionDetailId;
    /**
     * 店铺ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;
    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    private String sellerName;
    /**
     * 会员id
     */
    @Column(name = "member_id")
    private Integer memberId;
    // 团长名
    @Column(name = "member_name")
    private String memberName;
    /**
     * 目标值
     */
    @Column(name = "target_value")
    private BigDecimal targetValue;
    /**
     * 完成值
     */
    @Column(name = "complete_value")
    private BigDecimal completeValue;
    /**
     * 累计奖励
     */
    @Column(name = "total_reward")
    private BigDecimal totalReward;
    /**
     * 阶段1状态
     */
    @Column(name = "level1_status")
    private Integer level1Status;
    /**
     * 阶段2状态
     */
    @Column(name = "level2_status")
    private Integer level2Status;
    /**
     * 阶段3状态
     */
    @Column(name = "level3_status")
    private Integer level3Status;
    /**
     * 阶段4状态
     */
    @Column(name = "level4_status")
    private Integer level4Status;
    /**
     * 创建人
     */
    @Column(name = "create_name")
    private String createName;
    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Long updateTime;
    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

}