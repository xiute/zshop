package com.enation.app.javashop.core.distribution.model.enums;

import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 王志杨
 * @since 2021/1/22 17:21
 * 团长任务奖励方式枚举
 */
public enum RewardsTypeEnums {

    CASH(0, "现金奖励"),
    COUPON(1, "平台代金券");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (AccountTypeEnum item : AccountTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    RewardsTypeEnums(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }
}
