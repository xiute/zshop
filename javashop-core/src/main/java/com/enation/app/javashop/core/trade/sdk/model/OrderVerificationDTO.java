package com.enation.app.javashop.core.trade.sdk.model;

import com.enation.app.javashop.core.goods.model.vo.SpecValueVO;
import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 订单核销码信息DTO
 */
@ApiModel(description = "订单核销码信息DTO")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class OrderVerificationDTO {

    /**
     * 订单编号
     */
    @ApiModelProperty(name = "sn", value = "订单编号", required = false)
    private String sn;

    /**
     * 会员ID
     */
    @ApiModelProperty(name = "member_id", value = "会员ID", required = false)
    private Integer memberId;

    /**
     * 买家账号
     */
    @ApiModelProperty(name = "member_name", value = "买家账号", required = false)
    private String memberName;

    /**
     * 订单状态
     */
    @ApiModelProperty(name = "order_status", value = "订单状态", required = false)
    private String orderStatus;

    /**
     * 付款状态
     */
    @ApiModelProperty(name = "pay_status", value = "付款状态", required = false)
    private String payStatus;

    /**
     * 已支付金额
     */
    @ApiModelProperty(name = "pay_money", value = "已支付金额", required = false)
    private Double payMoney;

    /**
     * 订单总额
     */
    @ApiModelProperty(name = "order_price", value = "订单总额", required = false)
    private Double orderPrice;

    /**
     * 商品总额
     */
    @ApiModelProperty(name = "goods_price", value = "商品总额", required = false)
    private Double goodsPrice;

    /**
     * 钱包支付金额
     */
    @ApiModelProperty(name = "wallet_pay_price", value = "钱包支付金额", required = false)
    private Double walletPayPrice;

    /**
     * 支付类型trade/order
     */
    @ApiModelProperty(name = "trade_type", value = "支付类型trade/order", required = false)
    private String tradeType;

    @ApiModelProperty(value = "订单状态文字")
    private String orderStatusText;

    @ApiModelProperty(value = "付款状态文字")
    private String payStatusText;

    @ApiModelProperty(name ="mktprice", value = "虚拟商品市场价")
    private Double mktprice;

    @ApiModelProperty(name ="real_discount_money", value = "相比市场价本次优惠金额")
    private Double realDiscountMoney;

    @ApiModelProperty(name = "expiry_day",value = "虚拟商品的使用截止天数")
    private Integer expiryDay;

    @ApiModelProperty(name = "expiry_date",value = "虚拟商品的使用截止日期")
    private String expiryDate;

    @ApiModelProperty(name = "available_date",value = "虚拟商品的可用日期")
    private String availableDate;

    @ApiModelProperty(name = "verification_code",value = "核销码")
    private String verificationCode;

    @ApiModelProperty(name = "verification_status",value = "核销码状态")
    private Integer verificationStatus;

    @ApiModelProperty(name = "verification_time",value = "核销时间")
    private Long verificationTime;

    /**店铺Id*/
    @ApiModelProperty(name="shop_id",value="店铺Id",required=false)
    private Integer shopId;

    /**店铺名称*/
    @ApiModelProperty(name="shop_name",value="店铺名称",required=true)
    private String shopName;

    /**店铺logo*/
    @ApiModelProperty(name="shop_logo",value="店铺logo",required=false)
    private String shopLogo;

    /**店铺联系电话*/
    @ApiModelProperty(name="link_phone",value="店铺联系电话",required=false)
    private String linkPhone;

    /**店铺详细地址*/
    @ApiModelProperty(name="shop_add",value="店铺详细地址",required=false)
    private String shopAdd;

    @ApiModelProperty(name="shopLat",value="店铺纬度",required=false)
    private Double shopLat;

    @ApiModelProperty(name="shopLng",value="店铺经度",required=false)
    private Double shopLng;

    @ApiModelProperty(value = "商品id")
    private Integer goodsId;

    @ApiModelProperty(value = "产品id")
    private Integer skuId;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品图片")
    private String goodsImage;

    /** 商品规格列表*/
    @ApiModelProperty(value = "规格列表")
    private List<SpecValueVO> specList;


}
