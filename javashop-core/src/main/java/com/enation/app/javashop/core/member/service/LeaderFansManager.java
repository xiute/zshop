package com.enation.app.javashop.core.member.service;

import com.enation.app.javashop.core.member.model.dos.BuyerLastLeaderDO;
import com.enation.app.javashop.core.member.model.dos.LeaderFansDO;
import com.enation.app.javashop.framework.database.Page;

public interface LeaderFansManager {


    boolean band(LeaderFansDO  leaderFansDO);

    BuyerLastLeaderDO getLastLeader(Integer buyerId);

    Page getListByLeaderId(Integer leaderId, Integer pageNo, Integer pageSize);



}
