package com.enation.app.javashop.core.promotion.luck.model.enums;

import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 孙建
 * 兑奖类型
 */
public enum ExchangeTypeEnums {

    AUTO(0, "系统自动"),
    MANUAL(1, "人工处理");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (ExchangeTypeEnums item : ExchangeTypeEnums.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    ExchangeTypeEnums(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }
}
