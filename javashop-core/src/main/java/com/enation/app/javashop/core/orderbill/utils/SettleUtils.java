package com.enation.app.javashop.core.orderbill.utils;

public class SettleUtils {

    /**
     * 虚拟账户上线时间 6月18日凌晨
     */
    public static final long NEW_ACCOUNT_GO_ONLINE_TIME = 1592409600;

    /**
     * 判断是否上线
     */
    public static boolean isGoOnline(long currentTime){
        return currentTime > NEW_ACCOUNT_GO_ONLINE_TIME;
    }

}
