package com.enation.app.javashop.core.distribution.service.impl;

import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionRewardDO;
import com.enation.app.javashop.core.distribution.model.dto.QueryDistributionMissionRewardDTO;
import com.enation.app.javashop.core.distribution.service.DistributionMissionRewardManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zhou
 * @Date: 2021/1/26
 * @Description: 团长任务奖励明细
 */
@Service
public class DistributionMissionRewardManagerImpl implements DistributionMissionRewardManager {

    @Autowired
    @Qualifier("distributionDaoSupport")
    private DaoSupport daoSupport;

    @Override
    public void createDisMisReward(DistributionMissionRewardDO distributionMissionRewardDO) {
        distributionMissionRewardDO.setCreateTime(DateUtil.getDateline());
        daoSupport.insert(distributionMissionRewardDO);
    }

    @Override
    public Page<DistributionMissionRewardDO> queryDisMisRewList(QueryDistributionMissionRewardDTO queryRewardDTO) {
        StringBuilder sql = new StringBuilder(" SELECT * FROM es_distribution_mission_reward WHERE is_delete = 0 ");
        List<Object> term = new ArrayList<>();

        if(queryRewardDTO.getMissionType()!=null){
            sql.append(" and mission_type = ?");
            term.add(queryRewardDTO.getMissionType());
        }

        if(StringUtil.notEmpty(queryRewardDTO.getMemberName())){
            sql.append(" and member_name like ?");
            term.add("%"+queryRewardDTO.getMemberName()+"%");
        }

        if(queryRewardDTO.getRewardsType()!=null){
            sql.append(" and rewards_type = ?");
            term.add(queryRewardDTO.getRewardsType());
        }

        if(queryRewardDTO.getRewardStatus()!=null){
            sql.append(" and reward_status = ?");
            term.add(queryRewardDTO.getRewardStatus());
        }

        if(queryRewardDTO.getMemberId()!=null){
            sql.append(" and member_id = ?");
            term.add(queryRewardDTO.getMemberId());
        }
        // 根据
        sql.append(" order by create_time desc ");

        return this.daoSupport.queryForPage(sql.toString() , queryRewardDTO.getPageNo(),  queryRewardDTO.getPageSize(),DistributionMissionRewardDO.class,term.toArray());

    }
}
