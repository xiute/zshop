package com.enation.app.javashop.core.trade.order.support;

import com.enation.app.javashop.core.trade.order.model.enums.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kingapex on 2019-02-12.
 * 订单特殊状态text处理
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-02-12
 */
public abstract class OrderSpecialStatus {


    /**
     * 定义特殊的流程状态显示
     */
    private  static Map<String ,String> map = new HashMap(16);
    static {

        //拼团已经成团的为待发货
        map.put(OrderTypeEnum.pintuan+"_"+ PaymentTypeEnum.ONLINE+"_"+ OrderStatusEnum.FORMED,"待发货");

        //社团团购订单待发货
        map.put(OrderTypeEnum.shetuan+"_"+ PaymentTypeEnum.ONLINE+"_"+ OrderStatusEnum.PAID_OFF,"待发货");

        //社团团购订单待付款
        map.put(OrderTypeEnum.shetuan+"_"+ PaymentTypeEnum.ONLINE+"_"+ OrderStatusEnum.CONFIRM,"待付款");

        //社团团购订单待发货
        map.put( OrderTypeEnum.shetuan+"_"+ CommentStatusEnum.UNFINISHED+"_"+OrderStatusEnum.ROG,"待评价");

        //社团团购订单待付款
        map.put( OrderTypeEnum.shetuan+"_"+ ServiceStatusEnum.APPLY+"_","售后中");

        //普通订单在线付款的，已经付款显示为待发货
        map.put( OrderTypeEnum.normal+"_"+PaymentTypeEnum.ONLINE+"_"+OrderStatusEnum.PAID_OFF,"待发货");

        //普通订单在线付款的，已确认显示为待付款
        map.put( OrderTypeEnum.normal+"_"+PaymentTypeEnum.ONLINE+"_"+OrderStatusEnum.CONFIRM,"待付款");

        //普通订单货到付款的，已确认的显示为待发货
        map.put( OrderTypeEnum.normal+"_"+PaymentTypeEnum.COD+"_"+OrderStatusEnum.CONFIRM,"待发货");

        //普通订单在线付款的，已确认显示为待评价
        map.put( OrderTypeEnum.normal+"_"+ CommentStatusEnum.UNFINISHED+"_"+OrderStatusEnum.ROG,"待评价");

        //普通订单在线付款的，已确认显示为待评价
        map.put( OrderTypeEnum.normal+"_"+ ServiceStatusEnum.APPLY+"_","售后中");

    }


    /**
     * 获取特殊状态text
     * @param orderType 订单类型
     * @param paymentType 支付类型
     * @param orderStatus 订单状态
     * @return
     */
    public static String getStatusText(String orderType,String paymentType,String orderStatus) {
        return  map.get(orderType +"_"+ paymentType+"_"+orderStatus );
    }

}
