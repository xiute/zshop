package com.enation.app.javashop.core.shop.model.dos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import com.enation.app.javashop.framework.database.annotation.*;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;


/**
 * 运费模版实体
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-28 21:44:49
 */
@Table(name = "es_ship_template")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShipTemplateDO implements Serializable {

    private static final long serialVersionUID = 6348162955890093L;

    /**
     * 模版id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true, value = "模版id")
    private Integer id;

    @Column(name = "seller_id")
    @ApiModelProperty(hidden = true)
    private Integer sellerId;

    @Column
    @ApiParam("名字")
    private String name;

    @Column
    @ApiParam("模版类型，1 重量算运费 2 计件算运费")
    private Integer type;


    // ================JFENG================

    @Column(name = "template_type")
    @ApiParam("模板通途 KUAIDI TONGCHENG")
    private String templateType;

    @ApiParam("配送费用")
    @Column(name = "fee_setting")
    private String  feeSetting;

    @ApiParam("配送时间")
    @Column(name = "time_setting")
    private String  timeSetting;

    @ApiParam("起送价格")
    @Column(name = "base_ship_price")
    private BigDecimal baseShipPrice;

    @Column(name = "ship_range")
    @ApiParam("配送范围")
    private Integer  shipRange;

    @ApiParam("最低配送价格")
    @Column(name = "min_ship_price")
    private BigDecimal minShipPrice;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShipTemplateDO that = (ShipTemplateDO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sellerId, that.sellerId) &&
                Objects.equals(feeSetting, that.feeSetting) &&
                Objects.equals(timeSetting, that.timeSetting) &&
                Objects.equals(baseShipPrice, that.baseShipPrice) &&
                Objects.equals(shipRange, that.shipRange);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sellerId, name, type, templateType, feeSetting, timeSetting, baseShipPrice, shipRange);
    }
}
