package com.enation.app.javashop.core.thirdParty.model.dto;

import com.enation.app.javashop.core.aftersale.model.enums.ExceptionClaimsCodeEnum;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Author: zhou
 * @Date: 2020/10/13
 * @Description:
 */
public class Result<T> implements Serializable {

    @ApiModelProperty(name = "code",value = "返回状态码")
    private int code;
    @ApiModelProperty(name = "message",value = "提示信息")
    private String message;
    @ApiModelProperty(name = "data",value = "返回值")
    private T data;

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }

    public Result() {
    }

    public static Result success(){
        return new Result(ExceptionClaimsCodeEnum.S200.getCode(), ExceptionClaimsCodeEnum.S200.getDescription());
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
