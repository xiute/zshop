package com.enation.app.javashop.core.trade.order.service.impl;

import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.member.MemberClient;
import com.enation.app.javashop.core.goods.model.enums.GoodsType;
import com.enation.app.javashop.core.goods.model.vo.CacheGoods;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.dos.MemberAddress;
import com.enation.app.javashop.core.promotion.seckill.model.vo.SeckillGoodsVO;
import com.enation.app.javashop.core.promotion.seckill.service.SeckillGoodsManager;
import com.enation.app.javashop.core.promotion.tool.model.enums.PromotionTypeEnum;
import com.enation.app.javashop.core.trade.TradeErrorCode;
import com.enation.app.javashop.core.trade.cart.model.vo.*;
import com.enation.app.javashop.core.trade.order.model.dto.OrderDTO;
import com.enation.app.javashop.core.trade.order.model.enums.PaymentTypeEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.enation.app.javashop.core.trade.order.model.vo.CheckoutParamVO;
import com.enation.app.javashop.core.trade.order.model.vo.ConsigneeVO;
import com.enation.app.javashop.core.trade.order.model.vo.TradeVO;
import com.enation.app.javashop.core.trade.order.service.ShippingManager;
import com.enation.app.javashop.core.trade.order.service.TradeCreator;
import com.enation.app.javashop.core.trade.order.service.TradeSnCreator;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.security.model.Buyer;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.JsonUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by kingapex on 2019-01-24.
 * 默认交易创建器
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-01-24
 */
@SuppressWarnings("Duplicates")
public class DefaultTradeCreator implements TradeCreator {

    protected CheckoutParamVO param;
    protected CartView cartView;
    protected   MemberAddress memberAddress;
    protected ShippingManager shippingManager;
    protected GoodsClient goodsClient;
    protected MemberClient memberClient;
    protected TradeSnCreator tradeSnCreator;
    protected SeckillGoodsManager seckillGoodsManager;

    protected final Log logger = LogFactory.getLog(this.getClass());


    public DefaultTradeCreator() {
    }

    /**
     * 通过构造器设置构建交易所需要的原料
     * @param param 结算参数
     * @param cartView 购物车视图
     * @param memberAddress 收货地址
     */
    public DefaultTradeCreator(CheckoutParamVO param, CartView cartView, MemberAddress memberAddress) {

        this.param = param;
        this.cartView = cartView;
        this.memberAddress = memberAddress;
    }

    public DefaultTradeCreator setShippingManager(ShippingManager shippingManager) {
        this.shippingManager = shippingManager;
        return this;
    }

    public DefaultTradeCreator setGoodsClient(GoodsClient goodsClient) {
        this.goodsClient = goodsClient;
        return  this;
    }

    public DefaultTradeCreator setMemberClient(MemberClient memberClient) {
        this.memberClient= memberClient;
        return  this;
    }

    public DefaultTradeCreator setTradeSnCreator( TradeSnCreator tradeSnCreator) {
        this.tradeSnCreator = tradeSnCreator;
        return this;
    }

    public DefaultTradeCreator setSeckillGoodsManager(SeckillGoodsManager seckillGoodsManager) {
        this.seckillGoodsManager = seckillGoodsManager;
        return this;
    }

    @Override
    public TradeVO createTrade() {

        Assert.notNull(tradeSnCreator, "tradeSnCreator为空，请先调用setTradeSnCreator设置正确的交易号生成器");

        Map<Integer, String> shipWayMap =Maps.newHashMap();
        if(param.getShipWays()!=null){
            shipWayMap = param.getShipWays().stream().distinct().collect(Collectors.toMap(ShipWayVO::getSellerId, ShipWayVO::getShipWay));
        }

        if(param.getPaymentType()==null){
            param.setPaymentType(PaymentTypeEnum.ONLINE);
        }

        Buyer buyer = UserContext.getBuyer();

        List<CartVO> cartList = cartView.getCartList();

        //收货人
        ConsigneeVO consignee = new ConsigneeVO();
        if(memberAddress!=null){
            consignee.setConsigneeId(memberAddress.getAddrId());
            consignee.setAddress(memberAddress.getAddr());

            consignee.setProvince(memberAddress.getProvince());
            consignee.setCity(memberAddress.getCity());
            consignee.setCounty(memberAddress.getCounty());
            consignee.setTown(memberAddress.getTown());

            consignee.setProvinceId(memberAddress.getProvinceId());
            consignee.setCityId(memberAddress.getCityId());
            consignee.setCountyId(memberAddress.getCountyId());
            if (memberAddress.getTownId() != null) {
                consignee.setTownId(memberAddress.getTownId());
            }
            consignee.setMobile(memberAddress.getMobile());
            consignee.setTelephone(memberAddress.getTel());
            consignee.setName(memberAddress.getName());
        }


        String tradeNo = tradeSnCreator.generateTradeSn();
        TradeVO tradeVO = new TradeVO();

        //收货人
        tradeVO.setConsignee(consignee);

        //效果编号
        tradeVO.setTradeSn(tradeNo);

        //支付类型
        tradeVO.setPaymentType(param.getPaymentType().value());

        //会员信息
        tradeVO.setMemberId(buyer.getUid());
        tradeVO.setMemberName(buyer.getUsername());
        List<OrderDTO> orderList = new ArrayList<OrderDTO>();

        //订单创建时间
        long createTime = DateUtil.getDateline();

        List<CouponVO> couponVOS = new ArrayList<>();

        List<Integer> skuIds = new ArrayList<>();
        //生成订单
        for (CartVO cart : cartList) {

            //生成订单编号
            String orderSn = tradeSnCreator.generateOrderSn(tradeVO.getMemberId());

            //购物信息
            OrderDTO order = new OrderDTO(cart);

            //创建时间
            order.setCreateTime(createTime);

            //购买的会员信息
            order.setMemberId(buyer.getUid());
            order.setMemberName(buyer.getUsername());
            order.setTradeSn(tradeNo);
            order.setSn(orderSn);
            order.setConsignee(consignee);

            //支付类型
            order.setPaymentType(param.getPaymentType().value());
            //发票
            order.setNeedReceipt(0);
            if (param.getReceipt() != null && !StringUtil.isEmpty(param.getReceipt().getReceiptType())) {
                order.setNeedReceipt(1);
            }
            order.setReceiptHistory(param.getReceipt());

            //订单备注
            order.setRemark(param.getRemark());

            //订单来源
            order.setClientType(param.getClientType());

            //订单价格
            order.getPrice().reCountDiscountPrice();

            if (logger.isDebugEnabled()) {
                logger.debug("订单["+order.getSn()+"]的price:");
                logger.debug(order.getPrice());
            }


            order.setNeedPayMoney(order.getPrice().getTotalPrice());

            order.setOrderPrice(order.getPrice().getTotalPrice());

            // 订单支付最低价格保持0.01 能够调启在线支付 By JFENG 20202-07-03
            if(order.getOrderPrice()<=0.0){
                order.setOrderPrice(0.01);
            }
            order.setGoodsNum(cart.getSkuList().size());

            skuIds.addAll(cart.getSkuList().stream().map(CartSkuVO::getSkuId).collect(Collectors.toList()));

            //配送方式 这个参数暂时无效  补 JFENG 配送方式 LOCAL =1 GLOBAL=2 SELF=3
            String shipType = shipWayMap.get(cart.getSellerId())==null?ShipTypeEnum.SELF.name():shipWayMap.get(cart.getSellerId());
            order.setShippingType(shipType);
            order.setShippingId(ShipTypeEnum.getTextByIndex(shipType));
            if(shipType.equals(ShipTypeEnum.SELF.name())){
               order.setShipName(param.getShipName());
               order.setShipMobile(param.getShipMobile());
            }

            // 如果当前订单商品为虚拟商品，则添加过期时间
            if (!CollectionUtils.isEmpty(cart.getSkuList())) {
                CartSkuVO skuVO = cart.getSkuList().get(0);
                if (GoodsType.VIRTUAL.name().equals(skuVO.getGoodsType())) {
                    order.setExpiryDay(skuVO.getExpiryDay());
                    order.setAvailableDate(skuVO.getAvailableDate());
                }
            }

            orderList.add(order);

            if(cart.getCouponList()!=null&&cart.getCouponList().size()>0){
                for (CouponVO couponVO: cart.getCouponList()) {
                    if(couponVO.getSelected()==1){
                        couponVOS.add(couponVO);
                    }
                }
            }


        }

        //读取结算价格
        PriceDetailVO paymentDetail = cartView.getTotalPrice();
        paymentDetail.reCountDiscountPrice();
        if (logger.isDebugEnabled()) {
            logger.debug("生成TradeVO时price为");
            logger.debug(paymentDetail);

        }
        // 清除购物车
        tradeVO.setSkuIds(skuIds);
        //交易价格
        tradeVO.setPriceDetail(paymentDetail);

        tradeVO.setOrderList(orderList);


        tradeVO.setCouponList(couponVOS);
        return tradeVO;
    }


    /**
     * 验证快递配送范围
     * @return
     */
    @SuppressWarnings("Duplicates")
    @Override
    public TradeCreator checkShipRange() {

        Assert.notNull(shippingManager, "shippingManager为空，请先调用setShippingManager设置正确的交配送管理业务类");

        if (memberAddress == null) {
            throw new ServiceException(TradeErrorCode.E452.code(), "请填写收货地址");
        }

        //已选中结算的商品
        List<CartVO> cartList = cartView.getCartList();

        Integer areaId = memberAddress.getCountyId();

        //2、筛选不在配送区域的商品
        List<CacheGoods> list = this.shippingManager.checkArea(cartList, areaId);

        //验证后存在商品问题的集合
        List<Map> goodsErrorList = new ArrayList();

        if (list.size() > 0) {
            for (CacheGoods goods : list) {
                Map errorMap = new HashMap(16);
                errorMap.put("name", goods.getGoodsName());
                errorMap.put("image", goods.getThumbnail());
                goodsErrorList.add(errorMap);
            }
            throw new ServiceException(TradeErrorCode.E461.code(), "商品不在配送区域", goodsErrorList);
        }


        return this;
    }

    @Override
    public TradeCreator checkGoods() {

        Assert.notNull(goodsClient, "goodsClient为空，请先调用setGoodsClient设置正确的商品业务Client");


        //已选中结算的商品
        List<CartVO> cartList = cartView.getCartList();

        //1、检测购物车是否为空
        if (cartList == null || cartList.isEmpty()) {
            throw new ServiceException(TradeErrorCode.E452.code(), "购物车为空");
        }
        //验证后存在商品问题的集合
        List<Map> goodsErrorList = new ArrayList();

        boolean flag = true;
        //遍历购物车集合
        for (CartVO cartVO : cartList) {

            List<CartSkuVO> skuList = cartVO.getSkuList();
            for (CartSkuVO cartSkuVO : skuList) {
                Map errorMap = new HashMap(16);
                errorMap.put("name", cartSkuVO.getName());
                errorMap.put("image", cartSkuVO.getGoodsImage());

                Integer skuId = cartSkuVO.getSkuId();
                GoodsSkuVO skuVO = this.goodsClient.getSkuFromCache(skuId);

                //检测商品是否存在
                if (skuVO == null) {
                    goodsErrorList.add(errorMap);
                    continue;
                }

                Integer goodsId = skuVO.getGoodsId();
                CacheGoods goodsVo = this.goodsClient.getFromCache(goodsId);
                if (goodsVo.getIsAuth() == 0 || goodsVo.getIsAuth() == 3) {
                    goodsErrorList.add(errorMap);
                    continue;
                }

                //检测商品的上下架状态
                if (goodsVo.getMarketEnable() != null && goodsVo.getMarketEnable().intValue() != 1) {
                    goodsErrorList.add(errorMap);
                    continue;
                }

                //检测商品的删除状态
                //if (goodsVo.getDisabled() != null && goodsVo.getDisabled().intValue() != 1) {
                //    goodsErrorList.add(errorMap);
                //    continue;
                //}



                //读取此产品的可用库存数量
                int enableQuantity = skuVO.getEnableQuantity();
                //此产品将要购买的数量
                int num = cartSkuVO.getNum();

                //虚拟商品一次只能下单一个sku,且sku的数量为1
                //String goodsType = goodsVo.getGoodsType() == null ? GoodsType.NORMAL.name() : goodsVo.getGoodsType();
                //if (goodsType.equals(GoodsType.VIRTUAL.name())) {
                //    if(num != 1){
                //        goodsErrorList.add(errorMap);
                //    }
                //    break;
                //}

                //如果将要购买的产品数量大于redis中的数量，则此产品不能下单
                if (num > enableQuantity) {
                    flag = false;
                    goodsErrorList.add(errorMap);
                    continue;
                }
            }
            // 如果当前购买的商品为虚拟商品，则只循环一次
            //if (!CollectionUtils.isEmpty(skuList) && GoodsType.VIRTUAL.name().equals(skuList.get(0).getGoodsType())) {
            //    break;
            //}
        }

        if (!goodsErrorList.isEmpty()) {
            StringBuilder errorGoodsName = new StringBuilder();
            for (Map map : goodsErrorList) {
                String name = (String) map.get("name");
                errorGoodsName.append(name);
            }
            throw new ServiceException(TradeErrorCode.E452.code(), "抱歉，您以下商品所在地区无货:"+errorGoodsName.toString(), JsonUtil.objectToJson(goodsErrorList));
        }

        return this;
    }


    @Override
    public TradeCreator checkPromotion() {
        Assert.notNull(memberClient, "memberClient为空，请先调用setMemberClient设置正确的会员业务Client");

        List<CartVO> cartList = cartView.getCartList();

        for (CartVO cartVO : cartList) {

            List<CartSkuVO> skuList = cartVO.getSkuList();

            for (CartSkuVO cartSkuVO : skuList) {
                innerCheckPromotion(cartSkuVO);
            }
        }


        //读取订单的总交易价格信息
        PriceDetailVO detailVO = cartView.getTotalPrice();

        //此交易需要扣除用户的积分
        int point = detailVO.getExchangePoint();

        if (point > 0) {
            Buyer buyer = UserContext.getBuyer();
            Member member = this.memberClient.getModel(buyer.getUid());
            int consumPoint = member.getConsumPoint();

            //如果用户可使用的消费积分小于 交易需要扣除的积分时，则不能下单
            if (consumPoint < point) {
                //update by liuyulei 2019-07-17  修改bug,立即购买不经过购物车，错误信息返回:"您可使用的消费积分不足"
                throw new ServiceException(TradeErrorCode.E452.code(), "您可使用的消费积分不足");
            }
        }

        return this;
    }


    private void innerCheckPromotion(CartSkuVO  cartSkuVO){

        Map errorMap = new HashMap(16);
        errorMap.put("name", cartSkuVO.getName());
        errorMap.put("image", cartSkuVO.getGoodsImage());

        //验证后存在促销活动问题的集合
        List<Map> promotionErrorList = new ArrayList();
        boolean flag = true;
        //此商品参与的单品活动
        List<CartPromotionVo> singlePromotionList = cartSkuVO.getSingleList();
        if (!singlePromotionList.isEmpty()) {
            for (CartPromotionVo promotionGoodsVO : singlePromotionList) {

                // 默认参与的活动 && 非不参与活动的状态
                if (promotionGoodsVO.getIsCheck().intValue() == 1 && !promotionGoodsVO.getPromotionType().equals(PromotionTypeEnum.NO.name())) {
                    //当前活动的失效时间
                    long entTime = promotionGoodsVO.getEndTime();

                    //当前时间
                    long currTime = DateUtil.getDateline();

                    //如果当前时间大于失效时间，则此活动已经失效了，不能下单
                    if (currTime > entTime) {
                        flag = false;
                    }
                    if(promotionGoodsVO.getPromotionType().equals(PromotionTypeEnum.SECKILL.name())){
                        SeckillGoodsVO seckillGoodsVO = seckillGoodsManager.getSeckillById(promotionGoodsVO.getActivityId());
                        if(!seckillGoodsVO.getSalesEnable()){
                           throw new ServiceException("1000",seckillGoodsVO.getGoodsName()+"_秒杀库存已售罄！");
                        }
                    }
                    if(!flag){
                        promotionErrorList.add(errorMap);

                    }
                }

            }
        }

        //此商品参与的组合活动
        List<CartPromotionVo> groupPromotionList = cartSkuVO.getGroupList();
        if (!groupPromotionList.isEmpty()) {
            for (CartPromotionVo cartPromotionGoodsVo : groupPromotionList) {
                //当前活动的失效时间
                long entTime = cartPromotionGoodsVo.getEndTime();

                //当前时间
                long currTime = DateUtil.getDateline();

                //如果当前时间大于失效时间，则此活动已经失效了，不能下单
                if (currTime > entTime) {
                    flag = false;

                    promotionErrorList.add(errorMap);
                    continue;
                }
            }
        }
    }


}
