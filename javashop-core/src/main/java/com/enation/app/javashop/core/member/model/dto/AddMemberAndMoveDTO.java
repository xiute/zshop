package com.enation.app.javashop.core.member.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *  迁移老账号至新账号接口DTO
 */
@Data
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AddMemberAndMoveDTO {

    /**
     * 新账号用户名
     */
    @ApiModelProperty(name = "uname", value = "新账号用户名", required = true)
    private String uname;
    /**
     * 新账号密码
     */
    @ApiModelProperty(name = "password", value = "新账号密码", required = true)
    private String password;
    /**
     * 新账号昵称
     */
    @ApiModelProperty(name = "nickname", value = "新账号昵称", required = true)
    private String nickname;
    /**
     * 老会员id
     */
    @ApiModelProperty(name = "old_member_id", value = "老会员id", required = true)
    private Integer oldMemberId;

}
