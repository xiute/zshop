package com.enation.app.javashop.core.excel;

import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.trade.order.model.vo.OrderLineVO;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.BeanUtil;
import com.google.common.collect.Maps;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author JFeng
 * @date 2020/7/11 10:51
 */
public class ExcelDataConverter {

    /***
     * 调度单数据转换为excel格式数据
     */
    public static List<Map> convertGoodsList(List<Map> goodsList) {
        for (Map goods : goodsList) {
            goods.put("category_first", "测试");
        }
        return goodsList;
    }

}
