package com.enation.app.javashop.core.geo.model;

import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2019/10/22 10:43
 */
@Data
public class TencentParseLatLngResponse {
    private String status;
    private TencentLocation result;
}

