package com.enation.app.javashop.core.member.service;

import com.enation.app.javashop.core.member.model.dos.MemberVisitDataDO;

/**
 * @Author: zhou
 * @Date: 2020/9/14
 * @Description:
 */
public interface MemberVisitDataManager {
    void add(long time);
}
