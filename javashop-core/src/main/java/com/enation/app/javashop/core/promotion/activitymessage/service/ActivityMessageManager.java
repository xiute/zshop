package com.enation.app.javashop.core.promotion.activitymessage.service;

import com.enation.app.javashop.core.promotion.activitymessage.model.dos.ActivityMessageDO;
import com.enation.app.javashop.core.promotion.activitymessage.model.dos.ActivityMessageRecordDO;
import com.enation.app.javashop.core.promotion.activitymessage.model.vo.ActivityMessageRecordVO;

import java.util.List;

public interface ActivityMessageManager {


    /**
     * 添加活动的推送信息
     */
    void addActivityMessage(ActivityMessageDO activityMessageDO);

    /**
     * 记录活动订阅
     */
    void addActivityMessageRecordVO(ActivityMessageRecordVO activityMessageRecordVO);

    /**
     * 根据活动id查询 订阅的人
     */
    List<ActivityMessageRecordDO> getActivityMessageRecordDO(Integer activityId);

    /**
     * 根据活动id 查询推送消息
     */
    List<ActivityMessageDO> getActivityMessageDO(Integer activityId);
    /**
     * 查看当前人是否订阅了最新的活动
     */
   Integer getSubscribeActivity(Integer memberId,String city);

}
