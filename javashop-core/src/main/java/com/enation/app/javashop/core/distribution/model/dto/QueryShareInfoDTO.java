package com.enation.app.javashop.core.distribution.model.dto;

import lombok.Data;

@Data
public class QueryShareInfoDTO {

    // 主键
    private Integer id;

    // 分享类型 1商品 2店铺
    private Integer shareType;

    // 类型 1小程序
    private Integer channelType;

}
