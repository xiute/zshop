package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/5/27
 * @Description: 查询订单列表
 */
@Data
public class YiSuPaiQueryOrder {
    // 当前页
    private Integer currentPage;
    // 每页几条
    private Integer pageSize;
    // 排序列
    private String orderColumn;
    // 排序方式(0正序 1倒序)
    private String orderType;

    private String orderNo;

    // 订单状态:0-已取消，10-未付款，20-已付款，40-已发货，50-交易成功，60-交易关闭
    private Integer status;
}
