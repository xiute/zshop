package com.enation.app.javashop.core.promotion.activitymessage.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 活动消息记录表
 * @author xlg
 * 2020/11/05 17:51
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ActivityMessageRecordVO {

    /**
     * id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;

    /**
     * 活动id
     */
    @Column(name = "activity_id")
    @ApiModelProperty(name = "activity_id", value = "活动id", required = true)
    private Integer activityId;

    /**
     * 会员Id
     */
    @Column(name = "member_id")
    @ApiModelProperty(name = "member_id", value = "会员Id", required = false)
    private Integer memberId;

    /**
     * 活动消息Id
     */
    @Column(name = "message_id")
    @ApiModelProperty(name = "message_id", value = "活动消息Id", required = true)
    private Integer messageId;

    /**
     * 小程序唯一标识
     */
    @Column(name = "open_id")
    @ApiModelProperty(name = "open_id", value = "小程序唯一标识", required = true)
    private String openId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(name = "create_time", value = "创建时间", required = false)
    private Long createTime;

    /**
     * 所在的城市
     */
    @Column(name = "city")
    @ApiModelProperty(name = "city", value = "所在的城市", required = true)
    private String city;
}
