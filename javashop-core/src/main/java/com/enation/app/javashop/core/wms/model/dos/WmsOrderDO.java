package com.enation.app.javashop.core.wms.model.dos;

import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.enation.app.javashop.core.wms.model.enums.WmsOrderStatusEnums;
import com.enation.app.javashop.core.wms.model.enums.WmsOrderTypeEnums;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author JFeng
 * @date 2020/9/21 17:32
 */

@Table(name = "wms_order")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class WmsOrderDO {
    /** 主键 */
    @Id(name = "delivery_id")
    @ApiModelProperty(hidden = true)
    private Integer deliveryId;

    @Column(name = "delivery_sn")
    @ApiModelProperty(name = "delivery_sn", value = "出库编号", required = false)
    private String deliverySn;

    /**
     * {@link WmsOrderStatusEnums}
     */
    @Column(name = "delivery_status")
    @ApiModelProperty(name = "delivery_status", value = "出库状态", required = false)
    private String deliveryStatus;

    @Column(name = "claims_id")
    @ApiModelProperty(name = "claims_id", value = "理赔单id", required = false)
    private Integer claimsId;

    @Column(name = "order_id")
    @ApiModelProperty(name = "order_id", value = "订单id", required = false)
    private Integer orderId;

    @Column(name = "order_sn")
    @ApiModelProperty(name = "order_sn", value = "订单编号", required = false)
    private String orderSn;

    @Column(name = "ship_name")
    @ApiModelProperty(name = "ship_name", value = "收货人", required = false)
    private String shipName;

    @Column(name = "ship_mobile")
    @ApiModelProperty(name = "ship_mobile", value = "收货人手机号", required = false)
    private String shipMobile;

    @Column(name = "ship_addr")
    @ApiModelProperty(name = "ship_addr", value = "收货地址", required = false)
    private String shipAddr;

    @Column(name = "site_addr")
    @ApiModelProperty(name = "site_addr", value = "站点地址", required = false)
    private String siteAddr;

    @Column(name = "site_name")
    @ApiModelProperty(name = "site_name", value = "站点联系人", required = false)
    private String siteName;

    @Column(name = "site_mobile")
    @ApiModelProperty(name = "site_mobile", value = "站点手机号", required = false)
    private String siteMobile;

    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "店铺id", required = false)
    private Integer sellerId;

    @Column(name = "site_id")
    @ApiModelProperty(name = "site_id", value = "站点id", required = false)
    private Integer siteId;

    /**
     * {@link  ShipTypeEnum}
     */
    @Column(name = "shipping_type")
    @ApiModelProperty(name = "shipping_type", value = "配送类型", required = false)
    private String shippingType;

    /**
     * {@link  WmsOrderTypeEnums}
     */
    @Column(name = "order_type")
    @ApiModelProperty(name = "order_type", value = "上游来源类型 NORMAL 商城 SHETUAN 社团 EXCEPTION 异常单", required = false)
    private String orderType;

    @Column(name = "items_json")
    @ApiModelProperty(name = "items_json", value = "货物列表json", required = false)
    private String itemsJson;

    @Column(name = "delivery_date")
    @ApiModelProperty(name = "delivery_date", value = "出库日期", required = false)
    private Integer deliveryDate;

    @Column(name = "delivery_time")
    @ApiModelProperty(name = "delivery_time", value = "实际出库时间", required = false)
    private Long deliveryTime;

    @Column(name = "warehouse_code")
    @ApiModelProperty(name = "warehouse_code", value = "仓库分拣单号", required = false)
    private String warehouseCode;

    @Column(name = "print_times")
    @ApiModelProperty(name = "print_times", value = "打印次数", required = false)
    private Integer printTimes;

    @Column(name = "remarks")
    @ApiModelProperty(name = "remarks", value = "出库备注", required = false)
    private String remarks;

    @Column(name = "sorting_seq")
    private Integer sortingSeq;

    @Column(name = "sorting_batch")
    private Integer sortingBatch;

    @Column(name = "operator_name")
    private String operatorName;

    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "dispatch_type")
    private String dispatchType;

    @Column(name = "receive_time")
    private Long receiveTime;

    @Column(name = "receive_time_type")
    private Integer receiveTimeType;

    @Column(name = "order_num")
    private Integer orderNum;

    @Column(name = "order_seq")
    private Integer orderSeq;

    @Column(name = "sorting_seq_list")
    private String sortingSeqList;

}
