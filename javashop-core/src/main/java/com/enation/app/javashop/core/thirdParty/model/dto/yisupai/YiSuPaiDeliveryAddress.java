package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/5/24
 * @Description:
 */
@Data
public class YiSuPaiDeliveryAddress {
    @ApiModelProperty(value = "收货地址")
    private String addressDetail;
    @ApiModelProperty(value = "街道上一级的divisionCode")
    private String divisionCode;
    @ApiModelProperty(value = "收货人姓名")
    private String fullName;
    @ApiModelProperty(value = "收货人电话")
    private String mobilel;

    public YiSuPaiDeliveryAddress(){

    }

    public YiSuPaiDeliveryAddress(OrderDO order){
        this.addressDetail=order.getShipName();
        this.mobilel=order.getShipMobile();
        this.addressDetail=order.getShipAddr();
    }
}
