package com.enation.app.javashop.core.aftersale.model.vo;

import com.enation.app.javashop.core.aftersale.model.enums.ExceptionClaimsCodeEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2020年9月21日 14:00:50
 * 返回前端实体
 */
@Data
@ApiModel(value = "返回结果实体")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public  class Result<T> implements Serializable {

    private static final long serialVersionUID = 4286785219101103701L;

    @ApiModelProperty(name = "code",value = "返回状态码")
    private int code;
    @ApiModelProperty(name = "message",value = "提示信息")
    private String message;
    @ApiModelProperty(name = "data",value = "返回值")
    private T data;

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }

    public Result() {
    }

    public Result(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> Result success(T data){
        return new Result<>(ExceptionClaimsCodeEnum.S200.getCode(), ExceptionClaimsCodeEnum.S200.getDescription(), data);
    }

    public static Result successMessage(){
        return new Result(ExceptionClaimsCodeEnum.S200.getCode(), ExceptionClaimsCodeEnum.S200.getDescription());
    }
}
