package com.enation.app.javashop.core.shop.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * 定时达时间配置
 * @author JFeng
 * @date 2020/11/23 15:01
 *
 * DEMO
 * {
 *     "areaSetting": "定海区,普陀区",
 *     "timeSections": [
 *         {
 *             "seq": 1,
 *             "endOrderTime": 1100,
 *             "receiveTime": 1700,
 *             "halfDay": "PM",
 *             "receiveTimeType": 1
 *         },
 *         {
 *             "seq": 2,
 *             "endOrderTime": 2400,
 *             "receiveTime": 1100,
 *             "halfDay": "AM",
 *             "receiveTimeType": 0
 *         },
 *         {
 *             "seq": 3,
 *             "endOrderTime": 2400,
 *             "receiveTime": 1700,
 *             "halfDay": "PM",
 *             "receiveTimeType": 0
 *         },
 *
 *     ],
 *     "defaultReceiveTime": 1700,
 *     "exceptDay": 7
 * }
 *
 */
@Data
public class FixTimeDispatchSetting implements Serializable {
    private static final long serialVersionUID = 5739331716616202222L;

    private String areaSetting;

    private List<FixTimeSection> timeSections;

    private FixTimeSection defaultTimeSection;

    private Integer exceptDay;

    private ExceptDaySetting exceptDayOfWeek;




}

