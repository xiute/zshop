package com.enation.app.javashop.core.thirdParty.model.dto;

import lombok.Data;

import java.util.List;
//商品分类表
@Data
public class Category {
    //分类的id
    private Integer id;
    //所属⽗级分类ID
    private Integer fromId;
    // 排序
    private Integer sort;
    private Integer type;
    //子类
    private List<Category> children;
    //分类图⽚地址
    private String imgUrl;
    //前端图⽚展示名称（可忽略）
    private String frontName;
    // 前端图⽚展示地址（可忽略）
    private String frontImgUr;
    //分类的名称
    private String name;
    //创建时间
    private String createTime;

    public Category(){

    }

    public Category(Integer id, Integer fromId, Integer sort, Integer type, List<Category> children, String imgUrl, String frontName, String frontImgUr, String name, String createTime) {
        this.id = id;
        this.fromId = fromId;
        this.sort = sort;
        this.type = type;
        this.children = children;
        this.imgUrl = imgUrl;
        this.frontName = frontName;
        this.frontImgUr = frontImgUr;
        this.name = name;
        this.createTime = createTime;
    }


}
