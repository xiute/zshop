package com.enation.app.javashop.core.goods.service;

import com.enation.app.javashop.core.goods.model.dto.SupplierDTO;
import com.enation.app.javashop.core.goods.model.vo.SupplierVO;
import com.enation.app.javashop.framework.database.Page;

/**
 * 供应商业务层
 * @author xlg
 * @version v1.0
 * 2020-12-29 18:26
 */
public interface SupplierManager {
    /**
     * 查询供应商列表
     */
    Page<SupplierVO> querySupplierList(SupplierVO supplierVO);

    /**
     * 添加
     */
    void addSupplier(SupplierVO supplierDTO);

    /**
     * 修改
     */
    void updateSupplier(SupplierVO supplierDTO);

    /**
     * 删除
     */
    void delSupplier(Integer id);
}
