package com.enation.app.javashop.core.goods.model.enums;

public enum SharePageType {
    GOODS_DETAIL(1,"商品详情"),
    PAGE(1,"页面");

    private Integer  value;
    private String name;

    SharePageType(Integer value , String name){
        this.name = name;
        this.value= value;
    }

    public Integer  getValue(){
        return this.value;
    }
    public String  getName(){
        return this.name;
    }
}
