package com.enation.app.javashop.core.pagedata.model.enums;

/**
 * @author john
 * @Description 页面类型
 * @ClassName PageTypeEnum
 */
public enum PageTypeEnum {
    //首页
    INDEX("平台首页"),
    //首页
    CITY_INDEX("城市首页"),
    //微信
    DEFINED("自定义");

    private String description;

    PageTypeEnum(String des) {
        this.description = des;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
