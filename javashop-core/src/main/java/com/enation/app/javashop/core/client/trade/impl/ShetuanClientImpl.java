package com.enation.app.javashop.core.client.trade.impl;

import com.enation.app.javashop.core.client.trade.ShetuanClient;
import com.enation.app.javashop.core.goodssearch.service.ShetuanSearchManager;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanOrderDO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.ShetuanOrderStatisticVO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.ShetuanOrderVO;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanGoodsManager;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanManager;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanOrderManager;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.dto.ShetuanOrderQueryParam;
import com.enation.app.javashop.framework.database.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
@ConditionalOnProperty(value = "javashop.product", havingValue = "stand")
public class ShetuanClientImpl implements ShetuanClient {
    @Autowired
    private ShetuanManager shetuanManager;
    @Autowired
    private ShetuanGoodsManager shetuanGoodsManager;
    @Autowired
    private ShetuanSearchManager sheTuanSearchManager;
    @Autowired
    private ShetuanOrderManager shetuanOrderManager;

    @Override
    public List<ShetuanDO> get(String status) {
        return shetuanManager.get(status);
    }

      @Override
    public boolean deleteByShetuanId(Integer shetuanId) {
       return sheTuanSearchManager.deleteByShetuanId(shetuanId);
    }


    @Override
    public List<ShetuanGoodsDO> querySheuanGoods(Integer shetuanId, List<Integer> skuIds) {
        return shetuanGoodsManager.queryShetuanGoodsBySkuIds(shetuanId,skuIds);
    }

    @Override
    public List<ShetuanGoodsDO> querySheuanGoodsByGoodsIds(Integer[] goodsIds) {
        return shetuanGoodsManager.querySheuanGoodsByGoodsIds(goodsIds);
    }

    @Override
    public boolean addIndex(ShetuanGoodsVO shetuanGoodsVO, Map<String, Object> goods) {
        sheTuanSearchManager.addIndex(shetuanGoodsVO,goods);
        return true;
    }

    @Override
    public boolean delIndex(Integer shetuanId,Integer skuId) {
        sheTuanSearchManager.delIndex(shetuanId,skuId);
        return true;
    }

    @Override
    public ShetuanOrderDO confirm(OrderDO orderDO) {
        return shetuanOrderManager.confirm(orderDO);
    }

    @Override
    public Page<ShetuanOrderStatisticVO> statistic(ShetuanOrderQueryParam shetuanOrderQueryParam)  {
      return   shetuanOrderManager.statistic(shetuanOrderQueryParam);
    }

    @Override
    public ShetuanOrderStatisticVO statisticAll(ShetuanOrderQueryParam shetuanOrderQueryParame) {
      return   shetuanOrderManager.statisticAll(shetuanOrderQueryParame);
    }

    @Override
    public ShetuanDO queryShetuanById(Integer shetuanId) {
        return shetuanManager.getShetuanById(shetuanId);
    }

    @Override
    public  Page<ShetuanOrderVO> queryShetuanOrders(ShetuanOrderQueryParam shetuanOrderQueryParam) {
        Page<ShetuanOrderVO> page = shetuanOrderManager.queryPage(shetuanOrderQueryParam);
        return page;
    }

}
