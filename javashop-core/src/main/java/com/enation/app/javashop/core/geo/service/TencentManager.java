package com.enation.app.javashop.core.geo.service;


import com.enation.app.javashop.core.geo.model.Pois;
import com.enation.app.javashop.core.geo.model.TencentLocation;

import java.util.List;

/**
 * @author 96556
 * @since 2019-06-30
 */
public interface TencentManager {

    TencentLocation parseLatLng(Double lng, Double lat);

    List<Pois> inputtips(String keyword,String region);

    double countDrivingDistance(String origin, String destination);

}
