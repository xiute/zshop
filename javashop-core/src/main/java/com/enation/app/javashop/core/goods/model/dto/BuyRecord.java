package com.enation.app.javashop.core.goods.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author JFeng
 * @date 2020/6/12 19:57
 */

@Data
public class BuyRecord implements Serializable {
    private static final long serialVersionUID = -3642358108471082387L;
    private String memberFace;
    private String memberName;
    private Long buyTime;
    private Integer buyNum;
}
