package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2020/12/1
 * @Description: 导入批量发货的模板
 */
@Data
public class ShipOrderImport {

    //订单编号
    @ExcelProperty(value = "订单编号", index = 0)
    private String orderSn;
    //快递公司
    @ExcelProperty(value = "快递公司", index = 1)
    private String logiName;
    //快递单号
    @ExcelProperty(value = "快递单号", index = 2)
    private String shipNo;


}
