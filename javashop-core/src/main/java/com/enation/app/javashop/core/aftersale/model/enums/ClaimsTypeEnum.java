package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * @Author: zhou
 * @Date: 2020/9/21
 * @Description: 理赔类型枚举
 */
public enum ClaimsTypeEnum {

    COMPENSATION_GOODS("补发商品"),

    FREE_GIFT("赠送礼品"),

    PAYMENT_CASH("赔付现金"),

    REFUND_CASH("多退少补"),

    AFTERSALE_COUPON("售后券"),

    OTHER("其它");

    private String description;

    ClaimsTypeEnum(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
