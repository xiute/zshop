package com.enation.app.javashop.core.shop.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class TemplateTimeSetting implements Serializable {
    private static final long serialVersionUID = 6348162955890093L;

    @ApiParam("时间配置类型")
    private Integer  timeType;
    @ApiParam("基础距离")
    private BigDecimal baseDistance;
    @ApiParam("基础价格")
    private BigDecimal  baseTime;
    @ApiParam("单位距离")
    private BigDecimal  unitDistance;
    @ApiParam("单位费用")
    private BigDecimal  unitTime;
    @ApiParam("基础距离")
    private List<TimeSection> timeSections;

}
