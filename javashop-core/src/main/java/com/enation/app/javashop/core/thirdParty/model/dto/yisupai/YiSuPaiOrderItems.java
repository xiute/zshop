package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/5/24
 * @Description:
 */
@Data
public class YiSuPaiOrderItems {
    @ApiModelProperty(value = "商品ID")
    private String itemId;
    @ApiModelProperty(value = "购买数量")
    private Integer quantity;
    @ApiModelProperty(value = "skuId")
    private String skuId;
}
