package com.enation.app.javashop.core.wms.service;

import com.enation.app.javashop.core.wms.model.dto.WmsOrderQueryParam;

/**
 * @author JFeng
 * @date 2020/9/23 18:01
 */
public interface WmsOrderExcelManager {
    void exportSkuOrderList(WmsOrderQueryParam wmsOrderQueryParam);

    void exportSkuSupplierList(WmsOrderQueryParam wmsOrderQueryParam);

    void exportOrderSortingList(WmsOrderQueryParam wmsOrderQueryParam);

    void exportBModelList(WmsOrderQueryParam wmsOrderQueryParam);
}
