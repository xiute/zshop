package com.enation.app.javashop.core.system.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 小程序消息订阅DTO
 */
@Data
public class MiniproMsgDataDTO implements Serializable{

    private String value;

    public MiniproMsgDataDTO(){}

    public static MiniproMsgDataDTO build(String value){
        if(value.length() > 20){
            value = value.substring(0, 17) + "...";
        }

        MiniproMsgDataDTO miniproMsgDataDTO = new MiniproMsgDataDTO();
        miniproMsgDataDTO.setValue(value);
        return miniproMsgDataDTO;
    }

}
