package com.enation.app.javashop.core.distribution.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2021/1/22 17:32
 * 团长任务VO
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class DistributionMissionVO implements Serializable {

    /**
     * 团长任务ID
     */
    @ApiModelProperty(name = "id", value = "团长任务ID")
    private Integer id;
    /**
     * 团长任务名称
     */
    @ApiModelProperty(name = "mission_name", value = "团长任务名称")
    @NotBlank(message = "请输入团长任务名称")
    private String missionName;
    /**
     * 团长任务副标题
     */
    @ApiModelProperty(name = "mission_subtitle", value = "团长任务副标题")
    @NotBlank(message = "请输入团长任务副标题")
    private String missionSubtitle;
    /**
     * 团长任务状态
     */
    @ApiModelProperty(name = "mission_status", value = "团长任务状态")
    private Integer missionStatus;
    /**
     * 团长任务状态文本
     */
    @ApiModelProperty(name = "mission_status_text", value = "团长任务状态文本")
    private String missionStatusText;
    /**
     * 店铺ID
     */
    @ApiModelProperty(name = "seller_id", value = "店铺ID")
    private Integer sellerId;
    /**
     * 店铺名称
     */
    @ApiModelProperty(name = "seller_name", value = "店铺名称")
    private String sellerName;
    /**
     * 开始时间
     */
    @ApiModelProperty(name = "start_time", value = "开始时间")
    @NotNull(message = "请输入开始时间")
    private Long startTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(name = "end_time", value = "结束时间")
    @NotNull(message = "请输入结束时间")
    private Long endTime;
    /**
     * 奖励类型（0：现金奖励，1：优惠券奖励）
     */
    @ApiModelProperty(name = "rewards_type", value = "奖励类型（0：现金奖励，1：优惠券奖励）")
    @NotNull(message = "请选择奖励类型")
    private Integer rewardsType;
    /**
     * 奖励类型文本
     */
    @ApiModelProperty(name = "rewards_type_text", value = "奖励类型文本")
    private String rewardsTypeText;
    /**
     * 团长任务活动封面
     */
    @ApiModelProperty(name = "cover_img", value = "团长任务活动封面")
    private String coverImg;
    /**
     * 团长活动背景图
     */
    @ApiModelProperty(name = "background_img", value = "团长活动背景图")
    private String backgroundImg;
    /**
     * 团长任务描述
     */
    @ApiModelProperty(name = "mission_desc", value = "团长任务描述")
    private String missionDesc;
    /**
     * 创建人
     */
    @ApiModelProperty(name = "create_name", value = "创建人")
    private String createName;
    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long createTime;
    /**
     * 修改人
     */
    @ApiModelProperty(name = "update_name", value = "修改人")
    private String updateName;
    /**
     * 修改时间
     */
    @ApiModelProperty(name = "update_time", value = "修改时间")
    private Long updateTime;

}
