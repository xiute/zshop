package com.enation.app.javashop.core.trade.order.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/6/6 15:41
 */

@Data
public class ShetuanOrderQueryParam {

    @ApiModelProperty(value = "第几页")
    private Integer pageNo;

    @ApiModelProperty(value = "每页条数")
    private Integer pageSize;

    @ApiModelProperty(value = "团长会员Id")
    private Integer leaderMemberId;

    @ApiModelProperty(value = "开始时间")
    private Long startTime;

    @ApiModelProperty(value = "结束时间")
    private Long endTime;

    @ApiModelProperty(value = "统计类型", example = "0:按天统计,1:按月统计")
    private Integer type;

    private Integer sellerId;

    @ApiModelProperty(value = "统计类型", example = "0:按天统计,1:按月统计")
    private List<String> orderFields;

    private String tag;

    @ApiModelProperty(value = "订单号")
    private String orderSn;

    @ApiModelProperty(value = "会员id")
    private String memberId;

    @ApiModelProperty(value = "订单状态")
    private String orderStatus;

    @ApiModelProperty(value = "自提站点名称")
    private String siteName;

    @ApiModelProperty(value = "站长姓名")
    private String leaderName;

    @ApiModelProperty(value = "所属团长")
    private String leaderTeam;

    @ApiModelProperty(value = "收货人姓名")
    private String shipName;

    @ApiModelProperty(value = "收货人电话")
    private String shipMobile;

    @ApiModelProperty(value = "关键字")
    private String keyword;

    @ApiModelProperty(value = "配送方式")
    private String shippingType;

    @ApiModelProperty(value = "小票打印状态")
    private String printTimes;

    @ApiModelProperty(value = "会员昵称")
    private String memberNickName;



}
