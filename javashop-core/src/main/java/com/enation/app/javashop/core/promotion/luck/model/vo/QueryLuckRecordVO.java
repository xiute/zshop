package com.enation.app.javashop.core.promotion.luck.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author 孙建
 * 抽奖活动记录VO
 */
@Data
public class QueryLuckRecordVO implements Serializable {

    @ApiModelProperty(value = "页码", name = "page_no", required = true)
    @NotNull(message = "请输入当前页码")
    private Integer pageNo;

    @ApiModelProperty(value = "分页大小", name = "page_size", required = true)
    @NotNull(message = "请输入分页大小")
    private Integer pageSize;
    /**
     * 抽奖记录id
     */
    private Integer luckRecordId;
    /**
     * 开始时间
     */
    private Long startCreateTime;
    /**
     * 结束时间
     */
    private Long endCreateTime;
    /**
     * 店铺ID
     */
    private Integer sellerId;
    /**
     * 店铺名称
     */
    private String sellerName;
    /**
     * 抽奖活动ID
     */
    private Integer luckId;
    /**
     * 会员id
     */
    private Integer memberId;
    /**
     * 会员昵称
     */
    private String memberNickname;
    /**
     * 会员真实姓名
     */
    private String memberRealName;
    /**
     * 奖品名称
     */
    private String prizeName;
    /**
     * 奖品类型
     */
    private Integer prizeType;
    /**
     * 兑奖方式
     */
    private Integer exchangeType;
    /**
     * 兑奖状态
     */
    private Integer exchangeStatus;
    /**
     * 是否中奖
     */
    private Integer isHit;
    /**
     * 出库类型
     */
    private Integer deliveryType;

}