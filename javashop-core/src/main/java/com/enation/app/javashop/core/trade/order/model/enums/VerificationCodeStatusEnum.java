package com.enation.app.javashop.core.trade.order.model.enums;

/**
 * @author 王志杨
 * @since 2020年12月29日
 * 核销码状态枚举
 */
public enum VerificationCodeStatusEnum {

    UNUSED(1, "未使用"),
    USED(2, "已使用"),
    EXPIRED(-1, "已过期"),
    CANCELLED(-2, "已取消");

    private int index;
    private String describe;

    VerificationCodeStatusEnum(int index, String describe) {
        this.index = index;
        this.describe = describe;
    }

    public int getIndex() {
        return index;
    }

    public String getDescribe() {
        return describe;
    }
}
