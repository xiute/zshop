package com.enation.app.javashop.core.trade.order.model.dto;

import com.enation.app.javashop.core.member.model.dos.ReceiptHistory;
import com.enation.app.javashop.core.trade.cart.model.dos.CartDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderTypeEnum;
import com.enation.app.javashop.core.trade.order.model.vo.ConsigneeVO;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import springfox.documentation.annotations.ApiIgnore;

import java.io.Serializable;

/**
 * 订单DTO
 *
 * @author kingapex
 * @version 1.0
 * @since v7.0.0 2017年3月22日下午9:28:30
 */
@SuppressWarnings("AlibabaPojoMustOverrideToString")
@ApiIgnore
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class OrderDTO extends CartDO implements Serializable {

    private static final long serialVersionUID = 8206833000476657708L;

    @ApiModelProperty(hidden = true)
    private Integer orderId;

    @ApiModelProperty(value = "交易编号")
    private String tradeSn;

    @ApiModelProperty(value = "订单编号")
    private String sn;

    @ApiModelProperty(value = "收货信息")
    private ConsigneeVO consignee;

    @ApiModelProperty(value = "配送方式")
    private Integer shippingId;

    @ApiModelProperty(value = "支付方式")
    private String paymentType;

    @ApiModelProperty(value = "发货时间")
    private Long shipTime;

    @ApiModelProperty(value = "客订收货时间")
    private Long receiveTime;

    @ApiModelProperty(value = "客订收货时间类型")
    private Integer receiveTimeType;

    @ApiModelProperty(value = "会员id")
    private Integer memberId;

    @ApiModelProperty(value = "会员姓名")
    private String memberName;

    @ApiModelProperty(value = "订单备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;


    @ApiModelProperty(value = "配送方式名称")
    private String shippingType;

    @ApiModelProperty(value = "订单状态")
    private String orderStatus;

    @ApiModelProperty(value = "付款状态")
    private String payStatus;

    @ApiModelProperty(value = "配送状态")
    private String shipStatus;

    @ApiModelProperty(value = "收货人姓名")
    private String shipName;

    @ApiModelProperty(value = "收货人电话")
    private String shipMobile;

    @ApiModelProperty(value = "订单价格")
    private Double orderPrice;

    @ApiModelProperty(value = "配送费")
    private Double shippingPrice;

    @ApiModelProperty(value = "评论状态")
    private String commentStatus;

    @ApiModelProperty(value = "是否已经删除")
    private Integer disabled;

    @ApiModelProperty(value = "支付方式id")
    private Integer paymentMethodId;

    @ApiModelProperty(value = "支付插件id")
    private String paymentPluginId;

    @ApiModelProperty(value = "支付方式名称")
    private String paymentMethodName;

    @ApiModelProperty(value = "付款账号")
    private String paymentAccount;

    @ApiModelProperty(value = "商品数量")
    private Integer goodsNum;

    @ApiModelProperty(value = "发货仓库id")
    private Integer warehouseId;

    @ApiModelProperty(value = "取消原因")
    private String cancelReason;

    @ApiModelProperty(value = "收货地址省Id")
    private Integer shipProvinceId;

    @ApiModelProperty(value = "收货地址市Id")
    private Integer shipCityId;

    @ApiModelProperty(value = "收货地址区Id")
    private Integer shipRegionId;

    @ApiModelProperty(value = "收货地址街道Id")
    private Integer shipTownId;

    @ApiModelProperty(value = "收货省")
    private String shipProvince;

    @ApiModelProperty(value = "收货地址市")
    private String shipCity;

    @ApiModelProperty(value = "收货地址区")
    private String shipRegion;

    @ApiModelProperty(value = "收货地址街道")
    private String shipTown;

    @ApiModelProperty(value = "签收时间")
    private Long signingTime;

    @ApiModelProperty(value = "签收人姓名")
    private String theSign;

    @ApiModelProperty(value = "管理员备注")
    private String adminRemark;

    @ApiModelProperty(value = "收货地址id")
    private Integer addressId;

    @ApiModelProperty(value = "应付金额")
    private Double needPayMoney;

    @ApiModelProperty(value = "发货单号")
    private String shipNo;

    @ApiModelProperty(value = "物流公司Id")
    private Integer logiId;

    @ApiModelProperty(value = "物流公司名称")
    private String logiName;

    @ApiModelProperty(value = "是否需要发票")
    private Integer needReceipt;

    @ApiModelProperty(value = "抬头")
    private String receiptTitle;

    @ApiModelProperty(value = "内容")
    private String receiptContent;

    @ApiModelProperty(value = "售后状态")
    private String serviceStatus;

    @ApiModelProperty(value = "订单来源")
    private String clientType;
    @ApiModelProperty(value = "发票信息")
    private ReceiptHistory receiptHistory;

    /**
     * @see OrderTypeEnum
     * 因增加拼团业务新增订单类型字段 kingapex 2019/1/28 on v7.1.0
     */
    @ApiModelProperty(value = "订单类型")
    private String orderType;


    /**
     * 订单的扩展数据
     * 为了增加订单的扩展性，个性化的业务可以将个性化数据（如拼团所差人数）存在此字段 kingapex 2019/1/28 on v7.1.0
     */
    @ApiModelProperty(value = "扩展数据", hidden = true)
    private String orderData;

    @ApiModelProperty(value = "团长id", hidden = true)
    private Integer leaderId;

    @ApiModelProperty(value = "订单类型", hidden = true)
    private String orderFlag;

    private String pickAddr;

    @ApiModelProperty(name = "expiry_day",value = "虚拟商品的使用截止日期")
    private Integer expiryDay;

    @ApiModelProperty(name = "available_date",value = "虚拟商品的可用日期")
    private String availableDate;

    @ApiModelProperty(name = "is_shetuan",value = "是否是社团订单")
    private Integer isShetuan;

    /**
     * 无参构造器
     */
    public OrderDTO() {

    }


    /**
     * 用一个购物车购造订单
     *
     * @param cart
     */
    public OrderDTO(CartDO cart) {

        super(cart.getSellerId(), cart.getSellerName(), cart.getSellerLogo());

        // 初始化产品及优惠数据
        this.setWeight(cart.getWeight());
        this.setPrice(cart.getPrice());
        this.setSkuList(cart.getSkuList());
        this.setCouponList(cart.getCouponList());
        this.setGiftCouponList(cart.getGiftCouponList());
        this.setGiftList(cart.getGiftList());
        this.setGiftPoint(cart.getGiftPoint());
        this.orderType = OrderTypeEnum.normal.name();

    }


}
