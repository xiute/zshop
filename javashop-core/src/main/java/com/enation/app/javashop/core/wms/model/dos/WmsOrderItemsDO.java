package com.enation.app.javashop.core.wms.model.dos;

import com.enation.app.javashop.core.trade.order.model.enums.ShipStatusEnum;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author JFeng
 * @date 2020/9/21 17:32
 */

@Table(name = "wms_order_items")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class WmsOrderItemsDO {

    /**主键ID*/
    @Id(name = "wms_item_id")
    @ApiModelProperty(hidden=true)
    private Integer deliveryItemId;
    /**商品ID*/
    @Column(name = "goods_id")
    @ApiModelProperty(name="goods_id",value="商品ID",required=false)
    private Integer goodsId;
    /**货品ID*/
    @Column(name = "sku_id")
    @ApiModelProperty(name="sku_id",value="货品ID",required=false)
    private Integer skuId;
    /**发货量*/
    @Column(name = "ship_num")
    @ApiModelProperty(name="ship_num",value="发货量",required=false)
    private Integer shipNum;
    /**订单编号*/
    @Column(name = "order_sn")
    @ApiModelProperty(name="order_sn",value="订单编号",required=false)
    private String orderSn;
    /**图片*/
    @Column(name = "image")
    @ApiModelProperty(name="image",value="图片",required=false)
    private String image;
    /**商品名称*/
    @Column(name = "name")
    @ApiModelProperty(name="name",value="商品名称",required=false)
    private String name;
      /**分类ID*/
    @Column(name = "cat_id")
    @ApiModelProperty(name="cat_id",value="分类ID",required=false)
    private Integer catId;
     /**规格json*/
    @Column(name = "spec_json")
    @ApiModelProperty(name="spec_json",value="规格json",required=false)
    private String specJson;

    /**
     * {@link ShipStatusEnum}
     */
    @Column(name = "ship_status")
    @ApiModelProperty(name="ship_status",value="配送状态",required=false)
    private String shipStatus;

    @Column(name = "wms_order_id")
    @ApiModelProperty(name="wms_order_id",value="出库单id",required=false)
    private Integer wmsOrderId;

    @Column(name = "supplier_name")
    @ApiModelProperty(name="supplier_name",value="供应商姓名",required=false)
    private String supplierName;

}
