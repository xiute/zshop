package com.enation.app.javashop.core.promotion.shetuan.model.dos;

import com.enation.app.javashop.core.goods.model.dto.BuyRecord;
import com.enation.app.javashop.framework.util.DateUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kingapex on 2019-01-22.
 * 拼团商品信息VO
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2019-01-22
 */

@Data
public class ShetuanGoodsGroupVO {

    private Integer shopCatId;

    private String  shopCatName;

    private List<ShetuanGoodsVO> shetuanGoodsList;

}
