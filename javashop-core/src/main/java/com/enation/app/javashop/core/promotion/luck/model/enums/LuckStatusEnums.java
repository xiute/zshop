package com.enation.app.javashop.core.promotion.luck.model.enums;

import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 孙建
 * 抽奖活动状态
 */
public enum LuckStatusEnums {

    NEW(1, "待上线"),
    ONLINE(2, "已上线"),
    OFFLINE(3, "已下线");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (AccountTypeEnum item : AccountTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    LuckStatusEnums(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }
}
