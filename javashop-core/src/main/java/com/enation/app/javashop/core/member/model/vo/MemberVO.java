package com.enation.app.javashop.core.member.model.vo;


import com.enation.app.javashop.core.member.model.dos.Member;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 会员实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-30 14:27:48
 */
@Data
@ApiModel
public class MemberVO {

    /**
     * 会员ID
     */
    @ApiModelProperty(name="uid",value="会员ID")
    private Integer uid;
    /**
     * 会员登录用户名
     */
    @ApiModelProperty(name="username",value="会员登录用户名")
    private String username;
    /**
     * 会员昵称
     */
    @ApiModelProperty(name="nickname",value="会员昵称")
    private String nickname;
    /**
     * 会员真实姓名
     */
    @ApiModelProperty(name = "real_name", value = "会员真实姓名")
    private String realName;
    /**
     * token令牌
     */
    @ApiModelProperty(name="accessToken",value="token令牌")
    private String accessToken;
    /**
     * 刷新token
     */
    @ApiModelProperty(name="refreshToken",value="刷新token")
    private String refreshToken;

    /**
     * 会员头像
     */
    @ApiModelProperty(name="face",value="会员头像")
    private String face;

    /**
     * 账户会员id
     */
    @ApiModelProperty(name="accountMemberId",value="账户会员id")
    private String accountMemberId;

    public MemberVO() {

    }

    public MemberVO(Member member, String sccessToken, String refreshToken) {
        this.uid = member.getMemberId();
        this.face = member.getFace();
        this.username = member.getUname();
        this.nickname = member.getNickname();
        this.accountMemberId = member.getAccountMemberId();
        this.accessToken = sccessToken;
        this.refreshToken = refreshToken;
    }

}