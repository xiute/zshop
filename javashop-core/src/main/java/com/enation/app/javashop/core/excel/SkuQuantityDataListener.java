package com.enation.app.javashop.core.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.enation.app.javashop.core.goods.model.dto.GoodsQuantityRefreshDTO;
import com.enation.app.javashop.core.goods.service.GoodsQuantityManager;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * 模板的读取类
 */
@Slf4j
public class SkuQuantityDataListener extends AnalysisEventListener<GoodsQuantityRefreshDTO> {
    /**
     * 每隔20条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 100;
    private List<GoodsQuantityRefreshDTO> importList = new ArrayList<>();


    /**
     * 假设这个是一个DAO，当然有业务逻辑这个也可以是一个service。当然如果不用存储这个对象没用。
     */
    private GoodsQuantityManager goodsQuantityManager;

    /**
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     *
     * @param goodsQuantityManager
     */
    public SkuQuantityDataListener(GoodsQuantityManager goodsQuantityManager) {
        this. goodsQuantityManager= goodsQuantityManager;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data
     *            one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(GoodsQuantityRefreshDTO data, AnalysisContext context) {
        log.info("解析到一条数据:{}", data.toString());
        importList.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (importList.size() >= BATCH_COUNT) {
            updateData();
            // 存储完成清理 list
            importList.clear();
        }
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        updateData();
        log.info("所有数据解析完成！");
    }

    /**
     * 加上存储数据库
     */
    private void updateData() {
        log.info("{}条数据，开始存储数据库！", importList.size());
        goodsQuantityManager.batchUpdateQuantity(importList);
        log.info("存储数据库成功！");
    }

}