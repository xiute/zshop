package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * 退款(货)操作枚举类
 * @author zjp
 * @version v7.0
 * @since v7.0 下午4:54 2018/5/2
 */
public enum RefundOperateEnum {
    //申请退(款)货
    APPLY("申请退(款)货"),
    //卖家审核
    SELLER_APPROVAL("卖家审核"),
    //退货入库
    STOCK_IN("退货入库"),
    //取消
    CANCEL("取消"),
    //管理员退款
    ADMIN_REFUND("管理员退款"),
    //卖家退款
    SELLER_REFUND("卖家退款");

    private String description;

    RefundOperateEnum(String des) {
        this.description = des;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
