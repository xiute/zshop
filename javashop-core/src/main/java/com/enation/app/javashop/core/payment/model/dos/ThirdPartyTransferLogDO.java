package com.enation.app.javashop.core.payment.model.dos;

import java.io.Serializable;

import com.enation.app.javashop.framework.database.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Date: Created inMon Oct 12 17:17:59 CST 2020
 * @Author: zhou
 * @Description:转账到零钱
 */
@Data
@ApiModel
@Table(name = "es_third_party_transfer_log")
public class ThirdPartyTransferLogDO implements Serializable {

    private static final long serialVersionUID = 2132463941758878221L;

    @Id
    private Integer id;

    @Column(name = "transfer_no")
    @ApiModelProperty(value = "转账单号")
    private String transferNo;

    @Column(name = "transfer_price")
    @ApiModelProperty(value = "转账金额")
    private Double transferPrice;

    @Column(name = "payee_member_id")
    @ApiModelProperty(value = "收款人")
    private Integer payeeMemberId;

    @Column(name = "payee_member_name")
    @ApiModelProperty(value = "收款人真实姓名")
    private String payeeMemberName;

    @Column(name = "pay_config")
    @ApiModelProperty(value = "支付时使用的参数")
    private String payConfig;

    @Column(name = "open_id")
    @ApiModelProperty(value = "收款人的open ID")
    private String openId;

    @Column(name = "transfer_status")
    @ApiModelProperty(value = "转账状态")
    private String transferStatus;

    @Column(name = "payment_plugin_id")
    @ApiModelProperty(value = "支付插件")
    private String paymentPluginId;

    @Column(name = "operator_member_id")
    @ApiModelProperty(value = "操作人")
    private Integer operatorMemberId;

    @Column(name = "operator_member_name")
    @ApiModelProperty(value = "操作人姓名")
    private String operatorMemberName;

    @Column(name = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @Column(name = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    @Column(name = "transfer_describe")
    @ApiModelProperty(value = "企业付款备注")
    private String transferDescribe;

    @Column(name = "request_msg")
    @ApiModelProperty(value = "请求报文")
    private String requestMsg;

    @Column(name = "result_msg")
    @ApiModelProperty(value = "返回报文")
    private String resultMsg;

}
