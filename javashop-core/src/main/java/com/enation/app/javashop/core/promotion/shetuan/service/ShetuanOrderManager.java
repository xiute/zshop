package com.enation.app.javashop.core.promotion.shetuan.service;

import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanOrderDO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.ShetuanOrderStatisticVO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.ShetuanOrderVO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.dto.ShetuanOrderQueryParam;
import com.enation.app.javashop.framework.database.Page;

public interface ShetuanOrderManager {

    ShetuanOrderDO createOrder(OrderDO orderDO);

    ShetuanOrderDO confirm(OrderDO orderDO);

    Page<ShetuanOrderStatisticVO> statistic(ShetuanOrderQueryParam shetuanOrderQueryParam );

    ShetuanOrderStatisticVO statisticAll(ShetuanOrderQueryParam shetuanOrderQueryParam);

    Page<ShetuanOrderVO> queryPage(ShetuanOrderQueryParam shetuanOrderQueryParam);

    ShetuanOrderDO getByOrderId(Integer orderId);

    void calReturnCommission(String orderSn, Double refundPrice);

    void updateShetuanOrderDO(ShetuanOrderDO shetuanOrderDO);

    void initShopCat(Integer sellerId);

}
