package com.enation.app.javashop.core.aftersale.service;

import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.core.aftersale.model.vo.*;
import com.enation.app.javashop.core.aftersale.model.vo.ClaimsParamVO;
import com.enation.app.javashop.core.promotion.coupon.model.dos.CouponDO;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/9/21
 * @Description:
 */
public interface ClaimsManager {
    /**
     * 获取理赔单
     *
     * @return
     */
    Page<ClaimsVO> getClaimsList(ClaimsQueryVO claimsQueryVO,Seller seller);

    /**
     * 批量添加理赔单
     *
     * @param
     */
    Result<String> addClaims(ClaimsParamVO claimsParamVO, Seller seller);

    /**
     * 批量审核理赔单
     *
     * @param claimsIds     理赔单ID
     * @param claimsStatus  审核状态
     * @param auditDescribe 审核描述
     * @param seller        审核人信息
     */
    void auditClaims(List<Integer> claimsIds, String claimsStatus, String auditDescribe, Seller seller);

    /**
     * 批量确认，撤销理赔单
     *
     * @param claimsIds     理赔单ID
     * @param claimsStatus  理赔单状态
     * @param claimDescribe 理赔单描述
     * @param seller        理赔人信息
     */
    void claimsStatus(List<Integer> claimsIds, String claimsStatus, String claimDescribe, Seller seller);
    /**
     * 异常单撤销之后，撤销理赔单
     *
     * @param exceptionIds 异常单IDs
     * @param seller       理赔人
     */
    void auditClaimsByExcpIds(Integer[] exceptionIds, Seller seller);

    /**
     * 获取理赔单列表
     *
     * @param exceptionId 异常单ID
     * @return
     */
    List<ClaimsDO> getClaimsByExceptionId(Integer exceptionId);

    /**
     * 给前端返回封装好的数据
     * @param exceptionId
     * @return
     */
    ClaimsParamVO getClaimsParamVOByExceptionId(Integer exceptionId);

    /**
     * 检测订单是否可以赠送售后券
     * @param orderSn
     * @return 售后券信息
     */
    List<CouponDO> getAftersaleCoupon(String orderSn);

}
