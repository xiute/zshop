package com.enation.app.javashop.core.member.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 商家提现申请关系
 * @author 孙建
 */
@Table(name="es_withdraw_bill")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class WithdrawBillDO {

	/**id**/
	@Id(name="id")
	@ApiModelProperty(hidden=true)
	private Integer id;

	/** 提现记录id **/
	@Column(name = "withdraw_record_id")
	@ApiModelProperty(value="提现记录id")
	private Integer withdrawRecordId;

	/** 结算单id **/
	@Column(name = "bill_id")
	@ApiModelProperty(value="结算单id")
	private Integer billId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getWithdrawRecordId() {
		return withdrawRecordId;
	}

	public void setWithdrawRecordId(Integer withdrawRecordId) {
		this.withdrawRecordId = withdrawRecordId;
	}

	public Integer getBillId() {
		return billId;
	}

	public void setBillId(Integer billId) {
		this.billId = billId;
	}
}
	
