package com.enation.app.javashop.core.pagedata.model.enums;

import com.enation.app.javashop.core.base.context.Region;
import com.enation.app.javashop.core.pagedata.model.PageData;
import com.enation.app.javashop.core.pagedata.service.PageDataManager;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.StringUtil;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author john
 * @Description 页面类型
 * @ClassName PageTypeEnum
 */
public enum IndexPageTypeEnum {
    //平台首页
    PLATFORM_INDEX("INDEX"){
        @Override
        public Boolean setIndexPage( PageDataManager pageDataManager,Integer pageId,String homePageType,Region region) {
            String clientType = PageClientTypeEnum.APP.value();
            // 1.查询平台首页
            PageData indexPage = pageDataManager.getModel(pageId);
            // 判断当前页面属于 上架状态 微页面
//            if (!(PageStateEnum.ACTIVE.getValue().equals(indexPage.getState()) && indexPage.getOwnerType().equals(OwnerTypeEnum.DEFINED.value()))) {
//                throw new ServiceException("500","请查看当前页面是否已上架，并且页面状态是微页面");
//            }
            // 2.查询历史首页
            PageData queryPage = new PageData();
            // app客户端
            queryPage.setClientType(clientType);
            //页面类型
            queryPage.setOwnerType(OwnerTypeEnum.PLATFORM.value());
            // 上架标识
            queryPage.setState(PageStateEnum.ACTIVE.getValue());
            // 首页类型
            queryPage.setPageType(homePageType);
            PageData oldPage = pageDataManager.getOne(queryPage);
            if (oldPage != null && oldPage.getPageId().equals(pageId)) {
                return false;
            }
            //3.修改历史首页类型
            if (oldPage != null) {
                oldPage.setPageType(PageHomeTypeEnum.DEFINED.value().toString());
                oldPage.setPageTypeName(PageHomeTypeEnum.DEFINED.description());
                oldPage.setOwnerType(OwnerTypeEnum.DEFINED.value());
                pageDataManager.edit(oldPage, oldPage.getPageId());
            }
            //4.设置新首页
            indexPage.setOwnerType(OwnerTypeEnum.PLATFORM.value());
            indexPage.setPageTypeName(loadPageTypeName(homePageType));
            indexPage.setPageType(homePageType);
            pageDataManager.edit(indexPage, indexPage.getPageId());
            return true;
        }
    },
    //城市首页
    CITY_INDEX("CITY_INDEX"){
        @Override
        public Boolean setIndexPage(PageDataManager pageDataManager,Integer pageId,String homePageType,Region region) {
            String province = region.getProvince();
            String city = region.getCity();
            String county = region.getCounty();
            String town = region.getTown();
            Integer countyId = region.getCountyId();

            if(StringUtils.isEmpty(province)||StringUtils.isEmpty(city)){
                //throw new ServiceException("500","城市首页-城市信息不能为空");
                throw new ServiceException("500","请选择地区信息");

            }
            String clientType = PageClientTypeEnum.APP.value();
            // 1.查询城市首页
            PageData indexPage = pageDataManager.getModel(pageId);
            // 判断当前页面属于 (上架状态&&( 微页面||城市首页))
//            if(!(PageStateEnum.ACTIVE.getValue().equals(indexPage.getState()) &&
//                    (OwnerTypeEnum.DEFINED.value() == indexPage.getOwnerType() || OwnerTypeEnum.AGENT.value() == indexPage.getOwnerType()))){
//                throw new ServiceException("500","请查看当前页面是否已下架，并且页面类型是非平台首页");
//            }
            String cityStr =  StringUtil.notEmpty(town) ? town : StringUtil.notEmpty(county) ? county : StringUtil.notEmpty(city) ? city : StringUtil.notEmpty(province) ? province : "";

            // 2.查询当前省市-城市历史首页
            PageData oldPage = pageDataManager.getCityIndexPage(cityStr,homePageType);

            if (oldPage != null) {
                throw new ServiceException("500","当前城市首页已经存在-"+oldPage.getPageName());
            }

            //4.设置新首页
            indexPage.setOwnerType(OwnerTypeEnum.AGENT.value());
            indexPage.setPageType(homePageType);
            indexPage.setProvince(province);
            indexPage.setCity(city);
            indexPage.setCounty(county);
            indexPage.setTown(town);
            indexPage.setCountyId(countyId);
            indexPage.setPageTypeName(loadPageTypeName(homePageType));
            pageDataManager.edit(indexPage, indexPage.getPageId());
            return true;
        }
    };

    private String value;

    IndexPageTypeEnum(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    public  abstract Boolean setIndexPage(PageDataManager pageDataManager, Integer pageId,String homePageType,Region region);

    public String loadPageTypeName(String key){
        Map<String,String> map = new HashMap<String,String>();
        if(key != null ){
            map.put("0","无");
            map.put("1","商城首页");
            map.put("2","团购首页");
            map.put("3","附近首页");
            map.put("4","发现页");
        }
        System.out.println(map.get(key));
        return map.get(key);
    }
}
