package com.enation.app.javashop.core.orderbill.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 订单结算对象
 */
@Data
@Table(name = "es_order_settle")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderSettle implements Serializable {

    /**
     * 主键ID
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;
    /**
     * 订单id
     */
    @Column(name = "order_id")
    @ApiModelProperty(name = "order_id", value = "订单id", required = true)
    private Integer orderId;
    /**
     * 订单号
     */
    @Column(name = "order_sn")
    @ApiModelProperty(name = "order_sn", value = "订单号", required = true)
    private String orderSn;
    /**
     * 结算开始时间
     */
    @Column(name = "settle_time")
    @ApiModelProperty(name = "settle_time", value = "结算时间", required = true)
    private Long settleTime;
    /**
     * 结算项id
     */
    @Column(name = "bill_item_id")
    @ApiModelProperty(name = "bill_item_id", value = "结算项id", required = true)
    private Integer billItemId;
}