package com.enation.app.javashop.core.geo.model;

import lombok.Data;

/**
 * @author JFeng
 * @date 2020/5/28 11:39
 */

@Data
public class Pois {
    // 兴趣点名称
    private String name;

    //中心点坐标
    private String location;

    //详细地址
    private String address;

    //省+市+区（直辖市为“市+区”）
    private String district;
    // 纬度
    private Double lat;
    // 经度
    private Double lng;

    private Double distance;
}
