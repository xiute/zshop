package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * @author 王志杨
 * @since 2020年9月21日 14:00:50
 * 异常单处理状态
 */
public enum ExceptionStatusEnum {

    WAITING_RECEIVE("待接单"),
    WAITING_PROCESSING("待处理"),
    IN_PROCESSING("处理中"),
    ALREADY_PROCESSED("已处理"),
    ALREADY_FINISHED("已结单"),
    ALREADY_CANCELED("已取消");

    private String description;

    ExceptionStatusEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String value(){
        return this.name();
    }
}
