package com.enation.app.javashop.core.distribution.model.dto;


import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分销商
 */
@Data
public class DistributionDTO {

    /**
     * 主键id
     */
    @ApiModelProperty(hidden = true)
    private Integer id;

    /**
     * 关联会员的id
     */
    @ApiModelProperty(value = "会员id", required = true)
    private Integer memberId;


    /**
     * 会员名称
     */
    @ApiModelProperty(value = "会员名称", required = true)
    private String memberName;


    /**
     * 关系路径
     */
    @ApiModelProperty(value = "关系路径", required = true)
    private String path;

    /**
     * 2级分销商id（上上级）
     */
    @ApiModelProperty(name = "member_id_lv2", value = "2级分销商id（上上级）", required = true)
    private Integer memberIdLv2;

    /**
     * 1级分销商id（上级）
     */
    @ApiModelProperty(name = "member_id_lv1", value = "1级分销商id（上级）", required = true)
    private Integer memberIdLv1;

    /**
     * 下线人数
     */
    @ApiModelProperty(value = "下线人数", required = true)
    private Integer downline = 0;

    /**
     * 提成相关订单数
     */
    @ApiModelProperty(name = "order_num", value = "提成相关订单数", required = true)
    private Integer orderNum = 0;

    /**
     * 返利总额
     */
    @ApiModelProperty(name = "rebate_total", value = "返利总额", required = true)
    private Double rebateTotal = 0D;

    /**
     * 营业额总额
     */
    @ApiModelProperty(name = "turnover_price", value = "营业额总额", required = true)
    private Double turnoverPrice = 0D;

    /**
     * 可提现金额
     */
    @ApiModelProperty(name = "can_rebate", value = "可提现金额", required = true)
    private Double canRebate = 0D;

    /**
     * 返利金额冻结
     */
    @ApiModelProperty(name = "commission_frozen", value = "冻结金额", required = true)
    private Double commissionFrozen = 0D;


    /**
     * 提现冻结
     */
    @ApiModelProperty(name = "withdraw_frozen_price", value = "冻结金额", required = true)
    private Double withdrawFrozenPrice = 0D;

    /**
     * 使用模板id
     */
    @ApiModelProperty(name = "current_tpl_id", value = "使用模板id", required = true)
    private Integer currentTplId;


    /**
     * 使用模板id
     */
    @ApiModelProperty(name = "current_tpl_name", value = "使用模板名称", required = true)
    private String currentTplName;

    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间", required = false)
    private Long createTime;

    @ApiModelProperty(name = "audit_status", value = "状态", required = false)
    private Integer auditStatus;

    @ApiModelProperty(name = "status_name", value = "状态名称", required = false)
    private String statusName;

    @ApiModelProperty(name = "audit_time", value = "审核时间", required = false)
    private Long auditTime;

    @ApiModelProperty(name = "apply_reason", value = "申请原因", required = false)
    private String applyReason;

    @ApiModelProperty(name = "audit_remark", value = "审核备注", required = false)
    private String auditRemark;

    @ApiModelProperty(name = "real_name", value = "会员真实姓名")
    private String realName;

    @ApiModelProperty(name = "midentity", value = "身份证号", required = false)
    private String midentity;

    @ApiModelProperty(name = "distributor_grade", value = "分销等级", required = false)
    private Integer distributorGrade;

    @ApiModelProperty(name = "distributor_title", value = "分销头衔", required = false)
    private String distributorTitle;

    @ApiModelProperty(name = "status", value = "启用/禁用", required = false)
    private Integer status;

    @ApiModelProperty(name = "invite_code", value = "分销邀请码", required = false)
    private String inviteCode;

    @ApiModelProperty(name = "lv1_expire_time", value = "1级关系创建时间", required = false)
    private Long lv1CreateTime;

    @ApiModelProperty(name = "lv1_expire_time", value = "1级关系失效时间", required = false)
    private Long lv1ExpireTime;

    @ApiModelProperty(name = "lv2_expire_time", value = "2级关系失效时间", required = false)
    private Long lv2ExpireTime;

    @ApiModelProperty(name = "last_order_time", value = "最新下单时间", required = false)
    private Long lastOrderTime;

    @ApiModelProperty(name = "lv1_invite_code", value = "上级邀请码", required = false)
    private String lv1InviteCode;

    @ApiModelProperty(name = "mobile", value = "手机号", required = false)
    private String mobile;
    /**
     * 昵称(前端所属用户)
     */
    @ApiModelProperty(name="nickname", value="会员昵称")
    private String nickname;

    @ApiModelProperty(name="lv1_real_name", value="上级团长名称")
    private String lv1RealName;

    @ApiModelProperty(name = "team_name", value = "别名 如 第8团", required = false)
    private String teamName;

    @ApiModelProperty(name = "province_id", value = "省Id", required = false)
    private Integer provinceId;

    @ApiModelProperty(name = "province", value = "省", required = false)
    private String province;

    @ApiModelProperty(name = "city_id", value = "市Id", required = false)
    private Integer cityId;

    @ApiModelProperty(name = "city", value = "市", required = false)
    private String city;

    @ApiModelProperty(name = "county_id", value = "区/县Id", required = false)
    private Integer countyId;

    @ApiModelProperty(name = "county", value = "区/县", required = false)
    private String county;

    @ApiModelProperty(name = "address", value = "详细地址", required = false)
    private String address;

    @ApiModelProperty(name = "settle_mode", value = "结算模式", required = false)
    private Integer settleMode;

    @ApiModelProperty(name = "business_type", value = "业务类型", required = false)
    private Integer businessType;

    @ApiModelProperty(name = "visit_channel", value = "访问渠道", required = false)
    private Integer visitChannel;
}