package com.enation.app.javashop.core.thirdParty.model.vo;

import com.enation.app.javashop.core.thirdParty.model.dto.ZheretaoGood;
import lombok.Data;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/10/13
 * @Description: 浙里淘更新商品
 */
public class ZheretaoRequestVo {

    //商品列表
    private List<ZheretaoGood> zheretaoGoodList;

    //更新类型
    private String updateType;

    public List<ZheretaoGood> getZheretaoGoodList() {
        return zheretaoGoodList;
    }

    public void setZheretaoGoodList(List<ZheretaoGood> zheretaoGoodList) {
        this.zheretaoGoodList = zheretaoGoodList;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }
}
