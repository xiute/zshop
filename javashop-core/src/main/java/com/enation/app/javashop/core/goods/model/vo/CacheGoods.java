package com.enation.app.javashop.core.goods.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 缓存商品对象
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月29日 上午11:50:02
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class CacheGoods implements Serializable {
    private static final long serialVersionUID = -3642358108471082387L;
    @ApiModelProperty(name = "goods_id", value = "商品id")
    @Column(name = "goods_id")
    private Integer goodsId;

    @ApiModelProperty(name = "category_id", value = "分类id")
    private Integer categoryId;

    @ApiModelProperty(name = "goods_name", value = "商品名称")
    @Column(name = "goods_name")
    private String goodsName;

    @ApiModelProperty(name = "sn", value = "商品编号")
    @Column(name = "sn")
    private String sn;

    @ApiModelProperty(name = "price", value = "商品价格")
    @Column(name = "price")
    private Double price;

    @ApiModelProperty(name = "weight", value = "重量")
    @Column(name = "weight")
    private Double weight;

    @ApiModelProperty(name = "intro", value = "详情")
    private String intro;

    @ApiModelProperty(name = "goods_transfee_charge", value = "谁承担运费0：买家承担，1：卖家承担")
    @Column(name = "goods_transfee_charge")
    private Integer goodsTransfeeCharge;

    @ApiModelProperty(name = "template_id", value = "运费模板id,不需要运费模板时值是0")
    @Column(name = "template_id")
    private Integer templateId;

    @ApiModelProperty(name = "market_enable", value = "是否上架，1上架 0下架")
    @Column(name = "market_enable")
    private Integer marketEnable;

    @ApiModelProperty(name = "disabled", value = "是否放入回收站 0 删除 1未删除")
    @Column(name = "disabled")
    private Integer disabled;

    @ApiModelProperty(name = "is_auth", value = "是否审核通过 0 未审核  1 通过 2 不通过")
    @Column(name = "is_auth")
    private Integer isAuth;

    @ApiModelProperty(value = "可用库存")
    @Column(name = "enable_quantity")
    private Integer enableQuantity;

    @ApiModelProperty(name = "quantity", value = "库存")
    private Integer quantity;

    @ApiModelProperty(name = "seller_id", value = "卖家")
    private Integer sellerId;

    @ApiModelProperty(name = "seller_name", value = "卖家名称")
    private String sellerName;

    @ApiModelProperty(name = "sku_list", value = "sku列表")
    private List<GoodsSkuVO> skuList;

    @ApiModelProperty(name = "thumbnail", value = "商品缩略图")
    private String thumbnail;

    @ApiModelProperty(name = "last_modify", value = "商品最后修改时间")
    private Long lastModify;

    @ApiModelProperty(name = "shop_cat_id", value = "店铺分类id", required = false)
    private Integer shopCatId;

    @ApiModelProperty(name = "video_url", value = "主图视频", required = false)
    private String videoUrl;

    @ApiModelProperty(name = "original", value = "原图路径", required = false)
    private String original;

    @ApiModelProperty(name = "selling", value = "商品卖点", required = false)
    private String selling;

    @ApiModelProperty(name = "expiry_day",value = "虚拟商品的使用截止日期")
    private Integer expiryDay;

    @ApiModelProperty(name = "available_date",value = "虚拟商品的可用日期")
    private String availableDate;

    @ApiModelProperty(name = "goods_type", value = "商品类型NORMAL普通,POINT积分,VIRTUAL虚拟商品", required = false)
    private String goodsType;
}
