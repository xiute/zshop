package com.enation.app.javashop.core.payment.service;

import com.enation.app.javashop.core.aftersale.model.dos.RefundItemDO;

import java.util.List;
import java.util.Map;

/**
 * 退款详情接口
 * @author 王志杨
 * @since 2020-10-26 10:09:17
 */

public interface RefundItemManager {

    void insertRefundItem(RefundItemDO refundItemDO);

    List<RefundItemDO> queryRefundItemByRefundIdAndStatus(Integer refundId, String itemStatus);

    List<RefundItemDO> queryApplyRefundItemByOrderSn(String orderSn);

    Double queryRefundPriceByPluginId(String orderSn, String paymentPluginId);

    RefundItemDO queryRefundItemByRefundIdAndPaymentPluginId(Integer refundId, String paymentPluginId);

    void updateRefundItem(RefundItemDO refundItem);

    RefundItemDO queryRefundItemByPayOrderNo(String payOrderNo, String refundSn);

    void updateRefundItemDisabled(String refundSn);

    double querySuccessRefundPriceByRefundSn(String refundSn, String paymentPluginId);
}
