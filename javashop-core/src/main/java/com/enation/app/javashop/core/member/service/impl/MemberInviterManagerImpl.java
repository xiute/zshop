package com.enation.app.javashop.core.member.service.impl;

import com.enation.app.javashop.core.member.model.dos.MemberInviter;
import com.enation.app.javashop.core.member.service.MemberInviterManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 会员邀请人业务类
 */
@Service
public class MemberInviterManagerImpl implements MemberInviterManager {

    @Autowired
    @Qualifier("memberDaoSupport")
    private DaoSupport memberDaoSupport;


    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void addMemberInviter(MemberInviter memberInviter) {
        memberInviter.setInviteTime(new Date());
        memberDaoSupport.insert(memberInviter);
    }

    @Override
    public MemberInviter getByMemberId(Integer memberId) {
        String sql = "select * from es_member_inviter where member_id = ?";
        return this.memberDaoSupport.queryForObject(sql, MemberInviter.class, memberId);
    }

    @Override
    public List<MemberInviter> getByInviterMemberId(Integer inviterMemberId) {
        String sql = "select * from es_member_inviter where inviter_member_id = ?";
        return this.memberDaoSupport.queryForList(sql, MemberInviter.class, inviterMemberId);
    }
}
