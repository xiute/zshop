package com.enation.app.javashop.core.trade.order.model.vo;

import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.trade.cart.model.vo.CouponVO;
import com.enation.app.javashop.core.trade.cart.model.vo.PriceDetailVO;
import com.enation.app.javashop.core.trade.order.model.dto.OrderDTO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 交易VO
 * @author Snow create in 2018/4/9
 * @version v2.0
 * @since v7.0.0
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class TradeVO implements Serializable {

    private static final long serialVersionUID = -8580648928412433120L;

    @ApiModelProperty(name="trade_sn",value = "交易编号")
    private String tradeSn;

    @ApiModelProperty(value = "会员id")
    private Integer memberId;

    @ApiModelProperty(value = "会员昵称")
    private String memberName;

    @ApiModelProperty(value = "支付方式")
    private String paymentType;

    @ApiModelProperty(value = "价格信息")
    private PriceDetailVO priceDetail;

    @ApiModelProperty(value = "收货人信息")
    private ConsigneeVO consignee;

    @ApiModelProperty(value = "优惠券集合")
    private List<CouponVO> couponList;

    @ApiModelProperty(value = "订单集合")
    private List<OrderDTO> orderList;

    @ApiModelProperty(value = "赠品集合")
    private List<GiftVO> giftList;

    private List<Integer> skuIds;

    private String cartSourceType;
    //钱包支付金额
    private double walletPayPrice;

    private String  couponShare;

    //支付单
    private PaymentBillDO paymentBillDO;

}
