package com.enation.app.javashop.core.promotion.coupon.model.dos;

import org.apache.tools.ant.types.resources.First;

/**
 * 优惠券类型
 */
public enum CouponTypeEnum {

    /**
     * 商品券
     */
    GOODS("商品券"),

    /**
     * 新人券
     */
    NEW_REG("新人券"),


    /**
     * 推广券
     */
    MARKET("推广券"),

    /**
     * 红包券
     */
    RED_COUPON("红包券"),

    /**
     * 售后券
     */
    AFTERSALE("售后券"),

    /**
     * 首单券
     */
    FIRST_ORDER("首单券"),

    /**
     * 营销券
     */
    MARKETING_VOUCHER("营销券"),

    /**
     * 拉新券
     */
    PULL_NEW("拉新券");

    private String description;


    CouponTypeEnum(String description) {
        this.description = description;
    }


    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }

}
