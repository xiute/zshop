package com.enation.app.javashop.core.promotion.shetuan.model;

import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/6/2 9:57
 */

@Data
public class ShetuanVO extends ShetuanDO {
    private List<ShetuanGoodsDO> shetuanGoodsList;


    /**
     * 订阅人数
     */
    private Integer  subscribersNum;
}
