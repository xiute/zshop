package com.enation.app.javashop.core.aftersale.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2020年9月21日 14:00:50
 * 异常订单查询参数实体
 */
@ApiModel("异常订单查询参数实体")
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ExceptionQueryParamVO implements Serializable {

    private static final long serialVersionUID = -2770102048241492159L;

    @ApiModelProperty(name = "page_no", value = "页码", required = true)
    private Integer pageNo;

    @ApiModelProperty(name = "page_size", value = "分页数", required = true)
    private Integer pageSize;

    @ApiModelProperty(name = "start_time",value = "开始时间(时间戳)")
    private Long startTime;

    @ApiModelProperty(name = "end_time",value = "结束时间(时间戳)")
    private Long endTime;

    @ApiModelProperty(name = "time_type",  value = "查询时间类型")
    private String timeType;

    @ApiModelProperty(name = "seller_id", value = "商户ID")
    private Integer sellerId;

    @ApiModelProperty(name="exception_sn", value="异常单号")
    private String exceptionSn;

    @ApiModelProperty(name="order_sn",value="订单号")
    private String orderSn;

    @ApiModelProperty(name="member_nickname",value="买家昵称")
    private String memberNickname;

    @ApiModelProperty(name="exception_status",value="异常处理状态")
    private String exceptionStatus;

    @ApiModelProperty(name="exception_source",value="上报来源")
    private String exceptionSource;

    @ApiModelProperty(name="exception_type",value="异常类型")
    private String exceptionType;

    @ApiModelProperty(name="member_realname_lv1",value="一级团长真实姓名")
    private String memberRealnameLv1;

    @ApiModelProperty(name="site_name",value="站点名")
    private String siteName;

    @ApiModelProperty(name = "ship_name", value = "收货人姓名")
    private String shipName;

    @ApiModelProperty(name = "ship_mobile", value = "收货人手机")
    private String shipMobile;

}
