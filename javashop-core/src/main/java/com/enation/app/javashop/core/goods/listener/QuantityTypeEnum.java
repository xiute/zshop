package com.enation.app.javashop.core.goods.listener;

/**
 * @author JFeng
 * @date 2021/1/13 10:23
 */
public enum QuantityTypeEnum {
    /**
     * 拼团商品
     */
    PINTUAN_GOODS("拼团商品"),
    /**
     * 社区团购
     */
    SHETUAN_GOODS("社团团购商品"),
    /**
     * 虚拟商品
     */
    SECKILL_GOODS("秒杀商品");

    private String description;

    QuantityTypeEnum(String description) {
        this.description = description;

    }
}
