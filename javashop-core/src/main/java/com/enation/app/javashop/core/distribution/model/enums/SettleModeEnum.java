package com.enation.app.javashop.core.distribution.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 结算模式
 */
public enum SettleModeEnum {

    SELF_TAKE(1, "结算到自提点"),
    MEMBER(2, "结算到团长");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (SettleModeEnum item : SettleModeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    SettleModeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }


}
