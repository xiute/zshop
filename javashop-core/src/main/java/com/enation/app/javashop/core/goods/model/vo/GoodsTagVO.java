package com.enation.app.javashop.core.goods.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/*
 * @Author: zhou
 * @Date: 2020/11/25
 * @Description: 新增商品标签
 */
@Data
public class GoodsTagVO {

    /**
     * 主键
     */
    @Column(name = "tag_id")
    @ApiModelProperty(name = "tag_id", value = "标签ID（只有编辑的时候需要传）")
    private Integer tagId;
    /**
     * 标签名字
     */
    @Column(name = "tag_name")
    @ApiModelProperty(name = "tag_name", value = "标签名字", required = false)
    private String tagName;

    @ApiModelProperty(name = "goods_tag_display_position", value = "标签显示位置", required = false)
    private List<String> GoodsTagDisplayPosition;

    @Column(name = "shop_home_page")
    @ApiModelProperty(name = "shop_home_page", value = "店铺主页显示标签（0显示，1不显示）")
    private Integer shopHomePage;

    @Column(name = "goods_list_page")
    @ApiModelProperty(name = "goods_list_page", value = "商品列表页显示标签（0显示，1不显示）")
    private Integer goodsListPage;

    @Column(name = "goods_details_page")
    @ApiModelProperty(name = "goods_details_page", value = "商品详情页显示标签（0显示，1不显示）")
    private Integer goodsDetailsPage;

    @Column(name = "all_goods")
    @ApiModelProperty(name = "all_goods", value = "是否给全部商品添加标签（0是，1否）")
    private Integer allGoods;

    @Column(name = "priority")
    @ApiModelProperty(name = "priority", value = "标签的优先级（数字越大排序越靠前）")
    private Integer priority;

    @Column(name = "permanent_enable")
    @ApiModelProperty(name = "permanent_enable", value = "是否永久有效（0是，1否）")
    private Integer permanentEnable;

    @Column(name = "status")
    @ApiModelProperty(name = "status", value = "标签状态")
    private Integer status;

    @Column(name = "start_time")
    @ApiModelProperty(name = "start_time", value = "标签有效开始时间", required = false)
    private Long startTime;

    @Column(name = "end_time")
    @ApiModelProperty(name = "end_time", value = "标签有效截止时间", required = false)
    private Long endTime;

    @Column(name = "color")
    @ApiModelProperty(name = "color", value = "颜色")
    private String color;

    @Column(name = "style")
    @ApiModelProperty(name = "style", value = "样式")
    private String style;

    private String colorName;

    private String styleName;

    private String typeName;

    @ApiModelProperty(name = "type", value = "标签类型，0系统默认，1自定义")
    private Integer type;

    @Column(name = "tag_describe")
    @ApiModelProperty(name = "tag_describe", value = "标签描述")
    private String tagDescribe;
}
