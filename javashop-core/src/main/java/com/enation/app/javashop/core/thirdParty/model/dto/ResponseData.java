package com.enation.app.javashop.core.thirdParty.model.dto;

import lombok.Data;
@Data
public class ResponseData<T> {
    public T result;
    public long timeStamp;
}
