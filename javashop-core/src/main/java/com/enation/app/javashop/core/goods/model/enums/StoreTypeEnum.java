package com.enation.app.javashop.core.goods.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2020/12/21
 * @Description: 商品贮藏条件
 */
public enum StoreTypeEnum {
    NORMAL_TEMPERATURE(0, "常温"),
    REFRIGERATE(1, "冷藏"),
    FREEZE(2, "冷冻");

    private Integer index;
    private String name;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (StoreTypeEnum item : values()) {
            enumMap.put(item.getIndex(), item.getName());
        }
    }

    StoreTypeEnum(Integer index, String name) {
        this.name = name;
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public static String getNameByIndex(int index) {
        return enumMap.get(index);
    }
}
