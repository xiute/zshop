package com.enation.app.javashop.core.distribution.model.enums;

import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 王志杨
 * @since 2021/1/22 17:21
 * 团长任务状态枚举
 */
public enum DistributionMissionStatusEnums {

    NEW(1, "待上线"),
    ONLINE(2, "已上线"),
    OFFLINE(3, "已下线");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (AccountTypeEnum item : AccountTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    DistributionMissionStatusEnums(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }
}
