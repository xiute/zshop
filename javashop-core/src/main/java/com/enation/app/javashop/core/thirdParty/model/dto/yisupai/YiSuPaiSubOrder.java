package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/5/26
 * @Description:
 */
@Data
public class YiSuPaiSubOrder {
    // 纠纷ID
    private Integer disputeId;
    // 可空，逆向的状态(1: "买家已经申请退款，等待卖家同意" 2: "卖家已经同意退款，等待买家退货" 3: "买家已经退货，等待卖家确认收货"
    // 4: "退款关闭" 5: "退款成功" 6: "卖家拒绝退款" 7: "等待买家确认重新邮寄的货物" 8: "等待卖家确认退货地址" 9: "没有申请退款"
    // 10: "有活动的退款" 11: "退款结束" 12: "卖家确认收货,等待卖家发货" 14: "换货关闭,转退货退款"
    // 13: "卖家已发货,等待卖家和买家确认收货" 15: "邮费单已创建,待激活" 20: "等待平台处理")
    private Integer disputeStatus;
    // 子订单金额，以分为单位
    private Integer itemPrice;
    // 下单数量
    private Integer number;
    // 下单的商品SkuId
    private Integer skuId;
    // 订单状态:0-已取消，10-未付款，20-已付款，40-已发货，50-交易成功，60-交易关闭
    private Integer status;
    // 商品ID
    private String itemId;
    // 商品图片
    private String itemPic;
    // 商品标题
    private String itemTitle;
    // 下单的商品skuName
    private String skuName;
    // 子订单号
    private String subOrderNo;
}
