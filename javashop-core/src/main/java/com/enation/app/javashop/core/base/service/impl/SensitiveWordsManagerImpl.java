package com.enation.app.javashop.core.base.service.impl;

import com.enation.app.javashop.core.base.CachePrefix;
import com.enation.app.javashop.core.base.model.dos.SensitiveWords;
import com.enation.app.javashop.core.statistics.util.DateUtil;
import com.enation.app.javashop.framework.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.core.base.service.SensitiveWordsManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 敏感词业务类
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-08-02 11:30:59
 */
@Service
public class SensitiveWordsManagerImpl implements SensitiveWordsManager{

	@Autowired
	@Qualifier("systemDaoSupport")
	private	DaoSupport	daoSupport;
	@Autowired
	private Cache cache;
	
	@Override
	public Page list(int page, int pageSize, String keyword){

		StringBuffer sqlBuffer = new StringBuffer("select * from es_sensitive_words  ");

		List<Object> term = new ArrayList<>();
		if(keyword!=null){
			sqlBuffer.append("where word_name like ? ");
			term.add("%"+keyword+"%");
		}

		Page  webPage = this.daoSupport.queryForPage(sqlBuffer.toString(),page, pageSize ,SensitiveWords.class,term.toArray());
		
		return webPage;
	}
	
	@Override
	@Transactional(value = "systemTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public SensitiveWords add(SensitiveWords	sensitiveWords)	{

		sensitiveWords.setIsDelete(1);
		sensitiveWords.setCreateTime(DateUtil.getDateline());

		this.daoSupport.insert(sensitiveWords);

		cache.remove(CachePrefix.SENSITIVE_WORDS.getPrefix());
		
		return sensitiveWords;
	}
	
	@Override
	@Transactional(value = "systemTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public	SensitiveWords  edit(SensitiveWords	sensitiveWords,Integer id){
		this.daoSupport.update(sensitiveWords, id);
		cache.remove(CachePrefix.SENSITIVE_WORDS.getPrefix());
		return sensitiveWords;
	}
	
	@Override
	@Transactional(value = "systemTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public	void delete( Integer id)	{
		String sql = "update es_sensitive_words set is_delete = 0 where id = ?";
		this.daoSupport.execute(sql,id);
		cache.remove(CachePrefix.SENSITIVE_WORDS.getPrefix());
	}
	
	@Override
	public	SensitiveWords getModel(Integer id)	{
		return this.daoSupport.queryForObject(SensitiveWords.class, id);
	}

	@Override
	public List<String> listWords() {

		String sql = "select * from es_sensitive_words where is_delete = 1";

		List<SensitiveWords> list = this.daoSupport.queryForList(sql,SensitiveWords.class);

		List<String> words = new ArrayList<>();
		if(list !=null ){

			for(SensitiveWords word:list){
				words.add(word.getWordName());
			}

		}
		return words;
	}
}
