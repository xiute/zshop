package com.enation.app.javashop.core.payment.service.impl;

import com.enation.app.javashop.core.payment.model.dos.ThirdPartyTransferLogDO;
import com.enation.app.javashop.core.payment.service.ThirdPartyTransferLogManager;
import com.enation.app.javashop.core.statistics.util.DateUtil;
import com.enation.app.javashop.framework.database.DaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @Author: zhou
 * @Date: 2020/10/12
 * @Description:
 */
@Service
public class ThirdPartyTransferLogManagerImpl implements ThirdPartyTransferLogManager {
    @Autowired
    @Qualifier("tradeDaoSupport")
    private DaoSupport daoSupport;

    @Override
    public void addThirdPartyTransferLog(ThirdPartyTransferLogDO thirdPartyTransferLogDO) {
        this.daoSupport.insert(thirdPartyTransferLogDO);
    }

    @Override
    public void updateStatusByNo(String transferNo, String transferStatus) {
        String sql="update es_third_party_transfer_log set update_time = "+ DateUtil.getDateline() +", transfer_status = '"+transferStatus+"' where transfer_no = '"+transferNo+"'";
        this.daoSupport.execute(sql);
    }


}
