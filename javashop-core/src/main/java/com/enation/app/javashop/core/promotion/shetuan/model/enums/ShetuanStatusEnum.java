package com.enation.app.javashop.core.promotion.shetuan.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 结算单状态枚举
 * @author yanlin
 * @version v1.0
 * @since v7.0.0
 * @date 2018年4月15日 上午10:34:30
 */
public enum ShetuanStatusEnum {

	/**
	 * 未确认状态
	 */
	NEW(10,"待上线"),

	/**
	 * 报名中
	 */
	SIGN_UP(20,"报名中"),

	/**
	 * 待上线
	 */
	OFF_LINE(30,"已下线"),
	/**
	 * 已上线
	 */
	ON_LINE(40,"已上线"),
	/**
	 * 已删除
	 */
	DELETE(0,"已删除");

	private String description;
	private int index;

	ShetuanStatusEnum(int index,String description) {
		this.description = description;
		this.index = index;
	}

	public int getIndex(){
		return index;
	}

	public String getDescription() {
		return description;
	}

	private static Map<Integer, ShetuanStatusEnum> enumMap = new HashMap<>();

	static {
		for (ShetuanStatusEnum item : ShetuanStatusEnum.values()) {
			enumMap.put(item.getIndex(), item);
		}
	}


	public static String getTextByIndex(int index){
		final ShetuanStatusEnum enumValue = enumMap.get(index);
		return enumValue == null ? null : enumValue.getDescription();
	}

	public static ShetuanStatusEnum resolve(int index){
		return enumMap.get(index);
	}

}
