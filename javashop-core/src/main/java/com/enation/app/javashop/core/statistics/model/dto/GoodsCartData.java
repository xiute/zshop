package com.enation.app.javashop.core.statistics.model.dto;

import java.io.Serializable;

import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:
 * @Date: Created inFri Sep 11 14:21:16 CST 2020
 * @Author: zhou
 * @Modified By:
 */
@Data
public class GoodsCartData implements Serializable {

    private static final long serialVersionUID = 8846555917698150230L;

    @Column(name = "id")
    private Integer id;

    @ApiModelProperty(value = "会员ID")
    @Column(name = "member_id")
    private Integer memberId;

    @ApiModelProperty(value = "商品ID")
    @Column(name = "goods_id")
    private Integer goodsId;

    @ApiModelProperty(value = "商品名称")
    @Column(name = "goods_name")
    private String goodsName;

    @ApiModelProperty(value = "每天累计加购数")
    @Column(name = "cart_goods_num")
    private Integer cartGoodsNum;

    @ApiModelProperty(value = "年份")
    @Column(name = "year")
    private Integer year;

    @ApiModelProperty(value = "月份")
    @Column(name = "month")
    private Integer month;

    @ApiModelProperty(value = "天")
    @Column(name = "day")
    private Integer day;

}
