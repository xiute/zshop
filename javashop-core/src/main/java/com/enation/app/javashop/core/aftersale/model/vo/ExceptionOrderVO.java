package com.enation.app.javashop.core.aftersale.model.vo;

import com.enation.app.javashop.core.aftersale.model.dos.ExceptionOrderDO;
import com.enation.app.javashop.core.aftersale.model.enums.ExceptionSourceEnum;
import com.enation.app.javashop.core.aftersale.model.enums.ExceptionStatusEnum;
import com.enation.app.javashop.core.aftersale.model.enums.ExceptionTypeEnum;
import com.enation.app.javashop.core.trade.order.model.enums.OrderTypeEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2020年9月21日 14:00:50
 * 异常订单实体VO
 */
@ApiModel("异常订单实体VO")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ExceptionOrderVO extends ExceptionOrderDO implements Serializable {

    private static final long serialVersionUID = -6164256029036827306L;

    /**
     * 订单类型
     */
    @ApiModelProperty(name = "order_type", value = "订单类型")
    private String orderType;

    /**
     * 订单类型文本
     */
    @ApiModelProperty(name = "order_type_text", value = "订单类型文本")
    private String orderTypeText;

    /**
     * 配送方式
     */
    @ApiModelProperty(name = "shipping_type", value = "配送方式")
    private String shippingType;

    /**
     * 配送方式文本
     */
    @ApiModelProperty(name = "shipping_type_text", value = "配送方式文本")
    private String shippingTypeText;

    /**
     * 收货人姓名
     */
    @ApiModelProperty(name = "ship_name", value = "收货人姓名")
    private String shipName;
    /**
     * 收货地址
     */
    @ApiModelProperty(name = "ship_addr", value = "收货地址")
    private String shipAddr;

    /**
     * 收货人手机
     */
    @ApiModelProperty(name = "ship_mobile", value = "收货人手机")
    private String shipMobile;

    /**
     * 异常处理状态文本
     */
    @ApiModelProperty(name="exception_status_text",value="异常处理状态文本")
    private String exceptionStatusText;

    /**
     * 异常上报来源文本
     */
    @ApiModelProperty(name="exception_source_text",value="异常上报来源文本")
    private String exceptionSourceText;
    /**
     * 异常类型文本
     */
    @ApiModelProperty(name="exception_type_text",value="异常类型文本")
    private String exceptionTypeText;

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderTypeText() {
        if(this.getOrderType() != null){
            if(OrderTypeEnum.shetuan.toString().equals(orderType)){
                orderTypeText = "社区团购订单";
            }
            if(OrderTypeEnum.pintuan.toString().equals(orderType)){
                orderTypeText = "拼团订单";
            }
            if(OrderTypeEnum.normal.toString().equals(orderType)){
                orderTypeText = "普通订单";
            }
        }
        return orderTypeText;
    }

    public void setOrderTypeText(String orderTypeText) {
        this.orderTypeText = orderTypeText;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getShippingTypeText() {
        if (this.getShippingType() != null) {
            ShipTypeEnum shipTypeEnum = ShipTypeEnum.valueOf(this.getShippingType());
            shippingTypeText = shipTypeEnum.getDesc();
        }
        return shippingTypeText;
    }

    public void setShippingTypeText(String shippingTypeText) {
        this.shippingTypeText = shippingTypeText;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipAddr() {
        return shipAddr;
    }

    public void setShipAddr(String shipAddr) {
        this.shipAddr = shipAddr;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public String getExceptionStatusText() {
        if (this.getExceptionStatus() != null) {
            ExceptionStatusEnum statusEnum = ExceptionStatusEnum.valueOf(this.getExceptionStatus());
            exceptionStatusText = statusEnum.getDescription();
        }
        return exceptionStatusText;
    }

    public void setExceptionStatusText(String exceptionStatusText) {
        this.exceptionStatusText = exceptionStatusText;
    }

    public String getExceptionSourceText() {
        if (this.getExceptionSource() != null) {
            ExceptionSourceEnum exceptionSourceEnum = ExceptionSourceEnum.valueOf(this.getExceptionSource());
            exceptionSourceText = exceptionSourceEnum.getDescription();
        }
        return exceptionSourceText;
    }

    public void setExceptionSourceText(String exceptionSourceText) {
        this.exceptionSourceText = exceptionSourceText;
    }

    public String getExceptionTypeText() {
        if (this.getExceptionType() != null) {
            ExceptionTypeEnum exceptionTypeEnum = ExceptionTypeEnum.valueOf(this.getExceptionType());
            exceptionTypeText = exceptionTypeEnum.getDescription();
        }
        return exceptionTypeText;
    }

    public void setExceptionTypeText(String exceptionTypeText) {
        this.exceptionTypeText = exceptionTypeText;
    }
}
