package com.enation.app.javashop.core.shop.model.dto;

import com.enation.app.javashop.core.goods.model.dos.GoodsSkuDO;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/4/9
 * @Description: 分组商品对象（从数据库中查出来的）
 */
@Data
public class NearbyShopCatGoodsDTO extends GoodsSkuDO {
    @ApiModelProperty(name = "one_cat_id", value = "一级分组ID")
    private Integer oneCatId;

    @ApiModelProperty(name = "one_cat_name", value = "一级分组名")
    private String oneCatName;

    @ApiModelProperty(name = "two_cat_id", value = "二级分组ID")
    private Integer twoCatId;

    @ApiModelProperty(name = "two_cat_name", value = "二级分组名")
    private String twoCatName;

    @ApiModelProperty(name = "small", value = "商品小图")
    private String small;

    @Id(name = "tag_id")
    @ApiModelProperty(hidden = true)
    private Integer tagId;
    /**
     * 标签名字
     */
    @Column(name = "tag_name")
    @ApiModelProperty(name = "tag_name", value = "标签名字", required = false)
    private String tagName;
    /**
     * 所属卖家
     */
    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "所属卖家", required = false)
    private Integer sellerId;
    /**
     * 关键字
     */
    @Column(name = "mark")
    @ApiModelProperty(name = "mark", value = "关键字", required = false)
    private String mark;

    @Column(name = "shop_home_page")
    @ApiModelProperty(name = "shop_home_page", value = "店铺主页显示标签（0显示，1不显示）")
    private Integer shopHomePage;
    @Column(name = "goods_list_page")
    @ApiModelProperty(name = "goods_list_page", value = "商品列表页显示标签（0显示，1不显示）")
    private Integer goodsListPage;
    @Column(name = "goods_details_page")
    @ApiModelProperty(name = "goods_details_page", value = "商品详情页显示标签（0显示，1不显示）")
    private Integer goodsDetailsPage;

    @Column(name = "permanent_enable")
    @ApiModelProperty(name = "permanent_enable", value = "是否永久有效（0是，1否）")
    private Integer permanentEnable;
    @Column(name = "all_goods")
    @ApiModelProperty(name = "all_goods", value = "是否给全部商品添加标签（0是，1否）")
    private Integer allGoods;
    @Column(name = "priority")
    @ApiModelProperty(name = "priority", value = "标签的优先级（数字越大排序越靠前）")
    private Integer priority;
    @Column(name = "type")
    @ApiModelProperty(name = "type", value = "标签类型，0系统默认，1自定义")
    private Integer type;
    @Column(name = "status")
    @ApiModelProperty(name = "status", value = "标签状态")
    private Integer status;
    @Column(name = "start_time")
    @ApiModelProperty(name = "start_time", value = "标签有效开始时间", required = false)
    private Long startTime;
    @Column(name = "end_time")
    @ApiModelProperty(name = "end_time", value = "标签有效截止时间", required = false)
    private Long endTime;
    @Column(name = "color")
    @ApiModelProperty(name = "color", value = "颜色")
    private String color;
    @Column(name = "style")
    @ApiModelProperty(name = "style", value = "样式")
    private String style;
    @Column(name = "tag_describe")
    @ApiModelProperty(name = "tag_describe", value = "标签描述")
    private String tagDescribe;

    @Column(name = "create_time")
    @ApiModelProperty(name = "create_time", value = "创建时间", required = false)
    private Long createTime;
    @Column(name = "update_time")
    @ApiModelProperty(name = "update_time", value = "更新时间")
    private Long updateTime;
}
