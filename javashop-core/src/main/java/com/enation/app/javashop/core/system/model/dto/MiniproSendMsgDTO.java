package com.enation.app.javashop.core.system.model.dto;

import com.enation.app.javashop.core.system.enums.WechatMiniproTemplateTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 小程序消息订阅DTO
 * @author 孙建
 */
@Data
public class MiniproSendMsgDTO implements Serializable{

    // openId
    private String openId;

    // 模板类型
    private WechatMiniproTemplateTypeEnum templateTypeEnum;

    // 数据集合
    private Map<String, Object> dataMap;

    // 小程序页面参数
    private String pageParams;

    public MiniproSendMsgDTO(){}

    public MiniproSendMsgDTO(String openId, WechatMiniproTemplateTypeEnum templateTypeEnum, Map<String, Object> dataMap, String pageParams) {
        this.openId = openId;
        this.templateTypeEnum = templateTypeEnum;
        this.dataMap = dataMap;
        this.pageParams = pageParams;
    }
}
