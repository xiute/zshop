package com.enation.app.javashop.core.shop.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author JFeng
 * @date 2020/11/24 17:35
 */
@Data
public class ExceptDaySetting implements Serializable {
    private static final long serialVersionUID = 5739331716616204444L;

    private String stopDays;
    private Integer dispatchDay;
    private Integer isCurrent;
}
