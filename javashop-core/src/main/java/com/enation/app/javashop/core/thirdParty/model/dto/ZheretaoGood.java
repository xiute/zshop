package com.enation.app.javashop.core.thirdParty.model.dto;

import lombok.Data;

import java.util.List;

//商品详情表
@Data
public class ZheretaoGood{
    //商品详情中的商品ID
    private String goodId;
    //商品列表中的商品ID，为了兼容商品列表
    private String goodsId;
    //分类ID
    private String catId;
    // 商品名称
    private String title;
    //成本价
    private Double takeAmount;
    //商品售价
    private Double backAmount;
    // 商品原价
    private Double orderAmount;
    // 商品详情图
    private String description;
    //商品库存数
    private Integer initStock;
    //商品状态 ENABLED上架 UNENABLED下架
    private String status;
    //商品列表中的图（为了兼容商品列表）
    private String picStr;
    //商品详情中的主图列表
    private List<String> pics;
    //规格名和规格值
    private List<GoodsSpecs> goodsSpecsList;
    //sku的id及关联的规格
    private List<SkuAmt> skuAmtList;
}
