package com.enation.app.javashop.core.aftersale.model.dto;

import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author 王志杨
 * @Date 2020/8/31 15:40
 * @Descroption 差价退款VO
 * @Since
 * @Version
 */
@ApiModel
@Data
public class SpreadRefundDTO implements Serializable {

    @ApiModelProperty(name="refund_type",value="售后类型(多付退款-SPREAD_REFUND)",required=false)
    @NotBlank(message = "售后类型必选")
    private String refundType;

    @ApiModelProperty(name="min_refund",value="随机退款最小值",required=false)
    private Double minRefund;

    @ApiModelProperty(name="max_refund",value="随机退款最大值",required=false)
    private Double maxRefund;

    @ApiModelProperty(name="fix_refund",value="固定金额退款",required=false)
    private Double fixRefund;

    @ApiModelProperty(name="refund_reason",value="退款原因",required=false)
    @NotBlank(message = "退款原因必选")
    private String refundReason;

    @ApiModelProperty(name="refund_description",value="退款描述",required=false)
    @NotBlank(message = "退款描述必填")
    private String refundDescription;

}
