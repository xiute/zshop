package com.enation.app.javashop.core.goods.model.dto;

import lombok.Data;

/**
 * @author JFeng
 * @date 2020/7/11 17:31
 */

@Data
public class GoodsQuantityRefreshDTO {

    private Integer goodsId;

    private Integer skuId;

    private String goodsName;

    // 剩余库存
    private Integer quantity;

    // 剩余可用库存 不包含待发货商品数
    private Integer enableQuantity;

    private Integer shipNum;

}
