package com.enation.app.javashop.core.shop.service;

import com.enation.app.javashop.core.shop.model.dos.ShipLocalTemplateVO;
import com.enation.app.javashop.core.shop.model.dos.ShipTemplateDO;
import com.enation.app.javashop.core.shop.model.vo.ShipTemplateSellerVO;
import com.enation.app.javashop.core.shop.model.vo.ShipTemplateVO;

import java.util.List;

/**
 * 运费模版业务层
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-28 21:44:49
 */
public interface ShipTemplateLocalManager {

	ShipLocalTemplateVO saveLocalTemplate(ShipLocalTemplateVO localTemplateDO);

	Boolean editLocalTemplate(ShipLocalTemplateVO localTemplateDO);

	List<ShipLocalTemplateVO> getFromCacheBySellerId(Integer sellerId);

	ShipLocalTemplateVO getFromCache(Integer templateId);

	ShipLocalTemplateVO getFromDB(Integer templateId);
}
