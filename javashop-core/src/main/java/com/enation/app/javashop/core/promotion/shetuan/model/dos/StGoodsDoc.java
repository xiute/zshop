package com.enation.app.javashop.core.promotion.shetuan.model.dos;

import com.enation.app.javashop.core.goods.model.dos.TagsDO;
import com.enation.app.javashop.framework.elasticsearch.EsSettings;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;
import java.util.List;

/**
 * 社区团购商品在索引中的文档
 *
 */
@Document(indexName = "#{esConfig.indexName}_"+ EsSettings.SHETUAN_INDEX_NAME, type = EsSettings.SHETUAN_TYPE_NAME,replicas = 0,shards = 1)
@Data
public class StGoodsDoc {

    private Integer skuId;

    private Integer goodsId;

    private Integer shetuanId;

    private String goodsName;

    private String selling;

    private String thumbnail;

    private Double originPrice;

    private Double salesPrice;

    private Double shetuanPrice;

    private Long endTime;

    private Long startTime;

    private Long pickTime;

    private String address;

    private Integer enableQuantity;

    private Integer goodsNum;

    private Integer visualNum;

    private Integer shopCatId;

    private String shopCatName;

    private String shopCatPath;

    private String videoUrl;

    private String original;

    private Integer sellerId;

    private String sellerName;

    private Integer status;

    private Integer shetuanStatus;
    // 商品历史累计销量
    private Integer buyCount;

    private Integer priority;

    private Boolean isPreSale;

    private Integer disabled;

    private Integer marketEnable;

    private Integer isAuth;

    private String intro;

    private Integer limitNum;

    private Double grade;

    private Long timestamp;

    private Integer shetuanGoodsId;

    private String shopHomeTags;

    private String goodsListTags;

    private String goodsDetailsTags;


}
