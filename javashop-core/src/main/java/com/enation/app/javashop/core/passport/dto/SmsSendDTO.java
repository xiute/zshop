package com.enation.app.javashop.core.passport.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author JFeng
 * @date 2020/7/10 11:25
 */

@Data
public class SmsSendDTO {
    @ApiModelProperty(name = "mobile", value = "手机号", example = "17612166948",required = true)
    private String mobile;

    //@ApiModelProperty(name = "scence_type", value = "短信发送场景-默认登录",allowableValues = "LOGIN,REGISTER")
    //private String scenceType;

    @ApiModelProperty(name = "session_id", value = "阿里云",required = true)
    private String sessionId;

    @ApiModelProperty(name = "sig", value = "阿里云",required = true)
    private String sig;

    @ApiModelProperty(name = "nc_token", value = "阿里云",required = true)
    private String ncToken;

}
