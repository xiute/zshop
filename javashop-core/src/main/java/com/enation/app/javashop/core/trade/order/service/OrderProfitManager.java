package com.enation.app.javashop.core.trade.order.service;

import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.enums.CommissionTypeEnum;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.OrderProfitStatisticVO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderProfitDO;
import com.enation.app.javashop.core.trade.order.model.dto.OrderProfitQueryParam;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.database.Page;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author chien
 * @since 2020-07-29
 */
public interface OrderProfitManager {


    OrderProfitDO getModel(Integer id);

    void add(OrderProfitDO orderProfitDO);

    void edit(OrderProfitDO orderProfitDO);

    // 查询账单收益(日、月 账单总览)
    Page loadProfitList(OrderProfitQueryParam queryParam);

    //日，月账单页面 下面的统计

    OrderProfitStatisticVO statisticAll(OrderProfitQueryParam queryParam);

    // 查询对应的详情
    Page queryProfitList(OrderProfitQueryParam detailsQueryParam);

    // 查询合计佣金
    Double getTotalCommissionMoney(OrderProfitQueryParam queryParam);

    // 查询已结算佣金
    Double getSettledCommissionMoney(OrderProfitQueryParam queryParam);

    void updateOrderProfitStatus(OrderDetailDTO orderDetailDTO);

    List<OrderProfitDO> listByOrderId(Integer orderId);

    OrderProfitDO buildOrderProfit(OrderDO orderDO, OrderDetailDTO orderDetailDTO, Integer memberId, String memberName,
                                   BigDecimal commissionRate, BigDecimal commissionMoney,
                                   CommissionTypeEnum commissionTypeEnum, String memberTypeName, Integer spreadWay,Member buyerMember);

    void delete(Integer id);

    void updateOrderProfit(Double commissionMoney,String orderSn,String memberTypeName,Integer memberId);

    void  exportProfitList(OrderProfitQueryParam queryParam);

}
