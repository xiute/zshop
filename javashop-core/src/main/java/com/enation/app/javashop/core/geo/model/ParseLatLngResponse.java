package com.enation.app.javashop.core.geo.model;

import lombok.Data;

/**
 * @author JFeng
 * @date 2019/10/22 10:43
 */
@Data
public class ParseLatLngResponse {
    private String status;
    private String info;
    private String infocode;
    private RegeoCode regeocode;
}
