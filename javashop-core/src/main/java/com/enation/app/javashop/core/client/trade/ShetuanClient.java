package com.enation.app.javashop.core.client.trade;


import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanOrderDO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.ShetuanOrderStatisticVO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.dto.ShetuanOrderQueryParam;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;
import java.util.Map;

/**
 * 社区团购订单
 */

public interface ShetuanClient {

    /**
     * 根据状态查询拼团活动
     *
     * @param status 状态
     * @return 拼团活动集合
     */
    List<ShetuanDO> get(String status);

    boolean deleteByShetuanId(Integer shetuanId);

    List<ShetuanGoodsDO> querySheuanGoods(Integer shetuanId, List<Integer> skuIds);

    boolean addIndex(ShetuanGoodsVO shetuanGoodsVO, Map<String, Object> goods);

    boolean delIndex(Integer shetuanId,Integer skuId);

    ShetuanOrderDO confirm(OrderDO orderDO);

    Page<ShetuanOrderStatisticVO> statistic(ShetuanOrderQueryParam shetuanOrderQueryParam);

    ShetuanOrderStatisticVO statisticAll(ShetuanOrderQueryParam shetuanOrderQueryParam);

    ShetuanDO queryShetuanById(Integer shetuanId);

    Page queryShetuanOrders(ShetuanOrderQueryParam shetuanOrderQueryParam);

    List<ShetuanGoodsDO> querySheuanGoodsByGoodsIds(Integer[] goodsIds);
}
