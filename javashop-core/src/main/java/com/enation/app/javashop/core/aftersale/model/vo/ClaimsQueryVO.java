package com.enation.app.javashop.core.aftersale.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2020/9/24
 * @Description: 条件查询的实体类
 */
@Data
public class ClaimsQueryVO {

    @ApiModelProperty(name = "page_no", value = "页码", required = true)
    private Integer pageNo;

    @ApiModelProperty(name = "page_size", value = "分页数", required = true)
    private Integer pageSize;

    @ApiModelProperty(name = "start_time", value = "开始时间(时间戳)")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "结束时间(时间戳)")
    private Long endTime;

    @ApiModelProperty(name = "time_type", value = "查询时间类型")
    private String timeType;

    @ApiModelProperty(name="claim_sn", value = "理赔单号")
    private String claimSn;

    @ApiModelProperty(name="exception_sn", value = "异常单编号")
    private String exceptionSn;

    @ApiModelProperty(name="order_sn", value = "订单编号")
    private String orderSn;

    @ApiModelProperty(name="member_nickname", value = "买家昵称")
    private String memberNickname;

    @ApiModelProperty(name="claim_status", value = "理赔状态")
    private String claimStatus;

    @ApiModelProperty(name="claim_type", value = "理赔类型")
    private String claimType;

    @ApiModelProperty(name = "member_realname_lv1", value = "一级团长真实姓名")
    private String memberRealnameLv1;

    @ApiModelProperty(name = "site_name", value = "站点名")
    private String siteName;
}
