package com.enation.app.javashop.core.distribution.service.pattern;

import com.enation.app.javashop.core.distribution.model.dos.DistributionOrderDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;

public class DistributionContext {

    private DistributionStrategy distributionStrategy;

    public DistributionContext(DistributionStrategy distributionStrategy){
        this.distributionStrategy = distributionStrategy;
    }

    /**
     * 执行决定哪些团长是收益的人
     */
    public void executeDistributors(OrderDO orderDO, DistributionOrderDO distributionOrderDO){
        distributionStrategy.distributors(orderDO, distributionOrderDO);
    }


    /**
     * 执行计算收益
     */
    public void executeCountProfit(OrderDO orderDO, DistributionOrderDO distributionOrderDO){
        distributionStrategy.countProfit(orderDO, distributionOrderDO);
    }
}
