package com.enation.app.javashop.core.distribution.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分享DO
 */
@Data
@Table(name="es_share")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ShareDO {

    @Id(name="id")
    @ApiModelProperty(hidden=true)
    private Integer id;

    @Column()
    @ApiModelProperty(value="uuid")
    private String uuid;

    @Column(name = "member_id")
    @ApiModelProperty(value="会员id")
    private Integer memberId;

    @Column(name = "share_key")
    @ApiModelProperty(value="分享标识")
    private String shareKey;

    @Column()
    @ApiModelProperty(value="跳转地址")
    private String url;

}
