package com.enation.app.javashop.core.aftersale.service;

import com.enation.app.javashop.core.aftersale.model.dos.ExceptionOrderDO;
import com.enation.app.javashop.core.aftersale.model.dto.ExceptionOrderDTO;
import com.enation.app.javashop.core.aftersale.model.vo.*;
import com.enation.app.javashop.core.system.enums.MessageCodeEnum;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;

import java.util.List;
import java.util.Map;

/**
 * @author 王志杨
 * @since 2020年9月21日 11:45:26
 * 异常订单业务层
 */
public interface ExceptionOrderManager {
    /**
     *上报异常单
     * @param exceptionOrderDTO 异常单实体DTO
     * @param seller 登录的商户
     * @return Result<ExceptionOrderDTO> 封装的返回结果
     */
    Result<ExceptionOrderDTO> insertExceptionOrder(ExceptionOrderDTO exceptionOrderDTO, Seller seller);

    /**
     * 异常单分页查询
     * @param exceptionQueryParamVO 异常单分页查询参数
     * @return Page<ExceptionOrderVO> 异常单分页对象
     */
    Page<ExceptionOrderVO> pageExceptionOrder(ExceptionQueryParamVO exceptionQueryParamVO);

    /**
     * 根据异常单ID查询处理异常展示所需组合类
     * @param exceptionId 异常单ID
     * @return Result 包含异常处理展示组合类
     */
    Result queryExceptionStructVO(Integer exceptionId);

    /**
     * 根据异常单ID数组批量修改异常单状态
     * @param exceptionIds 异常单ID数组
     * @param seller 卖家信息
     */
    Result batchUpdateExceptionOrderStatus(Integer[] exceptionIds, Seller seller);

    /**
     * 根据异常ID的数组查询异常
     * @param exceptionIds 异常id数组
     */
    List<ExceptionOrderDO> queryListByIds(Integer[] exceptionIds);

    /**
     * 批量异常撤销
     * @param exceptionIds 异常id数组
     * @param seller 卖家信息
     */
    Result batchCancel(Integer[] exceptionIds, Seller seller);

    /**
     * 批量上报异常
     * @param insertList 添加的异常集合
     */
    Result batchInsertException(List<ExceptionOrderDTO> insertList, Seller seller);

    /**
     * 处理异常单后，添加理赔方式，更新
     * 异常单状态，处理人信息，解决说明和解决方式
     */
    void updateSolve(ExceptionOrderDTO exceptionOrderDTO);

    /**
     * 查询异常类型枚举
     */
    Map<String, List<ExceptionEnumStructs>> queryExceptionTypes();

    /**
     * 查询异常上报来源枚举
     */
    Map<String, List<ExceptionEnumStructs>> queryExceptionSources();

    /**
     * 查询异常状态枚举
     */
    Map<String, List<ExceptionEnumStructs>> queryExceptionStatus();

    /**
     * 查询异常时间类型枚举
     */
    Map<String, List<ExceptionEnumStructs>> queryExceptionTimeType();

    /**
     * 校验此异常是否进行过理赔，每条异常单只能生成一次理赔单，但每个订单可以重复生成异常单
     * @param exceptionId 异常单ID
     */
    ExceptionOrderDO checkAbleProcess(Integer exceptionId);

    /**
     * 获取售后评价模板并填充参数
     * @param messageCodeEnum 售后服务评分提醒短信模板枚举
     * @param orderSn 订单编号
     * @return 短信内容
     */
    String getAfterSaleTemplate(MessageCodeEnum messageCodeEnum, String orderSn);

}
