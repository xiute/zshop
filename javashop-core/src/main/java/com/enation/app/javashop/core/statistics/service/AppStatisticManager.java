package com.enation.app.javashop.core.statistics.service;

import com.enation.app.javashop.core.shop.model.vo.ShopInfoVO;

/**
 * 商户app 首页数据
 * @author xlg
 * 2020/10/21 15:26
 */
public interface AppStatisticManager {
    /**
     * 是否是社区团购店，获取店铺所在城市
     */
    ShopInfoVO getShopInfo();
    //------------------------
    /**
     *  支付订单数
     */
    Integer paidOrderNum();

    /**
     * 交易额
     */
    Double tradeAmount();

    /**
     * 客单价
     */
    Double orderAvgPrice();

    /**
     * 团长总数
     */
    Integer distributorNum();

    /**
     * 团均订单数
     */
    Double distributorOrderNum();

    /**
     * 团长出单率
     */
    Double distributorOrderRatio();

    /**
     * 自提点总数
     */
    Integer siteNum();

    /**
     * 点均订单数
     */
    Double siteOrderNum();

    /**
     * 自提点出单率
     */
    Double siteOrderRatio();

    // -----------------中间数据----------------------
    /**
     * 待发货 总数
     */
    String waitPayOrderNum();
    /**
     * 待退款 总数
     */
    String afterSaleOrderNum();
    // -------------------昨日经营情况--------------------------

    /**
     * 昨天支付订单数
     */
    Integer lastDayPaidOrderNum();

    /**
     * 昨天 交易额
     */
    Double lastDayTradeAmount();

    /**
     * 昨日客单价
     */
    Double lastDayOrderAvgPrice();

    /**
     * 昨日出单团数
     */
    Integer lastDayActiveDistributorNum();

    /**
     * 昨日新增团长
     */
    Integer lastDayNewDistributorNum();

    /**
     * 昨日新增自提点
     */
    Integer lastDayNewSiteNum();
}
