package com.enation.app.javashop.core.payment.plugin.walletpay;

import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.payment.model.dos.PaymentMethodDO;
import com.enation.app.javashop.core.payment.model.enums.ClientType;
import com.enation.app.javashop.core.payment.model.enums.PaymentPluginEnum;
import com.enation.app.javashop.core.payment.model.enums.TradeType;
import com.enation.app.javashop.core.payment.model.vo.ClientConfig;
import com.enation.app.javashop.core.payment.model.vo.Form;
import com.enation.app.javashop.core.payment.model.vo.PayBill;
import com.enation.app.javashop.core.payment.model.vo.RefundBill;
import com.enation.app.javashop.core.payment.plugin.walletpay.executor.WalletPaymentExecutor;
import com.enation.app.javashop.core.payment.plugin.walletpay.executor.WalletRefundExecutor;
import com.enation.app.javashop.core.payment.service.*;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 王志杨
 * @since 2020/10/15 15:22
 * 钱包支付插件
 */
@Service
public class WalletPayPlugin implements PaymentPluginManager {

    @Autowired
    private WalletPaymentExecutor walletPaymentExecutor;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private WalletRefundExecutor walletRefundExecutor;

    @Autowired
    private RefundManager refundManager;

    @Autowired
    private PaymentBillManager paymentBillManager;

    @Override
    public String getPluginId() {
        return PaymentPluginEnum.walletPayPlugin.toString();
    }

    @Override
    public String getPluginName() {
        return PaymentPluginEnum.walletPayPlugin.getDescription();
    }

    @Override
    public List<ClientConfig> definitionClientConfig() {
        return null;
    }

    @Override
    public Form pay(PayBill bill) {
        return walletPaymentExecutor.pay(bill);
    }

    @Override
    public void onReturn(TradeType tradeType) {
        //同步回调，钱包支付暂无回调
    }

    @Override
    public String onCallback(TradeType tradeType, ClientType clientType) {
        //异步回调，钱包支付暂无回调
        return null;
    }

    @Override
    public String onQuery(PayBill bill) {
        return walletPaymentExecutor.onQuery(bill);
    }

    @Override
    public boolean onTradeRefund(RefundBill bill) {
        return walletRefundExecutor.returnPay(bill);
    }

    @Override
    public String queryRefundStatus(RefundBill bill) {
        return walletRefundExecutor.queryRefundStatus(bill);
    }

    //支持原路退款
    @Override
    public Integer getIsRetrace() {
        return 1;
    }

    @Override
    public PayBill createPaymentBill(PayBill payBill, PaymentMethodDO paymentMethod) {
        paymentManager.createPaymentBill(payBill, paymentMethod);
        return payBill;
    }

    @Override
    public void createRefundItem(RefundDO refundDO, OrderDetailDTO orderDetail) {
        PaymentBillDO paymentBill = this.paymentBillManager.getBillBySnAndTradeTypeAndPaymentPluginID(refundDO.getOrderSn(), orderDetail.getTradeType(), this.getPluginId());
        paymentBill.setTradePrice(refundDO.getRefundPrice());
        refundManager.createRefundItem(refundDO, paymentBill);
    }
}
