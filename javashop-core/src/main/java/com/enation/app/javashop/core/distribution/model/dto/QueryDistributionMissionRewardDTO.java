package com.enation.app.javashop.core.distribution.model.dto;

import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: zhou
 * @Date: 2021/1/26
 * @Description: 团长任务奖励查询参数
 */
@Data
public class QueryDistributionMissionRewardDTO {

    // 任务明细类型（0：每日任务，1：每月任务）
    @ApiModelProperty(name = "mission_type", value = "任务明细类型（0：每日任务，1：每月任务）")
    private Integer missionType;

    // 团长名
    @ApiModelProperty(name = "member_name", value = "团长名")
    private String memberName;

    // 奖励类型
    @ApiModelProperty(name = "rewards_type", value = "奖励类型")
    private Integer rewardsType;

    // 奖励状态 0未发放 1已发放 2已失效
    @ApiModelProperty(name = "reward_status", value = "奖励状态 0未发放 1已发放 2已失效")
    private Integer rewardStatus;

    // 会员id
    @ApiModelProperty(name = "member_id", value = "会员id")
    private Integer memberId;

    @ApiModelProperty(name = "page_no", value = "页码")
    @NotNull(message = "页码不能为空")
    private int pageNo;

    @ApiModelProperty(name = "page_size", value = "每页显示数量")
    @NotNull(message = "每页显示数不能为空")
    private int pageSize;
}
