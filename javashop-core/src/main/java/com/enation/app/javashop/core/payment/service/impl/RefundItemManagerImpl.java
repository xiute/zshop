package com.enation.app.javashop.core.payment.service.impl;

import com.enation.app.javashop.core.aftersale.model.dos.RefundItemDO;
import com.enation.app.javashop.core.aftersale.model.enums.RefundItemStatusEnum;
import com.enation.app.javashop.core.payment.service.RefundItemManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 退款详情接口
 * @author 王志杨
 * @since 2020-10-26 10:09:17
 */
@Service
public class RefundItemManagerImpl implements RefundItemManager {

    @Autowired
    private DaoSupport daoSupport;


    @Override
    public void insertRefundItem(RefundItemDO refundItemDO) {
        this.daoSupport.insert(refundItemDO);
    }

    @Override
    public List<RefundItemDO> queryRefundItemByRefundIdAndStatus(Integer refundId, String itemStatus) {
        return this.daoSupport.queryForList("select * from es_refund_item where refund_id=? and item_status = ?"
                ,RefundItemDO.class, refundId, itemStatus);
    }

    @Override
    public List<RefundItemDO> queryApplyRefundItemByOrderSn(String orderSn) {
        return this.daoSupport.queryForList("select * from es_refund_item where order_sn=? and item_status = ?"
                ,RefundItemDO.class, orderSn, RefundItemStatusEnum.APPLY.value());
    }

    @Override
    public Double queryRefundPriceByPluginId(String orderSn, String paymentPluginId) {
        String sql = "select IFNULL(SUM(refund_price),0.00) refund_price from es_refund_item where order_sn=? and item_status in('SUCCESS','APPLY') and payment_plugin_id=?";
        return this.daoSupport.queryForDouble(sql, orderSn, paymentPluginId);
    }

    @Override
    public RefundItemDO queryRefundItemByRefundIdAndPaymentPluginId(Integer refundId, String paymentPluginId) {
        String sql = "select * from es_refund_item where refund_id=? and payment_plugin_id=? and item_status in(?,?) limit 0,1";
        return this.daoSupport.queryForObject(sql, RefundItemDO.class, refundId, paymentPluginId, RefundItemStatusEnum.SUCCESS.value(), RefundItemStatusEnum.FAIL.value());
    }

    @Override
    public void updateRefundItem(RefundItemDO refundItem) {
        this.daoSupport.update(refundItem, refundItem.getItemId());
    }

    @Override
    public RefundItemDO queryRefundItemByPayOrderNo(String payOrderNo, String refundSn) {
        String sql = "select * from es_refund_item where pay_order_no=? and refund_sn=? limit 0,1";
        return this.daoSupport.queryForObject(sql, RefundItemDO.class, payOrderNo, refundSn);
    }

    @Override
    public void updateRefundItemDisabled(String refundSn) {
        String sql = "update es_refund_item set item_status=? where refund_sn=? ";
        this.daoSupport.execute(sql, RefundItemStatusEnum.DISABLED.value(), refundSn);
    }

    @Override
    public double querySuccessRefundPriceByRefundSn(String refundSn, String paymentPluginId) {
        String sql = "select IFNULL(SUM(refund_price),0.00) refund_price from es_refund_item where refund_sn=? and item_status=? and payment_plugin_id=?";
        return this.daoSupport.queryForDouble(sql, refundSn, RefundItemStatusEnum.SUCCESS.value(), paymentPluginId);
    }
}
