package com.enation.app.javashop.core.goods.model.enums;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2020/11/30
 * @Description:
 */
public enum GoodsTagStatus {
    ENABLE(0, "启用"),
    DISABLE(1, "禁用"),
    DELETE(-1, "删除");

    private int index;
    private String text;


    GoodsTagStatus(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

}
