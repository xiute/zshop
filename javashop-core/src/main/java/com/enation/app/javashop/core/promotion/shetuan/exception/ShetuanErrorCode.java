package com.enation.app.javashop.core.promotion.shetuan.exception;

/**
 * PromotionErrorCode
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2019-02-19 下午1:31
 */
public enum ShetuanErrorCode {
    E5011("非社区团购商品,无法添加购物车"),
    E5012("非活动时间，无法开启/关闭活动"),
    E5013("非法操作"),

    E5014("积分商品无法参加"),
    E5015("活动商品无法参加"),
    E5016("拼团商品无法参与"),
    E5017("活动正在进行，无法操作，如要操作请联系管理员"),
    E5018("超出限购数量"),

    E6001("社区团购，时间配置错误");

    private String describe;

    ShetuanErrorCode(String des) {
        this.describe = des;
    }

    /**
     * 获取商品的异常码
     *
     * @return
     */
    public String code() {
        return this.name().replaceAll("E", "");
    }

    /**
     * 获取错误提示
     *
     * @return
     */
    public String describe() {
        return this.describe;
    }

}
