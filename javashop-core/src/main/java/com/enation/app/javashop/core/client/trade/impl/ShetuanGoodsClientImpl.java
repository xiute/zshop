package com.enation.app.javashop.core.client.trade.impl;

import com.enation.app.javashop.core.client.trade.ShetuanGoodsClient;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanGoodsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;


@Service
@ConditionalOnProperty(value = "javashop.product", havingValue = "stand")
public class ShetuanGoodsClientImpl implements ShetuanGoodsClient {
    @Autowired
    private ShetuanGoodsManager shetuanGoodsManager;

}
