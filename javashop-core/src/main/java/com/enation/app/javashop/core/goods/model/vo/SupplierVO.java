package com.enation.app.javashop.core.goods.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class SupplierVO implements Serializable {

    /**
     * 主键
     */
    @ApiModelProperty(name="id",hidden=true)
    private Integer id;

    /**
     * 供应商编码
     */
    @ApiModelProperty(name="supplier_code",value = "供应商编码")
    private String supplierCode;

    /**
     * 供应商名称
     */
    @ApiModelProperty(name="supplier_name",value = "供应商名称")
    private String supplierName;

    /**
     * 关联店铺id
     */
    @ApiModelProperty(name="shop_id",value = "关联店铺id")
    private Integer shopId;

    /**
     * 供应品类
     */
    @ApiModelProperty(name="supply_category",value = "供应品类")
    private String supplyCategory;

    /**
     * 供应商类型
     */
    @ApiModelProperty(name="supplier_type",value = "供应商类型")
    private String supplierType;

    /**
     * 采购方式
     */
    @ApiModelProperty(name="procurement_method",value = "采购方式")
    private String procurementMethod;

    /**
     * 供应商等级
     */
    @ApiModelProperty(name="supplier_level",value = "供应商等级")
    private String supplierLevel;

    /**
     * 结算方式
     */
    @ApiModelProperty(name="settle_mode",value = "结算方式")
    private String settleMode;

    /**
     * 对账日期
     */
    @ApiModelProperty(name="reconciliation_date",value = "对账日期")
    private String reconciliationDate;

    /**
     * 结算账期
     */
    @ApiModelProperty(name="settlement_period",value = "结算账期")
    private String settlementPeriod;

    /**
     * 联系人
     */
    @ApiModelProperty(name="contacts",value = "联系人")
    private String contacts;

    /**
     * 联系方式
     */
    @ApiModelProperty(name="contact_mobile",value = "联系方式")
    private String contactMobile;

    /**
     * 省id
     */
    @ApiModelProperty(name="province_id",value = "省id",hidden = true)
    private Integer provinceId;

    /**
     * 省
     */
    @ApiModelProperty(name="province",value = "省",hidden = true)
    private String province;

    /**
     * 城市id
     */
    @ApiModelProperty(name="city_id",value = "城市id",hidden = true)
    private Integer cityId;

    /**
     * 城市
     */
    @ApiModelProperty(name="city",value = "城市",hidden = true)
    private String city;

    /**
     * 区/县Id
     */
    @ApiModelProperty(name="county_id",value = "区/县Id",hidden = true)
    private Integer countyId;

    /**
     * 区/县
     */
    @ApiModelProperty(name="county",value = "区/县",hidden = true)
    private String county;

    /**
     * 详细地址
     */
    @ApiModelProperty(name="detailed_address",value = "详细地址")
    private String detailedAddress;

    /**
     * 供应商状态 1 启用 0 禁用
     */
    @ApiModelProperty(name="status",value = "供应商状态 1 启用 0 禁用")
    private Integer status;

    /**
     * 备注
     */
    @ApiModelProperty(name="remarks",value = "备注")
    private String remarks;

    @ApiModelProperty(name="pag_size",value = "每页条数")
    private Integer pageSize;

    @ApiModelProperty(name="page_no",value = "页数")
    private Integer pageNo;
}
