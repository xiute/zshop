package com.enation.app.javashop.core.promotion.seckill.model.vo;

import com.enation.app.javashop.core.promotion.seckill.model.dos.SeckillDO;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.exception.SystemErrorCodeV1;
import com.enation.app.javashop.framework.util.DateUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Snow
 * @version 1.0
 * @since 6.4.1
 * 2017年12月14日 16:58:55
 */
@ApiModel(description = "限时抢购活动vo")
@Data
public class SeckillVO extends SeckillDO {

	@ApiModelProperty(name = "range_list",value = "活动时刻表")
	@Size(min=1, max=23)
	private List<Integer> rangeList;

	@ApiModelProperty(name = "0:未报名,1:已报名,2:已截止" )
	private Integer isApply;

	@ApiModelProperty(name = "seckill_status_text",value="状态值")
	private String seckillStatusText;

	@ApiModelProperty(name = "各时间段商品列表" )
	private List<TimeLineGoods> timeLineGoodsList;

	/**
	 * 验证参数
	 * @param seckillVO
	 */
	public void verifyParam(SeckillVO seckillVO){
		//获取活动开始时间
		long startDay = seckillVO.getStartDay();

		//获取当前时间
		long currentTime = DateUtil.getDateline();

		//获取当天开始时间
		String currentDay = DateUtil.toString(currentTime, "yyyy-MM-dd");
		long currentStartTime = DateUtil.getDateline(currentDay + " 00:00:00", "yyyy-MM-dd HH:mm:ss");

		//活动时间小于当天开始时间
		if(startDay<currentStartTime){
			throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"活动时间不能小于当前时间");
		}

		List<Integer> termList = new ArrayList<>();
		for(Integer time : seckillVO.getRangeList()){
			if(termList.contains(time)){
				throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"抢购区间的值不能重复");
			}
			//抢购区间的值不在0到23范围内
			if(time<0||time>23){
				throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER,"抢购区间必须在0点到23点的整点时刻");
			}
			termList.add(time);
		}
	}
}
