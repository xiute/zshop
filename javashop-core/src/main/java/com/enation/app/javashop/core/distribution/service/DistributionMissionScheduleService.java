package com.enation.app.javashop.core.distribution.service;

import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionDetailDO;
import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionScheduleDO;
import com.enation.app.javashop.core.distribution.model.vo.DistributionMissionScheduleVO;
import com.enation.app.javashop.core.distribution.model.vo.DistributionMissionVO;
import com.enation.app.javashop.core.distribution.model.vo.QueryDistributionMissionScheduleVO;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.framework.database.Page;

/**
 * @author 孙建
 */
public interface DistributionMissionScheduleService {

    DistributionMissionScheduleDO createMissionSchedule(Member member, DistributionMissionVO distributionMission, DistributionMissionDetailDO distributionMissionDetailDO, String missionTime);

    void updateMissionSchedule(DistributionMissionScheduleDO distributionMissionScheduleDO);

    DistributionMissionScheduleDO queryDistributionMissionSchedule(Integer memberId,Integer missionId, Integer missionDetailId, Integer missionType, String missionTime);

    void increase(DistributionMissionVO distributionMission, DistributionMissionDetailDO distributionMissionDetailDO, DistributionMissionScheduleDO missionScheduleDO, OrderDO orderDO);

    void decrease(DistributionMissionVO distributionMission, DistributionMissionDetailDO distributionMissionDetailDO, DistributionMissionScheduleDO missionScheduleDO, OrderDO orderDO);

    Page<DistributionMissionScheduleVO> queryMissionScheduleList(QueryDistributionMissionScheduleVO queryDistributionMissionScheduleVO);

    void deleteDistributionMissionScheduleByMissionId(Integer missionId);

}
