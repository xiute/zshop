package com.enation.app.javashop.core.goods.model.vo;

import lombok.Data;

@Data
public class CategoryCountVO  {
 private Integer categoryId;
 private Integer count;

}


