package com.enation.app.javashop.core.app.model;

import lombok.Data;

/**
 * 参数信息
 */
@Data
public class Message {

    // 消息内容本身
    private String msg_content;

    // 标题
    private String title;

    // 内容类型 1调度单指派
    private String content_type;
}
