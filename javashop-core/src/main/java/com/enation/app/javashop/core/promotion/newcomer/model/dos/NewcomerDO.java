package com.enation.app.javashop.core.promotion.newcomer.model.dos;

import java.io.Serializable;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import lombok.Data;

/**
 * @Author: zhou
 * @Date 2021-02-25 10:05:27
 * @Description: 新人活动
 */
@Data
@Table( name ="es_newcomer" )
public class NewcomerDO  implements Serializable {
	private static final long serialVersionUID =  3505208394442248248L;

	@Id(name = "newcomer_id" )
	private Integer newcomerId;

	/**
	 * 新人专享活动名
	 */
   	@Column(name = "newcomer_name" )
	private String newcomerName;

	/**
	 * 活动状态
	 */
   	@Column(name = "status" )
	private Integer status;

	/**
	 * 每人限购数量
	 */
   	@Column(name = "limit_num" )
	private Integer limitNum;

	/**
	 * 活动所属店铺
	 */
   	@Column(name = "seller_id" )
	private Integer sellerId;

   	@Column(name = "seller_name" )
	private String sellerName;

	/**
	 * 活动描述
	 */
   	@Column(name = "newcomer_desc" )
	private String newcomerDesc;

	/**
	 * 活动开始时间
	 */
   	@Column(name = "start_time" )
	private Long startTime;

	/**
	 * 活动结束时间
	 */
   	@Column(name = "end_time" )
	private Long endTime;

   	@Column(name = "create_time" )
	private Long createTime;

   	@Column(name = "update_time" )
	private Long updateTime;

	/**
	 * 是否删除
	 */
   	@Column(name = "is_delete" )
	private Integer isDelete;

}
