package com.enation.app.javashop.core.base.message;

import com.enation.app.javashop.core.member.model.dos.MemberComment;

import java.io.Serializable;
import java.util.List;

/**
 * 商品评论消息
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018年3月23日 上午10:37:41
 */
public class GoodsCommentMsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8978542323509463579L;

	private List<MemberComment> comment;

	public List<MemberComment> getComment() {
		return comment;
	}

	public void setComment(List<MemberComment> comment) {
		this.comment = comment;
	}
}
