package com.enation.app.javashop.core.promotion.luck.service;

import com.enation.app.javashop.core.promotion.luck.model.dos.LuckPrizeDO;

import java.util.List;

/**
 * @author 孙建
 */
public interface LuckPrizeManager {

    List<LuckPrizeDO> queryListByLuckId(Integer luckId);

    void addAndUpdateLuckPrize(List<LuckPrizeDO> prizeDOList);

    void updateLuckPrize(LuckPrizeDO luckPrizeDO);

}
