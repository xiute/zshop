package com.enation.app.javashop.core.promotion.shetuan.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 结算单状态枚举
 * @author yanlin
 * @version v1.0
 * @since v7.0.0
 * @date 2018年4月15日 上午10:34:30
 */
public enum ShetuanGoodsStatusEnum {

	/**
	 * 待审核
	 */
	UN_CHECK(10,"待审核"),

	/**
	 * 报名中
	 */
	CHECKED(20,"待上架"),

	/**
	 * 报名中
	 */
	REJECT(30,"审核拒绝"),

	/**
	 * 待上线
	 */
	OFF_LINE(40,"已下架"),

	ON_LINE(50,"已上架"),

	DELETE(0,"已删除");

	private String description;

	private int index;

	ShetuanGoodsStatusEnum(int index, String description) {
		this.description = description;
		this.index = index;
	}

	public int getIndex(){
		return index;
	}

	public String getDescription() {
		return description;
	}

	private static Map<Integer, ShetuanGoodsStatusEnum> enumMap = new HashMap<>();

	static {
		for (ShetuanGoodsStatusEnum item : ShetuanGoodsStatusEnum.values()) {
			enumMap.put(item.getIndex(), item);
		}
	}


	public static String getTextByIndex(int index){
		final ShetuanGoodsStatusEnum enumValue = enumMap.get(index);
		return enumValue == null ? null : enumValue.getDescription();
	}

	public static ShetuanGoodsStatusEnum resolve(int index){
		return enumMap.get(index);
	}

}



