package com.enation.app.javashop.core.member.model.vo;

import lombok.Data;

import java.util.Date;

/**
 * 邀请人信息
 */
@Data
public class MemberInviterVO {

    // 邀请人id
    private Integer inviterMemberId;

    // 被邀请人id
    private Integer memberId;
    // 头像
    private String face;
    // 邀请时间
    private Long inviteTime;
    // 昵称
    private String nickName;
    // 手机号
    private String  mobile;
    // 邀请时间
    private String inviteTimeString;
    //邀请人数
    private Integer inviterNum;
    // 获得奖励
    private Double reward;

    // 描述一
    private String describe1;




}
