package com.enation.app.javashop.core.promotion.luck.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 孙建
 * 抽奖活动DO
 */
@Table(name="es_luck")
@Data
public class LuckDO implements Serializable {

    /**
     * 抽奖活动ID
     */
    @Id(name="luck_id")
    private Integer luckId;
    /**
     * 活动名称
     */
    @Column(name = "luck_name")
    private String luckName;
    /**
     * 活动标题
     */
    @Column(name = "luck_title")
    private String luckTitle;
    /**
     * 开始时间
     */
    @Column(name = "start_time")
    private Long startTime;
    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Long endTime;
    /**
     * 抽奖门槛
     */
    @Column(name = "min_limit_price")
    private Double minLimitPrice;
    /**
     * 每日最大抽奖次数
     */
    @Column(name = "day_max_times")
    private Integer dayMaxTimes;
    /**
     * 抽奖活动描述
     */
    @Column(name = "luck_desc")
    private String luckDesc;
    /**
     * 活动状态
     */
    @Column(name = "luck_status")
    private Integer luckStatus;
    /**
     * 店铺ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;
    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    private String sellerName;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 创建人
     */
    @Column(name = "create_name")
    private String createName;
    /**
     * 修改人
     */
    @Column(name = "update_name")
    private String updateName;
    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Long updateTime;
    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

}