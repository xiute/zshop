package com.enation.app.javashop.core.distribution.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: zhou
 * @Date: 2021/1/26
 * @Description: 任务奖励
 */
@Table(name="es_distribution_mission_reward")
@Data
public class DistributionMissionRewardDO implements Serializable {

    @Id(name = "id")
    private Integer id;

    // 任务明细类型（0：每日任务，1：每月任务）
    @Column(name = "mission_type")
    private Integer missionType;

    // 任务期间
    @Column(name = "mission_time")
    private String missionTime;

    // 任务id
    @Column(name = "mission_id")
    private Integer missionId;

    // 任务详情id
    @Column(name = "mission_detail_id")
    private Integer missionDetailId;

    // 任务进度id
    @Column(name = "mission_schedule_id")
    private Integer missionScheduleId;

    // 奖励类型
    @Column(name = "rewards_type")
    private Integer rewardsType;

    // 店铺ID
    @Column(name = "seller_id")
    private Integer sellerId;

    // 店铺名
    @Column(name = "seller_name")
    private String sellerName;
    /**
     * 奖励金额
     */
    @Column(name = "reward_money")
    private BigDecimal rewardMoney;

    // 奖励状态 0未发放 1已发放 2已失效
    @Column(name = "reward_status")
    private Integer rewardStatus;

    // 失效原因
    @Column(name = "invalid_reason")
    private String invalidReason;

    /**
     * 会员id
     */
    @Column(name = "member_id")
    private Integer memberId;

    // 团长名
    @Column(name = "member_name")
    private String memberName;

    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "update_time")
    private Long updateTime;
}
