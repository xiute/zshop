package com.enation.app.javashop.core.goods.service.impl;

import com.enation.app.javashop.core.goods.model.vo.SupplierVO;
import com.enation.app.javashop.core.goods.service.SupplierManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 供应商业务层
 * @author xlg
 * @version v1.0
 * 2020-12-29 18:26
 */
@Service
public class SupplierManagerImpl implements SupplierManager{

    @Autowired
    private DaoSupport daoSupport;

    @Override
    public Page<SupplierVO> querySupplierList(SupplierVO supplierVO) {
        StringBuffer sql = new StringBuffer("SELECT * from es_supplier where 1=1");
        List<Object> term = new ArrayList<>();

        // 供应商编码
        String supplierCode = supplierVO.getSupplierCode();
        if(StringUtil.notEmpty(supplierCode)){
            sql.append(" and supplier_code = ?");
            term.add(supplierCode);
        }

        // 供应商名称
        String supplierName = supplierVO.getSupplierName();
        if(StringUtil.notEmpty(supplierName)){
            sql.append(" and supplier_name like ?");
            term.add("%"+supplierName+"%");
        }

        // 供应商类型
        String supplierType = supplierVO.getSupplierType();
        if(StringUtil.notEmpty(supplierType)){
            sql.append(" and supplier_type =  ?");
            term.add(supplierType);
        }
        // 供应商品类
        String supplyCategory = supplierVO.getSupplyCategory();
        if(StringUtil.notEmpty(supplyCategory)){
            sql.append(" and supply_category =  ?");
            term.add(supplyCategory);
        }
        // 关联店铺
        Integer shopId = supplierVO.getShopId();
        if(shopId != null){
            sql.append(" and shop_id =  ?");
            term.add(shopId);
        }
        // 供应商等级
        String supplierLevel = supplierVO.getSupplierLevel();
        if(StringUtil.notEmpty(supplierLevel)){
            sql.append(" and supplier_level =  ?");
            term.add(supplierLevel);
        }
        // 供应商状态
        Integer status = supplierVO.getStatus();
        if(status != null){
            sql.append(" and status =  ?");
            term.add(status);
        }
        // 采购方式
        String procurementMethod = supplierVO.getProcurementMethod();
        if(StringUtil.notEmpty(procurementMethod)){
            sql.append(" and procurement_method =  ?");
            term.add(procurementMethod);
        }
        return daoSupport.queryForPage(sql.toString(),supplierVO.getPageNo(),supplierVO.getPageSize(),SupplierVO.class,term.toArray());
    }

    @Override
    public void addSupplier(SupplierVO supplierDTO) {

    }

    @Override
    public void updateSupplier(SupplierVO supplierDTO) {

    }

    @Override
    public void delSupplier(Integer id) {

    }
}
