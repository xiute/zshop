package com.enation.app.javashop.core.member.model.vo;

import com.enation.app.javashop.core.shop.model.enums.ShopStatusEnum;
import lombok.Data;

/**
 * 商家登录后返回信息
 *
 * @author zh
 * @version v7.0
 * @date 18/8/15 下午1:54
 * @since v7.0
 */
@Data
public class SellerInfoVO extends MemberVO {

    /**
     * 店铺状态
     */
    private String shopStatus;
    /**
     * 角色id
     */
    private Integer roleId;

    /**
     * 店铺ID
     */
    private Integer sellerId;
    /**
     * 是否是店主
     */
    private Integer founder;

    private String statusReason;

    private String shopName;

    private String shopLogo;

    public void checkStatusReason() {
        if(shopStatus.equals(ShopStatusEnum.REFUSED.name())){
          this.statusReason="店铺申请信息不完整，请按要求填写!";
        }
        if(shopStatus.equals(ShopStatusEnum.CLOSED.name())){
          this.statusReason="店铺不满足运营资质，被关闭!";
        }
    }
}
