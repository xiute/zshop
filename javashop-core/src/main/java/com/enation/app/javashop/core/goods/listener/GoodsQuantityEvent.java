package com.enation.app.javashop.core.goods.listener;

import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import org.springframework.context.ApplicationEvent;

/**
 * @author JFeng
 * @date 2021/1/13 9:33
 */

public class GoodsQuantityEvent extends ApplicationEvent {
    private static final long serialVersionUID = 6179932363487598961L;

    private String msg;

    private OrderDO orderDO;

    private QuantityTypeEnum quantityType;

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public GoodsQuantityEvent(Object source) {
        super(source);
    }

    public GoodsQuantityEvent(Object source, String msg, QuantityTypeEnum quantityType) {
        super(source);
        this.msg = msg;
        this.quantityType = quantityType;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public QuantityTypeEnum getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(QuantityTypeEnum quantityType) {
        this.quantityType = quantityType;
    }

    public OrderDO getOrderDO() {
        return orderDO;
    }

    public void setOrderDO(OrderDO orderDO) {
        this.orderDO = orderDO;
    }
}
