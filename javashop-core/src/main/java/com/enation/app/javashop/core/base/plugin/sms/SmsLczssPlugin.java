package com.enation.app.javashop.core.base.plugin.sms;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.enation.app.javashop.core.base.model.vo.ConfigItem;
import com.enation.app.javashop.core.base.plugin.express.util.MD5;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 联诚智胜短信工具类
 */
@Component
public class SmsLczssPlugin  implements SmsPlatform {

    private static Logger logger = Logger.getLogger(SmsLczssPlugin.class);

    private static final Gson GSON = new Gson();

    // 生产
    private static final String URL = "http://47.102.112.118:8080/sdk/smssdk!mt2.action";
    // 测试
    // private static final String URL = "http://101.132.189.89:8080/wxrysms/sdk/smssdk!mt2.action";
    //账号
   /* private static final String USERNAME = "600185";
    //密码
    private static final String PASSWORD = "miaoji01";
    //子号
    private static final String SUB_CODE = "21001";*/


    @Override
    public List<ConfigItem> definitionConfigItem() {
        List<ConfigItem> list = new ArrayList<>();


        ConfigItem name = new ConfigItem();
        name.setType("text");
        name.setName("userName");
        name.setText("用户名");

        ConfigItem password = new ConfigItem();
        password.setType("text");
        password.setName("password");
        password.setText("密码");

        ConfigItem id = new ConfigItem();
        id.setType("text");
        id.setName("subCode");
        id.setText("子号");


        return list;

    }

    /**
     * 发送短信
     */
    @Override
    public  boolean onSend(String mobile, String content, Map param){
        logger.debug("------------开始调用联诚智胜短信--------------");
        System.out.println("------------开始调用联诚智胜短信--------------");
        Map<String, Object> resultMap = new HashMap<>();

        String timestamp = String.valueOf(System.currentTimeMillis());
        // 生成签名
        String bodyString = (String)param.get("userName") + (String )param.get("password") + timestamp + mobile + content;
        String sign = MD5.encode(bodyString);
        // 参数
        Map<String, String> itemObj = new LinkedHashMap<> ();
        itemObj.put("srcnumber", (String)param.get("subCode"));
        itemObj.put("dstnumbers", mobile);
        itemObj.put("msgcontent", content);
        // 封装参数
        Map<String,Object> apiParams = new LinkedHashMap<>();
        apiParams.put("username", (String)param.get("userName"));
        apiParams.put("timestamp", timestamp);
        apiParams.put("checkhash", sign);
        apiParams.put("items", Lists.newArrayList(itemObj));
        String requestJson = GSON.toJson(apiParams);
        logger.info("联诚智胜短信请求报文:" + requestJson);
        try{
            String resultJson =  SmsLczssUtil.sendHttpClientPostJSON(URL, null, requestJson);
            logger.info("联诚智胜短信返回报文:" + resultJson);
            JSONObject jsonObject = JSON.parseObject(resultJson);
            String rescode = jsonObject.getString("rescode");
            boolean success = "0".equals(rescode);
            resultMap.put("success", success);
            if(success){
                resultMap.put("msgids", "LCZS" + jsonObject.getString("msgids"));
            }else{
                resultMap.put("restext", jsonObject.getString("restext"));
                return false;
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            logger.error("联诚智胜短信发送失败", e);
            resultMap.put("success", false);
            resultMap.put("restext", "联诚智胜短信发送失败" + e.getMessage());
            return false;
        }

    }



    @Override
    public String getPluginId() {
        return "smsLczssPlugin";
    }

    @Override
    public String getPluginName() {
        return "联诚智胜短信";
    }

    @Override
    public Integer getIsOpen() {
        return null;
    }

}
