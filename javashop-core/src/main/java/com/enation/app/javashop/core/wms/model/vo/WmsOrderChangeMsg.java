package com.enation.app.javashop.core.wms.model.vo;

import com.enation.app.javashop.core.wms.model.dos.WmsOrderDO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author JFeng
 * @date 2020/9/25 17:04
 */
@Data
public class WmsOrderChangeMsg implements Serializable {
    private static final long serialVersionUID = -5038346411691544478L;

    private List<String> orderSnList;
    private String operateType;
}
