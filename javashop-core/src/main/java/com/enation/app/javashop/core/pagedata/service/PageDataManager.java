package com.enation.app.javashop.core.pagedata.service;

import com.enation.app.javashop.core.base.context.Region;
import com.enation.app.javashop.core.pagedata.model.MenuDate;
import com.enation.app.javashop.core.pagedata.model.PageData;
import com.enation.app.javashop.core.pagedata.model.vo.PageListQueryVo;
import com.enation.app.javashop.framework.database.Page;

/**
 * 楼层业务层
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
public interface PageDataManager {

	/**
	 * 查询楼层列表
	 * @param page 页码
	 * @param pageSize 每页数量
	 * @return Page
	 */

	Page list(int page, int pageSize);
	/**
	 * 添加楼层
	 * @param page 楼层
	 * @return PageData 楼层
	 */
	PageData add(PageData page);

	/**
	* 修改楼层
	* @param page 楼层
	* @param id 楼层主键
	* @return PageData 楼层
	*/
	PageData edit(PageData page, Integer id);

	/**
	 * 删除楼层
	 * @param id 楼层主键
	 */
	void delete(Integer id);

	/**
	 * 获取楼层
	 * @param id 楼层主键
	 * @return PageData  楼层
	 */
	PageData getModel(Integer id);

	/**
	 * 查询数据
	 * @return
	 * @param clientType
	 * @param pageType
	 */
    PageData queryPageData(String clientType, String pageType);

	/**
	 * john 新增
	 * 根据页面id,查询页面数据
	 */
	PageData queryPageData(String clientType, String pageType,Integer pageId);

	/**
	 * 根据类型修改楼层
	 * @param pageData
	 * @return
	 */
	PageData editByType(PageData pageData);

	/**
	 * 根据类型查询数据
	 * @param clientType
	 * @param pageType
	 * @return
	 */
	PageData getByType(String clientType, String pageType);

	/**
	 * 查询当前首页
	 */
	PageData getOne(PageData pageData);


	/**
	 * 查询某个店铺的页面设计列表
	 */
	Page listOfStoreId(int page, int pageSize,String pageType,Integer storeId);

	/**
	 * 查询平台的页面设计列表
	 */
	Page listOfPlatform(PageListQueryVo pageListQueryVo);

	/**
	 * 查询某个店铺的页面设计列表
	 */
	Page listOfAgentId(int page, int pageSize,String pageType,Integer agentId);


	/**
	 * 获取城市的活动页
	 */
	PageData getDiscover(String clientType, String city);

	/**
	 * @auth john
	 * 将楼层进行上架
	 * @param id
	 * @return
	 */
	boolean putaway(Integer id);

	/**
	 * @auth john
	 * 将楼层进行下架
	 * @param id
	 * @return
	 */
	boolean soldOut(Integer id);


	boolean setIndexPage(Integer pageId,String homePageType, Region region);

	PageData getIndexPage(String clientType,String pageType, String city,Double lng,Double lat);

	PageData getCityIndexPage( String cityStr,String homePageType);

	//菜单配置——首页数据
	Page menuConfigList(PageListQueryVo pageListQueryVo);

	// 菜单配置——批量删除
	void delMenu(Integer[] ids);

	//菜单配置——添加接口
	MenuDate addMenu(MenuDate menuDate);

	//菜单配置——修改接口
	MenuDate udpMenu(MenuDate menuDate,Integer id);

	// 菜单配置 --- 复制接口
	void copyPageIndex(Integer pageId);

	/**
	 * 获取本地生活店铺所在的发现页
	 */
	PageData getLocalLifeShop(String clientType,Integer ownerType,String pageType, String town);

}
