package com.enation.app.javashop.core.aftersale.model.vo;

import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/9/23
 * @Description:
 */

public class ClaimsParamVO implements Serializable {

    private static final long serialVersionUID = -8184915517693445651L;

    @ApiModelProperty(value = "异常单ID")
    @Column(name = "exception_id")
    private Integer exceptionId;

    @ApiModelProperty(value = "异常单编号")
    @Column(name = "exception_sn")
    private String exceptionSn;

    @ApiModelProperty(value = "订单编号")
    @Column(name = "order_sn")
    private String orderSn;

    @ApiModelProperty(value = "理赔说明")
    @Column(name = "claim_describe")
    private String claimDescribe;

    @ApiModelProperty(value = "生成订单的用户ID")
    @Column(name = "member_id")
    private Integer memberId;

    @ApiModelProperty(value = "补发商品实体类")
    private ClaimsVO compensationClaim;

    @ApiModelProperty(value = "赠送礼品实体类")
    private ClaimsVO freeClaim;

    @ApiModelProperty(value = "赔付现金实体类")
    private ClaimsVO paymentClaim;

    @ApiModelProperty(value = "多退少补实体类")
    private ClaimsVO refundClaim;

    @ApiModelProperty(value = "售后优惠券实体类")
    private ClaimsVO couponClaim;

    @ApiModelProperty(value = "其它实体类")
    private ClaimsVO otherClaim;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getExceptionId() {
        return exceptionId;
    }

    public void setExceptionId(Integer exceptionId) {
        this.exceptionId = exceptionId;
    }

    public String getExceptionSn() {
        return exceptionSn;
    }

    public void setExceptionSn(String exceptionSn) {
        this.exceptionSn = exceptionSn;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getClaimDescribe() {
        return claimDescribe;
    }

    public void setClaimDescribe(String claimDescribe) {
        this.claimDescribe = claimDescribe;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public ClaimsVO getCompensationClaim() {
        return compensationClaim;
    }

    public void setCompensationClaim(ClaimsVO compensationClaim) {
        if (compensationClaim.getClaimType() != null) {
            compensationClaim.setExceptionId(this.exceptionId);
            compensationClaim.setExceptionSn(this.exceptionSn);
            compensationClaim.setOrderSn(this.orderSn);
            compensationClaim.setClaimDescribe(this.claimDescribe);
            compensationClaim.setMemberId(this.memberId);
        }
        this.compensationClaim = compensationClaim;
    }

    public ClaimsVO getFreeClaim() {
        return freeClaim;
    }

    public void setFreeClaim(ClaimsVO freeClaim) {
        if (freeClaim.getClaimType() != null) {
            freeClaim.setExceptionId(this.exceptionId);
            freeClaim.setExceptionSn(this.exceptionSn);
            freeClaim.setOrderSn(this.orderSn);
            freeClaim.setClaimDescribe(this.claimDescribe);
            freeClaim.setMemberId(this.memberId);
        }
        this.freeClaim = freeClaim;
    }

    public ClaimsVO getPaymentClaim() {
        return paymentClaim;
    }

    public void setPaymentClaim(ClaimsVO paymentClaim) {
        if (paymentClaim.getClaimType() != null) {
            paymentClaim.setExceptionId(this.exceptionId);
            paymentClaim.setExceptionSn(this.exceptionSn);
            paymentClaim.setOrderSn(this.orderSn);
            paymentClaim.setClaimDescribe(this.claimDescribe);
            paymentClaim.setMemberId(this.memberId);
        }
        this.paymentClaim = paymentClaim;
    }

    public ClaimsVO getRefundClaim() {
        return refundClaim;
    }

    public void setRefundClaim(ClaimsVO refundClaim) {
        if (refundClaim.getClaimType() != null) {
            refundClaim.setExceptionId(this.exceptionId);
            refundClaim.setExceptionSn(this.exceptionSn);
            refundClaim.setOrderSn(this.orderSn);
            refundClaim.setClaimDescribe(this.claimDescribe);
            refundClaim.setMemberId(this.memberId);
        }
        this.refundClaim = refundClaim;
    }

    public ClaimsVO getCouponClaim() {
        return couponClaim;
    }

    public void setCouponClaim(ClaimsVO couponClaim) {
        if (couponClaim.getClaimType() != null) {
            couponClaim.setExceptionId(this.exceptionId);
            couponClaim.setExceptionSn(this.exceptionSn);
            couponClaim.setOrderSn(this.orderSn);
            couponClaim.setClaimDescribe(this.claimDescribe);
            couponClaim.setMemberId(this.memberId);
        }
        this.couponClaim = couponClaim;
    }

    public ClaimsVO getOtherClaim() {
        return otherClaim;
    }

    public void setOtherClaim(ClaimsVO otherClaim) {
        if (otherClaim.getClaimType() != null) {
            otherClaim.setExceptionId(this.exceptionId);
            otherClaim.setExceptionSn(this.exceptionSn);
            otherClaim.setOrderSn(this.orderSn);
            otherClaim.setClaimDescribe(this.claimDescribe);
            otherClaim.setMemberId(this.memberId);
        }
        this.otherClaim = otherClaim;
    }
}
