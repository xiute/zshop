package com.enation.app.javashop.core.trade.cart.model.enums;

/**
 * Created by kingapex on 2018/12/20.
 * 购物车类型
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/12/20
 */
public enum CartSourceType {

    /**
     * 表明是所有购物车商品
     */
    ALL_CART,

    /**
     * 普通商品购物车
     */
    COMMON_CART,

    /**
     * 社区团购购物车
     */
    SHETUAN_CART,

    /**
     * 附近同城购物车
     */
    TONGCHENG_CART


}
