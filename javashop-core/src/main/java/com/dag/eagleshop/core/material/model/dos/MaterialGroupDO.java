package com.dag.eagleshop.core.material.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 素材分组模型
 */
@Table(name="mat_material_group")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MaterialGroupDO implements Serializable {

    @Id(name="material_group_id")
    @Column(name = "material_group_id")
    @ApiModelProperty(name="material_group_id",value="素材分组id")
    private Integer materialGroupId;

    @Column(name = "material_group_name")
    @ApiModelProperty(name="material_group_name",value="素材分组名")
    private String materialGroupName;

    @Column(name = "material_group_type")
    @ApiModelProperty(name="material_group_type",value="素材分组类型")
    private Integer materialGroupType;

    @Column(name = "seller_id")
    @ApiModelProperty(name="seller_id",value="卖家id")
    private Integer sellerId;

    public Integer getMaterialGroupId() {
        return materialGroupId;
    }

    public void setMaterialGroupId(Integer materialGroupId) {
        this.materialGroupId = materialGroupId;
    }

    public String getMaterialGroupName() {
        return materialGroupName;
    }

    public void setMaterialGroupName(String materialGroupName) {
        this.materialGroupName = materialGroupName;
    }

    public Integer getMaterialGroupType() {
        return materialGroupType;
    }

    public void setMaterialGroupType(Integer materialGroupType) {
        this.materialGroupType = materialGroupType;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }
}
