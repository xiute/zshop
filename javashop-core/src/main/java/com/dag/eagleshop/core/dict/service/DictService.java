package com.dag.eagleshop.core.dict.service;

import com.dag.eagleshop.core.dict.model.dos.Dict;
import com.dag.eagleshop.core.dict.model.vo.DictVO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;

public interface DictService {

    Page<DictVO> queryDictList(DictVO dictVO);

    void addDict( Dict dict);


    void updateDict(Dict dict);


    void delDict( Integer  id);

    List<String> queryAllType();

    DictVO queryDictById(Integer id);

    void initDictRedisCache();

    Dict queryByType(String type);

}
