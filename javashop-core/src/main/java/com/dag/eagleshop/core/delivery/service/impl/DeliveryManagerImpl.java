package com.dag.eagleshop.core.delivery.service.impl;

import com.dag.eagleshop.core.account.utils.HttpUtils;
import com.dag.eagleshop.core.delivery.model.dto.AddOrderDTO;
import com.dag.eagleshop.core.delivery.model.dto.CancelOrderDTO;
import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import com.dag.eagleshop.core.delivery.model.dto.UpdateOrderDTO;
import com.dag.eagleshop.core.delivery.service.DeliveryManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DeliveryManagerImpl implements DeliveryManager {

    @Value("${service.manage.url}")
    private String url;

    /** 添加订单 */
    private static final String ADD_ORDER = "/gateway/api/order/addOrder";

    /** 取消订单 */
    private static final String CANCEL_ORDER = "/gateway/api/order/cancelOrder";

    /** 修改订单状态 */
    private static final String UPDATE_ORDER = "/gateway/api/order/updateOrder";

    /** 查询配送员 */
    private static final String QUERY_RIDER = "/gateway/api//rider/queryRider";

    /**
     * 添加订单
     */
    @Override
    public JsonBean addOrder(AddOrderDTO addOrderDTO) {
        String requestPath = url + ADD_ORDER;
        return HttpUtils.httpPostByJson(requestPath, addOrderDTO, JsonBean.class);
    }

    /**
     * 修改订单
     */
    @Override
    public JsonBean updateOrder(UpdateOrderDTO updateOrderDTO) {
        String requestPath = url + UPDATE_ORDER;
        return HttpUtils.httpPostByJson(requestPath, updateOrderDTO, JsonBean.class);
    }

    /**
     * 查询配送员
     */
    @Override
    public JsonBean queryRider(Map<String,Object> map) {
        String requestPath = url + QUERY_RIDER;
        return HttpUtils.httpPostByJson(requestPath, map, JsonBean.class);
    }

    /**
     * 取消订单
     */
    @Override
    public JsonBean cancelOrder(CancelOrderDTO cancelOrderDTO) {
        String requestPath = url + CANCEL_ORDER;
        return HttpUtils.httpPostByJson(requestPath, cancelOrderDTO, JsonBean.class);
    }
}
