package com.dag.eagleshop.core.material.service;

import com.dag.eagleshop.core.material.model.dos.MaterialDO;
import com.enation.app.javashop.framework.database.Page;

public interface MaterialManager {

    /**
     * 查询素材列表
     */
    Page list(int pageNo, int pageSize, Integer sellerId, String materialName);

    /**
     * 添加素材
     */
    MaterialDO add(MaterialDO materialGroup);

    /**
     * 修改素材
     */
    MaterialDO edit(MaterialDO materialGroup, Integer id);

    /**
     * 根据id查询素材
     */
    MaterialDO getModel(Integer id);

    /**
     * 删除素材
     */
    void delete(Integer[] ids);

    /**
     * 批量修改素材分组
     */
    void batchEditGroup(Integer[] ids, Integer materialGroupId);


}
