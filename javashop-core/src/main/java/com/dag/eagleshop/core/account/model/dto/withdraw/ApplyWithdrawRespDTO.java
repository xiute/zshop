package com.dag.eagleshop.core.account.model.dto.withdraw;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 申请提现结果DTO
 */
@Data
public class ApplyWithdrawRespDTO extends BaseDTO {

    /**
     * 提现单id
     */
    private String withdrawId;
    /**
     * 提现申请号
     */
    private String withdrawNo;

    /**
     * 账户id
     */
    @ApiModelProperty(name ="accountId", value = "账户id")
    private String accountId;
    /**
     * 提现结果
     */
    @ApiModelProperty(name ="success", value = "提现结果")
    private boolean success;
    /**
     * 描述
     */
    @ApiModelProperty(name ="message", value = "提现结果描述")
    private String message;


    /**
     * 构建成功结果
     */
    public static ApplyWithdrawRespDTO bySuccess(String accountId, String message){
        ApplyWithdrawRespDTO transferRespDTO = by(accountId, message);
        transferRespDTO.setSuccess(true);
        return transferRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static ApplyWithdrawRespDTO byFail(String accountId, String message){
        ApplyWithdrawRespDTO transferRespDTO = by(accountId, message);
        transferRespDTO.setSuccess(false);
        return transferRespDTO;
    }


    private static ApplyWithdrawRespDTO by(String accountId, String message){
        ApplyWithdrawRespDTO transferRespDTO = new ApplyWithdrawRespDTO();
        transferRespDTO.setAccountId(accountId);
        transferRespDTO.setMessage(message);
        return transferRespDTO;
    }

}
