package com.dag.eagleshop.core.material.service.impl;

import com.dag.eagleshop.core.material.MaterialErrorCode;
import com.dag.eagleshop.core.material.model.dos.MaterialGroupDO;
import com.dag.eagleshop.core.material.service.MaterialGroupManager;
import com.enation.app.javashop.core.goods.GoodsErrorCode;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.SqlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 素材分组服务
 */
@Service
public class MaterialGroupManagerImpl implements MaterialGroupManager {

    @Autowired
    @Qualifier("systemDaoSupport")
    private DaoSupport daoSupport;


    @Override
    public Page list(int pageNo, int pageSize, Integer sellerId, String materialGroupName) {
        StringBuilder sqlBuilder = new StringBuilder("select * from mat_material_group ");
        sqlBuilder.append(" where seller_id = " + sellerId);
        List<Object> term = new ArrayList<>();
        if (materialGroupName != null) {
            sqlBuilder.append(" and material_group_name like ? ");
            term.add("%" + materialGroupName + "%");
        }

        sqlBuilder.append(" order by material_group_id");

        return this.daoSupport.queryForPage(sqlBuilder.toString(), pageNo, pageSize,
                MaterialGroupDO.class, term.toArray());
    }


    @Override
    public MaterialGroupDO add(MaterialGroupDO materialGroup) {
        String sql = "select * from mat_material_group where material_group_name = ? and material_group_type = ?";
        List list = this.daoSupport.queryForList(sql, materialGroup.getMaterialGroupName(), materialGroup.getMaterialGroupType());
        if (list.size() > 0) {
            throw new ServiceException(GoodsErrorCode.E302.code(), "素材分组重复");
        }

        this.daoSupport.insert(materialGroup);
        return materialGroup;
    }


    @Override
    public MaterialGroupDO edit(MaterialGroupDO materialGroup, Integer id) {
        MaterialGroupDO materialGroupDO = this.getModel(id);
        if (materialGroupDO == null) {
            throw new ServiceException(MaterialErrorCode.E1000.code(), "素材分组不存在");
        }

        String sql = "select * from mat_material_group where material_group_name = ? and material_group_type = ? and material_group_id != ?";
        List list = this.daoSupport.queryForList(sql, materialGroup.getMaterialGroupName(),
                materialGroup.getMaterialGroupType(), id);
        if (list.size() > 0) {
            throw new ServiceException(MaterialErrorCode.E1000.code(), "素材分组重复");
        }

        this.daoSupport.update(materialGroup, id);
        return materialGroup;
    }


    @Override
    public MaterialGroupDO getModel(Integer id) {
        return this.daoSupport.queryForObject(MaterialGroupDO.class, id);
    }

    @Override
    public void delete(Integer[] ids) {
        List term = new ArrayList<>();

        String idsStr = SqlUtil.getInSql(ids, term);

        //检测是否有关联
        String sql = "select count(0) from mat_material where material_group_id in (" + idsStr + ")";
        Integer count = this.daoSupport.queryForInt(sql, term.toArray());
        if(count > 0){
            throw new ServiceException(MaterialErrorCode.E1000.code(), "已有素材关联，不能删除");
        }

        sql = "delete from mat_material_group where material_group_id in (" + idsStr + ") ";

        this.daoSupport.execute(sql, term.toArray());
    }
}
