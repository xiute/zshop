package com.dag.eagleshop.core.account.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 账户性质枚举
 **/
public enum NatureEnum {

    PERSONAL(1, "个人"),
    ENTERPRISE(2, "企业"),
    PLATFORM(999, "平台");

    private int index;
    private String text;


    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (NatureEnum item : NatureEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    NatureEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }


}
