package com.dag.eagleshop.core.account.model.dto.withdraw;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chien
 * @since 2020-07-22
 */
@Data
public class WithdrawAccountDTO extends BaseDTO {

    @ApiModelProperty(name ="id", value = "提现账号id")
    private String id;
    /**
     * 会员id
     */
    @ApiModelProperty(name ="memberId", value = "会员id")
    private String memberId;
    /**
     * 账号类型
     */
    @ApiModelProperty(name ="type", value = "账号类型, 1支付宝 2微信")
    private Integer type;
    /**
     * 类型名称
     */
    @ApiModelProperty(name ="typeName", value = "类型名称")
    private String typeName;
    /**
     * 账户名
     */
    @ApiModelProperty(name ="accountName", value = "账户名")
    private String accountName;
    /**
     * 账号
     */
    @ApiModelProperty(name ="accountNo", value = "账号")
    private String accountNo;
    /**
     * 是否对公 0否 1是
     */
    @ApiModelProperty(name ="ofPublic", value = "是否对公 0否 1是", required = false)
    private Integer ofPublic;

}
