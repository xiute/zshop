package com.dag.eagleshop.core.account.model.dto.base;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 接收只有一个参数Id的DTO
 */
@Data
public final class IdDTO<T> extends BaseDTO {

    @NotNull(message = "id参数不能为空！")
    private T id;

    private IdDTO(){}

    private IdDTO(T id){
        this.id = id;
    }

    public static <T> IdDTO<T> by(T id){
        return new IdDTO<>(id);
    }

}
