package com.dag.eagleshop.core.account.model.dto.refund;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class UnifiedRefundRespDTO implements Serializable {

    private static final long serialVersionUID = 5706693415388408625L;
    /**
     * 退款流水号
     */
    private String serialNo;
    /**
     * 交易凭证单号
     */
    private String tradeVoucherNo;
    /**
     * 退款金额
     */
    private BigDecimal amount;
    /**
     * 转账结果
     */
    private boolean success;
    /**
     * 转账描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static UnifiedRefundRespDTO bySuccess(String serialNo, String tradeVoucherNo, String message){
        UnifiedRefundRespDTO unifiedRefundRespDTO = by(serialNo, tradeVoucherNo , message);
        unifiedRefundRespDTO.setSuccess(true);
        return unifiedRefundRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static UnifiedRefundRespDTO byFail(String serialNo, String tradeVoucherNo, String message){
        UnifiedRefundRespDTO unifiedRefundRespDTO = by(serialNo, tradeVoucherNo, message);
        unifiedRefundRespDTO.setSuccess(false);
        return unifiedRefundRespDTO;
    }


    private static UnifiedRefundRespDTO by(String serialNo, String tradeVoucherNo, String message){
        UnifiedRefundRespDTO unifiedRefundRespDTO = new UnifiedRefundRespDTO();
        unifiedRefundRespDTO.setSerialNo(serialNo);
        unifiedRefundRespDTO.setTradeVoucherNo(tradeVoucherNo);
        unifiedRefundRespDTO.setMessage(message);
        return unifiedRefundRespDTO;
    }

}
