package com.dag.eagleshop.core.account.model.dto.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * 修改对象DTO 所有修改对象请求类必须继承此DTO
 * 使用场景
 * 1、Feign请求DTO继承
 */
@Data
@Component
public class UpdateDTO extends BaseDTO{

    /** 主键 */
    @NotNull(message = "id不能为空")
    private String id;

    /** 更新人id */
    @ApiModelProperty(name ="updaterId", value = "更新人id", hidden = true)
    private String updaterId;

    /** 更新人名称 */
    @ApiModelProperty(name ="updaterName", value = "更新人名称", hidden = true)
    private String updaterName;

}
