package com.dag.eagleshop.core.account.model.dto.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 查询对象DTO 所有查询对象请求类必须继承此DTO
 * 使用场景
 * 1、Feign请求DTO继承
 */
@Data
@Component
public class QueryDTO extends SaaSDTO{

    /** 主键 */
    @ApiModelProperty(name ="id", value = "主键")
    private String id;

    /** 创建人id */
    @ApiModelProperty(name ="createrId", value = "创建人id", hidden = true)
    private String createrId;

    /** 创建人姓名 */
    @ApiModelProperty(name ="createrName", value = "创建人姓名", hidden = true)
    private String createrName;

    /** 组织ids */
    @ApiModelProperty(name ="orgIds", value = "组织ids", hidden = true)
    private List<String> orgIds;

    /** 开始创建时间 */
    @ApiModelProperty(name ="startCreateTime", value = "开始创建时间")
    private Date startCreateTime;

    /** 结束创建时间 */
    @ApiModelProperty(name ="endCreateTime", value = "结束创建时间")
    private Date endCreateTime;

}
