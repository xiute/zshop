package com.dag.eagleshop.core.dict.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = "es_dict")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Dict implements Serializable {

    @Id(name="id")
    @ApiModelProperty(name = "id",value = "主键")
    private Integer id;

    @Column(name = "parent_id")
    @ApiModelProperty(name = "parent_id",value = "父级字典")
    private String parentId;

    @Column(name = "type")
    @ApiModelProperty(name ="type",value = "类型")
    private String type;

    @Column(name="label")
    @ApiModelProperty(name="label",value = "标签名")
    private String label;

    @Column(name="value")
    @ApiModelProperty(name = "value",value = "数据值")
    private String value;

    @Column(name="description")
    @ApiModelProperty(name = "description",value = "描述")
    private String description;

    @Column(name="sort")
    @ApiModelProperty(name = "sort",value = "排序（升序）")
    private String sort;

    @Column(name="classify")
    @ApiModelProperty(name = "classify",value = "归类 1系统 2通用 3私有")
    private String classify;

    @Column(name="org_id")
    @ApiModelProperty(name = "org_id",value = "组织id")
    private String orgId;

    @Column(name="merchant_id")
    @ApiModelProperty(name = "merchant_id",value = "商户id")
    private String merchantId;

    @Column(name="creater_id")
    @ApiModelProperty(name = "creater_id",value = "创建人")
    private String createrId;

    @Column(name="create_time")
    @ApiModelProperty(name = "create_time",value = "创建时间")
    private Date createTime;

    @Column(name="updater_id")
    @ApiModelProperty(name = "updater_id",value = "更新人")
    private String updaterId;

    @Column(name="update_time")
    @ApiModelProperty(name = "update_time",value = "更新时间")
    private Date updateTime;

    @Column(name="remarks")
    @ApiModelProperty(name = "remarks",value = "备注")
    private String remarks;

    @Column(name="is_delete")
    @ApiModelProperty(name = "is_delete",value = "删除标识")
    private String isDelete;




}
