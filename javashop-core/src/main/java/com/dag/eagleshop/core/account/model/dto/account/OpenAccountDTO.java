package com.dag.eagleshop.core.account.model.dto.account;

import com.dag.eagleshop.core.account.model.dto.base.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 开通账户DTO
 */
@Data
public class OpenAccountDTO extends SaaSDTO {

    /**
     * 会员id
     */
    @NotNull(message = "会员id不能为空")
    private String memberId;
    /**
     * 账户类型 目前就一个主账户 AccountTypeEnum
     */
    @NotNull(message = "账户类型不能为空")
    private Integer accountType;
    /**
     * 账户类型名称 目前就一个主账户 AccountTypeEnum
     */
    @NotNull(message = "账户类型名称不能为空")
    private String accountTypeName;
    /**
     * 账户性质
     */
    @NotNull(message = "账户性质不能为空")
    private Integer nature;
    /**
     * 支付密码
     */
    private String payPwd;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;

}
