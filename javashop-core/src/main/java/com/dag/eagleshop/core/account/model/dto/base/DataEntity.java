package com.dag.eagleshop.core.account.model.dto.base;


import lombok.Data;

import java.util.Date;

/**
 * 数据库实体类
 */
@Data
public class DataEntity extends BaseEntity {

	/** 组织id */
	protected String orgId;

	/** 商户id */
	protected String merchantId;

	/** 创建人 */
	protected String createrId;

	/** 创建时间 */
	protected Date createTime;

	/** 更新人 */
	protected String updaterId;

	/** 更新时间 */
	protected Date updateTime;

	/** 删除标记 */
	protected Integer isDelete = 0;

	/** 备注信息 */
	protected String remarks;

}
