package com.dag.eagleshop.core.account.model.dto.account;

import com.dag.eagleshop.core.account.model.dto.base.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 业务转账DTO
 */
@Data
public class BusinessTransferReqDTO extends SaaSDTO {

    /**
     * 交易凭证单号
     */
    @NotNull(message = "交易凭证单号不能为空")
    private String tradeVoucherNo;
    /**
     * 业务转账明细
     */
    @NotNull(message = "转账明细不能为空")
    private List<TransferReqDTO> transferReqList;
    /**
     * 操作人id
     */
    @NotNull(message = "操作人id不能为空")
    private String operatorId;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;

}
