package com.dag.eagleshop.core.account.model.dto.payment;

import com.dag.eagleshop.core.account.model.dto.base.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 付款记录DTO
 */
@Data
public class PaymentRecordDTO extends SaaSDTO {

    /**
     * 交易业务单号
     */
    @NotNull(message = "交易业务单号不能为空")
    private String tradeVoucherNo;
    /**
     * 付款人id
     */
    @NotNull(message = "付款人id不能为空")
    private String draweeMemberId;
    /**
     * 交易渠道
     */
    @NotNull(message = "交易渠道不能为空")
    private Integer tradeChannel;
    /**
     * 金额
     */
    @NotNull(message = "金额不能为空")
    private BigDecimal amount;

    /**
     * 交易主题
     */
    @NotNull(message = "交易主题不能为空")
    private String tradeSubject;

    /**
     * 交易内容
     */
    @NotNull(message = "交易内容不能为空")
    private String tradeContent;
    /**
     * 支付状态
     */
    @NotNull(message = "支付状态不能为空")
    private Integer tradeStatus;
    /**
     * 交易时间
     */
    @NotNull(message = "交易时间不能为空")
    private Date tradeTime;
    /**
     * 操作人id
     */
    @NotNull(message = "操作人id不能为空")
    private String operatorId;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;
}
