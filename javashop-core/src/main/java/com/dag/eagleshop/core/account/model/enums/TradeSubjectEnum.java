package com.dag.eagleshop.core.account.model.enums;

public enum TradeSubjectEnum {

    PLATFORM_REVENUE("平台收益"),
    LV1_LEADER_REVENUE("提成结算"),
    LV2_LEADER_REVENUE("提成结算"),
    INVITE_REVENUE("提成结算"),
    SUBSIDY_REVENUE("平台补贴"),
    SELLER_REVENUE("订单结算"),
    ORDER_PAYMENT("订单支付"),
    ORDER_PAYMENT_WALLET("订单支付-余额"),
    ORDER_PAYMENT_WEIXIN("订单支付-微信"),
    ORDER_REFUND("订单退款"),
    ORDER_REFUND_WALLET("订单退款-余额"),
    ORDER_REFUND_WEIXIN("订单退款-微信"),
    SPREAD_REFUND("多退少补"),
    EXCEPTION_ORDER_PAYMENT_CASH("赔付现金");

    private String description;

    TradeSubjectEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return this.description;
    }
}
