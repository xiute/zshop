package com.dag.eagleshop.core.delivery.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * PC前端/安卓返回JSON
 */
@Data
public class JsonBean implements Serializable {

    private boolean success;    // 是否成功
    private String code;        // 编码 0正常 其他不正常
    private String message;     // 正确或错误消息
    private Object data;        // 结果

    /** JsonBean正常code */
    public static final String SUCCESS_CODE = "0";
    /** JsonBean通用错误code */
    public static final String COMMON_ERROR_CODE = "100000";
    /** JsonBean通用空指针错误code */
    public static final String COMMON_NULL_CODE = "100001";

    /**
     * 默认成功
     */
    public JsonBean(){
        this.success = true;
        this.code = SUCCESS_CODE;
        this.message = "操作成功";
    }

    /**
     * 默认成功，自定义消息提示
     */
    public JsonBean(String symbol){
        this.success = true;
        this.code = SUCCESS_CODE;
        this.message = symbol;
    }

    /**
     * 默认成功，自定义提示和返回值
     */
    public JsonBean(String symbol, Object data){
        this(symbol);
        this.data = data;
    }


    /**
     * 默认成功，自定义返回值
     */
    public JsonBean(Object data){
        this.success = true;
        this.code = SUCCESS_CODE;
        this.message = "操作成功";
        this.data = data;
    }

    /**
     * 通用，自定义成功/失败和消息提示
     */
    public JsonBean(boolean success, String symbol){
        this.success = success;
        this.message = symbol;
        this.code = success ? SUCCESS_CODE : COMMON_ERROR_CODE;
    }


    /**
     * 通用，全部自定义 除了code
     */
    public JsonBean(boolean success, String symbol, Object data){
        this(success, symbol);
        this.data = data;
    }


    /**
     * 通用，自定义成功/失败、编码和消息提示
     */
    public JsonBean(boolean success, String code, String symbol){
        this.success = success;
        this.message = symbol;
        this.code = code;
    }


    /**
     * 通用，全部自定义 除了错误信息
     */
    public JsonBean(boolean success, String code, String symbol, Object data){
        this(success, code, symbol);
        this.data = data;
    }

}
