package com.dag.eagleshop.core.account.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 交易状态
 * @author 孙建
 */
public enum TradeStatusEnum {

    /** 交易等待 */
    TRADE_STATUS_WAIT(1, "交易等待"),
    /** 交易中 */
    TRADE_STATUS_TRADING(2, "交易中"),
    /** 交易成功 */
    TRADE_STATUS_SUCCESS(3, "交易成功"),
    /** 交易关闭 */
    TRADE_STATUS_CLOSE(4, "交易关闭");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (TradeStatusEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    TradeStatusEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }

}
