package com.dag.eagleshop.core.dict.model.vo;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import lombok.Data;

import java.util.Date;

@Data
public class DictVO extends BaseDTO {

    private String id;
    /**
     * 父级字典
     */
    private String parentId;
    /**
     * 类型
     */
    private String type;
    /**
     * 标签名
     */
    private String label;
    /**
     * 数据值
     */
    private String value;
    /**
     * 描述
     */
    private String description;
    /**
     * 排序（升序）
     */
    private Integer sort;

    /** 归类 1系统 2通用 3私有 */
    private Integer classify;

    /** 组织id */
    protected String orgId;

    /** 商户id */
    protected String merchantId;

    /** 创建人 */
    protected String createrId;

    /** 创建时间 */
    protected Date createTime;

    /** 更新人 */
    protected String updaterId;

    /** 更新时间 */
    protected Date updateTime;

    /** 删除标记 */
    protected Integer isDelete = 0;

    /** 备注信息 */
    protected String remarks;

    private  Integer pageNo;

    private Integer pageSize;

}
