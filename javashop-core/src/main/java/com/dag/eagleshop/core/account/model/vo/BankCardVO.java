package com.dag.eagleshop.core.account.model.vo;

import com.dag.eagleshop.core.account.model.dto.base.UpdateDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "修改银行卡实体")
public class BankCardVO extends UpdateDTO {

    /**
     * 会员id
     */
    @ApiModelProperty(name ="memberId", value = "会员id")
    @NotNull(message = "会员id不能为空")
    private String memberId;     // 账号id
    /**
     * 账户id
     */
    @ApiModelProperty(name ="accountId", value = "账户id")
    @NotNull(message = "账户id不能为空")
    private String accountId;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 银行名称
     */
    @ApiModelProperty(name ="bankName", value = "银行名称")
    @NotNull(message = "银行名称不能为空")
    private String bankName;
    /**
     * 支行名称
     */
    @ApiModelProperty(name ="subBankName", value = "支行名称")
    private String subBankName;
    /**
     * 银行卡号
     */
    @ApiModelProperty(name ="cardNo", value = "银行卡号")
    @NotNull(message = "银行卡号不能为空")
    private String cardNo;
    /**
     * 开户名
     */
    @ApiModelProperty(name ="openAccountName", value = "开户名")
    @NotNull(message = "开户名不能为空")
    private String openAccountName;
    /**
     * 银行卡类型 0对私 1对公
     */
    @ApiModelProperty(name ="bankCardType", value = "银行卡类型0对私 1对公")
    @NotNull(message = "银行卡类型不能为空")
    private Integer bankCardType;
    /**
     * 是否默认
     */
    private Integer isDefault;
    /**
     * 卡状态
     */
    private Integer cardStatus;
    /**
     * 银行卡照片
     */
    private String bankUrls;

    /**
     * 开户省份
     */
    @ApiModelProperty(name ="bank_province", value = "开户省份")
    private String bankProvince;
    /**
     * 开户城市
     */
    @ApiModelProperty(name ="bank_city", value = "开户城市")
    private String bankCity;
    /**
     * 开户省份ID
     */
    @ApiModelProperty(name ="bank_province_id", value = "开户省份ID")
    private Integer bankProvinceId;
    /**
     * 开户城市ID
     */
    @ApiModelProperty(name ="bank_city_id", value = "开户城市ID")
    private Integer bankCityId;

}
