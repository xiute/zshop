package com.dag.eagleshop.core.account.model.dto.base;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * SaaS模式DTO 所有请求类必须继承此DTO
 * 使用场景
 * 1、Feign请求DTO继承
 */
@Data
@Component
public class SaaSDTO extends BaseDTO{

    /** 组织id */
    private String orgId;

    /** 商户id */
    private String merchantId;

}
