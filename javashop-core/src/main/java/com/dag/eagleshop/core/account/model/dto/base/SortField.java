package com.dag.eagleshop.core.account.model.dto.base;

import lombok.Data;

@Data
public class SortField {

    // 字段
    private String field;

    // 是否升序
    private boolean isAsc;

    public SortField(){}

    public SortField(boolean isAsc, String field){
        this.isAsc = isAsc;
        this.field = field;
    }

    // 获取转换后的列命
    public String getColumnField(){
        return toUnderline(field);
    }


    // 驼峰命名转换
    private String toUnderline(String field){
        StringBuilder sb = new StringBuilder(field);
        int temp = 0;
        if (!field.contains("_")) {
            for(int i=0; i<field.length(); i++){
                if(Character.isUpperCase(field.charAt(i))){
                    sb.insert(i + temp, "_");
                    temp += 1;
                }
            }
        }
        return sb.toString().toLowerCase();
    }

}
