package com.dag.eagleshop.core.account.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 交易类型
 * @author 孙建
 */
public enum TradeTypeEnum {

    /** 充值 */
    RECHARGE(1, "充值"),
    /** 转账 */
    TRANSFER(2, "转账"),
    /** 提现 */
    WITHDRAWAL(3, "提现"),
    /** 支付 */
    PAYED(4, "支付"),
    /** 退款 */
    REFUND(5, "退款");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (TradeTypeEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    TradeTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }

}
