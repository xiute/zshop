package com.dag.eagleshop.core.account.model.dto.base;

import lombok.Data;
import org.apache.lucene.search.SortField;

import java.io.Serializable;
import java.util.List;

/**
 * 前端分页对象
 */
@Data
public class ViewPage<T> implements Serializable{

    // 当前页
    private Integer pageNum;
    // 页大小
    private Integer pageSize;
    // 结果集
    private List<T> records;
    // 总条数
    private Long totalCount;
    // 总页数
    private Integer totalPages;
    // 排序字段
    private SortField[] orderBys;

}
