package com.dag.eagleshop.core.material;

/**
 * 素材异常类
 *
 * @author sunjian
 * @version 1.0
 * @since 7.0.0 2020/01/05
 */
public enum MaterialErrorCode {

    /**
     * 素材分组相关异常
     */
    E1000("素材分组相关异常");

    private String describe;

    MaterialErrorCode(String des) {
        this.describe = des;
    }

    /**
     * 获取商品的异常码
     * @return 获取商品的异常码
     */
    public String code() {
        return this.name().replaceAll("E", "");
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
