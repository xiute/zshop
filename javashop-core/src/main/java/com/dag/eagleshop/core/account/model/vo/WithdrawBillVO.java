package com.dag.eagleshop.core.account.model.vo;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 提现申请单
 * @author 孙建
 * @since 2020-05-30
 */
@Data
public class WithdrawBillVO extends BaseDTO {

    /**
     * 提现申请号
     */
    @ApiModelProperty(name ="withdrawNo", value = "提现申请号")
    private String withdrawNo;
    /**
     * 会员名称
     */
    @ApiModelProperty(name ="memberName", value = "会员名称")
    private String memberName;
    /**
     * 账户类型名称
     */
    @ApiModelProperty(name ="accountTypeName", value = "账户类型名称")
    private String accountTypeName;
    /**
     * 提现渠道
     */
    @ApiModelProperty(name ="withdrawChannel", value = "提现渠道")
    private String withdrawChannel;
    /**
     * 申请提现金额
     */
    @ApiModelProperty(name ="amount", value = "申请提现金额")
    private BigDecimal amount;
    /**
     * 银行名称
     */
    @ApiModelProperty(name ="bankName", value = "银行名称")
    private String bankName;
    /**
     * 支行名
     */
    @ApiModelProperty(name ="subBankName", value = "支行名")
    private String subBankName;
    /**
     * 银行卡号
     */
    @ApiModelProperty(name ="cardNo", value = "银行卡号")
    private String cardNo;
    /**
     * 开户名
     */
    @ApiModelProperty(name ="openAccountName", value = "开户名")
    private String openAccountName;
    /**
     * 其他账户名
     */
    @ApiModelProperty(name ="otherAccountName", value = "其他账户名")
    private String otherAccountName;
    /**
     * 其他账号
     */
    @ApiModelProperty(name ="otherAccountNo", value = "其他账号")
    private String otherAccountNo;
    /**
     * 提现状态
     */
    @ApiModelProperty(name ="status", value = "提现状态")
    private String status;
    /**
     * 申请时间
     */
    @ApiModelProperty(name ="applyTime", value = "申请时间")
    private Date applyTime;
    /**
     * 转账时间
     */
    @ApiModelProperty(name ="transferTime", value = "转账时间")
    private Date transferTime;

}
