package com.dag.eagleshop.core.material.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * 素材模型
 */
@Table(name="mat_material")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MaterialDO implements Serializable {

    @Id(name="material_id")
    @Column(name = "material_id")
    @ApiModelProperty(name="material_id",value="素材id")
    private Integer materialId;

    @Column(name = "material_name")
    @ApiModelProperty(name="material_name",value="素材名")
    private String materialName;

    @Column(name = "link_url")
    @ApiModelProperty(name="link_url",value="链接url")
    private String linkUrl;

    @Column(name = "material_type")
    @ApiModelProperty(name="material_type",value="素材类型")
    private Integer materialType;

    @Column(name = "material_group_id")
    @ApiModelProperty(name="material_group_id",value="素材分组id")
    private Integer materialGroupId;

    @Column(name = "cover_url")
    @ApiModelProperty(name="cover_url",value="封面图片url")
    private String coverUrl;

    @Column(name = "file_size")
    @ApiModelProperty(name="file_size",value="文件大小")
    private double fileSize;

    @Column(name = "upload_time")
    @ApiModelProperty(name="upload_time",value="上传时间")
    private Date uploadTime;

    @Column(name = "audit_status")
    @ApiModelProperty(name="audit_status",value="审核状态")
    private Integer auditStatus;

    @Column(name = "seller_id")
    @ApiModelProperty(name="seller_id",value="卖家id")
    private Integer sellerId;

    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public Integer getMaterialType() {
        return materialType;
    }

    public void setMaterialType(Integer materialType) {
        this.materialType = materialType;
    }

    public Integer getMaterialGroupId() {
        return materialGroupId;
    }

    public void setMaterialGroupId(Integer materialGroupId) {
        this.materialGroupId = materialGroupId;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }
}
