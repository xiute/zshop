package com.dag.eagleshop.core.account.model.dto.account;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 申请充值DTO
 */
@Data
public class ApplyRechargeReqDTO extends BaseDTO {

    /**
     * 账户id
     */
    @ApiModelProperty(name ="accountId", value = "账户id")
    @NotNull(message = "账户id不能为空")
    private String accountId;
    /**
     * 充值金额
     */
    @ApiModelProperty(name ="amount", value = "充值金额")
    @NotNull(message = "充值金额不能为空")
    private BigDecimal amount;
    /**
     * 交易渠道
     */
    @ApiModelProperty(name ="tradeChannel", value = "交易渠道，2支付宝 3微信")
    @NotNull(message = "交易渠道不能为空")
    private Integer tradeChannel;

}
