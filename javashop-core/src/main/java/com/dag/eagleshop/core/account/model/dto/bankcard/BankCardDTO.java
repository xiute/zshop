package com.dag.eagleshop.core.account.model.dto.bankcard;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BankCardDTO extends BaseDTO {

    private String id;
    /**
     * 会员id
     */
    private String memberId;     // 账号id
    /**
     * 账户id
     */
    private String accountId;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 支行名
     */
    private String subBankName;
    /**
     * 银行卡号
     */
    private String cardNo;
    /**
     * 开户名
     */
    private String openAccountName;
    /**
     * 银行卡类型 0对私 1对公
     */
    private Integer bankCardType;
    /**
     * 开户省份
     */
    @ApiModelProperty(name ="bank_province", value = "开户省份")
    private String bankProvince;
    /**
     * 开户城市
     */
    @ApiModelProperty(name ="bank_city", value = "开户城市")
    private String bankCity;
    /**
     * 开户省份ID
     */
    @ApiModelProperty(name ="bank_province_id", value = "开户省份ID")
    private Integer bankProvinceId;
    /**
     * 开户城市ID
     */
    @ApiModelProperty(name ="bank_city_id", value = "开户城市ID")
    private Integer bankCityId;
    /**
     * 是否默认
     */
    private Integer isDefault;
    /**
     * 卡状态
     */
    private Integer cardStatus;
    /**
     * 银行卡照片
     */
    private String bankUrls;

}
