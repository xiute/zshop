package com.dag.eagleshop.core.account.model.dto.withdraw;

import com.dag.eagleshop.core.account.model.dto.base.SaaSDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 提现记录查询DTO
 */
@Data
public class QueryWithdrawBillDTO extends SaaSDTO {

    private String id;
    /**
     * 提现申请号
     */
    private String withdrawNo;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 会员类型
     */
    private String memberType;
    /**
     * 账号id
     */
    private String accountId;
    /**
     * 账户类型
     */
    private Integer accountType;
    /**
     * 账户类型名称
     */
    private String accountTypeName;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 支行名
     */
    private String subBankName;
    /**
     * 银行卡号
     */
    private String cardNo;
    /**
     * 其他账号
     */
    private String otherAccountNo;
    /**
     * 开户名
     */
    private String openAccountName;
    /**
     * 提现方式
     */
    private Integer withdrawType;
    /**
     * 提现状态
     */
    private Integer status;
    /**
     * 其他账户名
     */
    private String otherAccountName;
    /**
     * 转账时间
     */
    private Date transferTime;
    /**
     * 提现渠道
     */
    private Integer withdrawChannel;

    /**
     * 申请提现开始时间
     */

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date startApplyTime;
    /**
     * 申请提现结束时间
     */
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date endApplyTime;

}
