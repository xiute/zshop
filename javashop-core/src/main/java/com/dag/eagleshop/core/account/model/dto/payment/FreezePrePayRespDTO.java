package com.dag.eagleshop.core.account.model.dto.payment;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 冻结支付响应DTO
 */

@Data
public class FreezePrePayRespDTO implements Serializable {

    private static final long serialVersionUID = -1356989474628208680L;
    /**
     * 支付流水号
     */
    private String serialNo;
    /**
     * 交易凭证单号
     */
    private String tradeVoucherNo;
    /**
     * 支付金额
     */
    private BigDecimal amount;
    /**
     * 转账结果
     */
    private boolean success;
    /**
     * 转账描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static FreezePrePayRespDTO bySuccess(String serialNo, String tradeVoucherNo, String message){
        FreezePrePayRespDTO freezePrePayRespDTO = by(serialNo, tradeVoucherNo , message);
        freezePrePayRespDTO.setSuccess(true);
        return freezePrePayRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static FreezePrePayRespDTO byFail(String serialNo, String tradeVoucherNo, String message){
        FreezePrePayRespDTO freezePrePayRespDTO = by(serialNo, tradeVoucherNo, message);
        freezePrePayRespDTO.setSuccess(false);
        return freezePrePayRespDTO;
    }


    private static FreezePrePayRespDTO by(String serialNo, String tradeVoucherNo, String message){
        FreezePrePayRespDTO freezePrePayRespDTO = new FreezePrePayRespDTO();
        freezePrePayRespDTO.setSerialNo(serialNo);
        freezePrePayRespDTO.setTradeVoucherNo(tradeVoucherNo);
        freezePrePayRespDTO.setMessage(message);
        return freezePrePayRespDTO;
    }

}
