package com.dag.eagleshop.core.delivery.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xlg
 * @since 2020/02/05
 */
@Data
public class UpdateOrderDTO implements Serializable {

    /**
     * 上游单据id
     */
    private String upsBillId;
    /**
     * 上游单据号
     */
    private String upsBillNo;
    /**
     * 订单类型
     */
    private Integer orderType;
    /**
     * 发货客户名称
     */
    private String consignerCustomerName;
    /**
     * 发货客户手机号
     */
    private String consignerCustomerMobile;
    /**
     * 发货人姓名
     */
    private String consignerName;
    /**
     * 发货人电话
     */
    private String consignerPhone;
    /**
     * 发货地址-省
     */
    private String consignerProvince;
    /**
     * 发货地址-市
     */
    private String consignerCity;
    /**
     * 发货地址-区
     */
    private String consignerDistrict;
    /**
     * 发货地址-街道
     */
    private String consignerStreet;
    /**
     * 发货地址-详细地址
     */
    private String consignerAddress;
    /**
     * 发货地址-经度
     */
    private Double consignerLng;
    /**
     * 发货地址-纬度
     */
    private Double consignerLat;

    /**
     * 收货客户名称
     */
    private String consigneeCustomerName;

    /**
     * 收货客户手机号
     */
    private String consigneeCustomerMobile;

    /**
     * 收货人姓名
     */
    private String consigneeName;
    /**
     * 收货人手机号
     */
    private String consigneePhone;
    /**
     * 收货地址-省
     */
    private String consigneeProvince;
    /**
     * 收货地址-市
     */
    private String consigneeCity;
    /**
     * 收货地址-区
     */
    private String consigneeDistrict;
    /**
     * 收货地址-其他
     */
    private String consigneeStreet;
    /**
     * 收货地址-详细地址
     */
    private String consigneeAddress;
    /**
     * 收货地址-经度
     */
    private Double consigneeLng;
    /**
     * 收货地址-纬度
     */
    private Double consigneeLat;
    /**
     * 货物类型
     */
    private String goodsType;
    /**
     * 货物名称
     */
    private String goodsName;
    /**
     * 重量
     */
    private BigDecimal goodsWeight;
    /**
     * 体积
     */
    private BigDecimal goodsVolume;
    /**
     * 件数
     */
    private Integer goodsPiece;
    /**
     * 预估价值
     */
    private BigDecimal estimateCost;
    /**
     * 保价费用
     */
    private BigDecimal insuranceCost;
    /**
     * 合计金额
     */
    private BigDecimal totalCost;
    /**
     * 是否代收
     */
    private Integer isCollecting;
    /**
     * 代收货款金额
     */
    private BigDecimal collectingGoodsCost;
    /**
     * 代收运费金额
     */
    private BigDecimal collectingFreightCost;
    /**
     * 客户备注
     */
    private String customerRemark;
    /**
     * 距离
     */
    private Double distance;
    /**
     * 是否预约
     */
    private Integer isAppointment;
    /**
     * 计划配送时间
     */
    private Date planDeliveryTime;
    /**
     * 标签
     */
    private String labels;
    /**
     * 是否自提
     */
    private Integer isSelfTake;
    /**
     * 站点负责人姓名
     */
    private String siteManName;
    /**
     *  立即配送标识  1是 0 否
     */
    private Integer  deliveryFlag;
    /**
     * 修改人名称
     */
    private String updateName;

}
