package com.dag.eagleshop.core.account.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 会员类型
 */
public enum IdentityTypeEnum {

    PLATFORM(999, "平台"),
    BUYER(1, "买家"),
    SELLER(2, "卖家"),
    LEADER(3, "团长");

    private int index;
    private String text;


    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (IdentityTypeEnum item : IdentityTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    IdentityTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }


}
