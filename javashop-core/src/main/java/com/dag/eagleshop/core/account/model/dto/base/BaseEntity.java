package com.dag.eagleshop.core.account.model.dto.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 实体类基类
 */
@Data
public class BaseEntity implements Serializable{

    // 主键
    protected String id;

}

