package com.enation.app.javashop.seller.api.member;

import com.dag.eagleshop.core.account.model.dto.withdraw.AuditingDTO;
import com.dag.eagleshop.core.account.service.AccountManager;
import com.enation.app.javashop.core.distribution.exception.DistributionErrorCode;
import com.enation.app.javashop.core.distribution.exception.DistributionException;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.security.model.Seller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 团长提现api
 * @author 孙建
 */
@RestController
@RequestMapping("/leader/members/withdraw")
@Api(description = "团长提现api")
public class WithdrawLeaderController {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private AccountManager accountManager;

    @PostMapping(value = "/auditing")
    @ApiOperation("提现审核")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "withdraw_bill_id", value = "提现记录id", required = true, paramType = "query", dataType = "int", allowMultiple = false),
            @ApiImplicitParam(name = "remark", value = "备注", required = false, paramType = "query", dataType = "String", allowMultiple = false),
            @ApiImplicitParam(name = "status", value = "申请结果", required = true, paramType = "query", dataType = "String", allowMultiple = false),
    })
    public void auditing(@ApiIgnore String withdrawBillId, String remark, @ApiIgnore Integer status) {
        try {
            Seller seller = UserContext.getSeller();
            AuditingDTO auditingDTO = new AuditingDTO();
            auditingDTO.setWithdrawBillId(withdrawBillId);
            auditingDTO.setInspectRemark(remark);
            auditingDTO.setStatus(status);
            auditingDTO.setOperatorId(String.valueOf(seller.getUid()));
            auditingDTO.setOperatorName(seller.getSellerName());
            accountManager.auditing(auditingDTO);
        } catch (DistributionException e) {
            logger.error("提现审核异常：", e);
            throw e;
        } catch (Exception e) {
            logger.error("提现审核异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }

}
