package com.enation.app.javashop.seller.api.statistics;

import com.enation.app.javashop.core.shop.model.vo.ShopInfoVO;
import com.enation.app.javashop.core.statistics.model.vo.AppIndexVO;
import com.enation.app.javashop.core.statistics.model.vo.ShopDashboardVO;
import com.enation.app.javashop.core.statistics.service.AppStatisticManager;
import com.enation.app.javashop.core.statistics.service.DashboardStatisticManager;
import com.enation.app.javashop.core.statistics.service.ReportsStatisticsManager;
import com.enation.app.javashop.framework.util.BeanUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 商家中心，首页数据
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018/6/25 15:13
 */
@RestController
@RequestMapping("/seller/statistics/dashboard")
@Api(description = "商家中心，首页数据")
public class DashboardStatisticsSellerController {

    @Autowired
    private DashboardStatisticManager dashboardStatisticManager;

    @Autowired
    private ReportsStatisticsManager reportsStatisticsManager;
    @Autowired
    private AppStatisticManager appStatisticManager;

    @ApiOperation(value = "首页数据",response = ShopDashboardVO.class)
    @GetMapping("/shop")
    public ShopDashboardVO shop() {
        return this.dashboardStatisticManager.getShopData();
    }


    /**
     * App首页数据 -JFENG 20200419
     * @return
     */
    @ApiOperation(value = "APP首页数据",response = AppIndexVO.class)
    @GetMapping("/appIndex")
    public AppIndexVO appIndex() {

        ShopDashboardVO shopData = this.dashboardStatisticManager.getShopData();
        AppIndexVO appIndexVO = new AppIndexVO();
        BeanUtil.copyProperties(shopData,appIndexVO);

        // 支付订单数
        Integer paidOrderNum = this.appStatisticManager.paidOrderNum();
        //交易额
        Double tradeAmount = this.appStatisticManager.tradeAmount();
        // 客单价
        Double orderAvgPrice = this.appStatisticManager.orderAvgPrice();
        //团长总数
        Integer distributorNum = this.appStatisticManager.distributorNum();
        //团均订单数
        Double distributorOrderNum = this.appStatisticManager.distributorOrderNum();
        //团长出单率
        Double distributorOrderRatio = this.appStatisticManager.distributorOrderRatio();
        // 自提点总数
        Integer siteNum = this.appStatisticManager.siteNum();
        //点均订单数
        Double siteOrderNum = this.appStatisticManager.siteOrderNum();
        //自提点出单率
        Double siteOrderRatio = this.appStatisticManager.siteOrderRatio();

        //昨日支付订单数
        Integer lastDayPaidOrderNum = this.appStatisticManager.lastDayPaidOrderNum();
        //昨日交易额
        Double lastDayTradeAmount = this.appStatisticManager.lastDayTradeAmount();
        //昨日客单价
        Double lastDayOrderAvgPrice = this.appStatisticManager.lastDayOrderAvgPrice();
        //昨日活跃团长
        Integer lastDayActiveDistributorNum = this.appStatisticManager.lastDayActiveDistributorNum();
        //昨日新增团长
        Integer lastDayNewDistributorNum = this.appStatisticManager.lastDayNewDistributorNum();
        //昨日新增自提点
        Integer lastDayNewSiteNum = this.appStatisticManager.lastDayNewSiteNum();

        ShopInfoVO shopInfoVO = this.appStatisticManager.getShopInfo();

        appIndexVO.setPaidOrderNum(paidOrderNum);
        appIndexVO.setTradeAmount(tradeAmount);
        appIndexVO.setOrderAvgPrice(orderAvgPrice);

        appIndexVO.setDistributorNum(distributorNum);
        appIndexVO.setDistributorOrderNum(distributorOrderNum);
        appIndexVO.setDistributorOrderRatio(distributorOrderRatio);

        appIndexVO.setSiteNum(siteNum);
        appIndexVO.setSiteOrderNum(siteOrderNum);
        appIndexVO.setSiteOrderRatio(siteOrderRatio);

//        appIndexVO.setWaitPayOrderNum(waitPayOrderNum);
//        appIndexVO.setAfterSaleOrderNum(afterSaleOrderNum);

        // 昨日AppIndexVO
        appIndexVO.setLastDayPaidOrderNum(lastDayPaidOrderNum);
        appIndexVO.setLastDayTradeAmount(lastDayTradeAmount);
        appIndexVO.setLastDayOrderAvgPrice(lastDayOrderAvgPrice);
        appIndexVO.setLastDayNewDistributorNum(lastDayNewDistributorNum);
        appIndexVO.setLastDayActiveDistributorNum(lastDayActiveDistributorNum);
        appIndexVO.setLastDayNewSiteNum(lastDayNewSiteNum);
        // es_shop_detail community_shop
        appIndexVO.setCommunityShop(shopInfoVO.getCommunityShop());

        return appIndexVO;
    }

}
