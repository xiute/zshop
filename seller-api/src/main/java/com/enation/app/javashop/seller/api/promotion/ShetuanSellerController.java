package com.enation.app.javashop.seller.api.promotion;

import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.base.CachePrefix;
import com.enation.app.javashop.core.excel.ExcelManager;
import com.enation.app.javashop.core.goods.model.dos.GoodsDO;
import com.enation.app.javashop.core.goods.model.dos.GoodsSkuDO;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.goods.service.GoodsQueryManager;
import com.enation.app.javashop.core.goods.service.GoodsSkuManager;
import com.enation.app.javashop.core.promotion.activitymessage.model.dos.ActivityMessageDO;
import com.enation.app.javashop.core.promotion.activitymessage.service.ActivityMessageManager;
import com.enation.app.javashop.core.promotion.shetuan.model.ShetuanVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.shetuan.model.enums.ShetuanGoodsStatusEnum;
import com.enation.app.javashop.core.promotion.shetuan.model.enums.ShetuanStatusEnum;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.*;
import com.enation.app.javashop.core.promotion.shetuan.service.*;
import com.enation.app.javashop.core.shop.ShopCatShowTypeEnum;
import com.enation.app.javashop.core.shop.model.dos.ShopCatDO;
import com.enation.app.javashop.core.shop.model.vo.ShopCatItem;
import com.enation.app.javashop.core.shop.service.ShopCatManager;
import com.enation.app.javashop.core.trade.cart.model.dos.OrderPermission;
import com.enation.app.javashop.core.trade.order.model.dto.ShetuanOrderQueryParam;
import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.enation.app.javashop.core.trade.order.model.vo.DeliveryVO;
import com.enation.app.javashop.core.trade.order.service.OrderOperateManager;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.redis.transactional.RedisTransactional;
import com.enation.app.javashop.framework.security.model.Seller;
import com.enation.app.javashop.framework.util.BeanUtil;
import io.jsonwebtoken.lang.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * 社区团购卖家
 *
 * @author JFENG
 */
@RestController
@RequestMapping("/seller/promotion/shetuan")
@Api(description = "社区团购")
@Validated
public class ShetuanSellerController {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    ShetuanManager shetuanManager;
    @Autowired
    ShetuanOperateManager shetuanOperateManager;

    @Autowired
    ShetuanGoodsManager shetuanGoodsManager;

    @Autowired
    ShetuanGoodsOperateManager shetuanGoodsOperateManager;

    @Autowired
    ShetuanOrderManager shetuanOrderManager;

    @Autowired
    ShopCatManager shopCatManager;

    @Autowired
    OrderOperateManager orderOperateManager;

    @Autowired
    private ExcelManager excelManager;

    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @Autowired
    private ActivityMessageManager activityMessageManager;

    @Autowired
    private GoodsSkuManager goodsSkuManager;

    @Autowired
    private Cache cache;

    @ApiOperation(value = "商品分页查询-db")
    @GetMapping(value = "/db/{shetuan_id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shetuan_id", value = "活动id", required = true, dataType = "int", paramType = "path")
    })
    public ShetuanVO getShetuanDetailFromDB(@ApiIgnore @PathVariable(value = "shetuan_id") Integer shetuanId) {
        ShetuanVO shetuanVO = this.shetuanManager.getShetuanDetailFromDB(shetuanId);
        return shetuanVO;
    }


    @PostMapping("add")
    @ApiOperation(value = "新增社区团购活动")
    public void add(@Valid @RequestBody ShetuanAddVO vo) {
        logger.info("【****新增社区团购活动*****】"+vo.toString());
        Assert.notNull(vo.getStartTime(), "开售时间不能为空");
        Assert.notNull(vo.getEndTime(), "结束时间不能为空");
        Assert.notNull(vo.getPickTime(), "提货时间不能为空");

        ShetuanDO shetuanDO = new ShetuanDO();
        BeanUtil.copyProperties(vo, shetuanDO);

        shetuanDO.setSellerName(UserContext.getSeller().getSellerName());
        shetuanDO.setSellerId(UserContext.getSeller().getSellerId());
        shetuanDO.setStatus(ShetuanStatusEnum.OFF_LINE.getIndex());

        this.shetuanManager.add(shetuanDO);
    }

    @PostMapping("edit")
    @ApiOperation(value = "修改社区团购活动")
    public boolean edit(@RequestBody ShetuanDO shetuanDO) {
        this.shetuanManager.edit(shetuanDO);
        return true;
    }

    @PostMapping("sign_up/{shetuan_id}")
    @ApiOperation(value = "设置开始报名中")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shetuan_id", value = "活动id", required = true, dataType = "int", paramType = "path")
    })
    public boolean signUp(@ApiIgnore @PathVariable(value = "shetuan_id") Integer shetuanId) {
        this.shetuanManager.startSignUp(shetuanId);
        return true;
    }

    @PostMapping("on_line/{shetuan_id}")
    @ApiOperation(value = "活动上线")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shetuan_id", value = "活动id", required = true, dataType = "int", paramType = "path")
    })
    public ShetuanDO onLine(@ApiIgnore @PathVariable(value = "shetuan_id") Integer shetuanId) {
        return this.shetuanOperateManager.onLine(shetuanId);
    }

    @PostMapping("off_line/{shetuan_id}")
    @ApiOperation(value = "活动下线")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shetuan_id", value = "活动id", required = true, dataType = "int", paramType = "path")
    })
    public ShetuanDO offLine(@ApiIgnore @PathVariable(value = "shetuan_id") Integer shetuanId) {
        return this.shetuanOperateManager.offLine(shetuanId);
    }


    @DeleteMapping("shetuan_delete/{shetuan_id}")
    @ApiOperation(value = "活动删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shetuan_id", value = "活动id", required = true, dataType = "int", paramType = "path")
    })
    public boolean shetuanDelete(@ApiIgnore @PathVariable(value = "shetuan_id") Integer shetuanId) {
        this.shetuanManager.delete(shetuanId);
        return true;
    }

    @DeleteMapping("shetuan_delete_new/{shetuan_id}")
    @ApiOperation(value = "社区团购待上线活动删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shetuan_id", value = "活动id", required = true, dataType = "int", paramType = "path")
    })
    public boolean newStatusShetuanDelete(@ApiIgnore @PathVariable(value = "shetuan_id") Integer shetuanId) {
        this.shetuanManager.deleteNewStatus(shetuanId);
        return true;
    }


    /**
     * 查询社区团购的活动
     */
    @GetMapping("page")
    @ApiOperation(value = "团购列表分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", dataType = "int", paramType = "query")
    })
    public Page<ShetuanDO> getPage(@RequestParam(name = "page_no") Integer page_no, @RequestParam(name = "page_size") Integer page_size) {
        Integer sellerId = UserContext.getSeller().getSellerId();

        return this.shetuanManager.getPageBySellerId(page_size, page_no, sellerId);
    }

    /**
     * 前端sku商品选择器组件，涉及活动商品【增加】【删除】操作
     */
    @PostMapping("add_goods")
    @ApiOperation(value = "社区团购活动添加商品")
    @RedisTransactional(acquireTimeout = 1,lockTimeout = 30,lockName = "ADD_SHETUAN_GOODS")
    public boolean addShetuanGoods(@RequestBody ShetuanGoodsListVO vo) {
        logger.info("【****社区团购活动添加商品*****】"+vo.toString());

        List<ShetuanGoodsDO> goodsList = vo.getGoodsList();
        List<ShetuanDO> overlapShetuan = shetuanManager.getOverlapShetuan(vo.getShetuanId());
        if (!CollectionUtils.isEmpty(overlapShetuan)) {
            List<ShetuanGoodsDO> overlapGoods = this.shetuanGoodsManager.getOverlapGoods(goodsList, overlapShetuan);
            if (!CollectionUtils.isEmpty(overlapGoods)) {
                List<String> overlapGoodsNameList = overlapGoods.stream().map(ShetuanGoodsDO::getGoodsName).collect(Collectors.toList());
                String names = StringUtils.join(overlapGoodsNameList, ",");
                throw new ServiceException("500", "【商品冲突】 : " + names);
            }
        }
        //批量batch添加  updateTime：2020年9月14日 19:15:12 王志杨
        List<ShetuanGoodsDO> shetuanGoodsDOList = new ArrayList<>();
        // 已下架商品 设置为【已下架】 无库存商品应 设置为【已下架】
        List<Integer> goodsIds = vo.getGoodsList().stream().map(ShetuanGoodsDO::getGoodsId).collect(Collectors.toList());
        Map<Integer, GoodsDO> goodsDOMap = getGoodsMap(goodsIds);
        for (ShetuanGoodsDO shetuanGoodsDO : vo.getGoodsList()) {

            // 查询商品库存
            GoodsSkuVO skuVO = goodsSkuManager.getSkuFromCache(shetuanGoodsDO.getSkuId());
            if (skuVO == null) {
                continue;
            }
            ShetuanGoodsDO needInsertShetuanGoodsDO = this.shetuanGoodsOperateManager.composeAddStGoods(skuVO,shetuanGoodsDO, goodsDOMap.get(shetuanGoodsDO.getGoodsId()),UserContext.getSeller());
            if(needInsertShetuanGoodsDO!=null){
                shetuanGoodsDOList.add(needInsertShetuanGoodsDO);
            }
        }

        //需要做添加的社团商品集合，批量添加操作
        if(!CollectionUtils.isEmpty(shetuanGoodsDOList)){
            this.shetuanGoodsOperateManager.batchAddShetuanGoods(shetuanGoodsDOList);
        }

        if(vo.getDeleteIds()!=null && vo.getDeleteIds().length>5){
            throw new ServiceException("500", "【社团团购商品删除】 : 一次性删除不能超过5条" );
        }

        // 删除商品
        for (Integer id : vo.getDeleteIds()) {
            this.shetuanGoodsOperateManager.operateStGoodsStatus(id, ShetuanGoodsStatusEnum.DELETE.getIndex());
        }
        return true;
    }

    private Map<Integer, GoodsDO> getGoodsMap(List<Integer> goodsIds) {
        List<GoodsDO> goodsDOList = this.goodsQueryManager.queryByIds(goodsIds);
        return goodsDOList.stream().collect(Collectors.toMap(GoodsDO::getGoodsId, Function.identity()));
    }

    @PostMapping("op_line/{id}/{op_type}")
    @ApiOperation(value = "商品上下线及删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "活动商品的id（非goodsId)", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "op_type", value = "操作类型，ON 代表上线， OFF代表下线,DELETE 代表删除", required = true, dataType = "string", paramType = "path")
    })
    public boolean opLine(@ApiIgnore @PathVariable(value = "id") Integer id,
                          @ApiIgnore @PathVariable(value = "op_type") String opType) {
        ShetuanGoodsStatusEnum statusEnum=null;
        if (opType.equals("ON")) {
            statusEnum= ShetuanGoodsStatusEnum.ON_LINE;
        }
        if (opType.equals("OFF")) {
            statusEnum= ShetuanGoodsStatusEnum.OFF_LINE;
        }
        if (opType.equals("DELETE")) {
            statusEnum= ShetuanGoodsStatusEnum.DELETE;
        }

        this.shetuanGoodsOperateManager.operateStGoodsStatus(id, statusEnum.getIndex());

        return true;
    }



    /**
     * 社区团购商品查询
     *
     * @param queryShetuanGoodsVO 社区团购商品查询对象
     * @return 分页对象
     */
    @PostMapping("goods_page")
    @ApiOperation(value = "活动商品列表")
    public Page getGoodsPage(@RequestBody QueryShetuanGoodsVO queryShetuanGoodsVO) {
        ShetuanDO shetuanDO = this.shetuanManager.getShetuanById(queryShetuanGoodsVO.getShetuanId());
        String shetuanStatusText = ShetuanStatusEnum.getTextByIndex(shetuanDO.getStatus());
        Page<ShetuanGoodsVO> page = this.shetuanGoodsManager.queryByPage(queryShetuanGoodsVO);
        List<ShetuanGoodsVO> goodsList = page.getData();

        //查询店铺分组
        Map<Integer, String> shopCatIndex = getShopCatIndex();

        List<Integer> skuIds = goodsList.stream().map(ShetuanGoodsVO::getSkuId).collect(Collectors.toList());
        List<GoodsSkuDO> skuList =goodsSkuManager.listBySkuIds(skuIds);
        Map<Integer, GoodsSkuDO> skuMap = skuList.stream().collect(Collectors.toMap(GoodsSkuDO::getSkuId, Function.identity()));

        for (ShetuanGoodsVO shetuanGoodsVO : goodsList) {
            shetuanGoodsVO.setShetuanName(shetuanDO.getShetuanName());
            shetuanGoodsVO.setShetuanStatus(shetuanDO.getStatus());
            shetuanGoodsVO.setShetuanStatusText(shetuanStatusText);
            shetuanGoodsVO.setSellerName(shetuanDO.getSellerName());
            shetuanGoodsVO.setShetuanGoodsStatusText(ShetuanGoodsStatusEnum.getTextByIndex(shetuanGoodsVO.getStatus()));

            // 店铺分组
            if (shetuanGoodsVO.getShopCatId() != null) {
                String  catPathName = shopCatIndex.get(shetuanGoodsVO.getShopCatId());
                shetuanGoodsVO.setShopCatName(catPathName);
            }
            if(StringUtils.isNotEmpty(shetuanGoodsVO.getShopCatItems())){
                shetuanGoodsVO.setShopCatItemsList(JSON.parseArray(shetuanGoodsVO.getShopCatItems(), ShopCatItem.class));
            }

            // 当前剩余库存
            GoodsSkuDO goodsSkuDO = skuMap.get(shetuanGoodsVO.getSkuId());
            if(goodsSkuDO==null){
                continue;
            }
            shetuanGoodsVO.setEnableQuantity(goodsSkuDO.getEnableQuantity());
        }
        return page;
    }

    private Map<Integer, String> getShopCatIndex() {
        String key = CachePrefix.SHOP_CAT_INDEX.getPrefix() + UserContext.getSeller().getSellerId();
        Map<Integer, String> shopCatIndex = (Map<Integer, String>) cache.get(key);

        if(shopCatIndex==null){
            List<ShopCatDO> shopCatDOS = this.shopCatManager.listAll(UserContext.getSeller().getSellerId(), ShopCatShowTypeEnum.ALL.name());
            shopCatIndex = shopCatDOS.stream().collect(Collectors.toMap(ShopCatDO::getShopCatId, ShopCatDO::getCatPathName));
            cache.put(key,shopCatIndex);
        }
        return shopCatIndex;
    }


    /**
     * 只更新es_shetuan_goods 和索引
     * 全量编辑社团团购商品
     */
    @PostMapping("editGoods")
    @ApiOperation(value = "全量编辑社团团购商品")
    public boolean editGoods(@RequestBody ShetuanGoodsListVO goodsListVO) {
        return this.shetuanGoodsOperateManager.batchUpdateStGoods(goodsListVO.getGoodsList());
    }

    /**
     * 单个修改社区团购商品
     */
    @PostMapping("editOneGoods")
    @ApiOperation(value = "单个修改社区团购商品")
    public void editOneGoods(@RequestBody ShetuanGoodsVO shetuanGoodsVO) {

        this.shetuanGoodsOperateManager.updateStGoods(shetuanGoodsVO);
    }

    /**
     * 规则：复制目标活动中未被删除的商品（待上架，已上架，已下架）
     * 复制社区团购活动
     */
    @PostMapping("copyShetuan")
    @ApiOperation(value = "复制社区团购活动")
    @RedisTransactional(acquireTimeout = 30)
    public boolean copyShetuan(@RequestBody ShetuanDO shetuanDO) {
        Seller seller = UserContext.getSeller();
        // 查询目标活动
        Integer shetuanId = shetuanDO.getShetuanId();
        shetuanDO.setSellerId(seller.getSellerId());
        shetuanDO.setSellerName(seller.getSellerName());
        ShetuanVO shetuanDetailFromDB = this.shetuanManager.getShetuanDetailFromDB(shetuanId);
        //新建活动
        shetuanDO.setShetuanId(null);
        ShetuanDO newShetuan = this.shetuanManager.add(shetuanDO);
        // 新建活动商品 ---- > 批量batch添加  updateTime：2020年9月14日 19:15:12 王志杨
        List<ShetuanGoodsDO> shetuanGoodsDOList = new ArrayList<>();
        List<ShetuanGoodsDO> shetuanGoodsList = shetuanDetailFromDB.getShetuanGoodsList();

        List<Integer> goodsIds = shetuanGoodsList.stream().map(ShetuanGoodsDO::getGoodsId).collect(Collectors.toList());
        Map<Integer, GoodsDO> goodsDOMap = getGoodsMap(goodsIds);

        for (ShetuanGoodsDO shetuanGoodsDO : shetuanGoodsList) {
            shetuanGoodsDO.setShetuanId(newShetuan.getShetuanId());
            shetuanGoodsDO.setBuyNum(0);

            // 查询商品库存
            GoodsSkuVO skuVO = goodsSkuManager.getSkuFromCache(shetuanGoodsDO.getSkuId());
            if (skuVO == null) {
                continue;
            }

            ShetuanGoodsDO needInsertShetuanGoodsDO = this.shetuanGoodsOperateManager.composeAddStGoods(  skuVO, shetuanGoodsDO,goodsDOMap.get(shetuanGoodsDO.getGoodsId()), seller);
            if(needInsertShetuanGoodsDO==null){
                logger.error("【添加商品异常】");
                logger.error(shetuanGoodsDO);
                continue;
            }
            shetuanGoodsDOList.add(needInsertShetuanGoodsDO);
        }
        //需要做添加的社团商品集合，批量添加操作
        if(!CollectionUtils.isEmpty(shetuanGoodsDOList)){
            this.shetuanGoodsOperateManager.batchAddShetuanGoods(shetuanGoodsDOList);
        }
        return true;
    }


    /**
     * 删除社区团购商品
     */
    @PostMapping("delete_goods")
    @ApiOperation(value = "全量编辑社团团购商品")
    public boolean deleteGoods(@RequestBody ShetuanGoodsListVO goodsListVO) {
        Integer[] deleteIds = goodsListVO.getDeleteIds();
        // 删除商品
        for (Integer id : deleteIds) {
            this.shetuanGoodsOperateManager.operateStGoodsStatus(id, ShetuanGoodsStatusEnum.DELETE.getIndex());
        }
        return true;
    }

    /**
     * 批量上架商品
     */
    @PostMapping("up_goods")
    @ApiOperation(value = "批量上架商品")
    public boolean upGoods(@RequestBody ShetuanGoodsListVO goodsListVO) {
        Integer[] goodsIds = goodsListVO.getGoodsIds();
        for (Integer id : goodsIds) {
            this.shetuanGoodsOperateManager.operateStGoodsStatus(id, ShetuanGoodsStatusEnum.ON_LINE.getIndex());
        }
        return true;
    }
    /**
     * 批量下架商品
     */
    @PostMapping("drop_goods")
    @ApiOperation(value = "批量下架商品")
    public boolean dropGoods(@RequestBody ShetuanGoodsListVO goodsListVO) {
        Integer[] goodsIds = goodsListVO.getGoodsIds();
        for (Integer id : goodsIds) {
            this.shetuanGoodsOperateManager.operateStGoodsStatus(id, ShetuanGoodsStatusEnum.OFF_LINE.getIndex());
        }
        return true;
    }


    /**
     * 社区团购订单列表
     */
    @PostMapping("shetuanOrderPage")
    @ApiOperation(value = "社区团购订单列表")
    public Page<ShetuanOrderVO> shetuanOrderPage(@RequestBody ShetuanOrderQueryParam shetuanOrderQueryParam) {
        shetuanOrderQueryParam.setSellerId(UserContext.getSeller().getSellerId());
        return this.shetuanOrderManager.queryPage(shetuanOrderQueryParam);
    }


    @ApiOperation(value = "社团团购订单发货", notes = "社团团购订单发货")
    @PostMapping(value = "/{order_sn_list}/delivery")
    @ApiImplicitParams({@ApiImplicitParam(name = "order_sn_list", value = "要查询的订单编号", required = true, dataType = "String", paramType = "path", allowMultiple = true)})
    public String ship(@ApiIgnore @NotNull(message = "必须指定订单编号") @PathVariable(name = "order_sn_list") String[] orderSnList,
                       @RequestParam(value = "shipping_type")String shippingType,
                       @RequestParam(value = "logi_id",required = false)Integer logiId,
                       @RequestParam(value = "logi_name",required = false)String logiName,
                       @RequestParam(value = "ship_no",required = false)String shipNo) {
        for (String orderSn : orderSnList) {
            DeliveryVO delivery = new DeliveryVO();
            delivery.setShipType(shippingType);
            delivery.setOrderSn(orderSn);
            if (shippingType.equals(ShipTypeEnum.SELF.name())) {
                delivery.setDeliveryNo("00000");
                delivery.setLogiName("自提配送");
                delivery.setLogiId(0);
            } else {
                delivery.setDeliveryNo(shipNo);
                delivery.setLogiName(logiName);
                delivery.setLogiId(logiId);
            }
            orderOperateManager.ship(delivery, OrderPermission.seller);
        }

        return "";
    }

    @PostMapping("group_purchase_export")
    @ApiOperation(value = "社区团购订单导出excle")
    public void groupPurchaseExport(@RequestBody ShetuanOrderQueryParam shetuanOrderQueryParam) {
        shetuanOrderQueryParam.setSellerId(UserContext.getSeller().getSellerId());
        shetuanOrderQueryParam.setPageSize(5000);
        excelManager.groupPurchaseExport(shetuanOrderQueryParam);

    }


    @ApiOperation(value = "导出理社团商品信息")
    @GetMapping(value = "/shetuanGoodsExport")
    public void shetuanGoodsExport(QueryShetuanGoodsVO queryShetuanGoodsVO) {
        Seller seller = UserContext.getSeller();
        queryShetuanGoodsVO.setPageSize(1000);
        excelManager.shetuanGoodsExport(queryShetuanGoodsVO,seller);
    }

    @ApiOperation(value = "导入社团商品信息")
    @ApiImplicitParam(name = "uploadFile", value = "社团商品信息文件.xlsx", paramType = "form", dataType = "__File", required = true)
    @PostMapping(value = "/shetuanGoodsImport", headers = "content-type=multipart/form-data")
    public void shetuanGoodsImport(@RequestParam("uploadFile") MultipartFile uploadFile){
        excelManager.importShetuanGoods(uploadFile);
    }

    /**
     * 配置活动推送信息
     */
    @PostMapping("activity_push_information")
    @ApiOperation(value = "配置活动推送信息")
    public void activityPushInformation (@RequestBody ActivityMessageDO activityMessageDO) {
        activityMessageManager.addActivityMessage(activityMessageDO);
    }

}
