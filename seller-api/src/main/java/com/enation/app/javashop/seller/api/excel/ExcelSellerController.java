package com.enation.app.javashop.seller.api.excel;

import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.aftersale.model.vo.RefundQueryParamVO;
import com.enation.app.javashop.core.base.context.Region;
import com.enation.app.javashop.core.base.context.RegionFormat;
import com.enation.app.javashop.core.base.model.vo.SuccessMessage;
import com.enation.app.javashop.core.client.member.MemberHistoryReceiptClient;
import com.enation.app.javashop.core.distribution.exception.DistributionException;
import com.enation.app.javashop.core.excel.ExcelManager;
import com.enation.app.javashop.core.member.model.vo.LeaderVO;
import com.enation.app.javashop.core.member.service.LeaderManager;
import com.enation.app.javashop.core.trade.cart.model.dos.OrderPermission;
import com.enation.app.javashop.core.trade.order.model.dos.SortingSerial;
import com.enation.app.javashop.core.trade.order.model.dto.OrderDetailQueryParam;
import com.enation.app.javashop.core.trade.order.model.dto.OrderQueryParam;
import com.enation.app.javashop.core.trade.order.model.enums.OrderTypeEnum;
import com.enation.app.javashop.core.trade.order.model.enums.PaymentTypeEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.enation.app.javashop.core.trade.order.model.vo.*;
import com.enation.app.javashop.core.trade.order.service.OrderOperateManager;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 商家订单控制器
 *
 * @author Snow create in 2018/6/13
 * @version v2.0
 * @since v7.0.0
 */

@Api(description = "商家导出")
@RestController
@RequestMapping("/seller/excel")
@Validated
public class ExcelSellerController {

    @Autowired
    private ExcelManager excelManager;

    @ApiOperation(value = "导出会员订单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "channel", value = "渠道", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "keywords", value = "关键字", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "order_sn", value = "订单编号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "buyer_name", value = "买家姓名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "goods_name", value = "商品名称", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "start_time", value = "开始时间", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "结束时间", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "ship_name", value = "收货人名称", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "ship_mobile", value = "收货人手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "order_status", value = "订单状态", dataType = "String", paramType = "query",
                    allowableValues = "ALL,WAIT_PAY,WAIT_SHIP,WAIT_ROG,CANCELLED,COMPLETE,WAIT_COMMENT,REFUND,WAIT_REFUND",
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货,WAIT_REFUND:待退款," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中"),
            @ApiImplicitParam(name = "page_no", value = "页数", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "条数", dataType = "int", paramType = "query")
    })
    @GetMapping("/exportOrdersItem")
    public void exportOrders(@ApiIgnore String orderSn, @ApiIgnore String buyerName, @ApiIgnore String goodsName,
                             @ApiIgnore Long startTime, @ApiIgnore Long endTime, @ApiIgnore String shipName, @ApiIgnore String shipMobile, @ApiIgnore String orderStatus,
                             @ApiIgnore String keywords) {

        Seller seller = UserContext.getSeller();

        OrderQueryParam param = new OrderQueryParam();
        param.setOrderSn(orderSn);
        param.setShipName(shipName);
        param.setGoodsName(goodsName);
        param.setBuyerName(buyerName);
        param.setTag(orderStatus);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        param.setPageNo(1);
        param.setPageSize(2000);
        param.setSellerId(seller.getSellerId());
        param.setKeywords(keywords);
        param.setShipMobile(shipMobile);
        param.setOrderType(OrderTypeEnum.normal.name());

        excelManager.exportSellerOrderItems(param);

    }


    @ApiOperation(value = "导入商品库存")
    @PostMapping(value = "/importGoodsQuantity")
    public void importGoods(@RequestParam MultipartFile file) {
        excelManager.importGoodsQuantity(file);
    }

}
