package com.enation.app.javashop.seller.api.promotion;

import com.enation.app.javashop.core.promotion.seckill.model.dos.SeckillApplyDO;
import com.enation.app.javashop.core.promotion.seckill.model.dos.SeckillDO;
import com.enation.app.javashop.core.promotion.seckill.model.dos.SeckillRangeDO;
import com.enation.app.javashop.core.promotion.seckill.model.dto.SeckillQueryParam;
import com.enation.app.javashop.core.promotion.seckill.model.enums.SeckillStatusEnum;
import com.enation.app.javashop.core.promotion.seckill.model.enums.SeckillTypeEnum;
import com.enation.app.javashop.core.promotion.seckill.model.vo.SeckillVO;
import com.enation.app.javashop.core.promotion.seckill.service.SeckillGoodsManager;
import com.enation.app.javashop.core.promotion.seckill.service.SeckillManager;
import com.enation.app.javashop.core.promotion.seckill.service.SeckillRangeManager;
import com.enation.app.javashop.core.trade.cart.model.enums.CartSourceType;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.NoPermissionException;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.exception.SystemErrorCodeV1;
import com.enation.app.javashop.framework.security.model.Seller;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.DictUtils;
import com.enation.app.javashop.framework.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * 限时抢购申请控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 17:30:09
 */
@RestController
@RequestMapping("/seller/promotion/seckill-applys")
@Api(description = "限时抢购申请相关API")
@Validated
public class SeckillApplySellerController {

    @Autowired
    private SeckillGoodsManager seckillApplyManager;

    @Autowired
    private SeckillRangeManager seckillRangeManager;

    @Autowired
    private SeckillManager seckillManager;

    @ApiOperation(value = "创建orB编辑 秒杀活动")
    @PostMapping("/{seckill_id}/release")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "seckill_id",	value = "要查询的限时抢购入库主键",	required = true, dataType = "int",	paramType = "path")
    })
    public SeckillVO publish(@Valid @RequestBody SeckillVO seckill,@ApiIgnore @PathVariable("seckill_id") Integer seckillId){

        seckill.verifyParam(seckill);
        //发布状态
        seckill.setSeckillStatus(SeckillStatusEnum.RELEASE.name());
        seckill.setSeckillType(SeckillTypeEnum.SELLLER.value());
        seckill.setSellerIds(UserContext.getSeller().getSellerId()+"");
        if(seckillId==null || seckillId==0){
            seckillManager.add(seckill);
        }else{
            seckillManager.edit(seckill,seckillId);
        }

        return seckill;
    }

    /**
     * 秒杀活动商品增删改
     * @param seckillApplyList
     * @return
     */
    @ApiOperation(value = "设置限时秒杀商品", response = SeckillApplyDO.class)
    @PostMapping
    public List<SeckillApplyDO> add(@Valid @RequestBody @NotEmpty(message = "申请参数为空") List<SeckillApplyDO> seckillApplyList) {

        SeckillApplyDO applyDO = seckillApplyList.get(0);
        this.verifyParam(seckillApplyList, applyDO.getSeckillId());
        this.seckillApplyManager.addOrUpdateApply(seckillApplyList);

        return seckillApplyList;
    }



    @GetMapping(value = "/{seckill_id}/seckill")
    @ApiOperation(value = "根据秒杀活动id 查询秒杀活动详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "seckill_id", value = "要查询的限时抢购活动主键", required = true, dataType = "int", paramType = "path")
    })
    public SeckillVO getSeckill(@ApiIgnore @PathVariable("seckill_id") Integer seckillId) {
        SeckillVO seckill = this.seckillManager.getSeckillDetail(seckillId);
        return seckill;
    }



    @ApiOperation(value = "查询限时抢购申请商品列表", response = SeckillApplyDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "seckill_id", value = "限时抢购活动id", dataType = "int", paramType = "query")
    })
    @GetMapping
    public Page<SeckillVO> list(@ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize,
                                @ApiIgnore @NotNull(message = "限时抢购活动参数为空") Integer seckillId) {

        SeckillQueryParam queryParam = new SeckillQueryParam();
        queryParam.setPageNo(pageNo);
        queryParam.setPageSize(pageSize);
        queryParam.setSeckillId(seckillId);
        return this.seckillApplyManager.list(queryParam);
    }



    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "删除限时抢购申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要删除的限时抢购申请主键", required = true, dataType = "int", paramType = "path")
    })
    public String delete(@PathVariable Integer id) {

        this.seckillApplyManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @ApiOperation(value = "查询一个限时抢购申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要查询的限时抢购申请主键", required = true, dataType = "int", paramType = "path")
    })
    public SeckillApplyDO get(@PathVariable Integer id) {
        SeckillApplyDO seckillApply = this.seckillApplyManager.getModel(id);

        Seller seller = UserContext.getSeller();
        //验证越权操作
        if (seckillApply == null || seller.getSellerId().intValue() != seckillApply.getSellerId().intValue()) {
            throw new NoPermissionException("无权操作");
        }

        return seckillApply;
    }


    @ApiOperation(value = "查询所有的限时抢购活动")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "keywords", value = "关键字", dataType = "String", paramType = "query"),
    })
    @GetMapping(value = "/seckill")
    public Page list(@ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize,
                     @ApiIgnore String keywords) {
        SeckillQueryParam seckillQueryParam = new SeckillQueryParam();
        seckillQueryParam.setKeywords(keywords);
        Integer sellerId = UserContext.getSeller().getSellerId();

        String dictValue = DictUtils.getDictValue("", "SHETUAN_SELLER", "SHETUAN_SELLER");
        if (StringUtils.isNotEmpty(dictValue) && dictValue.contains(sellerId + "")) {
            seckillQueryParam.setSellerId(sellerId);
        }

        Page page = this.seckillManager.list(pageNo, pageSize, seckillQueryParam);
        List<SeckillDO> list = page.getData();


        List<SeckillVO> seckillVOList = new ArrayList<>();
        for (SeckillDO seckillDO : list) {

            //处于编辑中的显示抢购活动，不在商家显示  update -by liuyulei 2019-05-23
            if(SeckillStatusEnum.EDITING.name().equals(seckillDO.getSeckillStatus())){
                continue;
            }

            String shopIds = seckillDO.getSellerIds();

            String[] shopId = new String[0];

            if (!StringUtil.isEmpty(shopIds)) {
                shopId = shopIds.split(",");
            }

            SeckillVO seckillVO = new SeckillVO();
            BeanUtils.copyProperties(seckillDO, seckillVO);

            if (Arrays.asList(shopId).contains(sellerId + "")) {
                seckillVO.setIsApply(1);

                //未报名
            } else {
                seckillVO.setIsApply(0);
            }
            seckillVOList.add(seckillVO);
        }
        page.setData(seckillVOList);
        return page;
    }


    /**
     * 验证参数的正确性
     *
     * @param applyDOList
     * @param seckillId   限时抢购活动id
     */
    private void verifyParam(List<SeckillApplyDO> applyDOList, Integer seckillId) {


        //根据限时抢购活动id 读取所有的时刻集合
        List<SeckillRangeDO> list = this.seckillRangeManager.getList(seckillId);
        List<Integer> rangIdList = new ArrayList<>();

        for (SeckillRangeDO seckillRangeDO : list) {
            rangIdList.add(seckillRangeDO.getRangeTime());
        }

        /**
         * 存储参加活动的商品id，用来判断同一个商品不能重复参加某个活动
         */
        Map<Integer, Integer> map = new HashMap<>();

        for (SeckillApplyDO applyDO : applyDOList) {

            Seller seller = UserContext.getSeller();
            applyDO.setSellerId(seller.getSellerId());
            applyDO.setShopName(seller.getSellerName());

            if (applyDO.getSeckillId() == null || applyDO.getSeckillId().intValue() == 0) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "限时抢购活动ID参数异常");
            }

            if (applyDO.getTimeLine() == null) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "时刻参数异常");
            } else {

                //判断此活动的时刻集合是否包含正要添加的时刻,如果不包含说明时刻参数有异常
                if (!rangIdList.contains(applyDO.getTimeLine())) {
                    throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "时刻参数异常");
                }
            }

            if (applyDO.getStartDay() == null || applyDO.getStartDay().intValue() == 0) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动开始时间参数异常");
            }

            if (applyDO.getSkuId() == null || applyDO.getSkuId().intValue() == 0) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "商品sku ID参数异常");
            }

            if (StringUtil.isEmpty(applyDO.getGoodsName())) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "商品名称参数异常");
            }

            if (applyDO.getPrice() == null || applyDO.getPrice() < 0) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "抢购价参数不能小于0");
            }

            if (applyDO.getSoldQuantity() == null || applyDO.getSoldQuantity().intValue() <= 0) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "售空数量参数不能小于0");
            }

            if (applyDO.getPrice() > applyDO.getOriginalPrice()) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动价格不能大于商品原价");
            }

            if (map.get(applyDO.getSkuId()) != null) {
                throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, applyDO.getGoodsName() + ",该商品不能同时参加多个时间段的活动");
            }

            map.put(applyDO.getSkuId(), applyDO.getSkuId());

        }
    }


}


