package com.enation.app.javashop.seller.api.statistics;

import com.enation.app.javashop.core.base.SearchCriteria;
import com.enation.app.javashop.core.orderbill.service.BillManager;
import com.enation.app.javashop.core.statistics.model.vo.IncomeDataVO;
import com.enation.app.javashop.core.statistics.model.vo.ShopProfileVO;
import com.enation.app.javashop.core.statistics.model.vo.SimpleChart;
import com.enation.app.javashop.core.statistics.service.OrderStatisticManager;
import com.enation.app.javashop.core.statistics.service.ShopProfileStatisticsManager;
import com.enation.app.javashop.core.statistics.util.DateUtil;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.PeriodTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.time.Period;
import java.util.Date;

/**
 * 商家中心统计，店铺概况
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018年4月18日上午11:30:50
 */
@Api(description = "商家统计 店铺概况")
@RestController
@RequestMapping("/seller/statistics/shop_profile")
public class ShopProfileStatisticsSellerController {

    @Autowired
    private ShopProfileStatisticsManager shopProfileStatisticsManager;

    @Autowired
    private OrderStatisticManager orderAnalysisManager;

    @ApiOperation(value = "获取30天店铺概况展示数据", response = ShopProfileVO.class)
    @GetMapping("/data")
    public ShopProfileVO getLast30dayStatus() {
        return this.shopProfileStatisticsManager.data();
    }

    @ApiOperation(value = "获取30天店铺下单金额统计图数据", response = SimpleChart.class)
    @GetMapping("/chart")
    public SimpleChart getLast30dayLineChart() {
        return this.shopProfileStatisticsManager.chart(30);
    }

    /**
     * 下单数据
     *
     * @param periodType
     * @return
     */
    @ApiOperation(value = "收入汇总", response = IncomeDataVO.class)
    @GetMapping("/incomeChart/{period_type}")
    public IncomeDataVO incomeChart(@PathVariable(value = "period_type") Integer periodType) {
        PeriodTypeEnum period = PeriodTypeEnum.resolve(periodType);
        if (period == null) {
            period = PeriodTypeEnum.WEEK;
        }
        SimpleChart chart = this.shopProfileStatisticsManager.chart(period.getText());
        IncomeDataVO incomeDataVO = new IncomeDataVO();
        incomeDataVO.setSimpleChart(chart);
        incomeDataVO.setDayMax(666.00);
        incomeDataVO.setWeekIncome(1888.00);
        incomeDataVO.setDayAverage(777.00);
        incomeDataVO.setSimpleChart(chart);
        return incomeDataVO;
    }
}
