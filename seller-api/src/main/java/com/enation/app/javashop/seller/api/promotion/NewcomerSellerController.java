package com.enation.app.javashop.seller.api.promotion;

import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import com.enation.app.javashop.core.promotion.newcomer.model.dos.NewcomerDO;
import com.enation.app.javashop.core.promotion.newcomer.model.dos.NewcomerGoodsDO;
import com.enation.app.javashop.core.promotion.newcomer.model.vos.QueryNewcomerVO;
import com.enation.app.javashop.core.promotion.newcomer.service.NewcomerGoodsManager;
import com.enation.app.javashop.core.promotion.newcomer.service.NewcomerManager;
import com.enation.app.javashop.framework.database.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2021/2/25
 * @Description: 查询新人购相关
 */
@RestController
@RequestMapping("/seller/promotion/newcomer")
@Api(description = "新人购活动API")
public class NewcomerSellerController {

    @Autowired
    private NewcomerManager newcomerManager;

    @Autowired
    private NewcomerGoodsManager newcomerGoodsManager;

    // 查询新人购活动列表
    @ApiOperation(value = "查询新人购活动列表")
    @GetMapping(value = "/queryList")
    public Page queryNewcomerList(QueryNewcomerVO queryNewcomerVO) {
        return newcomerManager.queryNewcomerList(queryNewcomerVO);
    }

    @ApiOperation(value = "新增或修改新人购活动")
    @PostMapping(value = "/addOrUpdate")
    public void addOrUpdate(@RequestBody NewcomerDO newcomerDO) {
        newcomerManager.addOrUpdateNewcomer(newcomerDO);
    }

    // 删除新人购活动
    @ApiOperation(value = "删除新人购活动")
    @GetMapping(value = "/delete/{newcomer_id}")
    public JsonBean deleteNewcomer(@PathVariable("newcomer_id") Integer newcomerId) {
        newcomerManager.deleteNewcomer(newcomerId);
        return new JsonBean();
    }

    // 活动上下线
    @ApiOperation(value = "上线或下线")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "newcomer_id", value = "活动Id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "上线或下线", required = true, dataType = "int", paramType = "query"),
    })
    @PostMapping(value = "/onOffLine")
    public JsonBean onOffLine(@ApiIgnore Integer newcomerId, @ApiIgnore Integer status) {
        newcomerManager.onOffLine(newcomerId, status);
        return new JsonBean();
    }

    @ApiOperation(value = "添加新人购商品")
    @PostMapping(value = "/addNewcomerGoods")
    public JsonBean addNewcomerGoods(@RequestBody List<NewcomerGoodsDO> newcomerGoodsDOList) {
        newcomerGoodsManager.addNewcomerGoods(newcomerGoodsDOList);
        return new JsonBean();
    }

    @ApiOperation(value = "获取新人购商品列表")
    @GetMapping(value = "/getNewcomerGoodsList/{newcomer_id}")
    public List<NewcomerGoodsDO> getNewcomerGoodsList(@PathVariable("newcomer_id") Integer newcomerId) {
        return newcomerGoodsManager.getNewcomerGoodsList(newcomerId);
    }
}
