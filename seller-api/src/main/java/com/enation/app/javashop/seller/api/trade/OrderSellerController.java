package com.enation.app.javashop.seller.api.trade;

import com.enation.app.javashop.core.aftersale.AftersaleErrorCode;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.aftersale.model.vo.RefundQueryParamVO;
import com.enation.app.javashop.core.aftersale.model.vo.Result;
import com.enation.app.javashop.core.base.context.Region;
import com.enation.app.javashop.core.base.context.RegionFormat;
import com.enation.app.javashop.core.base.model.vo.SuccessMessage;
import com.enation.app.javashop.core.client.member.MemberHistoryReceiptClient;
import com.enation.app.javashop.core.distribution.exception.DistributionException;
import com.enation.app.javashop.core.member.model.vo.LeaderVO;
import com.enation.app.javashop.core.member.service.LeaderManager;
import com.enation.app.javashop.core.trade.cart.model.dos.OrderPermission;
import com.enation.app.javashop.core.trade.order.model.dos.OrderItemsDO;
import com.enation.app.javashop.core.trade.order.model.dos.SortingSerial;
import com.enation.app.javashop.core.trade.order.model.dto.OrderDetailQueryParam;
import com.enation.app.javashop.core.trade.order.model.dto.OrderQueryParam;
import com.enation.app.javashop.core.trade.order.model.enums.*;
import com.enation.app.javashop.core.trade.order.model.vo.*;
import com.enation.app.javashop.core.trade.order.service.OrderOperateManager;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.security.model.Seller;
import com.enation.app.javashop.framework.util.BeanUtil;
import com.enation.app.javashop.framework.util.JsonUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 商家订单控制器
 *
 * @author Snow create in 2018/6/13
 * @version v2.0
 * @since v7.0.0
 */

@Api(description = "商家订单API")
@RestController
@RequestMapping("/seller/trade/orders")
@Validated
public class    OrderSellerController {

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Autowired
    private OrderOperateManager orderOperateManager;

    @Autowired
    private MemberHistoryReceiptClient memberHistoryReceiptClient;
    @Autowired
    private LeaderManager leaderManager;

    protected final Log logger = LogFactory.getLog(this.getClass());

    @ApiOperation(value = "查询会员订单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "channel", value = "渠道", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "keywords", value = "关键字", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "order_sn", value = "订单编号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "buyer_name", value = "买家姓名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "goods_name", value = "商品名称", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "start_time", value = "开始时间", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "结束时间", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "ship_name", value = "收货人名称", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "ship_mobile", value = "收货人手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "order_status", value = "订单状态", dataType = "String", paramType = "query",
                    allowableValues = "ALL,WAIT_PAY,WAIT_SHIP,WAIT_ROG,CANCELLED,COMPLETE,WAIT_COMMENT,REFUND,WAIT_REFUND",
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货,WAIT_REFUND:待退款," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中"),
            @ApiImplicitParam(name = "page_no", value = "页数", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "条数", dataType = "int", paramType = "query")
    })
    @GetMapping()
    public Page<OrderLineVO> list(@ApiIgnore String orderSn, @ApiIgnore String buyerName, @ApiIgnore String goodsName,
                                  @ApiIgnore Long startTime, @ApiIgnore Long endTime, @ApiIgnore String shipName, @ApiIgnore String shipMobile, @ApiIgnore String orderStatus,
                                  @ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize, @ApiIgnore String keywords, @ApiIgnore String channel) {

        Seller seller = UserContext.getSeller();

        OrderQueryParam param = new OrderQueryParam();
        if(orderStatus!=null && orderStatus.equals("WAIT_REFUND")){
            RefundQueryParamVO queryParam = new RefundQueryParamVO();
            queryParam.setRefundStatusArray(Arrays.asList(RefundStatusEnum.APPLY.value(),RefundStatusEnum.REFUNDFAIL.value(),RefundStatusEnum.STOCK_IN.value()));
            queryParam.setPageNo(pageNo);
            queryParam.setPageSize(pageSize);
            queryParam.setSellerId(seller.getSellerId());
            return this.orderQueryManager.queryRefundOrders(queryParam);
        }
        param.setOrderSn(orderSn);
        param.setBuyerName(buyerName);
        param.setGoodsName(goodsName);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        param.setTag(orderStatus);
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setSellerId(seller.getSellerId());
        param.setKeywords(keywords);
        param.setShipName(shipName);
        param.setShipMobile(shipMobile);
        if(StringUtils.isEmpty(channel)){
            param.setOrderType(OrderTypeEnum.normal.name());
        }
        return this.orderQueryManager.queryOrderAndNicknameList(param);
    }


    @ApiOperation(value = "查询单个订单明细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", required = true, dataType = "String", paramType = "path")
    })
    @GetMapping(value = "/{order_sn}")
    public OrderDetailVO get(@ApiIgnore @PathVariable("order_sn") String orderSn) {
        Seller seller = UserContext.getSeller();
        OrderDetailQueryParam queryParam = new OrderDetailQueryParam();
        queryParam.setSellerId(seller.getSellerId());
        OrderDetailVO detailVO = this.orderQueryManager.getModel(orderSn, queryParam);

        //在线支付的订单商家不允许确认收款
        if (PaymentTypeEnum.ONLINE.value().equals(detailVO.getPaymentType())) {
            detailVO.getOrderOperateAllowableVO().setAllowPay(false);
        }

        if (detailVO.getNeedReceipt().intValue() == 1) {
            detailVO.setReceiptHistory(memberHistoryReceiptClient.getReceiptHistory(orderSn));
        }

        return detailVO;
    }

    @ApiOperation(value = "根据核销码查询单个订单明细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "verification_code", value = "核销码", required = true, dataType = "String")
    })
    @GetMapping(value = "/verification_code")
    public OrderDetailVO getByVerCode(@RequestParam(value = "verification_code") String verificationCode) {
        Seller seller = UserContext.getSeller();
        OrderDetailVO detailVO = this.orderQueryManager.getOrderDetailByVerCode(verificationCode,seller.getSellerId());

        //在线支付的订单商家不允许确认收款
        if (PaymentTypeEnum.ONLINE.value().equals(detailVO.getPaymentType())) {
            detailVO.getOrderOperateAllowableVO().setAllowPay(false);
        }

        if (detailVO.getNeedReceipt().intValue() == 1) {
            detailVO.setReceiptHistory(memberHistoryReceiptClient.getReceiptHistory(detailVO.getSn()));
        }

        return detailVO;
    }

    @ApiOperation(value = "查询单个订单物流信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", required = true, dataType = "String", paramType = "path")
    })
    @GetMapping(value = "/ship_detail/{order_sn}")
    public List<OrderItemsVO> shipDetail(@ApiIgnore @PathVariable("order_sn") String orderSn) {

        // 1.验证订单和订单是否属于此店铺
        Seller seller = UserContext.getSeller();
        OrderDetailQueryParam queryParam = new OrderDetailQueryParam();
        queryParam.setSellerId(seller.getSellerId());
        OrderDetailDTO orderDetail = this.orderQueryManager.getModel(orderSn);
        if (ObjectUtils.isEmpty(orderDetail) || !seller.getSellerId().equals(orderDetail.getSellerId())) {
            throw new ServiceException(AftersaleErrorCode.E604.name(), AftersaleErrorCode.E604.describe());
        }

        // 2.获取订单对应的订单详情和skuList（skuList中有商品的售后状态，订单详情中没有）
        List<OrderItemsDO> orderItems = this.orderQueryManager.getOrderItems(orderSn);
        List<OrderSkuVO> skuList = JsonUtil.jsonToList(orderDetail.getItemsJson(),OrderSkuVO.class);
        if (CollectionUtils.isEmpty(skuList)) {
            throw new ServiceException(AftersaleErrorCode.E602.name(), AftersaleErrorCode.E602.describe());
        }
        List<OrderItemsVO> orderItemsVOList = new ArrayList<>();

        // 3.解决老订单的订单详情中没有物流信息，去查订单表中的物流信息
        List<OrderItemsDO> shipYesOrderItemList = this.orderQueryManager.getOrderItemsByShipStatus(orderSn, ShipStatusEnum.SHIP_YES.value());
        // 如果订单详情没有已发货的商品，但是订单中已经有快递单号和物流公司，并且物流公司ID>0(0是自提)，说明是老订单，已经在订单表中有物流信息
        if (CollectionUtils.isEmpty(shipYesOrderItemList)) {
            if ((!StringUtil.isEmpty(orderDetail.getShipNo())) && orderDetail.getLogiId() > 0) {
                // 将订单表的物流信息赋值到订单详情中
                for (OrderItemsDO orderItem : orderItems) {
                    orderItem.setShipStatus(orderDetail.getShipStatus());
                    orderItem.setLogiId(orderDetail.getLogiId());
                    orderItem.setLogiName(orderDetail.getLogiName());
                    orderItem.setShipNo(orderDetail.getShipNo());
                    orderItem.setShipTime(orderDetail.getShipTime());
                }
            }
        }

        // 4.将skuList中的商品的售后状态赋值给订单详情一起返回，商品已申请售后不允许分包裹发货
        for (OrderSkuVO orderSkuVO : skuList) {
            String serviceStatus = orderSkuVO.getServiceStatus();
            for (OrderItemsDO orderItemDO : orderItems) {
                if (orderItemDO.getProductId().equals(orderSkuVO.getSkuId())) {
                    OrderItemsVO orderItemsVO = new OrderItemsVO();
                    BeanUtil.copyProperties(orderItemDO, orderItemsVO);
                    orderItemsVO.setServiceStatus(serviceStatus);
                    orderItemsVOList.add(orderItemsVO);
                }
            }
        }
        return orderItemsVOList;
    }


    @ApiOperation(value = "查询订单状态的数量")
    @GetMapping(value = "/status-num")
    public OrderStatusNumVO getStatusNum() {
        Seller seller = UserContext.getSeller();
        return this.orderQueryManager.getOrderStatusNum(null, seller.getSellerId());
    }

    @ApiOperation(value = "订单按商品分包裹发货", notes = "商家对某订单执行按商品分包裹发货操作")
    @ResponseBody
    @PostMapping(value = "/split/delivery")
    public String splitPackageShip(@Valid @RequestBody DeliveryVO deliveryVO) {
        try {
            deliveryVO.setShipType(ShipTypeEnum.GLOBAL.name());
            if(StringUtils.isEmpty(deliveryVO.getDeliveryNo())){
                deliveryVO.setDeliveryNo("00000");
                deliveryVO.setLogiId(0);
                deliveryVO.setLogiName("自提配送");
                deliveryVO.setShipType(ShipTypeEnum.SELF.name());
            }
            Seller seller = UserContext.getSeller();
            deliveryVO.setOperator("店铺:" + seller.getSellerName());
            orderOperateManager.partShip(deliveryVO, OrderPermission.seller);
            return "SUCCESS";
        } catch (Exception e) {
            logger.error("分包裹发货失败", e);
            return "FAIL";
        }
    }

    @ApiOperation(value = "订单发货", notes = "商家对某订单执行发货操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单sn", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "ship_no", value = "发货单号", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "logi_id", value = "物流公司id", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "logi_name", value = "物流公司名称", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "ship_type", value = "物流配送方式", required = false, dataType = "String", paramType = "query"),
    })
    @ResponseBody
    @PostMapping(value = "/{order_sn}/delivery")
    public String ship(@ApiIgnore @NotNull(message = "必须指定订单编号") @PathVariable(name = "order_sn") String orderSn,
                       @ApiIgnore String shipNo, @ApiIgnore Integer logiId, @ApiIgnore String logiName) {
        String shipType=ShipTypeEnum.GLOBAL.name();
        if(StringUtils.isEmpty(shipNo)){
            shipNo="00000";
            logiId=0;
            logiName="自提配送";
            shipType=ShipTypeEnum.SELF.name();
        }

        Seller seller = UserContext.getSeller();
        DeliveryVO delivery = new DeliveryVO();
        delivery.setDeliveryNo(shipNo);
        delivery.setOrderSn(orderSn);
        delivery.setLogiId(logiId);
        delivery.setLogiName(logiName);
        delivery.setOperator("店铺:" + seller.getSellerName());
        if(shipType!=null){
            delivery.setShipType(shipType);
        }
        orderOperateManager.ship(delivery, OrderPermission.seller);

        return "";
    }


    @ApiOperation(value = "商家修改收货人地址", notes = "商家发货前，可以修改收货人地址信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单sn", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "ship_name", value = "收货人姓名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "remark", value = "订单备注", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "ship_addr", value = "收货地址", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "ship_mobile", value = "收货人手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "ship_tel", value = "收货人电话", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "receive_time", value = "送货时间", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "region", value = "地区id", dataType = "int", paramType = "query"),
    })
    @PutMapping(value = "/{order_sn}/address")
    public OrderConsigneeVO updateOrderConsignee(@ApiIgnore @PathVariable(name = "order_sn") String orderSn,
                                                 @ApiIgnore String shipName, @ApiIgnore String remark,
                                                 @ApiIgnore String shipAddr, @ApiIgnore String shipMobile,
                                                 @ApiIgnore String shipTel, @ApiIgnore String receiveTime,
                                                 @RegionFormat Region region) {

        OrderConsigneeVO orderConsignee = new OrderConsigneeVO();
        orderConsignee.setOrderSn(orderSn);
        orderConsignee.setShipName(shipName);
        orderConsignee.setRemark(remark);
        orderConsignee.setShipAddr(shipAddr);
        orderConsignee.setShipMobile(shipMobile);
        orderConsignee.setShipTel(shipTel);
        orderConsignee.setReceiveTime(receiveTime);
        orderConsignee.setRegion(region);
        orderConsignee = this.orderOperateManager.updateOrderConsignee(orderConsignee);
        return orderConsignee;
    }


    @ApiOperation(value = "商家修改订单价格", notes = "买家付款前可以修改订单价格")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单sn", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "order_price", value = "订单价格", required = true, dataType = "Double", paramType = "query"),
    })
    @PutMapping(value = "/{order_sn}/price")
    public String updateOrderPrice(@ApiIgnore @PathVariable(name = "order_sn") String orderSn,
                                   @ApiIgnore @NotNull(message = "修改后价格不能为空") Double orderPrice) {
        this.orderOperateManager.updateOrderPrice(orderSn, orderPrice);
        return "";
    }

    @ApiOperation(value = "APP商家修改订单价格", notes = "买家付款前可以修改订单价格")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单sn", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "goods_price", value = "商品金额", required = true, dataType = "Double", paramType = "query"),
            @ApiImplicitParam(name = "ship_price", value = "运费金额", required = true, dataType = "Double", paramType = "query"),
    })
    @PutMapping(value = "/{order_sn}/app/price")
    public String updateAPPOrderPrice(@ApiIgnore @PathVariable(name = "order_sn") String orderSn,
                                   @ApiIgnore @NotNull(message = "修改后商品价格不能为空") Double goodsPrice, @ApiIgnore @NotNull(message = "修改后运费不能为空") Double shipPrice) {
        this.orderOperateManager.updateOrderPrice(orderSn, goodsPrice,shipPrice);
        return "OK";
    }

    @ApiOperation(value = "iosAPP商家修改订单价格", notes = "买家付款前可以修改订单价格")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单sn", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "goods_price", value = "商品金额", required = true, dataType = "Double", paramType = "query"),
            @ApiImplicitParam(name = "ship_price", value = "运费金额", required = true, dataType = "Double", paramType = "query"),
    })
    @PutMapping(value = "/{order_sn}/iosapp/price")
    public Result updateOrderPrice(@ApiIgnore @PathVariable(name = "order_sn") String orderSn,
                                   @ApiIgnore @NotNull(message = "修改后商品价格不能为空") Double goodsPrice, @ApiIgnore @NotNull(message = "修改后运费不能为空") Double shipPrice) {
        this.orderOperateManager.updateOrderPrice(orderSn, goodsPrice,shipPrice);
        return Result.successMessage();
    }


    @ApiOperation(value = "确认收款")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "pay_price", value = "付款金额", dataType = "double", paramType = "query")
    })
    @PostMapping(value = "/{order_sn}/pay")
    public String payOrder(@ApiIgnore @PathVariable("order_sn") String orderSn, @ApiIgnore Double payPrice) {
        this.orderOperateManager.payOrder(orderSn, payPrice, "", OrderPermission.seller);
        return "";
    }


    @ApiOperation(value = "订单流程图数据", notes = "订单流程图数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单sn", required = true, dataType = "String", paramType = "path"),
    })
    @GetMapping(value = "/{order_sn}/flow")
    public List<OrderFlowNode> getOrderStatusFlow(@ApiIgnore @PathVariable(name = "order_sn") String orderSn) {
        List<OrderFlowNode> orderFlowList = this.orderQueryManager.getOrderFlow(orderSn);
        return orderFlowList;
    }

    /**
     * 删除订单
     * @param orderSn
     * @return
     */
    @ApiOperation(value = "删除取消订单", notes = "删除取消订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单sn", required = true, dataType = "String", paramType = "path"),
    })
    @DeleteMapping(value = "/{order_sn}")
    public String deleteOrder(@ApiIgnore @PathVariable(name = "order_sn") String orderSn) {
        this.orderOperateManager.deleteOrder(orderSn);
        return "";
    }

    /**
     * 订单批量打印
     * @param orderSnList
     * @return
     */
    @ApiOperation(value = "订单批量打印", notes = "订单批量打印")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn_list", value = "订单sn数组", required = true, dataType = "String", paramType = "path"),
    })
    @GetMapping(value = "/getByOrderSnList/{order_sn_list}")
    public List getByOrderSnList(@ApiIgnore @PathVariable("order_sn_list") String orderSnList) {
        String[] orderSnArray = orderSnList.split(",");
        Seller seller = UserContext.getSeller();
        OrderDetailQueryParam queryParam = new OrderDetailQueryParam();
        queryParam.setSellerId(seller.getSellerId());

        List<OrderDetailVO> orderDetailVOS=new ArrayList<>();
        for (String orderSn : orderSnArray) {
            OrderDetailVO detailVO = this.orderQueryManager.getModel(orderSn, queryParam);
            // 查询出货订单序列号
            SortingSerial sortingSerial = this.orderQueryManager.getSortingSerial(orderSn);
            if(sortingSerial != null){
                detailVO.setOrderSeq( sortingSerial.getBatNo()+"_"+sortingSerial.getSn());
            }
            // 更新打印次数
            Integer printTimes = detailVO.getPrintTimes() + 1;
            detailVO.setPrintTimes(printTimes);
            this.orderQueryManager.updatePrintTimes(orderSn,printTimes);

            orderDetailVOS.add(detailVO);
        }

        return orderDetailVOS;
    }

    @ApiOperation(value = "商家修改收货地址", notes = "商家发货前，可以修改收货人地址信息")
    @PutMapping(value = "/upd_order_address")
    public SuccessMessage updOrderAddress(OrderConsigneeVO orderConsigneeVO, @RegionFormat @RequestParam(value = "index_region", required = false) Region region) {
        try {
            orderConsigneeVO.setRegion(region);
            orderOperateManager.updateOrderAddres(orderConsigneeVO);
            return new SuccessMessage("修改成功!");
        } catch (Exception e) {
            logger.error("修改收货地址:", e);
            throw new DistributionException("-1", "修改收货地址:" + e.getMessage());
        }
    }

    @GetMapping(value = "/query_leader_by_site_name")
    @ApiOperation("查询自提点")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "site_name", value = "自提点名称", required = true, paramType = "query", dataType = "String")})
    public List<LeaderVO> queryLeaderBySiteName(@ApiIgnore String siteName) {
        return leaderManager.queryLeaderBySiteName(siteName);
    }

    @GetMapping(value = "/virtualOrderShip/{verification_code}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "verification_code", value = "核销码", required = true, dataType = "String", paramType = "path"),
    })
    @ApiOperation("虚拟商品订单核销")
    public Map<String, String> virtualOrderShip(@PathVariable("verification_code") String verificationCode){
        return orderOperateManager.virtualOrderShip(verificationCode);
    }


    @GetMapping(value = "/confirmRog")
    @ApiOperation("确认收货")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "单号", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "user_name", value = "用户名", required = true, paramType = "query", dataType = "String")})
    public void confirmRog(@ApiIgnore String orderSn,@ApiIgnore String userName){
        RogVO rogVO = new RogVO();
        rogVO.setOrderSn(orderSn);
        rogVO.setOperator(userName);
        // 确认收货
        orderOperateManager.executeOperate(orderSn,OrderPermission.admin, OrderOperateEnum.ROG, rogVO);
    }

}
