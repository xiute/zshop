package com.enation.app.javashop.seller.api.material;

import com.dag.eagleshop.core.material.model.dos.MaterialDO;
import com.dag.eagleshop.core.material.model.dos.MaterialGroupDO;
import com.dag.eagleshop.core.material.service.MaterialGroupManager;
import com.dag.eagleshop.core.material.service.MaterialManager;
import com.enation.app.javashop.core.goods.model.vo.GoodsParamsGroupVO;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 素材管理控制器
 *
 * @author sunjian
 */
@RestController
@RequestMapping("/seller/material")
@Api(description = "素材相关API")
public class MaterialController {

    @Autowired
    private MaterialGroupManager materialGroupManager;
    @Autowired
    private MaterialManager materialManager;


    @ApiOperation(value = "添加素材分组", response = MaterialGroupDO.class)
    @PostMapping(value = "addMaterialGroup")
    public MaterialGroupDO addMaterialGroup(@RequestBody MaterialGroupDO materialGroupDO) {
        return this.materialGroupManager.add(materialGroupDO);
    }


    @ApiOperation(value = "修改素材分组", response = MaterialGroupDO.class)
    @PostMapping(value = "editMaterialGroup")
    public MaterialGroupDO edit(@RequestBody MaterialGroupDO materialGroupDO) {
        return this.materialGroupManager.edit(materialGroupDO, materialGroupDO.getMaterialGroupId());
    }


    @ApiOperation(value = "删除素材分组", response = Boolean.class)
    @PostMapping(value = "deleteMaterialGroup")
    public boolean deleteMaterialGroup(@RequestBody Integer id) {
        this.materialGroupManager.delete(new Integer[]{ id });
        return true;
    }


    @ApiOperation(value = "添加素材", response = MaterialDO.class)
    @PostMapping(value = "addMaterialDO")
    public MaterialDO addMaterialDO(@RequestBody MaterialDO materialDO) {
        return this.materialManager.add(materialDO);
    }


    @ApiOperation(value = "修改素材", response = MaterialDO.class)
    @PostMapping(value = "editMaterialDO")
    public MaterialDO editMaterialDO(@RequestBody MaterialDO materialDO) {
        return this.materialManager.edit(materialDO, materialDO.getMaterialId());
    }


    @ApiOperation(value = "批量删除素材", response = Boolean.class)
    @PostMapping(value = "deleteMaterialDO")
    public boolean deleteMaterialDO(@RequestBody Integer[] ids) {
        this.materialManager.delete(ids);
        return true;
    }


    @ApiOperation(value = "批量修改素材的分组", response = Boolean.class)
    @PostMapping(value = "batchEditGroup")
    public boolean batchEditGroup(@RequestBody Integer[] ids, Integer materialGroupId) {
        this.materialManager.batchEditGroup(ids, materialGroupId);
        return true;
    }


    @ApiOperation(value = "根据条件查询素材列表", response = MaterialDO.class)
    @PostMapping(value = "list")
    public Page list(@RequestParam(value = "pageNo") Integer pageNo,
                     @RequestParam(value = "pageSize") Integer pageSize,
                     @RequestParam(value = "materialName") String materialName) {
        Seller seller = UserContext.getSeller();
        return this.materialManager.list(pageNo, pageSize, seller.getSellerId(), materialName);
    }


    @ApiOperation(value = "根据类型查询分组列表和默认分组素材")
    @PostMapping(value = "listGroupAndMaterial")
    public Page listGroupAndMaterial(@RequestParam(value = "pageNo") Integer pageNo,
                                     @RequestParam(value = "pageSize") Integer pageSize,
                                     @RequestParam(value = "materialGroupName") String materialGroupName) {
        Seller seller = UserContext.getSeller();
        return this.materialGroupManager.list(pageNo, pageSize,
                seller.getSellerId(), materialGroupName);
    }


}
