package com.dag.account.api.dto.account;

import com.dag.account.constants.AccountTypeEnum;
import com.dag.common.entity.SaaSDTO;
import com.dag.common.exception.BusinessRuntimeException;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 开通账户DTO
 */
@Data
public class OpenAccountDTO extends SaaSDTO {

    /**
     * 会员id
     */
    private String memberId;
    /**
     * 账户类型 目前就一个主账户 AccountTypeEnum
     */
    @NotNull(message = "账户类型不能为空")
    private Integer accountType;
    /**
     * 账户类型名称 目前就一个主账户 AccountTypeEnum
     */
    @NotNull(message = "账户类型名称不能为空")
    private String accountTypeName;
    /**
     * 账户性质
     */
    @NotNull(message = "账户性质不能为空")
    private Integer nature;
    /**
     * 支付密码
     */
    private String payPwd;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;


    @Override
    public void validate() {
        super.validate();

        if(accountType == AccountTypeEnum.PLATFORM.getIndex()
                || accountType == AccountTypeEnum.PROFIT.getIndex()){
            throw new BusinessRuntimeException("不能创建此账户");
        }
    }
}
