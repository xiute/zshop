package com.dag.account.api.dto.withdraw;

import com.dag.common.entity.AddDTO;
import lombok.Data;

/**
 * @author chien
 * @since 2020-07-22
 */
@Data
public class AddWithdrawAccountDTO extends AddDTO {

    /**
     * 会员id
     */
    private String memberId;
    /**
     * 账号类型 1支付宝 2微信
     */
    private Integer type;
    /**
     * 类型名称
     */
    private String typeName;
    /**
     * 账户名
     */
    private String accountName;
    /**
     * 账号
     */
    private String accountNo;
    /**
     * 是否对公 0否 1是
     */
    private Integer ofPublic;

}
