package com.dag.account.api.dto.settlebill;

import com.dag.common.entity.AddDTO;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CreateSettleBillDTO extends AddDTO{

    /**
     * 结算单名称
     */
    private String settleBillName;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 账单状态 1待确认 2待结算 3已结算
     */
    private Integer status;
    /**
     * 结算单明细项
     */
    private List<AddSettleBillItemDTO> settleBillItemList;

}
