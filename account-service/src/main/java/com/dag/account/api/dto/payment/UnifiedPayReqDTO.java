package com.dag.account.api.dto.payment;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class UnifiedPayReqDTO extends BaseDTO {

    /**
     * 交易凭证单号
     */
    @NotNull(message = "交易凭证单号不能为空")
    private String tradeVoucherNo;
    /**
     * 付款人id
     */
    @NotNull(message = "付款人id不能为空")
    private String draweeMemberId;
    /**
     * 付款账户类型
     */
    @NotNull(message = "付款账户类型")
    private Integer accountType;
    /**
     * 付款金额
     */
    @NotNull(message = "付款金额不能为空")
    private BigDecimal amount;
    /**
     * 交易主题
     */
    @NotNull(message = "交易主题不能为空")
    private String tradeSubject;
    /**
     * 交易内容
     */
    @NotNull(message = "交易内容不能为空")
    private String tradeContent;
    /**
     * 操作人id
     */
    @NotNull(message = "操作人id不能为空")
    private String operatorId;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;

}
