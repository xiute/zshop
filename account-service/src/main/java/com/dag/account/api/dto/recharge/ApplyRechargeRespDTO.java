package com.dag.account.api.dto.recharge;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 申请充值DTO
 */
@Data
public class ApplyRechargeRespDTO extends BaseDTO {

    /**
     * 申请结果
     */
    private boolean success;
    /**
     * 描述
     */
    private String message;
    /**
     * 充值单据号
     */
    @NotNull(message = "充值单据号不能为空")
    private String rechargeNo;
    /**
     * 账户id
     */
    @NotNull(message = "账户id不能为空")
    private String accountId;
    /**
     * 充值金额
     */
    @NotNull(message = "充值金额不能为空")
    private BigDecimal amount;


    /**
     * 构建成功结果
     */
    public static ApplyRechargeRespDTO bySuccess(String accountId, BigDecimal amount, String message){
        ApplyRechargeRespDTO rechargeRespDTO = by(accountId, amount, message);
        rechargeRespDTO.setSuccess(true);
        return rechargeRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static ApplyRechargeRespDTO byFail(String accountId, BigDecimal amount, String message){
        ApplyRechargeRespDTO rechargeRespDTO = by(accountId, amount, message);
        rechargeRespDTO.setSuccess(false);
        return rechargeRespDTO;
    }


    private static ApplyRechargeRespDTO by(String accountId, BigDecimal amount, String message){
        ApplyRechargeRespDTO rechargeRespDTO = new ApplyRechargeRespDTO();
        rechargeRespDTO.setMessage(message);
        rechargeRespDTO.setAccountId(accountId);
        rechargeRespDTO.setAmount(amount);
        return rechargeRespDTO;
    }

}
