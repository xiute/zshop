package com.dag.account.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dag.account.api.dto.bankcard.BindBankCardDTO;
import com.dag.account.api.dto.bankcard.QueryBankCardDTO;
import com.dag.account.dao.entity.BankCard;
import com.dag.account.service.IBankCardService;
import com.dag.common.entity.IdDTO;
import com.dag.common.entity.PageDTO;
import com.dag.common.entity.ViewPage;
import com.dag.common.exception.BusinessRuntimeException;
import com.dag.common.utils.ModelMapperUtils;
import com.dag.lock.redis.annotation.LockAction;
import com.dag.mybatis.utils.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 银行卡操作类
 * @author 孙建
 */
@Slf4j
@RestController
@RequestMapping("/account/bankcard")
public class BankCardApi {

    @Autowired
    private IBankCardService bankCardService;


    @LockAction(key = "bind", value = "#bindBankCardDTO.cardNo")
    @ApiOperation(value = "绑定银行卡", notes = "绑定银行卡")
    @PostMapping(value = "bind")
    public BankCard bind(@RequestBody BindBankCardDTO bindBankCardDTO) {
        bindBankCardDTO.validate();
        return bankCardService.bind(bindBankCardDTO);
    }


    @ApiOperation(value = "解绑银行卡", notes = "解绑银行卡")
    @PostMapping(value = "unbind")
    public void unbind(@RequestBody IdDTO<String> idDTO) {
        idDTO.validate();
        bankCardService.unbind(idDTO.getId());
    }


    @ApiOperation(value = "根据id查询银行卡", notes = "根据id查询银行卡")
    @PostMapping(value = "queryById")
    public BankCard queryById(@RequestBody IdDTO<String> idDTO){
        idDTO.validate();
        return bankCardService.getById(idDTO.getId());
    }


    @ApiOperation(value = "根据卡号查询银行卡", notes = "根据卡号查询银行卡")
    @PostMapping(value = "queryByCardNo")
    public BankCard queryByCardNo(@RequestParam(value = "cardNo") String cardNo) {
        return bankCardService.queryByCardNo(cardNo);
    }


    @ApiOperation(value = "根据卡号查询银行卡", notes = "根据卡号查询银行卡")
    @PostMapping(value = "queryByCardNos")
    public List<BankCard> queryByCardNo(@RequestBody List<String> cardNoList) {
        return bankCardService.queryByCardNoList(cardNoList);
    }


    @ApiOperation(value = "根据userId查询银行卡数量", notes = "根据userId查询银行卡数量")
    @PostMapping(value = "countByMemberId")
    public Integer countByMemberId(@RequestParam(value = "memberId") String memberId){
        BankCard bankCard = new BankCard();
        bankCard.setMemberId(memberId);
        return bankCardService.count(new QueryWrapper<>(bankCard));
    }


    @ApiOperation(value = "根据accountId查询银行卡数量", notes = "根据accountId查询银行卡数量")
    @PostMapping(value = "countByAccountId")
    public Integer countByAccountId(@RequestParam(value = "accountId") String accountId){
        BankCard bankCard = new BankCard();
        bankCard.setAccountId(accountId);
        return bankCardService.count(new QueryWrapper<>(bankCard));
    }


    @ApiOperation(value = "根据memberId查询银行卡列表", notes = "根据memberId查询银行卡列表")
    @PostMapping(value = "queryByMemberId")
    public List<BankCard> queryByMemberId(@RequestParam(value = "memberId") String memberId){
        BankCard bankCard = new BankCard();
        bankCard.setMemberId(memberId);
        return bankCardService.list(new QueryWrapper<>(bankCard));
    }


    @ApiOperation(value = "根据accountId查询银行卡列表", notes = "根据memberId查询银行卡列表")
    @PostMapping(value = "queryByAccountId")
    public List<BankCard> queryByAccountId(@RequestParam(value = "accountId") String accountId){
        BankCard bankCard = new BankCard();
        bankCard.setAccountId(accountId);
        return bankCardService.list(new QueryWrapper<>(bankCard));
    }


    @ApiOperation(value = "根据条件查询银行卡列表", notes = "根据条件查询银行卡列表")
    @PostMapping(value = "queryList")
    public ViewPage<BankCard> queryList(@RequestBody PageDTO<QueryBankCardDTO> pageDTO){
        // 校验
        pageDTO.validate();

        // 封装参数
        BankCard bankCard = ModelMapperUtils.map(pageDTO.getBody(), BankCard.class);

        // 根据条件查询
        PageHelper.startPage(pageDTO.getPageNum(), pageDTO.getPageSize());
        PageInfo<BankCard> bankCardPageInfo = new PageInfo<>(bankCardService.list(new QueryWrapper<>(bankCard)));
        return PageUtils.convert(bankCardPageInfo);
    }


    @ApiOperation(value = "根据id更新银行卡", notes = "根据id更新银行卡")
    @PostMapping(value = "updateById")
    public boolean updateById(@RequestBody BankCard bankCard){
        if(StringUtils.isBlank(bankCard.getId())){
            throw new BusinessRuntimeException("id不能为空");
        }
        return bankCardService.updateById(bankCard);
    }

}
