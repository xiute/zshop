package com.dag.account.api;

import com.dag.account.api.dto.refund.RefundRecordDTO;
import com.dag.account.api.dto.refund.UnifiedRefundReqDTO;
import com.dag.account.api.dto.refund.UnifiedRefundRespDTO;
import com.dag.account.service.IRefundService;
import com.dag.lock.redis.annotation.LockAction;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 退款api
 */
@Slf4j
@RestController
@RequestMapping("/account/refund")
public class RefundApi {

    @Autowired
    private IRefundService refundService;


    @LockAction(key = "unifiedRefund", value = "#refundRequestDTO.tradeVoucherNo")
    @ApiOperation(value = "账户统一退款", notes = "账户统一退款")
    @PostMapping(value = "unifiedRefund")
    public UnifiedRefundRespDTO unifiedRefund(@RequestBody UnifiedRefundReqDTO refundRequestDTO) {
        refundRequestDTO.validate();
        return refundService.unifiedRefund(refundRequestDTO);
    }


    @LockAction(key = "refundRecord", value = "#refundRecordDTO.tradeVoucherNo")
    @ApiOperation(value = "其他支付渠道生成退款记录", notes = "其他支付渠道生成退款记录")
    @PostMapping(value = "refundRecord")
    public boolean refundRecord(@RequestBody RefundRecordDTO refundRecordDTO) {
        refundRecordDTO.validate();
        refundService.refundRecord(refundRecordDTO);
        return true;
    }

}
