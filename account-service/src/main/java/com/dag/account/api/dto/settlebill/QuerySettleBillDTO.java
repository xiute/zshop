package com.dag.account.api.dto.settlebill;

import com.dag.common.entity.QueryDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @since 2020-07-26
 */
@Data
public class QuerySettleBillDTO extends QueryDTO {

    /**
     * 结算单号
     */
    private String settleBillNo;
    /**
     * 结算单名称
     */
    private String settleBillName;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 单据数量
     */
    private Integer billNum;
    /**
     * 账单金额
     */
    private BigDecimal amount;
    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 账单状态 1待确认 2已确认 3已完成
     */
    private Integer status;


}
