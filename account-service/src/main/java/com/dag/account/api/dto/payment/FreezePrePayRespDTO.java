package com.dag.account.api.dto.payment;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class FreezePrePayRespDTO extends BaseDTO {

    /**
     * 支付流水号
     */
    private String serialNo;
    /**
     * 交易凭证单号
     */
    private String tradeVoucherNo;
    /**
     * 支付金额
     */
    private BigDecimal amount;
    /**
     * 转账结果
     */
    private boolean success;
    /**
     * 转账描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static FreezePrePayRespDTO bySuccess(String serialNo, String tradeVoucherNo, String message){
        FreezePrePayRespDTO freezePrePayRespDTO = by(serialNo, tradeVoucherNo , message);
        freezePrePayRespDTO.setSuccess(true);
        return freezePrePayRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static FreezePrePayRespDTO byFail(String serialNo, String tradeVoucherNo, String message){
        FreezePrePayRespDTO freezePrePayRespDTO = by(serialNo, tradeVoucherNo, message);
        freezePrePayRespDTO.setSuccess(false);
        return freezePrePayRespDTO;
    }


    private static FreezePrePayRespDTO by(String serialNo, String tradeVoucherNo, String message){
        FreezePrePayRespDTO freezePrePayRespDTO = new FreezePrePayRespDTO();
        freezePrePayRespDTO.setSerialNo(serialNo);
        freezePrePayRespDTO.setTradeVoucherNo(tradeVoucherNo);
        freezePrePayRespDTO.setMessage(message);
        return freezePrePayRespDTO;
    }

}
