package com.dag.account.api.dto.account;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

/**
 * 业务转账结果DTO
 */
@Data
public class BusinessTransferRespDTO extends BaseDTO{

    /**
     * 转账结果
     */
    private boolean success;
    /**
     * 转账描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static BusinessTransferRespDTO bySuccess(String message){
        BusinessTransferRespDTO transferRespDTO = by(message);
        transferRespDTO.setSuccess(true);
        return transferRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static BusinessTransferRespDTO byFail(String message){
        BusinessTransferRespDTO transferRespDTO = by(message);
        transferRespDTO.setSuccess(false);
        return transferRespDTO;
    }


    private static BusinessTransferRespDTO by(String message){
        BusinessTransferRespDTO transferRespDTO = new BusinessTransferRespDTO();
        transferRespDTO.setMessage(message);
        return transferRespDTO;
    }

}
