package com.dag.account.api;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dag.account.api.dto.settlebill.CreateSettleBillDTO;
import com.dag.account.api.dto.settlebill.QuerySettleBillDTO;
import com.dag.account.api.dto.settlebill.SettleBillDTO;
import com.dag.account.dao.entity.SettleBill;
import com.dag.account.dao.entity.SettleBillItem;
import com.dag.account.service.ISettleBillItemService;
import com.dag.account.service.ISettleBillService;
import com.dag.common.entity.PageDTO;
import com.dag.common.entity.ViewPage;
import com.dag.common.utils.ModelMapperUtils;
import com.dag.mybatis.utils.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Administrator
 * @since 2020-07-26
 */
@Slf4j
@RestController
@RequestMapping("/account/settleBill")
public class SettleBillApi {

    @Autowired
    private ISettleBillService settleBillService;
    @Autowired
    private ISettleBillItemService settleBillItemService;


    @ApiOperation(value = "查询结算账单列表", notes = "查询结算账单列表")
    @PostMapping(value = "/querySettleBillList")
    public ViewPage<SettleBill> querySettleBillList(@RequestBody PageDTO<QuerySettleBillDTO> pageDTO){
        // 校验
        pageDTO.validate();

        // 封装参数
        SettleBill settleBill = ModelMapperUtils.map(pageDTO.getBody(), SettleBill.class);

        // 根据条件查询
        PageHelper.startPage(pageDTO.getPageNum(), pageDTO.getPageSize());
        PageInfo<SettleBill> settleBillPageInfo = new PageInfo<>(settleBillService.list(new QueryWrapper<>(settleBill)));
        return PageUtils.convert(settleBillPageInfo);
    }


    @ApiOperation(value = "查询结算账单详情", notes = "查询结算账单详情")
    @PostMapping(value = "/querySettleBillById")
    public SettleBillDTO querySettleBillById(@RequestParam(name = "id") String id){
        SettleBill settleBill = settleBillService.getById(id);
        SettleBillDTO settleBillDTO = ModelMapperUtils.map(settleBill, SettleBillDTO.class);
        List<SettleBillItem> settleBillItemList =  settleBillItemService.listBySettleBillId(id);
        settleBillDTO.setSettleBillItemList(settleBillItemList);
        return settleBillDTO;
    }


    @ApiOperation(value = "创建结算账单和账单明细", notes = "创建结算账单和账单明细")
    @PostMapping(value = "/createSettleBill")
    public SettleBill createSettleBill(@RequestBody CreateSettleBillDTO createSettleBillDTO){
        createSettleBillDTO.validate();
        return settleBillService.createSettleBill(createSettleBillDTO);
    }


    @ApiOperation(value = "结算账单下一步操作", notes = "结算账单下一步操作", hidden = true)
    @PostMapping(value = "/next")
    public SettleBill next(@RequestParam(name = "id") String id){
        return settleBillService.next(id);
    }


    @ApiOperation(value = "查询最新的一条账单", notes = "查询最新的一条账单")
    @PostMapping(value = "/queryLastSettleBill")
    public SettleBill queryLastSettleBill(@RequestParam(name = "memberId") String memberId){
        return settleBillService.queryLastSettleBill(memberId);
    }

}
