package com.dag.account.api;

import com.dag.account.constants.AccountTypeEnum;
import com.dag.account.dao.entity.Account;
import com.dag.account.dao.entity.Member;
import com.dag.account.service.IPlatformService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 平台操作
 * @author 孙建
 */
@Slf4j
@RestController
@RequestMapping("/platform")
public class PlatformApi {

    @Autowired
    private IPlatformService platformService;


    @ApiOperation(value = "查询平台会员", notes = "查询平台会员")
    @RequestMapping(value = "queryPlatformMember", method = { RequestMethod.GET, RequestMethod.POST })
    public Member queryPlatformMember() {
        return platformService.queryPlatformMember();
    }


    @ApiOperation(value = "查询平台账户", notes = "查询交易明细")
    @RequestMapping(value = "queryPlatformAccount", method = { RequestMethod.GET, RequestMethod.POST })
    public Account queryPlatformAccount(@RequestParam(value = "accountType") Integer accountType) {
        String accountTypeName = AccountTypeEnum.getTextByIndex(accountType);
        if(StringUtils.isEmpty(accountTypeName)){
            // 不是平台账户不能调用此接口
            return null;
        }
        return platformService.queryPlatformAccount(accountType);
    }


}
