package com.dag.account.api.dto.account;

import com.dag.common.entity.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 转账明细DTO
 */
@Data
public class TransferDetailDTO extends SaaSDTO{

    /**
     * 付款人id
     */
    @NotNull(message = "付款人id不能为空")
    private String draweeMemberId;
    /**
     * 收款人id
     */
    @NotNull(message = "收款人id不能为空")
    private String payeeMemberId;
    /**
     * 账户类型
     */
    @NotNull(message = "账户类型不能为空")
    private Integer accountType;
    /**
     * 转账金额
     */
    @NotNull(message = "转账金额不能为空")
    private BigDecimal amount;
    /**
     * 交易主题
     */
    @NotNull(message = "交易主题不能为空")
    private String tradeSubject;
    /**
     * 交易内容
     */
    @NotNull(message = "交易内容不能为空")
    private String tradeContent;
    /**
     * 转账方式 实时和非实时 TransferTypeEnum
     */
    @NotNull(message = "转账类型不能为空")
    private Integer transferType;

}
