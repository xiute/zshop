package com.dag.account.api;


import com.dag.account.api.dto.recharge.ApplyRechargeReqDTO;
import com.dag.account.api.dto.recharge.ApplyRechargeRespDTO;
import com.dag.account.api.dto.recharge.PayCallbackParamDTO;
import com.dag.account.service.IRechargeBillService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 充值api
 * @author chien
 * @since 2020-07-23
 */
@Slf4j
@RestController
@RequestMapping("/account/recharge")
public class RechargeBillApi {

    @Autowired
    private IRechargeBillService rechargeBillService;


    @ApiOperation(value = "申请充值", notes = "申请充值")
    @PostMapping(value = "applyRecharge")
    // @LockAction(key = "applyRecharge", value = "#applyRechargeDTO.accountId")
    public ApplyRechargeRespDTO applyRecharge(@RequestBody ApplyRechargeReqDTO applyRechargeDTO){
        applyRechargeDTO.validate();
        return rechargeBillService.applyRecharge(applyRechargeDTO);
    }


    // @LockAction(key = "rechargeCallback", value = "#payCallbackParamDTO.outTradeNo")
    @ApiOperation(value = "账户充值支付回调", notes = "账户充值支付回调")
    @PostMapping(value = "rechargeCallback")
    public boolean rechargeCallback(@RequestBody PayCallbackParamDTO payCallbackParamDTO) {
        payCallbackParamDTO.validate();
        return rechargeBillService.rechargeCallback(payCallbackParamDTO);
    }

}
