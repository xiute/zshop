package com.dag.account.api;

import com.dag.account.api.dto.account.*;
import com.dag.account.dao.entity.Account;
import com.dag.account.dao.entity.TradeRecord;
import com.dag.account.service.IAccountService;
import com.dag.account.service.ITradeRecordService;
import com.dag.account.service.ITransferService;
import com.dag.common.entity.PageDTO;
import com.dag.common.entity.ViewPage;
import com.dag.lock.redis.annotation.LockAction;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 账户操作类
 * @author 孙建
 */
@Slf4j
@RestController
@RequestMapping("/account")
public class AccountApi {

    @Autowired
    private IAccountService accountService;
    @Autowired
    private ITransferService transferService;
    @Autowired
    private ITradeRecordService tradeRecordService;


    @ApiOperation(value = "根据id查询账户", notes = "根据id查询账户")
    @RequestMapping(value = "queryAccountById", method = { RequestMethod.GET, RequestMethod.POST })
    public Account queryAccountById(@RequestParam(value = "id") String id) {
        return accountService.getById(id);
    }


    @ApiOperation(value = "根据会员和账户类型查询账户", notes = "根据会员和账户类型查询账户")
    @RequestMapping(value = "queryByMemberIdAndAccountType", method = { RequestMethod.GET, RequestMethod.POST })
    public Account queryByMemberIdAndAccountType(@RequestParam(value = "memberId") String memberId,
                                                        @RequestParam(value = "accountType") Integer accountType) {
        return accountService.queryByMemberIdAndAccountType(memberId, accountType);
    }


    @ApiOperation(value = "开通账户", notes = "开通账户")
    @PostMapping(value = "openAccount")
    public Account openAccount(@RequestBody OpenAccountDTO openAccountDTO) {
        openAccountDTO.validate();
        return accountService.openAccount(openAccountDTO);
    }


    @ApiOperation(value = "账户汇总查询", notes = "账户汇总查询")
    @RequestMapping(value = "summary", method = { RequestMethod.GET, RequestMethod.POST })
    public CountAccountDTO summary(@RequestParam(value = "memberId") String memberId) {
        return accountService.summary(memberId);
    }


    @LockAction(key = "transfer", value = "#transferDTO.tradeVoucherNo")
    @ApiOperation(value = "账户转账", notes = "账户转账")
    @PostMapping(value = "transfer")
    public TransferRespDTO transfer(@RequestBody TransferReqDTO transferDTO) {
        transferDTO.validate();
        return transferService.transfer(transferDTO);
    }


    @LockAction(key = "businessTransfer", value = "#transferDTO.tradeVoucherNo")
    @ApiOperation(value = "业务转账", notes = "业务转账")
    @PostMapping(value = "businessTransfer")
    public BusinessTransferRespDTO businessTransfer(@RequestBody BusinessTransferReqDTO transferDTO) {
        transferDTO.validate();
        return transferService.businessTransfer(transferDTO);
    }


    @ApiOperation(value = "查询交易明细", notes = "查询交易明细")
    @PostMapping(value = "queryTradeRecordList")
    public ViewPage<TradeRecord> queryTradeRecordList(@RequestBody PageDTO<QueryTradeRecordDTO> pageDTO) {
        pageDTO.validate();
        return tradeRecordService.queryTradeRecordList(pageDTO);
    }

}
