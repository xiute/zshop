package com.dag.account.api.dto.settlebill;

import com.dag.account.dao.entity.SettleBillItem;
import com.dag.common.entity.BaseDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @since 2020-07-26
 */
@Data
public class SettleBillDTO extends BaseDTO {

    private String id;
    /**
     * 结算单号
     */
    private String settleBillNo;
    /**
     * 结算单名称
     */
    private String settleBillName;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 单据数量
     */
    private Integer billNum;
    /**
     * 账单金额
     */
    private BigDecimal amount;
    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 账单状态 1待确认 2待结算 3已结算
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 结算单明细项
     */
    private List<SettleBillItem> settleBillItemList;

}
