package com.dag.account.api.dto.bankcard;

import com.dag.common.entity.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 绑定银行卡DTO
 */
@Data
public class BindBankCardDTO extends SaaSDTO {

    /**
     * 会员id
     */
    @NotNull(message = "会员id不能为空")
    private String memberId;     // 账号id
    /**
     * 账户id
     */
    private String accountId;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 开户行名称
     */
    @NotNull(message = "开户行名称不能为空")
    private String bankName;
    /**
     * 支行名
     */
    private String subBankName;
    /**
     * 银行卡号
     */
    @NotNull(message = "银行卡号不能为空")
    private String cardNo;
    /**
     * 开户名
     */
    @NotNull(message = "开户名不能为空")
    private String openAccountName;
    /**
     * 银行卡类型 0对私 1对公
     */
    private Integer bankCardType;
    /**
     * 是否默认
     */
    private Integer isDefault;
    /**
     * 银行卡照片
     */
    private String bankUrls;

}
