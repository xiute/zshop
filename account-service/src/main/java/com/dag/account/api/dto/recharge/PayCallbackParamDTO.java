package com.dag.account.api.dto.recharge;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author ChienSun
 * @date 2019/5/8
 */
@Data
public class PayCallbackParamDTO extends BaseDTO{

    /**
     * 结果
     */
    private boolean success;
    /**
     * 外部单号
     */
    private String outBillNo;
    /**
     * 第三方交易号
     */
    private String outTradeNo;
    /**
     * 交易渠道
     */
    private Integer tradeChannel;
    /**
     * 交易金额
     */
    private BigDecimal money;

}
