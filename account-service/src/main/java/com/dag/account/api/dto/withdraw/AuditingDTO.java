package com.dag.account.api.dto.withdraw;

import com.dag.common.entity.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 提现审核DTO
 */
@Data
public class AuditingDTO extends SaaSDTO {

    /**
     * 提现单id
     */
    @NotNull(message = "提现单id不能为空")
    private String withdrawBillId;
    /**
     * 审核备注
     */
    private String inspectRemark;
    /**
     * 提现审核状态
     */
    @NotNull(message = "提现审核状态不能为空")
    private Integer status;
    /**
     * 操作人id
     */
    @NotNull(message = "操作人id不能为空")
    private String operatorId;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;

}
