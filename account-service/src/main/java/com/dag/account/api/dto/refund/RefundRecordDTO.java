package com.dag.account.api.dto.refund;

import com.dag.common.entity.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 退款记录DTO
 */
@Data
public class RefundRecordDTO extends SaaSDTO{

    /**
     * 交易业务单号
     */
    @NotNull(message = "交易业务单号不能为空")
    private String tradeVoucherNo;
    /**
     * 金额
     */
    @NotNull(message = "金额不能为空")
    private BigDecimal amount;
    /**
     * 交易主题
     */
    @NotNull(message = "交易主题不能为空")
    private String tradeSubject;
    /**
     * 交易内容
     */
    @NotNull(message = "交易内容不能为空")
    private String tradeContent;
    /**
     * 交易时间
     */
    @NotNull(message = "交易时间不能为空")
    private Date tradeTime;
    /**
     * 操作人id
     */
    @NotNull(message = "操作人id不能为空")
    private String operatorId;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;
}
