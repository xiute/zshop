package com.dag.account.api.dto.settlebill;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @since 2020-07-26
 */
@Data
public class AddSettleBillItemDTO extends BaseDTO {

    /**
     * 业务单据id
     */
    private String itemBillId;
    /**
     * 业务单据编号
     */
    private String itemBillNo;
    /**
     * 类型
     */
    private Integer itemType;
    /**
     * 类型名称
     */
    private String itemTypeName;
    /**
     * 账单金额
     */
    private BigDecimal amount;
    /**
     * 业务发生时间
     */
    private Date occurTime;

}
