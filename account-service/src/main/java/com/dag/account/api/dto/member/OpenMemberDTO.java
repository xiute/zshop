package com.dag.account.api.dto.member;

import com.dag.common.entity.SaaSDTO;
import com.dag.common.utils.BeanValidatorsUtils;
import com.dag.common.utils.IDataUtils;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 开通会员DTO
 */
@Data
public class OpenMemberDTO extends SaaSDTO {

    /**
     * 会员姓名
     */
    private String memberName;
    /**
     * 手机号
     */
    @NotNull(message = "手机号不能为空")
    private String mobile;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 开通的身份列表
     */
    @NotNull(message = "开通的身份列表不能为空")
    private List<OpenMemberIdentityDTO> identityList;

    @Override
    public void validate() {
        super.validate();

        // 校验开通的身份列表
        if(IDataUtils.isNotEmpty(identityList)){
            for (OpenMemberIdentityDTO identityDTO : identityList) {
                BeanValidatorsUtils.validate(identityDTO);
            }
        }
    }
}
