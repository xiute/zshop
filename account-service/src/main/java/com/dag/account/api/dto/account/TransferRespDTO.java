package com.dag.account.api.dto.account;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 转账结果DTO
 */
@Data
public class TransferRespDTO extends BaseDTO {

    /**
     * 交易流水号
     */
    private String serialNo;
    /**
     * 交易凭证单号
     */
    private String tradeVoucherNo;
    /**
     * 转账金额
     */
    private BigDecimal amount;
    /**
     * 转账结果
     */
    private boolean success;
    /**
     * 转账描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static TransferRespDTO bySuccess(String serialNo, String tradeVoucherNo, String message){
        TransferRespDTO transferRespDTO = by(serialNo, tradeVoucherNo, message);
        transferRespDTO.setSuccess(true);
        return transferRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static TransferRespDTO byFail(String serialNo, String tradeVoucherNo, String message){
        TransferRespDTO transferRespDTO = by(serialNo, tradeVoucherNo, message);
        transferRespDTO.setSuccess(false);
        return transferRespDTO;
    }

    private static TransferRespDTO by(String serialNo, String tradeVoucherNo, String message){
        TransferRespDTO transferRespDTO = new TransferRespDTO();
        transferRespDTO.setSerialNo(serialNo);
        transferRespDTO.setTradeVoucherNo(tradeVoucherNo);
        transferRespDTO.setMessage(message);
        return transferRespDTO;
    }

}
