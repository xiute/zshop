package com.dag.account.api.dto.payment;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class UnifiedPayRespDTO extends BaseDTO {

    /**
     * 支付流水号
     */
    private String serialNo;
    /**
     * 交易凭证单号
     */
    private String tradeVoucherNo;
    /**
     * 支付金额
     */
    private BigDecimal amount;
    /**
     * 转账结果
     */
    private boolean success;
    /**
     * 转账描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static UnifiedPayRespDTO bySuccess(String serialNo, String tradeVoucherNo, String message){
        UnifiedPayRespDTO unifiedPayResponseDTO = by(serialNo, tradeVoucherNo , message);
        unifiedPayResponseDTO.setSuccess(true);
        return unifiedPayResponseDTO;
    }

    /**
     * 构建失败结果
     */
    public static UnifiedPayRespDTO byFail(String serialNo, String tradeVoucherNo, String message){
        UnifiedPayRespDTO unifiedPayResponseDTO = by(serialNo, tradeVoucherNo, message);
        unifiedPayResponseDTO.setSuccess(false);
        return unifiedPayResponseDTO;
    }


    private static UnifiedPayRespDTO by(String serialNo, String tradeVoucherNo, String message){
        UnifiedPayRespDTO unifiedPayResponseDTO = new UnifiedPayRespDTO();
        unifiedPayResponseDTO.setSerialNo(serialNo);
        unifiedPayResponseDTO.setTradeVoucherNo(tradeVoucherNo);
        unifiedPayResponseDTO.setMessage(message);
        return unifiedPayResponseDTO;
    }

}
