package com.dag.account.api;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dag.account.api.dto.withdraw.*;
import com.dag.account.constants.WithdrawChannelEnum;
import com.dag.account.dao.entity.WithdrawBill;
import com.dag.account.service.IWithdrawBillService;
import com.dag.common.entity.IdsDTO;
import com.dag.common.entity.PageDTO;
import com.dag.common.entity.ViewPage;
import com.dag.common.utils.ModelMapperUtils;
import com.dag.lock.redis.annotation.LockAction;
import com.dag.mybatis.utils.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * 提现管理
 * @author 孙建
 * @since 2020-05-30
 */
@Slf4j
@RestController
@RequestMapping("/account/withdrawBill")
public class WithdrawBillApi {

    @Autowired
    private IWithdrawBillService withdrawBillService;


    @ApiOperation(value = "提现记录查询", notes = "提现记录查询")
    @PostMapping(value = "queryWithdrawList")
    public ViewPage<WithdrawBill> queryWithdrawList(@RequestBody PageDTO<QueryWithdrawBillDTO> pageDTO) {
        pageDTO.validate();

        // 封装参数
        // WithdrawBill withdrawBill = ModelMapperUtils.map(pageDTO.getBody(), WithdrawBill.class);
        QueryWithdrawBillDTO queryWithdrawBillDTO = ModelMapperUtils.map(pageDTO.getBody(), QueryWithdrawBillDTO.class);
        // 根据条件查询
        QueryWrapper<WithdrawBill> queryWrapper = buildQuery(queryWithdrawBillDTO);

        PageUtils.sortFiledToQueryWrapper(pageDTO.getOrderBys(), queryWrapper);
        PageHelper.startPage(pageDTO.getPageNum(), pageDTO.getPageSize());
        PageInfo<WithdrawBill> withdrawBillList = new PageInfo<>(withdrawBillService.list(queryWrapper));

        return PageUtils.convert(withdrawBillList);
    }

    @ApiOperation(value = "根据ids查询提现记录", notes = "根据ids查询提现记录")
    @PostMapping(value = "queryListByIds")
    public List<WithdrawBill> queryListByIds(@RequestBody IdsDTO<String> idsDTO) {
        idsDTO.validate();
        return withdrawBillService.listByIds(idsDTO.getIds());
    }


    @LockAction(key = "applyWithdraw", value = "#applyWithdrawReqDTO.memberId")
    @ApiOperation(value = "申请提现", notes = "申请提现")
    @PostMapping(value = "applyWithdraw")
    public ApplyWithdrawRespDTO applyWithdraw(@RequestBody ApplyWithdrawReqDTO applyWithdrawReqDTO) {
        applyWithdrawReqDTO.validate();
        return withdrawBillService.applyWithdraw(applyWithdrawReqDTO);
    }


    @LockAction(key = "auditing", value = "#auditingDTO.withdrawBillId")
    @ApiOperation(value = "提现审核", notes = "提现审核")
    @PostMapping(value = "auditing")
    public boolean auditing(@RequestBody AuditingDTO auditingDTO) {
        auditingDTO.validate();
        withdrawBillService.auditing(auditingDTO);
        return true;
    }


    @LockAction(key = "markTransferred", value = "#markTransferredDTO.withdrawBillId")
    @ApiOperation(value = "标记为已转账", notes = "标记为已转账")
    @PostMapping(value = "markTransferred")
    public boolean markTransfer(@RequestBody MarkTransferredDTO markTransferredDTO) {
        markTransferredDTO.validate();
        withdrawBillService.markTransferred(markTransferredDTO);
        return true;
    }

    // 特殊查询条件
    private QueryWrapper buildQuery(QueryWithdrawBillDTO queryBill) {
        QueryWrapper<WithdrawBill> queryWrapper = new QueryWrapper<>();

        if (!StringUtils.isEmpty(queryBill.getId())) {
            queryWrapper.eq("id", queryBill.getId());
        }

        if (!StringUtils.isEmpty(queryBill.getMemberType())) {
            queryWrapper.likeRight("member_name", queryBill.getMemberType());
        } else if (!StringUtils.isEmpty(queryBill.getMemberName())) {
            queryWrapper.like("member_name", queryBill.getMemberName());
        }

        if (!StringUtils.isEmpty(queryBill.getWithdrawNo())) {
            queryWrapper.eq("withdraw_no", queryBill.getWithdrawNo());
        }

        if (!StringUtils.isEmpty(queryBill.getBankName())) {
            queryWrapper.like("bank_name", queryBill.getBankName());
        }

        if (queryBill.getWithdrawChannel() != null) {
            queryWrapper.eq("withdraw_channel", queryBill.getWithdrawChannel());
        }

        // 账户名
        if (!StringUtils.isEmpty(queryBill.getOtherAccountName())) {
            queryWrapper.eq("other_account_name", queryBill.getOtherAccountName());
            if (queryBill.getWithdrawChannel() != null && WithdrawChannelEnum.BANK_CARD.getIndex() == queryBill.getWithdrawChannel()) {
                queryWrapper.eq("open_account_name", queryBill.getOtherAccountName());
            }
        }

        if (!StringUtils.isEmpty(queryBill.getOtherAccountNo())) {
            queryWrapper.eq("other_account_no", queryBill.getOtherAccountNo());
            if (queryBill.getWithdrawChannel() != null && WithdrawChannelEnum.BANK_CARD.getIndex() == queryBill.getWithdrawChannel()) {
                queryWrapper.eq("card_no", queryBill.getOtherAccountNo());
            }
        }


        if (queryBill.getStatus() != null) {
            queryWrapper.eq("status", queryBill.getStatus());
        }

        Date startCreateTime = queryBill.getStartApplyTime();
        Date endCreateTime = queryBill.getEndApplyTime();
        if (startCreateTime!=null && endCreateTime!=null) {
            queryWrapper.between("apply_time", startCreateTime, endCreateTime);
        }
        return queryWrapper;
    }
}
