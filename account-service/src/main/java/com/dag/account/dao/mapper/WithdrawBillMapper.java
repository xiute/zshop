package com.dag.account.dao.mapper;

import com.dag.account.dao.entity.WithdrawBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 孙建
 * @since 2020-05-30
 */
public interface WithdrawBillMapper extends BaseMapper<WithdrawBill> {

}