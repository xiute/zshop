package com.dag.account.dao.mapper;

import com.dag.account.dao.entity.RechargeBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author chien
 * @since 2020-07-23
 */
public interface RechargeBillMapper extends BaseMapper<RechargeBill> {

}