package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.dag.common.entity.DataEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 账户
 **/
@Data
@TableName("acc_account")
public class Account extends DataEntity {

    /**
     * 会员id
     */
    private String memberId;
    /**
     * 账户类型 目前就一个主账户 AccountTypeEnum
     */
    private Integer accountType;
    /**
     * 账户类型名称 目前就一个主账户 AccountTypeEnum
     */
    private String accountTypeName;
    /**
     * 账户性质
     */
    private Integer nature;
    /**
     * 总金额
     */
    private BigDecimal totalAmount;
    /**
     * 可用余额
     */
    private BigDecimal balanceAmount;
    /**
     * 冻结金额
     */
    private BigDecimal freezeAmount;
    /**
     * 支付密码
     */
    private String payPwd;
    /**
     * 账户状态
     */
    private Integer status;
    /**
     * 乐观锁
     */
    @Version
    private Long version;
    /**
     * 更新人
     */
    private String updaterName;

}
