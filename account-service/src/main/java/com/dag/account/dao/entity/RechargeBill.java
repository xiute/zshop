package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dag.common.entity.DataEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chien
 * @since 2020-07-23
 */
@Data
@TableName("acc_recharge_bill")
public class RechargeBill extends DataEntity {

    /**
     * 充值单号
     */
    private String rechargeNo;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 账户id
     */
    private String accountId;
    /**
     * 交易渠道
     */
    private Integer tradeChannel;
    /**
     * 金额
     */
    private BigDecimal money;
    /**
     * 充值状态
     */
    private Integer rechargeStatus;
    /**
     * 交易时间
     */
    private Date payTime;


}
