package com.dag.account.dao.mapper;

import com.dag.account.dao.entity.WithdrawAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author chien
 * @since 2020-07-22
 */
public interface WithdrawAccountMapper extends BaseMapper<WithdrawAccount> {

}