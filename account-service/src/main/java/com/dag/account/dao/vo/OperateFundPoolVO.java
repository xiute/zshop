package com.dag.account.dao.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 操作资金池VO
 */
@Data
public class OperateFundPoolVO {

    private String fundPoolId;

    private BigDecimal amount;

    public OperateFundPoolVO(String fundPoolId, BigDecimal amount){
        this.fundPoolId = fundPoolId;
        this.amount = amount;
    }

}
