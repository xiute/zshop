package com.dag.account.dao.mapper;

import com.dag.account.dao.entity.SettleBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author Administrator
 * @since 2020-07-26
 */
public interface SettleBillMapper extends BaseMapper<SettleBill> {

}