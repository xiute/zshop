package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dag.common.entity.DataEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 提现申请单
 * @author 孙建
 * @since 2020-05-30
 */
@Data
@TableName("acc_withdraw_bill")
public class WithdrawBill extends DataEntity {

    /**
     * 提现申请号
     */
    private String withdrawNo;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 会员名称
     */
    private String memberName;
    /**
     * 账号id
     */
    private String accountId;
    /**
     * 账户类型
     */
    private Integer accountType;
    /**
     * 账户类型名称
     */
    private String accountTypeName;
    /**
     * 提现渠道
     */
    private Integer withdrawChannel;
    /**
     * 申请提现金额
     */
    private BigDecimal amount;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 支行名
     */
    private String subBankName;
    /**
     * 银行卡号
     */
    private String cardNo;
    /**
     * 开户名
     */
    private String openAccountName;
    /**
     * 其他账户类型名称
     */
    private String otherAccountTypeName;
    /**
     * 其他账户名
     */
    private String otherAccountName;
    /**
     * 其他账号
     */
    private String otherAccountNo;
    /**
     * 提现方式
     */
    private Integer withdrawType;
    /**
     * 提现状态
     */
    private Integer status;
    /**
     * 申请备注
     */
    private String applyRemark;
    /**
     * 申请时间
     */
    private Date applyTime;
    /**
     * 审核备注
     */
    private String inspectRemark;
    /**
     * 审核时间
     */
    private Date inspectTime;
    /**
     * 转账备注
     */
    private String transferRemark;
    /**
     * 转账时间
     */
    private Date transferTime;


}
