package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dag.common.entity.DataEntity;
import lombok.Data;

@Data
@TableName("acc_bank_card")
public class BankCard extends DataEntity {

    /**
     * 会员id
     */
    private String memberId;     // 账号id
    /**
     * 账户id
     */
    private String accountId;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 支行名
     */
    private String subBankName;
    /**
     * 银行卡号
     */
    private String cardNo;
    /**
     * 开户名
     */
    private String openAccountName;
    /**
     * 银行卡类型 0对私 1对公
     */
    private Integer bankCardType;
    /**
     * 是否默认
     */
    private Integer isDefault;
    /**
     * 卡状态
     */
    private Integer cardStatus;
    /**
     * 银行卡照片
     */
    private String bankUrls;

}
