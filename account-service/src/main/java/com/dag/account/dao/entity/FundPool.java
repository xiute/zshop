package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.dag.common.entity.DataEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 资金池总金额
 * @author 孙建
 * @since 2020-05-30
 */
@Data
@TableName("acc_fund_pool")
public class FundPool extends DataEntity {

    /**
     * 总金额
     */
    private BigDecimal totalAmount;
    /**
     * 乐观锁
     */
    @Version
    private Long version;

}
