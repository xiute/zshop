package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dag.common.entity.DataEntity;
import lombok.Data;

/**
 * 会员审核记录
 */
@Data
@TableName("acc_member_audit_record")
public class MemberAuditRecord extends DataEntity {

    /**
     * 会员id
     */
    private String memberId;
    /**
     * 身份id
     */
    private String memberIdentityId;
    /**
     * 审核状态
     */
    private Integer auditStatus;
    /**
     * 审核说明
     */
    private String auditExplain;


}
