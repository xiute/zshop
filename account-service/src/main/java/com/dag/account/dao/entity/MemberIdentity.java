package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dag.common.entity.DataEntity;
import lombok.Data;

/**
 * 会员身份
 */
@Data
@TableName("acc_member_identity")
public class MemberIdentity extends DataEntity {

    /**
     * 会员id
     */
    private String memberId;
    /**
     * 身份类型
     */
    private Integer identityType;
    /**
     * 身份类型名称
     */
    private String identityName;
    /**
     * 审核状态
     */
    private Integer auditStatus;


}
