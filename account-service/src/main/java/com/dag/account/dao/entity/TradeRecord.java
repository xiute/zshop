package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dag.common.entity.DataEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 交易记录
 */
@Data
@TableName("acc_trade_record")
public class TradeRecord extends DataEntity {

    /**
     * 流水号
     */
    private String serialNo;
    /**
     * 交易业务单号
     */
    private String tradeVoucherNo;
    /**
     * 交易会员id
     */
    private String memberId;
    /**
     * 交易会员姓名
     */
    private String memberName;
    /**
     * 交易账户id
     */
    private String accountId;
    /**
     * 交易对方会员id
     */
    private String otherMemberId;
    /**
     * 交易对方会员姓名
     */
    private String otherMemberName;
    /**
     * 交易对方会员账户id
     */
    private String otherAccountId;
    /**
     * 交易类型
     */
    private Integer tradeType;
    /**
     * 交易类型名称
     */
    private String tradeTypeName;
    /**
     * 交易渠道
     */
    private Integer tradeChannel;
    /**
     * 金额
     */
    private BigDecimal amount;
    /**
     * 交易主题
     */
    private String tradeSubject;
    /**
     * 交易内容
     */
    private String tradeContent;
    /**
     * 资金流向 10收 20支
     */
    private Integer fundFlowType;
    /**
     * 支付状态
     */
    private Integer tradeStatus;
    /**
     * 交易时间
     */
    private Date tradeTime;

}
