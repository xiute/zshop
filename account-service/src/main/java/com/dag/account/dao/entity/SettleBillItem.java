package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dag.common.entity.DataEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Administrator
 * @since 2020-07-26
 */
@Data
@TableName("acc_settle_bill_item")
public class SettleBillItem extends DataEntity {

    /**
     * 结算单id
     */
    private String settleBillId;
    /**
     * 业务单据id
     */
    private String itemBillId;
    /**
     * 业务单据编号
     */
    private String itemBillNo;
    /**
     * 类型
     */
    private Integer itemType;
    /**
     * 类型名称
     */
    private String itemTypeName;
    /**
     * 账单金额
     */
    private BigDecimal amount;
    /**
     * 业务发生时间
     */
    private Date occurTime;
    /**
     * 会员id
     */
    private String memberId;


}
