package com.dag.account.listener;

import com.dag.account.dao.entity.Account;
import com.dag.account.dao.entity.FundPool;
import com.dag.account.dao.entity.Member;
import com.dag.account.service.IPlatformService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 系统启动类
 * @author ChienSun
 * @date 2019/1/3
 */
@Slf4j
@Component
@Order(1)
public class ApplicationStartupRunner implements ApplicationRunner {

    @Autowired
    private IPlatformService platformService;


    /**
     * 初始化平台会员和账户
     */
    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        // 开通平台会员
        Member member = platformService.openPlatformMember();
        log.info("开通平台会员{}", member);
        // 开通平台账户
        Account platformAccount = platformService.openPlatformAccount();
        log.info("开通平台账户{}", platformAccount);
        // 开通平台收益账户
        Account profitAccount = platformService.openProfitAccount();
        log.info("开通平台收益账户{}", profitAccount);
        // 开通资金池账户
        FundPool fundPool = platformService.openFundPoolAccount();
        log.info("开通资金池账户{}", fundPool);
    }
}
