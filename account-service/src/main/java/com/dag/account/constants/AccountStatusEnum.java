package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 账户状态枚举
 **/
public enum AccountStatusEnum {

    AUTHING(0, "预开户"),
    VALID(1, "正常"),
    FREEZE(2, "冻结"),
    ABNORMAL(3, "异常"),
    LOGOUT(4, "销户");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (AccountStatusEnum item : AccountStatusEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    AccountStatusEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }


}
