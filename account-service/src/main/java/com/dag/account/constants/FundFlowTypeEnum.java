package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 资金流向类型
 * @author 孙建
 */
public enum FundFlowTypeEnum {

    /** 收入 */
    INCOME(10, "收"),
    /** 支出 */
    PAY(20, "支");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (FundFlowTypeEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    FundFlowTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }

}
