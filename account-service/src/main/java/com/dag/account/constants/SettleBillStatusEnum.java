package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 结算单状态
 */
public enum SettleBillStatusEnum {

    WAIT_CONFIRM(1, "待确认"),
    WAIT_PAID(2, "待结算"),
    COMPLETE(3, "已结算");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (SettleBillStatusEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    SettleBillStatusEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }

}
