package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 交易渠道
 * @author 孙建
 */
public enum TradeChannelEnum {

    /** 平台 */
    PLATFORM(0, "平台"),
    /** 账户 */
    ACCOUNT(1, "账户"),
    /** 支付宝 */
    ALIPAY(2, "支付宝"),
    /** 微信 */
    WECHAT(3, "微信");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (TradeChannelEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    TradeChannelEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }

}
