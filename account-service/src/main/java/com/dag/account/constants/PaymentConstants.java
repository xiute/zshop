package com.dag.account.constants;

/**
 * 支付配置常量
 */
public final class PaymentConstants {

    private PaymentConstants(){}

    // 支付宝
    public static final int ALIPAY = 1;
    // 微信
    public static final int WECHAT = 2;

}
