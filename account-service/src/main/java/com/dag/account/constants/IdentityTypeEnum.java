package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 会员类型
 */
public enum IdentityTypeEnum {

    /** 平台 */
    PLATFORM(999, "平台");

    private int index;
    private String text;


    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (IdentityTypeEnum item : IdentityTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    IdentityTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }


}
