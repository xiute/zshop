package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 账户类型
 */
public enum AccountTypeEnum {

    PLATFORM(999, "平台主账户"),
    PROFIT(666, "平台收益账户");

    private int index;
    private String text;


    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (AccountTypeEnum item : AccountTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    AccountTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }


}
