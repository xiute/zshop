package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 转账方式
 **/
public enum TransferTypeEnum {

    AT_ONCE(1, "实时"),
    TIMING(2, "非实时");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (TransferTypeEnum item : TransferTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    TransferTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }


}
