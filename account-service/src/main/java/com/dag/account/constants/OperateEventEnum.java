package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 操作事件枚举
 */
public enum OperateEventEnum {

    OPEN("开户", "开户"),
    UPDATE("账户变更", "账户变更"),
    AUTH("认证", "认证"),
    FREEZE_ACCOUNT("冻结账户", "冻结账户"),
    UNFREEZE_ACCOUNT("解结账户", "解结账户"),
    LOGOUT("销户", "销户"),
    ADD_MONEY("入账", "入账"),
    DEDUCTION_MONEY("扣款", "扣款"),
    FREEZE_MONEY("冻结", "冻结"),
    UNFREEZE_MONEY("解冻", "解冻"),
    UNFREEZE_DEDUCTION_MONEY("解冻扣除", "解冻扣除");

    private String index;
    private String text;

    private static Map<String, String> enumMap = new HashMap<>();

    static {
        for (OperateEventEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    OperateEventEnum(String index, String text){
        this.index = index;
        this.text = text;
    }


    public String getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(String index){
        return enumMap.get(index);
    }

}
