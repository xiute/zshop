package com.dag.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dag.account.dao.entity.MemberIdentity;
import com.dag.account.dao.mapper.MemberIdentityMapper;
import com.dag.account.service.IMemberIdentityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 会员身份操作
 * @author 孙建
 */
@Slf4j
@Service
public class MemberIdentityServiceImpl extends ServiceImpl<MemberIdentityMapper, MemberIdentity> implements IMemberIdentityService {


    @Override
    public MemberIdentity queryByIdentityType(int identityType) {
        MemberIdentity memberIdentity = new MemberIdentity();
        memberIdentity.setIdentityType(identityType);
        return getOne(new QueryWrapper<>(memberIdentity));
    }

}
