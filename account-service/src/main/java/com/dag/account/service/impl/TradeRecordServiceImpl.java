package com.dag.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dag.account.api.dto.account.QueryTradeRecordDTO;
import com.dag.account.dao.entity.TradeRecord;
import com.dag.account.dao.mapper.TradeRecordMapper;
import com.dag.account.service.ITradeRecordService;
import com.dag.common.entity.PageDTO;
import com.dag.common.entity.ViewPage;
import com.dag.common.utils.ModelMapperUtils;
import com.dag.mybatis.utils.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 交易记录
 * @author 孙建
 * @since 2020-5-30
 */
@Slf4j
@Service
public class TradeRecordServiceImpl extends ServiceImpl<TradeRecordMapper, TradeRecord> implements ITradeRecordService {

    /**
     * 根据业务号和交易类型查询
     */
    @Override
    public TradeRecord queryByTradeVoucherNo(String tradeVoucherNo, Integer tradeType, Integer fundFlowType, Integer tradeStatus) {
        TradeRecord tradeRecord = new TradeRecord();
        tradeRecord.setTradeVoucherNo(tradeVoucherNo);
        tradeRecord.setTradeType(tradeType);
        tradeRecord.setFundFlowType(fundFlowType);
        tradeRecord.setTradeStatus(tradeStatus);
        return getOne(new QueryWrapper<>(tradeRecord));
    }


    /**
     * 查询交易记录列表
     */
    @Override
    public ViewPage<TradeRecord> queryTradeRecordList(PageDTO<QueryTradeRecordDTO> pageDTO) {
        // 封装参数
        QueryTradeRecordDTO queryTradeRecordDTO = pageDTO.getBody();
        TradeRecord tradeRecord = ModelMapperUtils.map(queryTradeRecordDTO, TradeRecord.class);

        QueryWrapper<TradeRecord> queryWrapper = new QueryWrapper<>(tradeRecord);
        String memberId = queryTradeRecordDTO.getMemberId();
        if(StringUtils.isNotBlank(memberId)){
            queryWrapper.and(wrapper -> wrapper.eq("member_id", memberId)
                    .or().eq("other_member_id", memberId));
        }

        String accountId = queryTradeRecordDTO.getAccountId();
        if(StringUtils.isNotBlank(accountId)){
            queryWrapper.and(wrapper -> wrapper.eq("account_id", accountId)
                    .or().eq("other_account_id", accountId));
        }

        // 根据条件查询
        PageHelper.startPage(pageDTO.getPageNum(), pageDTO.getPageSize());
        PageUtils.sortFiledToQueryWrapper(pageDTO.getOrderBys(), queryWrapper);
        PageInfo<TradeRecord> tradeRecordList = new PageInfo<>(list(queryWrapper));

        return PageUtils.convert(tradeRecordList);
    }


    @Override
    public TradeRecord queryBySerialNo(String serialNo) {
        TradeRecord tradeRecord = new TradeRecord();
        tradeRecord.setSerialNo(serialNo);
        return getOne(new QueryWrapper<>(tradeRecord));
    }


    @Override
    public List<TradeRecord> queryByTradeVoucherNo(String tradeVoucherNo) {
        TradeRecord tradeRecord = new TradeRecord();
        tradeRecord.setTradeVoucherNo(tradeVoucherNo);
        return list(new QueryWrapper<>(tradeRecord));
    }

}
