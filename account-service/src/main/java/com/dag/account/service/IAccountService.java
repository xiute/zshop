package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.api.dto.account.CountAccountDTO;
import com.dag.account.api.dto.account.OpenAccountDTO;
import com.dag.account.dao.entity.Account;

import java.math.BigDecimal;
import java.util.List;

public interface IAccountService extends IService<Account> {

    Account openAccount(OpenAccountDTO openAccountDTO);

    List<Account> queryByMemberId(String memberId);

    Account queryByMemberIdAndAccountType(String memberId, Integer accountType);

    CountAccountDTO summary(String memberId);

    void add(Account account, BigDecimal amount, String operatorName);

    void deduction(Account account, BigDecimal amount, String operatorName);

    void freeze(Account account, BigDecimal amount, String operatorName);

    void unfreeze(Account account, BigDecimal amount, String operatorName);

    void unfreezeDeduction(Account account, BigDecimal amount, String operatorName);

}
