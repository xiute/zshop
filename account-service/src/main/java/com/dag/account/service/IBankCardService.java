package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.api.dto.bankcard.BindBankCardDTO;
import com.dag.account.dao.entity.BankCard;

import java.util.List;

public interface IBankCardService extends IService<BankCard> {

    BankCard bind(BindBankCardDTO bindBankCardDTO);

    void unbind(String id);

    BankCard queryByCardNo(String cardNo);

    List<BankCard> queryByCardNoList(List<String> cardNoList);

}
