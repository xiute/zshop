package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.dao.entity.MemberIdentity;

public interface IMemberIdentityService extends IService<MemberIdentity> {

    MemberIdentity queryByIdentityType(int identityType);

}
