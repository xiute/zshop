package com.dag.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dag.account.api.dto.settlebill.AddSettleBillItemDTO;
import com.dag.account.api.dto.settlebill.CreateSettleBillDTO;
import com.dag.account.constants.SettleBillStatusEnum;
import com.dag.account.dao.entity.SettleBill;
import com.dag.account.dao.entity.SettleBillItem;
import com.dag.account.dao.mapper.SettleBillMapper;
import com.dag.account.service.ISettleBillItemService;
import com.dag.account.service.ISettleBillService;
import com.dag.common.exception.BusinessRuntimeException;
import com.dag.common.utils.IDataUtils;
import com.dag.common.utils.ModelMapperUtils;
import com.dag.seqno.Sequence;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @since 2020-07-26
 */
@Slf4j
@Service
public class SettleBillServiceImpl extends ServiceImpl<SettleBillMapper, SettleBill> implements ISettleBillService {

    @Autowired
    private Sequence sequence;
    @Autowired
    private ISettleBillItemService settleBillItemService;

    /**
     * 创建结算单
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SettleBill createSettleBill(CreateSettleBillDTO createSettleBillDTO) {
        SettleBill settleBill = ModelMapperUtils.map(createSettleBillDTO, SettleBill.class);
        settleBill.setSettleBillNo(sequence.getFixedLengthSeqNo("acc_settle_bill", 8, "DZ"));
        settleBill.setBillNum(createSettleBillDTO.getSettleBillItemList().size());
        settleBill.setStatus(SettleBillStatusEnum.WAIT_CONFIRM.getIndex());
        settleBill.setAmount(this.countAmount(createSettleBillDTO.getSettleBillItemList()));
        settleBill.setCreateTime(new Date());
        save(settleBill);

        // 保存明细
        List<AddSettleBillItemDTO> settleBillItemList = createSettleBillDTO.getSettleBillItemList();
        if(IDataUtils.isNotEmpty(settleBillItemList)){
            List<SettleBillItem> itemList = new ArrayList<>();
            for (AddSettleBillItemDTO addSettleBillItemDTO : settleBillItemList) {
                SettleBillItem settleBillItem = ModelMapperUtils.map(addSettleBillItemDTO, SettleBillItem.class);
                settleBillItem.setSettleBillId(settleBill.getId());
                settleBillItem.setMemberId(settleBill.getMemberId());
                settleBillItem.setCreateTime(new Date());
                settleBillItem.setOrgId(settleBill.getOrgId());
                settleBillItem.setMerchantId(settleBill.getMerchantId());
                itemList.add(settleBillItem);
            }
            settleBillItemService.saveBatch(itemList);
        }
        return settleBill;
    }


    /**
     * 下一步操作
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SettleBill next(String id) {
        SettleBill settleBill = getById(id);
        if(settleBill == null){
            throw new BusinessRuntimeException("账单不存在");
        }

        Integer status = settleBill.getStatus();
        if(status == SettleBillStatusEnum.WAIT_CONFIRM.getIndex()){
            settleBill.setStatus(SettleBillStatusEnum.WAIT_PAID.getIndex());
        }else if(status == SettleBillStatusEnum.WAIT_PAID.getIndex()){
            settleBill.setStatus(SettleBillStatusEnum.COMPLETE.getIndex());
            // 启动结算到账户逻辑

        }else{
            throw new BusinessRuntimeException("账单已完结");
        }

        // 更新账单
        updateById(settleBill);

        return getById(settleBill);
    }

    @Override
    public SettleBill queryLastSettleBill(String memberId) {
        SettleBill settleBill = new SettleBill();
        settleBill.setMemberId(memberId);
        QueryWrapper<SettleBill> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.last("limit 1");
        List<SettleBill> settleBillList = list(queryWrapper);
        return IDataUtils.isNotEmpty(settleBillList) ? settleBillList.get(0) : null;
    }


    // 统计总额
    private BigDecimal countAmount(List<AddSettleBillItemDTO> settleBillItemList) {
        BigDecimal amount = BigDecimal.ZERO;
        for (AddSettleBillItemDTO addSettleBillItemDTO : settleBillItemList) {
            amount = amount.add(addSettleBillItemDTO.getAmount());
        }
        return amount;
    }
}
