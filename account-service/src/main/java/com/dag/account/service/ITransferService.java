package com.dag.account.service;

import com.dag.account.api.dto.account.BusinessTransferReqDTO;
import com.dag.account.api.dto.account.BusinessTransferRespDTO;
import com.dag.account.api.dto.account.TransferReqDTO;
import com.dag.account.api.dto.account.TransferRespDTO;

public interface ITransferService  {

    TransferRespDTO transfer(TransferReqDTO transferDTO);

    BusinessTransferRespDTO businessTransfer(BusinessTransferReqDTO transferDTO);

}
