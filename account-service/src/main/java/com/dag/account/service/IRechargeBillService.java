package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.api.dto.recharge.ApplyRechargeReqDTO;
import com.dag.account.api.dto.recharge.ApplyRechargeRespDTO;
import com.dag.account.api.dto.recharge.PayCallbackParamDTO;
import com.dag.account.dao.entity.RechargeBill;

/**
 * @author chien
 * @since 2020-07-23
 */
public interface IRechargeBillService extends IService<RechargeBill> {

    ApplyRechargeRespDTO applyRecharge(ApplyRechargeReqDTO applyRechargeDTO);

    boolean rechargeCallback(PayCallbackParamDTO payCallbackParamDTO);

    RechargeBill queryByRechargeNo(String rechargeNo);

}
