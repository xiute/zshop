package com.dag.account.service;

import com.dag.account.api.dto.payment.*;

/**
 * @author 孙建
 * @since 2020-06-01
 */
public interface IPaymentService {

    UnifiedPayRespDTO unifiedPay(UnifiedPayReqDTO payRequestDTO);

    void paymentRecord(PaymentRecordDTO paymentRecordDTO);

    FreezePrePayRespDTO freezePrePay(FreezePrePayReqDTO freezePrePayReqDTO);

    UnfreezeConfirmPayRespDTO unfreezeConfirmPay(UnfreezeConfirmPayReqDTO unfreezeConfirmPayReqDTO);

    UnfreezeCancelPayRespDTO unfreezeCancelPay(UnfreezeCancelPayReqDTO unfreezeCancelPayReqDTO);

}
