package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.api.dto.withdraw.ApplyWithdrawReqDTO;
import com.dag.account.api.dto.withdraw.ApplyWithdrawRespDTO;
import com.dag.account.api.dto.withdraw.AuditingDTO;
import com.dag.account.api.dto.withdraw.MarkTransferredDTO;
import com.dag.account.dao.entity.WithdrawBill;

/**
 * @author 孙建
 * @since 2020-05-30
 */
public interface IWithdrawBillService extends IService<WithdrawBill> {

    ApplyWithdrawRespDTO applyWithdraw(ApplyWithdrawReqDTO applyWithdrawReqDTO);

    void auditing(AuditingDTO auditingDTO);

    void markTransferred(MarkTransferredDTO markTransferredDTO);

}
