package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.api.dto.account.QueryTradeRecordDTO;
import com.dag.account.dao.entity.TradeRecord;
import com.dag.common.entity.PageDTO;
import com.dag.common.entity.ViewPage;

import java.util.List;

public interface ITradeRecordService extends IService<TradeRecord> {

    TradeRecord queryByTradeVoucherNo(String tradeVoucherNo, Integer tradeType, Integer fundFlowType, Integer tradeStatus);

    ViewPage<TradeRecord> queryTradeRecordList(PageDTO<QueryTradeRecordDTO> pageDTO);

    TradeRecord queryBySerialNo(String serialNo);

    List<TradeRecord> queryByTradeVoucherNo(String tradeVoucherNo);

}
