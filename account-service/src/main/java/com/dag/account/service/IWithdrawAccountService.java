package com.dag.account.service;

import com.dag.account.dao.entity.WithdrawAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author chien
 * @since 2020-07-22
 */
public interface IWithdrawAccountService extends IService<WithdrawAccount> {
	
}
