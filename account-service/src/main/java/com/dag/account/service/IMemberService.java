package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.api.dto.member.OpenAllDTO;
import com.dag.account.api.dto.member.OpenMemberDTO;
import com.dag.account.dao.entity.Member;

public interface IMemberService extends IService<Member> {

    Member openMember(OpenMemberDTO openMemberDTO);

    Member openAll(OpenAllDTO openAllDTO);

    void delById(String memberId);

}
