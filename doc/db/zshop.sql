/*
 Navicat Premium Data Transfer

 Source Server         : 智溢科技
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-uf62v8wo02ocqjqpwwo.mysql.rds.aliyuncs.com:3306
 Source Schema         : mkt_prd

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 08/11/2022 10:26:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for acc_account
-- ----------------------------
DROP TABLE IF EXISTS `acc_account`;
CREATE TABLE `acc_account` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会员id',
  `member_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会员id',
  `account_type` int(11) NOT NULL COMMENT '账户类型',
  `account_type_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '账户类型名称',
  `nature` int(11) NOT NULL COMMENT '账户性质',
  `total_amount` decimal(13,2) NOT NULL DEFAULT '0.00' COMMENT '总金额',
  `balance_amount` decimal(13,2) NOT NULL DEFAULT '0.00' COMMENT '可用金额',
  `freeze_amount` decimal(13,2) NOT NULL DEFAULT '0.00' COMMENT '冻结金额',
  `pay_pwd` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付密码',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '账户状态',
  `version` bigint(11) DEFAULT '0' COMMENT '乐观锁',
  `org_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `merchant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `creater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `creater_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `updater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `updater_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `remarks` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_member_id` (`member_id`) USING BTREE,
  KEY `idx_account_type` (`account_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for acc_bank_card
-- ----------------------------
DROP TABLE IF EXISTS `acc_bank_card`;
CREATE TABLE `acc_bank_card` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '账号id',
  `member_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会员id',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `bank_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '银行名称',
  `sub_bank_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支行名',
  `card_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '银行卡号',
  `open_account_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '开户名',
  `bank_card_type` tinyint(4) DEFAULT '0' COMMENT '银行卡类型 0对私 1对公',
  `is_default` tinyint(4) DEFAULT '0' COMMENT '是否默认',
  `card_status` tinyint(4) DEFAULT '0' COMMENT '卡状态',
  `bank_urls` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行卡图片',
  `org_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `merchant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `creater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `updater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT NULL,
  `remarks` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for acc_fund_pool
-- ----------------------------
DROP TABLE IF EXISTS `acc_fund_pool`;
CREATE TABLE `acc_fund_pool` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `total_amount` decimal(13,2) NOT NULL DEFAULT '0.00' COMMENT '总金额',
  `version` bigint(11) DEFAULT '0' COMMENT '乐观锁',
  `org_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `merchant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `creater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `updater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `remarks` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for acc_member
-- ----------------------------
DROP TABLE IF EXISTS `acc_member`;
CREATE TABLE `acc_member` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键',
  `member_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名称',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系电话',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `area` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `member_status` tinyint(3) DEFAULT NULL COMMENT '会员状态',
  `id_card` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证号',
  `id_card_front_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证正面url',
  `id_card_back_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证反面url',
  `hand_id_card_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手持身份证url',
  `org_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组织id',
  `merchant_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商户id',
  `creater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除标记0未删除1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='L2成员表';

-- ----------------------------
-- Table structure for acc_member_audit_record
-- ----------------------------
DROP TABLE IF EXISTS `acc_member_audit_record`;
CREATE TABLE `acc_member_audit_record` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键',
  `member_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '成员id',
  `member_identity_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员身份id',
  `audit_status` tinyint(4) DEFAULT NULL COMMENT '审核状态',
  `audit_explain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '审核说明',
  `org_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组织id',
  `merchant_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商户id',
  `creater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除标记0未删除1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for acc_member_identity
-- ----------------------------
DROP TABLE IF EXISTS `acc_member_identity`;
CREATE TABLE `acc_member_identity` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键',
  `member_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '成员id',
  `identity_type` int(11) DEFAULT NULL COMMENT '身份类型',
  `identity_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员身份名称',
  `audit_status` tinyint(3) DEFAULT '0' COMMENT '审核状态',
  `org_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组织id',
  `merchant_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商户id',
  `creater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除标记0未删除1已删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_member_id` (`member_id`,`identity_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='L2成员资质关系表';

-- ----------------------------
-- Table structure for acc_operate_record
-- ----------------------------
DROP TABLE IF EXISTS `acc_operate_record`;
CREATE TABLE `acc_operate_record` (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键s',
  `obj_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '对象id',
  `obj_type` tinyint(4) DEFAULT NULL COMMENT '对象类型',
  `event_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '事件',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '描述内容',
  `org_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组织id',
  `merchant_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商户id',
  `creater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除标记0未删除1已删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_obj_id` (`obj_id`) USING BTREE,
  KEY `idx_obj_type` (`obj_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for acc_trade_record
-- ----------------------------
DROP TABLE IF EXISTS `acc_trade_record`;
CREATE TABLE `acc_trade_record` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `serial_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '流水号',
  `trade_voucher_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易业务单号',
  `member_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易人会员id',
  `member_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '交易人会员名称',
  `account_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易账户id',
  `other_member_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易对方会员id',
  `other_member_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '交易对方会员姓名',
  `other_account_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易对方账户id',
  `trade_type` tinyint(4) DEFAULT NULL COMMENT '交易类型',
  `trade_type_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易类型名称',
  `trade_channel` tinyint(4) DEFAULT NULL COMMENT '交易渠道',
  `amount` decimal(11,2) DEFAULT NULL COMMENT '金额',
  `trade_subject` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易主题',
  `trade_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易内容',
  `fund_flow_type` tinyint(4) DEFAULT NULL COMMENT '资金流向内容',
  `trade_status` tinyint(4) DEFAULT NULL COMMENT '交易状态',
  `trade_time` datetime DEFAULT NULL COMMENT '交易时间',
  `org_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组织id',
  `merchant_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商户id',
  `creater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除标记0未删除1已删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_member_id` (`member_id`) USING BTREE,
  KEY `idx_other_member_id` (`other_member_id`) USING BTREE,
  KEY `idx_trade_voucher_no` (`trade_voucher_no`) USING BTREE,
  KEY `idx_trade_type` (`trade_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for acc_withdraw_account
-- ----------------------------
DROP TABLE IF EXISTS `acc_withdraw_account`;
CREATE TABLE `acc_withdraw_account` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会员id',
  `member_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员id',
  `type` tinyint(3) DEFAULT NULL COMMENT '账号类型',
  `type_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型名称',
  `account_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账户名',
  `account_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号',
  `of_public` tinyint(4) DEFAULT '0' COMMENT '是否对公 0否 1是',
  `org_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `merchant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `creater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `updater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `remarks` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for acc_withdraw_bill
-- ----------------------------
DROP TABLE IF EXISTS `acc_withdraw_bill`;
CREATE TABLE `acc_withdraw_bill` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `withdraw_no` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '提现申请号',
  `member_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员名称',
  `account_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `account_type` tinyint(4) DEFAULT NULL COMMENT '账户类型',
  `account_type_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '账户类型名称',
  `withdraw_channel` tinyint(3) DEFAULT NULL COMMENT '提现渠道',
  `amount` decimal(13,2) DEFAULT '0.00' COMMENT '申请提现金额',
  `bank_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '银行名称',
  `sub_bank_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支行名',
  `card_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行卡号',
  `open_account_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户名',
  `other_account_type_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '其他账户类型名称',
  `other_account_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '其他提现账号名称',
  `other_account_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '其他提现账号',
  `withdraw_type` tinyint(4) DEFAULT NULL COMMENT '提现方式',
  `status` tinyint(4) DEFAULT NULL COMMENT '提现状态',
  `apply_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '申请备注',
  `apply_time` datetime DEFAULT NULL COMMENT '申请时间',
  `inspect_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核备注',
  `inspect_time` datetime DEFAULT NULL COMMENT '审核时间',
  `transfer_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '转账备注',
  `transfer_time` datetime DEFAULT NULL COMMENT '转账时间',
  `org_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `merchant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `creater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `updater_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_trade_id` (`member_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_activity_message
-- ----------------------------
DROP TABLE IF EXISTS `es_activity_message`;
CREATE TABLE `es_activity_message` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `activity_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动名称',
  `activity_content` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动内容',
  `activity_time` bigint(20) DEFAULT NULL COMMENT '活动时间',
  `reminder` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '温馨提示',
  `seller_id` int(20) DEFAULT NULL COMMENT '店铺id',
  `activity_id` int(20) DEFAULT NULL COMMENT '活动id',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='订阅消息详情';

-- ----------------------------
-- Table structure for es_activity_message_record
-- ----------------------------
DROP TABLE IF EXISTS `es_activity_message_record`;
CREATE TABLE `es_activity_message_record` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `activity_id` int(10) DEFAULT NULL COMMENT '活动id',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `message_id` int(10) DEFAULT NULL COMMENT '活动消息Id',
  `open_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '小程序openId',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2794 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='活动订阅记录';

-- ----------------------------
-- Table structure for es_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `es_admin_user`;
CREATE TABLE `es_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '平台管理员id',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '管理员名称',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '管理员密码',
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '部门',
  `role_id` int(8) DEFAULT NULL COMMENT '权限id',
  `date_line` bigint(11) DEFAULT NULL COMMENT '创建日期',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `user_state` int(2) DEFAULT NULL COMMENT '是否删除',
  `real_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '管理员真实姓名',
  `face` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '头像',
  `founder` int(2) DEFAULT NULL COMMENT '是否为超级管理员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='平台管理员(es_admin_user)';

-- ----------------------------
-- Table structure for es_article
-- ----------------------------
DROP TABLE IF EXISTS `es_article`;
CREATE TABLE `es_article` (
  `article_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `article_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文章名称',
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `sort` int(5) DEFAULT NULL COMMENT '文章排序',
  `outside_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '外链url',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '文章内容',
  `show_position` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '显示位置',
  `create_time` bigint(20) DEFAULT NULL COMMENT '添加时间',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`article_id`) USING BTREE,
  KEY `ind_article_catid` (`category_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COMMENT='文章(es_article)';

-- ----------------------------
-- Table structure for es_article_category
-- ----------------------------
DROP TABLE IF EXISTS `es_article_category`;
CREATE TABLE `es_article_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类名称',
  `parent_id` int(10) DEFAULT NULL COMMENT '父分类id',
  `path` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '父子路径0|10|',
  `allow_delete` smallint(1) DEFAULT NULL COMMENT '是否允许删除1允许 0不允许',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类类型，枚举值',
  `sort` int(5) DEFAULT NULL COMMENT '排序，正序123',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=499 DEFAULT CHARSET=utf8 COMMENT='文章分类(es_article_category)';

-- ----------------------------
-- Table structure for es_bill
-- ----------------------------
DROP TABLE IF EXISTS `es_bill`;
CREATE TABLE `es_bill` (
  `bill_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `bill_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '结算单编号',
  `start_time` bigint(20) DEFAULT NULL COMMENT '结算开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '结算结束时间',
  `price` decimal(20,2) DEFAULT NULL COMMENT '结算总金额',
  `commi_price` decimal(20,2) DEFAULT NULL COMMENT '佣金',
  `discount_price` decimal(20,2) DEFAULT NULL COMMENT '优惠金额',
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '状态',
  `bill_type` int(1) DEFAULT NULL COMMENT '账单类型',
  `seller_id` int(8) DEFAULT NULL COMMENT '店铺id',
  `pay_time` bigint(20) DEFAULT NULL COMMENT '付款时间',
  `create_time` bigint(20) DEFAULT NULL COMMENT '出账日期',
  `bill_price` decimal(20,2) DEFAULT NULL COMMENT '结算金额',
  `refund_price` decimal(20,2) DEFAULT NULL COMMENT '在线支付退款金额',
  `refund_commi_price` decimal(20,2) DEFAULT NULL COMMENT '退还佣金金额',
  `sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '账单号',
  `shop_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺名称',
  `bank_account_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行开户名',
  `bank_account_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司银行账号',
  `bank_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户银行支行名称',
  `bank_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支行联行号',
  `bank_address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户银行地址',
  `cod_price` decimal(20,2) DEFAULT NULL COMMENT '货到付款金额',
  `cod_refund_price` decimal(20,2) DEFAULT NULL COMMENT '货到付款退款金额',
  `distribution_rebate` decimal(20,2) DEFAULT NULL COMMENT '分销返现支出',
  `distribution_return_rebate` decimal(20,2) DEFAULT NULL COMMENT '分销返现支出返还',
  PRIMARY KEY (`bill_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6763 DEFAULT CHARSET=utf8 COMMENT='结算单(es_bill)';

-- ----------------------------
-- Table structure for es_bill_item
-- ----------------------------
DROP TABLE IF EXISTS `es_bill_item`;
CREATE TABLE `es_bill_item` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `order_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `price` decimal(20,2) DEFAULT NULL COMMENT '订单价格',
  `discount_price` decimal(20,2) DEFAULT NULL COMMENT '优惠价格',
  `item_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '单项类型 收款/退款',
  `add_time` int(8) DEFAULT NULL COMMENT '加入时间',
  `bill_id` int(8) DEFAULT NULL COMMENT '所属账单id',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `status` int(1) DEFAULT NULL COMMENT '状态',
  `seller_id` int(8) DEFAULT NULL COMMENT '店铺id',
  `order_time` bigint(20) DEFAULT NULL COMMENT '下单时间',
  `refund_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款单号',
  `member_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员名称',
  `ship_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '收货人',
  `payment_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式',
  `refund_time` bigint(20) DEFAULT NULL COMMENT '退货时间',
  `platform_money` decimal(20,2) DEFAULT '0.00' COMMENT '平台收益',
  `seller_money` decimal(20,2) DEFAULT '0.00' COMMENT '卖家收益',
  `leader_money` decimal(20,2) DEFAULT '0.00' COMMENT '团长收益',
  `distribution_money` decimal(20,2) DEFAULT '0.00' COMMENT '分销商收益',
  `lv1_money` decimal(20,2) DEFAULT '0.00' COMMENT '一级分销金额',
  `lv2_money` decimal(20,2) DEFAULT '0.00' COMMENT '二级分销金额',
  `invite_money` decimal(20,2) DEFAULT '0.00' COMMENT '邀请人佣金',
  `subsidy_money` decimal(20,2) DEFAULT '0.00' COMMENT '补贴金额',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_order_sn` (`order_sn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=134718 DEFAULT CHARSET=utf8 COMMENT='结算单项表(es_bill_item)';

-- ----------------------------
-- Table structure for es_bill_member
-- ----------------------------
DROP TABLE IF EXISTS `es_bill_member`;
CREATE TABLE `es_bill_member` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `total_id` int(10) DEFAULT NULL COMMENT '总结算单id',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `start_time` int(10) DEFAULT NULL COMMENT '开始时间',
  `end_time` int(10) DEFAULT NULL COMMENT '结束时间',
  `final_money` decimal(10,2) DEFAULT NULL COMMENT '最终结算金额',
  `push_money` decimal(10,2) DEFAULT NULL COMMENT '提成金额',
  `order_money` decimal(10,2) DEFAULT NULL COMMENT '订单金额',
  `return_order_money` decimal(10,2) DEFAULT NULL COMMENT '订单返还金额',
  `return_push_money` decimal(10,2) DEFAULT NULL COMMENT '返还提成金额',
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '编号',
  `order_count` int(10) DEFAULT NULL COMMENT '订单数',
  `return_order_count` int(10) DEFAULT NULL COMMENT '返还订单数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2098 DEFAULT CHARSET=utf8 COMMENT='个人业绩单(es_bill_member)';

-- ----------------------------
-- Table structure for es_bill_total
-- ----------------------------
DROP TABLE IF EXISTS `es_bill_total`;
CREATE TABLE `es_bill_total` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `start_time` bigint(12) DEFAULT NULL COMMENT '开始时间',
  `end_time` bigint(12) DEFAULT NULL COMMENT '结束时间',
  `order_count` bigint(12) DEFAULT NULL COMMENT '订单数量',
  `return_order_count` int(10) DEFAULT NULL COMMENT '退还订单数量',
  `final_money` decimal(20,2) DEFAULT NULL COMMENT '结算金额',
  `push_money` decimal(20,2) DEFAULT NULL COMMENT '提成金额',
  `return_push_money` decimal(20,2) DEFAULT NULL COMMENT '退还提成金额',
  `order_money` decimal(20,2) DEFAULT NULL COMMENT '订单金额',
  `return_order_money` decimal(20,2) DEFAULT NULL COMMENT '退还订单金额',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='总业绩单(es_bill_total)';

-- ----------------------------
-- Table structure for es_brand
-- ----------------------------
DROP TABLE IF EXISTS `es_brand`;
CREATE TABLE `es_brand` (
  `brand_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '品牌名称',
  `logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '品牌图标',
  `disabled` int(1) DEFAULT NULL COMMENT '是否删除，0删除1未删除',
  PRIMARY KEY (`brand_id`) USING BTREE,
  KEY `ind_brand` (`disabled`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8 COMMENT='品牌(es_brand)';

-- ----------------------------
-- Table structure for es_buyer_last_leader
-- ----------------------------
DROP TABLE IF EXISTS `es_buyer_last_leader`;
CREATE TABLE `es_buyer_last_leader` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `leader_id` int(10) DEFAULT NULL COMMENT '团长id',
  `buyer_id` int(10) DEFAULT NULL COMMENT '会员uid',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7487 DEFAULT CHARSET=utf8 COMMENT='会员最后的团长';

-- ----------------------------
-- Table structure for es_category
-- ----------------------------
DROP TABLE IF EXISTS `es_category`;
CREATE TABLE `es_category` (
  `category_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类名称',
  `parent_id` int(10) DEFAULT NULL COMMENT '分类父id',
  `category_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类父子路径',
  `goods_count` int(8) DEFAULT NULL COMMENT '该分类下商品数量',
  `category_order` int(5) DEFAULT NULL COMMENT '分类排序',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类图标',
  PRIMARY KEY (`category_id`) USING BTREE,
  KEY `ind_goods_cat_parentid` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1746 DEFAULT CHARSET=utf8 COMMENT='商品分类(es_category)';

-- ----------------------------
-- Table structure for es_category_brand
-- ----------------------------
DROP TABLE IF EXISTS `es_category_brand`;
CREATE TABLE `es_category_brand` (
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `brand_id` int(10) DEFAULT NULL COMMENT '品牌id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类品牌关联表(es_category_brand)';

-- ----------------------------
-- Table structure for es_category_spec
-- ----------------------------
DROP TABLE IF EXISTS `es_category_spec`;
CREATE TABLE `es_category_spec` (
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `spec_id` int(10) DEFAULT NULL COMMENT '规格id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类规格关联表(es_category_spec)';

-- ----------------------------
-- Table structure for es_claims
-- ----------------------------
DROP TABLE IF EXISTS `es_claims`;
CREATE TABLE `es_claims` (
  `claim_id` int(11) NOT NULL AUTO_INCREMENT,
  `exception_id` int(11) DEFAULT NULL COMMENT '异常单ID',
  `exception_sn` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '异常单编号',
  `order_sn` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单编号',
  `claim_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '理赔状态',
  `claim_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '理赔类型',
  `claim_skus` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '理赔商品',
  `claim_price` decimal(20,2) DEFAULT NULL COMMENT '理赔金额',
  `claim_describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '理赔说明',
  `claim_time` bigint(20) DEFAULT NULL COMMENT '理赔时间,结案时间',
  `claim_member_id` int(11) DEFAULT NULL COMMENT '理赔人',
  `apply_member_id` int(11) DEFAULT NULL COMMENT '申请人',
  `audit_member_id` int(11) DEFAULT NULL COMMENT '审核人',
  `audit_time` bigint(20) DEFAULT NULL COMMENT '审核时间',
  `audit_describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '审核备注',
  `claim_member_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '理赔人姓名',
  `apply_member_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '申请人姓名',
  `audit_member_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核人姓名',
  `claim_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '理赔单号',
  `apply_time` bigint(20) DEFAULT NULL COMMENT '申请时间',
  `refund_reason` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '多退少补原因',
  `claim_cost` decimal(10,2) DEFAULT NULL COMMENT '理赔成本',
  `member_id` int(11) DEFAULT NULL COMMENT '生成订单的用户ID',
  `delivery_time` bigint(20) DEFAULT NULL COMMENT '出库日期',
  PRIMARY KEY (`claim_id`) USING BTREE,
  KEY `ind_exception_id` (`exception_id`) USING BTREE,
  KEY `ind_order_sn` (`order_sn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4442 DEFAULT CHARSET=utf8 COMMENT='理赔单';

-- ----------------------------
-- Table structure for es_clerk
-- ----------------------------
DROP TABLE IF EXISTS `es_clerk`;
CREATE TABLE `es_clerk` (
  `clerk_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '店员id',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `clerk_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店员名称',
  `founder` int(2) DEFAULT NULL COMMENT '是否为超级管理员，1为超级管理员 0为其他管理员',
  `role_id` int(8) DEFAULT NULL COMMENT '权限id',
  `user_state` int(2) DEFAULT NULL COMMENT '店员状态，0为禁用，1为正常',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建日期',
  `shop_id` int(8) DEFAULT NULL COMMENT '店铺id',
  PRIMARY KEY (`clerk_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8 COMMENT='店员(es_clerk)';

-- ----------------------------
-- Table structure for es_comment_gallery
-- ----------------------------
DROP TABLE IF EXISTS `es_comment_gallery`;
CREATE TABLE `es_comment_gallery` (
  `img_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `comment_id` int(10) DEFAULT NULL COMMENT '主键',
  `original` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片路径',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`img_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8 COMMENT='评论图片(es_comment_gallery)';

-- ----------------------------
-- Table structure for es_comment_reply
-- ----------------------------
DROP TABLE IF EXISTS `es_comment_reply`;
CREATE TABLE `es_comment_reply` (
  `reply_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `comment_id` int(10) DEFAULT NULL COMMENT '评论id',
  `parent_id` int(10) DEFAULT NULL COMMENT '回复父id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '回复内容',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家或者买家',
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '父子路径0|10|',
  `reply_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'INITIAL',
  PRIMARY KEY (`reply_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='评论回复(es_comment_reply)';

-- ----------------------------
-- Table structure for es_commission_tpl
-- ----------------------------
DROP TABLE IF EXISTS `es_commission_tpl`;
CREATE TABLE `es_commission_tpl` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `tpl_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `tpl_describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `cycle` int(10) DEFAULT NULL COMMENT '周期',
  `grade1` decimal(10,2) DEFAULT NULL COMMENT '1级返利',
  `grade2` decimal(10,2) DEFAULT NULL COMMENT '2级返利',
  `is_default` smallint(1) DEFAULT NULL COMMENT '是否默认',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='模版(es_commission_tpl)';

-- ----------------------------
-- Table structure for es_connect
-- ----------------------------
DROP TABLE IF EXISTS `es_connect`;
CREATE TABLE `es_connect` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(50) DEFAULT NULL COMMENT '会员id',
  `union_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '第三方唯一标示',
  `open_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信openid',
  `union_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '信任登录类型',
  `unbound_time` bigint(50) DEFAULT NULL COMMENT '解绑时间',
  `public_open_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信公众号openid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13149 DEFAULT CHARSET=utf8 COMMENT='信任登录(es_connect)';

-- ----------------------------
-- Table structure for es_connect_setting
-- ----------------------------
DROP TABLE IF EXISTS `es_connect_setting`;
CREATE TABLE `es_connect_setting` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '授权类型',
  `config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '参数组',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='信任登录参数(es_connect_setting)';

-- ----------------------------
-- Table structure for es_coupon
-- ----------------------------
DROP TABLE IF EXISTS `es_coupon`;
CREATE TABLE `es_coupon` (
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '优惠券标题',
  `coupon_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `coupon_price` decimal(10,2) DEFAULT NULL COMMENT '优惠券面额',
  `coupon_threshold_price` decimal(10,2) DEFAULT NULL COMMENT '优惠券门槛价格',
  `start_time` bigint(11) DEFAULT NULL COMMENT '使用起始时间',
  `end_time` bigint(11) DEFAULT NULL COMMENT '使用截止时间',
  `create_num` int(10) DEFAULT NULL COMMENT '发行量',
  `limit_num` int(10) DEFAULT NULL COMMENT '每人限领数量',
  `used_num` int(10) DEFAULT NULL COMMENT '已被使用的数量',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺ID',
  `received_num` int(10) DEFAULT NULL COMMENT '已被领取的数量',
  `seller_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺名称',
  `coupon_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '优惠券类型',
  `disabled` tinyint(4) DEFAULT NULL COMMENT '是否禁用',
  `create_time` bigint(11) DEFAULT NULL COMMENT '创建时间',
  `feature` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '券特征json',
  `use_time_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '使用时间类型',
  `use_start_time` bigint(20) DEFAULT NULL COMMENT '使用开始时间',
  `use_end_time` bigint(20) DEFAULT NULL COMMENT '使用结束时间',
  `use_period` int(10) DEFAULT NULL COMMENT '使用周期',
  PRIMARY KEY (`coupon_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8 COMMENT='优惠券(es_coupon)';

-- ----------------------------
-- Table structure for es_custom_words
-- ----------------------------
DROP TABLE IF EXISTS `es_custom_words`;
CREATE TABLE `es_custom_words` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `add_time` bigint(20) DEFAULT NULL COMMENT '添加时间',
  `disabled` smallint(1) DEFAULT NULL COMMENT '显示 1  隐藏 0',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自定义分词表(es_custom_words)';

-- ----------------------------
-- Table structure for es_dict
-- ----------------------------
DROP TABLE IF EXISTS `es_dict`;
CREATE TABLE `es_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '父级字典',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型',
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '标签名',
  `value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '数据值',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `sort` int(11) DEFAULT '0' COMMENT '排序（升序）',
  `classify` tinyint(4) DEFAULT NULL COMMENT '归类 1系统 2通用 3私有',
  `org_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组织id',
  `merchant_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商户id',
  `creater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '删除标记0未删除1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_distribution
-- ----------------------------
DROP TABLE IF EXISTS `es_distribution`;
CREATE TABLE `es_distribution` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(11) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '关系路径',
  `member_id_lv2` bigint(10) DEFAULT NULL COMMENT '上上级',
  `member_id_lv1` bigint(10) DEFAULT NULL COMMENT '上级',
  `downline` bigint(10) DEFAULT NULL COMMENT '下线人数',
  `order_num` bigint(10) DEFAULT NULL COMMENT '订单数',
  `rebate_total` decimal(20,2) DEFAULT NULL COMMENT '返利总额',
  `turnover_price` decimal(20,2) DEFAULT NULL COMMENT '营业额总额',
  `can_rebate` decimal(20,2) DEFAULT NULL COMMENT '可提现金额',
  `commission_frozen` decimal(20,2) DEFAULT NULL COMMENT '返利金额冻结',
  `current_tpl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '使用模版名称',
  `current_tpl_id` int(11) DEFAULT NULL COMMENT '使用模版',
  `withdraw_frozen_price` decimal(20,2) DEFAULT NULL COMMENT '提现冻结',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `audit_status` tinyint(4) DEFAULT '0' COMMENT '审核状态',
  `status_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '状态名称',
  `audit_time` bigint(20) DEFAULT NULL COMMENT '审核时间',
  `apply_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '申请原因',
  `audit_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核备注',
  `distributor_grade` tinyint(4) DEFAULT NULL COMMENT '分销等级',
  `distributor_title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分销头衔',
  `status` tinyint(4) DEFAULT '1' COMMENT '启用禁用',
  `invite_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分销邀请码',
  `lv1_create_time` bigint(20) DEFAULT NULL COMMENT '1级关系创建时间',
  `lv1_expire_time` bigint(20) DEFAULT NULL COMMENT '1级关系失效时间',
  `lv2_expire_time` bigint(20) DEFAULT NULL COMMENT '2级关系失效时间',
  `last_order_time` bigint(20) DEFAULT NULL COMMENT '最新下单时间',
  `lv1_invite_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上级邀请码',
  `province_id` int(10) DEFAULT NULL COMMENT '省Id',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city_id` int(10) DEFAULT NULL COMMENT '市Id',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `county_id` int(10) DEFAULT NULL COMMENT '区/县Id',
  `county` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区/县',
  `team_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '别名 如 第8团',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `operate_areas` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '运行区域',
  `wechat_groups_num` int(10) DEFAULT NULL COMMENT '微信群人数',
  `settle_mode` tinyint(4) DEFAULT NULL COMMENT '结算模式',
  `business_type` tinyint(4) DEFAULT NULL COMMENT '业务类型',
  `visit_channel` tinyint(4) DEFAULT NULL COMMENT '访问渠道',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_member_id` (`member_id`) USING BTREE,
  KEY `idx_invite_code` (`invite_code`) USING BTREE,
  KEY `idx_member_id_lv1` (`member_id_lv1`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13047 DEFAULT CHARSET=utf8 COMMENT='分销商(es_distribution)';

-- ----------------------------
-- Table structure for es_distribution_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_distribution_goods`;
CREATE TABLE `es_distribution_goods` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `goods_id` bigint(10) DEFAULT NULL COMMENT '商品id',
  `grade1_rebate` decimal(20,2) DEFAULT NULL COMMENT '一级返现',
  `grade2_rebate` decimal(20,2) DEFAULT NULL COMMENT '二级返现',
  `inviter_rate` decimal(20,2) DEFAULT '0.00' COMMENT '邀请佣金',
  `subsidy_rate` decimal(20,2) DEFAULT '-1.00' COMMENT '补贴比例',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6624 DEFAULT CHARSET=utf8 COMMENT='分销商品返现(es_distribution_goods)';

-- ----------------------------
-- Table structure for es_distribution_mission
-- ----------------------------
DROP TABLE IF EXISTS `es_distribution_mission`;
CREATE TABLE `es_distribution_mission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '团长任务ID',
  `mission_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长任务名称',
  `mission_subtitle` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长任务副标题',
  `mission_status` tinyint(4) DEFAULT NULL COMMENT '团长任务状态',
  `start_time` bigint(20) DEFAULT NULL COMMENT '开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '结束时间',
  `rewards_type` tinyint(4) DEFAULT NULL COMMENT '奖励类型',
  `seller_id` int(11) DEFAULT NULL COMMENT '店铺ID',
  `seller_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '店铺名称',
  `cover_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长任务活动封面',
  `background_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长活动背景图',
  `mission_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长任务描述',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `update_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_distribution_mission_detail
-- ----------------------------
DROP TABLE IF EXISTS `es_distribution_mission_detail`;
CREATE TABLE `es_distribution_mission_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '团长任务明细ID',
  `mission_id` int(11) DEFAULT NULL COMMENT '团长任务ID',
  `mission_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长任务名称',
  `mission_type` tinyint(4) DEFAULT NULL COMMENT '任务明细类型（0：每日任务，1：每月任务）',
  `mission_detail_status` tinyint(4) DEFAULT NULL COMMENT '任务详情状态（1:待上线,2：已上线，3：已下线）',
  `mission_target` tinyint(4) DEFAULT NULL COMMENT '任务明细指标（0：下单人数，1：成交金额）',
  `level1_target` int(11) DEFAULT NULL COMMENT '任务阶段1指标（人数或者金额）',
  `level1_cash` decimal(12,2) DEFAULT NULL COMMENT '任务阶段1奖励现金',
  `level1_coupon_id` int(11) DEFAULT NULL COMMENT '任务阶段1奖励现金券',
  `level2_target` int(11) DEFAULT NULL COMMENT '任务阶段2指标（人数或者金额）',
  `level2_cash` decimal(12,2) DEFAULT NULL COMMENT '任务阶段2奖励现金',
  `level2_coupon_id` int(11) DEFAULT NULL COMMENT '任务阶段2奖励现金券',
  `level3_target` int(11) DEFAULT NULL COMMENT '任务阶段3指标（人数或者金额）',
  `level3_cash` decimal(12,2) DEFAULT NULL COMMENT '任务阶段3奖励现金',
  `level3_coupon_id` int(11) DEFAULT NULL COMMENT '任务阶段3奖励现金券',
  `level4_target` int(11) DEFAULT NULL COMMENT '任务阶段4指标（人数或者金额）',
  `level4_cash` decimal(12,2) DEFAULT NULL COMMENT '任务阶段4奖励现金',
  `level4_coupon_id` int(11) DEFAULT NULL COMMENT '任务阶段4奖励现金券',
  `is_delete` tinyint(4) DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_distribution_mission_reward
-- ----------------------------
DROP TABLE IF EXISTS `es_distribution_mission_reward`;
CREATE TABLE `es_distribution_mission_reward` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '团长任务ID',
  `mission_type` tinyint(4) DEFAULT NULL COMMENT '任务明细类型（0：每日任务，1：每月任务）',
  `mission_time` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务期间',
  `mission_id` int(11) DEFAULT NULL COMMENT '团长任务id',
  `mission_detail_id` int(11) DEFAULT NULL COMMENT '任务详情id',
  `mission_schedule_id` int(11) DEFAULT NULL COMMENT '任务进度id',
  `rewards_type` tinyint(4) DEFAULT NULL COMMENT '奖励类型',
  `seller_id` int(11) DEFAULT NULL COMMENT '店铺ID',
  `seller_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '店铺名称',
  `reward_money` decimal(12,2) DEFAULT '0.00' COMMENT '奖励金额',
  `reward_status` tinyint(4) DEFAULT NULL,
  `invalid_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '失效原因',
  `member_id` int(11) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长名称',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_distribution_mission_schedule
-- ----------------------------
DROP TABLE IF EXISTS `es_distribution_mission_schedule`;
CREATE TABLE `es_distribution_mission_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '团长任务ID',
  `mission_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长任务名称',
  `mission_type` tinyint(4) DEFAULT NULL COMMENT '任务明细类型（0：每日任务，1：每月任务）',
  `mission_target` tinyint(4) DEFAULT NULL COMMENT '任务明细指标（0：下单人数，1：成交金额）',
  `mission_time` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务期间',
  `mission_id` int(11) DEFAULT NULL COMMENT '团长任务id',
  `mission_detail_id` int(11) DEFAULT NULL COMMENT '任务详情id',
  `seller_id` int(11) DEFAULT NULL COMMENT '店铺ID',
  `seller_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '店铺名称',
  `member_id` int(11) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长名称',
  `target_value` decimal(12,2) DEFAULT '0.00' COMMENT '目标值',
  `complete_value` decimal(12,2) DEFAULT '0.00' COMMENT '完成值',
  `total_reward` decimal(12,2) DEFAULT '0.00' COMMENT '累计奖励',
  `level1_status` tinyint(4) DEFAULT '0' COMMENT '阶段1状态',
  `level2_status` tinyint(4) DEFAULT '0' COMMENT '阶段2状态',
  `level3_status` tinyint(4) DEFAULT '0' COMMENT '阶段3状态',
  `level4_status` tinyint(4) DEFAULT '0' COMMENT '阶段3状态',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=284 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_distribution_order
-- ----------------------------
DROP TABLE IF EXISTS `es_distribution_order`;
CREATE TABLE `es_distribution_order` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_id` bigint(10) DEFAULT NULL COMMENT '订单id',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单sn',
  `buyer_member_id` bigint(20) DEFAULT NULL COMMENT '购买会员id',
  `buyer_member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '购买会员名称',
  `member_id_lv1` bigint(20) DEFAULT NULL COMMENT '一级分销商id',
  `member_id_lv2` bigint(20) DEFAULT NULL COMMENT '二级分销商id',
  `bill_id` bigint(20) DEFAULT NULL COMMENT '结算单id',
  `settle_cycle` bigint(20) DEFAULT NULL COMMENT '解冻日期',
  `create_time` bigint(20) DEFAULT NULL COMMENT '订单创建时间',
  `order_price` decimal(20,2) DEFAULT NULL COMMENT '订单金额',
  `grade1_rebate` decimal(20,2) DEFAULT NULL COMMENT '1级提成金额',
  `grade2_rebate` decimal(20,2) DEFAULT NULL COMMENT '2级提成金额',
  `grade1_sellback_price` decimal(20,2) DEFAULT NULL COMMENT '1级退款金额',
  `grade2_sellback_price` decimal(20,2) DEFAULT NULL COMMENT '2级退款金额',
  `is_return` smallint(1) DEFAULT NULL COMMENT '是否退货',
  `return_money` decimal(20,2) DEFAULT NULL COMMENT '退款金额',
  `is_withdraw` smallint(1) DEFAULT '0' COMMENT '是否已经提现',
  `seller_id` bigint(10) DEFAULT '1' COMMENT '店铺id',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'test' COMMENT '店铺名称',
  `lv1_point` decimal(10,2) DEFAULT '0.00' COMMENT '一级返点比例',
  `lv2_point` decimal(10,2) DEFAULT '0.00' COMMENT '二级返点比例',
  `inviter_member_id` bigint(11) DEFAULT NULL COMMENT '邀请人会员id',
  `invite_point` decimal(13,2) DEFAULT '0.00' COMMENT '邀请返点比例',
  `invite_rebate` decimal(13,2) DEFAULT '0.00' COMMENT '邀请佣金金额',
  `subsidy_member_id` bigint(11) DEFAULT NULL COMMENT '补贴人会员id',
  `subsidy_point` decimal(13,2) DEFAULT '0.00' COMMENT '补贴比例',
  `subsidy_rebate` decimal(13,2) DEFAULT '0.00' COMMENT '补贴金额',
  `goods_rebate` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '商品返现描述详细',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ind_order_id` (`order_id`) USING BTREE,
  KEY `ind_order_sn` (`order_sn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=138179 DEFAULT CHARSET=utf8 COMMENT='分销订单(es_distribution_order)';

-- ----------------------------
-- Table structure for es_distribution_relationship
-- ----------------------------
DROP TABLE IF EXISTS `es_distribution_relationship`;
CREATE TABLE `es_distribution_relationship` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `member_id` int(10) DEFAULT NULL COMMENT '修改上级团长的会员',
  `history_lv1_member_id` int(10) DEFAULT NULL COMMENT '历史上级团长',
  `member_lv1_id` int(10) DEFAULT NULL COMMENT '上级团长',
  `member_lv1_mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长电话',
  `operation_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作类型',
  `operation_mode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作方式 ',
  `operation_describe` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作描述',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36651 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_draft_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_draft_goods`;
CREATE TABLE `es_draft_goods` (
  `draft_goods_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '草稿商品id',
  `quantity` int(10) DEFAULT NULL COMMENT '商品总库存',
  `original` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '商品原始图片',
  `seller_id` int(10) DEFAULT NULL COMMENT '商品所属卖家ID',
  `shop_cat_id` int(10) DEFAULT NULL COMMENT '商品所属店铺类目ID',
  `template_id` int(10) DEFAULT NULL COMMENT '商品运费模板ID',
  `goods_transfee_charge` int(1) DEFAULT NULL COMMENT '是否为买家承担运费',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品所属店铺名称',
  `page_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'seo 标题',
  `meta_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'seo关键字',
  `meta_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'seo描述',
  `create_time` bigint(20) DEFAULT NULL COMMENT '商品添加时间',
  `have_spec` int(1) DEFAULT NULL COMMENT '是否开启规格',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `brand_id` int(10) DEFAULT NULL COMMENT '商品品牌ID',
  `category_id` int(10) DEFAULT NULL COMMENT '商品分类ID',
  `weight` decimal(20,2) DEFAULT NULL COMMENT '商品重量',
  `intro` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '商品详情',
  `price` decimal(20,2) DEFAULT NULL COMMENT '商品价格',
  `cost` decimal(20,2) DEFAULT NULL COMMENT '商品成本价',
  `mktprice` decimal(20,2) DEFAULT NULL COMMENT '商品市场价',
  `goods_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品类型',
  `exchange_money` decimal(10,2) DEFAULT NULL COMMENT '积分商品需要的金额',
  `exchange_point` int(10) DEFAULT NULL COMMENT '积分商品需要的积分',
  `exchange_category_id` int(10) DEFAULT NULL COMMENT '积分商品的分类id',
  `video_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频url',
  `selling` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '卖点',
  `supplier_id` int(10) DEFAULT NULL COMMENT '供应商id',
  `supplier_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '供应商姓名',
  `up_goods_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上游商品id',
  `is_local` tinyint(10) DEFAULT '0' COMMENT '本地商品 ',
  `is_global` tinyint(10) DEFAULT '0' COMMENT '全国商品',
  `is_self_take` tinyint(10) DEFAULT NULL COMMENT '是否自提',
  `freight_pricing_way` tinyint(10) DEFAULT NULL COMMENT '运费定价方式 1统一价 2模板',
  `freight_unified_price` decimal(20,2) DEFAULT '0.00' COMMENT '运费统一价',
  PRIMARY KEY (`draft_goods_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=384 DEFAULT CHARSET=utf8 COMMENT='草稿商品(es_draft_goods)';

-- ----------------------------
-- Table structure for es_draft_goods_params
-- ----------------------------
DROP TABLE IF EXISTS `es_draft_goods_params`;
CREATE TABLE `es_draft_goods_params` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `draft_goods_id` int(10) DEFAULT NULL COMMENT '草稿ID',
  `param_id` int(10) DEFAULT NULL COMMENT '参数ID',
  `param_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '参数名',
  `param_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '参数值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1154 DEFAULT CHARSET=utf8 COMMENT='草稿商品参数表(es_draft_goods_params)';

-- ----------------------------
-- Table structure for es_draft_goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `es_draft_goods_sku`;
CREATE TABLE `es_draft_goods_sku` (
  `draft_sku_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `draft_goods_id` int(10) DEFAULT NULL COMMENT '草稿id',
  `sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '货号',
  `quantity` int(10) DEFAULT NULL COMMENT '总库存',
  `price` decimal(20,2) DEFAULT NULL COMMENT '价格',
  `specs` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '规格',
  `cost` decimal(20,2) DEFAULT NULL COMMENT '成本',
  `weight` decimal(20,2) DEFAULT NULL COMMENT '重量',
  PRIMARY KEY (`draft_sku_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=666 DEFAULT CHARSET=utf8 COMMENT='草稿商品sku(es_draft_goods_sku)';

-- ----------------------------
-- Table structure for es_email
-- ----------------------------
DROP TABLE IF EXISTS `es_email`;
CREATE TABLE `es_email` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '邮件记录id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮件标题',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮件类型',
  `success` int(2) DEFAULT NULL COMMENT '是否成功',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮件接收者',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '邮件内容',
  `error_num` int(8) DEFAULT NULL COMMENT '错误次数',
  `last_send` bigint(12) DEFAULT NULL COMMENT '最后发送时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮件记录(es_email)';

-- ----------------------------
-- Table structure for es_exception_order
-- ----------------------------
DROP TABLE IF EXISTS `es_exception_order`;
CREATE TABLE `es_exception_order` (
  `exception_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '异常订单ID',
  `exception_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '异常单号',
  `order_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `exception_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '异常处理状态',
  `exception_source` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上报来源',
  `exception_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '异常类型',
  `exception_description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '异常描述',
  `solve_explain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '解决说明',
  `solve_types` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '解决方式拼接的字符串',
  `member_score` double(2,1) unsigned zerofill DEFAULT NULL COMMENT '客户评分',
  `member_comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '客户评价',
  `member_nickname` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '买家昵称',
  `member_mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '买家手机号',
  `member_id_lv1` int(11) DEFAULT NULL COMMENT '一级团长ID',
  `member_realname_lv1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '一级团长真实姓名',
  `member_mobile_lv1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '一级团长手机号码',
  `site_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '站点名',
  `site_leader_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '站长名称',
  `site_leader_mobile` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '站长手机号码',
  `site_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '自提点地址',
  `create_time` bigint(20) DEFAULT NULL COMMENT '上报时间',
  `create_id` int(11) DEFAULT NULL COMMENT '上报人ID',
  `create_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上报人',
  `recive_time` bigint(20) DEFAULT NULL COMMENT '接单时间',
  `recive_id` int(11) DEFAULT NULL COMMENT '接单人id',
  `recive_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '接单人',
  `process_time` bigint(20) DEFAULT NULL COMMENT '处理时间',
  `process_id` int(11) DEFAULT NULL COMMENT '处理人id',
  `process_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '处理人',
  `complete_time` bigint(20) DEFAULT NULL COMMENT '完成时间',
  `finish_time` bigint(20) DEFAULT NULL COMMENT '结单时间',
  `finish_id` int(11) DEFAULT NULL COMMENT '结单人id',
  `finish_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '结单人',
  PRIMARY KEY (`exception_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4238 DEFAULT CHARSET=utf8 COMMENT='异常订单表(es_exception_order)';

-- ----------------------------
-- Table structure for es_exchange
-- ----------------------------
DROP TABLE IF EXISTS `es_exchange`;
CREATE TABLE `es_exchange` (
  `exchange_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  `category_id` int(11) DEFAULT NULL COMMENT '商品所属积分分类',
  `enable_exchange` int(11) DEFAULT NULL COMMENT '是否允许兑换',
  `exchange_money` decimal(20,2) DEFAULT NULL COMMENT '兑换所需金额',
  `exchange_point` int(11) DEFAULT NULL COMMENT '兑换所需积分',
  `goods_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `goods_price` decimal(10,2) DEFAULT NULL COMMENT '商品原价',
  `goods_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品图片',
  PRIMARY KEY (`exchange_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分兑换(es_exchange)';

-- ----------------------------
-- Table structure for es_exchange_cat
-- ----------------------------
DROP TABLE IF EXISTS `es_exchange_cat`;
CREATE TABLE `es_exchange_cat` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类名称',
  `parent_id` int(11) DEFAULT NULL COMMENT '父分类',
  `category_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类id路径',
  `goods_count` int(11) DEFAULT NULL COMMENT '商品数量',
  `category_order` int(11) DEFAULT NULL COMMENT '分类排序',
  `list_show` int(10) DEFAULT NULL COMMENT '是否在页面上显示',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类图片',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='积分兑换分类(es_exchange_cat)';

-- ----------------------------
-- Table structure for es_express_platform
-- ----------------------------
DROP TABLE IF EXISTS `es_express_platform`;
CREATE TABLE `es_express_platform` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '快递平台id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '快递平台名称',
  `open` int(2) DEFAULT NULL COMMENT '是否开启快递平台,1开启，0未开启',
  `config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '快递平台配置',
  `bean` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '快递平台beanid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='快递平台(es_express_platform)';

-- ----------------------------
-- Table structure for es_focus_picture
-- ----------------------------
DROP TABLE IF EXISTS `es_focus_picture`;
CREATE TABLE `es_focus_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `pic_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片地址',
  `operation_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作类型',
  `operation_param` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作参数',
  `operation_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作地址',
  `client_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '客户端类型 pc:pc楼层mobile:移动端楼层',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='焦点图(es_focus_picture)';

-- ----------------------------
-- Table structure for es_full_discount
-- ----------------------------
DROP TABLE IF EXISTS `es_full_discount`;
CREATE TABLE `es_full_discount` (
  `fd_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '活动id',
  `full_money` decimal(20,2) DEFAULT NULL COMMENT '优惠门槛金额',
  `full_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '优惠门槛类型',
  `minus_value` decimal(20,2) DEFAULT NULL COMMENT '减现金',
  `point_value` int(10) DEFAULT NULL COMMENT '送积分',
  `is_full_minus` int(1) DEFAULT NULL COMMENT '活动是否减现金',
  `is_free_ship` int(1) DEFAULT NULL COMMENT '是否免邮',
  `is_send_point` int(1) DEFAULT NULL COMMENT '是否送积分',
  `is_send_gift` int(1) DEFAULT NULL COMMENT '是否有赠品',
  `is_send_bonus` int(1) DEFAULT NULL COMMENT '是否增优惠券',
  `gift_id` int(10) DEFAULT NULL COMMENT '赠品id',
  `bonus_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '优惠券id',
  `is_discount` int(1) DEFAULT NULL COMMENT '是否打折',
  `discount_value` decimal(20,2) DEFAULT NULL COMMENT '打多少折',
  `start_time` bigint(20) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '活动结束时间',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动标题',
  `range_type` int(1) DEFAULT NULL COMMENT '是否全部商品参与',
  `disabled` int(1) DEFAULT NULL COMMENT '是否停用',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '活动说明',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  PRIMARY KEY (`fd_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='满优惠活动(es_full_discount)';

-- ----------------------------
-- Table structure for es_full_discount_gift
-- ----------------------------
DROP TABLE IF EXISTS `es_full_discount_gift`;
CREATE TABLE `es_full_discount_gift` (
  `gift_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '赠品id',
  `gift_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '赠品名称',
  `gift_price` decimal(20,2) DEFAULT NULL COMMENT '赠品金额',
  `gift_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '赠品图片',
  `gift_type` int(1) DEFAULT NULL COMMENT '赠品类型',
  `actual_store` int(10) DEFAULT NULL COMMENT '库存',
  `enable_store` int(10) DEFAULT NULL COMMENT '可用库存',
  `create_time` bigint(20) DEFAULT NULL COMMENT '活动时间',
  `goods_id` int(10) DEFAULT NULL COMMENT '活动商品id',
  `disabled` int(1) DEFAULT NULL COMMENT '是否禁用',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  PRIMARY KEY (`gift_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='满优惠赠品(es_full_discount_gift)';

-- ----------------------------
-- Table structure for es_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_goods`;
CREATE TABLE `es_goods` (
  `goods_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品编号',
  `brand_id` int(10) DEFAULT NULL COMMENT '品牌id',
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `goods_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品类型normal普通point积分',
  `weight` decimal(10,2) DEFAULT NULL COMMENT '重量',
  `market_enable` int(1) DEFAULT NULL COMMENT '上架状态 1上架  0下架',
  `intro` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '详情',
  `price` decimal(20,2) DEFAULT NULL COMMENT '商品价格',
  `cost` decimal(20,2) DEFAULT NULL COMMENT '成本价格',
  `mktprice` decimal(20,2) DEFAULT NULL COMMENT '市场价格',
  `have_spec` int(1) DEFAULT NULL COMMENT '是否有规格0没有 1有',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `last_modify` bigint(20) DEFAULT NULL COMMENT '最后修改时间',
  `view_count` int(10) DEFAULT NULL COMMENT '浏览数量',
  `buy_count` int(10) DEFAULT NULL COMMENT '购买数量',
  `disabled` int(10) DEFAULT NULL COMMENT '是否被删除0 删除 1未删除',
  `quantity` int(10) DEFAULT NULL COMMENT '库存',
  `enable_quantity` int(10) DEFAULT NULL COMMENT '可用库存',
  `point` int(10) DEFAULT NULL COMMENT '如果是积分商品需要使用的积分',
  `page_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'seo标题',
  `meta_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'seo关键字',
  `meta_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'seo描述',
  `grade` decimal(20,2) DEFAULT NULL COMMENT '商品好评率',
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '缩略图路径',
  `big` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '大图路径',
  `small` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '小图路径',
  `original` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原图路径',
  `seller_id` int(10) DEFAULT NULL COMMENT '卖家id',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '卖家名字',
  `local_template_id` int(10) DEFAULT NULL COMMENT '同城配送模板id',
  `shop_cat_id` int(10) DEFAULT NULL COMMENT '店铺分类id',
  `comment_num` int(10) DEFAULT NULL COMMENT '评论数量',
  `template_id` int(10) DEFAULT NULL COMMENT '运费模板id',
  `goods_transfee_charge` int(1) DEFAULT NULL COMMENT '谁承担运费0：买家承担，1：卖家承担',
  `is_auth` int(1) DEFAULT NULL COMMENT '0 需要审核 并且待审核，1 不需要审核 2需要审核 且审核通过 3 需要审核 且审核未通过',
  `auth_message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核信息',
  `self_operated` int(1) DEFAULT NULL COMMENT '是否是自营商品 0 不是 1是',
  `under_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '下架原因',
  `priority` int(1) DEFAULT '1' COMMENT '优先级:高(3)、中(2)、低(1)',
  `is_local` tinyint(10) DEFAULT '0' COMMENT '本地商品 ',
  `is_global` tinyint(10) DEFAULT '0' COMMENT '全国商品',
  `is_self_take` tinyint(10) DEFAULT NULL COMMENT '是否自提',
  `freight_pricing_way` tinyint(10) DEFAULT NULL COMMENT '运费定价方式 1统一价 2模板',
  `freight_unified_price` decimal(20,2) DEFAULT '0.00' COMMENT '运费统一价',
  `video_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频url',
  `selling` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '卖点',
  `supplier_id` int(10) DEFAULT NULL COMMENT '供应商id',
  `supplier_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '供应商姓名',
  `up_goods_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上游商品id',
  `shop_cat_items` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺多分组json',
  `store` int(11) DEFAULT '0' COMMENT '贮藏要求',
  `pre_sort` tinyint(4) DEFAULT '0' COMMENT '预分拣',
  `expiry_day` int(11) DEFAULT NULL COMMENT '虚拟商品自购买日后几天可用',
  `available_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '虚拟商品的可用日期',
  PRIMARY KEY (`goods_id`) USING BTREE,
  KEY `ind_goods_category_id` (`category_id`) USING BTREE,
  KEY `ind_goods_brand_id` (`brand_id`) USING BTREE,
  KEY `ind_goods_name` (`goods_name`) USING BTREE,
  KEY `ind_goods_sn` (`sn`) USING BTREE,
  KEY `ind_goods_other` (`goods_type`,`market_enable`,`disabled`) USING BTREE,
  KEY `idx_supplier_name` (`supplier_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8244 DEFAULT CHARSET=utf8 COMMENT='商品(es_goods)';

-- ----------------------------
-- Table structure for es_goods_gallery
-- ----------------------------
DROP TABLE IF EXISTS `es_goods_gallery`;
CREATE TABLE `es_goods_gallery` (
  `img_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品主键',
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '缩略图路径',
  `small` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '小图路径',
  `big` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '大图路径',
  `original` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原图路径',
  `tiny` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '极小图路径',
  `isdefault` int(1) DEFAULT NULL COMMENT '是否是默认图片1   0没有默认',
  `sort` int(2) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`img_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27584 DEFAULT CHARSET=utf8 COMMENT='商品相册(es_goods_gallery)';

-- ----------------------------
-- Table structure for es_goods_params
-- ----------------------------
DROP TABLE IF EXISTS `es_goods_params`;
CREATE TABLE `es_goods_params` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `param_id` int(10) DEFAULT NULL COMMENT '参数id',
  `param_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '参数名字',
  `param_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '参数值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41326 DEFAULT CHARSET=utf8 COMMENT='商品关联参数值(es_goods_params)';

-- ----------------------------
-- Table structure for es_goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `es_goods_sku`;
CREATE TABLE `es_goods_sku` (
  `sku_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品编号',
  `quantity` int(10) DEFAULT NULL COMMENT '库存',
  `enable_quantity` int(10) DEFAULT NULL COMMENT '可用库存',
  `price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `specs` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '规格信息json',
  `cost` decimal(10,2) DEFAULT NULL COMMENT '成本价格',
  `weight` decimal(10,2) DEFAULT NULL COMMENT '重量',
  `seller_id` int(10) DEFAULT NULL COMMENT '卖家id',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '卖家名称',
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '缩略图',
  `hash_code` int(10) NOT NULL COMMENT '标识规格唯一性',
  `up_sku_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '上游商品Sku',
  `mktprice` decimal(20,2) DEFAULT NULL COMMENT '市场价格',
  PRIMARY KEY (`sku_id`) USING BTREE,
  KEY `idx_goods_id` (`goods_id`) USING BTREE,
  KEY `idx_category_id` (`category_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38256 DEFAULT CHARSET=utf8 COMMENT='商品sku(es_goods_sku)';

-- ----------------------------
-- Table structure for es_goods_snapshot
-- ----------------------------
DROP TABLE IF EXISTS `es_goods_snapshot`;
CREATE TABLE `es_goods_snapshot` (
  `snapshot_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品编号',
  `brand_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '品牌名称',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类名称',
  `goods_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品类型',
  `weight` decimal(10,2) DEFAULT NULL COMMENT '重量',
  `intro` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '商品详情',
  `price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `cost` decimal(10,2) DEFAULT NULL COMMENT '商品成本价',
  `mktprice` decimal(10,2) DEFAULT NULL COMMENT '商品市场价',
  `have_spec` smallint(1) DEFAULT NULL COMMENT '商品是否开启规格1 开启 0 未开启',
  `params_json` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '参数json',
  `img_json` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '图片json',
  `create_time` bigint(20) DEFAULT NULL COMMENT '快照时间',
  `point` int(10) DEFAULT NULL COMMENT '商品使用积分',
  `seller_id` int(10) DEFAULT NULL COMMENT '所属卖家',
  `promotion_json` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '促销json值',
  `coupon_json` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '优惠券json值',
  PRIMARY KEY (`snapshot_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=190050 DEFAULT CHARSET=utf8 COMMENT='交易快照(es_goods_snapshot)';

-- ----------------------------
-- Table structure for es_goods_words
-- ----------------------------
DROP TABLE IF EXISTS `es_goods_words`;
CREATE TABLE `es_goods_words` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `words` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分词',
  `goods_num` bigint(20) DEFAULT NULL COMMENT '数量',
  `quanpin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '全拼字母',
  `szm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '首字母',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'SYSTEM' COMMENT '类型，系统(SYSTEM)，平台(PLATFORM)',
  `sort` int(10) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11260 DEFAULT CHARSET=utf8 COMMENT='商品分词表(es_goods_words)';

-- ----------------------------
-- Table structure for es_groupbuy_active
-- ----------------------------
DROP TABLE IF EXISTS `es_groupbuy_active`;
CREATE TABLE `es_groupbuy_active` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '活动Id',
  `act_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动名称',
  `start_time` bigint(20) DEFAULT NULL COMMENT '活动开启时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '团购结束时间',
  `join_end_time` bigint(20) DEFAULT NULL COMMENT '团购报名截止时间',
  `add_time` bigint(20) DEFAULT NULL COMMENT '团购添加时间',
  `act_tag_id` int(10) DEFAULT NULL COMMENT '团购活动标签Id',
  `goods_num` int(10) DEFAULT NULL COMMENT '参与团购商品数量',
  PRIMARY KEY (`act_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='团购活动表(es_groupbuy_active)';

-- ----------------------------
-- Table structure for es_groupbuy_area
-- ----------------------------
DROP TABLE IF EXISTS `es_groupbuy_area`;
CREATE TABLE `es_groupbuy_area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '地区ID',
  `area_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地区名称',
  `area_order` int(11) DEFAULT NULL COMMENT '地区排序',
  `area_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地区路径结构',
  `parent_id` int(11) DEFAULT NULL COMMENT '地区父节点',
  PRIMARY KEY (`area_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='团购地区(es_groupbuy_area)';

-- ----------------------------
-- Table structure for es_groupbuy_cat
-- ----------------------------
DROP TABLE IF EXISTS `es_groupbuy_cat`;
CREATE TABLE `es_groupbuy_cat` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `parent_id` int(8) DEFAULT NULL COMMENT '父类id',
  `cat_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类名称',
  `cat_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类结构目录',
  `cat_order` int(8) DEFAULT NULL COMMENT '分类排序',
  PRIMARY KEY (`cat_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='团购分类(es_groupbuy_cat)';

-- ----------------------------
-- Table structure for es_groupbuy_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_groupbuy_goods`;
CREATE TABLE `es_groupbuy_goods` (
  `gb_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '团购商品Id',
  `sku_id` int(11) DEFAULT NULL COMMENT '货品Id',
  `act_id` int(11) DEFAULT NULL COMMENT '活动Id',
  `cat_id` int(11) DEFAULT NULL COMMENT 'cat_id',
  `area_id` int(11) DEFAULT NULL COMMENT '地区Id',
  `gb_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '团购名称',
  `gb_title` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '副标题',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品Id',
  `original_price` decimal(20,2) DEFAULT NULL COMMENT '原始价格',
  `price` decimal(20,2) DEFAULT NULL COMMENT '团购价格',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片地址',
  `goods_num` int(11) DEFAULT NULL COMMENT '商品总数',
  `visual_num` int(11) DEFAULT NULL COMMENT '虚拟数量',
  `limit_num` int(11) DEFAULT NULL COMMENT '限购数量',
  `buy_num` int(11) DEFAULT NULL COMMENT '已团购数量',
  `view_num` int(11) DEFAULT NULL COMMENT '浏览数量',
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '介绍',
  `gb_status` int(1) DEFAULT NULL COMMENT '状态',
  `add_time` bigint(20) DEFAULT NULL COMMENT '添加时间',
  `wap_thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'wap缩略图',
  `thumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'pc缩略图',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家ID',
  `seller_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺名称',
  PRIMARY KEY (`gb_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='团购商品(es_groupbuy_goods)';

-- ----------------------------
-- Table structure for es_groupbuy_quantity_log
-- ----------------------------
DROP TABLE IF EXISTS `es_groupbuy_quantity_log`;
CREATE TABLE `es_groupbuy_quantity_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `order_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品ID',
  `quantity` int(10) DEFAULT NULL COMMENT '团购售空数量',
  `op_time` bigint(20) DEFAULT NULL COMMENT '操作时间',
  `log_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志类型',
  `reason` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作原因',
  `gb_id` int(10) DEFAULT NULL COMMENT '团购商品id',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='团购商品库存日志表(es_groupbuy_quantity_log)';

-- ----------------------------
-- Table structure for es_half_price
-- ----------------------------
DROP TABLE IF EXISTS `es_half_price`;
CREATE TABLE `es_half_price` (
  `hp_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '第二件半价活动ID',
  `start_time` bigint(20) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '活动结束时间',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动标题',
  `range_type` int(1) DEFAULT NULL COMMENT '是否全部商品参与',
  `disabled` int(1) DEFAULT NULL COMMENT '是否停用',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '活动说明',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  PRIMARY KEY (`hp_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='第二件半价(es_half_price)';

-- ----------------------------
-- Table structure for es_history
-- ----------------------------
DROP TABLE IF EXISTS `es_history`;
CREATE TABLE `es_history` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品名称',
  `goods_price` decimal(20,2) DEFAULT NULL COMMENT '商品价格',
  `goods_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品主图',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '会员名称',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间，按天存',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=388238 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for es_hot_keyword
-- ----------------------------
DROP TABLE IF EXISTS `es_hot_keyword`;
CREATE TABLE `es_hot_keyword` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `hot_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文字内容',
  `sort` int(5) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='热门关键字(es_hot_keyword)';

-- ----------------------------
-- Table structure for es_keyword_search_history
-- ----------------------------
DROP TABLE IF EXISTS `es_keyword_search_history`;
CREATE TABLE `es_keyword_search_history` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '关键字',
  `count` int(10) DEFAULT '0' COMMENT '搜索次数',
  `add_time` bigint(20) DEFAULT NULL COMMENT '新增时间',
  `modify_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7063 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_leader
-- ----------------------------
DROP TABLE IF EXISTS `es_leader`;
CREATE TABLE `es_leader` (
  `leader_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '团长id',
  `leader_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '团长编码',
  `seller_id` int(10) DEFAULT NULL COMMENT '卖家id',
  `member_id` int(10) DEFAULT NULL COMMENT '会员uuid',
  `leader_mobile` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '团长手机',
  `leader_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '团长名',
  `leader_type` tinyint(4) DEFAULT NULL COMMENT '团长类型',
  `facade_pic_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '门面图片',
  `fans_count` int(10) DEFAULT '0' COMMENT '粉丝数',
  `street` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细信息',
  `doorplate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '门牌号',
  `source_from` int(10) DEFAULT '0' COMMENT '注册来源',
  `cell_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '小区名称',
  `site_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '站点名',
  `origin_site_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原站点名称',
  `site_type` tinyint(4) DEFAULT NULL COMMENT '站点类型',
  `op_status` int(2) DEFAULT NULL COMMENT '营业状态',
  `status` tinyint(3) DEFAULT NULL COMMENT '团长状态',
  `audit_status` tinyint(4) DEFAULT '0' COMMENT '审核状态',
  `join_reason` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '加入原因',
  `audit_time` bigint(20) DEFAULT NULL COMMENT '审核时间',
  `add_time` bigint(20) DEFAULT NULL COMMENT '增加时间',
  `audit_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核备注',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  `province_id` int(10) DEFAULT NULL COMMENT '店铺所在省id',
  `city_id` int(10) DEFAULT NULL COMMENT '店铺所在市id',
  `county_id` int(10) DEFAULT NULL COMMENT '店铺所在县id',
  `town_id` int(10) DEFAULT NULL COMMENT '店铺所在镇id',
  `province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在省',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在市',
  `county` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在县',
  `town` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在镇',
  `district` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区 待删除',
  `lat` double(10,6) DEFAULT NULL COMMENT '纬度',
  `lng` double(10,6) DEFAULT NULL COMMENT '经度',
  `work_time` varchar(0) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '工作时间',
  `creater_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `operate_areas` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '运营区域',
  `ship_priority` int(10) DEFAULT NULL COMMENT '配送优先级',
  `dispatch_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配送发货类型 B  ， B+C',
  PRIMARY KEY (`leader_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=298 DEFAULT CHARSET=utf8 COMMENT='团长信息';

-- ----------------------------
-- Table structure for es_leader_fans
-- ----------------------------
DROP TABLE IF EXISTS `es_leader_fans`;
CREATE TABLE `es_leader_fans` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `leader_id` int(10) DEFAULT NULL COMMENT '团长id',
  `buyer_id` int(10) DEFAULT NULL COMMENT '会员uid',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10631 DEFAULT CHARSET=utf8 COMMENT='团长粉丝';

-- ----------------------------
-- Table structure for es_logi_company
-- ----------------------------
DROP TABLE IF EXISTS `es_logi_company`;
CREATE TABLE `es_logi_company` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '物流公司id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '物流公司名称',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '物流公司code',
  `kdcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '快递鸟物流公司code',
  `is_waybill` int(10) DEFAULT NULL COMMENT '是否支持电子面单1：支持 0：不支持',
  `customer_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '物流公司客户号',
  `customer_pwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '物流公司电子面单密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='物流公司(es_logi_company)';

-- ----------------------------
-- Table structure for es_logistics_company
-- ----------------------------
DROP TABLE IF EXISTS `es_logistics_company`;
CREATE TABLE `es_logistics_company` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `is_waybill` int(1) DEFAULT NULL,
  `kdcode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `form_items` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_luck
-- ----------------------------
DROP TABLE IF EXISTS `es_luck`;
CREATE TABLE `es_luck` (
  `luck_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '抽奖活动id',
  `luck_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动名称',
  `luck_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动标题',
  `start_time` bigint(20) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '活动结束时间',
  `min_limit_price` decimal(12,2) DEFAULT '0.00' COMMENT '抽奖门槛',
  `day_max_times` int(11) DEFAULT NULL COMMENT '每日最大抽奖次数',
  `luck_desc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '抽奖活动描述',
  `luck_status` tinyint(4) DEFAULT NULL COMMENT '活动状态',
  `seller_id` bigint(20) DEFAULT NULL COMMENT '商家ID',
  `seller_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商家名称',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人姓名',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
  `update_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`luck_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_luck_prize
-- ----------------------------
DROP TABLE IF EXISTS `es_luck_prize`;
CREATE TABLE `es_luck_prize` (
  `luck_prize_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '抽奖奖品id',
  `luck_id` int(11) DEFAULT NULL COMMENT '抽奖活动id',
  `prize_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '奖品名称',
  `probability` decimal(12,9) DEFAULT '0.000000000' COMMENT '中奖概率',
  `prize_num` int(11) DEFAULT NULL COMMENT '奖品个数',
  `prize_type` tinyint(4) DEFAULT NULL COMMENT '奖品类型',
  `prize_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '奖品图片url',
  `delivery_type` tinyint(4) DEFAULT NULL COMMENT '出库类型',
  `coupon_id` int(11) DEFAULT NULL COMMENT '优惠券id',
  `seller_id` bigint(20) DEFAULT NULL COMMENT '商家ID',
  `seller_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商家名称',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  `remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`luck_prize_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_luck_record
-- ----------------------------
DROP TABLE IF EXISTS `es_luck_record`;
CREATE TABLE `es_luck_record` (
  `luck_record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '抽奖记录id',
  `luck_id` int(11) DEFAULT NULL COMMENT '抽奖活动id',
  `member_id` int(11) DEFAULT NULL COMMENT '会员id',
  `member_nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员昵称',
  `member_real_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员真实姓名',
  `member_face` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员头像',
  `luck_prize_id` int(11) DEFAULT NULL COMMENT '抽奖奖品id',
  `prize_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '奖品名称',
  `prize_type` tinyint(4) DEFAULT NULL COMMENT '奖品类型',
  `prize_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '奖品图片url',
  `win_prize_num` int(11) DEFAULT NULL COMMENT '中奖奖品个数',
  `is_hit` tinyint(4) DEFAULT NULL COMMENT '是否中奖',
  `delivery_type` tinyint(4) DEFAULT NULL COMMENT '出库类型',
  `exchange_type` tinyint(4) DEFAULT NULL COMMENT '兑奖方式',
  `exchange_status` tinyint(4) DEFAULT NULL COMMENT '兑奖状态',
  `exchange_time` bigint(20) DEFAULT NULL COMMENT '兑奖时间',
  `seller_id` bigint(20) DEFAULT NULL COMMENT '商家ID',
  `seller_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商家名称',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`luck_record_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2525 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_member
-- ----------------------------
DROP TABLE IF EXISTS `es_member`;
CREATE TABLE `es_member` (
  `member_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '会员ID',
  `real_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '真实姓名',
  `uname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员登陆用户名',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员登陆密码',
  `create_time` bigint(20) DEFAULT NULL COMMENT '会员注册时间',
  `sex` int(1) DEFAULT NULL COMMENT '会员性别',
  `birthday` bigint(20) DEFAULT NULL COMMENT '会员生日',
  `province_id` int(10) DEFAULT NULL COMMENT '所属省份ID',
  `city_id` int(10) DEFAULT NULL COMMENT '所属城市ID',
  `county_id` int(10) DEFAULT NULL COMMENT '所属县(区)ID',
  `town_id` int(10) DEFAULT NULL COMMENT '所属城镇ID',
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属省份名称',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属城市名称',
  `county` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属县(区)名称',
  `town` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属城镇名称',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `lat` double(10,6) DEFAULT NULL COMMENT '位置经度',
  `lng` double(10,6) DEFAULT NULL COMMENT '位置纬度',
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号码',
  `tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '座机号码',
  `grade_point` bigint(20) DEFAULT NULL COMMENT '等级积分',
  `msn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员MSN',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员备注',
  `last_login` bigint(20) DEFAULT NULL COMMENT '上次登陆时间',
  `login_count` int(10) DEFAULT NULL COMMENT '登陆次数',
  `is_cheked` int(1) DEFAULT NULL COMMENT '邮件是否已验证',
  `register_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '注册IP地址',
  `recommend_point_state` int(10) DEFAULT NULL COMMENT '是否已经完成了推荐积分',
  `info_full` int(1) DEFAULT NULL COMMENT '会员信息是否完善',
  `find_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'find_code',
  `face` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员头像',
  `midentity` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '身份证号',
  `disabled` int(8) DEFAULT NULL COMMENT '会员状态',
  `shop_id` int(10) DEFAULT NULL COMMENT '店铺ID',
  `have_shop` int(1) DEFAULT NULL COMMENT '是否开通店铺',
  `consum_point` bigint(20) DEFAULT NULL COMMENT '消费积分',
  `nickname` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '昵称',
  `account_member_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '账户会员id',
  PRIMARY KEY (`member_id`) USING BTREE,
  KEY `ind_member_uname` (`uname`(191),`email`) USING BTREE,
  KEY `ind_member_b2b2c` (`shop_id`,`have_shop`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13046 DEFAULT CHARSET=utf8 COMMENT='会员表(es_member)';

-- ----------------------------
-- Table structure for es_member_address
-- ----------------------------
DROP TABLE IF EXISTS `es_member_address`;
CREATE TABLE `es_member_address` (
  `addr_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货人姓名',
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货人国籍',
  `province_id` int(10) DEFAULT NULL COMMENT '所属省份ID',
  `city_id` int(10) DEFAULT NULL COMMENT '所属城市ID',
  `county_id` int(10) DEFAULT NULL COMMENT '所属县(区)ID',
  `town_id` int(10) DEFAULT NULL COMMENT '所属城镇ID',
  `county` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属县(区)名称',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属城市名称',
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属省份名称',
  `town` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属城镇名称',
  `addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系电话(一般指座机)',
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号码',
  `def_addr` int(1) DEFAULT NULL COMMENT '是否为默认收货地址',
  `ship_address_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地址别名',
  `ship_lng` double(13,10) DEFAULT NULL COMMENT '地址经度',
  `ship_lat` double(13,10) DEFAULT NULL COMMENT '地址纬度',
  PRIMARY KEY (`addr_id`) USING BTREE,
  KEY `ind_mem_addr` (`member_id`,`def_addr`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5014 DEFAULT CHARSET=utf8 COMMENT='会员收货地址表(es_member_address)';

-- ----------------------------
-- Table structure for es_member_ask
-- ----------------------------
DROP TABLE IF EXISTS `es_member_ask`;
CREATE TABLE `es_member_ask` (
  `ask_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '咨询内容',
  `create_time` bigint(20) DEFAULT NULL COMMENT '咨询时间',
  `seller_id` int(10) DEFAULT NULL COMMENT '卖家id',
  `reply` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '回复内容',
  `reply_time` bigint(20) DEFAULT NULL COMMENT '回复时间',
  `reply_status` smallint(1) DEFAULT NULL COMMENT '回复状态 1 已回复 2 未回复',
  `status` smallint(1) DEFAULT NULL COMMENT '状态 ',
  `member_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '卖家名称',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `member_face` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员头像',
  `auth_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'WAIT_AUDIT',
  PRIMARY KEY (`ask_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='咨询(es_member_ask)';

-- ----------------------------
-- Table structure for es_member_collection_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_member_collection_goods`;
CREATE TABLE `es_member_collection_goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `goods_id` int(10) DEFAULT NULL COMMENT '收藏商品ID',
  `create_time` bigint(20) DEFAULT NULL COMMENT '收藏商品时间',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `goods_price` decimal(20,2) DEFAULT NULL COMMENT '商品价格',
  `goods_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品编号',
  `goods_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品图片',
  `shop_id` int(8) DEFAULT NULL COMMENT '店铺id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=639 DEFAULT CHARSET=utf8 COMMENT='会员商品收藏表(es_member_collection_goods)';

-- ----------------------------
-- Table structure for es_member_collection_shop
-- ----------------------------
DROP TABLE IF EXISTS `es_member_collection_shop`;
CREATE TABLE `es_member_collection_shop` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '收藏id',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `shop_id` int(8) DEFAULT NULL COMMENT '店铺id',
  `shop_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺名称',
  `create_time` bigint(20) DEFAULT NULL COMMENT '收藏时间',
  `logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺logo',
  `shop_province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在省',
  `shop_city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在市',
  `shop_region` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在县',
  `shop_town` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在镇',
  `shop_praise_rate` decimal(20,2) DEFAULT NULL COMMENT '店铺好评率',
  `shop_description_credit` decimal(20,2) DEFAULT NULL COMMENT '店铺描述相符度',
  `shop_service_credit` decimal(20,2) DEFAULT NULL COMMENT '服务态度分数',
  `shop_delivery_credit` decimal(20,2) DEFAULT NULL COMMENT '发货速度分数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2613 DEFAULT CHARSET=utf8 COMMENT='会员收藏店铺表(es_member_collection_shop)';

-- ----------------------------
-- Table structure for es_member_comment
-- ----------------------------
DROP TABLE IF EXISTS `es_member_comment`;
CREATE TABLE `es_member_comment` (
  `comment_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '评论主键',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `sku_id` int(10) DEFAULT NULL COMMENT 'skuid',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `seller_id` int(10) DEFAULT NULL COMMENT '卖家id',
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `member_face` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员头像',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '评论内容',
  `create_time` bigint(20) DEFAULT NULL COMMENT '评论时间',
  `have_image` smallint(1) DEFAULT NULL COMMENT '是否有图片 1 有 0 没有',
  `status` smallint(1) DEFAULT NULL COMMENT '状态  1 正常 0 删除 ',
  `grade` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '好中差评',
  `order_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `reply_status` smallint(1) DEFAULT NULL COMMENT '是否回复 1 是 0 否',
  `audit_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'WAIT_AUDIT' COMMENT '初评审核:待审核(WAIT_AUDIT),审核通过(PASS_AUDIT),审核拒绝(REFUSE_AUDIT)',
  `comments_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'INITIAL' COMMENT '评论类型：初评(INITIAL),追评(ADDITIONAL)',
  `parent_id` int(10) DEFAULT '0' COMMENT '初评id，默认为0',
  `audit_remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核备注',
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=134211 DEFAULT CHARSET=utf8 COMMENT='评论(es_member_comment)';

-- ----------------------------
-- Table structure for es_member_coupon
-- ----------------------------
DROP TABLE IF EXISTS `es_member_coupon`;
CREATE TABLE `es_member_coupon` (
  `mc_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `coupon_id` int(10) DEFAULT NULL COMMENT '优惠券表主键',
  `member_id` int(10) DEFAULT NULL COMMENT '会员主键id',
  `used_time` bigint(11) DEFAULT NULL COMMENT '使用时间',
  `create_time` bigint(11) DEFAULT NULL COMMENT '领取时间',
  `order_id` int(10) DEFAULT NULL COMMENT '订单主键',
  `order_sn` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `member_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员昵称',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '优惠券名称',
  `coupon_price` decimal(10,2) DEFAULT NULL COMMENT '优惠券面额',
  `coupon_threshold_price` decimal(10,2) DEFAULT NULL COMMENT '优惠券门槛金额',
  `start_time` bigint(11) DEFAULT NULL COMMENT '使用起始时间',
  `end_time` bigint(11) DEFAULT NULL COMMENT '使用截止时间',
  `used_status` smallint(1) DEFAULT NULL COMMENT '使用状态',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家ID',
  `seller_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `receive_order` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '接收订单编号',
  `coupon_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '优惠卷类型',
  PRIMARY KEY (`mc_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32055 DEFAULT CHARSET=utf8 COMMENT='会员优惠券(es_member_coupon)';

-- ----------------------------
-- Table structure for es_member_inviter
-- ----------------------------
DROP TABLE IF EXISTS `es_member_inviter`;
CREATE TABLE `es_member_inviter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL COMMENT '会员ID',
  `inviter_member_id` int(11) DEFAULT NULL COMMENT '邀请人会员ID',
  `invite_time` datetime DEFAULT NULL COMMENT '邀请时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10864 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_member_notice_log
-- ----------------------------
DROP TABLE IF EXISTS `es_member_notice_log`;
CREATE TABLE `es_member_notice_log` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '会员历史消息id',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '站内信内容',
  `send_time` bigint(20) DEFAULT NULL COMMENT '发送时间',
  `is_del` int(2) DEFAULT NULL COMMENT '是否删除，0删除，1没有删除',
  `is_read` int(2) DEFAULT NULL COMMENT '是否已读，0未读，1已读',
  `receive_time` bigint(20) DEFAULT NULL COMMENT '接收时间',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=506489 DEFAULT CHARSET=utf8 COMMENT='会员站内消息历史(es_member_notice_log)';

-- ----------------------------
-- Table structure for es_member_order
-- ----------------------------
DROP TABLE IF EXISTS `es_member_order`;
CREATE TABLE `es_member_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `order_new` int(10) DEFAULT '0' COMMENT '待付款',
  `order_error` int(10) DEFAULT '0' COMMENT '出库失败',
  `order_confirm` int(10) DEFAULT '0' COMMENT '已确认',
  `order_formed` int(10) DEFAULT '0' COMMENT '已成团',
  `order_paid_off` int(10) DEFAULT '0' COMMENT '待发货',
  `order_shipped` int(10) DEFAULT '0' COMMENT '已发货',
  `order_rog` int(10) DEFAULT '0' COMMENT '已收货',
  `order_completed` int(10) DEFAULT '0' COMMENT '已完成',
  `order_cancelled` int(10) DEFAULT '0' COMMENT '已取消',
  `order_after_service` int(10) DEFAULT '0' COMMENT '售后中',
  `order_comment` int(10) DEFAULT '0' COMMENT '待评论',
  `cart_goods_count` int(10) DEFAULT '0' COMMENT '购物车商品数量',
  `create_time` bigint(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9507 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_member_point_history
-- ----------------------------
DROP TABLE IF EXISTS `es_member_point_history`;
CREATE TABLE `es_member_point_history` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `grade_point` bigint(20) DEFAULT NULL COMMENT '等级积分',
  `time` bigint(20) DEFAULT NULL COMMENT '操作时间',
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作理由',
  `grade_point_type` int(2) DEFAULT NULL COMMENT '等级积分类型',
  `operator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作者',
  `consum_point` bigint(20) DEFAULT NULL COMMENT '消费积分',
  `consum_point_type` int(2) DEFAULT NULL COMMENT '消费积分类型',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ind_ponit_history` (`member_id`,`grade_point_type`,`consum_point_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=333549 DEFAULT CHARSET=utf8 COMMENT='会员积分表(es_member_point_history)';

-- ----------------------------
-- Table structure for es_member_pv
-- ----------------------------
DROP TABLE IF EXISTS `es_member_pv`;
CREATE TABLE `es_member_pv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `member_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员名称',
  `visit_day` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员访问时间 天',
  `visit_count` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '访问次数',
  `distributor_id` int(11) DEFAULT NULL COMMENT '团长会员id',
  `distributor_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '团长姓名',
  `team_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '团名',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '访问城市',
  `district` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '访问区',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=196641 DEFAULT CHARSET=utf8 COMMENT='会员访问记录表(es_member_pv)';

-- ----------------------------
-- Table structure for es_member_receipt
-- ----------------------------
DROP TABLE IF EXISTS `es_member_receipt`;
CREATE TABLE `es_member_receipt` (
  `receipt_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `receipt_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票类型 ELECTRO：电子普通发票，VATORDINARY：增值税普通发票',
  `receipt_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票抬头',
  `receipt_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票内容',
  `tax_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '纳税人识别号',
  `member_mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票人手机号',
  `member_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票人邮箱',
  `is_default` smallint(1) DEFAULT NULL COMMENT '是否为默认选项 0：否，1：是',
  PRIMARY KEY (`receipt_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员发票信息缓存表(es_member_receipt)';

-- ----------------------------
-- Table structure for es_member_shop_score
-- ----------------------------
DROP TABLE IF EXISTS `es_member_shop_score`;
CREATE TABLE `es_member_shop_score` (
  `score_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `order_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `delivery_score` smallint(1) DEFAULT NULL COMMENT '发货速度评分',
  `description_score` smallint(1) DEFAULT NULL COMMENT '描述相符度评分',
  `service_score` smallint(1) DEFAULT NULL COMMENT '服务评分',
  `seller_id` int(10) DEFAULT NULL COMMENT '卖家',
  PRIMARY KEY (`score_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=56142 DEFAULT CHARSET=utf8 COMMENT='店铺评分(es_member_shop_score)';

-- ----------------------------
-- Table structure for es_member_zpzz
-- ----------------------------
DROP TABLE IF EXISTS `es_member_zpzz`;
CREATE TABLE `es_member_zpzz` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `uname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '会员用户名',
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '状态 NEW_APPLY：新申请，AUDIT_PASS：审核通过，AUDIT_REFUSE：审核未通过',
  `company_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '单位名称',
  `taxpayer_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '纳税人识别码',
  `register_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公司注册地址',
  `register_tel` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公司注册电话',
  `bank_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开户银行',
  `bank_account` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '银行账户',
  `audit_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '平台审核备注',
  `apply_time` bigint(20) DEFAULT NULL COMMENT '申请时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员增票资质表(es_member_zpzz)';

-- ----------------------------
-- Table structure for es_menu
-- ----------------------------
DROP TABLE IF EXISTS `es_menu`;
CREATE TABLE `es_menu` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `parent_id` int(8) DEFAULT NULL COMMENT '父id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单标题',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单url',
  `identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单唯一标识',
  `auth_regular` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '权限表达式',
  `delete_flag` int(2) DEFAULT NULL COMMENT '删除标记',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单级别标识',
  `grade` int(8) DEFAULT NULL COMMENT '菜单级别',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=utf8 COMMENT='菜单表(es_menu)';

-- ----------------------------
-- Table structure for es_message
-- ----------------------------
DROP TABLE IF EXISTS `es_message`;
CREATE TABLE `es_message` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '站内消息主键',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '消息内容',
  `member_ids` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '会员id',
  `admin_id` int(8) DEFAULT NULL COMMENT '管理员id',
  `admin_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '管理员名称',
  `send_time` bigint(20) DEFAULT NULL COMMENT '发送时间',
  `send_type` int(2) DEFAULT NULL COMMENT '发送类型',
  `disabled` int(2) DEFAULT '0' COMMENT '是否删除 0：否，1：是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站内消息(es_message)';

-- ----------------------------
-- Table structure for es_message_template
-- ----------------------------
DROP TABLE IF EXISTS `es_message_template`;
CREATE TABLE `es_message_template` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '模版ID',
  `tpl_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '模版编号',
  `tpl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '模板名称',
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型(会员 ,店铺 ,其他)',
  `notice_state` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '站内信提醒是否开启',
  `sms_state` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '短信提醒是否开启',
  `email_state` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '邮件提醒是否开启',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '站内信内容',
  `sms_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '短信内容',
  `email_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '邮件内容',
  `email_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮件标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='消息模版(es_message_template)';

-- ----------------------------
-- Table structure for es_minus
-- ----------------------------
DROP TABLE IF EXISTS `es_minus`;
CREATE TABLE `es_minus` (
  `minus_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '单品立减活动id',
  `single_reduction_value` decimal(20,0) DEFAULT NULL COMMENT '单品立减金额',
  `start_time` bigint(20) DEFAULT NULL COMMENT '起始时间',
  `start_time_str` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '起始时间字符串',
  `end_time` bigint(20) DEFAULT NULL COMMENT '结束时间',
  `end_time_str` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '结束时间字符串',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '单品立减活动标题',
  `range_type` int(1) DEFAULT NULL COMMENT '是否选择全部商品',
  `disabled` int(1) DEFAULT NULL COMMENT '是否停用',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '描述',
  `seller_id` int(11) DEFAULT NULL COMMENT '商家id',
  PRIMARY KEY (`minus_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='单品立减(es_minus)';

-- ----------------------------
-- Table structure for es_navigation
-- ----------------------------
DROP TABLE IF EXISTS `es_navigation`;
CREATE TABLE `es_navigation` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '导航id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `disable` int(10) DEFAULT NULL COMMENT '是否显示',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `nav_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'URL',
  `target` int(10) DEFAULT NULL COMMENT '新窗口打开',
  `shop_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `contents` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='店铺导航管理(es_navigation)';

-- ----------------------------
-- Table structure for es_newcomer
-- ----------------------------
DROP TABLE IF EXISTS `es_newcomer`;
CREATE TABLE `es_newcomer` (
  `newcomer_id` int(11) NOT NULL AUTO_INCREMENT,
  `newcomer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '新人专享活动名',
  `status` tinyint(4) DEFAULT NULL COMMENT '活动状态',
  `limit_num` int(11) DEFAULT NULL COMMENT '每人限购数量',
  `seller_id` bigint(20) DEFAULT NULL COMMENT '活动所属店铺',
  `seller_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `newcomer_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动描述',
  `start_time` bigint(20) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '活动结束时间',
  `create_time` bigint(20) DEFAULT NULL,
  `update_time` bigint(20) DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`newcomer_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_newcomer_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_newcomer_goods`;
CREATE TABLE `es_newcomer_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newcomer_id` int(11) DEFAULT NULL COMMENT '新人活动ID',
  `goods_id` int(11) DEFAULT NULL,
  `sku_id` int(11) DEFAULT NULL,
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品名',
  `seller_id` int(11) DEFAULT NULL COMMENT '店铺ID',
  `seller_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '店铺名',
  `cost` decimal(10,2) DEFAULT NULL COMMENT '成本价',
  `seller_price` decimal(10,2) DEFAULT NULL COMMENT '店铺价',
  `sales_price` decimal(10,2) DEFAULT NULL COMMENT '售价',
  `limit_num` int(11) DEFAULT NULL COMMENT '商品限购数量',
  `sold_quantity` int(10) DEFAULT NULL COMMENT '活动商品库存',
  `sales_num` int(10) DEFAULT NULL COMMENT '已售数量',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_order
-- ----------------------------
DROP TABLE IF EXISTS `es_order`;
CREATE TABLE `es_order` (
  `order_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `trade_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易编号',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺ID',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺名称',
  `seller_logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺logo',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '买家账号',
  `order_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单状态',
  `pay_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '付款状态',
  `ship_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '货运状态',
  `shipping_id` int(10) DEFAULT NULL COMMENT '配送方式ID',
  `comment_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '评论是否完成',
  `shipping_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配送方式',
  `template_id` int(10) DEFAULT NULL COMMENT '快递/同城配送 模板id',
  `payment_method_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式id',
  `payment_plugin_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付插件id',
  `payment_method_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式名称',
  `payment_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式类型',
  `payment_time` bigint(20) DEFAULT NULL COMMENT '支付时间',
  `pay_money` decimal(20,2) DEFAULT NULL COMMENT '已支付金额',
  `ship_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '收货人姓名',
  `ship_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货地址',
  `ship_zip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货人邮编',
  `ship_mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货人手机',
  `ship_tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货人电话',
  `receive_time` bigint(20) DEFAULT NULL COMMENT '收货时间',
  `receive_time_type` tinyint(4) DEFAULT NULL COMMENT '客订收货时间类型 0次日达 1当日达',
  `ship_province_id` int(10) DEFAULT NULL COMMENT '配送地区-省份ID',
  `ship_city_id` int(10) DEFAULT NULL COMMENT '配送地区-城市ID',
  `ship_county_id` int(10) DEFAULT NULL COMMENT '配送地区-区(县)ID',
  `ship_town_id` int(10) DEFAULT NULL COMMENT '配送街道id',
  `ship_province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配送地区-省份',
  `ship_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配送地区-城市',
  `ship_county` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配送地区-区(县)',
  `ship_town` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配送街道',
  `order_price` decimal(20,2) DEFAULT NULL COMMENT '订单总额',
  `goods_price` decimal(20,2) DEFAULT NULL COMMENT '商品总额',
  `shipping_price` decimal(20,2) DEFAULT NULL COMMENT '配送费用',
  `discount_price` decimal(20,2) DEFAULT NULL COMMENT '优惠金额',
  `disabled` int(1) DEFAULT NULL COMMENT '是否被删除',
  `weight` decimal(20,2) DEFAULT NULL COMMENT '订单商品总重量',
  `goods_num` int(10) DEFAULT NULL COMMENT '商品数量',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单备注',
  `cancel_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单取消原因',
  `the_sign` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '签收人',
  `items_json` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '货物列表json',
  `warehouse_id` int(10) DEFAULT NULL COMMENT '发货仓库ID',
  `need_pay_money` decimal(20,2) DEFAULT NULL COMMENT '应付金额',
  `wallet_pay_price` decimal(20,2) DEFAULT '0.00' COMMENT '钱包支付金额',
  `ship_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货单号',
  `address_id` int(10) DEFAULT NULL COMMENT '收货地址ID',
  `admin_remark` int(50) DEFAULT NULL COMMENT '管理员备注',
  `logi_id` int(10) DEFAULT NULL COMMENT '物流公司ID',
  `logi_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '物流公司名称',
  `complete_time` bigint(20) DEFAULT NULL COMMENT '完成时间',
  `create_time` bigint(20) DEFAULT NULL COMMENT '订单创建时间',
  `signing_time` bigint(20) DEFAULT NULL COMMENT '签收时间',
  `ship_time` bigint(20) DEFAULT NULL COMMENT '送货时间',
  `pay_order_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式返回的交易号',
  `trade_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易类型order/trade',
  `service_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '售后状态',
  `bill_status` int(10) DEFAULT NULL COMMENT '结算状态',
  `bill_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '结算单号',
  `client_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单来源',
  `sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `need_receipt` int(1) DEFAULT NULL COMMENT '是否需要发票',
  `order_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'normal' COMMENT '订单类型',
  `order_data` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '订单数据',
  `pick_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '提货地址',
  `leader_id` int(10) DEFAULT NULL COMMENT '团长id',
  `warehouse_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '仓库分拣单号',
  `print_times` int(11) DEFAULT '0' COMMENT '打印次数',
  `order_flag` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单类型 FIRST_ORDER 首单',
  `expiry_day` int(11) DEFAULT NULL COMMENT '虚拟商品自购买日后几天可用',
  `available_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '虚拟商品的可用日期',
  `verification_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '核销码',
  `verification_time` bigint(20) DEFAULT NULL COMMENT '核销时间',
  `is_shetuan` tinyint(4) DEFAULT '0' COMMENT '是否是社团订单 0-否，1-是',
  PRIMARY KEY (`order_id`) USING BTREE,
  KEY `ind_order_sn` (`sn`) USING BTREE,
  KEY `ind_order_state` (`order_status`,`pay_status`,`ship_status`) USING BTREE,
  KEY `ind_order_memberid` (`member_id`) USING BTREE,
  KEY `ind_order_term` (`disabled`) USING BTREE,
  KEY `ind_order_seller` (`seller_id`) USING BTREE,
  KEY `ind_trade_sn` (`trade_sn`) USING BTREE,
  KEY `ind_verification_code` (`verification_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=137932 DEFAULT CHARSET=utf8 COMMENT='订单表(es_order)';

-- ----------------------------
-- Table structure for es_order_items
-- ----------------------------
DROP TABLE IF EXISTS `es_order_items`;
CREATE TABLE `es_order_items` (
  `item_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品ID',
  `product_id` int(10) DEFAULT NULL COMMENT '货品ID',
  `num` int(10) DEFAULT NULL COMMENT '销售量',
  `ship_num` int(10) DEFAULT NULL COMMENT '发货量',
  `trade_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易编号',
  `order_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `price` decimal(20,2) DEFAULT NULL COMMENT '销售金额',
  `cat_id` int(10) DEFAULT NULL COMMENT '分类ID',
  `state` int(1) DEFAULT NULL COMMENT '状态',
  `snapshot_id` int(10) DEFAULT NULL COMMENT '快照id',
  `spec_json` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '规格json',
  `promotion_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '促销类型',
  `promotion_id` int(10) DEFAULT NULL COMMENT '促销id',
  `refund_price` decimal(20,2) DEFAULT NULL COMMENT '订单项可退款金额',
  `comment_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'UNFINISHED' COMMENT '评论状态',
  `shetuan_goods_id` int(10) DEFAULT NULL COMMENT '社区团购商品',
  `ship_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '货运状态',
  `ship_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货单号',
  `logi_id` int(10) DEFAULT NULL COMMENT '物流公司ID',
  `logi_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '物流公司名称',
  `the_sign` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '签收人',
  `ship_time` bigint(20) DEFAULT NULL COMMENT '送货时间',
  `sku_cost` decimal(20,2) DEFAULT NULL COMMENT '商品成本价',
  PRIMARY KEY (`item_id`) USING BTREE,
  KEY `idx_goods_id` (`goods_id`) USING BTREE,
  KEY `idx_product_id` (`product_id`) USING BTREE,
  KEY `idx_order_sn` (`order_sn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=190117 DEFAULT CHARSET=utf8 COMMENT='订单货物表(es_order_items)';

-- ----------------------------
-- Table structure for es_order_log
-- ----------------------------
DROP TABLE IF EXISTS `es_order_log`;
CREATE TABLE `es_order_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `order_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `op_id` int(10) DEFAULT NULL COMMENT '操作者id',
  `op_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作者名称',
  `message` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志信息',
  `op_time` bigint(20) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`log_id`) USING BTREE,
  KEY `ind_order_log` (`order_sn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=668724 DEFAULT CHARSET=utf8 COMMENT='订单日志表(es_order_log)';

-- ----------------------------
-- Table structure for es_order_meta
-- ----------------------------
DROP TABLE IF EXISTS `es_order_meta`;
CREATE TABLE `es_order_meta` (
  `meta_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `meta_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '扩展-键',
  `meta_value` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '扩展-值',
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '售后状态',
  PRIMARY KEY (`meta_id`) USING BTREE,
  KEY `es_ind_orderex_metaid` (`meta_id`) USING BTREE,
  KEY `ind_order` (`order_sn`,`meta_key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=827437 DEFAULT CHARSET=utf8 COMMENT='订单扩展信息表(es_order_meta)';

-- ----------------------------
-- Table structure for es_order_out_status
-- ----------------------------
DROP TABLE IF EXISTS `es_order_out_status`;
CREATE TABLE `es_order_out_status` (
  `out_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `out_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出库类型',
  `out_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出库状态',
  PRIMARY KEY (`out_id`) USING BTREE,
  KEY `ind_order` (`order_sn`,`out_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=413791 DEFAULT CHARSET=utf8 COMMENT='订单出库状态(es_order_out_status)';

-- ----------------------------
-- Table structure for es_order_profit
-- ----------------------------
DROP TABLE IF EXISTS `es_order_profit`;
CREATE TABLE `es_order_profit` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `profit_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '收益编号',
  `profit_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '收益名称',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员名称',
  `member_type_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员类型名称',
  `order_id` int(10) DEFAULT NULL COMMENT '订单id',
  `order_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单sn',
  `order_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单类型',
  `order_type_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单类型名称',
  `order_status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单状态',
  `order_status_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单状态名称',
  `seller_id` int(11) DEFAULT NULL COMMENT '店铺id',
  `seller_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '卖家名称',
  `buyer_id` int(11) DEFAULT NULL COMMENT '买家id',
  `buyer_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '买家姓名',
  `pay_time` bigint(20) DEFAULT NULL COMMENT '付款时间',
  `cancel_time` bigint(20) DEFAULT NULL COMMENT '取消时间',
  `order_price` decimal(13,2) DEFAULT '0.00' COMMENT '订单总金额',
  `commission_type` tinyint(30) DEFAULT NULL COMMENT '佣金类型',
  `commission_type_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '佣金类型名称',
  `commission_rate` decimal(13,2) DEFAULT '0.00' COMMENT '佣金比例',
  `commission_money` decimal(13,2) DEFAULT '0.00' COMMENT '佣金金额',
  `order_imgs` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单图片',
  `spread_way` tinyint(4) DEFAULT NULL COMMENT '推广方式 1直推 2间推',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_member_id` (`member_id`) USING BTREE,
  KEY `idx_order_id` (`order_id`) USING BTREE,
  KEY `idx_order_sn` (`order_sn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=687640 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_order_settle
-- ----------------------------
DROP TABLE IF EXISTS `es_order_settle`;
CREATE TABLE `es_order_settle` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_id` int(11) NOT NULL COMMENT '订单id',
  `order_sn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单编号',
  `settle_time` bigint(11) DEFAULT NULL COMMENT '结算时间',
  `bill_item_id` int(11) DEFAULT NULL COMMENT '结算单项id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_1` (`order_id`,`order_sn`,`bill_item_id`) USING BTREE,
  KEY `idx_order_sn` (`order_sn`) USING BTREE,
  KEY `idx_order_id` (`order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=126869 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_page
-- ----------------------------
DROP TABLE IF EXISTS `es_page`;
CREATE TABLE `es_page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `page_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '楼层名称',
  `page_data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '楼层数据',
  `page_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '页面类型',
  `page_type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '页面类型名称 0默认设置 1 商城首页 2 团购首页 3 附近首页 4 发现页',
  `client_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '客户端类型',
  `store_id` int(11) DEFAULT '1' COMMENT '页面归属商户 ， 后新增',
  `agent_id` int(11) DEFAULT NULL COMMENT '代理id，后新增',
  `state` int(11) DEFAULT '1' COMMENT '页面的状态，1-代表草稿，2-代表上架，3-代表下架；  后新增',
  `owner_type` int(2) DEFAULT '0' COMMENT '拥有者类型，1为平台，2为代理，3为商户。后新增',
  `background` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '页面背景',
  `page_desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '页面描述',
  `province` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省-页面所属',
  `city` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市-页面所属',
  `county` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区-添加配置',
  `town` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '镇-添加配置',
  `share_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '首页分享图片',
  `is_discover` int(11) DEFAULT '0' COMMENT '是否发现页',
  `county_id` int(10) DEFAULT NULL COMMENT '区/县Id',
  PRIMARY KEY (`page_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=889052 DEFAULT CHARSET=utf8 COMMENT='楼层(es_page)';

-- ----------------------------
-- Table structure for es_parameter_group
-- ----------------------------
DROP TABLE IF EXISTS `es_parameter_group`;
CREATE TABLE `es_parameter_group` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '参数组名称',
  `category_id` int(10) DEFAULT NULL COMMENT '关联分类id',
  `sort` int(2) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=285 DEFAULT CHARSET=utf8 COMMENT='参数组(es_parameter_group)';

-- ----------------------------
-- Table structure for es_parameters
-- ----------------------------
DROP TABLE IF EXISTS `es_parameters`;
CREATE TABLE `es_parameters` (
  `param_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `param_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '参数名称',
  `param_type` int(1) DEFAULT NULL COMMENT '参数类型1 输入项   2 选择项',
  `options` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '选择值，当参数类型是选择项2时，必填，逗号分隔',
  `is_index` int(1) DEFAULT NULL COMMENT '是否可索引，0 不显示 1 显示',
  `required` int(1) DEFAULT NULL COMMENT '是否必填是  1    否   0',
  `group_id` int(10) DEFAULT NULL COMMENT '参数分组id',
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `sort` int(2) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`param_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1051 DEFAULT CHARSET=utf8 COMMENT='参数(es_parameters)';

-- ----------------------------
-- Table structure for es_pay_log
-- ----------------------------
DROP TABLE IF EXISTS `es_pay_log`;
CREATE TABLE `es_pay_log` (
  `pay_log_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '收款单ID',
  `order_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `pay_way` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式',
  `pay_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付类型',
  `pay_time` bigint(20) DEFAULT NULL COMMENT '付款时间',
  `pay_money` decimal(10,2) DEFAULT NULL COMMENT '付款金额',
  `pay_member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '付款会员名',
  `pay_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '付款状态',
  `pay_log_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '流水号',
  `pay_order_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式返回流水号',
  PRIMARY KEY (`pay_log_id`) USING BTREE,
  KEY `ind_pay_log` (`order_sn`,`pay_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=140833 DEFAULT CHARSET=utf8 COMMENT='收款单(es_pay_log)';

-- ----------------------------
-- Table structure for es_payment_bill
-- ----------------------------
DROP TABLE IF EXISTS `es_payment_bill`;
CREATE TABLE `es_payment_bill` (
  `bill_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '单号',
  `out_trade_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '提交给第三方平台单号',
  `return_trade_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '第三方平台返回交易号',
  `is_pay` int(1) DEFAULT NULL COMMENT '是否已支付',
  `trade_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易类型',
  `payment_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式名称',
  `pay_config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '支付参数',
  `trade_price` decimal(20,2) DEFAULT NULL COMMENT '交易金额',
  `payment_plugin_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付插件',
  PRIMARY KEY (`bill_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=141800 DEFAULT CHARSET=utf8 COMMENT='支付帐单(es_payment_bill)';

-- ----------------------------
-- Table structure for es_payment_method
-- ----------------------------
DROP TABLE IF EXISTS `es_payment_method`;
CREATE TABLE `es_payment_method` (
  `method_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '支付方式id',
  `method_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式名称',
  `plugin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付插件名称',
  `pc_config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'pc是否可用',
  `wap_config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'wap是否可用',
  `app_native_config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'app 原生是否可用',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式图片',
  `is_retrace` int(2) DEFAULT NULL COMMENT '是否支持原路退回',
  `app_react_config` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'app RN是否可用',
  `mini_config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '小程序配置',
  PRIMARY KEY (`method_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1706 DEFAULT CHARSET=utf8 COMMENT='支付方式(es_payment_method)';

-- ----------------------------
-- Table structure for es_pintuan
-- ----------------------------
DROP TABLE IF EXISTS `es_pintuan`;
CREATE TABLE `es_pintuan` (
  `promotion_id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `promotion_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动名称',
  `promotion_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动标题',
  `start_time` bigint(20) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '活动结束时间',
  `required_num` int(10) DEFAULT NULL COMMENT '成团人数',
  `limit_num` int(10) DEFAULT NULL COMMENT '限购数量',
  `enable_mocker` smallint(1) DEFAULT NULL COMMENT '虚拟成团',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动状态',
  `option_status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api请求操作状态',
  `seller_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商家名称',
  `seller_id` bigint(20) DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`promotion_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for es_pintuan_child_order
-- ----------------------------
DROP TABLE IF EXISTS `es_pintuan_child_order`;
CREATE TABLE `es_pintuan_child_order` (
  `child_order_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '子订单id',
  `order_sn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编号',
  `member_id` int(20) DEFAULT NULL COMMENT '会员id',
  `sku_id` int(20) DEFAULT NULL COMMENT '会员id',
  `pintuan_id` int(20) DEFAULT NULL COMMENT '拼团活动id',
  `order_status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单状态',
  `order_id` int(20) DEFAULT NULL COMMENT '主订单id',
  `member_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '买家名称',
  `origin_price` decimal(20,2) DEFAULT NULL COMMENT '原价',
  `sales_price` decimal(20,2) DEFAULT NULL COMMENT '拼团价',
  PRIMARY KEY (`child_order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1463 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for es_pintuan_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_pintuan_goods`;
CREATE TABLE `es_pintuan_goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sku_id` int(10) DEFAULT NULL COMMENT 'sku_id',
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品名称',
  `origin_price` decimal(20,2) DEFAULT NULL COMMENT '原价',
  `sales_price` decimal(20,2) DEFAULT NULL COMMENT '活动价',
  `sn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'sn',
  `sold_quantity` int(10) DEFAULT NULL COMMENT '已售数量',
  `locked_quantity` int(10) DEFAULT NULL COMMENT '待发货数量',
  `pintuan_id` int(10) DEFAULT NULL COMMENT '拼团活动id',
  `goods_id` int(20) DEFAULT NULL COMMENT 'goods_id',
  `specs` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '规格',
  `seller_id` int(10) DEFAULT NULL COMMENT '卖家id',
  `seller_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '卖家名称',
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品缩略图',
  `limit_num` int(10) DEFAULT NULL COMMENT '限购数量',
  `pick_time` bigint(20) DEFAULT NULL COMMENT '提货时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=934 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for es_pintuan_order
-- ----------------------------
DROP TABLE IF EXISTS `es_pintuan_order`;
CREATE TABLE `es_pintuan_order` (
  `order_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `pintuan_id` int(10) DEFAULT NULL COMMENT '拼团id',
  `end_time` bigint(20) DEFAULT NULL COMMENT '结束时间',
  `sku_id` int(20) DEFAULT NULL COMMENT 'sku_id',
  `required_num` int(10) DEFAULT NULL COMMENT '成团人数',
  `offered_num` int(10) DEFAULT NULL COMMENT '已参团人数',
  `offered_persons` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '参团人',
  `order_status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单状态',
  `goods_id` int(20) DEFAULT NULL COMMENT '商品id',
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品缩略图',
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '商品名称',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=716 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for es_promotion_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_promotion_goods`;
CREATE TABLE `es_promotion_goods` (
  `pg_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '商品对照ID',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `product_id` int(10) DEFAULT NULL COMMENT '货品id',
  `start_time` bigint(20) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '活动结束时间',
  `activity_id` int(10) DEFAULT NULL COMMENT '活动id',
  `promotion_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '促销工具类型',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动标题',
  `num` int(10) DEFAULT NULL COMMENT '参与活动的商品数量',
  `price` decimal(20,2) DEFAULT NULL COMMENT '活动时商品的价格',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`pg_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=151899 DEFAULT CHARSET=utf8 COMMENT='有效活动商品对照表(es_promotion_goods)';

-- ----------------------------
-- Table structure for es_quantity_log
-- ----------------------------
DROP TABLE IF EXISTS `es_quantity_log`;
CREATE TABLE `es_quantity_log` (
  `log_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `order_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `sku_id` int(10) DEFAULT NULL COMMENT 'sku id',
  `quantity` int(10) DEFAULT NULL COMMENT '库存数',
  `enable_quantity` int(8) DEFAULT NULL COMMENT '可用库存',
  `op_time` int(10) DEFAULT NULL COMMENT '操作时间',
  `log_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志类型',
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原因',
  PRIMARY KEY (`log_id`) USING BTREE,
  KEY `ind_quantity_log_goodsid` (`goods_id`,`sku_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存日志表(es_quantity_log)';

-- ----------------------------
-- Table structure for es_receipt_address
-- ----------------------------
DROP TABLE IF EXISTS `es_receipt_address`;
CREATE TABLE `es_receipt_address` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `member_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票人姓名',
  `member_mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票人手机号',
  `province_id` int(10) DEFAULT NULL COMMENT '收票地址-所属省份ID',
  `city_id` int(10) DEFAULT NULL COMMENT '收票地址-所属城市ID',
  `county_id` int(10) DEFAULT NULL COMMENT '收票地址-所属区县ID',
  `town_id` int(10) DEFAULT NULL COMMENT '收票地址-所属乡镇ID',
  `province` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-所属省份',
  `city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-所属城市',
  `county` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-所属区县',
  `town` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-所属乡镇',
  `detail_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-详细地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员收票地址表(es_receipt_address)';

-- ----------------------------
-- Table structure for es_receipt_content
-- ----------------------------
DROP TABLE IF EXISTS `es_receipt_content`;
CREATE TABLE `es_receipt_content` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '发票内容id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发票内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发票内容(es_receipt_content)';

-- ----------------------------
-- Table structure for es_receipt_file
-- ----------------------------
DROP TABLE IF EXISTS `es_receipt_file`;
CREATE TABLE `es_receipt_file` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `history_id` int(10) DEFAULT NULL,
  `elec_file` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员电子发票附件表(es_receipt_file)';

-- ----------------------------
-- Table structure for es_receipt_history
-- ----------------------------
DROP TABLE IF EXISTS `es_receipt_history`;
CREATE TABLE `es_receipt_history` (
  `history_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `order_sn` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编号',
  `order_price` decimal(20,2) DEFAULT NULL COMMENT '订单金额',
  `seller_id` int(10) DEFAULT NULL COMMENT '开票商家ID',
  `seller_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开票商家',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `status` smallint(1) DEFAULT NULL COMMENT '开票状态 0：未开，1：已开',
  `receipt_method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开票方式(针对增值税专用发票)',
  `receipt_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票类型 ELECTRO：电子普通发票，VATORDINARY：增值税普通发票，VATOSPECIAL：增值税专用发票',
  `logi_id` int(10) DEFAULT NULL COMMENT '物流公司ID',
  `logi_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '物流公司名称',
  `logi_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '快递单号',
  `receipt_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票抬头',
  `receipt_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发票内容',
  `tax_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '纳税人识别号',
  `reg_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '注册地址',
  `reg_tel` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '注册电话',
  `bank_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开户银行',
  `bank_account` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '银行账户',
  `member_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票人名称',
  `member_mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票人手机号',
  `member_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票人邮箱',
  `province_id` int(10) DEFAULT NULL COMMENT '收票地址-所属省份ID',
  `city_id` int(10) DEFAULT NULL COMMENT '收票地址-所属城市ID',
  `county_id` int(10) DEFAULT NULL COMMENT '收票地址-所属区县ID',
  `town_id` int(10) DEFAULT NULL COMMENT '收票地址-所属乡镇ID',
  `province` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-所属省份',
  `city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-所属城市',
  `county` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-所属区县',
  `town` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-所属乡镇',
  `detail_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收票地址-详细地址',
  `add_time` bigint(20) DEFAULT NULL COMMENT '申请开票日期',
  `goods_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '订单商品信息',
  `order_status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单出库状态，NEW/CONFIRM',
  `uname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '会员名称',
  PRIMARY KEY (`history_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员开票历史记录表(es_receipt_history)';

-- ----------------------------
-- Table structure for es_refund
-- ----------------------------
DROP TABLE IF EXISTS `es_refund`;
CREATE TABLE `es_refund` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '退(货)款单id',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退货(款)单编号',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `seller_id` int(10) DEFAULT NULL COMMENT '卖家id',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '卖家姓名',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `refund_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退(货)款状态',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `refund_point` int(10) DEFAULT NULL COMMENT '退还积分',
  `refund_price` decimal(20,2) DEFAULT NULL COMMENT '退款金额',
  `refund_way` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款方式(原路退回，在线支付，线下支付)',
  `account_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款账户类型',
  `return_account` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款账户',
  `customer_remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '客户备注',
  `seller_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '客服备注',
  `warehouse_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '库管备注',
  `finance_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '财务备注',
  `refund_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款原因',
  `refuse_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '拒绝原因',
  `bank_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行名称',
  `bank_account_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行账号',
  `bank_account_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行开户名',
  `bank_deposit_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行开户行',
  `trade_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易编号',
  `refund_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '售后类型(取消订单,申请售后)',
  `pay_order_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付结果交易号',
  `refuse_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退(货)款类型(退货，退款)',
  `payment_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单类型(在线支付,货到付款)',
  `refund_fail_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款失败原因',
  `refund_time` bigint(20) DEFAULT NULL COMMENT '退款时间',
  `refund_gift` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '赠品信息',
  `order_create_time` bigint(20) DEFAULT NULL COMMENT '下单时间',
  `audit_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4649 DEFAULT CHARSET=utf8 COMMENT='退(货)款单(es_refund)';

-- ----------------------------
-- Table structure for es_refund_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_refund_goods`;
CREATE TABLE `es_refund_goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '退货表id',
  `refund_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退货(款)编号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `sku_id` int(10) DEFAULT NULL COMMENT '产品id',
  `ship_num` int(10) DEFAULT NULL COMMENT '发货数量',
  `price` decimal(20,2) DEFAULT NULL COMMENT '商品价格',
  `return_num` int(10) DEFAULT NULL COMMENT '退货数量',
  `storage_num` int(10) DEFAULT NULL COMMENT '入库数量',
  `goods_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `goods_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品图片',
  `spec_json` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '规格数据',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6323 DEFAULT CHARSET=utf8 COMMENT='退货商品表(es_refund_goods)';

-- ----------------------------
-- Table structure for es_refund_item
-- ----------------------------
DROP TABLE IF EXISTS `es_refund_item`;
CREATE TABLE `es_refund_item` (
  `item_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '退款单详情id',
  `refund_id` int(10) DEFAULT NULL COMMENT '退(货)款单id',
  `refund_sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '退货(款)单编号',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '会员名称',
  `trade_sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '交易编号',
  `order_sn` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单编号',
  `item_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '退款详情单状态',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `refund_price` decimal(20,2) DEFAULT NULL COMMENT '退款金额',
  `pay_order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '支付结果交易号',
  `payment_plugin_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '退款（支付）插件ID',
  `refund_time` bigint(20) DEFAULT NULL COMMENT '退款时间',
  `refund_fail_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '退款失败原因',
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3585 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_refund_log
-- ----------------------------
DROP TABLE IF EXISTS `es_refund_log`;
CREATE TABLE `es_refund_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `refund_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款sn',
  `logtime` bigint(20) DEFAULT NULL COMMENT '日志记录时间',
  `logdetail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志详细',
  `operator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8546 DEFAULT CHARSET=utf8 COMMENT='退货（款）日志(es_refund_log)';

-- ----------------------------
-- Table structure for es_regions
-- ----------------------------
DROP TABLE IF EXISTS `es_regions`;
CREATE TABLE `es_regions` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '地区id',
  `parent_id` int(10) DEFAULT NULL COMMENT '父地区id',
  `region_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '路径',
  `region_grade` int(8) DEFAULT NULL COMMENT '级别',
  `local_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `zipcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮编',
  `cod` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否支持货到付款',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=53128 DEFAULT CHARSET=utf8 COMMENT='地区(es_regions)';

-- ----------------------------
-- Table structure for es_role
-- ----------------------------
DROP TABLE IF EXISTS `es_role`;
CREATE TABLE `es_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `auth_ids` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '角色介绍',
  `role_describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='角色表(es_role)';

-- ----------------------------
-- Table structure for es_seckill
-- ----------------------------
DROP TABLE IF EXISTS `es_seckill`;
CREATE TABLE `es_seckill` (
  `seckill_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `seckill_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动名称',
  `start_day` bigint(10) DEFAULT NULL COMMENT '活动日期',
  `apply_end_time` bigint(10) DEFAULT NULL COMMENT '报名截至时间',
  `seckill_rule` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '申请规则',
  `seller_ids` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '商家id',
  `seckill_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '状态',
  `seckill_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '秒杀活动类型 SELLER-店铺 PLATFORM 平台',
  PRIMARY KEY (`seckill_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=294 DEFAULT CHARSET=utf8 COMMENT='限时抢购入库(es_seckill)';

-- ----------------------------
-- Table structure for es_seckill_apply
-- ----------------------------
DROP TABLE IF EXISTS `es_seckill_apply`;
CREATE TABLE `es_seckill_apply` (
  `apply_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `seckill_id` int(10) DEFAULT NULL COMMENT '活动id',
  `time_line` int(10) DEFAULT NULL COMMENT '时刻',
  `start_day` bigint(11) DEFAULT NULL COMMENT '活动开始日期',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品ID',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `sku_id` int(10) DEFAULT NULL COMMENT '商品规格ID',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家ID',
  `shop_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `price` decimal(20,2) DEFAULT NULL COMMENT '价格',
  `sold_quantity` int(10) DEFAULT NULL COMMENT '售空数量',
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '申请状态',
  `fail_reason` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '驳回原因',
  `sales_num` int(10) DEFAULT NULL COMMENT '已售数量',
  `original_price` decimal(20,2) DEFAULT NULL COMMENT '商品原始价格',
  `limit_num` int(10) DEFAULT NULL COMMENT '秒杀限量',
  PRIMARY KEY (`apply_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4601 DEFAULT CHARSET=utf8 COMMENT='限时抢购申请(es_seckill_apply)';

-- ----------------------------
-- Table structure for es_seckill_range
-- ----------------------------
DROP TABLE IF EXISTS `es_seckill_range`;
CREATE TABLE `es_seckill_range` (
  `range_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `seckill_id` int(10) DEFAULT NULL COMMENT '限时抢购活动id',
  `range_time` int(10) DEFAULT NULL COMMENT '整点时刻',
  PRIMARY KEY (`range_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=915 DEFAULT CHARSET=utf8 COMMENT='限时抢购时刻(es_seckill_range)';

-- ----------------------------
-- Table structure for es_seller_bill
-- ----------------------------
DROP TABLE IF EXISTS `es_seller_bill`;
CREATE TABLE `es_seller_bill` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `expenditure` decimal(20,2) DEFAULT NULL COMMENT '商品反现支出',
  `return_expenditure` decimal(20,2) DEFAULT NULL COMMENT '返还商品反现支出',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=131960 DEFAULT CHARSET=utf8 COMMENT='店铺结算(es_seller_bill)';

-- ----------------------------
-- Table structure for es_sensitive_words
-- ----------------------------
DROP TABLE IF EXISTS `es_sensitive_words`;
CREATE TABLE `es_sensitive_words` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `word_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '敏感词名称',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `is_delete` smallint(1) DEFAULT NULL COMMENT '删除状态  1正常 0 删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='敏感词(es_sensitive_words)';

-- ----------------------------
-- Table structure for es_settings
-- ----------------------------
DROP TABLE IF EXISTS `es_settings`;
CREATE TABLE `es_settings` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '系统设置id',
  `cfg_value` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '系统配置信息',
  `cfg_group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '业务设置标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=490 DEFAULT CHARSET=utf8 COMMENT='系统设置(es_settings)';

-- ----------------------------
-- Table structure for es_share
-- ----------------------------
DROP TABLE IF EXISTS `es_share`;
CREATE TABLE `es_share` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uuid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'uuid',
  `member_id` int(11) DEFAULT NULL COMMENT '会员id',
  `share_key` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分享标识key',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分享url',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_member_id` (`member_id`) USING BTREE,
  KEY `idx_url` (`url`) USING BTREE,
  KEY `idx_share_key` (`share_key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44556 DEFAULT CHARSET=utf8 COMMENT='短链接(es_short_url)';

-- ----------------------------
-- Table structure for es_shetuan
-- ----------------------------
DROP TABLE IF EXISTS `es_shetuan`;
CREATE TABLE `es_shetuan` (
  `shetuan_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `shetuan_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动名称',
  `shetuan_no` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '团购编码',
  `start_time` bigint(20) DEFAULT NULL COMMENT '开售时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '结束时间',
  `pick_time` bigint(20) DEFAULT NULL COMMENT '提货时间',
  `join_stop_time` bigint(20) DEFAULT NULL COMMENT '报名截止时间',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `province_id` int(10) DEFAULT NULL COMMENT '所属省份ID',
  `city_id` int(10) DEFAULT NULL COMMENT '所属城市ID',
  `county_id` int(10) DEFAULT NULL COMMENT '所属县(区)ID',
  `county` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属县(区)名称',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属城市名称',
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属省份名称',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动区域',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `status` int(1) DEFAULT NULL COMMENT '社区团购活动状态  WAIT ("等待"),UNDERWAY("进行中"),END("结束");',
  `plan_on_time` bigint(20) DEFAULT NULL COMMENT '计划上线时间',
  `pre` int(1) DEFAULT '0',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  `page_popup` tinyint(4) DEFAULT NULL COMMENT '首页弹框  1 弹框 0 不弹',
  PRIMARY KEY (`shetuan_id`) USING BTREE,
  UNIQUE KEY `shetuan_no_UNIQUE` (`shetuan_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=617 DEFAULT CHARSET=utf8 COMMENT='社区团购活动(es_shetuan)';

-- ----------------------------
-- Table structure for es_shetuan_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_shetuan_goods`;
CREATE TABLE `es_shetuan_goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `shetuan_id` int(10) DEFAULT NULL COMMENT '社群团购活动id',
  `sku_id` int(11) DEFAULT NULL COMMENT 'sku_id',
  `sn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品编号',
  `specs` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '规格json',
  `goods_id` int(10) DEFAULT NULL COMMENT 'goods_id',
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品名称',
  `thumbnail` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品缩略图',
  `video_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品视频',
  `shop_cat_id` int(10) DEFAULT NULL COMMENT '店铺分组',
  `shop_cat_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '店铺分组名称',
  `settle_price` decimal(20,2) DEFAULT NULL COMMENT '结算价',
  `origin_price` decimal(20,2) DEFAULT NULL COMMENT '市场价',
  `sales_price` decimal(20,2) DEFAULT NULL COMMENT '店铺价',
  `shetuan_price` decimal(20,2) DEFAULT NULL COMMENT '团购价',
  `goods_num` int(10) DEFAULT NULL COMMENT '活动总库存',
  `buy_num` int(10) DEFAULT '0' COMMENT '已售',
  `seller_id` int(10) DEFAULT NULL COMMENT '商户id',
  `seller_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商户名称',
  `limit_num` int(10) DEFAULT NULL COMMENT '限购数量',
  `visual_num` int(10) DEFAULT NULL COMMENT '虚拟数量',
  `mock_num` int(10) DEFAULT NULL COMMENT '虚拟数量 待删除',
  `first_rate` decimal(20,2) DEFAULT NULL COMMENT '青铜团长提成',
  `second_rate` decimal(20,2) DEFAULT NULL COMMENT '白银团长提成',
  `third_rate` decimal(20,2) DEFAULT NULL COMMENT '黄金团长提成',
  `invite_rate` decimal(20,2) DEFAULT NULL COMMENT '邀请佣金比例',
  `self_raising_rate` decimal(20,2) DEFAULT NULL COMMENT '自提服务佣金',
  `subsidy_rate` decimal(20,2) DEFAULT '0.00' COMMENT '补贴比例',
  `audit_time` bigint(20) DEFAULT NULL COMMENT '审核时间',
  `audit_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '审核人',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `status` int(2) DEFAULT NULL,
  `priority` int(10) DEFAULT '50' COMMENT '优先级: 0-100',
  `shop_cat_items` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺多分组json',
  `cost` decimal(10,2) DEFAULT NULL COMMENT '成本价',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=192623 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_shetuan_order
-- ----------------------------
DROP TABLE IF EXISTS `es_shetuan_order`;
CREATE TABLE `es_shetuan_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '社区团购订单id',
  `leader_id` int(10) DEFAULT NULL COMMENT '团长id',
  `leader_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长姓名',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `order_id` int(10) DEFAULT NULL COMMENT '订单id',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单sn',
  `order_status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单状态',
  `st_order_status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '社区团购订单状态',
  `buyer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '收货人姓名',
  `leader_member_id` int(10) DEFAULT NULL COMMENT '团长会员id',
  `leader_mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团长手机号',
  `buyer_mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '收货人电话',
  `buy_num` int(10) DEFAULT NULL COMMENT '购买商品总数量',
  `site_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '站点名称',
  `site_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '站点类型',
  `pick_time` bigint(20) DEFAULT NULL COMMENT '预计提货时间',
  `pick_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '自提地址',
  `leader_commission` decimal(20,3) DEFAULT '0.000' COMMENT '团长提成',
  `leader_return_commission` decimal(20,3) DEFAULT '0.000' COMMENT '退还提成',
  `commission_rate` decimal(10,2) DEFAULT '0.00' COMMENT '团长提成比例',
  `commission_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '佣金类型',
  `order_price` decimal(10,2) DEFAULT NULL COMMENT '订单总金额',
  `buyer_member_id` bigint(20) DEFAULT NULL COMMENT '购买会员id',
  `buyer_member_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '购买会员名称',
  `bill_id` bigint(20) DEFAULT NULL COMMENT '结算单id',
  `settle_cycle` bigint(20) DEFAULT NULL COMMENT '解冻日期',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间（用户支付时间）',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ind_order_sn` (`order_id`) USING BTREE,
  KEY `ind_leader_id` (`leader_id`) USING BTREE,
  KEY `ind_order_seller` (`seller_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=134980 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_shetuan_quantity_log
-- ----------------------------
DROP TABLE IF EXISTS `es_shetuan_quantity_log`;
CREATE TABLE `es_shetuan_quantity_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `order_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品ID',
  `product_id` int(10) DEFAULT NULL COMMENT 'sku_id',
  `quantity` int(10) DEFAULT NULL COMMENT '团购售空数量',
  `op_time` bigint(20) DEFAULT NULL COMMENT '操作时间',
  `log_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '日志类型',
  `reason` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作原因',
  `st_id` int(10) DEFAULT NULL COMMENT '社区团购商品id',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_ship_template
-- ----------------------------
DROP TABLE IF EXISTS `es_ship_template`;
CREATE TABLE `es_ship_template` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `seller_id` int(10) DEFAULT NULL COMMENT '卖家id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '模板名称',
  `type` smallint(1) DEFAULT NULL COMMENT '1 重量算运费 2 计件算运费',
  `template_type` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '模板通途 KUAIDI TONGCHENG',
  `fee_setting` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '费用配置',
  `time_setting` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '时间配置',
  `min_ship_price` decimal(10,2) DEFAULT NULL COMMENT '最低配送费',
  `base_ship_price` decimal(10,2) DEFAULT NULL COMMENT '起送价格',
  `ship_range` int(10) DEFAULT NULL COMMENT '配送范围',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=415 DEFAULT CHARSET=utf8 COMMENT='运费模版(es_ship_template)';

-- ----------------------------
-- Table structure for es_ship_template_child
-- ----------------------------
DROP TABLE IF EXISTS `es_ship_template_child`;
CREATE TABLE `es_ship_template_child` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `template_id` int(10) DEFAULT NULL COMMENT '外键  模板id',
  `first_company` int(10) DEFAULT NULL COMMENT '首个（件）',
  `first_price` decimal(20,2) DEFAULT NULL COMMENT '首个（件）价格',
  `continued_company` int(10) DEFAULT NULL COMMENT '续个（件）',
  `continued_price` decimal(20,2) DEFAULT NULL COMMENT '续个（件）价格',
  `area` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '地区json',
  `area_id` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '地区id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=654 DEFAULT CHARSET=utf8 COMMENT='运费模板子模板(es_ship_template_child)';

-- ----------------------------
-- Table structure for es_shop
-- ----------------------------
DROP TABLE IF EXISTS `es_shop`;
CREATE TABLE `es_shop` (
  `shop_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '店铺Id',
  `member_id` int(10) DEFAULT NULL COMMENT '会员Id',
  `member_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `shop_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺名称',
  `shop_disable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺状态',
  `shop_createtime` bigint(20) DEFAULT NULL COMMENT '店铺创建时间',
  `shop_endtime` bigint(20) DEFAULT NULL COMMENT '店铺关闭时间',
  `shop_lat` double(10,6) DEFAULT NULL COMMENT '店铺经度',
  `shop_lng` double(10,6) DEFAULT NULL COMMENT '店铺纬度',
  PRIMARY KEY (`shop_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COMMENT='店铺(es_shop)';

-- ----------------------------
-- Table structure for es_shop_cat
-- ----------------------------
DROP TABLE IF EXISTS `es_shop_cat`;
CREATE TABLE `es_shop_cat` (
  `shop_cat_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '店铺分组id',
  `shop_cat_pid` int(10) DEFAULT NULL COMMENT '店铺分组父ID',
  `shop_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `shop_cat_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺分组名称',
  `disable` int(10) DEFAULT NULL COMMENT '店铺分组显示状态:1显示 0不显示',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `cat_path` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分组路径',
  `cat_path_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分组完整路径名称',
  `shop_cat_desc` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分组描述',
  `shop_cat_pic` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分组图片地址',
  PRIMARY KEY (`shop_cat_id`) USING BTREE,
  KEY `es_shop_cat_shopid` (`shop_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=614 DEFAULT CHARSET=utf8 COMMENT='店铺分组(es_shop_cat)';

-- ----------------------------
-- Table structure for es_shop_detail
-- ----------------------------
DROP TABLE IF EXISTS `es_shop_detail`;
CREATE TABLE `es_shop_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '店铺详细id',
  `shop_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `shop_province_id` int(10) DEFAULT NULL COMMENT '店铺所在省id',
  `shop_city_id` int(10) DEFAULT NULL COMMENT '店铺所在市id',
  `shop_county_id` int(10) DEFAULT NULL COMMENT '店铺所在县id',
  `shop_town_id` int(10) DEFAULT NULL COMMENT '店铺所在镇id',
  `shop_province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在省',
  `shop_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在市',
  `shop_county` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在县',
  `shop_town` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺所在镇',
  `shop_add` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺详细地址',
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司名称',
  `company_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司地址',
  `company_phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司电话',
  `company_email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电子邮箱',
  `employee_num` int(10) DEFAULT NULL COMMENT '员工总数',
  `reg_money` decimal(20,2) DEFAULT NULL COMMENT '注册资金',
  `link_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人姓名',
  `link_phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人电话',
  `legal_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '法人姓名',
  `legal_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '法人身份证',
  `legal_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '法人身份证照片',
  `legal_back_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '法人身份证反面照片',
  `hold_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手持身份证照片',
  `license_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业执照号',
  `license_province_id` int(10) DEFAULT NULL COMMENT '营业执照所在省id',
  `license_city_id` int(10) DEFAULT NULL COMMENT '营业执照所在市id',
  `license_county_id` int(10) DEFAULT NULL COMMENT '营业执照所在县id',
  `license_town_id` int(10) DEFAULT NULL COMMENT '营业执照所在镇id',
  `license_province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业执照所在省',
  `license_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业执照所在市',
  `license_county` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业执照所在县',
  `license_town` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业执照所在镇',
  `license_add` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业执照详细地址',
  `establish_date` bigint(20) DEFAULT NULL COMMENT '成立日期',
  `licence_start` bigint(20) DEFAULT NULL COMMENT '营业执照有效期开始',
  `licence_end` bigint(20) DEFAULT NULL COMMENT '营业执照有效期结束',
  `scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '法定经营范围',
  `licence_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '营业执照电子版',
  `organization_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组织机构代码',
  `code_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组织机构电子版',
  `taxes_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '一般纳税人证明电子版',
  `bank_account_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行开户名',
  `bank_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行开户账号',
  `bank_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户银行支行名称',
  `bank_province_id` int(10) DEFAULT NULL COMMENT '开户银行所在省id',
  `bank_city_id` int(10) DEFAULT NULL COMMENT '开户银行所在市id',
  `bank_county_id` int(10) DEFAULT NULL COMMENT '开户银行所在县id',
  `bank_town_id` int(10) DEFAULT NULL COMMENT '开户银行所在镇id',
  `bank_province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户银行所在省',
  `bank_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户银行所在市',
  `bank_county` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户银行所在县',
  `bank_town` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户银行所在镇',
  `bank_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户银行许可证电子版',
  `taxes_certificate_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '税务登记证号',
  `taxes_distinguish_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '纳税人识别号',
  `taxes_certificate_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '税务登记证书',
  `goods_management_category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺经营类目',
  `shop_level` int(10) DEFAULT NULL COMMENT '店铺等级',
  `shop_level_apply` int(10) DEFAULT NULL COMMENT '店铺等级申请',
  `store_space_capacity` decimal(10,2) DEFAULT NULL COMMENT '店铺相册已用存储量',
  `shop_logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺logo',
  `shop_banner` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺横幅',
  `shop_desc` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '店铺简介',
  `shop_recommend` int(10) DEFAULT NULL COMMENT '是否推荐',
  `shop_themeid` int(10) DEFAULT NULL COMMENT '店铺主题id',
  `shop_theme_path` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺主题模版路径',
  `wap_themeid` int(10) DEFAULT NULL COMMENT '店铺主题id',
  `wap_theme_path` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'wap店铺主题',
  `shop_credit` decimal(20,2) DEFAULT NULL COMMENT '店铺信用',
  `shop_praise_rate` decimal(10,2) DEFAULT NULL COMMENT '店铺好评率',
  `shop_description_credit` decimal(10,2) DEFAULT NULL COMMENT '店铺描述相符度',
  `shop_service_credit` decimal(10,2) DEFAULT NULL COMMENT '服务态度分数',
  `shop_delivery_credit` decimal(10,2) DEFAULT NULL COMMENT '发货速度分数',
  `shop_collect` int(10) DEFAULT NULL COMMENT '店铺收藏数',
  `goods_num` int(10) DEFAULT NULL COMMENT '店铺商品数',
  `shop_qq` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺客服qq',
  `shop_commission` decimal(10,2) DEFAULT NULL COMMENT '店铺佣金比例',
  `goods_warning_count` int(10) DEFAULT NULL COMMENT '货品预警数',
  `self_operated` int(10) DEFAULT NULL COMMENT '是否自营 1:是 0:否',
  `step` int(10) DEFAULT NULL COMMENT '申请开店进度：1,2.3.4',
  `ordin_receipt_status` int(1) DEFAULT '0' COMMENT '是否允许开具增值税普通发票 0：否，1：是',
  `elec_receipt_status` int(1) DEFAULT '0' COMMENT '是否允许开具电子普通发票 0：否，1：是',
  `tax_receipt_status` int(1) DEFAULT '0' COMMENT '是否允许开具增值税专用发票 0：否，1：是',
  `shop_lat` double(10,6) DEFAULT NULL COMMENT '店铺经度',
  `shop_lng` double(10,6) DEFAULT NULL COMMENT '店铺纬度',
  `ship_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺支持的配送方式 1快递 2.同城配送',
  `open_time_type` smallint(1) DEFAULT NULL COMMENT '营业时间类型',
  `open_start_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开始营业时间',
  `open_end_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '结束营业时间',
  `shop_tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺标签',
  `community_shop` int(1) NOT NULL DEFAULT '0' COMMENT '是否社区团社店铺，0-代表否，1-代表是',
  `batch_size` int(10) DEFAULT NULL COMMENT '分拣处理能力',
  `cooperation_mode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '合作模式 PLATFORM_CONSIGNMENT 平台代销 SELF_SALE 商家自销',
  `settlement_method` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '结算方式 CASH_SETTLEMENT 现结  WEEK_SETTLEMENT 周结  MONTHLY_SETTLEMENT 月结',
  `shop_type` tinyint(4) DEFAULT NULL COMMENT '店铺类型 1商城 2团购 3 本地生活',
  `open_time` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '营业时间json',
  `business_status` tinyint(4) DEFAULT NULL COMMENT '店铺营业状态 1营业 0不营业',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_shop_id` (`shop_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COMMENT='店铺详细(es_shop_detail)';

-- ----------------------------
-- Table structure for es_shop_logi_rel
-- ----------------------------
DROP TABLE IF EXISTS `es_shop_logi_rel`;
CREATE TABLE `es_shop_logi_rel` (
  `logi_id` int(10) DEFAULT NULL COMMENT '物流公司ID',
  `shop_id` int(10) DEFAULT NULL COMMENT '店铺ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺物流公司对照表(es_shop_logi_rel)';

-- ----------------------------
-- Table structure for es_shop_logistics_setting
-- ----------------------------
DROP TABLE IF EXISTS `es_shop_logistics_setting`;
CREATE TABLE `es_shop_logistics_setting` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shop_id` int(10) DEFAULT NULL,
  `logistics_id` int(10) DEFAULT NULL,
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_shop_menu
-- ----------------------------
DROP TABLE IF EXISTS `es_shop_menu`;
CREATE TABLE `es_shop_menu` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '店铺菜单id',
  `parent_id` int(8) DEFAULT NULL COMMENT '菜单父id',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单标题',
  `identifier` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单唯一标识',
  `auth_regular` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '权限表达式',
  `delete_flag` int(2) DEFAULT NULL COMMENT '删除标记',
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单级别标识',
  `grade` int(8) DEFAULT NULL COMMENT '菜单级别',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='菜单管理店铺(es_shop_menu)';

-- ----------------------------
-- Table structure for es_shop_notice_log
-- ----------------------------
DROP TABLE IF EXISTS `es_shop_notice_log`;
CREATE TABLE `es_shop_notice_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `shop_id` int(10) DEFAULT NULL COMMENT '店铺ID',
  `notice_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '站内信内容',
  `send_time` bigint(50) DEFAULT NULL COMMENT '发送时间',
  `is_delete` int(10) DEFAULT NULL COMMENT '是否删除 ：1 删除   0  未删除',
  `is_read` int(10) DEFAULT NULL COMMENT '是否已读 ：1已读   0 未读',
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '消息类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=420406 DEFAULT CHARSET=utf8 COMMENT='店铺站内消息(es_shop_notice_log)';

-- ----------------------------
-- Table structure for es_shop_role
-- ----------------------------
DROP TABLE IF EXISTS `es_shop_role`;
CREATE TABLE `es_shop_role` (
  `role_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '角色主键',
  `role_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `auth_ids` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '角色 ',
  `role_describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色描述',
  `shop_id` int(8) DEFAULT NULL COMMENT '店铺id',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='店铺角色(es_shop_role)';

-- ----------------------------
-- Table structure for es_shop_silde
-- ----------------------------
DROP TABLE IF EXISTS `es_shop_silde`;
CREATE TABLE `es_shop_silde` (
  `silde_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '幻灯片Id',
  `shop_id` int(10) DEFAULT NULL COMMENT '店铺Id',
  `silde_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '幻灯片URL',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`silde_id`) USING BTREE,
  KEY `ind_shop_silde` (`shop_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='店铺幻灯片(es_shop_silde)';

-- ----------------------------
-- Table structure for es_shop_themes
-- ----------------------------
DROP TABLE IF EXISTS `es_shop_themes`;
CREATE TABLE `es_shop_themes` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '模版id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '模版名称',
  `image_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '模版图片路径',
  `is_default` int(10) DEFAULT NULL COMMENT '是否为默认',
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '模版类型',
  `mark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '模版标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1385 DEFAULT CHARSET=utf8 COMMENT='店铺模版(es_shop_themes)';

-- ----------------------------
-- Table structure for es_short_url
-- ----------------------------
DROP TABLE IF EXISTS `es_short_url`;
CREATE TABLE `es_short_url` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '跳转地址',
  `su` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '短链接key',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='短链接(es_short_url)';

-- ----------------------------
-- Table structure for es_site_navigation
-- ----------------------------
DROP TABLE IF EXISTS `es_site_navigation`;
CREATE TABLE `es_site_navigation` (
  `navigation_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `navigation_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '导航名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '导航地址',
  `client_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '客户端类型',
  `image` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '图片',
  `sort` int(5) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`navigation_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COMMENT='导航栏(es_site_navigation)';

-- ----------------------------
-- Table structure for es_sms_platform
-- ----------------------------
DROP TABLE IF EXISTS `es_sms_platform`;
CREATE TABLE `es_sms_platform` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '平台名称',
  `open` int(8) DEFAULT NULL COMMENT '是否开启',
  `config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '配置',
  `bean` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'beanid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='短信网关(es_sms_platform)';

-- ----------------------------
-- Table structure for es_smtp
-- ----------------------------
DROP TABLE IF EXISTS `es_smtp`;
CREATE TABLE `es_smtp` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `host` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '主机',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `last_send_time` bigint(20) DEFAULT NULL COMMENT '最后发信时间',
  `send_count` int(11) DEFAULT '0' COMMENT '已发数',
  `max_count` int(11) DEFAULT NULL COMMENT '最大发信数',
  `mail_from` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发信邮箱',
  `port` int(10) DEFAULT '0' COMMENT '端口',
  `open_ssl` int(1) DEFAULT '0' COMMENT 'ssl是否开启',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='邮件(es_smtp)';

-- ----------------------------
-- Table structure for es_sorting_byb
-- ----------------------------
DROP TABLE IF EXISTS `es_sorting_byb`;
CREATE TABLE `es_sorting_byb` (
  `leader_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_sorting_level
-- ----------------------------
DROP TABLE IF EXISTS `es_sorting_level`;
CREATE TABLE `es_sorting_level` (
  `pick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '自提点名称',
  `level` int(11) DEFAULT NULL COMMENT '级别',
  `pick_id` int(11) DEFAULT NULL COMMENT '自提点id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='分拣自提点的优先级';

-- ----------------------------
-- Table structure for es_sorting_order_level_tmp
-- ----------------------------
DROP TABLE IF EXISTS `es_sorting_order_level_tmp`;
CREATE TABLE `es_sorting_order_level_tmp` (
  `order_sn` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `leader_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_sorting_serial
-- ----------------------------
DROP TABLE IF EXISTS `es_sorting_serial`;
CREATE TABLE `es_sorting_serial` (
  `order_sn` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单号',
  `sn` int(11) DEFAULT NULL COMMENT '分拣序列号',
  `status` int(11) DEFAULT NULL COMMENT '10-代分拣,20-已分拣',
  `bat_no` int(11) DEFAULT NULL COMMENT '批次',
  `leader_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '站点名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `data_time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据日期',
  KEY `ind_order_sn` (`order_sn`) USING BTREE,
  KEY `ind_sort` (`bat_no`,`sn`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_sorting_sku_map
-- ----------------------------
DROP TABLE IF EXISTS `es_sorting_sku_map`;
CREATE TABLE `es_sorting_sku_map` (
  `sku_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'sku名称',
  `sku_num` int(11) DEFAULT NULL COMMENT '购买数量',
  `order_sn` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单号',
  `serial_no` int(11) DEFAULT NULL COMMENT '格子号',
  `supplier` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '供应商',
  `bat_no` int(11) DEFAULT NULL COMMENT '分拣批次',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `data_time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_spec_values
-- ----------------------------
DROP TABLE IF EXISTS `es_spec_values`;
CREATE TABLE `es_spec_values` (
  `spec_value_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `spec_id` int(10) DEFAULT NULL COMMENT '规格项id',
  `spec_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '规格值名字',
  `seller_id` int(10) DEFAULT NULL COMMENT '所属卖家',
  `spec_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '规格名字',
  PRIMARY KEY (`spec_value_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19388 DEFAULT CHARSET=utf8 COMMENT='规格值(es_spec_values)';

-- ----------------------------
-- Table structure for es_specification
-- ----------------------------
DROP TABLE IF EXISTS `es_specification`;
CREATE TABLE `es_specification` (
  `spec_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `spec_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '规格项名称',
  `disabled` int(1) DEFAULT NULL COMMENT '是否被删除0 删除   1  没有删除',
  `spec_memo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '规格描述',
  `seller_id` int(10) DEFAULT NULL COMMENT '所属卖家',
  PRIMARY KEY (`spec_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2713 DEFAULT CHARSET=utf8 COMMENT='规格项(es_specification)';

-- ----------------------------
-- Table structure for es_sss_goods_cart_data
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_goods_cart_data`;
CREATE TABLE `es_sss_goods_cart_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品ID',
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品名称',
  `cart_goods_num` int(20) DEFAULT NULL COMMENT '每天累计加购数',
  `year` int(10) DEFAULT NULL COMMENT '年份',
  `month` int(10) DEFAULT NULL COMMENT '月份',
  `day` int(10) DEFAULT NULL COMMENT '天',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=244531 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='商品加购统计';

-- ----------------------------
-- Table structure for es_sss_goods_data
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_goods_data`;
CREATE TABLE `es_sss_goods_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `brand_id` int(10) DEFAULT NULL COMMENT '品牌id',
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `category_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类路径',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `shop_cat_id` int(10) DEFAULT NULL COMMENT '商家分类id',
  `price` decimal(20,2) DEFAULT NULL COMMENT '商品价格',
  `favorite_num` int(10) DEFAULT NULL COMMENT '收藏数量',
  `market_enable` smallint(1) DEFAULT NULL COMMENT '是否上架',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8222 DEFAULT CHARSET=utf8 COMMENT='统计库商品数据(es_sss_goods_data)';

-- ----------------------------
-- Table structure for es_sss_goods_pv
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_goods_pv`;
CREATE TABLE `es_sss_goods_pv` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `vs_year` int(10) DEFAULT NULL COMMENT '年份',
  `vs_num` int(20) DEFAULT NULL COMMENT '访问量',
  `vs_month` int(10) DEFAULT NULL COMMENT '月份',
  `vs_day` int(10) DEFAULT NULL COMMENT '天',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=399261 DEFAULT CHARSET=utf8 COMMENT='商品访问量统计表(es_sss_goods_pv)';

-- ----------------------------
-- Table structure for es_sss_goods_pv_2015
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_goods_pv_2015`;
CREATE TABLE `es_sss_goods_pv_2015` (
  `id` int(10) NOT NULL DEFAULT '0',
  `seller_id` int(10) DEFAULT NULL,
  `goods_id` int(10) DEFAULT NULL,
  `goods_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `vs_year` int(10) DEFAULT NULL,
  `vs_month` int(10) DEFAULT NULL,
  `vs_day` int(10) DEFAULT NULL,
  `vs_num` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_goods_pv_2017
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_goods_pv_2017`;
CREATE TABLE `es_sss_goods_pv_2017` (
  `id` int(10) NOT NULL DEFAULT '0',
  `seller_id` int(10) DEFAULT NULL,
  `goods_id` int(10) DEFAULT NULL,
  `goods_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `vs_year` int(10) DEFAULT NULL,
  `vs_month` int(10) DEFAULT NULL,
  `vs_day` int(10) DEFAULT NULL,
  `vs_num` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_goods_pv_2018
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_goods_pv_2018`;
CREATE TABLE `es_sss_goods_pv_2018` (
  `id` int(10) NOT NULL DEFAULT '0',
  `seller_id` int(10) DEFAULT NULL,
  `goods_id` int(10) DEFAULT NULL,
  `goods_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `vs_year` int(10) DEFAULT NULL,
  `vs_month` int(10) DEFAULT NULL,
  `vs_day` int(10) DEFAULT NULL,
  `vs_num` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_goods_pv_2019
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_goods_pv_2019`;
CREATE TABLE `es_sss_goods_pv_2019` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `seller_id` int(10) DEFAULT NULL,
  `goods_id` int(10) DEFAULT NULL,
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `vs_year` int(10) DEFAULT NULL,
  `vs_month` int(10) DEFAULT NULL,
  `vs_day` int(10) DEFAULT NULL,
  `vs_num` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_sss_goods_pv_2020
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_goods_pv_2020`;
CREATE TABLE `es_sss_goods_pv_2020` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `vs_year` int(10) DEFAULT NULL COMMENT '年份',
  `vs_num` int(20) DEFAULT NULL COMMENT '访问量',
  `vs_month` int(10) DEFAULT NULL COMMENT '月份',
  `vs_day` int(10) DEFAULT NULL COMMENT '天',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=192718 DEFAULT CHARSET=utf8 COMMENT='商品访问量统计表(es_sss_goods_pv)';

-- ----------------------------
-- Table structure for es_sss_goods_pv_2021
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_goods_pv_2021`;
CREATE TABLE `es_sss_goods_pv_2021` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `vs_year` int(10) DEFAULT NULL COMMENT '年份',
  `vs_num` int(20) DEFAULT NULL COMMENT '访问量',
  `vs_month` int(10) DEFAULT NULL COMMENT '月份',
  `vs_day` int(10) DEFAULT NULL COMMENT '天',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=399261 DEFAULT CHARSET=utf8 COMMENT='商品访问量统计表(es_sss_goods_pv)';

-- ----------------------------
-- Table structure for es_sss_member_register_data
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_member_register_data`;
CREATE TABLE `es_sss_member_register_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(10) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名字',
  `create_time` int(12) DEFAULT NULL COMMENT '注册日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13012 DEFAULT CHARSET=utf8 COMMENT='注册会员信息(es_sss_member_register_data)';

-- ----------------------------
-- Table structure for es_sss_member_visit_data
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_member_visit_data`;
CREATE TABLE `es_sss_member_visit_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_uv_new` int(20) DEFAULT NULL COMMENT '新增用户留存',
  `visit_uv` int(20) DEFAULT NULL COMMENT '活跃用户留存',
  `create_time` int(11) DEFAULT NULL COMMENT '时间（当天零点）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=439 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_sss_order_data
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_data`;
CREATE TABLE `es_sss_order_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `buyer_id` int(10) DEFAULT NULL COMMENT '会员id',
  `buyer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `order_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单状态',
  `pay_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '付款状态',
  `order_price` decimal(20,2) DEFAULT NULL COMMENT '订单金额',
  `goods_num` int(10) DEFAULT NULL COMMENT '订单商品数量',
  `ship_province_id` int(10) DEFAULT NULL COMMENT '省id',
  `ship_city_id` int(10) DEFAULT NULL COMMENT '市id',
  `create_time` int(12) DEFAULT NULL COMMENT '订单创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ind_order` (`sn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=133234 DEFAULT CHARSET=utf8 COMMENT='订单数据(es_sss_order_data)';

-- ----------------------------
-- Table structure for es_sss_order_data_2015
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_data_2015`;
CREATE TABLE `es_sss_order_data_2015` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sn` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '订单编号',
  `buyer_id` int(10) DEFAULT NULL COMMENT '会员id',
  `buyer_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '商家名称',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `seller_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '商家名称',
  `order_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '订单状态',
  `pay_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '付款状态',
  `order_price` decimal(20,2) DEFAULT NULL COMMENT '订单金额',
  `goods_num` int(10) DEFAULT NULL COMMENT '订单商品数量',
  `ship_province_id` int(10) DEFAULT NULL COMMENT '省id',
  `ship_city_id` int(10) DEFAULT NULL COMMENT '市id',
  `create_time` int(12) DEFAULT NULL COMMENT '订单创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_order_data_2017
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_data_2017`;
CREATE TABLE `es_sss_order_data_2017` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sn` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '订单编号',
  `buyer_id` int(10) DEFAULT NULL COMMENT '会员id',
  `buyer_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '商家名称',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `seller_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '商家名称',
  `order_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '订单状态',
  `pay_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL COMMENT '付款状态',
  `order_price` decimal(20,2) DEFAULT NULL COMMENT '订单金额',
  `goods_num` int(10) DEFAULT NULL COMMENT '订单商品数量',
  `ship_province_id` int(10) DEFAULT NULL COMMENT '省id',
  `ship_city_id` int(10) DEFAULT NULL COMMENT '市id',
  `create_time` int(12) DEFAULT NULL COMMENT '订单创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_order_data_2018
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_data_2018`;
CREATE TABLE `es_sss_order_data_2018` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `buyer_id` int(10) DEFAULT NULL COMMENT '会员id',
  `buyer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `order_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单状态',
  `pay_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '付款状态',
  `order_price` decimal(20,2) DEFAULT NULL COMMENT '订单金额',
  `goods_num` int(10) DEFAULT NULL COMMENT '订单商品数量',
  `ship_province_id` int(10) DEFAULT NULL COMMENT '省id',
  `ship_city_id` int(10) DEFAULT NULL COMMENT '市id',
  `create_time` int(12) DEFAULT NULL COMMENT '订单创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_sss_order_data_2019
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_data_2019`;
CREATE TABLE `es_sss_order_data_2019` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `buyer_id` int(10) DEFAULT NULL,
  `buyer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `seller_id` int(10) DEFAULT NULL,
  `seller_name` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `order_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `pay_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `order_price` decimal(20,2) DEFAULT NULL,
  `goods_num` int(10) DEFAULT NULL,
  `ship_province_id` int(10) DEFAULT NULL,
  `ship_city_id` int(10) DEFAULT NULL,
  `create_time` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_sss_order_data_2020
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_data_2020`;
CREATE TABLE `es_sss_order_data_2020` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `buyer_id` int(10) DEFAULT NULL COMMENT '会员id',
  `buyer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `order_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单状态',
  `pay_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '付款状态',
  `order_price` decimal(20,2) DEFAULT NULL COMMENT '订单金额',
  `goods_num` int(10) DEFAULT NULL COMMENT '订单商品数量',
  `ship_province_id` int(10) DEFAULT NULL COMMENT '省id',
  `ship_city_id` int(10) DEFAULT NULL COMMENT '市id',
  `create_time` int(12) DEFAULT NULL COMMENT '订单创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ind_order` (`sn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60592 DEFAULT CHARSET=utf8 COMMENT='订单数据(es_sss_order_data)';

-- ----------------------------
-- Table structure for es_sss_order_data_2021
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_data_2021`;
CREATE TABLE `es_sss_order_data_2021` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `buyer_id` int(10) DEFAULT NULL COMMENT '会员id',
  `buyer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商家名称',
  `order_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单状态',
  `pay_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '付款状态',
  `order_price` decimal(20,2) DEFAULT NULL COMMENT '订单金额',
  `goods_num` int(10) DEFAULT NULL COMMENT '订单商品数量',
  `ship_province_id` int(10) DEFAULT NULL COMMENT '省id',
  `ship_city_id` int(10) DEFAULT NULL COMMENT '市id',
  `create_time` int(12) DEFAULT NULL COMMENT '订单创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ind_order` (`sn`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=133234 DEFAULT CHARSET=utf8 COMMENT='订单数据(es_sss_order_data)';

-- ----------------------------
-- Table structure for es_sss_order_goods_data
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_goods_data`;
CREATE TABLE `es_sss_order_goods_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `goods_num` int(10) DEFAULT NULL COMMENT '数量',
  `price` decimal(20,2) DEFAULT NULL COMMENT '商品单价',
  `sub_total` decimal(20,2) DEFAULT NULL COMMENT '小计',
  `category_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类path',
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `create_time` int(12) DEFAULT NULL COMMENT '创建时间',
  `industry_id` bigint(10) DEFAULT NULL COMMENT '行业id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=183931 DEFAULT CHARSET=utf8 COMMENT='订单商品表(es_sss_order_goods_data)';

-- ----------------------------
-- Table structure for es_sss_order_goods_data_2019
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_goods_data_2019`;
CREATE TABLE `es_sss_order_goods_data_2019` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `goods_id` int(10) DEFAULT NULL,
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `goods_num` int(10) DEFAULT NULL,
  `price` decimal(20,2) DEFAULT NULL,
  `sub_total` decimal(20,2) DEFAULT NULL,
  `category_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `category_id` int(10) DEFAULT '0',
  `create_time` int(12) DEFAULT '0',
  `industry_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_sss_order_goods_data_2020
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_goods_data_2020`;
CREATE TABLE `es_sss_order_goods_data_2020` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `goods_num` int(10) DEFAULT NULL COMMENT '数量',
  `price` decimal(20,2) DEFAULT NULL COMMENT '商品单价',
  `sub_total` decimal(20,2) DEFAULT NULL COMMENT '小计',
  `category_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类path',
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `create_time` int(12) DEFAULT NULL COMMENT '创建时间',
  `industry_id` bigint(10) DEFAULT NULL COMMENT '行业id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=86228 DEFAULT CHARSET=utf8 COMMENT='订单商品表(es_sss_order_goods_data)';

-- ----------------------------
-- Table structure for es_sss_order_goods_data_2021
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_order_goods_data_2021`;
CREATE TABLE `es_sss_order_goods_data_2021` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `goods_num` int(10) DEFAULT NULL COMMENT '数量',
  `price` decimal(20,2) DEFAULT NULL COMMENT '商品单价',
  `sub_total` decimal(20,2) DEFAULT NULL COMMENT '小计',
  `category_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类path',
  `category_id` int(10) DEFAULT NULL COMMENT '分类id',
  `create_time` int(12) DEFAULT NULL COMMENT '创建时间',
  `industry_id` bigint(10) DEFAULT NULL COMMENT '行业id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=183931 DEFAULT CHARSET=utf8 COMMENT='订单商品表(es_sss_order_goods_data)';

-- ----------------------------
-- Table structure for es_sss_refund_data
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_refund_data`;
CREATE TABLE `es_sss_refund_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单sn',
  `refund_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '售后订单sn',
  `refund_price` decimal(20,2) DEFAULT NULL COMMENT '退还金额',
  `create_time` int(12) DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3682 DEFAULT CHARSET=utf8 COMMENT='退货数据(es_sss_refund_data)';

-- ----------------------------
-- Table structure for es_sss_refund_data_2015
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_refund_data_2015`;
CREATE TABLE `es_sss_refund_data_2015` (
  `id` int(10) NOT NULL DEFAULT '0',
  `seller_id` int(10) DEFAULT NULL,
  `refund_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `refund_price` decimal(20,2) DEFAULT NULL,
  `create_time` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_refund_data_2017
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_refund_data_2017`;
CREATE TABLE `es_sss_refund_data_2017` (
  `id` int(10) NOT NULL DEFAULT '0',
  `seller_id` int(10) DEFAULT NULL,
  `refund_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `refund_price` decimal(20,2) DEFAULT NULL,
  `create_time` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_refund_data_2018
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_refund_data_2018`;
CREATE TABLE `es_sss_refund_data_2018` (
  `id` int(10) NOT NULL DEFAULT '0',
  `seller_id` int(10) DEFAULT NULL,
  `refund_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `refund_price` decimal(20,2) DEFAULT NULL,
  `create_time` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_refund_data_2019
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_refund_data_2019`;
CREATE TABLE `es_sss_refund_data_2019` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `seller_id` int(10) DEFAULT NULL,
  `refund_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `refund_price` decimal(20,2) DEFAULT NULL,
  `create_time` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_sss_refund_data_2020
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_refund_data_2020`;
CREATE TABLE `es_sss_refund_data_2020` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单sn',
  `refund_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '售后订单sn',
  `refund_price` decimal(20,2) DEFAULT NULL COMMENT '退还金额',
  `create_time` int(12) DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1747 DEFAULT CHARSET=utf8 COMMENT='退货数据(es_sss_refund_data)';

-- ----------------------------
-- Table structure for es_sss_refund_data_2021
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_refund_data_2021`;
CREATE TABLE `es_sss_refund_data_2021` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `seller_id` int(10) DEFAULT NULL COMMENT '商家id',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单sn',
  `refund_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '售后订单sn',
  `refund_price` decimal(20,2) DEFAULT NULL COMMENT '退还金额',
  `create_time` int(12) DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3682 DEFAULT CHARSET=utf8 COMMENT='退货数据(es_sss_refund_data)';

-- ----------------------------
-- Table structure for es_sss_shop_data
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_shop_data`;
CREATE TABLE `es_sss_shop_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `seller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺名称',
  `favorite_num` int(255) DEFAULT NULL COMMENT '店铺被收藏数量',
  `shop_disable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '店铺是否开启',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COMMENT='商品数据表(es_sss_shop_data)';

-- ----------------------------
-- Table structure for es_sss_shop_pv
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_shop_pv`;
CREATE TABLE `es_sss_shop_pv` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `vs_year` int(4) DEFAULT NULL COMMENT '年份',
  `vs_month` int(4) DEFAULT NULL COMMENT '月份',
  `vs_day` int(4) DEFAULT NULL COMMENT '日期',
  `vs_num` int(10) DEFAULT NULL COMMENT '访问量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=390 DEFAULT CHARSET=utf8 COMMENT='店铺访问量统计表(es_sss_shop_pv)';

-- ----------------------------
-- Table structure for es_sss_shop_pv_2015
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_shop_pv_2015`;
CREATE TABLE `es_sss_shop_pv_2015` (
  `id` int(10) NOT NULL,
  `seller_id` int(10) DEFAULT NULL,
  `vs_year` int(4) DEFAULT NULL,
  `vs_month` int(4) DEFAULT NULL,
  `vs_day` int(4) DEFAULT NULL,
  `vs_num` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_shop_pv_2017
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_shop_pv_2017`;
CREATE TABLE `es_sss_shop_pv_2017` (
  `id` int(10) NOT NULL,
  `seller_id` int(10) DEFAULT NULL,
  `vs_year` int(4) DEFAULT NULL,
  `vs_month` int(4) DEFAULT NULL,
  `vs_day` int(4) DEFAULT NULL,
  `vs_num` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_shop_pv_2018
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_shop_pv_2018`;
CREATE TABLE `es_sss_shop_pv_2018` (
  `id` int(10) NOT NULL DEFAULT '0',
  `seller_id` int(10) DEFAULT NULL,
  `vs_year` int(4) DEFAULT NULL,
  `vs_month` int(4) DEFAULT NULL,
  `vs_day` int(4) DEFAULT NULL,
  `vs_num` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for es_sss_shop_pv_2019
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_shop_pv_2019`;
CREATE TABLE `es_sss_shop_pv_2019` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `seller_id` int(10) DEFAULT NULL,
  `vs_year` int(4) DEFAULT NULL,
  `vs_month` int(4) DEFAULT NULL,
  `vs_day` int(4) DEFAULT NULL,
  `vs_num` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_sss_shop_pv_2020
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_shop_pv_2020`;
CREATE TABLE `es_sss_shop_pv_2020` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `vs_year` int(4) DEFAULT NULL COMMENT '年份',
  `vs_month` int(4) DEFAULT NULL COMMENT '月份',
  `vs_day` int(4) DEFAULT NULL COMMENT '日期',
  `vs_num` int(10) DEFAULT NULL COMMENT '访问量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8 COMMENT='店铺访问量统计表(es_sss_shop_pv)';

-- ----------------------------
-- Table structure for es_sss_shop_pv_2021
-- ----------------------------
DROP TABLE IF EXISTS `es_sss_shop_pv_2021`;
CREATE TABLE `es_sss_shop_pv_2021` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `vs_year` int(4) DEFAULT NULL COMMENT '年份',
  `vs_month` int(4) DEFAULT NULL COMMENT '月份',
  `vs_day` int(4) DEFAULT NULL COMMENT '日期',
  `vs_num` int(10) DEFAULT NULL COMMENT '访问量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=390 DEFAULT CHARSET=utf8 COMMENT='店铺访问量统计表(es_sss_shop_pv)';

-- ----------------------------
-- Table structure for es_tag_goods
-- ----------------------------
DROP TABLE IF EXISTS `es_tag_goods`;
CREATE TABLE `es_tag_goods` (
  `tag_id` int(10) DEFAULT NULL COMMENT '标签id',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签商品关联(es_tag_goods)';

-- ----------------------------
-- Table structure for es_tags
-- ----------------------------
DROP TABLE IF EXISTS `es_tags`;
CREATE TABLE `es_tags` (
  `tag_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tag_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标签名字',
  `seller_id` int(10) DEFAULT NULL COMMENT '所属卖家',
  `mark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '关键字',
  `shop_home_page` tinyint(4) DEFAULT '1' COMMENT '店铺主页显示标签（0显示，1不显示）',
  `goods_list_page` tinyint(4) DEFAULT '1' COMMENT '商品列表页显示标签（0显示，1不显示）',
  `goods_details_page` tinyint(4) DEFAULT '1' COMMENT '商品详情页显示标签（0显示，1不显示）',
  `permanent_enable` tinyint(4) DEFAULT NULL COMMENT '是否永久有效（0是，1否）',
  `all_goods` bigint(4) DEFAULT '1' COMMENT '是否给全部商品添加标签（0是，1否）',
  `type` int(11) DEFAULT NULL COMMENT '标签类型，0系统默认，1自定义',
  `status` int(11) DEFAULT NULL COMMENT '标签状态 ',
  `priority` int(11) DEFAULT '0' COMMENT '标签的优先级（数字越大排序越靠前）',
  `start_time` bigint(20) DEFAULT NULL COMMENT '标签生效开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '标签生效结束时间',
  `color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '颜色',
  `style` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '样式',
  `tag_describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标签描述',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`tag_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=807 DEFAULT CHARSET=utf8 COMMENT='商品标签(es_tags)';

-- ----------------------------
-- Table structure for es_third_party_transfer_log
-- ----------------------------
DROP TABLE IF EXISTS `es_third_party_transfer_log`;
CREATE TABLE `es_third_party_transfer_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '转账单号',
  `transfer_price` decimal(20,2) DEFAULT NULL COMMENT '转账金额',
  `payee_member_id` int(11) DEFAULT NULL COMMENT '收款人',
  `payee_member_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收款人姓名',
  `pay_config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '支付时使用的参数',
  `open_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收款人的open ID',
  `transfer_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '转账状态',
  `payment_plugin_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付插件',
  `operator_member_id` int(11) DEFAULT NULL COMMENT '操作人',
  `operator_member_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作人姓名',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  `transfer_describe` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '企业付款备注',
  `request_msg` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '请求报文',
  `result_msg` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '返回报文',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2491 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for es_trade
-- ----------------------------
DROP TABLE IF EXISTS `es_trade`;
CREATE TABLE `es_trade` (
  `trade_id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'trade_id',
  `trade_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交易编号',
  `member_id` int(10) DEFAULT NULL COMMENT '买家id',
  `member_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '买家用户名',
  `payment_method_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式id',
  `payment_plugin_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付插件id',
  `payment_method_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式名称',
  `payment_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式类型',
  `total_price` decimal(20,2) DEFAULT NULL COMMENT '总价格',
  `goods_price` decimal(20,2) DEFAULT NULL COMMENT '商品价格',
  `freight_price` decimal(20,2) DEFAULT NULL COMMENT '运费',
  `discount_price` decimal(20,2) DEFAULT NULL COMMENT '优惠的金额',
  `wallet_pay_price` decimal(20,2) DEFAULT '0.00' COMMENT '钱包支付金额',
  `consignee_id` int(10) DEFAULT NULL COMMENT '收货人id',
  `consignee_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货人姓名',
  `consignee_country` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货国家',
  `consignee_country_id` int(10) DEFAULT NULL COMMENT '收货国家id',
  `consignee_province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货省',
  `consignee_province_id` int(10) DEFAULT NULL COMMENT '收货省id',
  `consignee_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货市',
  `consignee_city_id` int(10) DEFAULT NULL COMMENT '收货市id',
  `consignee_county` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货区',
  `consignee_county_id` int(10) DEFAULT NULL COMMENT '收货区id',
  `consignee_town` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货镇',
  `consignee_town_id` int(10) DEFAULT NULL COMMENT '收货镇id',
  `consignee_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货详细地址',
  `consignee_mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货人手机号',
  `consignee_telephone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货人电话',
  `create_time` bigint(20) DEFAULT NULL COMMENT '交易创建时间',
  `order_json` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '订单json(预留，7.0可能废弃)',
  `trade_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单状态',
  PRIMARY KEY (`trade_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=137977 DEFAULT CHARSET=utf8 COMMENT='交易表(es_trade)';

-- ----------------------------
-- Table structure for es_transaction_record
-- ----------------------------
DROP TABLE IF EXISTS `es_transaction_record`;
CREATE TABLE `es_transaction_record` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `order_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品ID',
  `goods_num` int(10) DEFAULT NULL COMMENT '商品数量',
  `rog_time` bigint(20) DEFAULT NULL COMMENT '确认收货时间',
  `uname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
  `price` decimal(20,2) DEFAULT NULL COMMENT '交易价格',
  `member_id` int(10) DEFAULT NULL COMMENT '会员ID',
  PRIMARY KEY (`record_id`) USING BTREE,
  KEY `index_goods_id` (`goods_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=177341 DEFAULT CHARSET=utf8 COMMENT='交易记录表(es_transaction_record)';

-- ----------------------------
-- Table structure for es_upgrade_log
-- ----------------------------
DROP TABLE IF EXISTS `es_upgrade_log`;
CREATE TABLE `es_upgrade_log` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` bigint(10) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '切换类型',
  `old_tpl_id` bigint(20) DEFAULT NULL COMMENT '旧的模板id',
  `new_tpl_id` bigint(20) DEFAULT NULL COMMENT '新的模板id',
  `new_tpl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '新的模板名称',
  `old_tpl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '旧的模板名称',
  `create_time` int(20) DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='升级日志(es_upgrade_log)';

-- ----------------------------
-- Table structure for es_uploader
-- ----------------------------
DROP TABLE IF EXISTS `es_uploader`;
CREATE TABLE `es_uploader` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '储存id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '存储名称',
  `open` int(2) DEFAULT NULL COMMENT '是否开启',
  `config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '存储配置',
  `bean` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '存储插件id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='存储方案(es_uploader)';

-- ----------------------------
-- Table structure for es_waybill
-- ----------------------------
DROP TABLE IF EXISTS `es_waybill`;
CREATE TABLE `es_waybill` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '电子面单id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `open` int(2) DEFAULT NULL COMMENT '是否开启',
  `config` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '电子面单配置',
  `bean` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'beanid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='电子面单(es_waybill)';

-- ----------------------------
-- Table structure for es_wechat_msg_template
-- ----------------------------
DROP TABLE IF EXISTS `es_wechat_msg_template`;
CREATE TABLE `es_wechat_msg_template` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `msg_tmp_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '模板名称',
  `msg_tmp_sn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '消息编号',
  `template_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '模板ID',
  `msg_first` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '消息开头文字',
  `msg_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '消息结尾备注文字',
  `is_open` smallint(1) DEFAULT NULL COMMENT '是否开启',
  `tmp_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '模板类型，枚举',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for es_withdraw_apply
-- ----------------------------
DROP TABLE IF EXISTS `es_withdraw_apply`;
CREATE TABLE `es_withdraw_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `apply_money` decimal(20,2) DEFAULT NULL COMMENT '提现金额',
  `member_id` int(11) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `apply_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '申请备注',
  `inspect_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核备注',
  `transfer_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '转账备注',
  `apply_time` int(11) DEFAULT NULL COMMENT '申请时间',
  `inspect_time` int(11) DEFAULT NULL COMMENT '审核时间',
  `transfer_time` int(11) DEFAULT NULL COMMENT '转账时间',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '状态',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '编号',
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ip地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='提现申请(es_withdraw_apply)';

-- ----------------------------
-- Table structure for es_withdraw_bill
-- ----------------------------
DROP TABLE IF EXISTS `es_withdraw_bill`;
CREATE TABLE `es_withdraw_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `withdraw_record_id` int(11) DEFAULT NULL COMMENT '提现记录id',
  `bill_id` int(11) DEFAULT NULL COMMENT '结算单id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=323 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_withdraw_record
-- ----------------------------
DROP TABLE IF EXISTS `es_withdraw_record`;
CREATE TABLE `es_withdraw_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `apply_money` decimal(20,2) DEFAULT NULL COMMENT '提现金额',
  `member_id` int(11) DEFAULT NULL COMMENT '会员id',
  `member_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `seller_id` int(11) DEFAULT NULL COMMENT '商铺id',
  `seller_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商铺名称',
  `apply_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '申请备注',
  `inspect_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核备注',
  `transfer_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '转账备注',
  `apply_time` bigint(20) DEFAULT NULL COMMENT '申请时间',
  `inspect_time` bigint(20) DEFAULT NULL COMMENT '审核时间',
  `transfer_time` bigint(20) DEFAULT NULL COMMENT '转账时间',
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '状态',
  `bank_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开户银行支行名称',
  `bank_account_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行开户名',
  `bank_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '银行开户账号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for es_withdraw_setting
-- ----------------------------
DROP TABLE IF EXISTS `es_withdraw_setting`;
CREATE TABLE `es_withdraw_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(11) DEFAULT NULL COMMENT '用户id',
  `param` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='分销提现设置(es_withdraw_setting)';

-- ----------------------------
-- Table structure for report_market
-- ----------------------------
DROP TABLE IF EXISTS `report_market`;
CREATE TABLE `report_market` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '关联名称',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'title',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '注释',
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '0(sql) 1(表) 3 (视图)',
  `source` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '0:查询sql;1:表名称;2:视图名称',
  `theme_id` int(11) DEFAULT NULL COMMENT '主题id',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `del_flag` int(11) DEFAULT NULL COMMENT '删除标记',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `latest_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for report_market_field
-- ----------------------------
DROP TABLE IF EXISTS `report_market_field`;
CREATE TABLE `report_market_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '对应字段',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '对应字段标题',
  `market_id` int(11) DEFAULT NULL,
  `market_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '对应market的name ,关联字段',
  `mapping` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '映射形式',
  `show_no` int(11) DEFAULT NULL COMMENT '排序',
  `mapping_type` int(11) DEFAULT NULL COMMENT '映射关系类型',
  `field_type` int(11) DEFAULT NULL COMMENT '字段类型(0:text 1:select 2:time)',
  `show_flag` int(11) DEFAULT NULL COMMENT '是否显示 0:显示,1不显示',
  `query_flag` int(11) DEFAULT NULL COMMENT '查询标记 0:不 1:查询',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `del_flag` int(11) DEFAULT NULL COMMENT '删除标记',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `latest_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for sys_seq
-- ----------------------------
DROP TABLE IF EXISTS `sys_seq`;
CREATE TABLE `sys_seq` (
  `seq_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键:序列名称，建议直接用表名，统一小写',
  `allocated_value` bigint(20) NOT NULL DEFAULT '0' COMMENT '当前负载最大值，当redis中当前值达到该值时，需要增加cache_num',
  `cache_num` int(7) NOT NULL DEFAULT '100' COMMENT '缓存数量',
  `remarks` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`seq_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='序列表';

-- ----------------------------
-- Table structure for wms_order
-- ----------------------------
DROP TABLE IF EXISTS `wms_order`;
CREATE TABLE `wms_order` (
  `delivery_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '出库单 主键ID',
  `delivery_sn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '出库编号',
  `sorting_batch` int(10) DEFAULT NULL COMMENT '分拣批次',
  `sorting_seq` int(10) DEFAULT NULL COMMENT '分拣序列',
  `order_num` int(10) DEFAULT NULL,
  `order_seq` int(10) DEFAULT NULL,
  `sorting_seq_list` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `delivery_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出库状态',
  `claims_id` int(10) DEFAULT NULL COMMENT '理赔单id',
  `order_id` int(10) DEFAULT NULL COMMENT '订单id',
  `order_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `order_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '上游来源类型 NORMAL 商城 SHETUAN 社团 EXCEPTION 异常单',
  `items_json` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '货物列表json',
  `delivery_date` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '出库日期',
  `delivery_time` bigint(20) DEFAULT NULL COMMENT '实际出库时间',
  `warehouse_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '仓库分拣单号',
  `print_times` tinyint(5) DEFAULT '0' COMMENT '打印次数',
  `seller_id` int(10) DEFAULT NULL COMMENT '店铺id',
  `refuse_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拒绝出库原因',
  `ship_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '收货人姓名',
  `ship_mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货人手机',
  `ship_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '收货地址',
  `site_id` int(10) DEFAULT NULL COMMENT '站点id',
  `site_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '站点姓名',
  `site_mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '站点手机',
  `site_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '站点地址',
  `shipping_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '配送方式',
  `operator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '出库操作人',
  `create_time` bigint(10) DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出库备注',
  `delivery_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分拣模式 B+C B',
  `receive_time` bigint(20) DEFAULT NULL COMMENT '收货时间',
  `receive_time_type` tinyint(4) DEFAULT NULL COMMENT '客订收货时间类型 0次日达 1当日达',
  PRIMARY KEY (`delivery_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=120910 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for wms_order_items
-- ----------------------------
DROP TABLE IF EXISTS `wms_order_items`;
CREATE TABLE `wms_order_items` (
  `item_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `wms_order_id` int(10) DEFAULT NULL COMMENT '出库单id',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品ID',
  `sku_id` int(10) DEFAULT NULL COMMENT '货品ID',
  `ship_num` int(10) DEFAULT NULL COMMENT '发货量',
  `order_sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单编号',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商品名称',
  `price` decimal(20,2) DEFAULT NULL COMMENT '销售金额',
  `cat_id` int(10) DEFAULT NULL COMMENT '分类ID',
  `snapshot_id` int(10) DEFAULT NULL COMMENT '快照id',
  `spec_json` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '规格json',
  `ship_status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '货运状态',
  `ship_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货单号',
  `logi_id` int(10) DEFAULT NULL COMMENT '物流公司ID',
  `logi_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '物流公司名称',
  `supplier_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '供应商姓名',
  `goods_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货商品类型 正常 normal 补发 forget 赠品 gift',
  PRIMARY KEY (`item_id`) USING BTREE,
  KEY `idx_sku_id` (`sku_id`) USING BTREE,
  KEY `idx_wms_order` (`wms_order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=165993 DEFAULT CHARSET=utf8 COMMENT='出库货物表(wms_delivery_items)';

-- ----------------------------
-- Table structure for xxl_job_group
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_group`;
CREATE TABLE `xxl_job_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行器AppName',
  `title` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行器名称',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `address_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '执行器地址类型：0=自动注册、1=手动录入',
  `address_list` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器地址列表，多地址逗号分隔',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for xxl_job_info
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_info`;
CREATE TABLE `xxl_job_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_group` int(11) NOT NULL COMMENT '执行器主键ID',
  `job_cron` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务执行CRON',
  `job_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `author` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作者',
  `alarm_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '报警邮件',
  `executor_route_strategy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器路由策略',
  `executor_handler` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务参数',
  `executor_block_strategy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '阻塞处理策略',
  `executor_timeout` int(11) NOT NULL DEFAULT '0' COMMENT '任务执行超时时间，单位秒',
  `executor_fail_retry_count` int(11) NOT NULL DEFAULT '0' COMMENT '失败重试次数',
  `glue_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'GLUE备注',
  `glue_updatetime` datetime DEFAULT NULL COMMENT 'GLUE更新时间',
  `child_jobid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '子任务ID，多个逗号分隔',
  `trigger_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '调度状态：0-停止，1-运行',
  `trigger_last_time` bigint(13) NOT NULL DEFAULT '0' COMMENT '上次调度时间',
  `trigger_next_time` bigint(13) NOT NULL DEFAULT '0' COMMENT '下次调度时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for xxl_job_lock
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_lock`;
CREATE TABLE `xxl_job_lock` (
  `lock_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '锁名称',
  PRIMARY KEY (`lock_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for xxl_job_log
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_log`;
CREATE TABLE `xxl_job_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `job_group` int(11) NOT NULL COMMENT '执行器主键ID',
  `job_id` int(11) NOT NULL COMMENT '任务，主键ID',
  `executor_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器地址，本次执行的地址',
  `executor_handler` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务参数',
  `executor_sharding_param` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务分片参数，格式如 1/2',
  `executor_fail_retry_count` int(11) NOT NULL DEFAULT '0' COMMENT '失败重试次数',
  `trigger_time` datetime DEFAULT NULL COMMENT '调度-时间',
  `trigger_code` int(11) NOT NULL COMMENT '调度-结果',
  `trigger_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '调度-日志',
  `handle_time` datetime DEFAULT NULL COMMENT '执行-时间',
  `handle_code` int(11) NOT NULL COMMENT '执行-状态',
  `handle_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '执行-日志',
  `alarm_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '告警状态：0-默认、1-无需告警、2-告警成功、3-告警失败',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `I_trigger_time` (`trigger_time`) USING BTREE,
  KEY `I_handle_code` (`handle_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=213284 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for xxl_job_log_report
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_log_report`;
CREATE TABLE `xxl_job_log_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trigger_day` datetime DEFAULT NULL COMMENT '调度-时间',
  `running_count` int(11) NOT NULL DEFAULT '0' COMMENT '运行中-日志数量',
  `suc_count` int(11) NOT NULL DEFAULT '0' COMMENT '执行成功-日志数量',
  `fail_count` int(11) NOT NULL DEFAULT '0' COMMENT '执行失败-日志数量',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `i_trigger_day` (`trigger_day`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=795 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for xxl_job_logglue
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_logglue`;
CREATE TABLE `xxl_job_logglue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL COMMENT '任务，主键ID',
  `glue_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'GLUE备注',
  `add_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for xxl_job_registry
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_registry`;
CREATE TABLE `xxl_job_registry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `registry_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `registry_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `i_g_k_v` (`registry_group`,`registry_key`(191),`registry_value`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for xxl_job_user
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_user`;
CREATE TABLE `xxl_job_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `role` tinyint(4) NOT NULL COMMENT '角色：0-普通用户、1-管理员',
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '权限：执行器ID列表，多个逗号分割',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `i_username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Procedure structure for pro_sorting_no_gen
-- ----------------------------
DROP PROCEDURE IF EXISTS `pro_sorting_no_gen`;
delimiter ;;
CREATE PROCEDURE `pro_sorting_no_gen`(IN date_str varchar(20))
begin
	
	
	declare batch_num  int DEFAULT 100	;	/**每次分批的订单数量*/
	set @create_time = now();
	
	truncate table es_sorting_serial;

	truncate table es_sorting_sku_map;

	truncate table es_sorting_order_level_tmp;
	set  @i:=-1 ; /**定义起始变量*/
	
	

	/**订单按站点名与派送优先级排序*/
	
	
	insert into es_sorting_order_level_tmp (order_sn,leader_name,`level`)
			select
				o.sn 'order_sn',case when le.leader_name is null then 'C端' else le.leader_name end ,
				case when sl.`level` is null  then 999 else sl.`level` end 'level'
	
			from
				es_order o  left join es_leader  le on le.leader_id = o.leader_id
				left join es_sorting_level sl on sl.pick_id = o.leader_id
			where
				o.seller_id = 193
				and from_unixtime(o.create_time) between concat(date_str,' 00:00:00') and concat(date_str,' 23:59:59')
				-- and from_unixtime(o.create_time) between concat('2020-10-24 00:00:00') and concat('2020-10-25 23:59:59')
				and o.order_status = 'PAID_OFF'
				and o.service_status != 'APPLY' 
				-- 衢山镇除外 ，这个地方，不按C端进行分拣
				and not exists (select 1 from es_sorting_byb  byb where byb.leader_id=o.leader_id)
				order by le.leader_name;
			
	/** 生成格子数据*/
		insert into es_sorting_serial(order_sn,sn,status,bat_no,leader_name,create_time,data_time) 
			select
				lt.order_sn ,mod((@i:= @i+1),100)+1 ,0,floor(@i/100)+1 ,lt.leader_name,
				@create_time ,date_str 
			from
				 es_sorting_order_level_tmp  lt  order by `level`,leader_name;
			
						
	/**按sku与对应的订单关联*/
	
	insert into es_sorting_sku_map(order_sn,sku_name,sku_num,serial_no,supplier,bat_no,create_time,data_time)
	select
		o.sn as 'order_sn' ,
		oi.name as 'sku_name' ,
		oi.num as 'sku_num',
		ess.sn,
		g.supplier_name,
		ess.bat_no,
		@create_time,
		date_str
	from
		es_order o
	left join es_order_items oi on
		o.sn = oi.order_sn
	left join es_goods g on
		g.goods_id = oi.goods_id
	left join es_goods_sku sku on
		oi.product_id = sku.sku_id
		left join es_sorting_serial  ess on ess.order_sn = o.sn
	where
		o.seller_id = 193 
		  and from_unixtime(o.create_time) between concat(date_str,' 00:00:00') and concat(date_str,' 23:59:59')
		  -- and from_unixtime(o.create_time) between concat('2020-10-24 00:00:00') and concat('2020-10-25 23:59:59')
		and o.order_status = 'PAID_OFF'
		and o.service_status != 'APPLY'
		order by oi.name,ess.sn;
	
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
