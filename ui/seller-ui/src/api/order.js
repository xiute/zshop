/**
 * 订单相关API
 */

import request from '@/utils/request'

export function queryPrint(params) {
  return request({
    url: '/seller/waybill',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取订单列表
 * @param params
 * @returns {Promise<any>}
 */
export function getOrderList(params) {
  return request({
    url: 'seller/trade/orders',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取订单列表
 * @param params
 * @returns {Promise<any>}
 */
export function getByOrderSnList(params) {
  return request({
    url: `/seller/trade/orders/getByOrderSnList/${params}`,
    method: 'get',
    loading: false
  })
}

/**
 * 根据订单sn获取订单详情
 * @param sn
 * @returns {Promise<any>}
 */
export function getOrderDetail(sn) {
  return request({
    url: `seller/trade/orders/${sn}`,
    method: 'get',
    loading: false
  })
}

/**
 * 调整价格
 * @param sn
 * @returns {Promise<any>}
 */
export function updateOrderPrice(sn, params) {
  return request({
    url: `seller/trade/orders/${sn}/price`,
    method: 'put',
    loading: false,
    data: params
  })
}

/**
 * 修改收货人信息
 * @param sn
 * @param params
 * @returns {Promise<any>}
 */
export function updateConsigneeInfo(sn, params) {
  return request({
    url: `seller/trade/orders/${sn}/address`,
    method: 'put',
    loading: false,
    data: params
  })
}

/**
 * 确认收款
 * @param sn
 * @param params
 * @returns {Promise<any>}
 */
export function confirmGetAmount(sn, params) {
  return request({
    url: `seller/trade/orders/${sn}/pay`,
    method: 'post',
    loading: false,
    data: params
  })
}

/**
 * 发货
 * @param ids
 * @param params
 * @returns {Promise<any>}
 */
export function deliveryGoods(sn, params) {
  return request({
    url: `seller/trade/orders/${sn}/delivery`,
    method: 'post',
    loading: false,
    data: params
  })
}

/**
 * 查询快递物流信息
 * @param params
 * @returns {Promise<any>}
 */
export function getLogisticsInfo(params) {
  return request({
    url: `seller/express`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 生成电子面单
 * @param params
 * @returns {Promise<any>}
 */
export function generateElectronicSurface(params) {
  return request({
    url: `seller/waybill`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取订单流程图数据
 * @param ids
 * @param params
 * @returns {Promise<any>}
 */
export function getStepList(ids) {
  return request({
    url: `seller/trade/orders/${ids}/flow`,
    method: 'get',
    loading: false
  })
}

/**
 * 导出订单
 * @param params
 */
export function exportGoodsOrder(params) {
  return request({
    responseType: 'blob',
    url: '/seller/promotion/shetuan/group_purchase_export',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    loading: true,
    data: params
  })
}

/**
  * @date 2020/09/02
  * @author kaiqiang
  * @description { 多退少补添加 }
*/
export function addRetreat(array, params) {
  return request({
    url: '/seller/after-sales/refund/spread/' + array,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    loading: true,
    data: params
  })
}

/**
  * @date 2020/09/03
  * @author kaiqiang
  * @description { 获取多退少补列表 }
*/
export function getRetreatList(params) {
  return request({
    url: '/seller/after-sales/refund/spread',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
  * @date 2020/09/04
  * @author kaiqiang
  * @description { 导出退补明细 }
*/
export function getExportList(params) {
  return request({
    url: '/seller/after-sales/refund/spread/export',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params,
    responseType: 'blob',
    loading: true
  })
}

/**
  * @date 2020/11/26
  * @author kaiqiang
  * @description { 出库配送时间 }
*/
export function getSortBatchList() {
  return request({
    url: '/seller/wms/order/sortBatchList',
    method: 'get',
    loading: true
  })
}
/**
 * 导出订单明细
 * @param params
 */
export function exportOrderItems(params) {
  return request({
    responseType: 'blob',
    url: 'seller/excel/exportOrdersItem',
    method: 'get',
    timeout: 0,
    params
  })
}

/**
  * @date 2020/12/02
  * @author kaiqiang
  * @description { 获取发货信息 }
*/
export function getOrdersShipDetail(sn) {
  return request({
    url: `/seller/trade/orders/ship_detail/${sn}`,
    method: 'get'
  })
}

/**
  * @date 2020/12/02
  * @author kaiqiang
  * @description { 分批包裹确认发货 }
*/
export function getOrdersSplitDelivery(params) {
  return request({
    url: '/seller/trade/orders/split/delivery',
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: true,
    data: params
  })
}
