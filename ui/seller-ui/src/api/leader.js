/**团长管理api */
import request from '@/utils/request'

/**
  * @date 2020/06/17
  * @author kjh
  * @description {页面初始化获取团长信息列表}
*/

export function getLeaderList(params) {
  return request({
    url: 'seller/members/leader/list',
    method: 'get',
    loading: false,
    params
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {获取团长信息详情列}
*/
export function askLeaderList(params) {
  return request({
    url: `seller/members/leader/get/${params}`,
    method: 'get',
    loading: false 
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {发送添加团长请求}
*/
export function addLeaderList(params) {
  return request({
    url: 'seller/members/leader/add_leader',
    method: 'post',
    loading: false,
    params
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {发送修改团长信息请求}
*/
export function updateLeaderList(params) {
  return request({
    url: 'seller/members/leader/update_leader',
    method: 'post',
    loading: false,
    params
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {发送删除单行团长信息请求}
*/
export function deleteLeaderList(params) {
  return request({
    url: `seller/members/leader/${params}`,
    method: 'delete',
    loading: false 
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {查询单个会员信息}
*/
export function getMemberList(params) {
  return request({
    url: 'seller/members/',
    method: 'get',
    loading: false,
    params
  })
}

/**团购订单api */
/**
  * @date 2020/06/17
  * @author kjh
  * @description {页面初始化获取团购订单列表}
*/
export function getOrderList(params) {
  return request({
    url: '/seller/promotion/shetuan/shetuanOrderPage',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**团购订单api */
/**
  * @date 2020/8/7
  * @author kjh
  * @description {商品的批量发货}
*/
export function apiDeliveryOrder(_ids, _params) {
  return request({
    url: `/seller/promotion/shetuan/${_ids}/delivery`,
    method: 'post',
    loading: false,
    data: _params
  })
}

/**
  * @date 2020/10/15
  * @author kaiqiang
  * @description {获取自提点列表}
*/
export function querySiteName(params) {
  return request({
    url: '/seller/trade/orders/query_leader_by_site_name',
    method: 'get',
    loading: false,
    params
  })
}

/**
  * @date 2020/10/16
  * @author xlg
  * @description {修改收货人地址}
*/
export function updOrderAddress(params) {
  return request({
    url: '/seller/trade/orders/upd_order_address',
    method: 'put',
    loading: false,
    params
  })
}

/**
  * @date 2021/01/22
  * @author kaiqiang
  * @description { 创建团长任务 }
*/
export function addDistributionMission(params) {
  return request({
    url: '/seller/distribution/mission/create_mission',
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: false,
    data: params
  })
}

/**
  * @date 2021/01/22
  * @author kaiqiang
  * @description { 查询团长任务 }
*/
export function getDistributionMission(params) {
  return request({
    url: '/seller/distribution/mission/query_mission_list',
    method: 'get',
    loading: false,
    params
  })
}

/**
  * @date 2021/01/22
  * @author kaiqiang
  * @description { 删除团长任务 }
*/
export function deleteDistributionMission(params) {
  return request({
    url: '/seller/distribution/mission/delete_mission',
    method: 'post',
    loading: true,
    data: params
  })
}

/**
  * @date 2021/01/26
  * @author kaiqiang
  * @description { 修改团长任务 }
*/
export function updateDistributionMission(params) {
  return request({
    url: '/seller/distribution/mission/update_mission',
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: true,
    data: params
  })
}

/**
  * @date 2021/01/26
  * @author kaiqiang
  * @description { 创建团长任务详情 }
*/
export function addDistributionCreateMissionDetails(params) {
  return request({
    url: '/seller/distribution/mission/create_mission_details',
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: true,
    data: params
  })
}

/**
  * @date 2021/01/26
  * @author kaiqiang
  * @description { 上线或者下线 }
*/
export function onOffDistributionMission(mission_status, mission_id) {
  return request({
    url: '/seller/distribution/mission/on_off_line',
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: true,
    params: {
      'mission_status': mission_status,
      'mission_id': mission_id
    }
  })
}

/**
  * @date 2021/01/26
  * @author kaiqiang
  * @description { 查询团长任务进度列表 }
*/
export function getQueryMissionScheduleList(params) {
  return request({
    url: '/seller/distribution/mission/query_mission_schedule_list',
    method: 'get',
    loading: true,
    params
  })
}

/**
  * @date 2021/01/26
  * @author kaiqiang
  * @description { 查询团长任务进度列表 }
*/
export function getDistributionReward(params) {
  return request({
    url: '/seller/distribution/reward',
    method: 'get',
    loading: false,
    params
  })
}

/**
  * @date 2021/01/26
  * @author kaiqiang
  * @description { 通过id查询团长的任务 }
*/
export function getMissionDetailByMisId(mission_id) {
  return request({
    url: `/seller/distribution/mission/getMissionDetailByMisId/${mission_id}`,
    method: 'get',
    loading: false
  })
}
