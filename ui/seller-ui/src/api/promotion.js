/**
 * 活动相关API
 */

import request from '@/utils/request'

export function getShetuanActives(params) {
  return request({
    url: '/seller/promotion/shetuan/page',
    method: 'get',
    loading: false,
    params
  })
}

export function addShetuanActive(params) {
  return request({
    url: '/seller/promotion/shetuan/add',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function pushShetuanActive(params) {
  return request({
    url: '/seller/promotion/shetuan/activity_push_information',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function editShetuanActive(params) {
  return request({
    url: '/seller/promotion/shetuan/edit',
    method: 'post',
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function getShetuanGoodsList(params) {
  return request({
    url: '/seller/promotion/shetuan/goods_page',
    method: 'post',
    loading: false,
    data: params,
    headers: { 'Content-Type': 'application/json' }

  })
}

export function addGoodsList(params) {
  return request({
    url: '/seller/promotion/shetuan/add_goods',
    method: 'post',
    loading: false,
    data: params,
    headers: { 'Content-Type': 'application/json' }

  })
}

export function deleteGoodsByIds(params) {
  return request({
    url: '/seller/promotion/shetuan/delete_goods',
    method: 'post',
    loading: false,
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function upGoodsByIds(params) {
  return request({
    url: '/seller/promotion/shetuan/up_goods',
    method: 'post',
    loading: false,
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function dropGoodsByIds(params) {
  return request({
    url: '/seller/promotion/shetuan/drop_goods',
    method: 'post',
    loading: false,
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function updatePrice(params) {
  return request({
    url: '/seller/promotion/shetuan/update_goods_price',
    method: 'post',
    loading: false,
    data: params,
    headers: { 'Content-Type': 'application/json' }

  })
}

export function updateRate(params) {
  return request({
    url: '/seller/promotion/shetuan/update_goods_rate',
    method: 'post',
    loading: false,
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

export function updateNum(params) {
  return request({
    url: '/seller/promotion/shetuan/update_goods_num',
    method: 'post',
    loading: false,
    data: params,
    headers: { 'Content-Type': 'application/json' }

  })
}

/**
  * @date 2020/06/16
  * @author lxw
  * @description {复制团购活动}
*/
export function copyActivity(params) {
  return request({
    url: '/seller/promotion/shetuan/copyShetuan',
    method: 'post',
    loading: false,
    data: params,
    headers: { 'Content-Type': 'application/json' }
  })
}

/**
  * @date 2020/06/08
  * @author lxw
  * @description {下线请求}
*/
export function postShetuanOffline(id) {
  return request({
    url: `/seller/promotion/shetuan/off_line/${id}`,
    method: 'post',
    loading: false
  })
}

/**
  * @date 2020/06/08
  * @author lxw
  * @description {上线请求}
*/
export function postShetuanOnline(id) {
  return request({
    url: `/seller/promotion/shetuan/on_line/${id}`,
    method: 'post',
    loading: false
  })
}

/**
  * @date 2020/06/08
  * @author lxw
  * @description {删除活动请求}
*/
export function delShetuanActivity(id) {
  return request({
    url: `/seller/promotion/shetuan/shetuan_delete_new/${id}`,
    method: 'delete',
    loading: false
  })
}

/**
  * @date 2020/06/10
  * @author lxw
  * @description {修改商品参数}
*/
export function amendGoodsInfo(obj) {
  return request({
    url: `/seller/promotion/shetuan/editGoods`,
    method: 'post',
    loading: false,
    data: obj,
    headers: { 'Content-Type': 'application/json' }
  })
}

/**
  * @date 2020/08/18
  * @author kjh
  * @description {单个修改社区团购商品}
*/
export function apiUpdateGoods(obj) {
  return request({
    url: `/seller/promotion/shetuan/editOneGoods`,
    method: 'post',
    loading: false,
    data: obj,
    headers: { 'Content-Type': 'application/json' }
  })
}

/**
  * @date 2020/06/11
  * @author lxw
  * @description {商品上下线}
*/
export function amendGoodStatus(id, status) {
  return request({
    url: `/seller/promotion/shetuan/op_line/${id}/${status}`,
    method: 'post',
    loading: false
  })
}

/**
  * @date 2020/10/20
  * @author kaiqiang
  * @description { 导出excel }
*/
export function shetuanGoodsExport(params) {
  return request({
    url: '/seller/promotion/shetuan/shetuanGoodsExport',
    method: 'get',
    loading: true,
    responseType: 'blob',
    params
  })
}

/**
  * @date 2020/10/20
  * @author kaiqiang
  * @description { 导入 }
*/
export function shetuanGoodsImport(params) {
  return request({
    url: '/seller/promotion/shetuan/shetuanGoodsImport',
    method: 'post',
    headers: { 'Content-Type': 'multipart/form-data' },
    responseType: 'blob',
    loading: true,
    data: params
  })
}

/**
  * @date 2021/02/03
  * @author luocheng
  * @description { 抽奖活动 }
*/
export function queryLuckList(params) {
  return request({
    url: '/seller/promotion/luck/query_luck_list',
    method: 'get',
    loading: false,
    params
  })
}

/**
  * @date 2021/02/03
  * @author luocheng
  * @description { 抽奖活动奖品列表 }
*/
export function queryLuckPrizeList(params) {
  return request({
    url: '/seller/promotion/luck/query_luck_prize_list',
    method: 'get',
    loading: true,
    params
  })
}

/**
  * @date 2021/02/03
  * @author luocheng
  * @description { 抽奖记录导出 }
*/
export function luckRecordExport(params) {
  return request({
    url: '/seller/promotion/luck/luck_record_export',
    method: 'get',
    loading: true,
    responseType: 'blob',
    params
  })
}

/**
  * @date 2021/02/03
  * @author luocheng
  * @description { 抽奖记录 }
*/
export function queryLuckRecordList(params) {
  return request({
    url: '/seller/promotion/luck/query_luck_record_list',
    method: 'get',
    loading: false,
    params
  })
}

/**
  * @date 2021/02/03
  * @author luocheng
  * @description { 抽奖记录 }
*/
export function getCouponListByType() {
  return request({
    url: '/seller/promotion/coupons/getListByType?coupon_type=MARKETING_VOUCHER',
    method: 'get',
    loading: false
  })
}

/**
  * @date 2021/02/03
  * @author luocheng
  * @description { 活动上下线 }
*/
export function onOffLine(params) {
  return request({
    url: '/seller/promotion/luck/on_off_line',
    method: 'post',
    loading: false,
    data: params
  })
}

/**
  * @date 2021/02/03
  * @author luocheng
  * @description {删除抽奖活动}
*/
export function deleteLuck(luck_id) {
  return request({
    url: `/seller/promotion/luck/deleteLuck/${luck_id}`,
    method: 'post',
    loading: false
  })
}

/**
  * @date 2021/02/03
  * @author luocheng
  * @description {创建或修改抽奖活动}
*/
export function addOrUpdateLuck(params) {
  return request({
    url: `/seller/promotion/luck/addOrUpdateLuck`,
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: false,
    data: params
  })
}

/**
  * @date 2021/02/03
  * @author luocheng
  * @description {创建抽奖奖品}
*/
export function addLuckPrize(params) {
  return request({
    url: `/seller/promotion/luck/addLuckPrize`,
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: false,
    data: params
  })
}

/**
  * @date 2021/02/05
  * @author luocheng
  * @description {修改抽奖奖品}
*/
export function updateLuckPrize(params) {
  return request({
    url: `/seller/promotion/luck/updateLuckPrize`,
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: false,
    data: params
  })
}
/**
  * @date 2021/02/05
  * @author luocheng
  * @description {修改抽奖奖品}
*/
export function exchangePrize(params) {
  return request({
    url: `/seller/promotion/luck/exchangePrize`,
    method: 'post',
    data: params
  })
}
