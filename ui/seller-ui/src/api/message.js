/**
 * 消息相关API
 */

import request from '@/utils/request'

/**
 * 获取消息列表
 * @param params
 * @returns {Promise<any>}
 */
export function getMsgsList(params) {
  return request({
    url: 'seller/shops/shop-notice-logs',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 删除消息
 * @param ids
 * @param params
 * @returns {Promise<any>}
 */
export function deleteMsgs(ids) {
  return request({
    url: `seller/shops/shop-notice-logs/${ids}`,
    method: 'delete',
    loading: false
  })
}

/**
 * 标记消息为已读
 * @param ids
 * @param params
 * @returns {Promise<any>}
 */
export function signMsgs(ids) {
  return request({
    url: `seller/shops/shop-notice-logs/${ids}/read`,
    method: 'put',
    loading: false
  })
}

/**
 * 异常添加
 */
export function addExceptionOrder(params) {
  return request({
    url: '/seller/after-sales/exception_order/add_exception_order',
    method: 'post',
    loading: false,
    data: params
  })
}

/**
 * 异常添加
 */
export function getExceptionOrderList(params) {
  return request({
    url: '/seller/after-sales/exception_order/list',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: {
      page_no: params.page_no,
      page_size: params.page_size,
      time_type: params.time_type,
      exception_sn: params.exception_sn,
      order_sn: params.order_sn,
      member_nickname: params.member_nickname,
      exception_status: params.exception_status,
      exception_source: params.exception_source,
      exception_type: params.exception_type,
      member_realname_lv1: params.member_realname_lv1,
      site_name: params.site_name,
      ship_name: params.ship_name,
      ship_mobile: params.ship_mobile,
      start_time: params.start_time,
      end_time: params.end_time
    }
  })
}

/**
 * 获取异常下拉框
 */
export function queryExceptionEnums() {
  return request({
    url: '/seller/after-sales/exception_order/query_exception_enums',
    method: 'get',
    loading: false
  })
}

/**
 * 异常处理
 */
export function getQuerySloveMessage(id) {
  return request({
    url: '/seller/after-sales/exception_order/query_slove_message/' + id,
    method: 'get',
    loading: false
  })
}

/**
 * 异常处理
 */
export function addClaims(claimsVOList) {
  return request({
    url: '/seller/after-sales/claims/addClaims',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    loading: true,
    data: claimsVOList
  })
}

/**
 * 赔付展示
 */
export function getClaimsList(params) {
  return request({
    url: '/seller/after-sales/claims/getClaimsList',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 确定处理完成
 */
export function getProcessSuccess(params) {
  return request({
    url: '/seller/after-sales/exception_order/process_success/' + params,
    method: 'put',
    loading: true
  })
}

/**
 * 异常撤销
 */
export function getExceptionOrderCancel(params) {
  return request({
    url: '/seller/after-sales/exception_order/cancel/' + params,
    method: 'put',
    loading: true
  })
}

/**
 * 异常撤销
 */
export function exportExceptionOrder(params) {
  return request({
    url: '/seller/after-sales/exception_order/export',
    method: 'post',
    loading: true,
    responseType: 'blob',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 导入
 */
export function importExceptionOrder(params) {
  return request({
    url: '/seller/after-sales/exception_order/import',
    method: 'post',
    headers: { 'Content-Type': 'multipart/form-data' },
    data: params
  })
}

/**
 * 批量审核
 */
export function batchClaimsAudit(ids, params) {
  return request({
    url: '/seller/after-sales/claims/audit/' + ids,
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: true,
    params
  })
}

/**
 * 批量导出
 */
export function getClaimsExport(params) {
  return request({
    url: '/seller/after-sales/claims/claimsExport',
    method: 'get',
    loading: true,
    responseType: 'blob',
    params
  })
}

/**
 * 批量撤销
 */
export function getClaimsAudit(ids, params) {
  return request({
    url: '/seller/after-sales/claims/audit/' + ids,
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: true,
    params
  })
}

/**
 * 异常处理
 */
export function getAfterSalesList(id) {
  return request({
    url: '/seller/after-sales/claims/' + id,
    method: 'get',
    loading: true
  })
}

/**
 * 查询售后服务评分消息模版
 */
export function getMessageTemplete(order_sn) {
  return request({
    url: '/seller/after-sales/message/template/' + order_sn,
    method: 'get',
    loading: true
  })
}

/**
 * 发送短信
 */
export function sendMessage(params) {
  return request({
    url: '/seller/after-sales/exception_order/send_message',
    method: 'post',
    loading: true,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 获取优惠卷
 */
export function promotionCouponsList(params) {
  return request({
    url: '/seller/promotion/coupons/1/list',
    method: 'get',
    params
  })
}
