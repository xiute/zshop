/**
 * 限时活动相关API
 */

import request from '@/utils/request'

/**
 * 获取限时活动列表
 * @param params
 * @returns {Promise<any>}
 */
export function getLimitTimeActivityList(params) {
  return request({
    url: 'seller/promotion/seckill-applys/seckill',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取限时活动商品列表
 * @param params
 * @returns {Promise<any>}
 */
export function getLimitTimeGoodsList(params) {
  return request({
    url: 'seller/promotion/seckill-applys',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取限时活动详情
 * @param ids
 * @param params
 * @returns {Promise<any>}
 */
export function getLimitTimeActivityDetails(ids, params) {
  return request({
    url: `seller/promotion/seckill-applys/${ids}/seckill`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 限时活动报名
 * @param ids
 * @param params
 * @returns {Promise<any>}
 * @constructor
 */
export function signUpLimitTimeActivity(params) {
  return request({
    url: 'seller/promotion/seckill-applys',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 增加限时抢购
 * @param params
 */
export function addSeckill(params) {
  return request({
    url: 'seller/promotion/seckills',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 获取限时抢购详情
 * @param id
 */
export function getSeckillDetail(id) {
  return request({
    url: `seller/promotion/seckills/${id}`,
    method: 'get'
  })
}

/**
 * 删除限时抢购
 * @param id
 */
export function deleteSeckill(id) {
  return request({
    url: `/seller/promotion/seckill-applys/${id}`,
    method: 'delete'
  })
}

/**
 * 发布、修改限时秒杀
 * @param seckill_id
 * @param params
 */
export function setSeckill(seckill_id, params) {
  return request({
    url: `seller/promotion/seckill-applys/${seckill_id}/release`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 查看限时秒杀商品
 * @param params
 */
export function getSeckillGoods(params) {
  return request({
    url: `seller/promotion/seckill-applys`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 是否社区团购店铺
 * @param params
 */
export function isShetuanShop() {
  return request({
    url: `/seller/shops/isShetuanShop`,
    method: 'get',
    loading: false
  })
}

/**
 * 审核限时抢购秒杀
 * @param apply_id
 * @param status
 * @param fail_reason
 */
export function reviewSckillGoods(apply_id, status = 'yes', fail_reason) {
  return request({
    url: `seller/promotion/seckill-applys/review/${apply_id}`,
    method: 'post',
    data: {
      status,
      fail_reason
    }
  })
}
