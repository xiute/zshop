import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '@/views/layout/Layout'

Vue.use(Router)
let shopData = 1
if (JSON.parse(localStorage.getItem('shopData'))) {
  shopData = JSON.parse(localStorage.getItem('shopData')).community_shop
}
export const constantRouterMap = [
  { path: '/404', component: () => import('@/views/errorPage/404'), hidden: true },
  { path: '/login', component: () => import('@/views/login/login'), hidden: true },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      name: 'dashboard',
      meta: { title: 'dashboard', icon: 'dashboard' }
    }]
  }
]

export const asyncRouterMap = [
  // 商品管理
  {
    path: '/goods',
    component: Layout,
    redirect: '/goods/goods-list',
    name: 'goods',
    meta: {
      title: 'goods',
      icon: 'goods-manage'
    },
    children: [
      { path: 'goods-list', component: () => import('@/views/goods/goodsList'), name: 'goodsList', meta: { title: 'goodsList' }},
      { path: 'draft-list', component: () => import('@/views/goods/draftList'), name: 'draftList', meta: { title: 'draftList' }},
      { path: 'category-manage', component: () => import('@/views/goods/categoryManage'), name: 'categoryManage', meta: { title: 'categoryManage' }},
      { path: 'tag-manage', component: () => import('@/views/goods/tagManage'), name: 'tagManage', meta: { title: 'tagManage' }},
      { path: 'tag-add/:tag_id', component: () => import('@/views/goods/tagAdd'), name: 'tagAdd', meta: { title: 'tagAdd' }, hidden: true },
      { path: 'recycle-station', component: () => import('@/views/goods/recycleStation'), name: 'recycleStation', meta: { title: 'recycleStation' }},
      { path: 'understock', component: () => import('@/views/goods/understock'), name: 'understock', meta: { title: 'understock' }},
      { path: 'good-publish/:goodsid?/:isdraft?', component: () => import('@/views/goods/goodsPublish'), name: 'goodPublish', meta: { title: 'goodPublish' }, hidden: true },
      { path: 'good-publish-old/:goodsid?/:isdraft?', component: () => import('@/views/goods/goodsPublish-old'), name: 'goodPublishOld', meta: { title: 'goodPublishOld', noCache: true }, hidden: true }
    ]
  },
  // 订单管理
  {
    path: '/order',
    component: Layout,
    name: 'order',
    redirect: '/order/order-list',
    meta: { title: 'order', icon: 'order-manage' },
    children: [
      { path: 'order-list', component: () => import('@/views/order/orderList'), name: 'orderList', meta: { title: 'orderList' }},
      { path: 'detail/:sn', component: () => import('@/views/order/orderDetail'), name: 'orderDetail', hidden: true, meta: { title: 'orderDetail' }},
      { path: 'refund-list', component: () => import('@/views/order/refundList'), name: 'refundList', meta: { title: 'refundList' }},
      { path: 'logistics-manage', component: () => import('@/views/order/logisticsManage'), name: 'logisticsManage', meta: { title: 'logisticsManage' }},
      { path: 'settlement-manage', component: () => import('@/views/order/settlementManage'), name: 'settlementManage', meta: { title: 'settlementManage' }},
      { path: 'settlement-detail/:sn', component: () => import('@/views/order/settlementDetail'), name: 'settlementDetail', meta: { title: 'settlementDetail' }, hidden: true },
      { path: 'withdrawals-record', component: () => import('@/views/order/withdrawalsRecord'), name: 'withdrawalsRecord', meta: { title: 'withdrawalsRecord' }},
      { path: 'receipt-history', component: () => import('@/views/order/receiptHistory'), name: 'receiptHistory', meta: { title: 'receiptHistory' }},
      { path: 'receipt-detail/:history_id', component: () => import('@/views/order/receiptDetail'), name: 'receiptDetail', hidden: true, meta: { title: 'receiptDetail' }}
    ]
  },
  // 社区团购管理
  {
    path: '/leader',
    component: Layout,
    redirect: '/leader/leader-list',
    name: 'shetuan',
    meta: { title: 'shetuan', icon: 'user' },
    children: [
      // { path: 'leader-list', component: () => import('@/views/leader/leaderList'), name: 'leaderManage', meta: { title: 'leaderManage' }},
      { path: 'shetuan-list', component: () => import('@/views/promotions/shetuanList'), name: 'activity', meta: { title: 'activity' }},
      { path: 'shetuan-goods-list/:shetuan_id?/:isdraft?', component: () => import('@/views/promotions/shetuanGoodsList'), name: 'shetuanGoodsList', meta: { title: 'shetuanGoodsList', noCache: true }, hidden: true },
      { path: 'leader-order', component: () => import('@/views/leader/leaderOrder'), name: 'leaderOrder', meta: { title: 'leaderOrder' }},
      { path: 'more-refund', component: () => import('@/views/leader/moreRefund'), name: 'moreRefund', meta: { title: 'moreRefund' }},
      { path: 'leader-task', component: () => import('@/views/leader/leaderTask'), name: 'leaderTask', meta: { title: 'leaderTask' }, hidden: (shopData !== 1 ? 'true': false) },
      { path: 'lucky-buyer', component: () => import('@/views/leader/luckyBuyer'), name: 'luckyBuyer', meta: { title: 'luckyBuyer' }},
      { path: 'winner-list/:luck_id', component: () => import('@/views/leader/winnerList'), name: 'winnerList', meta: { title: 'winnerList' }, hidden: true },
      { path: 'task-progress', component: () => import('@/views/leader/taskProgress'), name: 'taskProgress', meta: { title: 'taskProgress' }, hidden: true },
      { path: 'task-reward', component: () => import('@/views/leader/taskReward'), name: 'taskReward', meta: { title: 'taskReward' }, hidden: true }
      // { path: 'leader-commission', component: () => import('@/views/leader/leaderCommission'), name: 'leaderCommission', meta: { title: 'leaderCommission' }}
    ]
  },
  // 客服管理
  {
    path: '/customer',
    component: Layout,
    redirect: '/customer/consultation',
    name: 'customer',
    meta: { title: 'customer', icon: 'user' },
    children: [
      { path: 'consultation', component: () => import('@/views/customer-service/consultation'), name: 'consultation', meta: { title: 'consultation' }},
      { path: 'comments-manage', component: () => import('@/views/order/commentsManage'), name: 'commentsManage', meta: { title: 'commentsManage' }},
      { path: 'message', component: () => import('@/views/customer-service/message'), name: 'message', meta: { title: 'message' }},
      { path: 'abnormal', component: () => import('@/views/customer-service/abnormal'), name: 'abnormal', meta: { title: 'abnormal' }},
      { path: 'abnormal-process/:sn', component: () => import('@/views/customer-service/abnormal-process'), name: 'abnormalProcess', hidden: true, meta: { title: 'abnormalProcess' }},
      { path: 'claim-settlement', component: () => import('@/views/customer-service/claim-settlement'), name: 'claimSettlement', meta: { title: 'claimSettlement' }}
    ]
  },
  // 店铺管理
  {
    path: '/shop',
    component: Layout,
    redirect: '/shop/shop-themes-pc',
    name: 'shop',
    meta: { title: 'shop', icon: 'shop-manage' },
    children: [
      { path: 'shop-themes-pc', component: () => import('@/views/shop/shopThemesPc'), name: 'shopThemesPc', meta: { title: 'shopThemesPc' }},
      { path: 'shop-themes-wap', component: () => import('@/views/shop/shopThemesWap'), name: 'shopThemesWap', meta: { title: 'shopThemesWap' }},
      { path: 'shop-slide', component: () => import('@/views/shop/shopSlide'), name: 'shopSlide', meta: { title: 'shopSlide' }},
      { path: 'shop-nav', component: () => import('@/views/shop/shopNav'), name: 'shopNav', meta: { title: 'shopNav' }}
    ]
  },
  // 促销管理
  {
    path: '/promotions',
    component: Layout,
    redirect: '/promotions/group-buy-manager',
    name: 'promotions',
    meta: { title: 'promotions', icon: 'promotions-manage' },
    children: [
      { path: 'full-cut', component: () => import('@/views/promotions/fullCut'), name: 'fullCut', meta: { title: 'fullCut' }},
      { path: 'single-cut', component: () => import('@/views/promotions/singleCut'), name: 'singleCut', meta: { title: 'singleCut' }},
      { path: 'second-half-price', component: () => import('@/views/promotions/secondHalfPrice'), name: 'secondHalfPrice', meta: { title: 'secondHalfPrice' }},
      { path: 'discount-manager', component: () => import('@/views/promotions/discountManager'), name: 'discountManager', meta: { title: 'discountManager' }},
      { path: 'gift-manager', component: () => import('@/views/promotions/giftManager'), name: 'giftManager', meta: { title: 'giftManager' }},
      { path: 'group-buy-manager', component: () => import('@/views/promotions/groupBuyManager'), name: 'groupBuyManager', meta: { title: 'groupBuyManager' }},
      { path: 'time-limit', component: () => import('@/views/promotions/timeLimit'), name: 'timeLimit', meta: { title: 'timeLimit' }},
      { path: 'add-time-limit/:id', component: () => import('@/views/promotions/addTimeLimit'), name: 'addTimeLimit', meta: { title: 'addTimeLimit', noCache: true }, hidden: true },
      { path: 'set-time-limit/:id', component: () => import('@/views/promotions/addTimeLimit'), name: 'setTimeLimit', meta: { title: 'setTimeLimit', noCache: true }, hidden: true },
      { path: 'activity-goods-data/:id', component: () => import('@/views/promotions/activityGoodsData'), name: 'activityGoodsData', meta: { title: 'activityGoodsData' }, hidden: true },
      { path: 'group-buy-goods/:goods_id?', component: () => import('@/views/promotions/groupBuyGoods'), name: 'groupBuyGoods', meta: { title: 'groupBuyGoods' }, hidden: true },
      { path: 'assembleManager', component: () => import('@/views/promotions/assembleManager'), name: 'assembleManager', meta: { title: 'assembleManager' }},
      { path: 'assemble/:ass_id?', component: () => import('@/views/promotions/assemble'), name: 'assemble', meta: { title: 'assemble' }, hidden: true },
      { path: 'assembleGoods/:promotion_id', component: () => import('@/views/promotions/assembleGoods'), name: 'assembleGoods', meta: { title: 'assembleGoods' }, hidden: true },
      { path: 'newComerActivity', component: () => import('@/views/promotions/newComerActivity'), name: 'newComerActivity', meta: { title: 'newComerActivity' }}
    ]
  },
  // 统计
  {
    path: '/statistics',
    component: Layout,
    redirect: '/statistics/generality-overview',
    name: 'statistics',
    meta: { title: 'statistics', icon: 'statistics-manage' },
    children: [
      { path: 'generality-overview', component: () => import('@/views/statistics/generalityOverview'), name: 'generalityOverview', meta: { title: 'generalityOverview' }},
      {
        path: '/statistics/goods-analysis',
        component: () => import('@/views/statistics/goodsAnalysis/index'),
        redirect: '/statistics/goods-analysis/goods-details',
        name: 'goodsAnalysis',
        meta: { title: 'goodsAnalysis' },
        children: [
          { path: 'goods-details', component: () => import('@/views/statistics/goodsAnalysis/goodsDetailsAnalysis'), name: 'goodsDetailsAnalysis', meta: { title: 'goodsDetailsAnalysis' }},
          { path: 'price-sales', component: () => import('@/views/statistics/goodsAnalysis/goodsPriceSales'), name: 'goodsPriceSales', meta: { title: 'goodsPriceSales' }},
          { path: 'hot-selling-goods', component: () => import('@/views/statistics/goodsAnalysis/hotSellingGoods'), name: 'hotSellingGoods', meta: { title: 'hotSellingGoods' }}
        ]
      },
      {
        path: '/statistics/operate-report',
        component: () => import('@/views/statistics/operateReport/index'),
        redirect: '/statistics/operate-report/regional-analysis',
        name: 'operateReport',
        meta: { title: 'operateReport' },
        children: [
          { path: 'regional-analysis', component: () => import('@/views/statistics/operateReport/regionalAnalysis'), name: 'regionalAnalysis', meta: { title: 'regionalAnalysis' }},
          { path: 'sales-statistics', component: () => import('@/views/statistics/operateReport/salesStatistics'), name: 'salesStatistics', meta: { title: 'salesStatistics' }},
          { path: 'buy-analysis', component: () => import('@/views/statistics/operateReport/buyAnalysis'), name: 'buyAnalysis', meta: { title: 'buyAnalysis' }}
        ]
      },
      { path: 'traffic-statistics', component: () => import('@/views/statistics/trafficStatistics'), name: 'trafficStatistics', meta: { title: 'trafficStatistics' }},
      { path: 'collect-statistics', component: () => import('@/views/statistics/collectStatistics'), name: 'collectStatistics', meta: { title: 'collectStatistics' }},
      { path: 'log-manage', component: () => import('@/views/statistics/logManage'), name: 'logManage', meta: { title: 'logManage' }, hidden: true }
    ]
  },

  // 仓库管理
  {
    path: '/warehouse',
    component: Layout,
    redirect: '/warehouse/wmsOrderList',
    name: 'warehouse',
    meta: { title: 'warehouse', icon: 'goods-manage' },
    children: [
      { path: 'wmsOrderList', component: () => import('@/views/warehouse/wmsOrderList'), name: 'wmsOrderList', meta: { title: 'wmsOrderList' }},
      { path: 'other', component: () => import('@/views/warehouse/other'), name: 'other', meta: { title: 'other' }}
    ]
  },

  // 设置
  {
    path: '/setting',
    component: Layout,
    redirect: '/setting/shop-setting',
    name: 'setting',
    meta: { title: 'setting', icon: 'setting-manage' },
    children: [
      { path: 'shop-setting', component: () => import('@/views/setting/shopSettings'), name: 'shopSettings', meta: { title: 'shopSettings' }},
      { path: 'goods-warning', component: () => import('@/views/setting/goodsWarning'), name: 'goodsWarning', meta: { title: 'goodsWarning' }},
      { path: 'grade-application', component: () => import('@/views/setting/gradeApplication'), name: 'gradeApplication', meta: { title: 'gradeApplication' }, hidden: true },
      { path: 'invoice-settings', component: () => import('@/views/setting/invoiceSettings'), name: 'invoiceSettings', meta: { title: 'invoiceSettings' }},
      { path: 'finance-setting', component: () => import('@/views/setting/financeSettings'), name: 'financeSettings', hidden: true, meta: { title: 'financeSettings' }}
    ]
  },
  // 店员管理
  {
    path: '/shop-auth',
    component: Layout,
    redirect: '/shop-auth/shop-assistant',
    name: 'shopAuth',
    meta: { title: 'shopAuth', icon: 'shop-assistant' },
    children: [
      { path: 'shop-assistant', component: () => import('@/views/shop-auth/shopAssistant'), name: 'shopAssistant', meta: { title: 'shopAssistant' }},
      { path: 'role-manage', component: () => import('@/views/shop-auth/roleManage'), name: 'roleManage', meta: { title: 'roleManage' }},
      { path: 'role-permission/:id(\\d+)', component: () => import('@/views/shop-auth/rolePermission'), name: 'rolePermission', hidden: true, meta: { title: 'rolePermission', noCache: true }}
    ]
  },
  // 分销管理
  // {
  //   path: '/retail',
  //   component: Layout,
  //   redirect: '/retail/retailAudit',
  //   name: 'retail',
  //   meta: { title: 'retail', icon: 'promotions-manage' },
  //   children: [
  //     { path: 'retail-audit', component: () => import('@/views/retail/retailAudit'), name: 'retailAudit', meta: { title: 'retailAudit' }}
  //   ]
  // },
  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
