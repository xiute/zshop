/**
 * Created by Andste on 2018/7/2.
 * base    : 基础业务API
 * buyer   : 买家API
 * seller  : 商家中心API
 * admin   ：后台管理API
 */

const env = process.server
  ? process.env
  : (global.window ? window.__NUXT__.state.env : {})

module.exports = {
  // 开发环境
  dev: {
    base: 'http://localhost:7000',
    buyer: 'http://localhost:7002',
    seller: 'http://localhost:7003',
    admin: 'http://localhost:7004'
  },
  // 生产环境
  pro: {
    base  : env.API_BASE || 'http://localhost:7000',
    buyer : env.API_BUYER || 'http://localhost:7002',
    seller: env.API_SELLER || 'http://localhost:7003',
    admin : env.API_ADMIN || 'http://localhost:7004'
  }
}
