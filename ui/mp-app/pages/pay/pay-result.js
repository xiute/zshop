// pages/pay/pay-result.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shareImage: '',
    goodsNum: '',
    orderSn: '',
    orderType: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const { img, num, order_sn, type } = options;
    this.setData({
      shareImage: img,
      goodsNum: num,
      orderSn: order_sn,
      orderType: type
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const { shareImage, goodsNum, orderSn } = this.data;
    return {
      title: `@团长 接单啦~我刚刚买了${goodsNum}个商品，感觉超划算~爱了爱了`,
      desc: `@团长 接单啦~我刚刚买了${goodsNum}个商品，感觉超划算~爱了爱了`,
      path: `/pages/order-share/order-share?order_sn=${orderSn}`,
      imageUrl: shareImage
    };
  },

  // 查看订单
  handleLookOrder() { wx.navigateTo({ url: '/pages/ucenter/order/order' }) },
  // 回到首页
  handleToHome() {
    if (this.data.orderType === 'shetuan') {
      wx.switchTab({ url: '/pages/index-group/index/index' });
    } else {
      wx.switchTab({ url: '/pages/index/index' });
    };
  }
})