import * as API_Promotions from '../../../../api/promotions'
import { Foundation } from '../../../../ui-utils/index'
const beh = require('../../../../utils/behavior.js')

Component({
  properties: {
    promotions:{
      type:Object,
      value:{}
    },
    groupbuy:{
      type:Object,
      value:{}
    },
    seckill: {
      type: Object,
      value: {}
    },
    shetuan: {
      type: Object,
      value: {}
    }
  },
  data: {
    showPromotion: true
  },
  behaviors: [beh],
  lifetimes: {
    attached() {

    }
  },
  computed:{
    promotion(){
      const { promotions } = this.data
      if (!promotions || !promotions.length) return null
      let prom = [];
      let gbProm = [];
      let skProm = [];
      let stProm = [];
      promotions.forEach(item => {
        if (item.groupbuy_goods_vo) {
          gbProm.push(item);
        } else if (item.seckill_goods_vo) {
          skProm.push(item);
        } else if (item.shetuan_goods_vo) {
          stProm.push(item);
        };
      });
      if (gbProm && gbProm[0]) {
        gbProm[0].prom_type = 'gb';
        prom = gbProm;
      } else if (skProm && skProm[0]) {
        if (skProm[0] && skProm[0].seckill_goods_vo.distance_start_time < 0) {
          return false
        };
        prom = skProm;
      } else if (stProm && stProm[0]) {
        if (stProm[0] && stProm[0].shetuan_goods_vo.distance_start_time < 0) {
          return false
        };
        prom = stProm;
      };
      // 如果都没有，返回false
      if (!prom || !prom[0]) return false
      return prom[0]
    }
  },
  methods: {
    handleCountEnd() {
      this.setData({showPromotion : false})
      wx.showToast({title: '活动已结束，商品已恢复原价。',icon:"none"})
    }
  }
})