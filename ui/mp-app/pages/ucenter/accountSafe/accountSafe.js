// pages/ucenter/changemobile/changemobile.js
Page({
  goPassword(){
    wx.navigateTo({ url: '../accountSafe/changepassword/changepassword' })
  },
  goMobile() {
    wx.navigateTo({ url: '../accountSafe/changemobile/changemobile' })
  },
  goEmail(){
    wx.navigateTo({ url: '../accountSafe/changeemail/changeemail' })
  }
})