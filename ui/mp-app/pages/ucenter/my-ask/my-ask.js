const app = getApp();
let util = require('../../../utils/util.js')
import * as API_Members from '../../../api/members'
import { Foundation } from '../../../ui-utils/index.js'

Page({
  data: {
    activeTab: 0,
    scrollHeight: '',
    scrollTop: 0,//滚动高度
    finished: false,//是否已经加载完毕
    showGoTop: false,//显示返回顶部按钮
    delBtnWidth: 120,
    pageCount: 0,
    //我的提问
    askList: [],
    ask_params: {
      page_no: 1,
      page_size: 10,
    },
    //我的回答
    answerList:[],
    answer_params:{
      page_no: 1,
      page_size: 10,
      reply_status:'YES'
    },
    //邀请我答
    invitationList:[],
    invitation_params:{
      page_no:1,
      page_size:10,
      reply_status:'NO'
    }
  },
  onLoad: function (options) {
    this.setData({
      scrollHeight: wx.getSystemInfoSync().windowHeight - 42,
      activeTab: parseInt(options.active) || 0
    })
    this.initEleWidth();
  },
  onShow() {
    if(this.data.activeTab === 0){
      this.setData({ askList:[],finished:false,'ask_params.page_no':1})
      this.getAskList()
    } else if (this.data.activeTab === 1){
      this.setData({ answerList: [], finished: false, 'answer_params.page_no': 1 })
      this.getAnswerList()
    }else if(this.data.activeTab === 2){
      this.setData({ invitationList: [], finished: false, 'invitation_params.page_no': 1 })
      this.getinvitationList()
    }
  },
  //状态切换
  switchTab(event) {
    let index = parseInt(event.currentTarget.dataset.index);
    if (index !== this.data.activeTab){
      this.setData({
        activeTab: index,
        finished: false,
        scrollTop: 0,
        pageCount: 0,
      })
      if (index === 0) {
        this.setData({ askList: [], 'ask_params.page_no': 1 })
        this.getAskList()
      } else if (index === 1) {
        this.getAnswerList()
      } else if (index === 2) {
        this.getinvitationList()
      }
    }
  },
  //我的提问
  getAskList() {
    API_Members.getConsultations(this.data.ask_params).then(response=>{
      let _pageCount = Math.ceil(response.data_total / this.data.ask_params.page_size)
      this.setData({ pageCount: _pageCount })
      const { data } =  response
      if(data && data.length){
        data.forEach(key=>{key.create_time = Foundation.unixToDate(key.create_time)})
        this.data.askList.push(...data)
        this.setData({askList: this.data.askList})
      }else{
        this.setData({ finished: true})
      }
    })
  },
  //我的回答
  getAnswerList(){
    API_Members.getAnswers(this.data.answer_params).then(response => {
      let _pageCount = Math.ceil(response.data_total / this.data.answer_params.page_size)
      this.setData({ pageCount: _pageCount })
      const { data } = response
      if (data && data.length) {
        this.data.answerList.push(...data)
        this.setData({ answerList: this.data.answerList })
      } else {
        this.setData({ finished: true })
      }
    })
  },
  //邀请我答
  getinvitationList(){
    API_Members.getAnswers(this.data.invitation_params).then(response => {
      let _pageCount = Math.ceil(response.data_total / this.data.invitation_params.page_size)
      this.setData({ pageCount: _pageCount })
      const { data } = response
      if (data && data.length) {
        this.data.invitationList.push(...data)
        this.setData({ invitationList: this.data.invitationList })
      } else {
        this.setData({ finished: true })
      }
    })
  },
  //详情
  handleAskDetail(e){
    const ask_id = e.currentTarget.dataset.ask_id
    wx.navigateTo({
      url: '/pages/goods/goods-ask-list/ask-detail/ask-detail?ask_id=' + ask_id,
    })
  },
  //去回答
  handleReplyAsk(e){
    wx.navigateTo({
      url: '/pages/ucenter/reply-answer/reply-answer?ask_id=' + e.currentTarget.dataset.ask_id,
    })
  },
  //删除资询
  handleDeleteAsk(e){
    var that = this
    const ask_id = e.currentTarget.dataset.ask_id
    this.data.askList.forEach(key=>{key.txtStyle = ''})
    this.setData({ askList: this.data.askList })
    wx.showModal({
      title: '提示',
      content: '确定要删除这条消息吗?',
      confirmColor: '#f42424',
      success: function (res) {
        if (res.confirm) {
          API_Members.deleteAsk(ask_id).then(() => {
            that.setData({ askList: [] })
            that.getAskList()
            wx.showToast({ title: '删除成功' })
          })
        }
      }
    })
  },
  //删除回复
  handleDeleteAnswer(e){
    var that = this
    const id = e.currentTarget.dataset.id
    this.data.answerList.forEach(key => { key.txtStyle = '' })
    this.setData({ answerList: this.data.answerList })
    wx.showModal({
      title: '提示',
      content: '确定要删除这条消息吗?',
      confirmColor: '#f42424',
      success: function (res) {
        if (res.confirm) {
          API_Members.deleteAnswers(id).then(() => {
            that.setData({ answerList: [] })
            that.getAnswerList()
            wx.showToast({ title: '删除成功' })
          })
        }
      }
    })
  },
  loadMore() {
    if (!this.data.finished) {
      if(this.data.activeTab === 0){
        this.setData({ "ask_params.page_no": this.data.ask_params.page_no += 1 })
        if (this.data.pageCount >= this.data.ask_params.page_no) {
          this.getAskList();
        }
      }else if(this.data.activeTab === 1){
        this.setData({ "answer_params.page_no": this.data.answer_params.page_no += 1 })
        if (this.data.pageCount >= this.data.answer_params.page_no) {
          this.getAnswerList();
        }
      }else if (this.data.activeTab === 2) {
        this.setData({ "invitation_params.page_no": this.data.invitation_params.page_no += 1 })
        if (this.data.pageCount >= this.data.invitation_params.page_no) {
          this.getinvitationList();
        }
      }
    }
  },
  scroll: function (e) {
    if (e.detail.scrollTop > 200) {
      this.setData({ showGoTop: true })
    } else {
      this.setData({ showGoTop: false })
    }
  },
  //返回顶部
  goTop: function () { this.setData({ scrollTop: 0 }) },
  // 开始滑动事件
  touchS: function (e) {
    if (e.touches.length == 1) {
      this.setData({
        //设置触摸起始点水平方向位置 
        startX: e.touches[0].clientX
      });
    }
  },
  touchM: function (e) {
    if (e.touches.length == 1) {
      //手指移动时水平方向位置 
      var moveX = e.touches[0].clientX;
      //手指起始点位置与移动期间的差值 
      var disX = this.data.startX - moveX;
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) { //如果移动距离小于等于0，文本层位置不变 
        txtStyle = "left:0rpx";
      } else if (disX > 0) { //移动距离大于0，文本层left值等于手指移动距离 
        txtStyle = "left:-" + disX + "rpx";
        if (disX >= delBtnWidth) {
          //控制手指移动距离最大值为删除按钮的宽度 
          txtStyle = "left:-" + delBtnWidth + "rpx";
          //获取手指触摸的是哪一项 
          var index = e.currentTarget.dataset.index;
          if (this.data.activeTab === 0 ){
            var list = this.data.askList;
          }else if(this.data.activeTab === 1){
            var list = this.data.answerList;
          }   
          list.forEach(key => { key.txtStyle = '' })
          list[index].txtStyle = txtStyle;
          //更新列表的状态 
          if (this.data.activeTab === 0) {
            this.setData({ askList: list });
          } else if (this.data.activeTab === 1) {
            this.setData({ answerList: list });
          }
        }
      }
    }
  },
  // 滑动中事件
  touchE: function (e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置 
      var endX = e.changedTouches[0].clientX;
      //触摸开始与结束，手指移动的距离 
      var disX = this.data.startX - endX;
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮 
      var txtStyle = "";
      txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0rpx";

      //获取手指触摸的是哪一项 
      var index = e.currentTarget.dataset.index;
      if (this.data.activeTab === 0) {
        var list = this.data.askList;
      } else if (this.data.activeTab === 1) {
        var list = this.data.answerList;
      }
      list.forEach(key => { key.txtStyle = '' })
      list[index].txtStyle = txtStyle;
      //更新列表的状态 
      if (this.data.activeTab === 0) {
        this.setData({ askList: list });
      } else if (this.data.activeTab === 1) {
        this.setData({ answerList: list });
      }
    }
  },
  //获取元素自适应后的实际宽度 
  getEleWidth: function (w) {
    var real = 0;
    try {
      var res = wx.getSystemInfoSync().windowWidth;
      var scale = (750 / 2) / (w / 2);
      real = Math.floor(res / scale);
      return real;
    } catch (e) {
      return false;
    }
  },
  initEleWidth: function () {
    var delBtnWidth = this.getEleWidth(this.data.delBtnWidth);
    this.setData({delBtnWidth: delBtnWidth});
  }
})