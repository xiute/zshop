const app = getApp();
import * as API_Members from '../../../api/members'
import { Foundation } from '../../../ui-utils/index.js'
let util = require('../../../utils/util.js')

Page({
  data: {
    typeId: 0,
    goods_id:0,
    shop_id:0,
    goodsList:[],
    params_goods:{
      page_no: 1,
      page_size: 10
    },
    shopsList:[],
    params_shops: {
      page_no: 1,
      page_size: 10
    },
    finished_goods: false, // 是否加载完成
    finished_shops: false, // 是否加载完成
    scrollHeight: '',
    scrollTop: 0, // 滚动高度
    showGoTop: false, // 显示返回顶部按钮
    pageCount: 0
  },
  getCollection(){
    if (this.data.typeId === 0){
      API_Members.getGoodsCollection(this.data.params_goods).then(res => {
        let _pageCount = Math.ceil(res.data_total / this.data.params_goods.page_size)
        this.setData({ pageCount: _pageCount })
        const data = res.data
        if (data && data.length) {
          data.forEach(key => {
            key.goods_price = Foundation.formatPrice(key.goods_price)
          })
          this.setData({ goodsList: this.data.goodsList.concat(data) })
        } else {
          this.setData({ finished_goods: true })
        }
      }).catch(() => {  })
    }else{
      API_Members.getShopCollection(this.data.params_shops).then(res => {
        let _pageCount = Math.ceil(res.data_total / this.data.params_shops.page_size)
        this.setData({ pageCount: _pageCount })
        const data = res.data
        if (data && data.length) {
          this.setData({ shopsList: this.data.shopsList.concat(data) })
        } else {
          this.setData({ finished_shops: true })
        }
      }).catch(() => {  })
    }
  },
  deleteGoodsColl: function(e){
    const that = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除这个店铺收藏吗？',
      confirmText: '确定',
      cancelText: '取消',
      confirmColor: '#e50c0c',
      cancelColor: '#cdcdcd',
      success(res) {
        if (res.confirm) {
          let { goods_id } = e.currentTarget.dataset;
          that.confirmGoods(goods_id);
        } else if (res.cancel) {
          console.log('用户点击取消');
        };
      }
    });
  },
  deleteShopColl: util.throttle(function(e){
    const that = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除这个店铺收藏吗？',
      confirmText: '确定',
      cancelText: '取消',
      confirmColor: '#e50c0c',
      cancelColor: '#cdcdcd',
      success(res) {
        if (res.confirm) {
          let { shop_id } = e.currentTarget.dataset;
          that.confirmShops(shop_id);
        } else if (res.cancel) {
          console.log('用户点击取消');
        };
      }
    });
  }),
  //取消
  cloneDialog: function () {
    this.setData({
      confirmGoods: true,
      confirmShops: true
    })
  },
  confirmShops: function (shopId) {
    let that = this;
    API_Members.deleteShopCollection(shopId).then(() => {
      wx.showToast({ title: '删除成功！'})
      that.setData({
        'params_shops.page_no': 1,
        shopsList: []
      })
      that.getCollection()
    })
  },
  onLoad: function (options) {
    let that = this;
    wx.getSystemInfo({
      success: function (res) {
        let query = wx.createSelectorQuery();
        query.select('.collection-switch').boundingClientRect();
        query.exec(response => {
          that.setData({
            scrollHeight: res.windowHeight - response[0].height
          });
        });
      }
    });
    this.getCollection()
  }, 
  switchTab(event) {
    let index = parseInt(event.currentTarget.dataset.index);
    if (index === 0 && index !== this.data.typeId){
      this.setData({
        'params_goods.page_no': 1,
        goodsList: [],
        typeId: index,
        finished_goods:false,
        pageCount:0
      })
      this.getCollection()
    }
    if (index === 1 && index !== this.data.typeId) {
      this.setData({
        'params_shops.page_no': 1,
        shopsList: [],
        typeId: index,
        finished_shops: false,
        pageCount: 0
      })
      this.getCollection()
    }
  },

  loadMore: function () {
    if (!this.data.finshed) {
      if (this.data.typeId === 0) {
        this.setData({ "params_goods.page_no": this.data.params_goods.page_no += 1 })
        if (this.data.pageCount >= this.data.params_goods.page_no) {
          this.getCollection()
        }
      } else {
        this.setData({ 'params_shops.page_no': this.data.params_shops.page_no += 1 })
        if (this.data.pageCount >= this.data.params_shops.page_no) {
          this.getCollection()
        }
      }
    }
  },
  scroll: function (e) {
    if (e.detail.scrollTop > 200) {
      this.setData({ showGoTop: true })
    } else {
      this.setData({ showGoTop: false })
    }
  },
  //返回顶部
  goTop: function () { this.setData({ scrollTop: 0 }) }
})