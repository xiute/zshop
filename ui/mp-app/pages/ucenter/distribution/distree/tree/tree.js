const beh = require('../../../../../utils/behavior.js')
import { Foundation } from '../../../../../ui-utils/index.js'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    model:Object,
    num:String,
    nodes:String,
    root:String
  },
  data: {
    
  },
  lifetimes: {
    attached: function () {
      let _model = this.data.model
      _model.rebate_total = Foundation.formatPrice(_model.rebate_total)
      this.setData({model:_model})
    },
  },
  behaviors: [beh],
  computed:{
    hasChild(){
      // return this.data.model.item.length > 0
    },
    isOpenOrClose() {
      // return this.data.model.isExpand ? 'open' : 'close'
    }
  },
  methods: {
    openExpand(m) {
      this.triggerEvent("openExpand", m.currentTarget.dataset.model)
    },
    delAction(m) {
      this.triggerEvent("delAction", m.currentTarget.dataset.model);
    },
    handlelookDetails(m) {
      wx.navigateTo({
        url: '/pages/ucenter/distribution/my-performance/my-performance?member_id=' + m.currentTarget.dataset.model.id,
      })
    }
  }
})