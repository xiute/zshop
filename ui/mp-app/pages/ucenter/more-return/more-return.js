import * as API_Distribution from '../../../api/distribution.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 页面的高度
    scrollH: 0,
    finshed: false,
    returnList: [],
    // 分页
    params: {
      page_no: 1,
      page_size: 10,
      member_id: ''
    },
    // 总页
    page_count: 0 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取页面高度
    wx.getSystemInfo({
      success: (response) => {
        this.setData({
          scrollH: response.windowHeight
        });
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getPageData();
  },

  /**
    * @date 2020/09/04
    * @author kaiqiang
    * @description { 获取多退少补列表数据 }
  */
  getPageData() {
    let { params, returnList } = this.data;
    this.setData({
      'params.member_id' : wx.getStorageSync('user').member_id
    });
    API_Distribution.getMoreReturnList(params).then((response) => {
      const { data, data_total, page_size } = response;
      let _pageCount = Math.ceil(data_total / page_size);
      let _returnList = returnList.concat(data);
      this.setData({
        returnList: _returnList,
        page_count: _pageCount,
        finshed: true
      });
    });
  },

  /**
    * @date 2020/09/05
    * @author kaiqiang
    * @description { 下拉加载更多 }
  */
  loadMore() {
    let { params, page_count } = this.data;
    let _pageNumber = params.page_no + 1;
    this.setData({
      'params.page_no': _pageNumber
    });
    if (page_count >= _pageNumber) {
      this.getPageData();
    }
  },
  
  // 上啦刷新
  onPullDownRefresh: function () {
    this.setData({
      returnList: [],
      params: {
        page_no: 1,
        page_size: 10
      }
    });
    // 刷新导航栏数据
    this.getPageData();
    wx.stopPullDownRefresh();
  }
})