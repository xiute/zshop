// pages/ucenter/wallet/wallet.js

import * as API_Account from '../../../api/account.js'
import { Foundation } from '../../../ui-utils/index.js'
var _this;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    transDataList:[],
    balanceAmount: 0.00,
    amount: 0,
    params: {
      page_no: 1,
      page_size: 10,
    },
    finished: false,
    showGoTop: false,
    scrollTop: 0, // 滚动高度
    scrollHeight: '',
    pageCount: 0,
    msg: '',
    accountId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    _this = this;
    let query = wx.createSelectorQuery();
    query.select('.total-momey').boundingClientRect(rect => {
      _this.setData({
        scrollHeight: wx.getSystemInfoSync().windowHeight - rect.height
      });
    }).exec();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({ 
      transDataList: [],
      params: {
        page_no: 1,
        page_size: 10,
      },
    });
    this.getBalance();
    this.getRecordList();
  },

  goWidthdrawal(){
    if (this.data.amount > 0) {
      wx.navigateTo({ url: "/pages/ucenter/withdrawal/withdrawal" });
    } else {
      wx.showToast({
        title: '当前无可提现金额',
        icon: 'none'
      })
    };
  },

  getBalance() {
    API_Account.getWalletIndex().then((resp)=>{
      const { id, balance_amount } = resp.data.account;
      _this.setData({
        accountId: id, 
        amount: balance_amount,
        balanceAmount: Foundation.formatPrice(balance_amount) || '0.00'
      });
      wx.setStorageSync('accountId', id);
    });
  },
  getTradeStatus(_type) {
    switch(_type) {
      case 1:
        return '交易等待';
      case 2:
        return '交易中';
      case 3:
        return '交易成功';
      case 4:
        return '交易关闭';
      default:
        return '';
    }
  },
  getRecordList() {
    API_Account.queryTradeRecordList({ 
      body: {}, 
      page_num: _this.data.params.page_no, 
      page_size: _this.data.params.page_size,  
    }).then((response)=>{
      if (_this.data.params.page_no === 1) {
        wx.stopPullDownRefresh();
      };
      let pageCount = response.data.total_pages;
      _this.setData({ pageCount});
      const data = response.data.records
      if (data && data.length) {
        data.forEach(key => {
          key.tradeStatusName = _this.getTradeStatus(key.tradeStatus);
        });
        _this.setData({ transDataList: _this.data.transDataList.concat(data) })
      } else {
        _this.setData({ finished: true })
      }
    });
  },

  loadMore: function () {
    if (!this.data.finished) {
      this.setData({ "params.page_no": this.data.params.page_no + 1 })
      if (this.data.pageCount >= this.data.params.page_no) {
        this.getRecordList()
      } else {
        this.setData({ msg: '已经到底了~' })
      }
    }
  },
  scroll: function (e) {
    let that = this
    if (e.detail.scrollTop > 200) {
      that.setData({ showGoTop: true })
    } else {
      that.setData({ showGoTop: false })
    }
  },
  //返回顶部
  goTop: function () { this.setData({ scrollTop: 0 }) },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      transDataList: [],
      params: {
        page_no: 1,
        page_size: 10,
      },
    });
    this.getRecordList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})