// pages/ucenter/after-sale/cancel/cancel.js
import * as API_ORDER from '../../../../api/order.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    order_sn: '',
    reasonList: ['我不想买了', '地址信息填错', '商品选错了', '其他'],
    refund_reason: '',
    customer_remark: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ 
      order_sn: options.order_sn
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  // 表单提交
  submitCancle(e) {
    let { refund_reason, customer_remark } = e.detail.value;
    if (!refund_reason) {
      wx.showToast({title: '请选择取消原因', icon:'none'});
      return false
    };
    const reason = `${refund_reason}${customer_remark}`;
    API_ORDER.cancelOrder(this.data.order_sn, reason).then(() => {
      wx.showToast({
        title: '订单取消成功！'
      });
      wx.navigateBack();
    })
  }
})