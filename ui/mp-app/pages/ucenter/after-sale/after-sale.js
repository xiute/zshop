 import * as API_AfterSale from '../../../api/after-sale.js'
 import { Foundation } from '../../../ui-utils/index.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    afterSaleList:[],
    params:{
      page_no:1,
      page_size:10
    },
    finished:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getAfterSale()
  },
  getAfterSale(){
    API_AfterSale.getAfterSale(this.data.params).then(response=>{
      const { data } = response
      if(data && data.length){
        data.forEach(key=>{
          key.refund_price = Foundation.formatPrice(key.refund_price)
        })
        this.data.afterSaleList.push(...data)
        this.setData({ afterSaleList: this.data.afterSaleList })
      }else{
        this.setData({finished:true})
      }
    })
  },
  onReachBottom: function () {
    if (!this.data.finshed) {
      this.setData({ "params.page_no": this.data.params.page_no + 1 })
      this.getAfterSale()
    } else {
      return
    }
  }
})