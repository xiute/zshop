import * as API_Distribution from '../../../api/distribution.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultFace: 'https://localhost/images/icon-noface.png',
    // 页面是否加载完成
    finshed: false,
    // 我的自提点
    distributionVO: '',
    // 我的团长
    // leaderVO: '',
    // 我的邀请人
    inviterPage: '',
    // 判断我的团长是否显示
    leaderVOBollen: false,
    // 判断我的自提点是否显示
    // distributionVOBollen: false,
    // 我的邀请人是否显示
    inviterPageBollen: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: async function () {
    // 调用获取上级的接口
    this.getPageData();
  },
  /**
   * @date 2020/08/25
   * @author kaiqiang
   * @description { 展示我的上级首页列表 }
   */
  getPageData() {
    API_Distribution.getSuperiorList().then(response => {
      const { leaderVO, distributionVO, inviterPage } = response;
      // 将获取的数据赋值给列表页面
      this.setData({
        // distributionVOBollen: !!distributionVO.member_id,
        leaderVOBollen: !!leaderVO.leader_id,
        inviterPageBollen: !!inviterPage.member_id,
        distributionVO,
        leaderVO,
        inviterPage,
        finshed: true
      });
    })
  }
})
