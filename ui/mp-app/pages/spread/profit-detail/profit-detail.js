// pages/leader/profit-detail/profit-detail.js
import * as API_Trade from '../../../api/trade.js'

Page({
  /**
   * 页面的初始数据
   */
  data: {
    scrollHeight: 0, // 滚动高度
    time: '',
    type: 0,
    profitDetail: [],
    totalCount: {
      order_num: 0,
      show_order_price_sum: '¥0.0',
      show_commission_sum: '¥0.0'
    },
    // 商品分页
    page_no: 1,
    page_size: 10,
    page_count: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.computeHeight();
    this.setData({
      time: options.time
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      profitDetail: [],
      totalCount: {
        order_num: 0,
        show_order_price_sum: '¥0.0',
        show_commission_sum: '¥0.0'
      },
      page_no: 1
    });
    this.getPageData();
  },
  /**
    * @date 2020/06/16
    * @author wolfMan
    * @description {计算滚动高度}
  */
  computeHeight () {
    const that = this;
    wx.getSystemInfo({
      success: (res) => {
        let query = wx.createSelectorQuery();
        query.select('.bottom-fixed').boundingClientRect();
        query.exec(response => {
          that.setData({
            scrollHeight: res.windowHeight - response[0].height + 'px'
          });
        });
      }
    });
  },
  /**
    * @date 2020/08/11
    * @author wolfMan
    * @description {获取页面数据}
  */
  getPageData () {
    const { time, type, page_no, page_size } = this.data;
    let member_id = wx.getStorageSync('user').member_id;
    let _params = {
      page_no,
      page_size,
      member_id,
      type,
      date_time: time
    };
    API_Trade.getProfitDetail(_params).then(res => {
      const { data, data_total, page_size } = res.statisticPageList;
      const { order_num, show_commission_sum, show_order_price_sum } = res.statisticCount;
      let _pageCount = Math.ceil(data_total / page_size);
      let _profitDetail = this.data.profitDetail.concat(data);
      this.setData({
        profitDetail: _profitDetail,
        page_count: _pageCount,
        totalCount: {
          order_num: order_num || 0,
          show_order_price_sum: show_order_price_sum || '¥0.0',
          show_commission_sum: show_commission_sum || '¥0.0'
        }
      });
    });
  },
  // 复制订单号
  orderCopy(e) {
    const orderSn = e.currentTarget.dataset.sn;
    wx.setClipboardData({
      data: orderSn,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            wx.showToast({
              title: '订单号复制成功'
            });
          }
        });
      }
    });
  },
  /**
    * @date 2020/08/12
    * @author wolfMan
    * @description {加载更多}
  */
  loadMore: function () {
    let { page_no, page_count } = this.data;
    this.setData({ 
      page_no: page_no += 1
    });
    if (page_count >= page_no) {
      this.getPageData();
    };
  }
})