// pages/index/index.js
import * as API_Floor from '../../api/floor.js'
import * as API_Shop from '../../api/shop.js'
const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 页面颜色
    pageBack: '#f4f4f4',
    // 自定义头部高度
    navHeight: 0,
    // 滚动区域高度
    scrollHeight: 0,
    // 分享图片
    shareImage: '',
    // 楼层数据
    floorData: [],
    // 社区团购分组数据
    catsTypeData: [],
    // 距离数据
    heightData: {},
    pageDesc:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      navHeight: app.globalData.navbar.height,
      scrollHeight: app.globalData.systemInfo.windowHeight - app.globalData.navbar.height + 'px'
    });
    // 获取分享时需要绑定的key
    app.getShareKey('pages/index/index', 'shareIndex');
    this.setData({
      locationData: app.globalData.locationData,
      floorData: [],
      shareImage: ''
      
    });
    this.getPageData();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      locationData: app.globalData.locationData
    });
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      locationData: app.globalData.locationData,
      floorData: [],
      shareImage: ''
    });
    this.getPageData('pull');
  },
  // 获取页面楼层数据
  getPageData: function (_type) {
    const { province, city } = this.data.locationData;
    let _position = city || province;
    // 1:商城首页
    API_Floor.getFloorCityData('WXS', '1', _position).then(async response => {
      if (_type === 'pull') {
        // 停止下拉[回弹]
        wx.stopPullDownRefresh();
      };
      let _floorData = [];
      let _pageData = response.page_data;
      if (_pageData) {
        _floorData = JSON.parse(_pageData).filter(item => item.tpl_id !== 9000 && item.tpl_id !== 9001);
      };
      this.setData({
        pageBack: response.background,
        floorData: _floorData,
        pageDesc: response.page_desc,
        shareImage: response.share_image
      });
    });
  },
  // 页面滚动
  onPageScroll: function (e) {
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    let _shareKey = wx.getStorageSync('shareIndex') || '';
    let _opt = _shareKey ? `?shareKey=${_shareKey}`: '';
    const { pageDesc, shareImage } = this.data;
    return {
      title: pageDesc || app.globalData.shareTitle,
      desc: pageDesc || app.globalData.shareTitle,
      path: `/pages/index/index${_opt}`,
      imageUrl: shareImage
    };
  }
})