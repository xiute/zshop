/**
 * 结算页
 * 优惠券组件
 */
import * as API_Trade from '../../../../../api/trade'
import regeneratorRuntime from '../../../../../lib/wxPromise.min.js'
Component({
  properties: {
    inventories: Array,
    show: Boolean,
    way: String
  },
  data: {
    shopList: ''
  },
  methods: {
    /** 关闭弹窗 */
    handleCouponsClose() {
      this.setData({ show: false })
    },
    /** 使用优惠券 */
    async useCoupon(e) {
      const shop = e.currentTarget.dataset.shop
      const coupon = e.currentTarget.dataset.coupon
      if (coupon.enable === 0) {
        wx.showToast({ title: coupon.error_msg ,icon:'none'})
        return
      }
      const { seller_id } = shop
      const used = coupon.selected === 1
      if (used) {
        await API_Trade.useCoupon(seller_id, 0, this.data.way)
      } else {
        await API_Trade.useCoupon(seller_id, coupon.member_coupon_id, this.data.way)
      }
      this.triggerEvent('changed', used ? '' : coupon)
      this.setData({ show: false })
    }
  }
})
