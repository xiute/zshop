// pages/shopping/checkout/checkout-group.js
let util = require('../../../utils/util.js')
import * as watch from "../../../utils/watch.js";
const middleware = require('../../../utils/middleware.js')
import regeneratorRuntime from '../../../lib/wxPromise.min.js'
import * as API_Trade from '../../../api/trade'
import * as API_Address from '../../../api/address'
import * as API_Account from '../../../api/account.js'
import { Foundation, RegExp } from '../../../ui-utils/index'
const app = getApp();
let shipFeeListDef = [{
  type: 'SELF',
  name: '站点自提',
  showName: '站点自提',
  feeStr: '0',
  enable: true,
  fee: 0
}, {
  type: 'GLOBAL',
  name: '直送到家',
  showName: '直送到家',
  enable: true,
  fee: 0
}];

Page(middleware.identityFilter({
  data: {
    // 结算店铺id
    sellerId: '',
    /** 滚动区域高度 */
    height: '',
    // 配送方式
    activeShipWay: shipFeeListDef[0],
    // 自提点信息
    selfTakeData: {},
    // 已选地址
    address: '',
    // 自提时，收货人姓名、电话
    ship_name: '',
    ship_mobile: '',
    // 结算参数
    params: '',
    // 订单总金额
    orderTotal: {
      discount_price: 0,
      freight_price: 0,
      goods_price: 0,
      is_free_freight: 0,
      total_price: 0,
    },
    // 钱包可用余额
    balance_amount: 0,
    balance_amount_show: '0',
    // 钱包抵扣金额
    walletPayPrice: '',
    // 钱包抵扣所有金额下单
    walletCreatOrder: false,
    // 购物清单
    inventories: '',
    inventoriesTotal: '',
    paymentData: [],
    // 已选地址
    address: '',
    // 显示优惠券弹窗
    showCouponsPopup: false,
    // 优惠券张数
    coupon_num: 0,
    // 显示购物清单弹窗
    showInventoryPopup: false,
    // 显示支付配送弹窗
    showPaymentPopup: false,
    // 订单备注暂缓区
    remark: '',
    // 显示错误actionsheet
    showErrorActionsheet: false,
    // 错误actionsheet消息
    errorActionsheetMessage: '',
    // 错误actionsheet数据
    errorActionsheetData: [],
    // 页面跳转过来的是立即购买还是购物车结算
    way: '',
    // 参数 订单id
    orderId: '',
    // 询问框
    prompt: '',
    sellerids: '', // 商品所属的商家id
    allCreate: true, // 允许下单
    subDisabled: false, // 提交订单禁用
    // 确认团长
    confirmLeader: true,
    // 拆弹确认
    confirmOrders: true
  },
  onLoad(options) {
    this.setData({
      sellerId: options.sellerId,
      way: options.way,
      orderId: options.order_id || ''
    });
    watch.setWatcher(this); // 启用数据监听
  },
  watch: {
    inventories: function (newVal) {
      /** 计算总运费、总订单金额 */
      let { walletPayPrice, orderTotal } = this.data;
      let _act_freight_price = 0; // 实际运费
      let goods_price = orderTotal.goods_price ? orderTotal.goods_price : '0.00,0.00,00';
      let _goods_price = ''; //
      goods_price.split(',').forEach(item => {
        _goods_price = `${_goods_price}${item}`;
      });
      let _act_total_price = parseFloat(_goods_price); // 实际订单金额
      if (newVal) {
        newVal.forEach(item => {
          let _shipFee = item.active_ship_way === 'LOCAL' ? item.price.local_freight_price : item.active_ship_way === 'GLOBAL' ? item.price.global_freight_price : 0;
          _act_freight_price += _shipFee;
          _act_total_price += _shipFee;
        });
      };
      orderTotal.act_freight_price = _act_freight_price.toFixed(2);
      orderTotal.act_total_price = _act_total_price.toFixed(2);
      if (walletPayPrice && orderTotal.act_total_price) {
        if (Number(walletPayPrice) > Number(orderTotal.act_total_price)) {
          walletPayPrice = orderTotal.act_total_price;
        };
      };
      this.setData({ walletPayPrice, orderTotal });
    }
  },
  onReady() {
    this.prompt = this.selectComponent('#prompt')
  },
  onShow() {
    wx.showLoading({ title: '加载中...' });
    // 获取购物车、账户信息
    Promise.all([
      API_Trade.getCheckoutParams(),
      API_Account.getMemberAccount()
    ]).then(async response => {
      let params = response[0];
      params.payment_type = 'ONLINE';
      this.setData({
        params: params,
        remark: params.remark,
        height: wx.getSystemInfoSync().windowHeight - 50 + 'px'
      });
      const { activeShipWay, sellerId } = this.data;
      if (!params.ship_way || (sparams.ship_ways && params.ship_ways[0].ship_way !== activeShipWay.type)) {
        const shipWayList = [{
          ship_way: activeShipWay.type,
          seller_id: sellerId
        }];
        await API_Trade.setShipWay(shipWayList);
      };
      let accountParams = response[1];
      let balanceAmount = accountParams.data.balance_amount;
      let balance_amount_show = '0';
      if (accountParams.success) {
        balance_amount_show = Foundation.formatPrice(accountParams.data.balance_amount);
      };
      this.setData({
        balance_amount: balanceAmount,
        balance_amount_show
      });
      // 获取地址信息
      this.getAddressDetail();
      // 查询店铺购物车
      await this.GET_Inventories();
      wx.hideLoading();
    }).catch(error => {
      console.log(error);
      wx.hideLoading();
    });
    // 获取购物清单
    API_Trade.getCheckoutParams().then(async params => {
    }).catch(error => {
      console.log(error);
      wx.hideLoading();
    });
  },
  /**
   * 获取购物清单，和结算金额
   * @returns {Promise<void>}
   * @constructor
   */
  GET_Inventories() {
    this.setData({
      allCreate: true
    });
    API_Trade.getSingleCarts(this.data.sellerId, this.data.way, 'SHETUAN_CART').then(response => {
      const { cart_list, total_goods_num, total_price } = response;
      total_price.goods_price = Foundation.formatPrice(total_price.goods_price)
      total_price.original_price = Foundation.formatPrice(total_price.original_price)
      total_price.discount_price = Foundation.formatPrice(total_price.discount_price)
      total_price.freight_price = total_price.freight_price ? Foundation.formatPrice(total_price.freight_price) : '0';
      total_price.total_price = Foundation.formatPrice(total_price.total_price)
      total_price.total_goods_num = total_goods_num;
      if (total_price.coupon_price !== 0) {
        total_price.coupon_price = Foundation.formatPrice(total_price.coupon_price)
      }
      this.setData({
        orderTotal: total_price
      })
      let coupon_num = 0;
      if (cart_list && cart_list.length) {
        cart_list.forEach(shop => {
          // 处理商品信息
          shop.price.freight_price = Foundation.formatPrice(shop.price.freight_price);
          shop.pick_tomorry = (shop.pick_time_text).indexOf('明日') > -1;
          if (shop.promotion_notice) {
            shop.promotion_notice = Foundation.formatPrice(shop.promotion_notice)
          }
          // 处理优惠券
          if (shop.coupon_list && shop.coupon_list.length) {
            coupon_num += shop.coupon_list.filter(coupon => coupon.enable !== 0).length
            shop.coupon_list.forEach(coupon => {
              coupon.amount = Foundation.formatPrice(coupon.amount)
              coupon.use_term = parseFloat(coupon.use_term.slice(1, 6)).toString()
              coupon.end_time = Foundation.unixToDate(coupon.end_time, 'yyyy-MM-dd')
            })
          }
          //处理赠品
          if (shop.gift_coupon_list && shop.gift_coupon_list.length) {
            shop.gift_coupon_list.forEach(gift_coupon => {
              gift_coupon.amount = Foundation.formatPrice(gift_coupon.amount)
            })
          }
          if (shop.gift_list && shop.gift_list.length) {
            shop.gift_list.forEach(gift => {
              gift.gift_price = Foundation.formatPrice(gift.gift_price)
            })
          }
          // 处理sku数据
          if (shop.sku_list && shop.sku_list.length) {
            shop.sku_list.forEach(item => {
              item.purchase_price = Foundation.formatPrice(item.purchase_price)
              item.original_price = Foundation.formatPrice(item.original_price)
              if (!item.skuName) {
                item.skuName = this.formatterSkuSpec(item)
              }
              if (item.single_list) {
                const _list = item.single_list.filter(item => item.is_check === 1)
                item.promotionName = _list[0] ? _list[0].title : '不参加活动'
              }
            })
          }
        })
      }
      let _sellerIds = cart_list.map(item => item.seller_id).join(',');
      this.setData({
        coupon_num: coupon_num,
        inventories: cart_list,
        sellerids: _sellerIds
      });
      let { activeShipWay } = this.data;
      if (total_price.active_ship_way === 'GLOBAL') {
        activeShipWay.feeStr = total_price.freight_price;
        this.setData({
          activeShipWay,
          allCreate: total_price.allow_create_order
        });
      };
    })
  },
  /** 提交收货人 */
  changeName(e) {this.setData({'ship_name': e.detail.value})},
  /** 提交收货电话 */
  changeMobile(e) {this.setData({'ship_mobile': e.detail.value})},
  /** 格式化地址信息 */
  formatterAddress(address) {
    if (!address) return ''
    return `${address.province} ${address.city} ${address.county} ${address.town} ${address.addr}`
  },
  /** 提交订单 */
  async handleSubmitTrade() {
    const { walletPayPrice, orderTotal, allCreate, subDisabled, activeShipWay, ship_name, ship_mobile, selfTakeData } = this.data;
    if (!allCreate || subDisabled) {
      return;
    };
    if (activeShipWay.type === 'SELF') {
      if (!ship_name || !ship_mobile) {
        wx.showToast({ title: '请填写收货人和收货人电话', icon: 'none' });
        return;
      } else if (!RegExp.mobile.test(ship_mobile)) {
        wx.showToast({ title: '收货人手机号码格式有误', icon: 'none' });
        return;
      } else if (!selfTakeData.leader_id && selfTakeData.leader_id !== 0) {
        wx.showToast({ title: '请选择自提站点', icon: 'none' });
        return;
      };
    };
    // 校验是否全部钱包抵扣
    if (walletPayPrice && (Number(walletPayPrice) === Number(orderTotal.act_total_price))) {
      const that = this;
      wx.showModal({
        title: '支付提示',
        content: '您购买的商品将全部由您的钱包余额支付',
        confirmText: '继续购买',
        cancelText: '稍后在说',
        confirmColor: '#e50c0c',
        cancelColor: '#cdcdcd',
        success(res) {
          if (res.confirm) {
            that.setData({
              walletCreatOrder: true
            });
            that.verificatNext('SELF');
          } else if (res.cancel) {
            console.log('用户点击取消');
          };
        }
      });
    } else {
      this.verificatNext();
    };
  },
  /**  */
  verificatNext(_shipType) {
    if (_shipType === 'SELF') {
      const { inventories } = this.data;
      if (inventories.length > 1) {
        this.setData({
          confirmOrders: false
        });
      } else {
        this.setData({
          confirmLeader: false
        });
      };
    } else {
      this.createTrade();
    };
  },
  /** 创建订单 */
  async createTrade() {
    this.setData({
      subDisabled: true
    });
    /** 先调用创建订单接口，再跳转到收银台 */
    try {
      let { way, sellerId, activeShipWay, ship_name, ship_mobile, walletPayPrice } = { ...this.data };
      if (activeShipWay.type === 'SELF') {
        await API_Trade.checkoutShipInfo({ship_mobile, ship_name});
      };
      // const { cart_list } = await API_Trade.getCarts('all', 'BUY_NOW');
      // wx.setStorageSync('shoplist', cart_list);
      walletPayPrice = walletPayPrice ? Number(walletPayPrice) : 0;
      const response = await API_Trade.createSingle('MINI', sellerId, way, 'SHETUAN_CART', walletPayPrice);
      
      wx.redirectTo({ url: `/pages/cashier/cashier?trade_sn=${response.trade_sn}` });
      this.setData({
        subDisabled: false
      });
    } catch (error) {
      console.log(error);
      this.setData({
        subDisabled: false
      });
      const { data } = error.response || {}
      if (data && data.data) {
        const { data: list } = data
        if (!list || !list[0]) {
          wx.showToast({ title: data.message, icon: 'none', duration: 2000 });
          console.log(data.message);
          return;
        };
      } else {
        wx.showToast({ title: data.message, icon: 'none', duration: 2000 });
        console.log(data.message);
      };
    }
  },
  /** 规格格式化显示 */
  formatterSkuSpec(sku) {
    if (!sku.spec_list || !sku.spec_list.length) return ''
    return sku.spec_list.map(spec => spec.spec_value).join(' - ')
  },
  /** 添加/更换收自提站点 */
  handleLeader() {
    wx.navigateTo({
      url: '/pages/leader-list/leader-list?from=checkout'
    })
  },
  /** 添加/更换收货地址 */
  handleAddress() {
    wx.navigateTo({
      url: '/pages/ucenter/address/address?from=checkout'
    })
  },
  /* 显示支付配送选择弹窗 */
  handleShowPayment() {
    const _paymentData = {
      currentShipFee: this.data.activeShipWay,
      shipFeeList: shipFeeListDef
    };
    this.setData({
      showPaymentPopup: true,
      paymentData: _paymentData
    });
  },
  /** 支付配送发生改变 */
  handlePaymentChanged(e) {
    const currentShipFee = e.detail.currentShipFee;
    const shipWayList = [{
      ship_way: currentShipFee.type,
      seller_id: this.data.sellerId
    }];
    API_Trade.setShipWay(shipWayList).then(() => {
      this.setData({
        activeShipWay: currentShipFee
      });
      this.getAddressDetail();
      // 查询店铺购物车
      this.GET_Inventories();
    });
  },
  // 获取地址信息
  async getAddressDetail() {
    const { activeShipWay, params } = this.data;
    if (activeShipWay.type === 'SELF') {
      let { ship_name, ship_mobile } = this.data;
      if (!ship_name && !ship_mobile) {
        if (!params.ship_name && !params.ship_mobile) {
          const _user = wx.getStorageSync('user');
          ship_name = _user.nickname;
          ship_mobile = _user.mobile;
        } else {
          ship_name = params.ship_name;
          ship_mobile = params.ship_mobile;
        };
        this.setData({
          ship_name,
          ship_mobile
        });
      };
      app.getCurrentLeader().then(res => {
        this.setData({
          selfTakeData: res
        });
      });
    } else if (activeShipWay.type === 'GLOBAL' && params.address_id) {
      await API_Trade.setAddressId(params.address_id)
      const address = await API_Address.getAddressDetail(params.address_id);
      const addressStr = this.formatterAddress(address);
      this.setData({ address:address, addressStr:addressStr })
    };
  },
  // 输入钱包抵扣金额
  changeWalletPay(e) {
    let { walletPayPrice, orderTotal } = this.data;
    walletPayPrice = e.detail.value;
    if (walletPayPrice) {
      if (!RegExp.money.test(walletPayPrice)) {
        wx.showToast({ title: '请输入有效的金额,保留位小数', icon: 'none' });
        walletPayPrice = '';
      } else if (Number(walletPayPrice) > Number(orderTotal.act_total_price)) {
        wx.showToast({ title: '钱包抵扣金额不能大于订单合计金额', icon: 'none' });
        walletPayPrice = orderTotal.act_total_price;
      };
    };
    this.setData({
      walletPayPrice
    });
  },
  // 全部抵扣钱包抵扣
  walletPayAll() {
    let { orderTotal, balance_amount, walletPayPrice } = this.data;
    if (!balance_amount) {
      wx.showToast({ title: '钱包无可用余额', icon: 'none' });
      return;
    };
    walletPayPrice = Number(balance_amount) > Number(orderTotal.act_total_price) ? orderTotal.act_total_price : Number(balance_amount);
    this.setData({
      walletPayPrice
    });
  },
  // 显示prompt 显示备注弹窗
  showPrompt() {
    this.prompt.showPrompt()
  },
  // 将输入的value暂时保存起来
  getInput(e) {
    this.setData({ remark: e.detail.value })
  },
  // 输入确认
  confirm() {
    const { remark } = this.data
    API_Trade.setRemark(remark).then(() => {
      this.data.params = {
        ...this.data.params,
        remark: remark
      }
      this.setData({
        params: this.data.params
      })
      wx.showToast({ title: '提交成功' })
      this.prompt.hidePrompt()
    })
  },
  // 输入取消
  cancel() {
    this.setData({
      remark: this.data.params.remark
    })
    this.prompt.hidePrompt()
  },
  /** 显示优惠券 */
  handleShowCoupons() {
    this.setData({ showCouponsPopup: true })
    // let { inventories, way } = this.data;
    // let _couponsString = inventories[0].coupon_list && inventories[0].coupon_list.length ? JSON.stringify(inventories[0].coupon_list) : '';
    // wx.navigateTo({
    //   url: `/pages/coupons-select/coupons-select?couponsString=${_couponsString}&&way=${way}`
    // });
  },
  // 确认团长取消
  confirmCancle(e) {
    let { type } = e.currentTarget.dataset;
    if (type === 'orders') {
      this.setData({
        confirmOrders: true
      });
    } else {
      this.setData({
        confirmLeader: true
      });
    };
  },
  // 确认团长确定
  confirmSubmit() {
    if (this.data.subDisabled) {
      return;
    };
    this.createTrade();
  }
})
)