const app = getApp()
let util = require('../../../utils/util.js')
import * as watch from "../../../utils/watch.js";
const middleware = require('../../../utils/middleware.js')
import regeneratorRuntime from '../../../lib/wxPromise.min.js'
import * as API_Trade from '../../../api/trade'
import * as API_Address from '../../../api/address'
import { Foundation } from '../../../ui-utils/index'

Page(middleware.identityFilter({
    data: {
      // 结算店铺id
      sellId: '',
      /** 滚动区域高度 */
      height: '',
      // 是否是拼团
      isAssemble: false,
      // 结算参数
      params: '',
      // 订单总金额
      orderTotal: {
        discount_price: 0,
        exchange_point: 0,
        freight_price: 0,
        goods_price: 0,
        is_free_freight: 0,
        total_price: 0,
      },
      // 购物清单
      inventories: '',
      paymentData: [],
      // 已选地址
      address: '',
      // 已选地址字符串
      addressStr: '',
      // 显示优惠券弹窗
      showCouponsPopup: false,
      // 优惠券张数
      coupon_num: 0,
      // 显示购物清单弹窗
      showInventoryPopup: false,
      // 显示发票信息弹窗
      showReceiptPopup: false,
      // 显示支付配送弹窗
      showPaymentPopup: false,
      // 订单备注暂缓区
      remark: '',
      // 显示错误actionsheet
      showErrorActionsheet: false,
      // 错误actionsheet消息
      errorActionsheetMessage: '',
      // 错误actionsheet数据
      errorActionsheetData: [],
      // 表面跳转过来的是立即购买还是购物车结算
      way: '',
      // 参数 订单id
      orderId: '',
      // 询问框
      prompt: '',
      sellerids: '',//商品所属的商家id
      subDisabled: false
    },
    onLoad(options) {
      this.setData({
        sellerId: options.sellerId,
        way: options.way,
        isAssemble: options.from_nav === 'assemble',
        orderId: options.order_id || ''
      });
      watch.setWatcher(this);//启用数据监听
    },
    watch: {
      inventories: function (newVal) {
        /** 计算总运费、总订单金额 */
        const { orderTotal } = this.data;
        let _act_freight_price = 0; // 实际运费
        let goods_price = orderTotal.goods_price ? orderTotal.goods_price : '0.00,0.00,00';
        let _goods_price = ''; //
        goods_price.split(',').forEach(item => {
          _goods_price = `${_goods_price}${item}`;
        });
        let _act_total_price = parseFloat(_goods_price); // 实际订单金额
        if (newVal) {
          newVal.forEach(item => {
            let _shipFee = item.active_ship_way === 'LOCAL' ? item.price.local_freight_price : item.active_ship_way === 'GLOBAL' ? item.price.global_freight_price : 0;
            _act_freight_price += _shipFee;
            _act_total_price += _shipFee;
          });
        };
        orderTotal.act_freight_price = _act_freight_price.toFixed(2);
        orderTotal.act_total_price = _act_total_price.toFixed(2);
        this.setData({ orderTotal });
      }
    },
    onReady(){
      this.prompt = this.selectComponent("#prompt")
    },
    async onShow() {
      wx.showLoading({ title: '加载中...' })
      // 获取购物清单
      let params
      try {
        params = await API_Trade.getCheckoutParams();
        //拼团订单只能在线支付
        if(this.data.isAssemble){
          params.payment_type = 'ONLINE'
        }
      } catch (e) {
        wx.hideLoading()
      }
      this.setData({
        params: params,
        remark: params.remark,
        height: wx.getSystemInfoSync().windowHeight - 50 + 'px'
      })
      if (params.address_id) {
        await API_Trade.setAddressId(params.address_id)
        const address = await API_Address.getAddressDetail(params.address_id)
        const addressStr = this.formatterAddress(address)
        this.setData({ address:address, addressStr:addressStr })
      }
      // 是否是拼团
      await this.GET_Inventories()
      wx.hideLoading()
    },
    /**
     * 获取购物清单，和结算金额
     * @returns {Promise<void>}
     * @constructor
     */
    GET_Inventories() {
      Promise.all([
        // this.data.isAssemble ? API_Trade.getAssembleCarts() : API_Trade.getCarts('checked', this.data.way),
        API_Trade.getSingleCarts(this.data.sellerId, this.data.way)
      ]).then(response => {
        const { cart_list, total_price } = response[0]
        total_price.goods_price = Foundation.formatPrice(total_price.goods_price)
        total_price.original_price = Foundation.formatPrice(total_price.original_price)
        total_price.discount_price = Foundation.formatPrice(total_price.discount_price)
        total_price.freight_price = Foundation.formatPrice(total_price.freight_price)
        total_price.total_price = Foundation.formatPrice(total_price.total_price)
        if (total_price.coupon_price !== 0){
          total_price.coupon_price = Foundation.formatPrice(total_price.coupon_price)
        }
        if(total_price.cash_back !== 0){
          total_price.cash_back = Foundation.formatPrice(total_price.cash_back)
        }
        this.setData({ orderTotal: total_price })
        let coupon_num = 0
        if (cart_list && cart_list.length) {
          cart_list.forEach(shop => {
            // 处理商品信息
            shop.price.freight_price = Foundation.formatPrice(shop.price.freight_price)
            if(shop.promotion_notice) {
              shop.promotion_notice = Foundation.formatPrice(shop.promotion_notice)
            }
            // 处理优惠券
            if (shop.coupon_list && shop.coupon_list.length) {
              coupon_num += shop.coupon_list.filter(coupon => coupon.enable !== 0).length
              shop.coupon_list.forEach(coupon => {
                coupon.amount = Foundation.formatPrice(coupon.amount)
                coupon.use_term = parseFloat(coupon.use_term.slice(1,6)).toString()
                coupon.end_time = Foundation.unixToDate(coupon.end_time, 'yyyy-MM-dd')
              })
            }
            //处理赠品
            if (shop.gift_coupon_list && shop.gift_coupon_list.length) {
              shop.gift_coupon_list.forEach(gift_coupon => {
                gift_coupon.amount = Foundation.formatPrice(gift_coupon.amount)
              })
            }
            if(shop.gift_list && shop.gift_list.length){
              shop.gift_list.forEach(gift=>{
                gift.gift_price = Foundation.formatPrice(gift.gift_price)
              })
            }
            // 处理sku数据
            if (shop.sku_list && shop.sku_list.length) {
              shop.sku_list.forEach(item => {
                item.purchase_price = Foundation.formatPrice(item.purchase_price)
                item.original_price = Foundation.formatPrice(item.original_price)
                if (!item.skuName) {
                  item.skuName = this.formatterSkuSpec(item)
                }
                if(item.single_list){
                  const _list = item.single_list.filter(item => item.is_check === 1)
                  item.promotionName = _list[0] ? _list[0].title : '不参加活动'
                }
              })
            }
          })
        }
        let _sellerIds = cart_list.map(item => item.seller_id).join(',')
        this.setData({ coupon_num: coupon_num, inventories: cart_list, sellerids:_sellerIds })
      })
    },
    /** 发票信息发生改变 */
    async handleReceiptChanged(e) {
      const receipt = e.detail
      if(receipt.receipt_type === 'VATOSPECIAL'){
        const params= await API_Trade.getCheckoutParams()
        if(params.address_id){
          await API_Trade.setAddressId(params.address_id)
          this.setData({address:await API_Address.getAddressDetail(params.address_id)})
        }
        this.setData({
          params:params,
          remark:params.remark,
          receipt:params.receipt
        })
        return
      }
      this.setData({ 'params.receipt': receipt })
    },
    /** 支付配送发生改变 */
    handlePaymentChanged(e) {
      const payment = e.detail;
      let _inventories = JSON.parse(JSON.stringify(this.data.inventories));
      _inventories = _inventories.map(item => {
        if (item.seller_id === payment.currentShipFee.seller_id) {
          item.active_ship_way = payment.currentShipFee.type
        };
        return item;
      });
      this.setData({
        inventories: _inventories
      });
      // const { params } = this.data;
      // let shipWayList = JSON.parse(JSON.stringify(params.ship_ways));
      // if (shipWayList) {
      //   shipWayList = shipWayList.map(item => {
      //     if (item.seller_id = payment.seller_id) {
      //       item.ship_way = payment.ship_way;
      //     };
      //     return item;
      //   });
      // }
      // params.ship_ways = shipWayList;
      // this.setData({ params })
    },
    /** 格式化地址信息 */
    formatterAddress(address) {
      if (!address) return ''
      return `${address.province} ${address.city} ${address.county} ${address.town} ${address.addr}`
    },
    /** 提交订单 */
    async handleCreateTrade() {
      if (this.data.subDisabled) {
        return;
      };
      this.setData({
        subDisabled: true
      });
      /** 先调用创建订单接口，再跳转到收银台 */
      try {
        // const response = this.data.isAssemble ? await API_Trade.createAssembleTrade('MINI', this.data.orderId) : await API_Trade.createTrade('MINI',this.data.way)
        // if(this.data.isAssemble) { // 更新拼团购物清单数据
        // } else { // 获取购物车信息 正常更新购物清单数据
        //   const { cart_list } = await API_Trade.getCarts('all', 'BUY_NOW')
        //   wx.setStorageSync('shoplist', cart_list)
        // }
        // const { cart_list } = await API_Trade.getCarts('all', 'BUY_NOW');
        // wx.setStorageSync('shoplist', cart_list);
        const { way, sellerId } = { ...this.data };
        const response = await API_Trade.createSingle('MINI', sellerId, way);
        wx.redirectTo({ url: `/pages/cashier/cashier?trade_sn=${response.trade_sn}` });
        this.setData({
          subDisabled: false
        });
      } catch (error) {
        console.log(error);
        this.setData({
          subDisabled: false
        });
        const { data } = error.response || {}
        if (data.data) {
          const { data: list } = data
          if (!list || !list[0]) {
            wx.showToast({ title: data.message,icon:'none'})
            return
          }
        } else {
          wx.showToast({ title: data.message,icon:'none'})
        }
      }
    },
    /** 规格格式化显示 */
    formatterSkuSpec(sku) {
      if (!sku.spec_list || !sku.spec_list.length) return ''
      return sku.spec_list.map(spec => spec.spec_value).join(' - ')
    },
    /** 添加/更换收货地址 */
    handleAddress() {
      wx.navigateTo({
        url: '/pages/ucenter/address/address?from=checkout'
      })
    },
    /* 显示支付配送选择弹窗 */
    handleShowPayment(e) {
      this.setData({
        showPaymentPopup: true,
        paymentData: e.detail
      })
    },
    /* 显示发票选择弹窗 */
    handleShowReceipt() {
      this.setData({ showReceiptPopup: true })
    },
    // 显示prompt 显示备注弹窗
    showPrompt(){
      this.prompt.showPrompt()
    },
    // 将输入的value暂时保存起来
    getInput(e) {
      this.setData({ remark: e.detail.value })
    },
    // 输入确认
    confirm() {
      const { remark } = this.data
      API_Trade.setRemark(remark).then(() => {
        this.data.params = {
          ...this.data.params,
          remark: remark
        }
        this.setData({
          params: this.data.params
        })
        wx.showToast({  title: '提交成功' })
        this.prompt.hidePrompt()
      })
    },
    // 输入取消
    cancel() {
      this.setData({
        remark: this.data.params.remark
      })
      this.prompt.hidePrompt()
    },
    /** 显示优惠券 */
    handleShowCoupons() {
      this.setData({ showCouponsPopup: true })
      // let { inventories, way } = this.data;
      // let _couponsString = inventories[0].coupon_list && inventories[0].coupon_list.length ? JSON.stringify(inventories[0].coupon_list) : '';
      // wx.navigateTo({
      //   url: `/pages/coupons-select/coupons-select?couponsString=${_couponsString}&&way=${way}`
      // });
    }
  })
)