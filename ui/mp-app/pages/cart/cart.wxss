page {
    height: 100%;
    min-height: 100%;
    background: #f4f4f4;
}

view,
text {
    color: inherit;
    font-size: inherit;
}

.container {
    padding-top: 100rpx;
    width: 100%;
    height: auto;
    min-height: 100%;
    box-sizing: border-box;
    overflow: hidden;
}

cover-view {
    line-height: unset;
}
.tab-switch {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 100;
    width: 100%;
    box-sizing: border-box;
    background: #fff;
}
.tab-switch .tab-list {
    display: flex;
    height: 100rpx;
}
.tab-switch .switch-item {
    flex: 1;
    display: flex;
    justify-content: center;
    padding: 0 10rpx;
    line-height: 0;
    text-align: center;
    color: #000;
}
.tab-switch .item-box {
    position: relative;
    padding: 30rpx 0;
    width: 160rpx;
    border-top: 6rpx solid transparent;
    border-bottom: 6rpx solid transparent;
    text-align: center;
    color: #000;
}

.tab-switch .switch-txt {
    display: inline-block;
    line-height: 32rpx;
    text-align: center;
    font-size: 32rpx;
    font-weight: 700;
}

.tab-switch .tip-num {
    position: absolute;
    top: 22rpx;
    left: 50%;
    padding: 0 5rpx;
    margin-left: 32rpx;
    height: 24rpx;
    line-height: 24rpx;
    min-width: 24rpx;
    border-radius: 12rpx;
    box-sizing: border-box;
    background: #e83227;
    color: #fff;
    font-size: 20rpx;
}

.tab-switch .switch-item.active .item-box {
    color: #e83227;
    border-bottom-color: #e83227;
}

/*购物车无数据*/
.no-cart {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

.no-cart .c {
    width: 100%;
    height: auto;
    margin-top: 128rpx;
}

.no-cart .c image {
    margin: 0 auto 12rpx auto;
    display: block;
    text-align: center;
    width: 400rpx;
    height: 400rpx;
}

.no-cart .c text {
    margin: 0 auto;
    display: block;
    width: 220rpx;
    line-height: 28rpx;
    text-align: center;
    color: #a5a5a5;
    font-size: 24rpx;
}

.no-cart .goshopping {
    display: block;
    margin: 60rpx auto 0 auto;
    width: 280rpx;
    line-height: 76rpx;
    border: 2rpx solid #e83227;
    border-radius: 40rpx;
    box-sizing: border-box;
    text-align: center;
    background: #fff;
    color: #e83227;
    font-size: 30rpx;
}

/* cart-list */
.cart-list {
    padding: 20rpx;
}

/* .cart-list .opera-row {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20rpx 10rpx;
}
.cart-list .opera-row .del-list{
  color: #909090;
  font-size: 24rpx;
} */

.cart-list .cart-shop {
    padding: 0 20rpx;
    border-radius: 24rpx;
    background: #fff;
}

.cart-list .cart-shop+.cart-shop {
    margin-top: 20rpx;
}

.cart-list .shop-title {
    display: flex;
    align-items: center;
    position: relative;
    padding: 20rpx 0;
    height: 94rpx;
    box-sizing: border-box;
}

.cart-list .shop-title .show-shop {
    display: flex;
    align-items: center;
    margin-left: 20rpx;
}

.cart-list .show-shop .name {
    line-height: 24rpx;
    color: #000;
    font-size: 24rpx;
}

.cart-list .show-shop icon {
    display: inline-flex;
    justify-content: center;
    align-items: center;
    margin-left: 20rpx;
    width: 12rpx;
    height: 22rpx;
    line-height: 22rpx;
    font-size: 20rpx;
}

.cart-list .type-group-tag {
    margin-left: 20rpx;
    width: 110rpx;
    line-height: 30rpx;
    border-radius: 15rpx;
    text-align: center;
    background: #e83227;
    color: #fff;
    font-size: 20rpx;
}

.cart-list .shop-title .show-coupons {
    position: absolute;
    top: 20rpx;
    right: 0;
    line-height: 54rpx;
    color: #e83227;
    font-size: 24rpx;
}

.cart-list .goods-list {
    max-height: 760rpx;
    transition: max-height 0.5s;
    overflow: hidden;
}

.cart-list .goods-list.more-show {
    max-height: 50000rpx;
}

.cart-list .goods-item {
    display: flex;
    align-items: center;
    position: relative;
}

.cart-list .goods-item .radio_type {
    position: absolute;
    left: 0;
    top: 50%;
    margin-top: -17rpx;
}

.cart-list .goods-item .goods-image {
    position: relative;
    margin-right: 20rpx;
    width: 130rpx;
    height: 130rpx;
}

.cart-list .goods-image image {
    width: 100%;
    height: 100%;
}

.cart-list .goods-item .sku-info {
    flex: 1;
    width: 0;
    position: relative;
}

.cart-list .goods-item .sku-name {
    margin-top: -2rpx;
    line-height: 32rpx;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    color: #000;
    font-size: 28rpx;
}

.cart-list .goods-item .sku-spc {
    margin-top: 6rpx;
    height: 24rpx;
    line-height: 24rpx;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    color: #a5a5a5;
    font-size: 20rpx;
}

.cart-list .goods-item .pick-time,
.cart-list .goods-item .pick-time-tomorrow {
    display: inline-block;
    padding: 0 6rpx;
    margin-top: 8rpx;
    height: 30rpx;
    line-height: 30rpx;
    border-radius: 6rpx;
    font-size: 20rpx;
}
.cart-list .goods-item .pick-time {
    background: rgba(0, 160, 233, .3);
    color: #00a0e9;
}
.cart-list .goods-item .pick-time-tomorrow {
    background: rgba(232, 50, 39, .3);
    color: #e83227;
}

.cart-list .goods-item .sku-price {
    display: flex;
    align-items: flex-end;
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
}

.cart-list .sku-price .price {
    display: inline-block;
    line-height: 24rpx;
    color: #e83227;
    font-size: 28rpx;
}

.cart-list .sku-price text {
    display: inline-block;
    margin-left: 28rpx;
    line-height: 20rpx;
    text-decoration: line-through;
    color: #000;
    font-size: 24rpx;
}

.cart-list .select_num {
    position: absolute;
    right: 0;
    bottom: 22rpx;
}

.invalid .sku-info view,
.invalid .sku-info span,
.invalid .sku-info text {
    /* opacity: .5; */
    color: #a5a5a5 !important;
}

.invalid .invalid-tip {
    position: absolute;
    top: 20rpx;
    left: 20rpx;
    width: 80rpx;
    height: 80rpx;
    line-height: 80rpx;
    border-radius: 50%;
    text-align: center;
    background: rgba(0, 0, 0, .5);
    color: #fff;
    font-size: 20rpx;
}

.shop-goods .show-more {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 30rpx;
    height: 50rpx;
    line-height: 50rpx;
    border-radius: 25rpx;
    background: #f0f0f0;
    color: #a5a5a5;
    font-size: 20rpx;
}

.shop-goods .show-more .iconfontimg {
    display: inline-flex;
    justify-content: center;
    align-items: center;
    width: 22rpx;
    height: 22rpx;
    font-size: 18rpx;
}

.shop-goods .show-more span + .iconfontimg {
    margin-left: 16rpx;
}

.shop-goods .shop-settle {
    display: flex;
    align-items: center;
    padding-bottom: 30rpx;
}

.shop-goods .shop-settle .shop-total {
    flex: 1;
    width: 0;
    text-align: right;
    color: #a5a5a5;
    font-size: 24rpx;
}

.shop-goods .shop-settle .total-price {
    margin-left: 40rpx;
    color: #000;
    font-size: 28rpx;
    font-weight: 600;
}

.shop-goods .total-price text {
    color: #e83227;
    font-size: 36rpx;
}

.shop-goods .shop-settle .shop-submit {
    margin-left: 29rpx;
    width: 190rpx;
    height: 80rpx;
    line-height: 80rpx;
    text-align: center;
    border-radius: 40rpx;
    background: linear-gradient(90deg, #fd7f26, #ff3444);
    color: #fff;
    font-size: 30rpx;
}

.shop-goods .shop-settle .shop-submit[disabled] {
    background: linear-gradient(90deg, #c9c9c9, #a5a5a5);
    color: #fff!important;
}

.goods-item-box {
    position: relative;
    z-index: 5;
    display: flex;
    width: 100%;
    min-height: 180rpx;
    padding: 30rpx 0 30rpx 54rpx;
    background-color: #fff;
    transition: left 0.2s ease-in-out;
}

.goods-del {
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    top: 0;
    right: 0;
    z-index: 4;
    width: 120rpx;
    height: 100%;
    background-color: #f42424;
    color: #fff;
}

/** 底部按钮 */
.cart-bottom {
    position: fixed;
    bottom: 0;
    left: 0;
    z-index: 99;
    display: flex;
    align-items: center;
    width: 100%;
    height: 120rpx;
    box-sizing: border-box;
    background: #fff;
}

.cart-bottom .checkbox {
    margin: -16rpx 0 0 30rpx;
}

.cart-bottom .total {
    height: 34rpx;
    margin: 0 10rpx;
    font-size: 25rpx;
    color: #f42424;
}

.cart-bottom .delete {
    height: 34rpx;
    width: auto;
    margin: 33rpx 18rpx;
    font-size: 29rpx;
}

.cart-bottom .checkout {
    height: 80rpx;
    width: 190rpx;
    text-align: center;
    line-height: 80rpx;
    font-size: 30rpx;
    background: #f42424;
    color: #fff;
}

.cart-bottom .checkout.disabled {
    background: #e8e8e8;
    color: #999;
}