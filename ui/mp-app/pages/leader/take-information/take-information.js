// pages/leader/take-information/take-information.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.foldAnimate = wx.createAnimation({
      duration:600,//整个动画多长时间完成
      timingFunction:"ease",//匀速完成
      delay:80,//延迟时间
    })
  },

  //折叠订单
  foldOrder () {
    if(this.data.isShow){
      this.foldAnimate.height(0).step();
      this.setData({
        foldAnimate: this.foldAnimate.export(),
        isShow: false
      })
    }else{
      this.foldAnimate.height(600).step();
      this.setData({
        foldAnimate: this.foldAnimate.export(),
        isShow: true
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})