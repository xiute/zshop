// pages/InWarehouse/in-warehouse.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //折叠效果
    isShow: true,
    foldAnimate: '',
    //全选
    isAll: false,
    //订单选择数组
    selectBox:[{index:0,type:false},{index:1,type:false},{index:2,type:false}]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.foldOrder();
  },

  //折叠订单
  foldOrder () {
    let option = {
      duration:600,//整个动画多长时间完成
      timingFunction:"ease",//匀速完成
      delay:80,//延迟时间
    };
    let animation_ins = wx.createAnimation(option);
    if (this.data.isShow) {
      animation_ins.height(600).step();
      this.setData({
        foldAnimate: animation_ins.export(),
        isShow: !this.data.isShow
      })
    } else {
      animation_ins.height(0).step();
      this.setData({
        foldAnimate: animation_ins.export(),
        isShow: !this.data.isShow
      })
    }
  },

  //订单全选
  selectAll () {
    let status = !this.data.isAll;
    if(status) {
      this.data.selectBox.forEach(item => {
        item.type=true;
      })
    } else {
      this.data.selectBox.forEach(item => {
        item.type=false;
      })
    }
    this.setData({
      selectBox: this.data.selectBox,
      isAll: status
    })
  },

  //订单选择
  orderselect (e){
    let _index = Number(e.currentTarget.dataset.index);
    let box = 'selectBox['+_index+'].type';
    this.setData({[box]: !this.data.selectBox[_index].type});
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})