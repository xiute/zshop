import * as API_Home from '../../api/home'

Page({
  data: {
    categoryList: [],           // 所有1级分类
    currentCategory: {},        // 当前选中分类
    scrollLeft: 0,
    scrollTop: 0,
    goodsCount: 0,
    scrollHeight: 0
  },
  onLoad: function (options) {
    let that = this;
    API_Home.getCategory(0).then(function(data){
      that.setData({
        categoryList: data,
        currentCategory: data[0]
      });
    })
  },
  setCurrentCategory: function (id) {
    let that = this;
    for (var item of that.data.categoryList) {
      if (item.category_id == id) {
        that.setData({
          currentCategory: item
        });
        break;
      }
    }
  },
  switchCate: function (event) {
    var that = this;
    var currentTarget = event.currentTarget;
    if (this.data.currentCategory.category_id == event.currentTarget.dataset.id) {
      return false;
    }

    this.setCurrentCategory(event.currentTarget.dataset.id);
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
})