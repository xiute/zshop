import * as API_Trade from "../../api/trade.js"
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    goodsNum: {
      type: String,
      value: 0
    },
    skuNum: {
      type: String,
      value: 0
    },
    totalNum: {
      type:String,
      value: 0
    },
    isHidden: {
      type: Boolean,
      value: true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    goodsNum: '0',
    skuNum: '0',
    totalNum: '0',
    isHidden: true
  },
  // 生命周期
  lifetimes: {
    attached() {
      this.getPageData();
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getPageData() {
      API_Trade.getGuideCartCount().then(response => {
        // console.log(response)
        this.setData({
          goodsNum: '2',
          skuNum: '3'
        })
      })
    }
  }
})
