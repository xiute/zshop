// components/bottom-menu/bottom-menu.js

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    list: [
      {
        "pagePath": "pages/index-group/index/index",
        "iconPath": "static/images/menu-icon-group.png",
        "selectedIconPath": "static/images/menu-icon-group-act.png",
        "text": "团购"
      },
      {
        "pagePath": "pages/index/index",
        "iconPath": "static/images/menu-icon-shop.png",
        "selectedIconPath": "static/images/menu-icon-shop-act.png",
        "text": "商城"
      },
      {
        "pagePath": "pages/cart/cart",
        "iconPath": "static/images/menu-icon-cart.png",
        "selectedIconPath": "static/images/menu-icon-cart-act.png",
        "text": "购物车"
      },
      {
        "pagePath": "pages/ucenter/index/index",
        "iconPath": "static/images/menu-icon-person.png",
        "selectedIconPath": "static/images/menu-icon-person-act.png",
        "text": "我的"
      }
    ],
    activeMenu: ''
  },

  // 生命周期
  lifetimes: {
    async ready() {
      const pages = getCurrentPages();
      const currentUrl = pages[pages.length - 1].route;
      this.setData({
        activeMenu: currentUrl
      });
    }
  },

  // 数据监听器
  observers: {
  },

  /**
   * 组件的方法列表
   */
  methods: {
    menuShow: function (e) {
      const pages = getCurrentPages();
      const currentUrl = pages[pages.length - 1].route;
      const { url } = e.currentTarget.dataset;
      if (currentUrl !== url) {
        this.setData({
          activeMenu: url
        });
        wx.navigateTo({
          url: `/${url}`
        })
      };
    }
  }
})
