// components/ative-leader/ative-leader.js

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    navHeight: {
      type: Number
    },
    leaderData: {
      type: Object
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    nearTop: 0,
    currentData: {}
  },

  // 生命周期
  lifetimes: {
    async ready() {
      this.setData({
        nearTop: this.properties.navHeight
      });
    }
  },
  // 数据监听器
  observers: {
    leaderData() {
      const { leaderData } = this.properties;
      this.setData({
        currentData: leaderData
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {

  }
})
