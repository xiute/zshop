// pages/floor/template/tpl9000/tpl9000.js
Component({
  options: {
    addGlobalClass: true,
  },
  /**
   * 组件的属性列表
   */
  properties: {
    catsTypeList: {
      type: Array,
      value: []
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    swiperHight: 328,
    typeList: []
  },
  // 数据监听器
  observers: {
    catsTypeList(newVal) {
      let _height = 328;
      let _twoRowCells = [];
      if (newVal.length > 1 && newVal.length <= 5) {
        _height = 164;
        for (let i = 0; i < 5; i++) {
          let _cell = newVal[i] || {};
          _twoRowCells.push(_cell);
        };
      } else if (newVal.length > 5 && newVal.length <= 10) {
        newVal.forEach((item, index) => {
          if (index < 5) {
            _twoRowCells.push(item);
          } else {
            _twoRowCells[index - 5] = {
              ..._twoRowCells[index - 5],
              hasTwo: true,
              cat_id: item.shop_cat_id,
              cat_name: item.shop_cat_name,
              cat_pic: item.shop_cat_pic
            };
          };
        });
      } else if (newVal.length > 10) {
        _height = 336;
        newVal.forEach((item, index) => {
          if (index < 5) {
            _twoRowCells.push(item);
          } else if (index > 4 && index < 10) {
            _twoRowCells[index - 5] = {
              ..._twoRowCells[index - 5],
              hasTwo: true,
              cat_id: item.shop_cat_id,
              cat_name: item.shop_cat_name,
              cat_pic: item.shop_cat_pic
            };
          } else {
            if (index % 2 === 0) {
              _twoRowCells.push(item);
            } else {
              // 获取数组最后一项的值修改后再添加志数组末尾
              let _last = _twoRowCells.pop(); // 最后一项
              _last = {
                ..._last,
                hasTwo: true,
                cat_id: item.shop_cat_id,
                cat_name: item.shop_cat_name,
                cat_pic: item.shop_cat_pic
              };
              _twoRowCells.push(_last);
            };
          };
        });
      };
      this.setData({
        swiperHight: _height,
        typeList: _twoRowCells
      });
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转分组页面
    getHref(e) {
      const _id = e.currentTarget.dataset.id;
      wx.navigateTo({
        url: `/pages/grouping/grouping?grouping_id=${_id}`
      })
    }
  }
})
