// pages/floor/template/tpl62/tpl62.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tplItemData: {
      type: Object,
      value: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    getHref: function (e) {
      const _operate = e.currentTarget.dataset.operate;
      this.triggerEvent('hrefEvent', _operate)
    }
  }
})
