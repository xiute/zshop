// pages/floor/template/tpl70/tpl70.js
import * as API_Shop from '../../../api/shop.js'
const _app = getApp();

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tplItemData: {
      type: Object,
      value: {}
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    shopListData: []
  },
  // 生命周期
  lifetimes: {
    async ready() {
      const _locationData = _app.globalData.locationData;
      const _tplData = this.properties.tplItemData;
      let _params = { 
        'shop_lat': _locationData.latitude,
        'shop_lng': _locationData.longitude,
        'shop_tag': _tplData.blockStyle.showType ? _tplData.blockStyle.shop_tag : '',
        'page_size': _tplData.blockStyle.limitNum ? _tplData.blockStyle.limitNum : 3,
        'page_no': 1
      };
      let response = await API_Shop.getNearbyShops(_params);
      this.setData({
        shopListData: response.data
      });
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {

  }
})
