// pages/floor/template/tpl66/tpl66.js
import * as API_Promotions from '../../../api/promotions.js'
import * as API_Members from '../../../api/members.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tplItemData: {
      type: Object,
      value: {}
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    couponList: []
  },
  // 生命周期
  lifetimes: {
    async ready() {
      const _this = this;
      const { blockList } = { ...this.properties.tplItemData };
      let _ids = [];
      _ids = blockList.map(item => {
        return item.block_value.id;
      });
      let response = await API_Promotions.getCouponListByIds(_ids.join(','));
      let _list = response.map(item => {
        return {
          'id': item.coupon_id,
          'value': item.coupon_price,
          'tag': '元', // 当前只支持满减
          'term': item.title,
          'lastNum': item.create_num - item.received_num
        };
      });
      this.setData({
        couponList: _list
      });
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    receiveCoupon: function(e) {
      let _id = e.currentTarget.dataset['id'];
      if (!wx.getStorageSync('refresh_token')) {
        wx.showToast({ title: '您还未登录!', image: 'https://localhost/images/icon_error.png' })
        return false
      };
      API_Members.receiveCoupons(_id).then(() => {
        wx.showToast({ title: '领取成功' });
      });
    }
  }
})
