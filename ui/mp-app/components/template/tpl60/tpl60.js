// pages/floor/template/tpl60/tpl60.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tplItemData: {
      type: Object,
      value: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    pagePadding: 0,
    radius: 0,
    swiperHight: 0,
    swiperData: []
  },
  // 生命周期
  lifetimes: {
    async ready() {
      const _this = this;
      let { blockStyle, blockList } = { ...this.properties.tplItemData };
      // 处理http图片历史数据
      let _blockList = blockList.map(item => {
        if (item.block_value && item.block_value.img_url && item.block_value.img_url.split('http://')[1]) {
          item.block_value.img_url = `https://${item.block_value.img_url.split('http://')[1]}`;
        }
        return item;
      });
      const _imgUrl = _blockList[0].block_value.img_url;
      if (_imgUrl) {
        await wx.getImageInfo({
          src: _imgUrl,
          success: function (res) {
            const _windowWidth = wx.getSystemInfoSync().windowWidth;
            let _useWidth = !blockStyle.pagePadding ? _windowWidth : _windowWidth - blockStyle.pagePadding * 2;
            let _height = _useWidth * (res.height / res.width);
            _this.setData({
              swiperHight: _height
            });
          }
        });
      };
      this.setData({
        pagePadding: blockStyle.pagePadding ? blockStyle.pagePadding : 0,
        radius: blockStyle.radius ? blockStyle.radius : 0,
        swiperData: _blockList
      });
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转分类页面
    getHref: function(e) {
      const _operate = e.currentTarget.dataset.operate;
      this.triggerEvent('hrefEvent', _operate)
    }
  }
})
