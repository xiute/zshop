// components/nav/locationNav.js
const app = getApp();
let that;

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 获取位置权限
    getLocation: {
      type: Boolean,
      value: false
    },
    // 位置信息
    locationData: {
      type: Object,
      value: {}
    }
  },
  data: {
    showChange: false,
    addressShow: '',
    statusBarHeight: 0, // 状态栏高度
    navbarHeight: 0, // 顶部导航栏高度
    navbarBtn: { // 胶囊位置信息
      height: 0,
      width: 0,
      top: 0,
      bottom: 0,
      right: 0
    },
    cusnavH: 0, // title高度
  },
  // 微信7.0.0支持wx.getMenuButtonBoundingClientRect()获得胶囊按钮高度
  attached: function () {
    that = this;
    if (!app.globalData.systeminfo) {
      app.globalData.systeminfo = wx.getSystemInfoSync();
    }
    if (!app.globalData.headerBtnPosi) app.globalData.headerBtnPosi = wx.getMenuButtonBoundingClientRect();
    let statusBarHeight = app.globalData.systeminfo.statusBarHeight // 状态栏高度
    let headerPosi = app.globalData.headerBtnPosi // 胶囊位置信息
    let btnPosi = { // 胶囊实际位置，坐标信息不是左上角原点
      height: headerPosi.height,
      width: headerPosi.width,
      top: headerPosi.top - statusBarHeight, // 胶囊top - 状态栏高度
      bottom: headerPosi.bottom - headerPosi.height - statusBarHeight, // 胶囊bottom - 胶囊height - 状态栏height （胶囊实际bottom 为距离导航栏底部的长度）
      right: app.globalData.systeminfo.windowWidth - headerPosi.right // 这里不能获取 屏幕宽度，PC端打开小程序会有BUG，要获取窗口高度 - 胶囊right
    }
    let haveBack;
    if (getCurrentPages().length != 1) { // 当只有一个页面时，并且是从分享页进入
      haveBack = false;
    } else {
      haveBack = true;
    }
    var cusnavH = btnPosi.height + btnPosi.top + btnPosi.bottom // 导航高度
    // console.log(app.globalData.systeminfo.windowWidth, headerPosi.width)
    this.setData({
      haveBack: haveBack, // 获取是否是通过分享进入的小程序
      statusBarHeight: statusBarHeight,
      navbarHeight: headerPosi.bottom + btnPosi.bottom, // 胶囊bottom + 胶囊实际bottom
      navbarBtn: btnPosi,
      cusnavH: cusnavH
    });
    //将实际nav高度传给父类页面
    this.triggerEvent('commonNavAttr', {
      height: headerPosi.bottom + btnPosi.bottom
    });
    
    const _locationData = app.globalData.locationData;
    this.setData({
      addressShow: _locationData.addressName
    });
  },
  // 生命周期
  lifetimes: {
    async ready() {
      if (app.globalData.systemInfo) {
        await app.getHeader();
      };
      this.setData({
        fixedTop: app.globalData.navbar.height - 1
      });
      const _locationData = app.globalData.locationData;
      this.setData({
        addressShow: _locationData.addressName
      });
    }
  },
  // 数据监听器
  observers: {
    getLocation(newValue) {
      this.setData({
        showChange: newValue
      });
    },
    locationData() {
      const { locationData } = this.properties;
      if (!locationData.hasAuth || (!locationData.latitude && !locationData.longitude)) {
        return;
      };
      this.setData({
        addressShow: locationData.addressName
      });
      let _history = wx.getStorageSync('locationHistory') || [];
      if (_history.length && _history[0].addressName === locationData.addressName) {
        return;
      };
      _history = _history.filter(item => item.addressName !== locationData.addressName);
      _history.unshift(locationData);
      if (_history.length > 4) {
        _history = _history.splice(0, 4);
      };
      wx.setStorageSync('locationHistory', _history);
    }
  },
  methods: {
    watchBack: function (value) {
      that.setData({
        addressShow: value.addressName
      });
    },
    // 返回上一页
    pageBack: function () {
      wx.navigateBack();
    },
    openSetting: function (res) {
      console.log(res);
      if (res.detail.authSetting['scope.userLocation'] === true) {
        // 储存获取位置权限信息
        wx.setStorageSync('getLocation', true);
        wx.navigateTo({
          url: '/pages/city-list/city-list'
        });
      } else {
        // 储存获取位置权限信息
        wx.setStorageSync('getLocation', false);
      };
    },
    // 切换选择地址页面
    changeLocation: function () {
      // 获取用户网路状态
      wx.getNetworkType({
        success(response){
          if (response.networkType === 'none') {
            wx.showToast({
              title: '无法连接网络，请检查您的网络',
              icon: 'none'
            });
            setTimeout(() => {
              wx.hideToast();
            }, 1500);
            return;
          };
          // 获取用户授权
          wx.getSetting({
            success(res) {
              wx.navigateTo({
                url: '/pages/city-list/city-list'
              });
              console.log(res);
            }, fail(error) {
              if (error.errMsg === 'getSetting:fail getaddrinfo ENOTFOUND servicewechat.com') {
                wx.showToast({
                  title: '无法连接网络，请检查您的网络',
                  icon: 'none'
                });
              } else {
                wx.showToast({
                  title: '获取位置信息失败，请打开手机定位服务',
                  icon: 'none'
                });
              };
              setTimeout(() => {
                wx.hideToast();
              }, 1500);
            }
          });
        }
      });
    }
  }
})
