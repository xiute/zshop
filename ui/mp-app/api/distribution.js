import request from '../utils/request'

/**
 * 申请分销
 * @param params
 */
export function applyDistribution(params) {
  return request.ajax({
    url: '/distribution/apply',
    method: 'post',
    isJson: true,
    loading: true,
    needToken: true,
    params
  })
}

/**
 * 生成短链接
 */
export function generateShortLink() {
  return request.ajax({
    url: '/distribution/su/get-short-url',
    method: 'post',
    needToken: true
  })
}

/**
 * 访问短链接
 * @param params
 */
export function accessShortLink(params) {
  return request.ajax({
    url: '/distribution/su/visit',
    method: 'get',
    params
  })
}

/**
 * 生成小程序码
 * @param params
 */
export function getMiniprogramCode(params) {
  return request.ajax({
    url: '/passport/mini-program/code-unlimit',
    method: 'get', 
    needToken: true,
    params
  })
}

/**
 * 获取我的推荐人
 */
export function getMyRefereer() {
  return request.ajax({
    url: '/distribution/recommend-me',
    method: 'get',
    needToken: true 
  })
}

/**
 * 获取推荐人列表
 */
export function getRefereeList() {
  return request.ajax({
    url: '/distribution/lower-list',
    method: 'get',
    loading: true,
    needToken: true
  })
}

/**
 * 获取与我相关的结算单信息
 * @param params
 */
export function getSettlementTotal(params) {
  return request.ajax({
    url: '/distribution/bill/member',
    method: 'get', 
    needToken: true ,
    params
  })
}

/**
 * 获取与我相关的订单信息
 * @param params
 */
export function getRelevantList(params) {
  return request.ajax({
    url: '/distribution/bill/order-list',
    method: 'get', 
    needToken: true ,
    params
  })
}

/**
 * 获取与我相关的退款单信息
 * @param params
 */
export function getRelevantRefundList(params) {
  return request.ajax({
    url: '/distribution/bill/sellback-order-list',
    method: 'get', 
    needToken: true ,
    params
  })
}

/**
 * 获取我的历史业绩
 * @param params
 */
export function getMyHistoryList(params) {
  return request.ajax({
    url: '/distribution/bill/history',
    method: 'get', 
    needToken: true ,
    params
  })
}

/**
 * 获取提现参数设置
 */
export function getWithdrawalsParams() {
  return request.ajax({
    url: '/distribution/withdraw/params',
    method: 'get', 
    needToken: true 
  })
}

/**
 * 保存提现设置
 * @param params
 */
export function reserveWithdrawalsParams(params) {
  return request.ajax({
    url: '/distribution/withdraw/params',
    method: 'put', 
    needToken: true ,
    params
  })
}

/**
 * 申请提现
 * @param params
 */
export function applyWithdrawals(params) {
  return request.ajax({
    url: '/distribution/withdraw/apply-withdraw',
    method: 'post', 
    needToken: true ,
    params
  })
}

/**
 * 获取可提现金额
 */
export function getWithdrawalsCanRebate() {
  return request.ajax({
    url: '/distribution/withdraw/can-rebate',
    method: 'get', 
    needToken: true 
  })
}

/**
 * 获取提现记录
 * @param params
 */
export function getWithdrawalsList(params) {
  return request.ajax({
    url: '/distribution/withdraw/apply-history',
    method: 'get', 
    needToken: true ,
    params
  })
}

/**
 * 生成分享标识键
 */
export function getShareKey(params) {
  return request.ajax({
    url: '/distribution/minipro/create-share',
    method: 'get', 
    needToken: true,
    params
  })
}

/**
 * 访问分享链接，绑定关系
 */
export function visitBind(params) {
  return request.ajax({
    url: '/distribution/minipro/visit',
    method: 'get', 
    needToken: true,
    params
  })
}

/**
  * @date 2020/08/22
  * @author kaiqiang
  * @description { 我的下级首页 }
*/
export function getFansIndexList() {
  return request.ajax({
    url: '/distribution/fans_index',
    method: 'get', 
    needToken: true,
  })
}

/**
  * @date 2020/08/24
  * @author kaiqiang
  * @description { 查询邀请人列表 }
*/
export function getRegInviteList(params) {
  return request.ajax({
    url: '/distribution/query_inviter_list',
    method: 'get', 
    needToken: true,
    loading: true,
    params
  })
}

/**
  * @date 2020/08/24
  * @author kaiqiang
  * @description { 查询我的粉丝列表 }
*/
export function getMyFansList(params) {
  return request.ajax({
    url: '/distribution/query_my_fans_list',
    method: 'get', 
    needToken: true,
    loading: true,
    params
  })
}

/**
  * @date 2020/08/24
  * @author kaiqiang
  * @description { 查询下级团长 }
*/
export function getSubHeadList(params) {
  return request.ajax({
    url: '/distribution/query_next_leader_list',
    method: 'get',
    needToken: true,
    loading: true,
    params
  })
}

/**
  * @date 2020/08/25
  * @author kaiqiang
  * @description { 获取下级团长粉丝列表 }
*/
export function getSubHeadFansList(params) {
  return request.ajax({
    url: '/distribution/query_my_fans_list',
    method: 'get', 
    needToken: true,
    loading: true,
    params
  })
}

/**
  * @date 2020/08/25
  * @author kaiqiang
  * @description { 获取我的上级首页列表 }
*/
export function getSuperiorList() {
  return request.ajax({
    url: '/distribution/superior_index',
    method: 'get', 
    loading: true,
    needToken: true
  })
}

/**
  * @date 2020/09/04
  * @author kaiqiang
  * @description { 多退少补列表数据 }
*/
export function getMoreReturnList(params) {
  return request.ajax({
    url: '/after-sales/refund/spread',
    method: 'get', 
    loading: true,
    needToken: true,
    params
  })
}

/**
  * @date 2020/10/28
  * @author kaiqiang
  * @description { 获取我的上级团长 }
*/
export function getMySuperiorDistribution() {
  return request.ajax({
    url: '/distribution/my_superior_distribution',
    method: 'get', 
    needToken: true,
  })
}

/**
  * @date 2020/10/28
  * @author kaiqiang
  * @description { 修改我的上级团长 }
*/
export function updateSuperiorDistribution(params) {
  return request.ajax({
    url: '/distribution/update_superior_distribution/',
    method: 'get', 
    needToken: true,
    params
  })
}
