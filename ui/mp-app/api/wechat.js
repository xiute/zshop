/**
 * 微信相关API
 */

import request from '../utils/request'

/**
 * 获取模版数据
 * @param params[type: PAY_NOTICE;SHIP_NOTICE;CANCEL_NOTICE_1;CANCEL_NOTICE_2;CANCEL_NOTICE_3;RETUND_NOTICE_1;RETUND_NOTICE_2]
 */
export function getNoticeTpl(params) {
  return request.ajax({
    url: '/wechat/minipro',
    method: 'get',
    params
  })
}