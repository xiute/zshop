/**
 * 首页楼层相关API
 */

import request from '../utils/request'

/**
 * 获取楼层数据
 * @param client_type
 * @param page_type(0:自定义页面;1:商城首页;2:团购首页;3:附近首页)
 */
export function getFloorCityData(client_type = 'WXS', page_type = '1', city) {
  return request.ajax({
    url: `/pages/v2/${client_type}/${page_type}?city=${city}`,
    method: 'get',
    loading: true
  })
}

/**
 * 获取楼层数据
 * @param client_type
 * @param page_type (0:自定义页面;1:商城首页;2:团购首页;3:附近首页)
 */
export function getFloorData(client_type = 'WXS', page_type = '0', page_id) {
  return request.ajax({
    url: `/pages/v2/${client_type}/${page_type}?page_id=${page_id}`,
    method: 'get',
    loading: true
  })
}

/**
 * 获取发现页面数据
 * @param client_type
 * @param page_type
 */
export function getDiscoverData(client_type = 'WXS', cityName) {
  return request.ajax({
    url: `/pages/get_discover/${client_type}/${cityName}`,
    method: 'get',
    loading: true
  })
}