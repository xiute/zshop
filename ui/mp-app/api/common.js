/**
 * 公共API
 */
import request from '../utils/request'
import { api } from '../config/config'

const urlPrefix = api.base + '/'

/**
 * 获取图片验证码URL
 * @param uuid
 * @param type
 * @returns {string}
 */
export function getValidateCodeUrl(uuid, type) {
  if (!uuid || !type) return ''
  return `${urlPrefix}captchas/${uuid}/${type}?r=${new Date().getTime()}`
}

/**
 * 获取站点设置
 */
export function getSiteData() {
  return request.ajax({
    url: `${urlPrefix}site-show`,
    method: 'get'
  })
}
/**
 * 记录商品浏览量【用于统计】
 */
export function recordView(url) {
  return request.ajax({
    url: '/view',
    method: 'get',
    needToken: !!wx.getStorageSync('refresh_token'),
    params:{
      url,
      uuid: wx.getStorageSync('uuid')
    }
  })
}