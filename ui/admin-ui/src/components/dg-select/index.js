import dgSelect from './select';

/* istanbul ignore next */
dgSelect.install = function (Vue) {
  Vue.component(dgSelect.name, dgSelect);
};

export default dgSelect
