/**团长管理api */
import request from '@/utils/request'

/**
  * @date 2020/06/17
  * @author kjh
  * @description {页面初始化获取团长信息列表}
*/

export function getLeaderList(params) {
  return request({
    url: 'admin/members/leader/list',
    method: 'get',
    loading: false,
    params
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {获取团长信息详情列}
*/
export function askLeaderList(params) {
  return request({
    url: `admin/members/leader/get/${params}`,
    method: 'get',
    loading: false 
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {发送添加团长请求}
*/
export function addLeaderList(params) {
  return request({
    url: 'admin/members/leader/add_leader',
    method: 'post',
    loading: false,
    params
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {发送修改团长信息请求}
*/
export function updateLeaderList(params) {
  return request({
    url: 'admin/members/leader/update_pick_up_site',
    method: 'post',
    loading: true,
    params
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {发送删除单行团长信息请求}
*/
export function deleteLeaderList(params) {
  return request({
    url: `admin/members/leader/${params}`,
    method: 'delete',
    loading: false 
  })
}

/**
  * @date 2020/06/17
  * @author kjh
  * @description {查询单个会员信息}
*/
export function getMemberList(params) {
  return request({
    url: 'admin/members/',
    method: 'get',
    loading: false,
    params
  })
}

/**团购订单api */
/**
  * @date 2020/06/17
  * @author kjh
  * @description {页面初始化获取团购订单列表}
*/
export function getOrderList(params) {
  return request({
    url: '/admin/promotion/shetuan/shetuanOrderPage',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
  * @date 2020/10/12
  * @author zkq
  * @description {修改团长配置优先级}
*/
export function getUpdatePriority(params) {
  return request({
    url: '/admin/members/leader/updatePriority/' + params.leader_id + '/' + params.ship_priority,
    method: 'post',
    loading: true,
    headers: { 'Content-Type': 'application/json' }
  })
}

/**
 * 分销审核 /admin/members/leader/audit
 * @param params
 */
export function auditDistributor(params) {
  return request({
    url: 'admin/members/leader/audit',
    method: 'post',
    loading: true,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 启用或者禁用
 * @param params
 */
export function openOrCloseLeader(params) {
  return request({
    url: 'admin/members/leader/openOrClose',
    method: 'post',
    loading: false,
    params
  })
}

/**
 * 佣金管理的查询接口
 * @param params
 */
export function queryProfitList(params) {
  return request({
    url: 'admin/trade/profit/query_profit_list',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

export function distributionOrderRebuild(params) {
  return request({
    url: '/admin/distribution/order/rebuild?order_sn=' + params,
    method: 'get',
    loading: true
  })
}

/**
  * @date 2020/10/28
  * @author kaiqiang
  * @description { 导出佣金管理excel }
*/
export function exportProfitList(params) {
  return request({
    responseType: 'blob',
    url: '/admin/trade/profit/export_profit_list',
    method: 'post',
    loading: true,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}
