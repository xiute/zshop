/**
 * 数据字典相关API
 */

import request from '@/utils/request'
import { api } from '~/ui-domain'

/**
 * 获取数据字典列表
 * @param params
 */
export function GET_DataDict(params) {
  return request({
    url: `${api.base}/dict/query_dict_list`,
    method: 'post',
    loading: false,
    params
  })
}

/**
 * 获取数据字典列表
 * @param params
 */
export function queryAllType() {
  return request({
    url: `${api.base}/dict/query_all_type`,
    method: 'get',
    loading: false
  })
}

/**
 * 删除一条数据
 * @param params
 */
export function Del_DataDict(id) {
  return request({
    url: `${api.base}/dict/del_dict/${id}`,
    method: 'get',
    loading: false
  })
}

/**
 * 添加
 * @param params
 */
export function add_DataDict(params) {
  return request({
    url: `${api.base}/dict/add_dict`,
    method: 'post',
    loading: false,
    params
  })
}

/**
 * 修改
 * @param params
 */
export function udp_DataDict(params) {
  return request({
    url: `${api.base}/dict/udp_dict`,
    method: 'post',
    loading: false,
    params
  })
}

/**
 * 根据id查询
 * @param params
 */

export function query_DictById(id) {
  return request({
    url: `${api.base}/dict/query_dict_by_id/${id}`,
    method: 'get',
    loading: false
  })
}
