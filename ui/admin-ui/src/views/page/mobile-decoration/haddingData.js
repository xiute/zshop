/* eslint-disable */
/**
 * Created by WolfMan on 2020/01/13.
 */

/** hadding */
/*
  * @date 2020-01-13
  * @author wolfMan
  * @description {右侧设置模块数据初始化}
*/
export function haddingDefault (data) {
  const tplId = target.tpl_id;
  const blockData = JSON.parse(JSON.stringify(data.blockList));
  const blockStyle = JSON.parse(JSON.stringify(data.blockStyle));
  let _default = null;
  let _listVal = [];
  let _listOpt = [];
  if (blockData[0].block_value === '' || blockData[0].block_value === null) {
    return _default;
  } else {
    blockData.forEach(item => {
      _listVal.push(item.block_value);
      _listOpt.push(item.block_opt);
    });
    _default = {
      ...blockStyle,
    };
    switch (tplId) {
      case 60:
        _default.valList = _listVal;
        _default.optList = _listVal;
        break;
    };
  };
};