/*
  * @date 2020-02-05
  * @author wolfMan
  * @description {营销组件}
*/

import tpl_66 from './tpl_66';
import tpl_67 from './tpl_67';
import tpl_68 from './tpl_68';
import tpl_69 from './tpl_69';
import tpl_72 from './tpl_72';
import tpl_74 from './tpl_74';
import tpl_75 from './tpl_75';
import tpl_9000 from './tpl_9000';
import tpl_9001 from './tpl_9001';

export default {
  66: tpl_66,
  // 67: tpl_67,
  // 68: tpl_68,
  69: tpl_69,
  72: tpl_72,
  74: tpl_74,
  75: tpl_75,
  9000: tpl_9000,
  9001: tpl_9001
}
