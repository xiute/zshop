/**
 * Created by Andste on 2018/5/18.
 */

import common from './common'
import active from './active'
import customs from './customs'

const templates = {
  ...common,
  ...active,
  ...customs
}

const templateArray = []
const templateArrayMenu = [{
  title: '基础组件',
  list: []
}, {
  title: '营销组件',
  list: []
}]
Object.keys(common).forEach(key => {
  if (common[key].dataTpl) {
    templateArray.push(common[key].dataTpl)
    templateArrayMenu[0].list.push((common[key].dataTpl))
  }
})

Object.keys(active).forEach(key => {
  if (active[key].dataTpl) {
    templateArray.push(active[key].dataTpl)
    templateArrayMenu[1].list.push((active[key].dataTpl))
  }
})

Object.keys(customs).forEach(key => {
  if (customs[key].dataTpl) templateArray.push(customs[key].dataTpl)
})

export { templateArray, templateArrayMenu }
export default templates
