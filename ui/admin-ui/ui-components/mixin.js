/**
 * Created by WolfMan on 2020/01/13.
 */

/** mixin */
export default {
  filters: {
    filterRadio (val, option) {
      let _back = '';
      if (val || val === 0) {
        for (let item of option) {
          if (item.value === val) {
            _back = item.label;
            break;
          };
        };
      };
      return _back;
    }
  }
};