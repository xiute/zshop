import Vue from 'vue'
import UrlPicker from './src/main'

UrlPicker.install = () => {
  Vue.component(UrlPicker.name, UrlPicker)
}

export default UrlPicker
