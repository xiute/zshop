/**
 * Created by Andste on 2018/6/15.
 * 秒杀选择器
 * 主要用于楼层
 */

import Vue from 'vue'
import EnNearbyShopsPicker from './src/main'

EnNearbyShopsPicker.install = () => {
  Vue.component(EnNearbyShopsPicker.name, EnNearbyShopsPicker)
}

export default EnNearbyShopsPicker