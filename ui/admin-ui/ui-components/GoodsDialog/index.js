/**
 * Created by Andste on 2018/5/28.
 * 商品选择器
 * 依赖于element-ui
 */

import Vue from 'vue'
import EnGoodsDialog from './src/main.vue'

EnGoodsDialog.install = () => {
  Vue.component(EnGoodsDialog.name, EnGoodsDialog)
}

export default EnGoodsDialog
