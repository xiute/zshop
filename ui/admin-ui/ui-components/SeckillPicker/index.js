/**
 * Created by Andste on 2018/6/15.
 * 秒杀选择器
 * 主要用于楼层
 */

import Vue from 'vue'
import EnSeckillPicker from './src/main'

EnSeckillPicker.install = () => {
  Vue.component(EnSeckillPicker.name, EnSeckillPicker)
}

export default EnSeckillPicker
