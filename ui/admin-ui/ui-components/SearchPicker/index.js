/**
 * Created by Andste on 2018/6/15.
 * 商品搜索
 * 主要用于楼层
 */

import Vue from 'vue'
import SearchPicker from './src/main'

SearchPicker.install = () => {
  Vue.component(SearchPicker.name, SearchPicker)
}

export default SearchPicker
