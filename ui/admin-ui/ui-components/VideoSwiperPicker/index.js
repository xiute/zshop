/**
 * Created by Andste on 2018/6/15.
 * 文本选择器
 * 主要用于楼层
 */

import Vue from 'vue'
import VideoSwiperPicker from './src/main'

VideoSwiperPicker.install = () => {
  Vue.component(VideoSwiperPicker.name, VideoSwiperPicker)
}

export default VideoSwiperPicker
