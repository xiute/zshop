/*
  * @date 2020-02-03
  * @author wolfMan
  * @description {优惠券选择器}
*/

import Vue from 'vue'
import EnCouponDialog from './src/main.vue'

EnCouponDialog.install = () => {
  Vue.component(EnCouponDialog.name, EnCouponDialog)
}

export default EnCouponDialog
