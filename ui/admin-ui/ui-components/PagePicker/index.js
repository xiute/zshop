/*
  * @date 2020-01-14
  * @author wolfMan
  * @description {文本选择器，主要用于页面装修}
*/

import Vue from 'vue'
import PagePicker from './src/main'

PagePicker.install = () => {
  Vue.component(PagePicker.name, PagePicker)
}

export default PagePicker