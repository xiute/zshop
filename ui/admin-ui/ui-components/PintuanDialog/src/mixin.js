/*
  * @date 2020-02-03
  * @author wolfMan
  * @description {}
*/

import request from '@/utils/request'
import { api } from '~/ui-domain'

export default {
  props: {
    // 显示dialog
    show: {
      type: Boolean,
      default: false
    },
    // 默认数据
    defaultData: {
      type: Array,
      default: () => ([])
    }
  },
  data() {
    return {
      /** 加载状态 */
      loading: false,
      /** 商品列表 */
      tuanData: [],
      /** 已选列表 */
      selectedList: [],
      dialogVisible: this.show
    }
  },
  watch: {
    show(newVal) {
      this.dialogVisible = newVal
      if (this.tuanData.length === 0 && newVal === true) {
        this.GET_TuanList()
      }
    },
    dialogVisible(newVal) {
      newVal === false && this.$emit('close')
    },
    'defaultData': 'defaultDataChanged'
  },
  computed: {
    /** 计算已选择个数 */
    selectedNum() {
      return this.selectedList.filter(item => item).length
    }
  },
  methods: {
    /** 确认 */
    handleConfirm() {
      if (!this.selectedList.length) {
        this.$message.error('请选择商品')
        return
      }
      let _data = [];
      _data = this.selectedList.map(item => {
        return {
          skuId: item.sku_id,
          goodsid: item.goods_id,
          image: item.thumbnail
        }
      });
      this.$emit('confirm', _data)
      this.$emit('close')
      this.$refs.tuanTable.clearSelection()
      this.selectedList = []
    },
    /** 当默认数据发生改变 */
    defaultDataChanged(newVal) {
      this.selectedList = []
      if (newVal && newVal.length > 0) {
        this.$nextTick(() => {
          this.tuanData.forEach(row => {
            if(newVal.indexOf(row.coupon_id) >= 0){
              this.$refs.tuanTable.toggleRowSelection(row, true);
            }
          })
        })
      }
    },
    /** 选择团购商品 */
    handleTuanSelect (_row) {
      this.selectedList = _row;
    },
    /** 获取团购商品列表 */
    async GET_TuanList(clean = false) {
      this.loading = true
      await request({
        url: `${api.buyer}/pintuan/goods/skus`,
        method: 'get',
        loading: false
      }).then(response => {
        this.loading = false
        // 如果clean为true，先清空tuanData
        !!clean && (this.tuanData = [])
        this.$nextTick(() => {
          this.tuanData = response
        });
      })
    }
  }
}
