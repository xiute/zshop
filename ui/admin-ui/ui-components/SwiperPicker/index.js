/**
 * Created by Andste on 2018/6/15.
 * 轮播图
 * 主要用于楼层
 */

import Vue from 'vue'
import SwiperPicker from './src/main'

SwiperPicker.install = () => {
  Vue.component(SwiperPicker.name, SwiperPicker)
}

export default SwiperPicker
