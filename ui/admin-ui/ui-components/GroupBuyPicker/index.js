/**
 * Created by Andste on 2018/6/15.
 * 文本选择器
 * 主要用于楼层
 */

import Vue from 'vue'
import GroupBuyPicker from './src/main'

GroupBuyPicker.install = () => {
  Vue.component(GroupBuyPicker.name, GroupBuyPicker)
}

export default GroupBuyPicker
