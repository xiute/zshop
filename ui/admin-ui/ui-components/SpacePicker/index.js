/**
 * Created by Andste on 2018/6/15.
 * 辅助间隔
 * 主要用于楼层
 */

import Vue from 'vue'
import SpacePicker from './src/main'

SpacePicker.install = () => {
  Vue.component(SpacePicker.name, SpacePicker)
}

export default SpacePicker
