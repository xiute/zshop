/**
 * Created by Andste on 2018/5/28.
 * 商品选择器
 * 依赖于element-ui
 */

import Vue from 'vue'
import PageDialog from './src/main.vue'

PageDialog.install = () => {
  Vue.component(PageDialog.name, PageDialog)
}

export default PageDialog
