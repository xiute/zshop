/**
 * Created by Andste on 2018/5/28.
 */

import * as API_Floor from '@/api/floor'

export default {
  props: {
    // 显示dialog
    show: {
      type: Boolean,
      default: false
    },
    // 默认数据
    defaultData: {
      type: String,
      default: ''
    }
  },
  data() {
    return {
      dialogVisible: this.show,
      // 查询列表参数
      // 列表参数
      params: {
        page_no: 1,
        page_size: 10,
        store_id: 1,
        // page_type: 'INDEX',
        clientType: 'WAP',
        page_type: '',
        page_name: ''
      },
      currentData: {},
      currentPage: undefined,
      /** 加载状态 */
      loading: false,
      /** 页面列表 */
      tableData: ''
    }
  },
  watch: {
    show(newVal) {
      this.dialogVisible = newVal
      this.GET_PageList()
    },
    dialogVisible(newVal) {
      newVal === false && this.$emit('close')
    },
  },
  filters: {
    formatPrice(price) {
      return '￥' + String(Number(price).toFixed(2)).replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    }
  },
  methods: {
    /** 分页大小发生改变 */
    handlePageSizeChange(size) {
      this.params.page_size = size
      this.GET_PageList()
    },

    /** 分页页数发生改变 */
    handlePageCurrentChange(page) {
      this.params.page_no = page
      this.GET_PageList()
    },
    /** 获取微页面列表 */
    GET_PageList(clean = false) {
      this.loading = true
      API_Floor.getPageList(this.params).then(response => {
        this.loading = false
        this.tableData = response;
      }).catch(() => { this.loading = false })
    },
    /*
      * @date 2020-01-20
      * @author wolfMan
      * @description {微页面选中}
    */
    handleCurrentChange(current) {
      this.currentData = current;
      this.currentPage = current ? current.page_id : null;
    },
    /** 确认 */
    handleConfirm() {
      if (!this.currentPage) {
        this.$message.error('请选择微页面')
        return
      }
      this.$emit('confirm', this.currentData)
      this.$emit('close')
      this.currentData = {}
      this.currentPage = undefined
    },
    searchEvent() {
      console.log(123)
    },
  }
}
