/**
 * 导出选择器
 */

import CategoryPicker from './CategoryPicker'
import GoodsPicker from './GoodsPicker'
import SheTuanPicker from './SheTuanPicker'
import ImagePicker from './ImagePicker'
import PriceRange from './PriceRange'
import RegionPicker from './RegionPicker'
import TableLayout from './TableLayout'
import TableSearch from './TableSearch'
import TextPicker from './TextPicker'
import YearMonthPicker from './YearMonthPicker'
import MemberPicker from './MemberPicker'
import VideoSwiperPicker from './VideoSwiperPicker'

import RadioTab from './RadioTab'
import RadioColor from './RadioColor'
import PagePicker from './PagePicker'
import SwiperPicker from './SwiperPicker'
import AdvertPicker from './AdvertPicker'
import SearchPicker from './SearchPicker'
import GoodsDialog from './GoodsDialog'
import SpacePicker from './SpacePicker'
import CouponPicker from './CouponPicker'
import CouponDialog from './CouponDialog'
import SeckillPicker from './SeckillPicker'
import NearbyShopsPicker from './NearbyShopsPicker'
import GroupBuyPicker from './GroupBuyPicker'
import PintuanDialog from './PintuanDialog'
import FollowPicker from './FollowPicker'
import UrlPicker from './UrlPicker'
import PageDialog from './PageDialog'
import NearShopPicker from './NearShopPicker'
import VideoPicker from './VideoPicker'

const components = {
  CategoryPicker,
  PriceRange,
  ImagePicker,
  RegionPicker,
  TableLayout,
  TableSearch,
  TextPicker,
  YearMonthPicker,
  MemberPicker,
  VideoSwiperPicker,

  RadioTab,
  RadioColor,
  PagePicker,
  SwiperPicker,
  AdvertPicker,
  GoodsPicker,
  SheTuanPicker,
  GoodsDialog,
  SearchPicker,
  SpacePicker,
  CouponPicker,
  CouponDialog,
  SeckillPicker,
  NearbyShopsPicker,
  GroupBuyPicker,
  PintuanDialog,
  FollowPicker,
  UrlPicker,
  PageDialog,
  NearShopPicker,
  VideoPicker
}

components.install = function(Vue, opts) {
  Object.keys(components).forEach(key => {
    key !== 'install' && Vue.component(components[key].name, components[key])
  })
}

export {
  CategoryPicker,
  PriceRange,
  ImagePicker,
  RegionPicker,
  TableLayout,
  TableSearch,
  TextPicker,
  YearMonthPicker,
  VideoSwiperPicker,

  RadioTab,
  RadioColor,
  PagePicker,
  SwiperPicker,
  AdvertPicker,
  GoodsPicker,
  SheTuanPicker,
  GoodsDialog,
  SearchPicker,
  SpacePicker,
  CouponPicker,
  CouponDialog,
  SeckillPicker,
  NearbyShopsPicker,
  GroupBuyPicker,
  PintuanDialog,
  FollowPicker,
  UrlPicker,
  PageDialog,
  NearShopPicker,
  VideoPicker
}

export default components
